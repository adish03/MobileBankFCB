# MobileBankCore

[![CI Status](https://img.shields.io/travis/tennet0505/MobileBankCore.svg?style=flat)](https://travis-ci.org/tennet0505/MobileBankCore)
[![Version](https://img.shields.io/cocoapods/v/MobileBankCore.svg?style=flat)](https://cocoapods.org/pods/MobileBankCore)
[![License](https://img.shields.io/cocoapods/l/MobileBankCore.svg?style=flat)](https://cocoapods.org/pods/MobileBankCore)
[![Platform](https://img.shields.io/cocoapods/p/MobileBankCore.svg?style=flat)](https://cocoapods.org/pods/MobileBankCore)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MobileBankCore is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'MobileBankCore'
```

## Author

tennet0505, tennet0505@gmail.com

## License

MobileBankCore is available under the MIT license. See the LICENSE file for more info.
