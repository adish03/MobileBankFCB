//
//  Utilities.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/22/19.
//
import ObjectMapper

// модель Categories
open class Categories: Mappable {
    
    open var
    items: [CategoriesType]?,
    date: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        items <- map["Items"]
        date <- map["Date"]
    }
}
// модель CategoriesType
open class CategoriesType: Mappable {
    
    open var
    categoryID:Int?,
    categoryName: String?,
    logoFileName: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        categoryID <- map["CategoryID"]
        categoryName <- map["CategoryName"]
        logoFileName <- map["LogoFileName"]
    }
}


// модель Utilities
open class Utilities: Mappable {
    
    open var
    items: [CategoriesById]?,
    date: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        items <- map["Items"]
        date <- map["Date"]
    }
}
// модель CategoriesById
open class CategoriesById: Mappable {
    
    open var
    categoryID: Int?,
    items: [UtilityModel]?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        categoryID <- map["CategoryID"]
        items <- map["Items"]
        
    }
}
// модель UtilityModel
open class UtilityModel: Mappable {
    
    open var
    ID:Int?,
    name: String?,
    logo: String?,
    parentID:Int?,
    inputValidate: String?,
    placeholder: String?,
    categoryID: Int?,
    currencyID: Int?,
    min: Int?,
    max: Int?,
    userRecomendation: String?,
    taxProviderTypeID: Int?,
    children: [UtilityModel]?,
    
    category: Utilities?
    
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        ID <- map["ID"]
        name <- map["Name"]
        logo <- map["Logo"]
        parentID <- map["ParentID"]
        inputValidate <- map["InputValidate"]
        placeholder <- map["Placeholder"]
        currencyID <- map["CurrencyID"]
        min <- map["Min"]
        max <- map["Max"]
        logo <- map["Logo"]
        userRecomendation <- map["UserRecomendation"]
        taxProviderTypeID <- map["TaxProviderTypeID"]
        children <- map["Children"]
    }
   
}

// модель для UtilityPaymentOperationModel
open class UtilityPaymentOperationModel: Mappable {
    
    open var
    accountNo:String?,
    currencyID: Int?,
    providerID: Int?,
    contragentID:Int?,
    personalAccountNo: String?,
    сurrentAccountNo: String?,
    
    sum: Double?,
    paymentSum: Double?,
        
    comment: String?,
    operationType:Int?,
    operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName:String?,
    templateDescription: String?,
    scheduleDate: String?,
    scheduleID: Int?,
    schedule: Schedule?,
    fullName: String?
    
    init(){}
    required public init(providerID: Int,
                         personalAccountNo: String,
                         contragentID: Int,
                         sum: Double){
        self.providerID = providerID
        self.personalAccountNo = personalAccountNo
        self.contragentID = contragentID
        self.sum = sum
    }
    required public init(accountNo: String,
                         currencyID: Int,
                         providerID: Int,
                         contragentID:Int,
                         personalAccountNo: String,
                         сurrentAccountNo: String?,
                         sum: Double,
                         paymentSum: Double?,
                         comment: String?,
                         operationID: Int?,
                         isSchedule: Bool?,
                         isTemplate: Bool?,
                         templateName: String?,
                         templateDescription: String?,
                         scheduleID: Int?,
                         schedule: Schedule?,
                         fullName: String?){
        
        self.accountNo = accountNo
        self.currencyID  = currencyID
        self.providerID = providerID
        self.contragentID = contragentID
        self.personalAccountNo = personalAccountNo
        self.сurrentAccountNo = сurrentAccountNo
        self.sum = sum
        self.paymentSum = paymentSum
        self.comment = comment
        self.operationID = operationID
        self.isSchedule = isSchedule
        self.isTemplate = isTemplate
        self.templateName = templateName
        self.templateDescription = templateDescription
        self.scheduleID = scheduleID
        self.schedule = schedule
        self.fullName = fullName
    }
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        providerID <- map["ProviderID"]
        contragentID <- map["ContragentID"]
        personalAccountNo <- map["PersonalAccountNo"]
        сurrentAccountNo <- map["CurrentAccountNo"]
        sum <- map["Sum"]
        paymentSum <- map["PaymentSumm"]
        comment <- map["Comment"]
        operationID <- map["OperationID"]
        isSchedule <- map["IsSchedule"]
        isTemplate <- map["IsTemplate"]
        templateName <- map["TemplateName"]
        templateDescription <- map["TemplateDescription"]
        scheduleDate <- map["ScheduleDate"]
        scheduleID <- map["ScheduleID"]
        schedule <- map["Schedule"]
        fullName <- map["FullName"]
    }
}

public enum TaxProviderType: Int {
    case Default = 1
    case Taxes = 2
    case Penalties = 3
    case TransportTax = 4
}

// модель DecimalExecuteResult
open class DecimalExecuteResult: Mappable {
    
    open var
    result: Double?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}
open class StringExecuteResult: Mappable {
    open var
    result: String?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    public required init?(map: Map) {}
    
    public func mapping(map: Map) {
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}
// модель SubmitForPayModel
open class SubmitForPayModel {
    
    open var
    name: String,
    props: String,
    account: String,
    contract: String,
    sum: Double,
    commission: String,
    total: String,
    description: String,
    image: String?,
    currencyID: Int
    
    required public init(name:String,
                         props: String,
                         account: String,
                         contract: String,
                         sum: Double,
                         commission: String,
                         total: String,
                         description: String,
                         image: String?,
                         currencyID: Int){
        self.name = name
        self.props = props
        self.account = account
        self.contract = contract
        self.sum = sum
        self.commission = commission
        self.total = total
        self.description = description
        self.image = image
        self.currencyID = currencyID
    }
}
// модель ExportStatementModel
open class ExportStatementModel: Mappable {
    
    open var
    accountNo: String?,
    currencyID: Int?,
    startDate: String?,
    endDate: String?,
    toExcel: Bool?
    
    
    init(){}
    required public init( accountNo: String,
                          currencyID: Int,
                          startDate: String,
                          endDate: String,
                          toExcel: Bool){
        self.accountNo = accountNo
        self.currencyID = currencyID
        self.startDate = startDate
        self.endDate = endDate
        self.toExcel = toExcel
    }
    
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        accountNo <- map["accountNo"]
        currencyID <- map["currencyID"]
        startDate <- map["startDate"]
        endDate <- map["endDate"]
        toExcel <- map["toExcel"]
    }
}



// модель OperationModel
open class OperationModel: Mappable {
    
    open var
    operationID: Int?,
    code: String?,
    operationTypeID: Int?
    
    
    init(){}
    required public init(operationID:Int,
                         code: String,
                         operationTypeID: Int){
        self.operationID = operationID
        self.code = code
        self.operationTypeID = operationTypeID
    }
    required public init(operationID:Int){
        self.operationID = operationID
    }
    
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        code <- map["Code"]
        operationTypeID <- map["OperationTypeID"]
    }
}

public struct UtilityDictionaries : Codable {
    public let Governments: [Government]?
    public let Regions: String
    public let Departments: [Department]?
    public let Services: [Service]?
    public let PaymentsTypes: [PaymentType]?
    public let PayerTypes: [PaymentType]?
    public let Comissions: [Comission]?
}

public struct Region: Codable {
    
}

public struct Comission: Codable {
    public let ProviderID: Int?
    public let ContractTypeID: Int?
    public let DirectionTypeID: Int?
    public let OfficeID: Int?
    public let StartSumm: Int?
    public let EndSumm: Int?
    public let Comission: Int?
    public let ComissionTypeID: Int?
}

public struct Department: Codable {
    public let DepartmentID: Int?
    public let DepartmentName: String?
    public let DepartmentAddress: String?
    public let TakeUserCommentAsClearingComment: Bool?
    public let IsBudgetPayments: Bool?
    public let GovernmentID: Int?
}

public struct Service: Codable {
    public let ServiceID: Int?
    public let ServiceName: String?
    public let ShortServiceName: String?
    public let PaymentCode: String?
    public let PaymentTypeID: Int?
    public let PayerTypeID: Int?
}

public struct PaymentType: Codable {
    public let Key: Int?
    public let Value: String?
}

public struct Government: Codable {
    public let GovernmentID: Int?
    public let GovernmentName: String?
    public let GovernmentCode: String?
}
