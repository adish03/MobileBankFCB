//
//  XFincaCityDictionary.swift
//  Alamofire
//
//  Created by Рустам on 19/3/22.
//

import ObjectMapper

open class XFincaCityDictionary: Mappable {
    open var
    id: Int?,
    name: String?,
    directions: [String?]?,
    currencies: [Int?]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        directions <- map["directions"]
        currencies <- map["currencies"]
    }
}
open class XFincaCountryDictionary: Mappable {
    open var
    countryID: Int?,
    shortName: String?,
    shortNameEN: String?,
    fullName: String?,
    capital: String?,
    code: Int?,
    code2: String?,
    code3: String?,
    countryCode: Int?,
    countryTypeID: Int?,
    fatfTypeID: Int?,
    isHighRisk: Bool?,
    phoneCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        countryID <- map["CountryID"]
        shortName <- map["ShortName"]
        shortNameEN <- map["ShortNameEN"]
        fullName <- map["FullName"]
        capital <- map["Capital"]
        code <- map["Code"]
        code2 <- map["Code2"]
        code3 <- map["Code3"]
        countryCode <- map["CountryCode"]
        countryTypeID <- map["CountryTypeID"]
        fatfTypeID <- map["FatfTypeID"]
        isHighRisk <- map["IsHighRisk"]
        phoneCode <- map["PhoneCode"]
    }
}

open class XFincaCommission: Mappable {
    open var
    status: Int?,
    description: String?,
    data: XFincaData?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        status <- map["Status"]
        description <- map["Description"]
        data <- map["Data"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}

open class XFincaData: Mappable {
    open var
    feeTotal: XFincaFee?,
    toCountryISO: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        feeTotal <- map["feeTotal"]
        toCountryISO <- map["toCountryISO"]
    }
}

open class XFincaFee: Mappable {
    open var
    currency: Int?,
    amount: Double?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        currency <- map["currency"]
        amount <- map["amount"]
    }
}

open class XFincaPhoneMask: Mappable {
    open var
    phoneCode: String?,
    lenghtPhoneNumber: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        phoneCode <- map["PhoneCode"]
        lenghtPhoneNumber <- map["LenghtPhoneNumber"]
    }
}
