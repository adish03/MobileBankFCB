//
//  ConversionOperationModel.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/20/19.
//

import ObjectMapper
// Модель конвертации 
open class ConversionOperationModel: Mappable {
    open var
    sellAccount: SellAccount?,
    buyAccount: BuyAccount?,
    sum: Double?,
    buySum: String?,
    buySymDouble: Double?,
    save: Bool?,
    operationType: InternetBankingOperationType?,
    operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName: String?,
    scheduleDate: String?,
    scheduleID: Int?,
    schedule: Schedule?,
    createDate: String?,
    confirmType: Int?
    
    init(){}
    required public init(sellAccount: SellAccount, buyAccount: BuyAccount, sum: Double, buy: Double, save: Bool ){
        self.sellAccount = sellAccount
        self.buyAccount = buyAccount
        self.buySum = String(format: "%.2f", buy)
        self.sum = sum
        self.save = save
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        sellAccount <- map["SellAccount"]
        buyAccount <- map["BuyAccount"]
        sum <- map["Sum"]
        buySum <- map["BuySum"]
        buySymDouble <- map["BuySum"]
        save <- map["Save"]
        operationType <- map["OperationType"]
        operationID <- map["OperationID"]
        isTemplate <- map["TemplateName"]
        isSchedule <- map["IsSchedule"]
        templateName <- map["TemplateName"]
        scheduleDate <- map["ScheduleDate"]
        scheduleID <- map["ScheduleID"]
        schedule <- map["Schedule"]
        createDate <- map["CreateDate"]
        confirmType <- map["ConfirmType"]
    }
}

// Модель для счета покупке валюты
open class SellAccount: Mappable {
    open var
    accountNo: String?,
    currencyID: Int?
    
    init(){}
    required public init(accountNo: String, currencyID: Int){
        self.accountNo = accountNo
        self.currencyID = currencyID
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
    }
}
// Модель для счета продаже валюты
open class BuyAccount: Mappable {
    open var
    accountNo: String?,
    currencyID: Int?
    
    init(){}
     required public init(accountNo: String, currencyID: Int){
        self.accountNo = accountNo
        self.currencyID = currencyID
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
    }
}

// Модель для суммы при конвертации валют
open class ConversionSum: Mappable {
    open var
    key: Int?,
    value: Double?

    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        key <- map["Key"]
        value <- map["Value"]
    }
}

