//
//  Account.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/20/19.
//

import ObjectMapper

// модель для Account
open class Account: Mappable {
    open var
    accountNo: String?,
    currencyID: Int?,
    customerID: Int?,
    officeID: Int?,
    balanceGroup: String?,
    accountName: String?,
    openDate: String?,
    endDate: Bool?,
    closeDate: String?,
    accountTypeID: Int?,
    currentBalance: Double?,
    currentNationalBalance: Double?,
    codename: String?,
    accountGroup: String?,
    currencySymbol: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        customerID <- map["CustomerID"]
        officeID <- map["OfficeID"]
        balanceGroup <- map["BalanceGroup"]
        balanceGroup <- map["BalanceGroup"]
        accountName <- map["AccountName"]
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        accountTypeID <- map["AccountTypeID"]
        currentBalance <- map["CurrentBalance"]
        currentNationalBalance <- map["CurrentNationalBalance"]
        codename <- map["Codename"]
        accountGroup <- map["AccountGroup"]
        currencySymbol <- map["CurrencySymbol"]
    }
}

// модель для Deposit
open class Deposit: Mappable {
    open var
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: Bool?,
    imageName: String?,
    name: String?,
    accountNo: String?,
    currencyID: Int?,
    currencySymbol: String?,
    isShowButton: Bool?,
    
    currency: Currency?
    
    init(){}
    required public init( isShowButton: Bool?)
    {
        self.isShowButton = isShowButton
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        imageName <- map["ImageName"]
        name <- map["Name"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        currencySymbol <- map["CurrencySymbol"]
    }
}

// модель для Accounts
open class Accounts: Mappable{
    open var
    text: String?,
    accountType: Int?,
    isExpandable: Bool?,
    items: [Deposit]?
    
    init(){}
    
    required public init( isExpandable: Bool?){
        self.isExpandable = isExpandable
    }
    
    required public init(text: String?,
                         accountType: Int?,
                         isExpandable: Bool?,
                         items: [Deposit]?){
        self.text = text
        self.accountType = accountType
        self.isExpandable = isExpandable
        self.items = items
        
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        text <- map["Text"]
        accountType <- map["AccountType"]
        items <- map["Items"]
    }
    //    функция для создания копии счетов
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = Accounts(text: text, accountType: accountType, isExpandable: isExpandable, items: items)
        return copy
    }
}
// модель для PaymentCode
open class PaymentCode: Mappable {
    open var
    code: String?,
    description: String?,
    treasuryCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        code <- map["Code"]
        description <- map["Description"]
        treasuryCode <- map["TreasuryCode"]
    }
}
// модель для BikCode
open class BikCode: Mappable {
    open var
    code: String?,
    name: String?,
    mfo: String?,
    corrNBKF: String?,
    regionName: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        code <- map["Code"]
        name <- map["Name"]
        mfo <- map["Mfo"]
        corrNBKF <- map["CorrNBKF"]
        regionName <- map["RegionName"]
    }
}


// модель для SubmitForCurrenciesModel
open class SubmitForCurrenciesModel {
    
    open var
    accountFrom: String,
    sumSale: String,
    accountTo: String,
    sumBuy: String,
    converssions: Double,
    transactionNumber: String,
    dateTime: String
    
    required public init(accountFrom:String,
                         sumSale: String,
                         accountTo: String,
                         sumBuy: String,
                         converssions: Double,
                         transactionNumber: String,
                         dateTime: String){
        self.accountFrom = accountFrom
        self.sumSale = sumSale
        self.accountTo = accountTo
        self.sumBuy = sumBuy
        self.converssions = converssions
        self.transactionNumber = transactionNumber
        self.dateTime = dateTime
    }
    
}
// модель для списка истории операций
open class ListContractModelExecuteResult: Mappable {
    
    open var
    paginationInfo: PaginationInfo?,
    result: [ContractModel]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        paginationInfo <- map["PaginationInfo"]
        result <- map["Result"]
    }
}
// модель для пагинаций
open class PaginationInfo: Mappable {
    
    open var
    page: Int?,
    pageSize: Int?,
    totalItems: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        page <- map["Page"]
        pageSize <- map["PageSize"]
        totalItems <- map["TotalItems"]
        
    }
}
// модель для списка операций
open class ContractModel: Mappable {
    open var
    operationID: Int?,
    confirmTypeID: Int?,
    date: String?,
    description: String?,
    operationType: String?,
    confirmType: String?,
    isSchedule: Bool?,
    isLock: Bool?,
    amount: Double?,
    currencyID: Int?,
    operationTypeID: Int?,
    operationParameters: Int?,
    imageName: String?,
    currency: Currency?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        confirmTypeID <- map["ConfirmTypeID"]
        date <- map["Date"]
        description <- map["Description"]
        operationType <- map["OperationType"]
        confirmType <- map["ConfirmType"]
        isSchedule <- map["IsSchedule"]
        isLock <- map["IsLock"]
        amount <- map["Amount"]
        currencyID <- map["CurrencyID"]
        operationTypeID <- map["OperationTypeID"]
        operationParameters <- map["OperationParameters"]
        imageName <- map["ImageName"]
        
    }
}

// модель для деталей счетов
open class DetailModel: Mappable {
    open var
    accountName: String?,
    inBalance: Double?,
    outBalance: Double?,
    operations: [AccountOperation]?,
    totalOperationsCount: Int?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountName <- map["AccountName"]
        inBalance <- map["InBalance"]
        outBalance <- map["OutBalance"]
        operations <- map["Operations"]
        totalOperationsCount <- map["TotalOperationsCount"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        
        
    }
}

// модель для закрытии депозита
open class CloseDepositModel: Mappable {
    open var
    accountNo: String?,
    currencyID: Int?,
    corrAccountNo: String?,
    reason: String?
    
    init(){}
    required public init(
        accountNo: String?,
        currencyID: Int?,
        corrAccountNo: String?,
        reason: String?
        ){
        self.accountNo = accountNo
        self.currencyID = currencyID
        self.corrAccountNo = corrAccountNo
        self.reason = reason
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        corrAccountNo <- map["CorrAccountNo"]
        reason <- map["Reason"]
    }
}

// модель для документов
open class ReportsExecuteResult: Mappable {
    open var
    reportDocuments: [Document]?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        reportDocuments <- map["ReportDocuments"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        
    }
}

// модель для документа
open class Document: Mappable {
    open var
    fileName: String?,
    fileBody: String?,
    contentType: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        fileName <- map["FileName"]
        fileBody <- map["FileBody"]
        contentType <- map["ContentType"]
        
    }
}


// модель для деталей
open class AccountOperation: Mappable {
    open var
    postion: Int?,
    transactionDate: String?,
    documentNo: String?,
    dtSumm: Double?,
    ctSumm: Double?,
    comment: String?,
    operationDate: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        postion <- map["Postion"]
        transactionDate <- map["TransactionDate"]
        documentNo <- map["DocumentNo"]
        dtSumm <- map["DtSumm"]
        ctSumm <- map["CtSumm"]
        comment <- map["Comment"]
        operationDate <- map["OperationDate"]
    }
}
// модель для DetailAccountsModel
open class DetailAccountsModel: Mappable {
    open var
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: Bool?,
    imageName: String?,
    name: String?,
    accountNo: String?,
    currencyID: Int?,
    currencySymbol: String?,
    isShowButton: Bool?
    
    init(){}
    required public init( isShowButton: Bool?)
    {
        self.isShowButton = isShowButton
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        imageName <- map["ImageName"]
        name <- map["Name"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        currencySymbol <- map["CurrencySymbol"]
    }
}


// модель для деталей депозитов
open class DetailDepositsModel: Mappable {
    open var
    percentAccountNo: String?,
    percentAccountName: String?,
    percentAccountCloseDate: String?,
    percentsSumm: Double?,
    period: Int?,
    programID: Int?,
    interestRate: Double?,
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: Bool?,
    imageName: String?,
    name: String?,
    currencySymbol: String?,
    accountNo: String?,
    isShowButton: Bool?,
    currencyID: Int?
    
    init(){}
    required public init( isShowButton: Bool?)
    {
        self.isShowButton = isShowButton
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        percentAccountNo <- map["PercentAccountNo"]
        percentAccountName <- map["PercentAccountName"]
        percentAccountCloseDate <- map["PercentAccountCloseDate"]
        percentsSumm <- map["PercentsSumm"]
        period <- map["Period"]
        programID <- map["ProgramID"]
        interestRate <- map["InterestRate"]
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        imageName <- map["ImageName"]
        name <- map["Name"]
        currencySymbol <- map["CurrencySymbol"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
    }
}

// модель для деталей карт
open class DetailCardsModel: Mappable {
    open var
    codeWord: String?,
    companyID: Int?,
    companyName: String?,
    productID: Int?,
    productName: String?,
    period: Int?,
    balanceGroup: String?,
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: String?,
    imageName: String?,
    name: String?,
    currencySymbol: String?,
    accountNo: String?,
    currencyID: Int?,
    availableLimit: Float?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        codeWord <- map["CodeWord"]
        companyID <- map["CompanyID"]
        companyName <- map["CompanyName"]
        productID <- map["ProductID"]
        productName <- map["ProductName"]
        period <- map["Period"]
        balanceGroup <- map["BalanceGroup"]
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        imageName <- map["ImageName"]
        name <- map["Name"]
        currencySymbol <- map["CurrencySymbol"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        availableLimit <- map["AvailableLimit"]
        
    }
}

// модель для деталей кредита
open class CreditModel: Mappable {
    open var
    IsRequest: Bool?,
    CreditID: Int?,
    ProductTypeID: Int?,
    ProductName: String?,
    GroupName: String?,
    StatusID: Int?,
    StatusName: String?,
    StatusDate: String?,
    ApprovedSumm: Double?,
    ApprovedCurrency: String?,
    CurrencyID: Int?,
    ApprovedRate: Double?,
    ApprovedPeriod: Int?,
    OfficeName: String?,
    AgreementNo: String?,
    IsTranchesAllowed: String?,
    EffectiveRate: Double?,
    AccountNo: String?,
    AccBalance: Double?,
    CreditFines: Int?,
    OverdueDays: Int?,
    NextPaymentDate: String?,
    NotCalculationStatus: Bool?,
    StopCalculationStatus: Bool?,
    ExceedMaxFinesCredits: Bool?,
    LoanAccountNo: String?,
    PercentAccountNo: String?,
    Tranches: String?,
    isShowButton: Bool?
    
    init(){}
    required public init( isShowButton: Bool?)
    {
        self.isShowButton = isShowButton
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        IsRequest <- map["IsRequest"]
        CreditID <- map["CreditID"]
        ProductTypeID <- map["ProductTypeID"]
        ProductName <- map["ProductName"]
        GroupName <- map["GroupName"]
        StatusID <- map["StatusID"]
        StatusName <- map["StatusName"]
        StatusDate <- map["StatusDate"]
        ApprovedSumm <- map["ApprovedSumm"]
        ApprovedCurrency <- map["ApprovedCurrency"]
        CurrencyID <- map["CurrencyID"]
        ApprovedRate <- map["ApprovedRate"]
        ApprovedPeriod <- map["ApprovedPeriod"]
        OfficeName <- map["OfficeName"]
        AgreementNo <- map["AgreementNo"]
        IsTranchesAllowed <- map["IsTranchesAllowed"]
        EffectiveRate <- map["EffectiveRate"]
        AccountNo <- map["AccountNo"]
        AccBalance <- map["AccBalance"]
        CreditFines <- map["CreditFines"]
        OverdueDays <- map["OverdueDays"]
        NextPaymentDate <- map["NextPaymentDate"]
        NotCalculationStatus <- map["NotCalculationStatus"]
        StopCalculationStatus <- map["StopCalculationStatus"]
        ExceedMaxFinesCredits <- map["ExceedMaxFinesCredits"]
        LoanAccountNo <- map["LoanAccountNo"]
        PercentAccountNo <- map["PercentAccountNo"]
        Tranches <- map["Tranches"]
        
    }
}

// модель для деталей кредитов
open class DetailCreditsModel: Mappable {
    open var
    credits: [CreditModel]?,
    requests: [CreditModel]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        credits <- map["Credits"]
        requests <- map["Requests"]
        
    }
}
// модель для AllFinance
open class AllFinance {
    
    open var
    text: String,
    accountsType: Int,
    isExpandable: Bool?,
    items: [AnyObject]
    
    required public init(text:String,
                         accountsType: Int,
                         items:[AnyObject]){
        self.text = text
        self.accountsType = accountsType
        self.items = items
    }
    required public init(text:String,
                         accountsType: Int,
                         isExpandable: Bool,
                         items:[AnyObject]){
        self.text = text
        self.accountsType = accountsType
        self.isExpandable = isExpandable
        self.items = items
    }
    
    //    функция для создания копии финансов
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = AllFinance(text: text, accountsType: accountsType, isExpandable: isExpandable ?? false, items: items)
        return copy
    }
}
// модель для деталей карты

open class DetailCardModel: Mappable {
    open var
    balance: Double?,
    cardNo: String?,
    status: Int?,
    expiryDate: String?,
    condSet: String?,
    baseSupp: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        balance <- map["Balance"]
        cardNo <- map["CardNo"]
        status <- map["Status"]
        expiryDate <- map["ExpiryDate"]
        condSet <- map["CondSet"]
        baseSupp <- map["BaseSupp"]
    }
}

// модель для  Products

open class Products: Mappable {
    open var
    result: ProductItems?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        
    }
}
// модель для ProductItems
open class ProductItems: Mappable {
    open var
    customerOrganizationType: Int?,
    isCustomerLegalEntity: Bool?,
    selectableProductItems: [SelectableProductItem]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        customerOrganizationType <- map["CustomerOrganizationType"]
        isCustomerLegalEntity <- map["IsCustomerLegalEntity"]
        selectableProductItems <- map["SelectableProductItems"]
    }
}
// модель для SelectableProductItem
open class SelectableProductItem: Mappable {
    open var
    organizationType: Int?,
    organizationTypeName: String?,
    products: [Product]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        organizationType <- map["OrganizationType"]
        organizationTypeName <- map["OrganizationTypeName"]
        products <- map["Products"]
    }
}
// модель для Product
open class Product: Mappable {
    open var
    Name: String?,
    Description: String?,
    ImageName: String?,
    ProductID: Int?,
    Currencies: [PeriodByCurrency]?,
    CapitalizationAgreementMessage: String?,
    ConfirmationAgreementMessage: String?

    
    init(){}
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        Name <- map["Name"]
        Description <- map["Description"]
        ImageName <- map["ImageName"]
        ProductID <- map["ProductID"]
        Currencies <- map["Currencies"]
        CapitalizationAgreementMessage <- map["CapitalizationAgreementMessage"]
        ConfirmationAgreementMessage <- map["ConfirmationAgreementMessage"]
    }
}
// модель для PeriodByCurrency
open class PeriodByCurrency: Mappable {
    open var symbol: String?,
    currencyID: Int?,
    periods: [Period]?
    
    init(){}
    required public init( symbol: String?)
    {
        self.symbol = symbol
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        symbol <- map["Symbol"]
        currencyID <- map["CurrencyID"]
        periods <- map["Periods"]
        
    }
}
// модель для Period
open class Period: Mappable {
    open var
    
    periodType: Int?,
    currencyID: Int?,
    rate: Int?,
    startPeriod: Int?,
    endPeriod: Int?,
    minSummOnAccount: Int?,
    periodCount: Int?,
    productID: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        periodType <- map["PeriodType"]
        currencyID <- map["CurrencyID"]
        rate <- map["Rate"]
        startPeriod <- map["StartPeriod"]
        endPeriod <- map["EndPeriod"]
        periodCount <- map["PeriodCount"]
        minSummOnAccount <- map["MinSummOnAccount"]
        productID <- map["ProductID"]
    }
}
// модель для OpenDepositModel
open class OpenDepositModel: Mappable {
    open var
    
    accountNo: String?,
    currencyID: Int?,
    period: Int?,
    productID: Int?,
    sumV: Double?
    
    init(){}
    required public init(  accountNo: String?,
                           currencyID: Int?,
                           period: Int?,
                           productID: Int?,
                           sumV: Double?)
    {
        self.accountNo = accountNo
        self.currencyID = currencyID
        self.period = period
        self.productID = productID
        self.sumV = sumV
        
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        period <- map["PeriodCount"]
        productID <- map["ProductID"]
        sumV <- map["SumV"]
        
    }
}

open class DepositPublicOfferExecuteResult: Mappable {
    open var
    
    accountNo: String?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init(  accountNo: String?,
                           state: Int?,
                           message: String?,
                           messageCode: String?)
    {
        self.accountNo = accountNo
        self.state = state
        self.message = message
        self.messageCode = messageCode
        
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        accountNo <- map["AccountNo"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        
    }
}


open class GetStatus: Mappable {
    open var
    message: String?,
    messageCode: String?,
    result: [String]?,
    state: Int?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        result <- map["Result"]
        state <- map["State"]
    }
}

