//
//  ConfirmModel.swift
//  MobileBankCore
//
//  Created by Рустам on 7/8/21.
//

public struct ConfirmModel {
    public let depositName: String
    public let currency: String
    public let openDate: String
    public let closeDate: String
    public let termDay: String
    public let percenteageBet: String
    public let copitalization: String
    public let agreement: String
    
    public init (depositName: String,
                 currency: String,
                 openDate: String,
                 closeDate: String,
                 termDay: String,
                 percentageBet: String,
                 copitalization: String,
                 agreement: String){
        self.depositName = depositName
        self.currency = currency
        self.openDate = openDate
        self.closeDate = closeDate
        self.termDay = termDay
        self.percenteageBet = percentageBet
        self.copitalization = copitalization
        self.agreement = agreement
    }
}
