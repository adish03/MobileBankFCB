//
//  Currencies.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/6/19.
//

import ObjectMapper
//  модель CurrencyRate
open class CurrencyRate: Mappable {
    
    open var currencyID:Int?,
    crossCurrencyID: Int?,
    sellRate: Double?,
    buyRate: Double?,
    diffSellRate:  Double?,
    diffBuyRate:  Double?,
    currency:Currency?,
    crossCurrency: Currency?
    
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        currencyID <- map["CurrencyID"]
        crossCurrencyID <- map["CrossCurrencyID"]
        sellRate <- map["SellRate"]
        buyRate <- map["BuyRate"]
        diffSellRate <- map["DiffSellRate"]
        diffBuyRate <- map["DiffBuyRate"]
        
    }
}
//  модель Currency
open class Currency: Mappable {
    
    open var
    currencyID:Int?,
    currencyName: String?,
    symbol: String?,
    name1: String?,
    name2: String?,
    name3: String?,
    nameC1: String?,
    nameC2: String?,
    nameC3: String?,
    countryID: String?,
    genderTypeID: Int?,
    
    currencyRate: CurrencyRate?
    
    
    init(){}
    required public init(currencyID:Int?,
                         currencyName: String?,
                         symbol: String?
        ){
        self.currencyID = currencyID
        self.currencyName = currencyName
        self.symbol = symbol
        
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        currencyID <- map["CurrencyID"]
        currencyName <- map["CurrencyName"]
        symbol <- map["Symbol"]
        name1 <- map["Name1"]
        name2 <- map["Name1"]
        name3 <- map["Name1"]
        nameC1 <- map["NameC1"]
        nameC2 <- map["NameC1"]
        nameC3 <- map["NameC1"]
        countryID <- map["CountryID"]
        genderTypeID <- map["GenderTypeID"]
        
    }
}

//  модель CurrencyNational
open class CurrencyNational: Mappable {
    
    open var
    currencyID:Int?,
    currencyName: String?,
    symbol: String?,
    
    currency:Currency?
    
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        currencyID <- map["CurrencyID"]
        currencyName <- map["currencyName"]
        symbol <- map["Symbol"]
    }
}
