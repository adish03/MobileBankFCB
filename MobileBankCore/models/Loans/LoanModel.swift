//
//  LoanModel.swift
//  Alamofire
//
//  Created by Oleg Ten on 5/22/19.
//

import ObjectMapper
// кредитная модель LoanPaymentModel
open class LoanPaymentModel: Mappable {
    open var
    creditID: Int?,
    tranchID: Int?,
    accountNo: String?,
    currentAccountNo: String?,
    isTrancheable: Bool?,
    currentAccountBalance: Double?,
    paymentSumm: Double?,
    currencyID: Int?,
    paymentType: Int?,
    programName: String?,
    mainSumm: AmountDescriptionModel?,
    percentsSumm: AmountDescriptionModel?,
    finesSumm: AmountDescriptionModel?,
    totalSumm: AmountDescriptionModel?,
    loan: LoanInfoModel?,
    operationType: Int?,
    operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName: String?,
    templateDescription: String?,
    scheduleDate: String?,
    scheduleID: Int?,
    schedule: String?,
    confirmType: Int?,
    createDate: String?
    
    init(){}
    required public init(creditID: Int?,
                         tranchID: Int?,
                         accountNo: String?,
                         currentAccountNo: String?,
                         currentAccountBalance: Double?,
                         paymentSumm: Double?,
                         currencyID: Int?,
                         paymentType: Int?,
                         programName: String?,
                         operationType: Int?,
                         operationID: Int?,
                         isSchedule: Bool?,
                         isTemplate: Bool?,
                         templateName: String?,
                         templateDescription: String?)
    
    {
        self.creditID = creditID
        self.tranchID = tranchID
        self.accountNo = accountNo
        self.currentAccountNo = currentAccountNo
        self.currentAccountBalance = currentAccountBalance
        self.paymentSumm = paymentSumm
        self.currencyID = currencyID
        self.paymentType = paymentType
        self.programName = programName
        self.programName = programName
        self.operationType = operationType
        self.operationID = operationID
        self.isSchedule = isSchedule
        self.isTemplate = isTemplate
        self.templateName = templateName
        self.templateDescription = templateDescription
    }
    
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
      
       creditID <- map["CreditID"]
       tranchID <- map["TranchID"]
       accountNo <- map["AccountNo"]
       currentAccountNo <- map["CurrentAccountNo"]
       isTrancheable <- map["IsTrancheable"]
       currentAccountBalance <- map["CurrentAccountBalance"]
       paymentSumm <- map["PaymentSumm"]
       currencyID <- map["CurrencyID"]
       paymentType <- map["PaymentType"]
       programName <- map["ProgramName"]
       mainSumm <- map["MainSumm"]
       percentsSumm <- map["PercentsSumm"]
       finesSumm <- map["FinesSumm"]
       totalSumm <- map["TotalSumm"]
       loan <- map["Loan"]
       operationType <- map["OperationType"]
       operationID <- map["OperationID"]
       isSchedule <- map["IsSchedule"]
       isTemplate <- map["IsTemplate"]
       templateName <- map["TemplateName"]
       templateDescription <- map["TemplateDescription"]
       scheduleDate <- map["ScheduleDate"]
       scheduleID <- map["ScheduleID"]
       schedule <- map["Schedule"]
       confirmType <- map["ConfirmType"]
       createDate <- map["CreateDate"]
        
    }
}
// кредитная модель
open class AmountDescriptionModel: Mappable{
    open var
    amount: Int?,
    description: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        amount <- map["Amount"]
        description <- map["Description"]
    }
}

// кредитная модель LoanInfoModel
open class LoanInfoModel: Mappable{
    open var
   
    tranchID: Int?,
    interestRate: Int?,
    overdueDays: Int?,
    loanBalance: Double?,
    nextPaymentDate: String?,
    actualStatus: Int?,
    statusDate: String?,
    closeDate: String?,
    issueDate: String?,
    loanSumm: Double?,
    agreementNo: String?,
    period: Int?,
    percentsToPay: Int?,
    finesSumm: Double?
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
       
        tranchID <- map["TranchID"]
        interestRate <- map["InterestRate"]
        overdueDays <- map["OverdueDays"]
        loanBalance <- map["LoanBalance"]
        nextPaymentDate <- map["NextPaymentDate"]
        actualStatus <- map["ActualStatus"]
        statusDate <- map["StatusDate"]
        closeDate <- map["CloseDate"]
        issueDate <- map["IssueDate"]
        loanSumm <- map["LoanSumm"]
        agreementNo <- map["AgreementNo"]
        period <- map["Period"]
        percentsToPay <- map["PercentsToPay"]
        finesSumm <- map["FinesSumm"]
        
    }
}
// кредитная модель LoanProductModel
open class LoanProductModel: Mappable{
    open var
    iD: Int?,
    name: String?,
    description: String?,
    imageName: String?,
    mortrageTypes: [LoanProductMortrageTypeModel]?
    
    init(){}
    required public init( name: String?)
    {
        self.name = name
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
       iD <- map["ID"]
       name <- map["Name"]
       description <- map["Description"]
       imageName <- map["ImageName"]
       mortrageTypes <- map["MortrageTypes"]
        
    }
}
// кредитная модель LoanProductMortrageTypeModel
open class LoanProductMortrageTypeModel: Mappable{
    open var
    name: String?,
    mortrageTypeID: Int?,
    approveTypes: [LoanProductApproveTypeModel]?
    
    init(){}
    required public init( name: String?)
    {
        self.name = name
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        mortrageTypeID <- map["MortrageTypeID"]
        approveTypes <- map["ApproveTypes"]
        
    }
}
// кредитная модель LoanProductApproveTypeModel
open class LoanProductApproveTypeModel: Mappable{
    open var
    name: String?,
    approveTypeID: Int?,
    currencies: [LoanProductCurrencyModel]?
    
    init(){}
    required public init( name: String?)
    {
        self.name = name
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        approveTypeID <- map["ApproveTypeID"]
        currencies <- map["Currencies"]
        
    }
}
// кредитная модель LoanProductCurrencyModel
open class LoanProductCurrencyModel: Mappable{
    open var
    name: String?,
    currencyID: Int?,
    interests: [LoanProductInterestModel]?
    
    init(){}
    required public init( name: String?)
    {
        self.name = name
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        currencyID <- map["CurrencyID"]
        interests <- map["Interests"]
        
    }
}
// кредитная модель LoanProductInterestModel
open class LoanProductInterestModel: Mappable{
    open var
    
    minPeriod: Int?,
    maxPeriod: Int?,
    minSumm: Double?,
    maxSumm: Double?,
    minRate: Double?,
    maxRate: Double?
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        minPeriod <- map["MinPeriod"]
        maxPeriod <- map["MaxPeriod"]
        minSumm <- map["MinSumm"]
        maxSumm <- map["MaxSumm"]
        minRate <- map["MinRate"]
        maxRate <- map["MaxRate"]
        
    }
}

// кредитная модель CreateRequestLoanModel
open class CreateRequestLoanModel: Mappable{
    open var
    productID: Int?,
    mortrageTypeID: Int?,
    approveTypeID: Int?,
    currencyID: Int?,
    period: Int?,
    amount: Double?,
    rate: Double?,
    creditPurpose: String?
    
    init(){}
    required public init(
        productID: Int?,
        mortrageTypeID: Int?,
        approveTypeID: Int?,
        currencyID: Int?,
        period: Int?,
        amount: Double?,
        rate: Double?,
        creditPurpose: String?
        ){
        
        self.productID = productID
        self.mortrageTypeID = mortrageTypeID
        self.approveTypeID = approveTypeID
        self.currencyID = currencyID
        self.period = period
        self.amount = amount
        self.rate = rate
        self.creditPurpose = creditPurpose
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
   
        productID <- map["ProductID"]
        mortrageTypeID <- map["MortrageTypeID"]
        approveTypeID <- map["ApproveTypeID"]
        currencyID <- map["CurrencyID"]
        period <- map["Period"]
        amount <- map["Amount"]
        rate <- map["Rate"]
        creditPurpose <- map["CreditPurpose"]
        
    }
}
// кредитная модель LoanRequestResultModel
open class LoanRequestResultModel: Mappable{
    open var
    creditID: Int?,
    creditStatus: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        creditID <- map["CreditID"]
        creditStatus <- map["CreditStatus"]
        
    }
}
// кредитная модель RejectCreditRequestModel
open class RejectCreditRequestModel: Mappable{
    open var
    creditID: Int?,
    reason: String?
    
    init(){}
    required public init(
        creditID: Int?,
        reason: String?
        ){
        self.creditID = creditID
        self.reason = reason
       
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        creditID <- map["CreditID"]
        reason <- map["Reason"]
        
    }
}
// кредитная модель Модель для расчета погашения
open class CalculateLoanPaymentModel: Mappable{
    open var
   
    paymentSumm: Double?,
    creditID: Int?,
    tranchID: Int?,
    paymentType: Int?
    
    init(){}
    required public init(
        paymentSumm: Double?,
        creditID: Int?,
        tranchID: Int?,
        paymentType: Int?
        
        ){
        self.paymentSumm = paymentSumm
        self.creditID = creditID
        self.tranchID = tranchID
        self.paymentType = paymentType
        
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        paymentSumm <- map["PaymentSumm"]
        creditID <- map["CreditID"]
        tranchID <- map["TranchID"]
        paymentType <- map["PaymentType"]
        
    }
}

// кредитная модель Получение расчета CalculateLoanPaymentResult
open class CalculateLoanPaymentResult: Mappable{
    open var
    mainSumm: Double?,
    mainSummCalculationComment: String?,
    percentSumm: Double?,
    percentSummCalculationComment: String?,
    fineSumm: Double?,
    fineSummCalculationComment: String?,
    comissionSumm: Double?,
    comissionSummCalculationComment: String?,
    tranchUnusedSumm: Double?,
    tranchUnusedSummCalculationComment: String?,
    totalSumm: Double?,
    totalSummCalculationComment: String?,
    hasErrors: Bool?,
    errorMessage: String?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
      mainSumm <- map["MainSumm"]
      mainSummCalculationComment <- map["MainSummCalculationComment"]
      percentSumm <- map["PercentSumm"]
      percentSummCalculationComment <- map["PercentSummCalculationComment"]
      fineSumm <- map["FineSumm"]
      fineSummCalculationComment <- map["FineSummCalculationComment"]
      comissionSumm <- map["ComissionSumm"]
      comissionSummCalculationComment <- map["ComissionSummCalculationComment"]
      tranchUnusedSumm <- map["TranchUnusedSumm"]
      tranchUnusedSummCalculationComment <- map["TranchUnusedSummCalculationComment"]
      totalSumm <- map["TotalSumm"]
      totalSummCalculationComment <- map["TotalSummCalculationComment"]
      hasErrors <- map["HasErrors"]
      errorMessage <- map["ErrorMessage"]
      state <- map["State"]
      message <- map["Message"]
      messageCode <- map["MessageCode"]
        
        
    }
}

// кредитная модель CalculatePlanLoanPayment
open class CalculatePlanLoanPayment: Mappable{
    open var
    
    planDate: String?,
    creditID: Int?,
    tranchID: Int?,
    paymentType: Int?
    
    init(){}
    required public init(
        planDate: String?,
        creditID: Int?,
        tranchID: Int?,
        paymentType: Int?
        
        ){
        self.planDate = planDate
        self.creditID = creditID
        self.tranchID = tranchID
        self.paymentType = paymentType
        
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        planDate <- map["PlanDate"]
        creditID <- map["CreditID"]
        tranchID <- map["TranchID"]
        paymentType <- map["PaymentType"]
        
    }
}

// кредитная модель LoanGrafic
open class LoanGraficModel: Mappable{
    open var
    customerID: Int?,
    customerFullName: String?,
    loanId: Int?,
    trancheId: Int?,
    scheduleId: Int?,
    forceScheduleRecalculation: Bool?,
    loanScheduleModel: [LoanScheduleModel]?,
    loanScheduleDeferredPercent: [LoanScheduleDeferredPercent]?,
    isNoSchedule: Bool?,
    currency: String?,
    scheduleSettings: ScheduleSettings?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        customerID <- map["CustomerID"]
        customerFullName <- map["CustomerFullName"]
        loanId <- map["LoanId"]
        trancheId <- map["TrancheId"]
        scheduleId <- map["ScheduleId"]
        forceScheduleRecalculation <- map["ForceScheduleRecalculation"]
        loanScheduleModel <- map["LoanScheduleModel"]
        loanScheduleDeferredPercent <- map["LoanScheduleDeferredPercent"]
        isNoSchedule <- map["IsNoSchedule"]
        currency <- map["Currency"]
        scheduleSettings <- map["ScheduleSettings"]
        
    }
}

// кредитная модель LoanGrafic
open class LoanScheduleModel: Mappable{
    open var
    graphicId: Int?,
    payDate: String?,
    mainSumm: Double?,
    percentsSumm: Double?,
    deferredPercentsSumm: Double?,
    totalSumm: Double?,
    baseDeptToPay: Double?,
    daysInCalculation: Int?,
    salesTaxOnPercentsSumm: Double?
   
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        graphicId <- map["GraphicId"]
        payDate <- map["PayDate"]
        mainSumm <- map["MainSumm"]
        percentsSumm <- map["PercentsSumm"]
        deferredPercentsSumm <- map["DeferredPercentsSumm"]
        totalSumm <- map["TotalSumm"]
        baseDeptToPay <- map["BaseDeptToPay"]
        daysInCalculation <- map["DaysInCalculation"]
        salesTaxOnPercentsSumm <- map["SalesTaxOnPercentsSumm"]
        
        
    }
}

// кредитная модель LoanScheduleDeferredPercent
open class LoanScheduleDeferredPercent: Mappable{
    open var
    graphicId: Int?,
    isDeferredPercents: Bool?
    
    init(){}
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        graphicId <- map["GraphicId"]
        isDeferredPercents <- map["IsDeferredPercents"]
        
    }
}
// кредитная модель ScheduleSettings
open class ScheduleSettings: Mappable{
    open var
    startDate: String?,
    rate: Int?,
    firstPaymentDate: String?
    
    init(){}
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        startDate <- map["StartDate"]
        rate <- map["Rate"]
        firstPaymentDate <- map["FirstPaymentDate"]
        
    }
}
// кредитная модель ScheduleSettings
open class ReferenceCreditItemModel: Mappable{
    open var
    dependency: String?,
    value: Int?,
    text: String?
    
    init(){}
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
       dependency <- map["Dependency"]
       value <- map["Value"]
       text <- map["Text"]
        
    }
}

// кредитная модель LoanInProcessModel
open class LoanInProcessModel: Mappable{
    open var
    creditID: Int?,
    productName: String?,
    mortrageType: String?,
    incomeApproveTypes: String?,
    currency: String?,
    period: Int?,
    rate: Int?,
    summ: Int?,
    creditPurpose: String?,
    statusID: Int?,
    statusName: String?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        creditID <- map["CreditID"]
        productName <- map["ProductName"]
        mortrageType <- map["MortrageType"]
        incomeApproveTypes <- map["IncomeApproveTypes"]
        currency <- map["Currency"]
        period <- map["Period"]
        rate <- map["Rate"]
        summ <- map["Summ"]
        creditPurpose <- map["CreditPurpose"]
        statusID <- map["StatusID"]
        statusName <- map["StatusName"]
        
        
    }
}
