//
//  MapModel.swift
//  Alamofire
//
//  Created by Oleg Ten on 6/4/19.
//

import ObjectMapper
import CoreLocation

//  модель Offices
open class OfficesAndDevices: Mappable{
    open var
    result: [OfficeDeviceModel]?,
    state: String?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
    
}

// кредитная модель OfficeModel
open class OfficeDeviceModel: Mappable {
    open var
    iD: Int?,
    branchID: Int?,
    mainOfficeID: Int?,
    iDDevice: Int?,
    nameDevice: String?,
    adress: String?,
    name: String?,
    shortName: String?,
    address: String?,
    phone: String?,
    fax: String?,
    openDate: String?,
    closeDate: String?,
    managerName: String?,
    managerPosition: String?,
    attorneyNo: String?,
    attorneyIssueDate: String?,
    isMainOffice: Bool?,
    adminDepartmentPosition: String?,
    adminDepartmentManager: String?,
    cityID: Int?,
    schedule: String?,
    latitude: Double?,
    longitude: Double?,
    schedules: [ScheduleDevice]?,
    distance: Double?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        iD <- map["ID"]
        branchID <- map["BranchID"]
        mainOfficeID <- map["MainOfficeID"]
        iDDevice <- map["IDDevice"]
        nameDevice <- map["NameDevice"]
        adress <- map["Address"]
        name <- map["Name"]
        shortName <- map["ShortName"]
        address <- map["Address"]
        phone <- map["Phone"]
        fax <- map["Fax"]
        openDate <- map["OpenDate"]
        closeDate <- map["CloseDate"]
        managerName <- map["ManagerName"]
        managerPosition <- map["ManagerPosition"]
        attorneyNo <- map["AttorneyNo"]
        attorneyIssueDate <- map["AttorneyIssueDate"]
        isMainOffice <- map["IsMainOffice"]
        adminDepartmentPosition <- map["AdminDepartmentPosition"]
        adminDepartmentManager <- map["AdminDepartmentManager"]
        cityID <- map["CityID"]
        schedule <- map["Schedule"]
        latitude <- map["Latitude"]
        longitude <- map["Longitude"]
        schedules <- map["Schedules"]
    }
    
    open var locationCoordinate:CLLocationCoordinate2D?{
        get{
            if let latitude = latitude,
            let longitude = longitude{
                return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            } else {
                return nil
            }
        }
    }
}


// кредитная модель DevicesModel
open class ScheduleDevice: Mappable {
    open var
    officeID: Int?,
    day: Int?,
    workTime: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        officeID <- map["OfficeID"]
        day <- map["Day"]
        workTime <- map["WorkTime"]
       
    }
}


