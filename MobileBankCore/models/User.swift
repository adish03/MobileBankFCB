//
//  User.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//


import ObjectMapper
// модель User
open class User: Mappable {
    open var
    access_token:String?,
    token_type: String?,
    expires_in: Int?,
    refresh_token: String?,
    username:String?,
    userdisplayname:String?,
    phone: String?,
    loginID:String?,
    pwd_expiry_date:String?,
    pwd_expiry_notify:String?,
    change_pwd_after_login:String?,
    additional_auth_types: String?,
    issued:String?,
    expires:String?
   
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        access_token <- map["access_token"]
        token_type <- map["token_type"]
        expires_in <- map["expires_in"]
        refresh_token <- map["refresh_token"]
        username <- map["username"]
        userdisplayname <- map["user_display_name"]
        phone <- map["user_phone"]
        loginID <- map["login_id"]
        pwd_expiry_date <- map["pwd_expiry_date"]
        pwd_expiry_notify <- map["pwd_expiry_notify"]
        change_pwd_after_login <- map["change_pwd_after_login"]
        additional_auth_types <- map["additional_auth_types"]
        issued <- map[".issued", nested: false]
        expires <- map[".expires", nested: false]
    }
    
}
// модель Pin
open class Pin: Mappable {
    open var
    applicationID: String?,
    pin: String?,
    newPin: String?,
    oldPin: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        applicationID <- map["ApplicationID"]
        pin <- map["Pin"]
        newPin <- map["NewPin"]
        oldPin <- map["OldPin"]
    }
}
// модель PinValidate
open class PinValidate: Mappable {
    open var
    result: String?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}
// модель PinExist
open class PinExist: Mappable {
    open var
    isExist: Bool?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        isExist <- map["isExist"]
    }
}

open class CustomerData: Mappable {
    open var
        customerID: Int?,
        customerName: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        customerID <- map["CustomerID"]
        customerName <- map["CustomerFullName"]
    }
}
