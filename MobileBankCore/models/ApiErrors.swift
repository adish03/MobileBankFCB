//
//  ApiErrors.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import ObjectMapper
// модель для обработки ошибок
open class ApiErrors: Mappable {
    open var statusCode:Int?,
    errors:[ApiError]?,
    exceptionMessage:String?,
    message: String?

    
    var stringDescription:String?{
        let strings = (errors ?? []).map{$0.error_description ?? ""}
        return strings.joined(separator: "\n")
    }
    
    //Mappable
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        errors <- map["errors"]
        exceptionMessage <- map["ExceptionMessage"]
        message <- map["Message"]
    }
    
    var hasAuthErrors:Bool{
        if let statusCode = statusCode{
            return statusCode == 400
        } else {
            return errors?.filter{$0.error == "invalid_grant" && $0.error_description == "Unauthorized"}.first != nil
        }
    }
}
// модель для обработки ошибок
open class ApiError: Mappable {
    open var
    statusCode:Int?,
    error:String?,
    error_description:String?
    //Mappable
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        error <- map["error"]
        error_description <- map["error_description"]
    }
}
// модель для обработки ошибок
open class PinError: Mappable {

    open var
    statusCode:Int?,
    exceptionMessage:String?,
    message: String?

    required public init?(map: Map) {}
    public func mapping(map: Map) {
        exceptionMessage <- map["ExceptionMessage"]
        message <- map["Message"]
    }
    
    public var hasPinError:Bool{
        if let statusCode = statusCode{
            return statusCode == 500
        } else {
            return (exceptionMessage != nil)
        }
    }
}
// модель для обработки ошибок со статусом
open class MessageStatus: Mappable {
    
    open var
    exceptionMessage:String?,
    message: String?

    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        exceptionMessage <- map["ExceptionMessage"]
        message <- map["Message"]
    }
}


