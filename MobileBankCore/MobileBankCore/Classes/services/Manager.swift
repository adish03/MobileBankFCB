//
//  Manager.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/5/19.
//

import Foundation
import RxSwift
import UIKit
import Alamofire
import ObjectMapper

open class Manager: BaseService{
    //singleton
    public static func setup(url:String,disableEvaluationUrl:String){
        shared = Manager(url: url,disableEvaluationUrl: disableEvaluationUrl)
    }
    public static var shared:Manager!
    
    //init
    init(url:String,disableEvaluationUrl:String) {
        serverUrl = url
        super.init(disableEvaluationUrl: disableEvaluationUrl)
    }
    private let serverUrl: String
    
    //     тип header
    private var jsonHeaders: HTTPHeaders{
        return ["Content-Type":"application/json"]
    }
    //     тип header
    private var authHeaders: HTTPHeaders{
        return ["Content-Type":"application/x-www-form-urlencoded"]
    }
   //     тип header
    private var authHeadersSMS: HTTPHeaders{
        var tokenAuth = ""
        if let token = SessionManager.shared.token{
            tokenAuth = token
        }
        return ["Content-Type":"application/x-www-form-urlencoded",
                "Authorization": "bearer \(tokenAuth)"]
    }
    //     тип header
    private var authTokenHeaders: [String:String]{
        var tokenAuth = ""
        if let token = SessionManager.shared.token{
            tokenAuth = token
        }
        return ["Authorization": "bearer \(tokenAuth)"]

    }
    //     тип header
    private var authJsonTokenHeaders: HTTPHeaders{
        var tokenAuth = ""
        if let token = SessionManager.shared.token{
            tokenAuth = token
        }
        return ["Content-Type":"application/json",
                "Authorization": "bearer \(tokenAuth)"]
    }
    
//   получение юзера
    public func getUserWithoutCertificate (grant_type:String, username:String, password:String, clientId: String, clientSecret: String  )->(Observable<User>){

        let parameters = ["grant_type":grant_type, "username":username, "password":password, "client_id": clientId, "client_secret": clientSecret, "platform": "ios" ]
        let URL = "\(serverUrl)/token"
        
        let request:Observable<User> = createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authHeaders,
            method: .post
        )
        return request
    }
  //  grant_type: refresh_token, refresh_token:
//    обновление токена
    public func getUpdateTokenWithoutCertificate(grant_type: String, refresh_token:String)->(Observable<User>){
        
        let parameters = ["grant_type":grant_type, "refresh_token":refresh_token, "platform": "ios"]
        let URL = "\(serverUrl)/token"
        
        let request:Observable<User> = createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authHeaders,
            method: .post
        )
        return request
    }
//    обновление токена
    public func getUpdateTokenWithAdditionalAuth(grant_type: String, refresh_token:String, command: String, sms_code: String?, pin: String?)->(Observable<User>){
        
        var parameters = ["grant_type":grant_type, "refresh_token":refresh_token, "command": command]
        
        if let pin = pin{
            parameters["pin"] =  pin
        }
        if let sms_code = sms_code{
            parameters["sms_code"] =  sms_code
        }
        
        let URL = "\(serverUrl)/token"
        
        let request:Observable<User> = createRequestWithoutCertificate(
            url: URL,
            parameters: parameters as [String : Any],
            headers:authHeaders,
            method: .post
        )
        return request
    }
    //    смена пароля
    public func postChangePassword(grant_type: String, refresh_token:String, command: String, old_password: String, new_password: String) -> (Observable<User>){
        
        let parameters = ["grant_type":grant_type, "refresh_token":refresh_token, "command": command, "old_password":old_password, "new_password":new_password]
        
        let URL = "\(serverUrl)/token"
        
        let request:Observable<User> = createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authHeaders,
            method: .post
        )
        return request
    }
    
    //    получение MobileSettings
    public func getMobileSettings ()->(Observable<MobileSettings>){
        
        let URL = "\(serverUrl)/api/MobileSettings"
        
        let request:Observable<MobileSettings> =  createRequestWithoutCertificate(
            url: URL,
            method: .get
        )
        return request
    }
    
    // MARK: - Avatar
     //    get Avatar
    public func getUserAvatar(loginId: String) -> (Observable<String>) {
        
        let URL = "\(serverUrl)/api/DownloadAvatarFile"
        let params: Parameters = ["loginID" : loginId]
        let request:Observable<String> =  createRequestWithoutCertificate(
            url: URL,
            parameters: params,
            headers: authTokenHeaders,
            method: .get
        )
        
        return request
    }
    
    //MARK - Currencies
     //    получение курса валют
    public func getCurrenciesRatesWithoutCertificate  ()->(Observable<[CurrencyRate]>){
        
        let URL = "\(serverUrl)/api/Currencies/Rates"
        
        let request:Observable<[CurrencyRate]> =  createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
        return request
    }
    
    //    получение Currencies
    public func getCurrenciesWithoutCertificate  ()->(Observable<[Currency]>){
        
        let URL = "\(serverUrl)/api/Currencies"
        
        let request:Observable<[Currency]> =  createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
        return request
    }
     //    получение CurrencyNational
    public func getCurrenciesNationalWithoutCertificate  ()->(Observable<CurrencyNational>){
        
        let URL = "\(serverUrl)/api/Currencies/GetNationalCurrency"
        
        let request:Observable<CurrencyNational> =  createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
        return request
    }
    
    
    
    
   //MARK - PIN request
    // Создание запроса для создание пин
    public func postPinCreate(ApplicationID: String, Pin: String)->(Observable<Pin>){
        
        let parameters = ["ApplicationID":ApplicationID, "Pin":Pin]
        let URL = "\(serverUrl)/api/Pin/Create"
        
        let request:Observable<Pin> = createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .post
        )
        return request
    }
    // Создание запроса для проверку пин
    public func postPinValidate(ApplicationID: String, Pin: String)->(Observable<PinValidate>){
        
        let parameters = ["ApplicationID":ApplicationID, "Pin":Pin]
        let URL = "\(serverUrl)/api/Pin/Validate"
        
        let request:Observable<PinValidate> = createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .post
        )
        return request
    }
    // Создание запроса для смену пин
    public func postPinChange(ApplicationID: String, NewPin: String, OldPin: String)->(Observable<Data>){
        
        let parameters = ["ApplicationID":ApplicationID, "NewPin": NewPin, "OldPin": OldPin ]
        let URL = "\(serverUrl)/api/Pin/Change"
        
        let request:Observable<Data> = createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .post
        )
        return request
    }
    // Создание запроса на проверку пин
    public func getIsPinExist(ApplicationID: String)->(Observable<String>){
        
        let parameters = ["applicationID":ApplicationID]
        let URL = "\(serverUrl)/api/Pin/Exist"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authJsonTokenHeaders,
            method: .get
        )
    }
    // Создание запроса для удаления пин
    public func postPinRemove(ApplicationID: String)->(Observable<Pin>){
        
        let URL = "\(serverUrl)/api/Pin/Remove?applicationID=\(ApplicationID)"
        
        let request:Observable<Pin> = createRequestWithoutCertificate(
            url: URL,
            headers:authJsonTokenHeaders,
            method: .post
        )
        return request
    }
    
    //MARK - AccountRequest
    // Создание запроса для получение счетов
    public func getAccounts(currencyID: Int)->(Observable<[Accounts]>){
        
        let parameters = ["currencyID": currencyID]
        let URL = "\(serverUrl)/api/Accounts"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func activeDemart(accountNo: String, currencyID: Int) ->(Observable<[Accounts]>){
        let URL = "\(serverUrl)/api/Deposits/ActivateDormantAccount?accountNo=\(accountNo)&currencyID=\(currencyID)"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers: authJsonTokenHeaders,
            method: .post
        )
    }
    
    public func deleteDemart(accountNo: String, currencyID: Int) ->(Observable<[Accounts]>){
        let URL = "\(serverUrl)/api/Deposits/Accounts/DeleteCurrentAccount?accountNo=\(accountNo)&currencyID=\(currencyID)"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers: authJsonTokenHeaders,
            method: .post
        )
    }
    
    // Создание конвертации
    public func postConversion(ConversionOperationModel: ConversionOperationModel) -> (Observable<ConversionSum>){
        let params: ConversionOperationModel = ConversionOperationModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Deposits/Accounts/Conversion"
        
        
        let request:Observable<ConversionSum> = createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
        return request
    }
    
    //MARK: Taxes
    
    //Возвращает информацию по регионам
    public func getRegions() -> (Observable<[TaxRegionModel]>){
        let URL = "\(serverUrl)/api/Utilities/GetRegions"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    //Возвращает информацию по органам ГНС
    public func getTaxOkmotCodes() -> (Observable<[TaxOkmotCodeModel]>){
        let URL = "\(serverUrl)/api/Utilities/GetTaxOkmotCodes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    //Возвращает информацию по регионам
    public func getDistricts() -> (Observable<[TaxDistrictModel]>){
        let URL = "\(serverUrl)/api/Utilities/GetDistricts"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    //Возвращает информацию по регионам
    public func getDepartments() -> (Observable<[TaxDepartmentModel]>){
        let URL = "\(serverUrl)/api/Utilities/GetDepartments"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    //MARK - UtilitiesRequest
    
   // Создание запроса для получение Categories
    public func getCategories()->(Observable<Categories>){
        
        let URL = "\(serverUrl)/api/Utilities/Categories"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса для получение списка услуг
    public func getListUtilities()->(Observable<[UtilityModel]>){
        
        let URL = "\(serverUrl)/api/Utilities/GetList"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса для получение списка услуг
    public func getUtilities()->(Observable<Utilities>){
        
        let URL = "\(serverUrl)/api/Utilities/"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getScenario(contragentID: Int) -> Observable<[ScenarioFieldModel]> {
        let URL = "\(serverUrl)/api/Utilities/GetScenarioFields?contragentID=\(contragentID)"
        
        let request:Observable<[ScenarioFieldModel]> = createRequestWithoutCertificate(
            url: URL,
            headers:authJsonTokenHeaders,
            method: .get
        )
        return request
    }
    
    // Создание запроса для проверки услуги
    public func postCheckUtility(Pay_CheckModel: UtilityPaymentModel) -> (Observable<StringExecuteResult>){
        let params: UtilityPaymentModel = Pay_CheckModel
        let paramsBody = params.toJSON()
        
        print(params)
        print(paramsBody)
        let URL = "\(serverUrl)/api/Utilities/CheckModel"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    // Создание запроса для получение коммиссии
    public func postGetCommission(UtilityPaymentOperationModel: UtilityPaymentOperationModel) -> (Observable<DecimalExecuteResult>){
        let params: UtilityPaymentOperationModel = UtilityPaymentOperationModel
        let paramsBody = params.toJSON()
        
        print(params)
        print(paramsBody)
        let URL = "\(serverUrl)/api/Utilities/GetCommission"
        
            return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    // Создание оплаты услуги
    public func postPayUtility(Pay_CheckModel: UtilityPaymentModel) -> (Observable<String>){
        let params: UtilityPaymentModel = Pay_CheckModel
        let paramsBody = params.toJSON()
        
        print(params)
        print(paramsBody)
        let URL = "\(serverUrl)/api/Utilities/MakePayment"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    public func getOperation(operationID: Int) -> (Observable<UtilityPaymentOperationModel>){
        
        let parameters = ["operationID": operationID]
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса для обновления даты
    public func getIsModifyCategories(date: String)->(Observable<String>){
        
        let parameters = ["date": date]
        let URL = "\(serverUrl)/api/Utilities/IsModifyCategories"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса для проверки данных
    public func getIsModifyUtilities(date: String)->(Observable<String>){
        
        let parameters = ["date": date]
        let URL = "\(serverUrl)/api/Utilities/IsModify"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    //SMS Authorization
    public func postSMS() -> (Observable<String>){
        
        let URL = "\(serverUrl)/api/AuthSettings/ResendSmsCode"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authHeadersSMS,
            method: .post
        )
    }
//     Генерация и отправка нового кода подтверждения
    public func postSendConfirmOperationCode(operationID: Int) -> (Observable<String>){
        
        let URL = "\(serverUrl)/api/Operations/SendConfirmOperationCode?operationID=\(operationID)"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authHeadersSMS,
            method: .post
        )
    }
    
    //Image fetch
    public func getImage(imageName: String) -> (Observable<Data>){
        
        let URL = "\(serverUrl)/api/Image/mobile/\(imageName)/"
        
        return createRequestWithoutCertificate(
            url: URL,
            method: .get
        )
    }
    
    
    // ClearingGross
    
    public func getClearingGrossValidateData()->(Observable<InternetBankingClearingGrossValidateDataModel>){
        
        let URL = "\(serverUrl)/api/ClearingGross/GetValidateSettings"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
     // Создание запроса на
    public func getClearingGrossAcconts()->(Observable<[Accounts]>){
        
        let URL = "\(serverUrl)/api/ClearingGross/GetAccounts"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
     // Создание запроса на получение PaymentCode
    public func getPaymentsCodes()->(Observable<[PaymentCode]>){
        
        let URL = "\(serverUrl)/api/ClearingGross/PaymentCodes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
     // Создание запроса на получение BikCode
    public func getBikCodes()->(Observable<[BikCode]>){
        
        let URL = "\(serverUrl)/api/ClearingGross/BikCodes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
     // Создание запроса на создание ClearingGross
    public func postClearinGrossCreate(ClearingGrossOperationModel: ClearingGrossOperationModel) -> (Observable<BaseCreateOperation>){
        let params: ClearingGrossOperationModel = ClearingGrossOperationModel
        let paramsBody = params.toJSON()
        
        print(params)
        print(paramsBody)
        let URL = "\(serverUrl)/api/ClearingGross/Create"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
     // Создание запроса на смену даты ClearingGross
    public func postNeedChangeDate(clearingGrossNeedChangeDateModel: ClearingGrossNeedChangeDateModel)->(Observable<ClearingGrossNeedChangeDateResultModel>){
        
        let params: ClearingGrossNeedChangeDateModel = clearingGrossNeedChangeDateModel
        let paramsBody = params.toJSON()
        
        print(paramsBody)
        
        let URL = "\(serverUrl)/api/ClearingGross/NeedChangeDate"

        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    // Internal transaction
    // Создание запроса на InternalTransaction
    public func postInternalTransaction(InternalOperationModel: InternalOperationModel) -> (Observable<BaseCreateOperation>){
        let params: InternalOperationModel = InternalOperationModel
        let paramsBody = params.toJSON()
        
        print(params)
        print(paramsBody)
        let URL = "\(serverUrl)/api/Accounts/InternalTransaction"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
     // Создание запроса на InternalOperationCustomerAccounts
    public func postInternalOperationCustomerAccounts(InternalOperationModel: InternalOperationModel) -> (Observable<BaseCreateOperation>){
        let params: InternalOperationModel = InternalOperationModel
        let paramsBody = params.toJSON()
        
        print(params)
        print(paramsBody)
        let URL = "\(serverUrl)/api/Accounts/InternalOperationCustomerAccounts"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
      // Создание запроса на получение счетов
    public func getAccountForInternalTransaction(accountNo: String, currencyID: Int)->(Observable<String>){
        
        let parameters = ["accountNo": accountNo, "currencyID": currencyID] as [String : Any]
        let URL = "\(serverUrl)/api/Accounts/GetAccountForInternalTransaction"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getAccountsByAccountNoOrPhone(account: String)->(Observable<[SimpleAccountModel]>){
        let URL = "\(serverUrl)/api/Accounts/GetAccountsByAccountNoOrPhone?account=\(account)"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
  
     // Создание запроса на счетов
    public func getAccountForConversion()->(Observable<[Accounts]>){
        
        let URL = "\(serverUrl)/api/Deposits/GetAccounts"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getDepositPurposes()->(Observable<[DepositPurposeReferenceItemModel]>){
        let URL = "\(serverUrl)/api/Reference/DepositPurposes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
   // Создание запроса на проверку доступности счета
    public func getCheckAccountAvailable(accountNo: String, currencyID: Int)->(Observable<String>){
        
        let parameters = ["accountNo": accountNo, "currencyID": currencyID] as [String : Any]
        let URL = "\(serverUrl)/api/Accounts/CheckAccountAvailable"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    //Swift transaction
    // Создание запроса  на получение аккаунтов
    public func getAccountSwift()->(Observable<[Accounts]>){
        
        let URL = "\(serverUrl)/api/Swift/Transfers/GetAccounts"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса  на получение  кодов операции
    public func getRefOpers()->(Observable<[ReferenceItemModel]>){
        
        let URL = "\(serverUrl)/api/Swift/Transfers/RefOpers"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса  на получение валютных кодов
    public func getVOCodes()->(Observable<[ReferenceItemModel]>){
        
        let URL = "\(serverUrl)/api/Swift/Transfers/VOCodes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса  на получение типов операции
    public func getExpenseTypes()->(Observable<[expenseTypeModel]>){
        
        let URL = "\(serverUrl)/api/Swift/Transfers/ExpenseTypes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса  на получение стран Swift
    public func getSwiftCountries()->(Observable<[ReferenceItemModel]>){
        
        let URL = "\(serverUrl)/api/Swift/Transfers/SwiftCountries"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getCountries()->(Observable<[CountryReferenceItemModel]>){
        
        let URL = "\(serverUrl)/api/Reference/Countries"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса  на получение ValidateSettings
    public func getValidateSettings()->(Observable<SwiftValidateDataModel>){
        
        let URL = "\(serverUrl)/api/Swift/Transfers/GetValidateSettings"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса  на получение коммиссии
    public func getSwiftCommission(accountNo: String, currencyID: Int, transferSum: Double, expenseType: Int)->(Observable<[SwiftCommissionModel]>){
        
        let parameters = ["accountNo": accountNo,
                          "currencyID": currencyID,
                          "transferSum": transferSum,
                          "expenseType": expenseType] as [String : Any]
        let URL = "\(serverUrl)/api/Swift/Transfers/GetCommission"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание перевода-Swift
    public func postSwiftCreate(SwiftTransferModel: SwiftTransferModel) -> (Observable<SwiftOperation>){
        let params: SwiftTransferModel = SwiftTransferModel
        let paramsBody = params.toJSON()
       
        let URL = "\(serverUrl)/api/Swift/Transfers/Create"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    //MARK: - SwiftBIK BANK
    // Создание запроса  на получение bic кодов
    public func getSwiftBik(bic: String)->(Observable<SwiftBikModel>){
        
        let parameters = ["bic": bic] as [String : Any]
        let URL = "\(serverUrl)/api/Swift/Transfers/GetSwiftBank"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса  на транслитерацию
    public func getTransliteration(detailsOfPayment: String, fullNameReceiver: String, lang: String) ->(Observable<TransliterationSwiftTransferModel>){
        
        let parameters = ["DetailsOfPayment": detailsOfPayment, "FullNameReceiver": fullNameReceiver, "Lang": lang] as [String : Any]
        
        let URL = "\(serverUrl)/api/Swift/Transfers/Transliteration"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // запрос на получение баннеров
    public func getBanners(location: Int?) -> (Observable<[Banner]>){
        
        let parameters = ["location": 0] as [String : Any]
        
        let URL = "\(serverUrl)/api/Banner"
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }    
    // Создание запроса  на получение шаблона
    public func getSwiftTemplate(template: String, type: Int) ->(Observable<SwiftTransferModel>){
        
        let parameters = ["template": template, "type": type] as [String : Any]
        
        let URL = "\(serverUrl)/api/Swift/Transfers/GetOperation"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса  на Получение данных для создания нового перевода на основании существующего перевода
    public func getSwiftExistTemplate(operationID: Int, template: String) ->(Observable<SwiftTransferModel>){
        
        let parameters = ["operationID": operationID, "template": template] as [String : Any]
        
        let URL = "\(serverUrl)/api/Swift/Transfers/GetOperation"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса на получение списка шаблонов
    public func getTemplateOperations(name: String?, operationType: Int?, page: Int, pageSize: Int, sortField: String?, sortType: Int) ->(Observable<Template>){
        
        let parameters = ["name": name ?? "", "operationType": operationType , "page": page, "pageSize": pageSize, "sortField": sortField ?? "", "sortType": sortType] as [String : Any]
        let URL = "\(serverUrl)/api/TemplateOperations/Get"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authJsonTokenHeaders,
            method: .get
        )
    }
    // запрос на получение истории операций
    public func getOperations(operationType: String?, operationState: String?, dateFrom: String?, dateTo: String?, filter: String?, page: Int,pageSize: Int, sortField: String,sortType: Int) -> (Observable<ListContractModelExecuteResult>){
        
        let parameters = ["operationType": operationType ?? "null",
                          "operationState": operationState ?? 0,
                          "dateFrom": dateFrom ?? "null",
                          "dateTo": dateTo ?? "null",
                          "filter": filter ?? "",
                          "fileID": "",
                          "page": page,
                          "pageSize": pageSize,
                          "sortField": sortField,
                          "sortType": sortType] as [String : Any]
        
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authJsonTokenHeaders,
            method: .get
        )
    }
    
    // запрос на получение истории clearingGross
    public func getClearingGrossTransfers(paymentType: String?, paymentDirection: String?, startDate: String?, endDate: String?, status: String?, text: String?,PageSize: Int, Page: Int, TotalItems: Int?) -> (Observable<ClearingGrossTransfersModel>){
        
        let parameters = ["paymentType": paymentType ?? "null",
                          "paymentDirection": paymentDirection ?? 0,
                          "startDate": startDate ?? "null",
                          "endDate": endDate ?? "null",
                          "status": status ?? "",
                          "text": text ?? "",
                          "PageSize": PageSize,
                          "Page": Page,
                          "TotalItems": TotalItems] as [String : Any]
        
        let URL = "\(serverUrl)/api/ClearingGross/Transfers"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authJsonTokenHeaders,
            method: .get
        )
    }
    
    // запрос на получение истории SwiftTransfers
    public func getSwiftTransfers(startDate: String?, endDate: String?, status: String?, text: String?, PageSize: Int, Page: Int,TotalItems: Int?) -> (Observable<SwiftTransfersModel>){
        
        let parameters = ["startDate": startDate ?? "null",
                          "endDate": endDate ?? "null",
                          "status": status ?? "",
                          "text": text,
                          "PageSize": PageSize,
                          "Page": Page,
                          "TotalItems": TotalItems] as [String : Any]
        
        let URL = "\(serverUrl)/api/Swift/Transfers/Get"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authJsonTokenHeaders,
            method: .get
        )
    }
    
    // удаление шаблона
    public func deleteTemplate(operationID: String) ->(Observable<String>){
        
        let body = operationID
        
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificateBodyString(
            url: URL,
            body: body,
            headers: authJsonTokenHeaders,
            method: .delete)
        
    }
    // история операций
    //  запрос на получение статуса "Необходим ли код подтверждения?"
    public func postIsRequireConfirmOperationCode(operationID: Int)->(Observable<String>){
        
        let URL = "\(serverUrl)/api/Operations/IsRequireConfirmOperationCode?operationID=\(operationID)"
        
        let request:Observable<String> = createRequestWithoutCertificate(
            url: URL,
            headers:authJsonTokenHeaders,
            method: .post
        )
        return request
    }
    // запрос на получение подтверждения операции
    public func postConfirmUtility(OperationModel: OperationModel) -> (Observable<MessageStatus>){
        let params: OperationModel = OperationModel
        
        let paramsBody = params.toJSON()
        
        print(params)
        print(paramsBody)
        let URL = "\(serverUrl)/api/Operations/Confirm"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    // Создание запроса  на получение списка статусов операций
    public func getOperationStates()->(Observable<[expenseTypeModel]>){
        
        let URL = "\(serverUrl)/api/Reference/OperationStates"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса  на получение списка типов операций
    public func getOperationTypes()->(Observable<[expenseTypeModel]>){
        
        let URL = "\(serverUrl)/api/Reference/OperationTypes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // запрос на получение excel file операции
    public func getExportStatement( accountNo: String,
                                    currencyID: Int,
                                    startDate: String,
                                    endDate: String,
                                    toExcel: Bool) -> (Observable<Data>){
       
        let parameters = [ "accountNo": accountNo,
                           "currencyID": currencyID,
                           "startDate": startDate,
                           "endDate": endDate,
                           "toExcel": toExcel] as [String : Any]
        
        
        let URL = "\(serverUrl)/api/Accounts/ExportStatement"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getExportMiniStatement( accountNo: String,
                                    currencyID: Int) -> (Observable<ReportsExecuteResult>){
       
        let parameters = [ "accountNo": accountNo,
                           "currencyID": currencyID] as [String : Any]
        
        
        let URL = "\(serverUrl)/api/Cards/GetMiniStatement"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }

    // Создание запроса на Получение информации о клиринг/гросс переводе
    public func getInfoCliringGross(operationID: Int)->(Observable<ClearingGrossOperationModel>){
        
        let parameters = ["operationID": operationID]
        
        let URL = "\(serverUrl)/api/ClearingGross/Get"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса на Получение информации об операции оплаты услуг
    public func getInfoOperation(operationID: Int)->(Observable<ModelOperation>){
        
        let parameters = ["operationID": operationID]
        
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса на Получение информации о переводах
    public func getInfoOperationTransactions(operationID: Int)->(Observable<InternalOperationModel>){
        
        let parameters = ["operationID": operationID]
        
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса на Получение информации об операции SWIFT
    public func getInfoOperationSwift(operationID: Int)->(Observable<SwiftTransferModel>){
        
        let parameters = ["operationID": operationID]
        
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
   
    // Создание запроса на Получение информации об операции credit
    public func getInfoOperationCredit(operationID: Int)->(Observable<LoanPaymentModel>){
        
        let parameters = ["operationID": operationID]
        
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса на Получение информации об операции ClearingGross
    public func getInfoOperationCliringGross(operationID: Int)->(Observable<ClearingGrossOperationModel>){
        
        let parameters = ["operationID": operationID]
        
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса на Получение информации о шаблоне
    public func getInfoOperationFile(operationID: Int, code: Int, operationTypeID: Int)->(Observable<DocumentTemplate>){
        
        let parameters = [ "OperationID": operationID ,"Code": code ,"OperationTypeID": operationTypeID ]
        let URL = "\(serverUrl)/api/Operations/GetDocuments"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса на Получение информации о конвертации
    public func getInfoOperationConversion(operationID: Int)->(Observable<ConversionOperationModel>){
        
        let parameters = ["operationID": operationID]
        
        let URL = "\(serverUrl)/api/Operations"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса на Получение деталей счетов
    public func getInfoDetails(accountNo: String,
                               currencyID: Int,
                               startDate: String,
                               endDate: String,
                               page: Int,
                               pageSize: Int,
                               totalItems: Int)->(Observable<DetailModel>){
        
        let parameters = ["accountNo": accountNo,
                          "currencyID": currencyID,
                          "startDate": startDate,
                          "endDate": endDate,
                          "Page": page,
                          "PageSize": pageSize,
                          "TotalItems": totalItems] as [String : Any]
        
        let URL = "\(serverUrl)/api/Accounts/Statement"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса -Возвращает банковский день-
    public func getBankDate()->(Observable<String>){
       
        let URL = "\(serverUrl)/api/BankDate"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }



    // Создание запроса для аккаунтов
    public func getDetailsAccounts()->(Observable<[DetailAccountsModel]>){
        
        let URL = "\(serverUrl)/api/Deposits/Accounts"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса для депозитов
    public func getDetailsDeposits()->(Observable<[DetailDepositsModel]>){
        
        let URL = "\(serverUrl)/api/Deposits"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса для закрытия депозита
    public func postCloseDeposit(CloseDepositModel: CloseDepositModel)->(Observable<ReportsExecuteResult>){
        let params: CloseDepositModel = CloseDepositModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Deposits/CloseDeposit"
        
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    // Создание запроса для открытия депозита
    public func postOpenDeposit(OpenDepositModel: OpenDepositModel)->(Observable<DepositPublicOfferExecuteResult>){
        let params: OpenDepositModel = OpenDepositModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Deposits/OpenDeposit"
        
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    public func buildDepositSchedule(schedule: ScheduleParameters)->(Observable<DepositScheduleResponse>){
        let params: ScheduleParameters = schedule
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Deposits/Products/BuildSchedule"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    
    // Создание запроса всех продуктов депозитов
    public func getAllProductsDeposit()->(Observable<Products>){
      
        let URL = "\(serverUrl)/api/Deposits/Products/AvailableProducts"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getAllProductsDepositModel()->(Observable<[Currency]>){
      
        let URL = "\(serverUrl)/api/Deposits/Products/AvailableCurrencies"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func protoTest()->(Observable<Products>){
      
        let URL = "\(serverUrl)/api/Deposits/Products/AvailableCurrentAccountProducts"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func openCurrentAccounts(model: OpenCurrentAccountModel?) ->(Observable<Products>){
      
        let URL = "\(serverUrl)/api/Deposits/OpenCurrentAccount"
        let json = model?.toJSON()
        
        print(model)
        print(json)

        return createRequestWithoutCertificate(
            url: URL,
            parameters: json,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default)
    }
    
    
    // Создание запроса для карт
    public func getDetailsCards()->(Observable<[DetailCardsModel]>){
        
        let URL = "\(serverUrl)/api/Cards"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
  
    // Создание запроса на Получение деталей карты
    public func getInfoCard(accountNo: String, currencyID: Int)->(Observable<DetailCardModel>){
        
        let parameters = ["accountNo": accountNo,
                          "currencyID": currencyID] as [String : Any]
        
        let URL = "\(serverUrl)/api/Cards/GetCardBalanceInfo"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Занесение карты в мягкий стоп-лист
    public func postAddToStopList(AccountModel: AccountModel) -> (Observable<String>){
        let params: AccountModel = AccountModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Cards/AddToStopList"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    // Сброс счетчика введенных ПИН-кодов
    public func postRebasePin(AccountModel: AccountModel) -> (Observable<String>){
        let params: AccountModel = AccountModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Cards/RebasePin"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    // Проверка используется ли онлайн-интеграция карт
    public func getCheckIsUsedOnlineCard()->(Observable<String>){
        
        let URL = "\(serverUrl)/api/Cards/CheckIsUsedOnlineCard"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса для Список кредитов выданных пользователю
    public func getDetailsCredits()->(Observable<DetailCreditsModel>){
        
        let URL = "\(serverUrl)/api/Loans/ApprovedLoans"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса для Получение детальной информации кредита
    public func getLoansDetails(creditID: Int)->(Observable<LoanPaymentModel>){
        
        let parameters = ["creditID": creditID] as [String : Any]
        
        let URL = "\(serverUrl)/api/Loans/Details"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса для Получение детальной информации кредита в процессе
    public func getLoansInProcessDetails(creditID: Int)->(Observable<[LoanInProcessModel]>){
        
        let parameters = ["creditID": creditID] as [String : Any]
        
        let URL = "\(serverUrl)/api/Loans/ViewRequest"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса Возвращает график кредита
//    TODO response model???
    public func getLoansSchedule(loanId: Int, trancheId: Int)->(Observable<LoanGraficModel>){
        
        let parameters = ["loanId": loanId, "trancheId":trancheId] as [String : Any]
        
        let URL = "\(serverUrl)/api/Loans/Schedule"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание заявления на кредит
    public func postCreateRequest(CreateRequestLoanModel: CreateRequestLoanModel) -> (Observable<LoanRequestResultModel>){
        let params: CreateRequestLoanModel = CreateRequestLoanModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Loans/CreateRequest"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    // Отказ клиента от заявки на получение кредита
    public func postRejectRequest(RejectCreditRequestModel: RejectCreditRequestModel) -> (Observable<String>){
        let params: RejectCreditRequestModel = RejectCreditRequestModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Loans/RejectRequest"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    // Просмотр заявки
    public func getViewRequest(creditID: Int)->(Observable<String>){
        
        let parameters = ["creditID": creditID] as [String : Any]
        
        let URL = "\(serverUrl)/api/Loans/ViewRequest"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
//LoanPaymentOperation  Контроллер для работы с операциями погашения кредитов
    // Получение операции
    public func getLoanGetOperation(operationID: Int, creditID: Int, paymentType: Int)->(Observable<LoanPaymentModel>){
        
        let parameters = ["operationID": operationID, "creditID": creditID , "paymentType": paymentType] as [String : Any]
        
        let URL = "\(serverUrl)/api/Loans/LoanPaymentOperation/GetOperation"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Получение расчета
    
    public func postCalculateLoanPayment(CalculateLoanPaymentModel: CalculateLoanPaymentModel) -> (Observable<CalculateLoanPaymentResult>){
        let params: CalculateLoanPaymentModel = CalculateLoanPaymentModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Loans/LoanPaymentOperation/CalculateLoanPayment"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
     // Получение расчета плановый
    
    public func postCalculatePlanLoanPayment(CalculatePlanLoanPayment: CalculatePlanLoanPayment) -> (Observable<CalculateLoanPaymentResult>){
        let params: CalculatePlanLoanPayment = CalculatePlanLoanPayment
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Loans/LoanPaymentOperation/CalculatePlanLoanPayment"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    // Получение Обработка операции
    public func postProcess(LoanPaymentModel: LoanPaymentModel) -> (Observable<String>){
        let params: LoanPaymentModel = LoanPaymentModel
        let paramsBody = params.toJSON()
        
        let URL = "\(serverUrl)/api/Loans/LoanPaymentOperation/Process"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: paramsBody,
            headers:authJsonTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
  
    
    // Создание запроса для "Кредитные продукты доступные для открытия"
    public func getLoansAvailableProducts()->(Observable<[LoanProductModel]>){
        
        let URL = "\(serverUrl)/api/Loans/Products/AvailableProducts"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса для "Получение списка Вид обеспечения кредита"
    public func getLoansMortrageTypes()->(Observable<[ReferenceCreditItemModel]>){
        
        let URL = "\(serverUrl)/api/Reference/MortrageTypes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // Создание запроса для "Кредитные продукты доступные для открытия"
    public func getLoansIncomeApproveTypes()->(Observable<[ReferenceCreditItemModel]>){
        
        let URL = "\(serverUrl)/api/Reference/IncomeApproveTypes"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса для языков
    public func getLocalization(culture: String?)->(Observable<Localize>){
        
        let parameters = ["culture": culture ?? ""] as [String : Any]
        let URL = "\(serverUrl)/api/Localization/culture"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса для языков
    public func getLocalizationString(culture: String)->(Observable<String>){
        
        let parameters = ["culture": culture] as [String : Any]
        let URL = "\(serverUrl)/api/Localization/culture"
        
        let o:Observable<String> =  createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            method: .get
        )
        return o.map{ jsonText in
            let data = jsonText.data(using: String.Encoding.utf8)!
            let dictonary = try! JSONSerialization.jsonObject(with: data, options: []) as? [String:String]
            var result = ""
            for (key, value) in dictonary! {
                result += "\"\(key)\" = \"\(value)\";\n"
            }
            return result
        }
    }
    
    // Создание запроса на Получение информации об операции Requisites documents
    public func getAccountRequisites(accountNo: String, currencyID: Int) -> (Observable<ReportsExecuteResult>){
        
        let parameters = ["accountNo": accountNo, "currencyID": currencyID] as [String : Any]
        
        let URL = "\(serverUrl)/api/Accounts/AccountRequisites"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса для получение офисов
    public func getOffices()->(Observable<OfficesAndDevices>){
        
        let URL = "\(serverUrl)/api/Offices"
        
        return createRequestWithoutCertificate(
            url: URL,
            method: .get )
    }
    // Создание запроса для получение терминалов
    public func getDevices()->(Observable<OfficesAndDevices>){

        let URL = "\(serverUrl)/api/Devices"

        return createRequestWithoutCertificate(
            url: URL,
            method: .get )
    }
    
    // Создание запроса для получение офисов
    public func getAllowedOperations()->(Observable<[String]>){
        
        let URL = "\(serverUrl)/api/Security/AllowedOperations"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get )
    }
    
    // Создание запроса на Получение информации об операции clearingGross by paymentID
    public func getInfoOperationClearingGrossbyPaymentID(paymentID: Int)->(Observable<ClearingGrossPaymenModel>){
        
        let parameters = ["paymentID": paymentID]
        
        let URL = "\(serverUrl)/api/ClearingGross/GetClearingGrossPayment"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса на Получение информации об операции SWIFT documnets
    public func getDocumentsSwift(TransferID: Int)->(Observable<DocumentTemplate>){
        
        let parameters = ["id": TransferID]
        
        let URL = "\(serverUrl)/api/Swift/Transfers/GetDocuments"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса на Получение информации об операции ClearingGross documnets
    public func getDocumentsClearingGross(paymentID: Int)->(Observable<DocumentTemplate>){
        
        let parameters = ["paymentID": paymentID]
        
        let URL = "\(serverUrl)/api/ClearingGross/Documents"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    // get's money system xfinca
    public func getMoneySystem()->(Observable<[MoneySystem]>){
        
        let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/GetMoneySystems"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get )
    }
    
    // find's money transfer if exist
    public func findMoneyTransfer(
        paymentCode: String,
        moneySystemID: Int,
        transferSum: Double,
        transferCurrencyID: Int
    )->(Observable<FindTransferResponseModel>){
        
        let parameters = [
            "paymentCode"        : paymentCode,
            "moneySystemID"      : moneySystemID,
            "transferSum"        : transferSum,
            "transferCurrencyID" : transferCurrencyID,
        
        ] as [String : Any]
        
        let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/FindMoneyTransfer"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getConversionSum(
        accountNo: String,
        accountCurrencyID: Int,
        directionTypeID: Int,
        moneySystemID: Int,
        transferSum: Double,
        transferCurrencyID: Int
    )->(Observable<ConversionSumResult>){
        let parameters = [
            "accountNo": accountNo,
            "accountCurrencyID": accountCurrencyID,
            "directionTypeID": directionTypeID,
            "moneySystemID": moneySystemID,
            "transferSum": transferSum,
            "transferCurrencyID": transferCurrencyID,
        ] as [String : Any]
        
        let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/GetConversionSum"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getXFincaCommission(customerID: Int, transferSum: Double, currencyID: Int, countryID: Int) -> (Observable<XFincaCommission>){
        let parameters = [
            "customerID": customerID,
            "transferSum": transferSum,
            "currencyID": currencyID,
            "countryID": countryID
        ] as [String : Any]
        
        let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/GetXFincaGetCommissionZk"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    // Создание запроса на создание Быстрого перевода X-Finca
    public func postMoneyTransferCreate(createMoneyTransfer: CreateMoneyTransfer) -> (Observable<BaseCreateOperation>){
       let params: CreateMoneyTransfer = createMoneyTransfer
       let paramsBody = params.toJSON()
       
       print(params)
       print(paramsBody)
       let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/Create"
       
       return createRequestWithoutCertificate(
           url: URL,
           parameters: paramsBody,
           headers:authJsonTokenHeaders,
           method: .post,
           encoding: JSONEncoding.default
       )
    }
    
    // Создание запроса на создание Быстрого перевода X-Finca
    public func postMoneyTransferConfirm(operationModel: OperationModel) -> (Observable<String>){
       let params: OperationModel = operationModel
        let paramsBody = params.toJSON()
       
       print(params)
       let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/ConfirmOperation"
       
       return createRequestWithoutCertificate(
           url: URL,
           parameters: paramsBody,
           headers:authJsonTokenHeaders,
           method: .post,
           encoding: JSONEncoding.default
       )
    }
    
    public func getStatus() -> (Observable<GetStatus>) {
        
        let URL = "\(serverUrl)/api/GetCustomerStatus"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: nil,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getCustomerData() -> (Observable<CustomerData>) {
        let URL = "\(serverUrl)/api/GetCustomerData"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: nil,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getXFincaCountries() -> (Observable<[XFincaCountryDictionary]>) {
        let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/GetXFincaCountriesZk"
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: nil,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getXFincaCities(countryID: Int) -> (Observable<[XFincaCityDictionary]>) {
        let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/GetXFincaCitiesZk"
        let parameters = [
            "countryID": countryID ] as [String : Any]
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getPhoneMask(countryID: Int) -> (Observable<XFincaPhoneMask>){
        let URL = "\(serverUrl)/api/MoneyTransfer/MoneyTransfers/GetDataForPhoneNumberMask"
        let parameters = [
            "countryID": countryID ] as [String : Any]
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getPaymentModel(providerID: Int) -> (Observable<StringExecuteResult>){
        let URL = "\(serverUrl)/api/Utilities/GetPaymentModel"
        let parameters = [
            "providerID": providerID ] as [String : Any]
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getTermsDocument() -> (Observable<TermsTypeData>) {
        let URL = "\(serverUrl)/api/Reference/TermsDocuments"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getDowloadTermsDocument(documentID: Int) -> (Observable<DowloadTerms>) {
        let URL = "\(serverUrl)/api/Reference/DownloadTermsDocument?documentID=\(documentID)"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func getLimits() -> (Observable<[LimitsModel]>) {
        let URL = "\(serverUrl)/api/Reference/GetLimitsIB"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers:authTokenHeaders,
            method: .get
        )
    }
    
    public func uploadImage(Content: String, FileName: String) -> (Observable<Data>) {
        let URL = "\(serverUrl)/api/SaveAvatarFile"
        
        let parameters = ["Content" : Content, "FileName" : FileName] as [String : Any]
        
        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers: authTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    public func getSenderFullName() -> (Observable<Data>) {
        let URL = "\(serverUrl)/api/ClearingGross/GetSenderFullName"
        
        return createRequestWithoutCertificate(
            url: URL,
            headers: authTokenHeaders,
            method: .get
        )
    }
    
    public func changeLogin(NewUserLogin: String, Password: String) -> (Observable<User>) {
        let URL = "\(serverUrl)/api/AuthSettings/ChangeLogin"
        let parameters = ["NewUserLogin" : NewUserLogin, "Password" : Password] as [String : Any]

        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers: authTokenHeaders,
            method: .post
        )
    }
    
    public func changePin(ApplicationID: String, NewPin: String, OldPin: String) -> (Observable<Data>) {
        let URL = "\(serverUrl)/api/Pin/Change"
        let parameters = ["ApplicationID" : ApplicationID, "NewPin" : NewPin, "OldPin" : OldPin] as [String : Any]

        return createRequestWithoutCertificate(
            url: URL,
            parameters: parameters,
            headers: authTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
    }
    
    public func checkURL(url:String,
                         parameters: [String: Any]? = nil,
                         method: HTTPMethod = .get,
                         encoding: ParameterEncoding = URLEncoding.default) {
        
        let observer: Observable<Data> = createRequestWithoutCertificate(
            url: url,
            parameters: parameters,
            headers: authTokenHeaders,
            method: .post,
            encoding: JSONEncoding.default
        )
        
    }
//    data class ChangePinCodeModel(
//        val ApplicationID: String,
//        val NewPin: String,
//        val OldPin: String
//    )
//    public func getTariffs() -> (Observable<Void>) {
//        let URL = "\(serverUrl)/api/Reference/GetTariffsIB"
//
//        return createRequestWithoutCertificate(
//            url: URL,
//            headers:authTokenHeaders,
//            method: .get
//        )
//    }
}

open class OpenCurrentAccountModel: Mappable {
    required public init?(IsNew: Bool?, OfficeID: Int?, ProductID: Int?, AccountNo: String?, ProductType: Int?, Currencies: String?, OrganizationType: Int?) {
        self.IsNew = IsNew
        self.OfficeID = OfficeID
        self.ProductID = ProductID
        self.AccountNo = AccountNo
        self.ProductType = ProductType
        self.Currencies = Currencies
        self.OrganizationType = OrganizationType
    }
    
    required public init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        IsNew <- map["IsNew"]
        OfficeID <- map["OfficeID"]
        ProductID <- map["ProductID"]
        AccountNo <- map["AccountNo"]
        ProductType <- map["ProductType"]
        Currencies <- map["Currencies"]
        OrganizationType <- map["OrganizationType"]
    }
    
    var IsNew: Bool?
    var OfficeID: Int?
    var ProductID: Int?
    var AccountNo: String?
    var ProductType: Int?
    var Currencies: String?
    var OrganizationType: Int?
}
