//
//  SessionManager.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import Foundation
import RxSwift

open class SessionManager{
    public static let shared = SessionManager()
    var managerApi:Manager{
        get{ return Manager.shared}
    }
   
    
    public var users:[User]?{
        get{ return UserDefaultsHelper.users }
        set{ UserDefaultsHelper.users = newValue }
    }
    public var user:User?{
        get{ return UserDefaultsHelper.user }
        set{ UserDefaultsHelper.user = newValue }
    }

    public var indexPathCurrent: Int?{
        get{ return UserDefaultsHelper.currentIndexPath }
        set{ UserDefaultsHelper.currentIndexPath = newValue }
    }
    
//    // keychain=======================
//
//    public var user:User?{
//        get{ return KeyChain.user }
//        set{ KeyChain.user = newValue }
//    }
//
//    public var users: [User]?{
//        get{ return KeyChain.users }
//        set{ KeyChain.users = newValue }
//    }
    // keychain=======================
    
    
    
    public init(){}
    
    public var isLoggedIn:Bool{ return user?.access_token != nil }
    
    public var isTimeUpToken:Bool{ return user?.expires != nil }
  
    public var currentUserIndexParh: Int?{
        return indexPathCurrent
    }
    
   public var token:String?{
        return user?.access_token
    }
    public var refreshToken:String?{
        return user?.refresh_token
    }

    
    public func logOutUser(loginID: String){
        if var users = self.users{
            if users.count <= 1{
                user?.access_token = ""
                SessionManager.shared.user = nil
                UserDefaultsHelper.PIN_new = ""
                UserDefaultsHelper.pinIsEnabled = false
                UserDefaultsHelper.pinNewIsEnabled = false
                UserDefaultsHelper.PinLength = nil
                UserDefaultsHelper.AuthType = nil
                UserDefaultsHelper.SmsCodeLength = nil
                UserDefaultsHelper.EtokenCodeLength = nil
                UserDefaultsHelper.SmsCodeAuthExpires = nil
                UserDefaultsHelper.IsInvalidSmsCodeAuthOnIncorrect = nil
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOut"), object: nil)
            }else{
                for user in users{
                    if loginID == user.loginID{
                        if let index = users.index(where: { $0.loginID == loginID }) {
                            print(index)
                            users.remove(at: index)
                            self.users = users
                            UserDefaultsHelper.user = users.last
                        }
                    }
                }
            }
        }
    }
    public func setIndexPath(userIndex: Int){
        self.indexPathCurrent = userIndex
      
    }
    

    public func didLogin(with user:User){
        self.user = user
        //NOTIFICATION
    }
    public func updateUser(user:User){
        self.user = user
    }

//    public func handle(error:Error){
//        if error.apiErrors?.hasAuthErrors ?? false{
//            logOut()
//        }
//    }
    //MARK- Add new users
    public func current(loginID: String){

        if let users = self.users{
            for user in users{
                if loginID == user.loginID{
                    self.user = user
                }
            }
        }
    }
    public func getUsers(){
       
    }
    
    
    public func addNewUser(user: User){
    
        var tempArray = users ?? []
        if !tempArray.isEmpty{
            
            let i = tempArray.firstIndex { (userId) in
                userId.loginID == user.loginID
            }
            if let index = i{
                tempArray[index] = user
                self.user = user
                UserDefaultsHelper.PIN_new = ""
                UserDefaultsHelper.pinIsEnabled = false
                UserDefaultsHelper.pinNewIsEnabled = false
                UserDefaultsHelper.currentUserID = user.loginID
            } else {
                tempArray.append(user)
                self.user = user
                UserDefaultsHelper.PIN_new = ""
                UserDefaultsHelper.pinIsEnabled = false
                UserDefaultsHelper.pinNewIsEnabled = false
                UserDefaultsHelper.currentUserID = user.loginID
            }
            self.users = tempArray
        }else{
            tempArray.append(user)
            self.users = tempArray
            UserDefaultsHelper.PIN_new = ""
            UserDefaultsHelper.pinIsEnabled = false
            UserDefaultsHelper.pinNewIsEnabled = false
            UserDefaultsHelper.currentUserID = user.loginID
            self.user = user
        }
    }
  
//    проверка на обновление токена
    public func getUpdateTokenIfNeedObservable() -> Observable<Void> {
        if isNeedUpdateToken(){
            return managerApi
                .getUpdateTokenWithoutCertificate(grant_type: "refresh_token", refresh_token: user!.refresh_token!)
                .map{ user in
                    self.user = user
                    return Void()}
        } else {
            return Observable.just(Void())
        }
    }
//    проверка, нужен ли updateToken
    private func isNeedUpdateToken() -> Bool{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
        dateFormatter.locale = Locale(identifier: "en_US")

        //"Wed, 06 Feb 2019 10:22:57 GMT "EEE, dd MM yyyy HH:mm:ss ZZZ
        var CurrentTime = 0
        var ExpTime = 0
        
        if let exString = user!.expires{
            
            print("date expire: \(exString)")
            let formattedDate = dateFormatter.date(from: exString)
            print("formattedDate: \(String(describing: formattedDate))")
            
            if let timeEx = formattedDate?.timeIntervalSince1970{
                ExpTime = Int(timeEx)
                print("ExpTime: \(ExpTime)")
            }
        }
        
        let nowTime = Date()
        let timeInterval = nowTime.timeIntervalSince1970
        CurrentTime = Int(timeInterval)
        print("CurrentTime: \(CurrentTime)")
        
        return CurrentTime > ExpTime
    }
}
