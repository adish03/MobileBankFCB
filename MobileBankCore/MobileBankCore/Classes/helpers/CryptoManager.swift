//
//  AesEncrypte.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/7/19.
//

import Foundation
import CryptoSwift

open class CryptoManager {
    
    public static func stringToIVWithAES128EncodedBase64(text: String) -> String{
        
        var StringCode = ""
      
        let key: Array<UInt8> = [225, 62, 18, 122, 77, 71, 7, 193, 217, 113, 61, 146, 98, 175, 82, 108]
        let iv = AES.randomIV(AES.blockSize)
        let text: Array<UInt8> = Array(text.utf8)
        do {
            let encrypted = try AES(key: key, blockMode: CBC(iv: iv), padding: .pkcs7).encrypt(text)
            print(encrypted.toHexString())
            
            let ivEncrypted = iv + encrypted
            
            StringCode = ivEncrypted.toBase64() ?? ""
            
        } catch {
            print(error)
        }
        
        return StringCode
    }
    
}
