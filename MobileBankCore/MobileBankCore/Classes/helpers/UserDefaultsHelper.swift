//
//  UserDefaultsHelper.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import Foundation
import ObjectMapper

open class UserDefaultsHelper{
    //    сохранение состояния смены языка
    open class var isChangeLanguage:Bool{
        get{return UserDefaults.standard.bool(forKey: "UserDefaultsHelper.isChangeLanguage")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.isChangeLanguage")}
    }
    //    сохранение состояния текущего  языка
    open class var currentLanguage: String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.currentLanguage")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.currentLanguage")}
    }
    //    сохранение состояния текущего флага языка
    open class var currentFlagLanguage:String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.currentFlagLanguage")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.currentFlagLanguage")}
    }
    //    сохранение состояния pin
    open class var pinIsEnabled:Bool{
        get{return UserDefaults.standard.bool(forKey: "UserDefaultsHelper.pinIsEnabled")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.pinIsEnabled")}
    }
    //    сохранение состояния pin
    open class var pinNewIsEnabled:Bool{
        get{return UserDefaults.standard.bool(forKey: "UserDefaultsHelper.pinNewIsEnabled")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.pinNewIsEnabled")}
    }
    //    сохранение состояния sms
    open class var smsIsEnabledForPayment:Bool{
        get{return UserDefaults.standard.bool(forKey: "UserDefaultsHelper.smsIsEnabledForPayment")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.smsIsEnabledForPayment")}
    }
    //    сохранение user
    open class var user:User?{
        get{
            if let json  = UserDefaults.standard.string(forKey: "UserDefaultsHelper.user"),
                let user = User(JSONString: json){
                return user
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.user")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.user")
            }
        }
    }
   //    сохранение пользователей
    open class var users:[User]?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.users"){
                let users = Mapper<User>().mapArray(JSONString: json)
                return users
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.users")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.users")
            }
        }
    }
     //    сохранение валют
    open class var currencies:[Currency]?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.currencies"){
                let currencies = Mapper<Currency>().mapArray(JSONString: json)
                return currencies
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.currencies")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.currencies")
            }
        }
    }
    //    сохранение bankDate
    open class var bankDate: String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.bankDate")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.bankDate")}
    }
    //    сохранение currentIndexPath
    open class var currentIndexPath: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.currentIndexPath")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.currentIndexPath")}
    }
     //    сохранение состояния PIN_new
    open class var PIN_new: String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.Pin_new")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.Pin_new")}
    }
    //    сохранение состояния PIN_confirm
    open class var PIN_confirm: String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.Pin_confirm")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.Pin_confirm")}
    }
    //    сохранение состояния PIN
    open class var PIN: String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.Pin")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.Pin")}
    }
    
    //MObile Settings "AuthType": 0,"IsInvalidSmsCodeAuthOnIncorrect": false
    // Сохранение длины пин
    open class var PinLength: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.PinLength")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.PinLength")}
    }
     // Сохранение AuthType
    open class var AuthType: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.AuthType")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.AuthType")}
    }
    
    //Сохранение количества символов в счете
    open class var AccountLength: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.AccountLength")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.AccountLength")}
    }
     // Сохранение длины смс
    open class var SmsCodeLength: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.SmsCodeLength")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.SmsCodeLength")}
    }
     // Сохранение длины Etoken
    open class var EtokenCodeLength: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.EtokenCodeLength")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.EtokenCodeLength")}
    }
     // Сохранение длины Sms
    open class var SmsCodeAuthExpires: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.SmsCodeAuthExpires")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.SmsCodeAuthExpires")}
    }
     // Сохранение длины Sms
    open class var IsInvalidSmsCodeAuthOnIncorrect: Bool?{
        get{return UserDefaults.standard.bool(forKey: "UserDefaultsHelper.IsInvalidSmsCodeAuthOnIncorrect")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.IsInvalidSmsCodeAuthOnIncorrect")}
    }
    // Сохранение длины PasswordLength
    open class var PasswordLength: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.PasswordLength")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.PasswordLength")}
    }
    // Сохранение длины PasswordStrength
    open class var PasswordStrength: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.PasswordStrength")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.PasswordStrength")}
    }
    
    
    
     // Сохранение состояния TouchID
    open class var TouchIDisEnabled: Bool?{
        get{return UserDefaults.standard.bool(forKey: "UserDefaultsHelper.TouchIDisEnabled")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.TouchIDisEnabled")}
    }
     // Сохранение UserID текушего
    open class var currentUserID: String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.currentUserID")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.currentUserID")}
    }
    
    //ModifyUtilities
    // Сохранение состояния услуг
    open class var isModifyUtlities:Bool{
        get{return UserDefaults.standard.bool(forKey: "UserDefaultsHelper.isModifyUtlities")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.isModifyUtlities")}
    }
    // Сохранение даты изменения услуг
    open class var modifyUtlitiesDate:String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.modifyUtlitiesDate")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.modifyUtlitiesDate")}
    }
    // Сохранение даты изменения категорий
    open class var modifyCategoriesDate:String?{
        get{return UserDefaults.standard.string(forKey: "UserDefaultsHelper.modifyCategoriesDate")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.modifyCategoriesDate")}
    }
    // Сохранение услуг
    open class var utilities:Utilities?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.Utilities"){
                let utilities = Utilities(JSONString: json)
                return utilities
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.Utilities")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.Utilities")
            }
        }
    }
    // Сохранение категорий услуг
    open class var categories:Categories?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.Categories"){
                let utilities = Categories(JSONString: json)
                return utilities
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.Categories")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.Categories")
            }
        }
    }
    // Сохранение списка услуг
    open class var listUtilities:[UtilityModel]?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.listUtilities"){
                let listUtilities = Mapper<UtilityModel>().mapArray(JSONString: json)
                return listUtilities
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.listUtilities")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.listUtilities")
            }
        }
    }
    // Сохранение операций
    open class var refOpers:[ReferenceItemModel]?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.refOpers"){
                let refOpers = Mapper<ReferenceItemModel>().mapArray(JSONString: json)
                return refOpers
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.refOpers")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.refOpers")
            }
        }
    }
    // Сохранение кодов валютных операций
    open class var voCodes:[ReferenceItemModel]?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.voCodes"){
                let voCodes = Mapper<ReferenceItemModel>().mapArray(JSONString: json)
                return voCodes
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.voCodes")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.voCodes")
            }
        }
    }
    // Сохранение типов операций
    open class var expenseTypes:[expenseTypeModel]?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.expenseTypes"){
                let expenseTypes = Mapper<expenseTypeModel>().mapArray(JSONString: json)
                return expenseTypes
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.expenseTypes")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.expenseTypes")
            }
        }
    }
    // Сохранение бик-кодов
    open class var bikCodes:[BikCode]?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.bikCodes"){
                let bikCodes = Mapper<BikCode>().mapArray(JSONString: json)
                return bikCodes
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.bikCodes")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.bikCodes")
            }
        }
    }
    // Сохранение кодов платежей
    open class var paymentsCodes:[PaymentCode]?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.paymentsCodes"){
                let paymentsCodes = Mapper<PaymentCode>().mapArray(JSONString: json)
                return paymentsCodes
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.paymentsCodes")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.paymentsCodes")
            }
        }
    }
    // Сохранение кодов валютных операций
    open class var requireVoCodeForCurrencies:SwiftValidateDataModel?{
        get{
            if let json = UserDefaults.standard.string(forKey: "UserDefaultsHelper.requireVoCodeForCurrencies"){
                let requireVoCodeForCurrencies = SwiftValidateDataModel(JSONString: json)
                return requireVoCodeForCurrencies
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.requireVoCodeForCurrencies")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.requireVoCodeForCurrencies")
            }
        }
    }
   // Сохранение количества операций для mainpage
    open class var mainPageOperationCount: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.mainPageOperationCount")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.mainPageOperationCount")}
    }
    // Сохранение максимальной суммы для ClearingGross
    open class var clearingMaxAmount: Int?{
        get{return UserDefaults.standard.integer(forKey: "UserDefaultsHelper.clearingMaxAmount")}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.clearingMaxAmount")}
    }
    
//    Перевод для приложения
    open class var localize:Localize?{
        get{
            if let json  = UserDefaults.standard.string(forKey: "UserDefaultsHelper.localize"),
                let localize = Localize(JSONString: json){
                return localize
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                UserDefaults.standard.set(newValue.toJSONString(), forKey: "UserDefaultsHelper.localize")
            } else {
                UserDefaults.standard.set(nil, forKey: "UserDefaultsHelper.localize")
            }
        }
    }
  
    //    сохранение прав For Views
    open class var allowedViewOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedViewOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedViewOperations")}
    }
    
    //    сохранение прав Template
    open class var allowedTemplateOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedTemplateOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedTemplateOperations")}
    }
    
    //    сохранение прав Swift
    open class var allowedSwiftOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedSwiftOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedSwiftOperations")}
    }
    
    //    сохранение прав Loans
    open class var allowedLoansOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedLoansOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedLoansOperations")}
    }
    
    //    сохранение прав Operation
    open class var allowedOperationOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedOperationOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedOperationOperations")}
    }
    
    //    сохранение прав InternalOperation
    open class var allowedInternalOperationOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedInternalOperationOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedInternalOperationOperations")}
    }
    
    //    сохранение прав ClearingGross
    open class var allowedClearingGrossOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedClearingGrossOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedClearingGrossOperations")}
    }
    
    //    сохранение прав Deposits
    open class var allowedDepositsOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedDepositsOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedDepositsOperations")}
    }
    
    //    сохранение прав Common Utilities
    open class var allowedUtilitiesOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedUtilitiesOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedUtilitiesOperations")}
    }
    
    //    сохранение прав Cards
    open class var allowedCardsOperations:[String: Bool]?{
        get{return UserDefaults.standard.value(forKey: "UserDefaultsHelper.allowedCardsOperations") as? [String : Bool]}
        set{UserDefaults.standard.set(newValue, forKey: "UserDefaultsHelper.allowedCardsOperations")}
    }
    
    
}

