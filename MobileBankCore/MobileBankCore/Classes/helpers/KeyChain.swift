//
//  KeyChain.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/17/19.
//


import Foundation
import ObjectMapper
import SwiftKeychainWrapper

open class KeyChain{
   
    
    open class var user:User?{
        get{
            if let json  = KeychainWrapper.standard.string(forKey: "KeyChain.user"),
                let user = User(JSONString: json){
                return user
            } else {
                return nil
            }
        }
        set{
            if let value  = newValue{
                KeychainWrapper.standard.set(value.toJSONString()!, forKey: "KeyChain.user")
            }else{
                 KeychainWrapper.standard.set("", forKey: "KeyChain.user")
            }
        }
    }
    
    open class var users:[User]?{
        get{
            if let json = KeychainWrapper.standard.string(forKey: "KeyChain.users"){
                let users = Mapper<User>().mapArray(JSONString: json)
                return users
            } else {
                return nil
            }
        }
        set{
            if let newValue = newValue{
                KeychainWrapper.standard.set(newValue.toJSONString()!, forKey: "KeyChain.users")
            } else {
                KeychainWrapper.standard.set("", forKey: "KeyChain.users")
            }
        }
    }
    
    open class var currentUserID: String?{
        get{return KeychainWrapper.standard.string(forKey: "KeyChain.currentUserID")}
        set{KeychainWrapper.standard.set(newValue!, forKey: "KeyChain.currentUserID")}
    }
}
