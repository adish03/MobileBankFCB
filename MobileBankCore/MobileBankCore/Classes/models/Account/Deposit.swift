//
//  Account.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/20/19.
//

import ObjectMapper

// модель для Account
open class Account: Mappable {
    open var
    accountNo: String?,
    currencyID: Int?,
    customerID: Int?,
    officeID: Int?,
    balanceGroup: String?,
    accountName: String?,
    openDate: String?,
    endDate: Bool?,
    closeDate: String?,
    accountTypeID: Int?,
    currentBalance: Double?,
    currentNationalBalance: Double?,
    codename: String?,
    accountGroup: String?,
    currencySymbol: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        customerID <- map["CustomerID"]
        officeID <- map["OfficeID"]
        balanceGroup <- map["BalanceGroup"]
        balanceGroup <- map["BalanceGroup"]
        accountName <- map["AccountName"]
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        accountTypeID <- map["AccountTypeID"]
        currentBalance <- map["CurrentBalance"]
        currentNationalBalance <- map["CurrentNationalBalance"]
        codename <- map["Codename"]
        accountGroup <- map["AccountGroup"]
        currencySymbol <- map["CurrencySymbol"]
    }
}

// модель для Deposit
open class Deposit: Mappable {
    open var
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: Bool?,
    imageName: String?,
    name: String?,
    accountNo: String?,
    currencyID: Int?,
    currencySymbol: String?,
    isShowButton: Bool?,
    
    currency: Currency?
    
    init(){}
    required public init( isShowButton: Bool?)
    {
        self.isShowButton = isShowButton
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        imageName <- map["ImageName"]
        name <- map["Name"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        currencySymbol <- map["CurrencySymbol"]
    }
}

// модель для Accounts
open class Accounts: Mappable{
    open var
    text: String?,
    accountType: Int?,
    isExpandable: Bool?,
    items: [Deposit]?
    
    init(){}
    
    required public init( isExpandable: Bool?){
        self.isExpandable = isExpandable
    }
    
    required public init(text: String?,
                         accountType: Int?,
                         isExpandable: Bool?,
                         items: [Deposit]?){
        self.text = text
        self.accountType = accountType
        self.isExpandable = isExpandable
        self.items = items
        
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        text <- map["Text"]
        accountType <- map["AccountType"]
        items <- map["Items"]
    }
    //    функция для создания копии счетов
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = Accounts(text: text, accountType: accountType, isExpandable: isExpandable, items: items)
        return copy
    }
}
// модель для PaymentCode
open class PaymentCode: Mappable {
    open var
    code: String?,
    description: String?,
    treasuryCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        code <- map["Code"]
        description <- map["Description"]
        treasuryCode <- map["TreasuryCode"]
    }
}
open class SimpleAccountModel: Mappable {
    open var
    name: String?,
    customerID: Int?,
    canMakeIncome: Bool?,
    currencySymbol: String?,
    accountNo: String?,
    currencyID: Int?
    
    init(){}
    required public init(name: String?,
                         customerID: Int?,
                         canMakeIncome: Bool?,
                         currencySymbol: String?,
                         accountNo: String?,
                         currencyID: Int?){
        self.name = name
        self.customerID = customerID
        self.canMakeIncome = canMakeIncome
        self.currencySymbol = currencySymbol
        self.accountNo = accountNo
        self.currencyID = currencyID
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        name <- map["Name"]
        customerID <- map["CustomerID"]
        canMakeIncome <- map["CanMakeIncome"]
        currencySymbol <- map["CurrencySymbol"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
    }
    
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = SimpleAccountModel(name: name, customerID: customerID, canMakeIncome: canMakeIncome, currencySymbol: currencySymbol, accountNo: accountNo, currencyID: currencyID)
        return copy
    }
}
// модель для BikCode
open class BikCode: Mappable {
    open var
    code: String?,
    name: String?,
    mfo: String?,
    corrNBKF: String?,
    regionName: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        code <- map["Code"]
        name <- map["Name"]
        mfo <- map["Mfo"]
        corrNBKF <- map["CorrNBKF"]
        regionName <- map["RegionName"]
    }
}


// модель для SubmitForCurrenciesModel
open class SubmitForCurrenciesModel {
    
    open var
    accountFrom: String,
    sumSale: String,
    accountTo: String,
    sumBuy: String,
    converssions: Double,
    transactionNumber: String,
    dateTime: String
    
    required public init(accountFrom:String,
                         sumSale: String,
                         accountTo: String,
                         sumBuy: String,
                         converssions: Double,
                         transactionNumber: String,
                         dateTime: String){
        self.accountFrom = accountFrom
        self.sumSale = sumSale
        self.accountTo = accountTo
        self.sumBuy = sumBuy
        self.converssions = converssions
        self.transactionNumber = transactionNumber
        self.dateTime = dateTime
    }
    
}
// модель для списка истории операций
open class ListContractModelExecuteResult: Mappable {
    
    open var
    paginationInfo: PaginationInfo?,
    result: [ContractModel]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        paginationInfo <- map["PaginationInfo"]
        result <- map["Result"]
    }
}
// модель для пагинаций
open class PaginationInfo: Mappable {
    
    open var
    page: Int?,
    pageSize: Int?,
    totalItems: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        page <- map["Page"]
        pageSize <- map["PageSize"]
        totalItems <- map["TotalItems"]
        
    }
}
// модель для списка операций
open class ContractModel: Mappable {
    open var
    operationID: Int?,
    confirmTypeID: Int?,
    date: String?,
    description: String?,
    operationType: String?,
    confirmType: String?,
    isSchedule: Bool?,
    isLock: Bool?,
    amount: Double?,
    currencyID: Int?,
    operationTypeID: Int?,
    operationParameters: Int?,
    imageName: String?,
    currency: Currency?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        confirmTypeID <- map["ConfirmTypeID"]
        date <- map["Date"]
        description <- map["Description"]
        operationType <- map["OperationType"]
        confirmType <- map["ConfirmType"]
        isSchedule <- map["IsSchedule"]
        isLock <- map["IsLock"]
        amount <- map["Amount"]
        currencyID <- map["CurrencyID"]
        operationTypeID <- map["OperationTypeID"]
        operationParameters <- map["OperationParameters"]
        imageName <- map["ImageName"]
        
    }
}

// модель для деталей счетов
open class DetailModel: Mappable {
    open var
    accountName: String?,
    inBalance: Double?,
    outBalance: Double?,
    operations: [AccountOperation]?,
    totalOperationsCount: Int?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountName <- map["AccountName"]
        inBalance <- map["InBalance"]
        outBalance <- map["OutBalance"]
        operations <- map["Operations"]
        totalOperationsCount <- map["TotalOperationsCount"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        
        
    }
}

// модель для закрытии депозита
open class CloseDepositModel: Mappable {
    open var
    accountNo: String?,
    currencyID: Int?,
    corrAccountNo: String?,
    reason: String?
    
    init(){}
    required public init(
        accountNo: String?,
        currencyID: Int?,
        corrAccountNo: String?,
        reason: String?
        ){
        self.accountNo = accountNo
        self.currencyID = currencyID
        self.corrAccountNo = corrAccountNo
        self.reason = reason
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        corrAccountNo <- map["CorrAccountNo"]
        reason <- map["Reason"]
    }
}

// модель для документов
open class ReportsExecuteResult: Mappable {
    open var
    reportDocuments: [Document]?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        reportDocuments <- map["ReportDocuments"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        
    }
}

// модель для документа
open class Document: Mappable {
    open var
    fileName: String?,
    fileBody: String?,
    contentType: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        fileName <- map["FileName"]
        fileBody <- map["FileBody"]
        contentType <- map["ContentType"]
        
    }
}


// модель для деталей
open class AccountOperation: Mappable {
    open var
    postion: Int?,
    transactionDate: String?,
    documentNo: String?,
    dtSumm: Double?,
    ctSumm: Double?,
    comment: String?,
    operationDate: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        postion <- map["Postion"]
        transactionDate <- map["TransactionDate"]
        documentNo <- map["DocumentNo"]
        dtSumm <- map["DtSumm"]
        ctSumm <- map["CtSumm"]
        comment <- map["Comment"]
        operationDate <- map["OperationDate"]
    }
}
// модель для DetailAccountsModel
open class DetailAccountsModel: Mappable {
    open var
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: Bool?,
    imageName: String?,
    name: String?,
    accountNo: String?,
    currencyID: Int?,
    currencySymbol: String?,
    isShowButton: Bool?,
    isShowSwithButton: Bool?,
    DepositAccountStatusID: Int?,
    DepositAccountStatusName: String?

    init(){}
    required public init( isShowButton: Bool?)
    {
        self.isShowButton = isShowButton
    }
    
    required public init( isShowSwithButton: Bool?)
    {
        self.isShowSwithButton = isShowSwithButton
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        imageName <- map["ImageName"]
        name <- map["Name"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        currencySymbol <- map["CurrencySymbol"]
        DepositAccountStatusID <- map["DepositAccountStatusID"]
        DepositAccountStatusName <- map["DepositAccountStatusName"]
    }
}


// модель для деталей депозитов
open class DetailDepositsModel: Mappable {
    open var
    percentAccountNo: String?,
    percentAccountName: String?,
    percentAccountCloseDate: String?,
    percentsSumm: Double?,
    period: Int?,
    programID: Int?,
    interestRate: Double?,
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: Bool?,
    imageName: String?,
    name: String?,
    currencySymbol: String?,
    accountNo: String?,
    isShowButton: Bool?,
    currencyID: Int?
    
    init(){}
    required public init( isShowButton: Bool?)
    {
        self.isShowButton = isShowButton
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        percentAccountNo <- map["PercentAccountNo"]
        percentAccountName <- map["PercentAccountName"]
        percentAccountCloseDate <- map["PercentAccountCloseDate"]
        percentsSumm <- map["PercentsSumm"]
        period <- map["Period"]
        programID <- map["ProgramID"]
        interestRate <- map["InterestRate"]
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        imageName <- map["ImageName"]
        name <- map["Name"]
        currencySymbol <- map["CurrencySymbol"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
    }
}

// модель для деталей карт
open class DetailCardsModel: Mappable {
    open var
    codeWord: String?,
    companyID: Int?,
    companyName: String?,
    productID: Int?,
    productName: String?,
    period: Int?,
    balanceGroup: String?,
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: String?,
    imageName: String?,
    name: String?,
    currencySymbol: String?,
    accountNo: String?,
    currencyID: Int?,
    availableLimit: Float?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        codeWord <- map["CodeWord"]
        companyID <- map["CompanyID"]
        companyName <- map["CompanyName"]
        productID <- map["ProductID"]
        productName <- map["ProductName"]
        period <- map["Period"]
        balanceGroup <- map["BalanceGroup"]
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        imageName <- map["ImageName"]
        name <- map["Name"]
        currencySymbol <- map["CurrencySymbol"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        availableLimit <- map["AvailableLimit"]
        
    }
}

// модель для деталей кредита
open class CreditModel: Mappable {
    open var
    IsRequest: Bool?,
    CreditID: Int?,
    ProductTypeID: Int?,
    ProductName: String?,
    GroupName: String?,
    StatusID: Int?,
    StatusName: String?,
    StatusDate: String?,
    ApprovedSumm: Double?,
    ApprovedCurrency: String?,
    CurrencyID: Int?,
    ApprovedRate: Double?,
    ApprovedPeriod: Int?,
    OfficeName: String?,
    AgreementNo: String?,
    IsTranchesAllowed: String?,
    EffectiveRate: Double?,
    AccountNo: String?,
    AccBalance: Double?,
    CreditFines: Int?,
    OverdueDays: Int?,
    NextPaymentDate: String?,
    NotCalculationStatus: Bool?,
    StopCalculationStatus: Bool?,
    ExceedMaxFinesCredits: Bool?,
    LoanAccountNo: String?,
    PercentAccountNo: String?,
    Tranches: String?,
    isShowButton: Bool?
    
    init(){}
    required public init( isShowButton: Bool?)
    {
        self.isShowButton = isShowButton
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        IsRequest <- map["IsRequest"]
        CreditID <- map["CreditID"]
        ProductTypeID <- map["ProductTypeID"]
        ProductName <- map["ProductName"]
        GroupName <- map["GroupName"]
        StatusID <- map["StatusID"]
        StatusName <- map["StatusName"]
        StatusDate <- map["StatusDate"]
        ApprovedSumm <- map["ApprovedSumm"]
        ApprovedCurrency <- map["ApprovedCurrency"]
        CurrencyID <- map["CurrencyID"]
        ApprovedRate <- map["ApprovedRate"]
        ApprovedPeriod <- map["ApprovedPeriod"]
        OfficeName <- map["OfficeName"]
        AgreementNo <- map["AgreementNo"]
        IsTranchesAllowed <- map["IsTranchesAllowed"]
        EffectiveRate <- map["EffectiveRate"]
        AccountNo <- map["AccountNo"]
        AccBalance <- map["AccBalance"]
        CreditFines <- map["CreditFines"]
        OverdueDays <- map["OverdueDays"]
        NextPaymentDate <- map["NextPaymentDate"]
        NotCalculationStatus <- map["NotCalculationStatus"]
        StopCalculationStatus <- map["StopCalculationStatus"]
        ExceedMaxFinesCredits <- map["ExceedMaxFinesCredits"]
        LoanAccountNo <- map["LoanAccountNo"]
        PercentAccountNo <- map["PercentAccountNo"]
        Tranches <- map["Tranches"]
        
    }
}

// модель для деталей кредитов
open class DetailCreditsModel: Mappable {
    open var
    credits: [CreditModel]?,
    requests: [CreditModel]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        credits <- map["Credits"]
        requests <- map["Requests"]
        
    }
}
// модель для AllFinance
open class AllFinance {
    
    open var
    text: String,
    accountsType: Int,
    isExpandable: Bool?,
    items: [AnyObject]
    
    required public init(text:String,
                         accountsType: Int,
                         items:[AnyObject]){
        self.text = text
        self.accountsType = accountsType
        self.items = items
    }
    required public init(text:String,
                         accountsType: Int,
                         isExpandable: Bool,
                         items:[AnyObject]){
        self.text = text
        self.accountsType = accountsType
        self.isExpandable = isExpandable
        self.items = items
    }
    
    //    функция для создания копии финансов
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = AllFinance(text: text, accountsType: accountsType, isExpandable: isExpandable ?? false, items: items)
        return copy
    }
}
// модель для деталей карты

open class DetailCardModel: Mappable {
    open var
    balance: Double?,
    cardNo: String?,
    status: Int?,
    expiryDate: String?,
    condSet: String?,
    baseSupp: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        balance <- map["Balance"]
        cardNo <- map["CardNo"]
        status <- map["Status"]
        expiryDate <- map["ExpiryDate"]
        condSet <- map["CondSet"]
        baseSupp <- map["BaseSupp"]
    }
}

// модель для  Products

open class Products: Codable {
    public let result: Result?
    public let state: Int?
    public let message: String?
    public let messageCode: String?
    
    enum CodingKeys: String, CodingKey {
        case result = "Result"
        case state = "State"
        case message = "Message"
        case messageCode = "messageCode"
    }

    init(result: Result?, state: Int?, message: String?, messageCode: String?) {
        self.result = result
        self.state = state
        self.message = message
        self.messageCode = messageCode
    }
}

open class Result: Codable {
    public let customerOrganizationType: Int?
    public let isCustomerLegalEntity: Bool?
    public let selectableProductItems: [SelectableProductItem]?

    enum CodingKeys: String, CodingKey {
        case customerOrganizationType = "CustomerOrganizationType"
        case isCustomerLegalEntity = "IsCustomerLegalEntity"
        case selectableProductItems = "SelectableProductItems"
    }

    init(customerOrganizationType: Int?, isCustomerLegalEntity: Bool?, selectableProductItems: [SelectableProductItem]?) {
        self.customerOrganizationType = customerOrganizationType
        self.isCustomerLegalEntity = isCustomerLegalEntity
        self.selectableProductItems = selectableProductItems
    }
}

// MARK: - SelectableProductItem
open class SelectableProductItem: Codable {
    public let organizationType: Int?
    public let organizationTypeName: String?
    public let products: [Product]?

    enum CodingKeys: String, CodingKey {
        case organizationType = "OrganizationType"
        case organizationTypeName = "OrganizationTypeName"
        case products = "Products"
    }

    init(organizationType: Int?, organizationTypeName: String?, products: [Product]?) {
        self.organizationType = organizationType
        self.organizationTypeName = organizationTypeName
        self.products = products
    }
}

open class PeriodByCurrency: Codable {
    public let symbol: String?
    public let currencyID: Int?
    public let periods: [Period]?
    
    enum CodingKeys: String, CodingKey {
        case symbol = "Symbol"
        case currencyID = "CurrencyID"
        case periods = "Periods"
    }
    
    required public init(symbol: String?) {
        self.symbol = symbol
        self.currencyID = nil
        self.periods = nil
    }
    init(symbol: String?, currencyID: Int?, periods: [Period]?){
        self.symbol = symbol
        self.currencyID = currencyID
        self.periods = periods
    }
}

// MARK: - Product
open class Product: Codable {
    public var name: String?
    public var productID, productTypeID, productPeriodTypeID: Int?
    public var currencies: [PeriodByCurrency]?
    public var capitalizationAgreementMessage, confirmationAgreementMessage: String?
    public var requiredCurrentAccount: Bool?
    public var productDescription: String?
    public var imageName: String?
    public var genderDeposit: Bool?
    public var showOrder: Int?
    public var isGraphic: Bool?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case productID = "ProductID"
        case currencies = "Currencies"
        case productTypeID = "ProductTypeID"
        case productPeriodTypeID = "ProductPeriodTypeID"
        case capitalizationAgreementMessage = "CapitalizationAgreementMessage"
        case confirmationAgreementMessage = "ConfirmationAgreementMessage"
        case requiredCurrentAccount = "RequiredCurrentAccount"
        case productDescription = "Description"
        case imageName = "ImageName"
        case genderDeposit = "GenderDeposit"
        case showOrder = "ShowOrder"
        case isGraphic = "IsGraphic"
    }

    init(name: String?, productID: Int?, currencies: [PeriodByCurrency]?,productTypeID: Int?, productPeriodTypeID: Int?, capitalizationAgreementMessage: String?, confirmationAgreementMessage: String?, requiredCurrentAccount: Bool?, productDescription: String?, imageName: String?, genderDeposit: Bool?, showOrder: Int?, isGraphic: Bool?) {
        self.name = name
        self.productID = productID
        self.currencies = currencies
        self.productTypeID = productTypeID
        self.productPeriodTypeID = productPeriodTypeID
        self.capitalizationAgreementMessage = capitalizationAgreementMessage
        self.confirmationAgreementMessage = confirmationAgreementMessage
        self.requiredCurrentAccount = requiredCurrentAccount
        self.productDescription = productDescription
        self.imageName = imageName
        self.genderDeposit = genderDeposit
        self.showOrder = showOrder
        self.isGraphic = isGraphic
    }
}



// MARK: - Period
open class Period: Codable {
    public let periodType, currencyID, periodCount: Int?
    public let rate: Double?
    public let startPeriod, endPeriod, productID, minSummOnAccount: Int?
    public let maxDepositSumm: Int?

    enum CodingKeys: String, CodingKey {
        case periodType = "PeriodType"
        case currencyID = "CurrencyID"
        case periodCount = "PeriodCount"
        case rate = "Rate"
        case startPeriod = "StartPeriod"
        case endPeriod = "EndPeriod"
        case productID = "ProductID"
        case minSummOnAccount = "MinSummOnAccount"
        case maxDepositSumm = "MaxDepositSumm"
    }

    init(periodType: Int?, currencyID: Int?, periodCount: Int?, rate: Double?, startPeriod: Int?, endPeriod: Int?, productID: Int?, minSummOnAccount: Int?, maxDepositSumm: Int?) {
        self.periodType = periodType
        self.currencyID = currencyID
        self.periodCount = periodCount
        self.rate = rate
        self.startPeriod = startPeriod
        self.endPeriod = endPeriod
        self.productID = productID
        self.minSummOnAccount = minSummOnAccount
        self.maxDepositSumm = maxDepositSumm
    }
}

// модель для OpenDepositModel
open class OpenDepositModel: Mappable {
    open var
    accountNo: String?,
    currencyID: Int?,
    period: Int?,
    productID: Int?,
    sumV: Double?,
    purposeID: Int?,
    scheduleParameters: ScheduleParameters?
    
    
    init(){}
    required public init(accountNo: String?,
                         currencyID: Int?,
                         period: Int?,
                         productID: Int?,
                         sumV: Double?,
                         purposeID: Int?,
                         scheduleParameters: ScheduleParameters?)
    {
        self.accountNo = accountNo
        self.currencyID = currencyID
        self.period = period
        self.productID = productID
        self.sumV = sumV
        self.purposeID = purposeID
        self.scheduleParameters = scheduleParameters
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        purposeID <- map["PurposeID"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        period <- map["PeriodCount"]
        productID <- map["ProductID"]
        sumV <- map["SumV"]
        scheduleParameters <- map["ScheduleParameters"]
    }
}

open class ScheduleParameters: Mappable {
    open var
    productID: Int?,
    currencyID: Int?,
    period: Int?,
    initialFee: Double?,
    replanishmentAmount: Double?,
    refillBalanceDate: String?,
    date: String?,
    considerWorkingDays: Bool?
    
    init(){}
    required public init(productID: Int?,
                         currencyID: Int?,
                         period: Int?,
                         initialFee: Double?,
                         replanishmentAmount: Double?,
                         refillBalanceDate: String?,
                         date: String?,
                         considerWorkingDays: Bool?) {
        self.productID = productID
        self.currencyID = currencyID
        self.period = period
        self.initialFee = initialFee
        self.replanishmentAmount = replanishmentAmount
        self.refillBalanceDate = refillBalanceDate
        self.date = date
        self.considerWorkingDays = considerWorkingDays
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        productID <- map["ProductID"]
        currencyID <- map["CurrencyID"]
        period <- map["Period"]
        initialFee <- map["InitialFee"]
        replanishmentAmount <- map["ReplenishmentAmount"]
        refillBalanceDate <- map["RefillBalanceDate"]
        date <- map["Date"]
        considerWorkingDays <- map["ConsiderWorkingDays"]
    }
}

open class DepositPublicOfferExecuteResult: Mappable {
    open var
    
    accountNo: String?,
    state: Int?,
    message: String?,
    messageCode: String?,
    reportDocuments: [Documents]?
        
    init(){}
    required public init(  accountNo: String?,
                           state: Int?,
                           message: String?,
                           messageCode: String?)
    {
        self.accountNo = accountNo
        self.state = state
        self.message = message
        self.messageCode = messageCode
        
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        accountNo <- map["AccountNo"]
        state <- map["State"]
        reportDocuments <- map["ReportDocuments"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        
    }
}


open class GetStatus: Mappable {
    open var
    message: String?,
    messageCode: String?,
    result: [String]?,
    state: Int?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        result <- map["Result"]
        state <- map["State"]
    }
}

open class DepositScheduleResponse: Mappable {
    open var
    message: String?,
    messageCode: String?,
    result: [DepositScheduleResult]?,
    state: Int?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        result <- map["Result"]
        state <- map["State"]
    }
}

open class DepositScheduleResult: Mappable {
    open var
    date: String?,
    daysCount: Int?,
    mainSumm: Double?,
    replanishmentAmount: Int?,
    rate: Double?,
    bonus: Double?,
    capitalizationSumm: Double?,
    isCapitalization: Bool?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        date <- map["Date"]
        daysCount <- map["DaysCount"]
        mainSumm <- map["MainSumm"]
        replanishmentAmount <- map["ReplenishmentAmount"]
        rate <- map["Rate"]
        bonus <- map["Bonus"]
        capitalizationSumm <- map["CapitalizationSumm"]
        isCapitalization <- map["isCapitalization"]
    }
}
