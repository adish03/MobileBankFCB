//
//  XFincaCityDictionary.swift
//  Alamofire
//
//  Created by Рустам on 19/3/22.
//

import ObjectMapper

open class XFincaCityDictionary: Mappable {
    open var
    id: Int?,
    name: String?,
    directions: [String?]?,
    currencies: [Int?]?
    
    init(){}
    required public init(id: Int?,
                         name: String?,
                         directions: [String?]?,
                         currencies: [Int?]?) {
        self.id = id
        self.name = name
        self.directions = directions
        self.currencies = currencies
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        directions <- map["directions"]
        currencies <- map["currencies"]
    }
    
    //    функция для создания копии справочника городов X-Finca
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = XFincaCityDictionary(id: id, name: name, directions: directions, currencies: currencies)
        return copy
    }
}
open class XFincaCountryDictionary: Mappable {
    open var
    countryID: Int?,
    shortName: String?,
    shortNameEN: String?,
    fullName: String?,
    capital: String?,
    code: Int?,
    code2: String?,
    code3: String?,
    countryCode: Int?,
    countryTypeID: Int?,
    fatfTypeID: Int?,
    isHighRisk: Bool?,
    phoneCode: String?
    
    init(){}
    required public init(countryID: Int?,
                         shortName: String?,
                         shortNameEN: String?,
                         fullName: String?,
                         capital: String?,
                         code: Int?,
                         code2: String?,
                         code3: String?,
                         countryCode: Int?,
                         countryTypeID: Int?,
                         fatfTypeID: Int?,
                         isHighRisk: Bool?,
                         phoneCode: String?) {
        self.countryID = countryID
        self.shortName = shortName
        self.fullName = fullName
        self.capital = capital
        self.code = code
        self.code2 = code2
        self.code3 = code3
        self.countryTypeID = countryTypeID
        self.fatfTypeID = fatfTypeID
        self.isHighRisk = isHighRisk
        self.phoneCode = phoneCode
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        countryID <- map["CountryID"]
        shortName <- map["ShortName"]
        shortNameEN <- map["ShortNameEN"]
        fullName <- map["FullName"]
        capital <- map["Capital"]
        code <- map["Code"]
        code2 <- map["Code2"]
        code3 <- map["Code3"]
        countryCode <- map["CountryCode"]
        countryTypeID <- map["CountryTypeID"]
        fatfTypeID <- map["FatfTypeID"]
        isHighRisk <- map["IsHighRisk"]
        phoneCode <- map["PhoneCode"]
    }
    
    //функция для создания копии справочника стран X-Finca
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = XFincaCountryDictionary(countryID: countryID, shortName: shortName, shortNameEN: shortNameEN, fullName: fullName, capital: capital, code: code, code2: code2, code3: code3, countryCode: countryCode, countryTypeID: countryTypeID, fatfTypeID: fatfTypeID, isHighRisk: isHighRisk, phoneCode: phoneCode)
        return copy
    }
}

open class XFincaCommission: Mappable {
    open var
    status: Int?,
    description: String?,
    data: XFincaData?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        status <- map["Status"]
        description <- map["Description"]
        data <- map["Data"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}

open class XFincaData: Mappable {
    open var
    feeTotal: XFincaFee?,
    toCountryISO: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        feeTotal <- map["feeTotal"]
        toCountryISO <- map["toCountryISO"]
    }
}

open class XFincaFee: Mappable {
    open var
    currency: Int?,
    amount: Double?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        currency <- map["currency"]
        amount <- map["amount"]
    }
}

open class XFincaPhoneMask: Mappable {
    open var
    phoneCode: String?,
    lenghtPhoneNumber: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        phoneCode <- map["PhoneCode"]
        lenghtPhoneNumber <- map["LenghtPhoneNumber"]
    }
}
