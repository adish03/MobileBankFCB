//
//  Pin.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/11/19.
//

import ObjectMapper

open class MobileSettings: Mappable {
    
    open
    var pinLength: Int?,
    authType: Int?,
    accountLength: Int?,
    smsCodeLength: Int?,
    etokenCodeLength: Int?,
    smsCodeAuthExpires: Int?,
    isInvalidSmsCodeAuthOnIncorrect: Bool?,
    mainPageOperationCount: Int?,
    passwordLength: Int?,
    passwordStrength: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        pinLength <- map["PinLength"]
        authType <- map["AuthType"]
        accountLength <- map["AccountLength"]
        smsCodeLength <- map["SmsCodeLength"]
        etokenCodeLength <- map["EtokenCodeLength"]
        smsCodeAuthExpires <- map["SmsCodeAuthExpires"]
        isInvalidSmsCodeAuthOnIncorrect <- map["IsInvalidSmsCodeAuthOnIncorrect"]
        mainPageOperationCount <- map["MainPageOperationCount"]
        passwordLength <- map["PasswordLength"]
        passwordStrength <- map["PasswordStrength"]

    }
}

//  модель для получения баннеров
open class Banner: Mappable {
    
    open
    var fileName: String?,
    link: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        fileName <- map["FileName"]
        link <- map["Link"]
       
    }
}



