//
//  Swift.swift
//  Alamofire
//
//  Created by Oleg Ten on 4/2/19.
//

import ObjectMapper

// Модель для описания коммиссий
open class CommissionModel {
    
    open
    var name: String?,
    summ: Double?,
    surrencyName: String?
    
    public init(name: String?,
                summ: Double?,
                surrencyName: String?)
    {
        self.name = name
        self.summ = summ
        self.surrencyName = surrencyName
    }
}

// Модель для получения данных для проверки создания нового перевода
open class SwiftValidateDataModel: Mappable {
    
    open
    var requireVoCodeForCurrencies: [Int]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        requireVoCodeForCurrencies <- map["RequireVoCodeForCurrencies"]
    }
}


// Модель для получения данных операций
open class ReferenceItemModel: Mappable {
    
    open
    var dependency: Dependency?,
    value: String?,
    text: String?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        dependency <- map["Dependency"]
        value <- map["Value"]
        text <- map["Text"]
        
    }
}

// Модель для получения данных операций
open class DepositPurposeReferenceItemModel: Mappable {
    
    open
    var dependency: Dependency?,
    value: Int?,
    text: String?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        dependency <- map["Dependency"]
        value <- map["Value"]
        text <- map["Text"]
        
    }
}

open class CountryReferenceItemModel: Mappable {
    
    open
    var dependency: Dependency?,
    value: Int?,
    text: String?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        dependency <- map["Dependency"]
        value <- map["Value"]
        text <- map["Text"]
        
    }
}
// Модель для получения данных операций
open class expenseTypeModel: Mappable {
    
    open
    var dependency: Dependency?,
    value: Int?,
    text: String?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        dependency <- map["Dependency"]
        value <- map["Value"]
        text <- map["Text"]
        
    }
}
// Модель для получения данных dependency при получении операций
open class Dependency: Mappable {
    
    open
    var text: [BaseAccountDataModel]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        text <- map["Text"]
    }
}

// Модель для SwiftCommissionModel.
open class SwiftCommissionModel: Mappable {
    
    open
    var commissionName: String?,
    commissionSumm: Double?,
    currencyID: Int?,
    commissionID: Int?,
    
    currency: Currency?
    
    init(){}
    required public init(
                         commissionName: String?,
                         commissionSumm: Double?,
                         currencyID: Int?
        ){
        self.commissionName = commissionName
        self.commissionSumm = commissionSumm
        self.currencyID = currencyID
        
    }

    required public init?(map: Map) {}
    public func mapping(map: Map) {
        commissionName <- map["CommissionName"]
        commissionSumm <- map["CommissionSumm"]
        currencyID <- map["CurrencyID"]
        commissionID <- map["CommissionID"]
        
    }
}
// Модель для Транслитерации
open class TransliterationSwiftTransferModel: Mappable {
    
    open
    var detailsOfPayment: String?,
    fullNameReceiver: String?,
    lang: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        detailsOfPayment <- map["DetailsOfPayment"]
        fullNameReceiver <- map["FullNameReceiver"]
        lang <- map["Lang"]
    }
}
// Модель для получения БИК кода
open class SwiftBikModel: Mappable {
    
    open
    var bic: String?,
    address: String?,
    name: String?,
    branch: String?,
    city: String?,
    country: String?,
    countryCode: String?,
    accountNo: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        bic <- map["Bic"]
        address <- map["Address"]
        name <- map["Name"]
        branch <- map["CommissionID"]
        bic <- map["Bic"]
        city <- map["City"]
        country <- map["Country"]
        countryCode <- map["CountryCode"]
        accountNo <- map["AccountNo"]
        
    }
}

open class SwiftOperation: Mappable{
    open var
    operationID: Int?,
    message: String?
    
    init(){}
    required public init(operationID: Int,
                         message: String?){
        self.operationID = operationID
        self.message = message
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        message <- map["Message"]
    }
}
// Модель для создания SWIFT перевода
open class SwiftTransferModel: Mappable {
    open var
    senderFullName: String?,
    senderCustomerID: Int?,
    expenseType: Int?,
    paymentCode: String?,
    currencyID: Int?,
    accountNo: String?,
    transferSum: Double?,
    datePP: String?,
    dateVal: String?,
    detailsOfPayment: String?,
    codeVO: String?,
    intermediaryBank: String?,
    intermediaryBankCountry: String?,
    intermediaryBankBic: String?,
    intermediaryBankLoroAccountNo: String?,
    receiverBank: String?,
    receiverBankCountryCode: String?,
    receiverBankBic: String?,
    receiverBankLoroAccountNo: String?,
    fullNameReceiver: String?,
    receiverAccountNo: String?,
    numberPP: String?,
    operationType: Int?,
    senderBank: String?,
    senderBankCountry: String?,
    senderBankBic: String?,
    senderBankLoroAccountNo: String?,
    beneficiaryBank: String?,
    beneficiaryBankCountry: String?,
    beneficiaryBankBic: String?,
    beneficiaryBankLoroAccountNo: String?,
    type: Int?,
    operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName: String?,
    templateDescription: String?,
    scheduleID: Int?,
    schedule: Schedule?,
    confirmType: Int?,
    additionalInfo: String?,
    receiverAddress: String?,
    receiverCountryCode: String?,
    isCommissionAgreementChecked: Bool?,
    swiftDocuments: [SwiftDocument]?
    
    init(){}
    required public init(senderFullName: String?,
                         senderCustomerID: Int?,
                         expenseType: Int?,
                         currencyID: Int?,
                         accountNo: String?,
                         transferSum: Double?,
                         detailsOfPayment: String?,
                         codeVO: String?,
                         intermediaryBank: String?,
                         intermediaryBankCountry: String?,
                         intermediaryBankBic: String?,
                         intermediaryBankLoroAccountNo: String?,
                         paymentCode: String?,
                         receiverBank: String?,
                         receiverBankCountryCode: String?,
                         receiverBankBic: String?,
                         fullNameReceiver: String?,
                         receiverAccountNo: String?,
                         receiverBankLoroAccountNo: String?,
                         numberPP: String?,
                         datePP: String?,
                         dateVal: String?,
                         operationType: Int?,
                         type: Int?,
                         senderBank: String?,
                         senderBankCountry: String?,
                         senderBankBic: String?,
                         senderBankLoroAccountNo: String?,
                         operationID: Int?,
                         isSchedule: Bool?,
                         isTemplate: Bool?,
                         templateName: String?,
                         templateDescription: String?,
                         scheduleID: Int?,
                         schedule: Schedule?,
                         additionalInfo: String?,
                         receiverAddress: String?,
                         receiverCountryCode: String?,
                         isCommissionAgreementChecked: Bool?,
                         swiftDocuments: [SwiftDocument]? 
                         
        ){
        self.senderFullName = senderFullName
        self.senderCustomerID = senderCustomerID
        self.expenseType = expenseType
        self.currencyID = currencyID
        self.accountNo = accountNo
        self.transferSum = transferSum
        self.detailsOfPayment = detailsOfPayment
        self.codeVO = codeVO
        self.intermediaryBank = intermediaryBank
        self.intermediaryBankCountry = intermediaryBankCountry
        self.intermediaryBankBic = intermediaryBankBic
        self.intermediaryBankLoroAccountNo = intermediaryBankLoroAccountNo
        self.paymentCode = paymentCode
        self.receiverBank = receiverBank
        self.receiverBankCountryCode = receiverBankCountryCode
        self.receiverBankBic = receiverBankBic
        self.fullNameReceiver = fullNameReceiver
        self.receiverAccountNo = receiverAccountNo
        self.receiverBankLoroAccountNo = receiverBankLoroAccountNo
        self.numberPP = numberPP
        self.datePP = datePP
        self.dateVal = dateVal
        self.operationType = operationType
        self.type = type
        self.senderBank = senderBank
        self.senderBankCountry = senderBankCountry
        self.senderBankBic = senderBankBic
        self.senderBankLoroAccountNo = senderBankLoroAccountNo
        self.operationID = operationID
        self.isSchedule = isSchedule
        self.isTemplate = isTemplate
        self.templateName = templateName
        self.templateDescription = templateDescription
        self.scheduleID = scheduleID
        self.schedule = schedule
        self.additionalInfo = additionalInfo
        self.receiverAddress = receiverAddress
        self.receiverCountryCode = receiverCountryCode
        self.isCommissionAgreementChecked = isCommissionAgreementChecked
        self.swiftDocuments = swiftDocuments
    }

    required public init(operationID: Int?){
       self.operationID = operationID
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        senderFullName <- map["SenderFullName"]
        senderCustomerID <- map["SenderCustomerID"]
        expenseType <- map["ExpenseType"]
        paymentCode <- map["PaymentCode"]
        currencyID <- map["CurrencyID"]
        accountNo <- map["AccountNo"]
        transferSum <- map["TransferSum"]
        detailsOfPayment <- map["DetailsOfPayment"]
        codeVO <- map["CodeVO"]
        intermediaryBank <- map["IntermediaryBank"]
        intermediaryBankCountry <- map["IntermediaryBankCountry"]
        intermediaryBankBic <- map["IntermediaryBankBic"]
        intermediaryBankLoroAccountNo <- map["IntermediaryBankLoroAccountNo"]
        receiverBank <- map["ReceiverBank"]
        receiverBankCountryCode <- map["ReceiverBankCountryCode"]
        receiverBankBic <- map["ReceiverBankBic"]
        receiverBankLoroAccountNo <- map["ReceiverBankLoroAccountNo"]
        fullNameReceiver <- map["FullNameReceiver"]
        receiverAccountNo <- map["ReceiverAccountNo"]
        numberPP <- map["NumberPP"]
        datePP <- map["DatePP"]
        dateVal <- map["DateVal"]
        operationType <- map["OperationType"]
        senderBank <- map["SenderBank"]
        senderBankCountry <- map["SenderBankCountry"]
        senderBankBic <- map["SenderBankBic"]
        senderBankLoroAccountNo <- map["SenderBankLoroAccountNo"]
        beneficiaryBank <- map["BeneficiaryBank"]
        beneficiaryBankCountry <- map["BeneficiaryBankCountry"]
        beneficiaryBankBic <- map["BeneficiaryBankBic"]
        beneficiaryBankLoroAccountNo <- map["BeneficiaryBankLoroAccountNo"]
        type <- map["Type"]
        operationID <- map["OperationID"]
        isSchedule <- map["IsSchedule"]
        isTemplate <- map["IsTemplate"]
        templateName <- map["TemplateName"]
        templateDescription <- map["TemplateDescription"]
        scheduleID <- map["ScheduleID"]
        schedule <- map["Schedule"]
        confirmType <- map["ConfirmType"]
        additionalInfo <- map["AdditionalInfo"]
        receiverAddress <- map["ReceiverAddress"]
        receiverCountryCode <- map["ReceiverCountryCode"]
        isCommissionAgreementChecked <- map["IsCommissionAgreementChecked"]
        swiftDocuments <- map["Documents"]
    }
}

// модель swiftTransfersModel
open class SwiftTransfersModel: Mappable {
    
    open
    var paginationInfo: PageInfo?,
    result: [TransfersSwiftModel]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        paginationInfo <- map["PaginationInfo"]
        result <- map["Result"]
        
    }
}

// модель для TransfersSwiftModel
open class TransfersSwiftModel: Mappable {
    open var
    operationID: Int?,
    directionTypeID: Int?,
    transferID: Int?,
    numberPP: String?,
    datePP: String?,
    state: String?,
    dateVal: String?,
    paymentCode: String?,
    currency: String?,
    currencyID: Int?,
    accountNo: String?,
    transferSum: Double?,
    receiverBankBic: String?,
    receiverBankCountryCode: String?,
    receiverBankLoroAccountNo: String?,
    intermediaryBankLoroAccountNo: String?,
    receiverAccountNo: String?,
    detailsOfPayment: String?,
    commissions: [Commissions]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        directionTypeID <- map["DirectionTypeID"]
        transferID <- map["TransferID"]
        numberPP <- map["NumberPP"]
        datePP <- map["DatePP"]
        state <- map["State"]
        dateVal <- map["DateVal"]
        paymentCode <- map["PaymentCode"]
        currency <- map["Currency"]
        currencyID <- map["CurrencyID"]
        accountNo <- map["AccountNo"]
        transferSum <- map["TransferSum"]
        receiverBankBic <- map["ReceiverBankBic"]
        receiverBankCountryCode <- map["ReceiverBankCountryCode"]
        receiverBankLoroAccountNo <- map["ReceiverBankLoroAccountNo"]
        intermediaryBankLoroAccountNo <- map["IntermediaryBankLoroAccountNo"]
        receiverAccountNo <- map["ReceiverAccountNo"]
        detailsOfPayment <- map["DetailsOfPayment"]
        commissions <- map["Commissions"]
        
        
    }
}

// Модель для получения БИК кода
open class Commissions: Mappable {
    
    open
    var
    name: String?,
    amount: Int?,
    currency: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        name <- map["Name"]
        amount <- map["Amount"]
        currency <- map["Currency"]
       
    }
}

open class SwiftDocument: Mappable {
    open var
    fileName: String?,
    fileBody: String?,
    contentType: String?
    
    init(){}
    required public init(fileName: String?,
                         fileBody: String?,
                         contentType: String?
        ){
        self.fileName = fileName
        self.fileBody = fileBody
        self.contentType = contentType
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        fileName <- map["FileName"]
        fileBody <- map["FileBody"]
        contentType <- map["ContentType"]
    }
}
