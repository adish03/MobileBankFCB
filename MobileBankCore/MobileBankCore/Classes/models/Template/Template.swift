//
//  Template.swift
//  Alamofire
//
//  Created by Oleg Ten on 4/16/19.
//
import ObjectMapper
import Darwin

 // Модель для описания шаблонов
open class Template: Mappable {
    
    open
    var paginationInfo: PaginationInfo?,
    result: [ContractModelTemplate]?,
    state: ExecuteState?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        paginationInfo <- map["PaginationInfo"]
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}

// Модель для описания ContractModel
open class ContractModelTemplate: Mappable {
    open
    var operationID: Int?,
    name: String?,
    operationType: String?,
    isSchedule: Bool?,
    isLock: Bool?,
    amount: Double?,
    currencyID: Int?,
    from: String?,
    to: String?,
    schedule: Schedule?,
    operationTypeID: Int?,
    operationParameters: [OperationParameters]?,
    comment: String?,
    imageName: String?,
    description: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        name <- map["Name"]
        operationType <- map["OperationType"]
        isSchedule <- map["IsSchedule"]
        isLock <- map["IsLock"]
        amount <- map["Amount"]
        currencyID <- map["CurrencyID"]
        from <- map["From"]
        to <- map["To"]
        schedule <- map["Schedule"]
        operationTypeID <- map["OperationTypeID"]
        operationParameters <- map["OperationParameters"]
        comment <- map["Comment"]
        imageName <- map["ImageName"]
        description <- map["Description"]
       
    }
}
//  Модель для описания расписания
open class Schedule: Mappable {
    
    open
    var operationID: Int?,
    scheduleID: Int?,
    startDate: String?,
    endDate: String?,
    recurringTypeID: Int?,
    processDay: Int?
    
    init(){}
    required public init(operationID: Int,
                         startDate: String?,
                         endDate: String?,
                         recurringTypeID: Int?,
                         processDay: Int?
        ){
        self.operationID = operationID
        self.startDate = startDate
        self.endDate = endDate
        self.recurringTypeID = recurringTypeID
        self.processDay = processDay
        
        
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        scheduleID <- map["ScheduleID"]
        startDate <- map["StartDate"]
        endDate <- map["EndDate"]
        recurringTypeID <- map["RecurringTypeID"]
        processDay <- map["ProcessDay"]
        
    }
}

//  Модель для описания расписания
open class OperationParameters: Mappable {
    
    open
    var key: String?,
    value: Int?
    
    init(){}
    required public init?(map: Map) {}

    public func mapping(map: Map) {
        key <- map["Key"]
        value <- map["Value"]
        
    }
}
//  Модель для pdf чека операции
open class DocumentTemplate: Mappable {
    
    open
    var reportDocuments:[Documents]?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        reportDocuments <- map["ReportDocuments"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        
    }
}
//  Модель для pdf
open class Documents: Mappable {
    
    open
    var fileName: String?,
    fileBody: String?,
    contentType: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        fileName <- map["FileName"]
        fileBody <- map["FileBody"]
        contentType <- map["ContentType"]
        
    }
}

// model for moneySystem xFinca
open class MoneySystem: Mappable {

    open
    var systemId: Int?,
        moneySystemName: String?,
        isOutsideCountrySystem: Bool?,
        nationalBankName: String?,
        operatorName: String?,
        contragentBankID: Int?,
        isActive: Bool?,
        isIBReceivingAllowed: Bool?,
        isIBSendingAllowed: Bool?
    
    init(){}
    required public init(systemId: Int?,
                         moneySystemName: String?,
                         isOutsideCountrySystem: Bool?,
                         nationalBankName: String?,
                         operatorName: String?,
                         contragentBankID: Int?,
                         isActive: Bool?,
                         isIBReceivingAllowed: Bool?,
                         isIBSendingAllowed: Bool?){
        self.systemId = systemId
        self.moneySystemName = moneySystemName
        self.isOutsideCountrySystem = isOutsideCountrySystem
        self.nationalBankName = nationalBankName
        self.operatorName = operatorName
        self.contragentBankID = contragentBankID
        self.isActive = isActive
        self.isIBReceivingAllowed = isIBReceivingAllowed
        self.isIBSendingAllowed = isIBSendingAllowed
    }
    required public init?(map: Map) {}
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = MoneySystem(systemId: systemId, moneySystemName: moneySystemName, isOutsideCountrySystem: isOutsideCountrySystem, nationalBankName: nationalBankName, operatorName: operatorName, contragentBankID: contragentBankID, isActive: isActive, isIBReceivingAllowed: isIBReceivingAllowed, isIBSendingAllowed: isIBSendingAllowed)
        return copy
    }
    
    public func mapping(map: Map) {
        systemId <- map["SystemID"]
        moneySystemName <- map["MoneySystemName"]
        isOutsideCountrySystem <- map["IsOutsideCountrySystem"]
        nationalBankName <- map["NationalBankName"]
        operatorName <- map["OperatorName"]
        contragentBankID <- map["ContragentBankID"]
        isActive <- map["IsActive"]
        isIBReceivingAllowed <- map["IsIBReceivingAllowed"]
        isIBSendingAllowed <- map["IsIBSendingAllowed"]
    }
}
// model FindTransferResponseModel
open class FindTransferResponseModel: Mappable {
    open
    var senderCustomName: String?,
        commission: Double?,
        senderCountryId: Int?,
        state: ExecuteState?,
        message: String?,
        messageCode: String?,
        isOk: Bool?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        senderCustomName <- map["SenderCustomerName"]
        commission <- map["Commission"]
        senderCountryId <- map["SenderCountryId"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
        isOk <- map["IsOk"]
    }
}

open class ConversionSumResult: Mappable {
    open
    var debetSum: Double?,
        debetSumN: Double?,
        creditSum: Double?,
        creditSumN: Double?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        debetSum <- map["DebetSum"]
        debetSumN <- map["DebetSumN"]
        creditSum <- map["CreditSum"]
        creditSumN <- map["CreditSumN"]
    }
}

open class CreateMoneyTransfer: Mappable {
    open
    var operationType: Int?,
        moneySystemID: Int?,
        directionTypeID: Int?,
        transferCurrencyID: Int?,
        transferSumm: Double?,
        contragentCountryID: Int?,
        goldenCrownContragentCountryID: Int?,
        goldenCrownContragentCityID: Int?,
        goldenCrownContactPhone: String?,
        transferDate: String?,
        receiverCustomerID: Int?,
        receiverCustomerName: String?,
        senderCustomerID: Int?,
        senderCustomerName: String?,
        paymentCode: String?,
        paymentComment: String?,
        bankCommission: Double?,
        totalCommission: Double?,
        convertedTransferSum: Double?,
        transferAccountNo: String?,
        transferAccountCurrencyID: Int?
    
    public init(operationType: Int?,
                moneySystemID: Int?,
                directionTypeID: Int?,
                transferCurrencyID: Int?,
                transferSumm: Double?,
                contragentCountryID: Int?,
                goldenCrownContragentCountryID: Int?,
                goldenCrownContragentCityID: Int?,
                goldenCrownContactPhone: String?,
                receiverCustomerID: Int?,
                receiverCustomerName: String?,
                senderCustomerID: Int?,
                senderCustomerName: String?,
                transferDate: String?,
                paymentCode: String?,
                paymentComment: String?,
                bankCommission: Double?,
                totalCommission: Double?,
                convertedTransferSum: Double?,
                transferAccountNo: String?,
                transferAccountCurrencyID: Int?){
        self.operationType = operationType
        self.transferSumm = transferSumm
        self.moneySystemID = moneySystemID
        self.directionTypeID = directionTypeID
        self.transferCurrencyID = transferCurrencyID
        self.contragentCountryID = contragentCountryID
        self.goldenCrownContragentCountryID = goldenCrownContragentCountryID
        self.goldenCrownContragentCityID = goldenCrownContragentCityID
        self.goldenCrownContactPhone = goldenCrownContactPhone
        self.receiverCustomerName = receiverCustomerName
        self.receiverCustomerID = receiverCustomerID
        self.senderCustomerID = senderCustomerID
        self.senderCustomerName = senderCustomerName
        self.transferDate = transferDate
        self.paymentCode = paymentCode
        self.paymentComment = paymentComment
        self.bankCommission = bankCommission
        self.totalCommission = totalCommission
        self.convertedTransferSum = convertedTransferSum
        self.transferAccountCurrencyID = transferAccountCurrencyID
        self.transferAccountNo = transferAccountNo
        self.transferCurrencyID = transferCurrencyID
    }
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationType <- map["OperationType"]
        moneySystemID <- map["MoneySystemID"]
        directionTypeID <- map["DirectionTypeID"]
        transferCurrencyID <- map["TransferCurrencyID"]
        transferSumm <- map["TransferSumm"]
        contragentCountryID <- map["ContragentCountryID"]
        goldenCrownContragentCountryID <- map["GoldenCrownContragentCountryID"]
        goldenCrownContragentCityID <- map["GoldenCrownContragentCityID"]
        goldenCrownContactPhone <- map["GoldenCrownContactPhone"]
        transferDate <- map["TransferDate"]
        receiverCustomerID <- map["ReceiverCustomerID"]
        receiverCustomerName <- map["ReceiverCustomerName"]
        senderCustomerID <- map["SenderCustomerID"]
        senderCustomerName <- map["SenderCustomerName"]
        paymentCode <- map["PaymentCode"]
        paymentComment <- map["PaymentComment"]
        bankCommission <- map["BankCommission"]
        totalCommission <- map["TotalCommission"]
        convertedTransferSum <- map["ConvertedTransferSum"]
        transferAccountNo <- map["TransferAccountNo"]
        transferAccountCurrencyID <- map["TransferAccountCurrencyID"]
    }
}

public struct CheckModel: Decodable {
    public let fullName: String?
    public let paymentSum: String?
    public let message: String?
}

public struct PaymentModel: Decodable {
    public let Governments: [Government]?
    public let Departments: [Department]?
    public let Services: [Service]?
    public let Regions: [Region]?
    public let Districts: [District]?
    public let PayerTypes: [PayerType]?
    public let PaymentTypes: [PaymentType]?
    public let Comissions: [Commission]?
}

public struct Government: Decodable {
    public let GovernmentID: Int?
    public let GovernmentName: String?
    public let GovernmentCode: String?
}

public struct Department: Decodable {
    public let DepartmentID: Int?
    public let DepartmentName: String?
    public let DepartmentAddress: String?
    public let TakeUserCommentAsClearingComment: Bool?
    public let IsBudgetPayment: Bool?
    public let GovernmentID: Int?
    public let TaxDistrictID: Int?
}

public struct TaxDepartment: Decodable {
    public let DepartmentID: Int?
    public let TaxDistrictID: Int?
    public let DepartmentName: String?
    public let IsOkmotCodeRequired: Bool?
    public let Comissions: [Commission]?
}

public struct Service: Decodable {
    public let CustomerTypeID: String?
    public let ServiceID: Int?
    public let ServiceName: String?
    public let ShortServiceName: String?
    public let PaymentCode: String?
    public let PaymentTypeID: Int?
    public let PayerTypeID: Int?
    public let TaxCode: String?
}

public struct PaymentType: Decodable {
    public let Key: Int?
    public let Value: String?
}
public struct PayerType: Decodable {
    public let Key: Int?
    public let Value: String?
}
public struct Region: Decodable {
    public let Key: Int?
    public let Value: String?
}
public struct District: Decodable {
    public let DistrictCode: String?
    public let DistrictID: Int?
    public let DistrictName: String?
    public let RegionID: Int?
    public let TaxDepartments: [TaxDepartment]?
}
public struct Commission: Decodable {
    public let Comission: Double?
    public let ComissionType: Int?
    public let OfficeID: Int?
    public let DepartmentID: Int?
    public let EndSumm: Double?
    public let StartSumm: Double?
}

//
//open class CreateMoneyTransfer: Mappable {
//    open
//    var operationType: Int?,
//        moneySystemID: Int?,
//        directionTypeID: Int?,
//        transferCurrencyID: Int?,
//        transferSumm: Double?,
//        contragentCountryID: Int?,
//        goldenCrownContragentCountryID: Int?,
//        goldenCrownContragentCityID: Int?,
//        goldenCrownContactPhone: String?,
//        transferDate: String?,
//        receiverCustomerID: Int?,
//        receiverCustomerName: String?,
//        senderCustomerID: Int?,
//        senderCustomerName: String?,
//        paymentCode: String?,
//        paymentComment: String?,
//        bankCommission: Double?,
//        totalCommission: Double?,
//        convertedTransferSum: Double?,
//        transferAccountNo: String?,
//        transferAccountCurrencyID: Int?
//
//    public init(operationType: Int?,
//                moneySystemID: Int?,
//                directionTypeID: Int?,
//                transferCurrencyID: Int?,
//                transferSumm: Double?,
//                contragentCountryID: Int?,
//                goldenCrownContragentCountryID: Int?,
//                goldenCrownContragentCityID: Int?,
//                goldenCrownContactPhone: String?,
//                receiverCustomerID: Int?,
//                receiverCustomerName: String?,
//                senderCustomerID: Int?,
//                senderCustomerName: String?,
//                transferDate: String?,
//                paymentCode: String?,
//                paymentComment: String?,
//                bankCommission: Double?,
//                totalCommission: Double?,
//                convertedTransferSum: Double?,
//                transferAccountNo: String?,
//                transferAccountCurrencyID: Int?){
//        self.operationType = operationType
//        self.transferSumm = transferSumm
//        self.moneySystemID = moneySystemID
//        self.directionTypeID = directionTypeID
//        self.transferCurrencyID = transferCurrencyID
//        self.contragentCountryID = contragentCountryID
//        self.goldenCrownContragentCountryID = goldenCrownContragentCountryID
//        self.goldenCrownContragentCityID = goldenCrownContragentCityID
//        self.goldenCrownContactPhone = goldenCrownContactPhone
//        self.receiverCustomerName = receiverCustomerName
//        self.receiverCustomerID = receiverCustomerID
//        self.senderCustomerID = senderCustomerID
//        self.senderCustomerName = senderCustomerName
//        self.transferDate = transferDate
//        self.paymentCode = paymentCode
//        self.paymentComment = paymentComment
//        self.bankCommission = bankCommission
//        self.totalCommission = totalCommission
//        self.convertedTransferSum = convertedTransferSum
//        self.transferAccountCurrencyID = transferAccountCurrencyID
//        self.transferAccountNo = transferAccountNo
//        self.transferCurrencyID = transferCurrencyID
//    }
//
//    init(){}
//    required public init?(map: Map) {}
//    public func mapping(map: Map) {
//        operationType <- map["OperationType"]
//        moneySystemID <- map["MoneySystemID"]
//        directionTypeID <- map["DirectionTypeID"]
//        transferCurrencyID <- map["TransferCurrencyID"]
//        transferSumm <- map["TransferSumm"]
//        contragentCountryID <- map["ContragentCountryID"]
//        goldenCrownContragentCountryID <- map["GoldenCrownContragentCountryID"]
//        goldenCrownContragentCityID <- map["GoldenCrownContragentCityID"]
//        goldenCrownContactPhone <- map["GoldenCrownContactPhone"]
//        transferDate <- map["TransferDate"]
//        receiverCustomerID <- map["ReceiverCustomerID"]
//        receiverCustomerName <- map["ReceiverCustomerName"]
//        senderCustomerID <- map["SenderCustomerID"]
//        senderCustomerName <- map["SenderCustomerName"]
//        paymentCode <- map["PaymentCode"]
//        paymentComment <- map["PaymentComment"]
//        bankCommission <- map["BankCommission"]
//        totalCommission <- map["TotalCommission"]
//        convertedTransferSum <- map["ConvertedTransferSum"]
//        transferAccountNo <- map["TransferAccountNo"]
//        transferAccountCurrencyID <- map["TransferAccountCurrencyID"]
//    }
//}
