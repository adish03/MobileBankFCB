//
//  Utilities.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/22/19.
//
import ObjectMapper
import Kingfisher

// модель Categories
open class Categories: Mappable {
    
    open var
    items: [CategoriesType]?,
    date: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        items <- map["Items"]
        date <- map["Date"]
    }
}
// модель CategoriesType
open class CategoriesType: Mappable {
    
    open var
    categoryID:Int?,
    categoryName: String?,
    logoFileName: String?,
    parentCategoryID: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        categoryID <- map["CategoryID"]
        categoryName <- map["CategoryName"]
        logoFileName <- map["LogoFileName"]
        parentCategoryID <- map["ParentCategoryID"]
    }
}


// модель Utilities
open class Utilities: Mappable {
    
    open var
    items: [CategoriesById]?,
    date: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        items <- map["Items"]
        date <- map["Date"]
    }
}
// модель CategoriesById
open class CategoriesById: Mappable {
    
    open var
    categoryID: Int?,
    items: [UtilityModel]?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        categoryID <- map["CategoryID"]
        items <- map["Items"]
        
    }
}
// модель UtilityModel
open class UtilityModel: Mappable {
    
    open var
    ID:Int?,
    name: String?,
    logo: String?,
    parentID:Int?,
    inputValidate: String?,
    placeholder: String?,
    categoryID: Int?,
    currencyID: Int?,
    min: Double?,
    max: Double?,
    userRecomendation: String?,
    taxProviderTypeID: Int?,
    children: [UtilityModel]?,
    
    category: Utilities?
    
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        ID <- map["ID"]
        name <- map["Name"]
        logo <- map["Logo"]
        parentID <- map["ParentID"]
        inputValidate <- map["InputValidate"]
        placeholder <- map["Placeholder"]
        currencyID <- map["CurrencyID"]
        min <- map["Min"]
        max <- map["Max"]
        logo <- map["Logo"]
        userRecomendation <- map["UserRecomendation"]
        taxProviderTypeID <- map["TaxProviderTypeID"]
        children <- map["Children"]
    }
   
}

open class ScenarioFieldValueModel: Mappable {
    open var
    fieldID: Int?,
    value: String?
    
    init(){}
    required public init(fieldID: Int?,
                         value: String?) {
        self.fieldID = fieldID
        self.value = value
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        fieldID <- map["FieldID"]
        value <- map["Value"]
    }
}

// модель для UtilityPaymentOperationModel
open class UtilityPaymentOperationModel: Mappable {
    
    open var
    accountNo:String?,
    currencyID: Int?,
    providerID: Int?,
    contragentID:Int?,
    personalAccountNo: String?,
    сurrentAccountNo: String?,
    fields: [ScenarioFieldValueModel]?,
    sum: Double?,
    paymentSum: Double?,
    commission: Double?,
    taxDepartmentID: Int?,
    serviceID: Int?,
    governmentCode: String?,
    payerTypeID: Int?,
    paymentTypeID: Int?,
    total: Double?,
    departmentID: Int?,
    paymentCode: String?,
    OKPOCode: String?,
    taxServiceID: Int?,
    comment: String?,
    operationType:Int?,
    operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName:String?,
    templateDescription: String?,
    scheduleDate: String?,
    scheduleID: Int?,
    schedule: Schedule?,
    fullName: String?
    
    init(){}
    required public init(providerID: Int,
                         personalAccountNo: String,
                         contragentID: Int?,
                         sum: Double,
                         fields: [ScenarioFieldValueModel]?){
        self.providerID = providerID
        self.personalAccountNo = personalAccountNo
        self.contragentID = contragentID
        self.sum = sum
        self.fields = fields
    }
    required public init(accountNo: String,
                         currencyID: Int,
                         providerID: Int,
                         contragentID:Int?,
                         personalAccountNo: String,
                         сurrentAccountNo: String?,
                         sum: Double,
                         paymentSum: Double?,
                         
                         commission: Double?,
                         taxDepartmentID: Int?,
                         departmentID: Int?,
                         OKPOCode: String?,
                         taxServiceID: Int?,
                         
                         comment: String?,
                         operationID: Int?,
                         isSchedule: Bool?,
                         isTemplate: Bool?,
                         templateName: String?,
                         templateDescription: String?,
                         scheduleID: Int?,
                         schedule: Schedule?,
                         fullName: String?,
                         fields: [ScenarioFieldValueModel]?){
        
        self.accountNo = accountNo
        self.currencyID  = currencyID
        self.providerID = providerID
        self.contragentID = contragentID
        self.personalAccountNo = personalAccountNo
        self.сurrentAccountNo = сurrentAccountNo
        self.sum = sum
        self.paymentSum = paymentSum
        self.commission = commission
        self.taxDepartmentID = taxDepartmentID
        self.total = sum + (commission ?? 0)
        self.departmentID = departmentID
        self.OKPOCode = OKPOCode
        self.taxServiceID = taxServiceID
        self.comment = comment
        self.operationID = operationID
        self.isSchedule = isSchedule
        self.isTemplate = isTemplate
        self.templateName = templateName
        self.templateDescription = templateDescription
        self.scheduleID = scheduleID
        self.schedule = schedule
        self.fullName = fullName
        self.fields = fields
    }
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        providerID <- map["ProviderID"]
        contragentID <- map["ContragentID"]
        personalAccountNo <- map["PersonalAccountNo"]
        сurrentAccountNo <- map["CurrentAccountNo"]
        sum <- map["Sum"]
        serviceID <- map["ServiceID"]
        paymentCode <- map["PaymentCode"]
        paymentSum <- map["PaymentSumm"]
        commission <- map["Commission"]
        taxDepartmentID <- map["TaxDepartmentID"]
        governmentCode <- map["GovernmentCode"]
        payerTypeID <- map["PayerTypeID"]
        total <- map["Total"]
        paymentTypeID <- map["PaymentTypeID"]
        departmentID <- map["DepartmentID"]
        OKPOCode <- map["OKPOCode"]
        taxServiceID <- map["TaxServiceID"]
        comment <- map["Comment"]
        operationID <- map["OperationID"]
        isSchedule <- map["IsSchedule"]
        isTemplate <- map["IsTemplate"]
        templateName <- map["TemplateName"]
        templateDescription <- map["TemplateDescription"]
        scheduleDate <- map["ScheduleDate"]
        scheduleID <- map["ScheduleID"]
        schedule <- map["Schedule"]
        fullName <- map["FullName"]
        fields <- map["Fields"]
    }
}

public enum TaxProviderType: Int {
    case Default = 1
    case Taxes = 2
    case Penalties = 3
    case TransportTax = 4
}

// модель DecimalExecuteResult
open class DecimalExecuteResult: Mappable {
    
    open var
    result: Double?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}


open class TermsTypeDescriptions: Mappable {
    open var Key : Int?
    open var value : String?

    public required init?(map: Map) {

    }

    public func mapping(map: Map) {
        Key <- map["Key"]
        value <- map["Value"]
    }
}


open class TermsTypeData : Mappable {
    open var termsTypeDescriptions : [TermsTypeDescriptions]?
    open var result : [ResultDataTerm]?
    open var state : Int?
    open var message : String?
    open var messageCode : String?

    public required init?(map: Map) {

    }

    public func mapping(map: Map) {

        termsTypeDescriptions <- map["TermsTypeDescriptions"]
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }

}

open class ResultDataTerm : Mappable {
    open var documentID : Int?
    open var roleCustomerTypeID : Int?
    open var termsTypeID : Int?
    open var fileName : String?
    open var toPdf : Bool?

    public required init?(map: Map) {

    }

    public func mapping(map: Map) {

        documentID <- map["DocumentID"]
        roleCustomerTypeID <- map["RoleCustomerTypeID"]
        termsTypeID <- map["TermsTypeID"]
        fileName <- map["FileName"]
        toPdf <- map["ToPdf"]
    }

}


open class StringExecuteResult: Mappable {
    open var
    result: String?,
    state: Int?,
    message: String?,
    messageCode: String?
    
    init(){}
    public required init?(map: Map) {}
    
    public func mapping(map: Map) {
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}
// модель SubmitForPayModel
open class SubmitForPayModel {
    
    open var
    name: String,
    props: String,
    account: String,
    contract: String,
    sum: Double,
    commission: String,
    total: String,
    description: String,
    comment: String?,
    image: String?,
    currencyID: Int
    
    required public init(name:String,
                         props: String,
                         account: String,
                         contract: String,
                         sum: Double,
                         commission: String,
                         total: String,
                         description: String,
                         comment: String?,
                         image: String?,
                         currencyID: Int){
        self.name = name
        self.props = props
        self.account = account
        self.contract = contract
        self.sum = sum
        self.commission = commission
        self.total = total
        self.description = description
        self.comment = comment
        self.image = image
        self.currencyID = currencyID
    }
}
// модель ExportStatementModel
open class ExportStatementModel: Mappable {
    
    open var
    accountNo: String?,
    currencyID: Int?,
    startDate: String?,
    endDate: String?,
    toExcel: Bool?
    
    
    init(){}
    required public init( accountNo: String,
                          currencyID: Int,
                          startDate: String,
                          endDate: String,
                          toExcel: Bool){
        self.accountNo = accountNo
        self.currencyID = currencyID
        self.startDate = startDate
        self.endDate = endDate
        self.toExcel = toExcel
    }
    
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        accountNo <- map["accountNo"]
        currencyID <- map["currencyID"]
        startDate <- map["startDate"]
        endDate <- map["endDate"]
        toExcel <- map["toExcel"]
    }
}



// модель OperationModel
open class OperationModel: Mappable {
    
    open var
    operationID: Int?,
    code: String?,
    operationTypeID: Int?
    
    
    init(){}
    required public init(operationID:Int,
                         code: String,
                         operationTypeID: Int){
        self.operationID = operationID
        self.code = code
        self.operationTypeID = operationTypeID
    }
    required public init(operationID:Int){
        self.operationID = operationID
    }
    
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        code <- map["Code"]
        operationTypeID <- map["OperationTypeID"]
    }
}

open class UtilityPaymentModel: Mappable {
    open var
    providerID: Int?,
    paymentModel: String?
    
    required public init?(map: Map) {}
    
    required public init(
        providerID: Int,
        paymentModel: String
    ){
        self.providerID = providerID
        self.paymentModel = paymentModel
    }
    
    public func mapping(map: Map) {
        providerID <- map["ProviderID"]
        paymentModel <- map["PaymentModel"]
    }
}

open class ScenarioFieldModel: Mappable {
    open var
    fieldID: Int?,
    parameterName: String?,
    nameToDisplay: String?,
    isForCheck: Bool?,
    isForPay: Bool?,
    isLetterAllowed: Bool?,
    isNumberAllowed: Bool?,
    isSymbolAllowed: Bool?
    
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        fieldID <- map["FieldID"]
        parameterName <- map["ParameterName"]
        nameToDisplay <- map["NameToDisplay"]
        isForCheck <- map["IsForCheck"]
        isLetterAllowed <- map["IsLetterAllowed"]
        isNumberAllowed <- map["IsNumberAllowed"]
        isSymbolAllowed <- map["isSymbolAllowed"]
    }
}

open class AdditionalData: Mappable {
    open var
    providerID: Int?,
    personalAccountNo: String?,
    governmentCode: String?,
    paymentCode: String?,
    OKPOCode: String?,
    departmentID: Int?,
    taxDepartmentID: Int?,
    accountNo: String?,
    currencyID: Int?,
    contragentID: Int?,
    sum: Double?,
    comment: String?,
    createDate: String?,
    serviceID: Int?,
    customerName: String?,
    paymentTypeID: Int?,
    commission: Double?,
    total: Double?,
    fields: [ScenarioFieldValueModel]?
    
    required public init?(map: Map) {}
    required public init(    providerID: Int?,
                             personalAccountNo: String?,
                             governmentCode: String?,
                             paymentCode: String?,
                             OKPOCode: String?,
                             departmentID: Int?,
                             taxDepartmentID: Int?,
                             accountNo: String?,
                             currencyID: Int?,
                             contragentID: Int?,
                             sum: Double?,
                             comment: String?,
                             createDate: String?,
                             serviceID: Int?,
                             customerName: String?,
                             paymentTypeID: Int?,
                             commission: Double?,
                             total: Double?,
                             fields: [ScenarioFieldValueModel]?) {
        self.providerID = providerID
        self.personalAccountNo = personalAccountNo
        self.governmentCode = governmentCode
        self.paymentCode = paymentCode
        self.OKPOCode = OKPOCode
        self.departmentID = departmentID
        self.taxDepartmentID = taxDepartmentID
        self.accountNo = accountNo
        self.currencyID = currencyID
        self.contragentID = contragentID
        self.sum = sum
        self.comment = comment
        self.createDate = createDate
        self.serviceID = serviceID
        self.customerName = customerName
        self.paymentTypeID = paymentTypeID
        self.commission = commission
        self.total = total
        self.fields = fields
    }
    
    public func mapping(map: Map) {
        providerID <- map["ProviderID"]
        personalAccountNo <- map["PersonalAccountNo"]
        governmentCode <- map["GovernmentCode"]
        paymentCode <- map["PaymentCode"]
        OKPOCode <- map["OKPOCode"]
        departmentID <- map["DepartmentID"]
        taxDepartmentID <- map["TaxDepartmentID"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        contragentID <- map["ContragentID"]
        sum <- map["Sum"]
        comment <- map["Comment"]
        createDate <- map["CreateDate"]
        serviceID <- map["ServiceID"]
        customerName <- map["CustomerName"]
        paymentTypeID <- map["PaymentTypeID"]
        commission <- map["Commission"]
        total <- map["total"]
    }
}

open class ResultFileDowload : Mappable {
    open var fileName : String?
    open var fileBody : String?

    required public init?(map: Map) {

    }

    public func mapping(map: Map) {

        fileName <- map["FileName"]
        fileBody <- map["FileBody"]
    }

}

open class DowloadTerms : Mappable {
    open var result : ResultFileDowload?

    required public init?(map: Map) {

    }

    public func mapping(map: Map) {

        result <- map["Result"]
    }

}


open class LimitsModel : Mappable {
    open var iD : Int?
    open var limitName : String?
    open var currencyID : Int?
    open var limitSum : Double?
    open var comment : String?
    open var date : String?
    open  var userID : Int?

    required public init?(map: Map) {

    }

    public func mapping(map: Map) {
        iD <- map["ID"]
        limitName <- map["LimitName"]
        currencyID <- map["CurrencyID"]
        limitSum <- map["LimitSum"]
        comment <- map["Comment"]
        date <- map["Date"]
        userID <- map["UserID"]
    }
}
