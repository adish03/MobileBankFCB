//
//  Enum.swift
//  Alamofire
//
//  Created by Oleg Ten on 3/4/19.
//

import Foundation
// Enum для InternetBankingAccountType
public enum InternetBankingAccountType: Int, CaseIterable  {
    case Current = 0
    case Deposit = 1
    case Card = 2
    case Credit = 3

}
// Enum для InternetBankingOperationType
public enum InternetBankingOperationType: Int, CaseIterable {
    case InternalOperation = 1
    case InternalOperationCustomerAccounts = 12
    case LoanPayment = 2
    case UtilityPayment = 3
    case CliringTransfer = 4
    case GrossTransfer = 5
    case SwiftTransfer = 6
    case Conversion = 7
    case CardOperation = 9
    case MoneyTransfer = 13
   
}
// Enum для PaymentTypeID
public enum PaymentTypeID: Int, CaseIterable {
    case Clearing = 1
    case Gross = 2
}
// Enum для ExecuteState
public enum ExecuteState: Int, CaseIterable  {
    case OK = 0
    case Error = 1
}
// Enum для ScheduleRepeat
public enum ScheduleRepeat: Int, CaseIterable  {
    case OneTime = 1
    case EachWeek = 2
    case EachMonth = 3
}
// Enum для SchedulePickerView
public enum SchedulePickerView: Int, CaseIterable  {
    case EachWeek = 0
    case EachMonth = 1
    case OneTime = 2
}
// Enum для ScheduleWeekDay
public enum ScheduleWeekDay: Int, CaseIterable {
    case monday = 1
    case tuesday = 2
    case wednesday = 3
    case thursday = 4
    case friday = 5
    case saturday = 6
    case sunday = 7
   
}
// Enum для ConfirmType
public enum ConfirmType: Int, CaseIterable {
    case Save = 1
    case Confirm = 2
    case InConfirm = 3
    case Error = 4
    case Verified = 6
}

// Enum для InternetBankingLoanPaymentType
public enum InternetBankingLoanPaymentType: Int, CaseIterable {
    case OnSchedule = 0  //Внутренняя операция
    case FullEarlyPayment = 1 //Полное досрочное погашение
    case PartialEarlyPayment = 2 //Погашение кредита
    case PartialEarlyPaymentMainDeptOnly = 3 //Частичное досрочное погашение только основного долга
}

// Enum для ValidatePin
public enum ValidateStatePin: Int, CaseIterable {
    case Ok = 0
    case Error = 1
    case Blocked = 2
}

// Enum для ValidatePin
public enum StatusType: Int, CaseIterable {
    public typealias RawValue = Int
    
    case inProgress = 0
    case Ok = 1
    case Approve = 2
    case Okey = 3
    case Denied = 4
    case Issued = 5 //выдан
    case Closed = 6
}

public enum PasswordStrengthType : Int, CaseIterable {
    //    [EnumDescription("Пароль может не достигать минимальной длины")]
    //    [Description("Пароль может не достигать минимальной длины")]
    case Lowest = 1
    
    //    [EnumDescription("Пароль должен достигать минимальной длины")]
    //    [Description("Пароль должен достигать минимальной длины")]
    case VeryWeak = 2
    
    //    [EnumDescription("Пароль должен достигать минимальной длины, содержать буквы верх. и нижн. регистра")]
    //    [Description("Пароль должен достигать минимальной длины, содержать буквы верх. и нижн. регистра")]
    case Weak = 3
    
    //    [EnumDescription("Пароль должен достигать минимальной длины, содержать цифры,  буквы верх. и нижн. регистра")]
    //    [Description("Пароль должен достигать минимальной длины, содержать цифры,  буквы верх. и нижн. регистра")]
    case Medium = 4
    
    //    [EnumDescription("Пароль должен достигать минимальной длины, содержать спец. символы, цифры,  буквы верх. и нижн. регистра")]
    //    [Description("Пароль должен достигать минимальной длины, содержать спец. символы, цифры,  буквы верх. и нижн. регистра")]
    case High = 5
}
