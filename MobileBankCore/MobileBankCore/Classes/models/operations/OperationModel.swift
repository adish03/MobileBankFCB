//
//  OperationModel.swift
//  Alamofire
//
//  Created by Oleg Ten on 4/25/19.
//

import ObjectMapper

// Модель для описания операций
open class CommandOperationModelExecuteResult: Mappable {
    open
    var result: BaseOperationModel?,
    state: ExecuteState?,
    message: String?,
    messageCode: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        result <- map["Result"]
        state <- map["State"]
        message <- map["Message"]
        messageCode <- map["MessageCode"]
    }
}

// Модель для описания ContractModel
open class BaseOperationModel: Mappable {
    open
    var operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName: String?,
    templateDescription: String?,
    scheduleDate: String?,
    scheduleID: Int?,
    schedule: Schedule?,
    operationType: Int?,
    confirmType: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
        isSchedule <- map["IsSchedule"]
        isTemplate <- map["IsTemplate"]
        templateName <- map["TemplateName"]
        templateDescription <- map["TemplateDescription"]
        scheduleDate <- map["ScheduleDate"]
        scheduleID <- map["ScheduleID"]
        schedule <- map["Schedule"]
        operationType <- map["OperationType"]
        confirmType <- map["ConfirmType"]
       
    }
}

open class BaseCreateOperation: Mappable{
    open var
    operationID: Int?
    
    init(){}
    required public init(operationID: Int){
        self.operationID = operationID
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        operationID <- map["OperationID"]
    }
}

// Модель для описания Модели операции
open class ModelOperation: Mappable {
    open
    var personalAccountNo: String?,
    comment: String?,
    service: String?,
    category: String?,
    categoryID: Int?,
    accountNo: String?,
    currencyID: Int?,
    providerID: Int?,
    contragentID: Int?,
    sum: Double?,
    userComment: String?,
    userTin: String?,
    userAddress: String?,
    taxProviderTypeID: Int?,
    commission: Double?,
    total: Double?,
    operationType: Int?,
    operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName: String?,
    templateDescription: String?,
    scheduleDate: Schedule?,
    scheduleID: Int?,
    schedule: String?,
    confirmType: Int?,
    fields: [ScenarioFieldValueModel]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        personalAccountNo <- map["PersonalAccountNo"]
        comment <- map["Comment"]
        service <- map["Service"]
        category <- map["Category"]
        categoryID <- map["CategoryID"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        providerID <- map["ProviderID"]
        contragentID <- map["ContragentID"]
        sum <- map["Sum"]
        userComment <- map["UserComment"]
        userTin <- map["UserTin"]
        userAddress <- map["UserAddress"]
        taxProviderTypeID <- map["TaxProviderTypeID"]
        commission <- map["Commission"]
        total <- map["Total"]
        operationType <- map["OperationType"]
        operationID <- map["OperationID"]
        isSchedule <- map["IsSchedule"]
        isTemplate <- map["IsTemplate"]
        templateName <- map["TemplateName"]
        templateDescription <- map["TemplateDesription"]
        scheduleDate <- map["ScheduleDate"]
        scheduleID <- map["ScheduleID"]
        schedule <- map["Schedule"]
        confirmType <- map["ConfirmType"]
        fields <- map["Fields"]
    }
}



