//
//  AccountGroups.swift
//  Alamofire
//
//  Created by Oleg Ten on 3/4/19.
//

import ObjectMapper
// модель для списка счетов
open class AccountGroup: Mappable {
    
    open
    var text: String?,
    accountType: InternetBankingAccountType?,
    items: [BaseAccountDataModel]?
   
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        text <- map["Text"]
        accountType <- map["AccountType"]
    }
}
// модель для счета
open class BaseAccountDataModel: Mappable {
    
    open
    var openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: Bool?,
    name: String?,
    accountNo: String?,
    currencyID: Int?,
    currencySymbol: String?

    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        name <- map["Name"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        currencySymbol <- map["CurrencySymbol"]
    }
}
// модель для ClearingGross счетов
open class ClearingGrossOperationModel: Mappable {
    open var
    paymentDate: String?,
    pNumber: String?,
    paymentTypeID: Int?,
    senderFullName: String?,
    receiverFullName: String?,
    receiverBIK: String?,
    senderAccountNo: String?,
    receiverAccountNo: String?,
    paymentComment: String?,
    paymentCode: String?,
    transferSumm: Double?,
    confirmType: Int?,
    operationType: Int?,
    operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName: String?,
    templateDescription: String?,
    scheduleDate: String?,
    scheduleID: Int?,
    schedule: Schedule?
    
    init(){}
    required public init( paymentDate: String?,
                          pNumber: String?,
                          paymentTypeID: Int?,
                          senderFullName: String?,
                          receiverFullName: String?,
                          receiverBIK: String?,
                          senderAccountNo: String?,
                          receiverAccountNo: String?,
                          paymentComment: String?,
                          paymentCode: String?,
                          transferSumm: Double?,
                          operationType: Int?,
                          operationID: Int?,
                          isSchedule: Bool?,
                          isTemplate: Bool?,
                          templateName: String?,
                          templateDescription: String?,
                          scheduleID: Int?,
                          schedule: Schedule?
        ){
        self.paymentDate = paymentDate
        self.pNumber = pNumber
        self.paymentTypeID = paymentTypeID
        self.senderFullName = senderFullName
        self.receiverFullName = receiverFullName
        self.receiverBIK = receiverBIK
        self.senderAccountNo = senderAccountNo
        self.receiverAccountNo = receiverAccountNo
        self.paymentComment = paymentComment
        self.paymentCode = paymentCode
        self.transferSumm = transferSumm
        self.operationType = operationType
        self.operationID = operationID
        self.isSchedule = isSchedule
        self.isTemplate = isTemplate
        self.templateName = templateName
        self.templateDescription = templateDescription
        self.scheduleID = scheduleID
        self.schedule = schedule
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        paymentDate <- map["PaymentDate"]
        pNumber <- map["PNumber"]
        paymentTypeID <- map["PaymentTypeID"]
        senderFullName <- map["SenderFullName"]
        receiverFullName <- map["ReceiverFullName"]
        receiverBIK <- map["ReceiverBIK"]
        senderAccountNo <- map["SenderAccountNo"]
        receiverAccountNo <- map["ReceiverAccountNo"]
        paymentComment <- map["PaymentComment"]
        paymentCode <- map["PaymentCode"]
        transferSumm <- map["TransferSumm"]
        confirmType <- map["ConfirmType"]
        operationType <- map["OperationType"]
        operationID <- map["OperationID"]
        isSchedule <- map["IsSchedule"]
        isTemplate <- map["IsTemplate"]
        templateName <- map["TemplateName"]
        templateDescription <- map["TemplateDescription"]
        scheduleDate <- map["ScheduleDate"]
        scheduleID <- map["ScheduleID"]
        schedule <- map["Schedule"]
      
    }
}
// модель для InternalOperation счетов
open class InternalOperationModel: Mappable {
    
    open
    var internalOperationType: Int?,
    dtAccountNo: String?,
    ctAccountNo: String?,
    currencyID: Int?,
    sum: Double?,
    comment: String?,
    operationType: Int?,
        moneySystemID: Int?,
        directionTypeID: Int?,
        senderCustomerName: String?,
        receiverCustomerName: String?,
        transferCurrencyID: Int?,
        contragentCountryID: Int?,
        totalCommission: Double?,
        transferAccountCurrencyID: Int?,
        paymentCode: String?,
        transferAccountNo: String?,
        paymentComment: String?,
        transferSumm: Double?,
    operationID: Int?,
    isSchedule: Bool?,
    isTemplate: Bool?,
    templateName: String?,
    templateDescription: String?,
    scheduleDate: String?,
    scheduleID: Int?,
    schedule: Schedule?,
    createDate: String?,
    confirmType: Int?
    
    required public init( dtAccountNo: String?,
                          ctAccountNo: String?,
                          operationType: Int?,
                          operationID: Int?,
                          currencyID: Int?,
                          sum: Double?,
                          comment: String?,
                          isSchedule: Bool?,
                          isTemplate: Bool?,
                          templateName: String?,
                          templateDescription: String?,
                          scheduleID: Int?,
                          schedule: Schedule?
        ){
        self.dtAccountNo = dtAccountNo
        self.ctAccountNo = ctAccountNo
        self.operationType = operationType
        self.operationID = operationID
        self.currencyID = currencyID
        self.sum = sum
        self.comment = comment
        self.isSchedule = isSchedule
        self.isTemplate = isTemplate
        self.templateName = templateName
        self.templateDescription = templateDescription
        self.scheduleID = scheduleID
        self.schedule = schedule
       
    }
    required public init( dtAccountNo: String?,
                          ctAccountNo: String?,
                          operationType: Int?,
                          operationID: Int?,
                          currencyID: Int?,
                          sum: Double?,
                          comment: String?
        ){
        self.dtAccountNo = dtAccountNo
        self.ctAccountNo = ctAccountNo
        self.operationType = operationType
        self.operationID = operationID
        self.currencyID = currencyID
        self.sum = sum
        self.comment = comment
        
    }
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        internalOperationType <- map["InternalOperationType"]
        dtAccountNo <- map["DtAccountNo"]
        ctAccountNo <- map["CtAccountNo"]
        currencyID <- map["CurrencyID"]
        sum <- map["Sum"]
        comment <- map["Comment"]
        operationType <- map["OperationType"]
        operationID <- map["OperationID"]
        isSchedule <- map["AccoIsScheduleuntNo"]
        moneySystemID <- map["MoneySystemID"]
        directionTypeID <- map["DirectionTypeID"]
        senderCustomerName <- map["SenderCustomerName"]
        receiverCustomerName <- map["ReceiverCustomerName"]
        transferCurrencyID <- map["TransferCurrencyID"]
        contragentCountryID <- map["ContragentCountryID"]
        paymentCode <- map["PaymentCode"]
        transferSumm <- map["TransferSumm"]
        transferAccountNo <- map["TransferAccountNo"]
        totalCommission <- map["TotalCommission"]
        transferAccountCurrencyID <- map["TransferAccountCurrencyID"]
        isTemplate <- map["IsTemplate"]
        templateName <- map["TemplateName"]
        templateDescription <- map["TemplateDescription"]
        scheduleDate <- map["ScheduleDate"]
        scheduleID <- map["ScheduleID"]
        schedule <- map["Schedule"]
        createDate <- map["CreateDate"]
        confirmType <- map["ConfirmType"]
    }
    
}
// модель при смене даты ClearingGross
open class ClearingGrossNeedChangeDateModel: Mappable {
    
    open
    var CreateDate: String?,
    PNumber: String?,
    PaymentCode: String?,
    PaymentComment: String?,
    PaymentDate: String?,
    PaymentTypeID: String?,
    ReceiverAccountNo: String?,
    ReceiverBIK: String?,
    ReceiverFullName: String?,
    SenderAccountNo: String?,
    SenderFullName: String?,
    TransferSumm: Double?
    
    required public init(
        PaymentDate: String?,
        PaymentTypeID: String?
    ){
        self.PaymentDate = PaymentDate
        self.PaymentTypeID = PaymentTypeID
    }
    
    required public init(CreateDate: String?,
                         PNumber: String?,
                         PaymentCode: String?,
                         PaymentComment: String?,
                         PaymentDate: String?,
                         PaymentTypeID: String?,
                         ReceiverAccountNo: String?,
                         ReceiverBIK: String?,
                         ReceiverFullName: String?,
                         SenderAccountNo: String?,
                         SenderFullName: String?,
                         TransferSumm: Double?){
        self.CreateDate = CreateDate
        self.PNumber = PNumber
        self.PaymentCode = PaymentCode
        self.PaymentComment = PaymentComment
        self.PaymentDate = PaymentDate
        self.PaymentTypeID = PaymentTypeID
        self.ReceiverAccountNo = ReceiverAccountNo
        self.ReceiverBIK = ReceiverBIK
        self.ReceiverFullName = ReceiverFullName
        self.SenderAccountNo = SenderAccountNo
        self.SenderFullName = SenderFullName
        self.TransferSumm = TransferSumm
    }
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        CreateDate <- map["CreateDate"]
        PNumber <- map["PNumber"]
        PaymentCode <- map["PaymentCode"]
        PaymentComment <- map ["PaymentComment"]
        PaymentDate <- map["PaymentDate"]
        PaymentTypeID <- map["PaymentTypeID"]
        ReceiverAccountNo <- map["ReceiverAccountNo"]
        ReceiverBIK <- map["ReceiverBIK"]
        ReceiverFullName <- map["ReceiverFullName"]
        SenderAccountNo <- map["SenderAccountNo"]
        SenderFullName <- map["SenderFullName"]
        TransferSumm <- map["TransferSumm"]
    }
}
// модель при смене даты ClearingGross
open class ClearingGrossNeedChangeDateResultModel: Mappable {
    
    open
    var nextDate: String?,
    start: String?,
    end: String?,
    isSessionExpired: Bool?,
    isFuture: Bool?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        nextDate <- map["NextDate"]
        start <- map["Start"]
        end <- map["End"]
        isSessionExpired <- map["IsSessionExpired"]
        isFuture <- map["IsFuture"]
    }
}
// модель проверки даты ClearingGross
open class InternetBankingClearingGrossValidateDataModel: Mappable {
    
    open
    var clearingMaxAmount: Int?,
    grossCreatePeriodFrom: String?,
    grossCreatePeriodTo: String?,
    clearingCreatePeriodFrom: String?,
    clearingCreatePeriodTo: String?,
    timeOfDay: String?,
    dateFrom: String?,
    dateTo: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        clearingMaxAmount <- map["ClearingMaxAmount"]
        grossCreatePeriodFrom <- map["GrossCreatePeriodFrom"]
        grossCreatePeriodTo <- map["GrossCreatePeriodTo"]
        clearingCreatePeriodFrom <- map["ClearingCreatePeriodFrom"]
        clearingCreatePeriodTo <- map["ClearingCreatePeriodTo"]
        timeOfDay <- map["TimeOfDay"]
        dateFrom <- map["DateFrom"]
        dateTo <- map["DateTo"]
       
    }
}
// модель AccountModel
open class AccountModel: Mappable {
    
    open
    var accountNo: String?,
    currencyID: Int?
    
    init(){}
    required public init(
        accountNo: String?,
        currencyID: Int?
        ){
        self.accountNo = accountNo
        self.currencyID = currencyID
        
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
    }
}
// модель InfoDetail
open class InfoDetail: Mappable {
    
    open
    var accountNo: String?,
    balance: Double?,
    dateExpired: String?
    
    init(){}
    required public init(
        accountNo: String?,
        balance: Double?,
        dateExpired: String?
        ){
        self.accountNo = accountNo
        self.balance = balance
        self.dateExpired = dateExpired
        
    }
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        accountNo <- map["AccountNo"]
        balance <- map["Balance"]
        dateExpired <- map["DateExpired"]
    }
}

// модель ClearingGrossTransfersModel
open class ClearingGrossTransfersModel: Mappable {
    
    open
    var paginationInfo: PageInfo?,
    result: [TransfersModel]?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        paginationInfo <- map["PaginationInfo"]
        result <- map["Result"]
        
    }
}

// модель PageInfo
open class PageInfo: Mappable {
    
    open
    var
    page: Int?,
    pageSize: Int?,
    totalItems: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        page <- map["Page"]
        pageSize <- map["PageSize"]
        totalItems <- map["TotalItems"]
        
    }
}

// модель для TransfersModel
open class TransfersModel: Mappable {
    open var
    statusID: Int?,
    paymentID: Int?,
    paymentTypeID: Int?,
    paymentType: String?,
    paymentDirectionID: Int?,
    paymentDirection: String?,
    pNumber: String?,
    statusDate: String?,
    paymentDate: String?,
    status: String?,
    transferSumm: Double?,
    paymentCode: String?,
    receiverFullName: String?,
    receiverBIK: String?,
    receiverAccountNo: String?,
    paymentComment: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        statusID <- map["StatusID"]
        paymentID <- map["PaymentID"]
        paymentTypeID <- map["PaymentTypeID"]
        paymentType <- map["PaymentType"]
        paymentDirectionID <- map["PaymentDirectionID"]
        paymentDirection <- map["PaymentDirection"]
        pNumber <- map["PNumber"]
        statusDate <- map["StatusDate"]
        paymentDate <- map["PaymentDate"]
        status <- map["Status"]
        transferSumm <- map["TransferSumm"]
        paymentCode <- map["PaymentCode"]
        receiverFullName <- map["ReceiverFullName"]
        receiverBIK <- map["ReceiverBIK"]
        receiverAccountNo <- map["ReceiverAccountNo"]
        paymentComment <- map["PaymentComment"]
    
        
    }
}

// модель для ClearingGrossPaymenModel
open class ClearingGrossPaymenModel: Mappable {
    open var
    paymentID: Int?,
    paymentDate: String?,
    dateP: String?,
    pNumber: String?,
    paymentTypeID: Int?,
    sendingTypeID: Int?,
    paymentDirectionID: Int?,
    customerID: Int?,
    senderFullName: String?,
    receiverFullName: String?,
    senderBIK: String?,
    receiverBIK: String?,
    senderAccountNo: String?,
    receiverAccountNo: String?,
    paymentComment: String?,
    paymentCode: String?,
    transferSumm: Double?,
    paymentFileRef: String?,
    format: Int?,
    paymentsOperations: [PaymentsOperations]?,
    inputPaymentSenderInfo: [InputPaymentSenderInfo]?
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        
        paymentID <- map["PaymentID"]
        paymentDate <- map["PaymentDate"]
        dateP <- map["DateP"]
        pNumber <- map["PNumber"]
        paymentTypeID <- map["PaymentTypeID"]
        sendingTypeID <- map["SendingTypeID"]
        paymentDirectionID <- map["PaymentDirectionID"]
        customerID <- map["CustomerID"]
        senderFullName <- map["SenderFullName"]
        receiverFullName <- map["ReceiverFullName"]
        senderBIK <- map["SenderBIK"]
        receiverBIK <- map["ReceiverBIK"]
        senderAccountNo <- map["SenderAccountNo"]
        receiverAccountNo <- map["ReceiverAccountNo"]
        paymentComment <- map["PaymentComment"]
        paymentCode <- map["PaymentCode"]
        transferSumm <- map["TransferSumm"]
        paymentFileRef <- map["PaymentFileRef"]
        format <- map["Format"]
        paymentsOperations <- map["PaymentsOperations"]
        inputPaymentSenderInfo <- map["InputPaymentSenderInfo"]
        
    }
}

// модель проверки даты PaymentsOperations
open class PaymentsOperations: Mappable {
    
    open
    var clearingMaxAmount: Int?,
    grossCreatePeriodFrom: String?,
    grossCreatePeriodTo: String?,
    clearingCreatePeriodFrom: String?,
    clearingCreatePeriodTo: String?,
    timeOfDay: String?,
    dateFrom: String?,
    dateTo: String?

    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        clearingMaxAmount <- map["ClearingMaxAmount"]
        grossCreatePeriodFrom <- map["GrossCreatePeriodFrom"]
        grossCreatePeriodTo <- map["GrossCreatePeriodTo"]
        clearingCreatePeriodFrom <- map["ClearingCreatePeriodFrom"]
        clearingCreatePeriodTo <- map["ClearingCreatePeriodTo"]
        timeOfDay <- map["TimeOfDay"]
        dateFrom <- map["DateFrom"]
        dateTo <- map["DateTo"]

    }
}

// модель проверки даты InputPaymentSenderInfo
open class InputPaymentSenderInfo: Mappable {
    
        open var
    paymentID: Int?,
    inn: String?,
    okpo: String?,
    sfond: String?,
    refnum: String?,
    isReciverOk: Bool?,
    accountNo: String?,
    senderName: String?,
    officeid: Int?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        paymentID <- map["PaymentID"]
        inn <- map["Inn"]
        okpo <- map["Okpo"]
        sfond <- map["Sfond"]
        refnum <- map["refnum"]
        isReciverOk <- map["isReciverOk"]
        accountNo <- map["AccountNo"]
        senderName <- map["SenderName"]
        officeid <- map["Officeid"]
        
    }
}



