//
//  Class.swift
//  Alamofire
//
//  Created by Рустам on 18/12/20.
//

import ObjectMapper

//MARK: Regions
open class TaxRegionModel: Mappable{
    
    open var
        regionID: Int?,
        regionName: String?
    
    init(){}
    public required init?(map: Map) {}
    
    public func mapping(map: Map) {
        regionID <- map["RegionID"]
        regionName <- map["RegionName"]
    }
}

//MARK: Districts
open class TaxDistrictModel: Mappable{
    open var
        districtID: Int?,
        districtName: String?,
        districtCode: String?,
        regionID: Int?
        
    init(){}
    public required init?(map: Map) {}
    
    public func mapping(map: Map) {
        districtID <- map["DistrictID"]
        districtName <- map["DistrictName"]
        districtCode <- map["DistrictCode"]
        regionID <- map["RegionID"]
    }
}

//MARK: Departments
open class TaxDepartmentModel: Mappable{
    
    open var
        departmentID: Int?,
        taxDistrictID: Int?,
        departmentName: String?,
        externalBIKCode: String?,
        externalAccountNo: String?
    
    init(){}
    public required init?(map: Map) {}
    
    public func mapping(map: Map) {
        departmentID <- map["DepartmentID"]
        taxDistrictID <- map["TaxDistrictID"]
        departmentName <- map["DepartmentName"]
        externalBIKCode <- map["ExternalBIKCode"]
        externalAccountNo <- map["ExternalAccountNo"]
    }
}

//MARK: OkmotCodes
open class TaxOkmotCodeModel: Mappable{
    
    open var
        taxDepartmentID: Int?,
        okmotCode: String?
    
    init(){}
    public required init?(map: Map) {}
    
    public func mapping(map: Map){
        taxDepartmentID <- map["TaxDepartmentID"]
        okmotCode <- map["OkmotCode"]
    }
}
