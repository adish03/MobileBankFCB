//
//  Localize.swift
//  Alamofire
//
//  Created by Oleg Ten on 5/27/19.
//
//
import ObjectMapper

// Модель для описания шаблонов
open class Localize: Mappable {
    
    open var
   
    _32_amount: String?,
    _50_originator: String?,
    _56_intermediary_bank: String?,
    _57_pay_thru: String?,
    _59_beneficiary: String?,
    _70_benefinfo: String?,
    acc_card_to_soft_stop_list: String?,
    acc_len_must_be_greater_16: String?,
    AccBalance: String?,
    access_to_mobile_bank: String?,
    account: String?,
    account_already_active: String?,
    account_for_loan_repayment: String?,
    account_for_replenishment: String?,
    account_information: String?,
    account_operations: String?,
    account_statement: String?,
    AccountNo: String?,
    AccountNotFound: String?,
    accounts: String?,
    AccountStatement: String?,
    accrued_penalties: String?,
    add_user: String?,
    AdditionInfo: String?,
    Aggregators: String?,
    agreement_number: String?,
    All: String?,
    all_directions: String?,
    all_operations: String?,
    all_payment_types: String?,
    all_statuses: String?,
    all_templates: String?,
    all_utilities: String?,
    allowable_amount: String?,
    allowable_interest_rate: String?,
    amount: String?,
    amount_to_be_credited: String?,
    Apply: String?,
    apply_filter: String?,
    approve_type: String?,
    approved: String?,
    ApprovedLoansTitle: String?,
    are_you_sure: String?,
    are_you_sure_you_want_a_deposit_with_the_chosen_conditions: String?,
    at_period: String?,
    atms: String?,
    authentication_was_cancelled_by_application: String?,
    authentication_was_cancelled_by_the_system: String?,
    autopayment: String?,
    available_time: String?,
    Back: String?,
    balance: String?,
    bank_day: String?,
    BankSpecialist: String?,
    BeneficiaryBank: String?,
    BeneficiaryBankBic: String?,
    BeneficiaryBankCountry: String?,
    BeneficiaryBankLoroAccountNo: String?,
    bic_codes: String?,
    borrower: String?,
    branches: String?,
    buying: String?,
    calculate: String?,
    cancel: String?,
    caps_from: String?,
    card_accounts: String?,
    card_expire_date: String?,
    card_information: String?,
    card_number: String?,
    CardProduct: String?,
    CloseDepositReason: String?,
    cards: String?,
    cashing: String?,
    Categories: String?,
    change_language: String?,
    change_user: String?,
    ChangePassword: String?,
    Check: String?,
    check_nir_code: String?,
    check_operation_for_correctness: String?,
    check_requisite_for_correctness: String?,
    CheckBalance: String?,
    CheckSuccess: String?,
    choose_a_supplier: String?,
    choose_confirmation_form: String?,
    clearing_gross: String?,
    clearing_gross_transfer: String?,
    clearing_payment: String?,
    ClearingGrossOperations: String?,
    close: String?,
    close_date: String?,
    close_deposit: String?,
    closed: String?,
    closing_deposit: String?,
    Code: String?,
    code_is_confirmed: String?,
    CodeWord: String?,
    Comment: String?,
    commission: String?,
    commission_calculation: String?,
    commission_repayment: String?,
    CompanyName: String?,
    completion_day: String?,
    confirm: String?,
    confirm_loan_operation: String?,
    сейчас: String?,
    confirm_type_confirm: String?,
    confirm_type_error: String?,
    confirm_type_in_confirm: String?,
    confirm_type_save: String?,
    confirmation: String?,
    ConfirmConversion: String?,
    ConfirmConversionMessage: String?,
    ConfirmConversionSuccess: String?,
    ConfirmError: String?,
    ConfirmNewPasswordRequired: String?,
    ConfirmOperationSuccess: String?,
    ConfirmPayment: String?,
    ConfirmPaymentError: String?,
    ConfirmPaymentSuccess: String?,
    ConfirmRemoveSchedule: String?,
    ConfirmTransfer: String?,
    ConfirmTransferError: String?,
    ConfirmTransferMessage: String?,
    ConfirmTransferSuccess: String?,
    contain_numbers: String?,
    contains_special_characters: String?,
    continue_title: String?,
    conversion_rate: String?,
    ConversionParameters: String?,
    ConversionSuccessSave: String?,
    could_not_find_credit: String?,
    Create: String?,
    create_clearing: String?,
    create_gross: String?,
    create_operation_request: String?,
    create_operation_template: String?,
    create_pin_code: String?,
    create_swift: String?,
    create_template: String?,
    create_transfer: String?,
    created: String?,
    CreateLoanRequest: String?,
    CreateOperation: String?,
    CreateOperationBasedOnPayment: String?,
    CreateTemplateBasedOnPayment: String?,
    credit: String?,
    credit_application: String?,
    credit_graph: String?,
    credit_information: String?,
    credit_purpose: String?,
    credit_rate: String?,
    credit_schedules: String?,
    credit_score: String?,
    credit_term: String?,
    CreditInfoFull: String?,
    CreditInfoShort: String?,
    credits: String?,
    currencies: String?,
    currency: String?,
    currency_codes: String?,
    currency_conversion: String?,
    currency_exchange: String?,
    currency_operation_code: String?,
    currency_quotes_from: String?,
    currency_rates: String?,
    current_accounts: String?,
    current_balance: String?,
    current_card: String?,
    current_language: String?,
    current_password: String?,
    current_percents: String?,
    CurrentAccountNo: String?,
    CurrentPasswordRequired: String?,
    Date: String?,
    date_and_time: String?,
    DateEnd: String?,
    DateStart: String?,
    DateVal: String?,
    DayGenetiv: String?,
    DayNominativ: String?,
    DayPlural: String?,
    days_count: String?,
    dd_mm_yyyy: String?,
    debit: String?,
    DeferredPercentsSumm: String?,
    delete: String?,
    delete_operation: String?,
    delete_template: String?,
    delete_template_format: String?,
    Deposit: String?,
    deposit_accounts: String?,
    deposit_information: String?,
    deposit_replenishment: String?,
    deposit_sum: String?,
    deposits: String?,
    DepositsManagementTitle: String?,
    Description: String?,
    detail_info: String?,
    detailed: String?,
    detailed_information: String?,
    details_of_operation: String?,
    do_oyu_really_want_card_add_to_stop_list: String?,
    do_oyu_really_want_card_rebase_pin: String?,
    do_you_reallly_want_request_credit: String?,
    do_you_realy_want_close_deposit: String?,
    Documents: String?,
    download_check: String?,
    edit: String?,
    edit_template: String?,
    effective_percent_rate: String?,
    eight_or_more_characters: String?,
    ElcardTransfer: String?,
    Empty: String?,
    end_day: String?,
    english_language: String?,
    enter: String?,
    enter_etoken: String?,
    enter_pin_code: String?,
    Error: String?,
    ErrorMaxConversionSum: String?,
    ErrorMaxPaymentSum: String?,
    etoken_code_is_confirmed: String?,
    every_month: String?,
    every_week: String?,
    exchange: String?,
    exchange_at_rate: String?,
    exchange_rates_of_the_head_office: String?,
    Execute: String?,
    execute_operation: String?,
    ExecutingRequest: String?,
    execution_time: String?,
    expense_type: String?,
    Export: String?,
    export_statement: String?,
    field_cannot_be_blank: String?,
    fill_in_all_the_fields: String?,
    Filter: String?,
    filters: String?,
    finances: String?,
    find_bik: String?,
    find_currency_operation_code: String?,
    find_expense_type: String?,
    find_payment_code: String?,
    find_the_bic_code: String?,
    Fines: String?,
    fines_repayment: String?,
    first_3_must_be_same_as_in_bik: String?,
    for_your_financial_security_passowrd_text: String?,
    forgot_pin_message: String?,
    forgot_pin_question: String?,
    fri: String?,
    from_account: String?,
    FromTo: String?,
    full_early_repayment: String?,
    full_receiver_name: String?,
    full_sender_name: String?,
    further: String?,
    go_out: String?,
    GoToPasswordChangePage: String?,
    graph: String?,
    gross_payment: String?,
    GroupName: String?,
    HasToChangePasswordAfterLogin: String?,
    history: String?,
    Import: String?,
    ImportDragFileHere: String?,
    ImportFileContainsError: String?,
    ImportMaxFileSize: String?,
    in_balance: String?,
    in_processing: String?,
    InBalance: String?,
    Incoming: String?,
    incorrectly_specified_props_check_again: String?,
    instructions: String?,
    insufficient_account_balance: String?,
    insufficient_full_loan_repayment: String?,
    insufficient_loan_repayment: String?,
    interest: String?,
    interest_rate: String?,
    intermediary_bank_account_number: String?,
    intermediary_bank_bic: String?,
    intermediary_bank_country: String?,
    intermediary_bank_name: String?,
    IntermediaryBankLoroAccountNo: String?,
    internal_bank_transfer: String?,
    Internal_operation: String?,
    invalid_payment_code: String?,
    invalid_payment_code_entered: String?,
    invalid_recipient_account: String?,
    invalid_recipient_bik: String?,
    invalid_requisite: String?,
    is_done: String?,
    issued: String?,
    IssueDate: String?,
    kilometers_dot: String?,
    kirghiz_language: String?,
    language_settings: String?,
    last_operation: String?,
    last_operations: String?,
    LastOperationDate: String?,
    Later: String?,
    less_than_one_km: String?,
    list: String?,
    loading: String?,
    loan_account_statement: String?,
    loan_amount: String?,
    loan_create_request: String?,
    loan_of_account: String?,
    loan_repayment: String?,
    loan_summ: String?,
    LoanCreateRequest: String?,
    LoanDetailInfo: String?,
    LoanIndex: String?,
    LoanIsOverdueMessage: String?,
    LoanNotCalculationStatusMessage: String?,
    LoanProduct: String?,
    LoanState: String?,
    LoanStopCalculationStatusMessage: String?,
    location_permission_text: String?,
    Lock: String?,
    LockReason: String?,
    LockUnlock: String?,
    log_in: String?,
    login: String?,
    login_to_mobile_bank: String?,
    LoginRequired: String?,
    logout: String?,
    look: String?,
    main: String?,
    main_dot_summ: String?,
    MainSumm: String?,
    manage_users: String?,
    map_information: String?,
    map_title: String?,
    meters_dot: String?,
    MFO: String?,
    minimal_sum: String?,
    MinPaymentDate: String?,
    Resources_Common_Additional_info: String?,
    mon: String?,
    month: String?,
    month_day: String?,
    MoneyTransfer: String?,
    MoneyTransfers: String?,
    currency_rate: String?,
    more: String?,
    more_than_one_km: String?,
    mortrage_type: String?,
    my_finances: String?,
    my_templates: String?,
    n_of_operation: String?,
    NameTitle: String?,
    near_you: String?,
    NewPasswordRequired: String?,
    next_repayment: String?,
    NextPayDate: String?,
    no_active_accounts_found: String?,
    no_data: String?,
    no_operations_for_the_selected_period: String?,
    not_defined: String?,
    not_interactive: String?,
    not_planned: String?,
    not_selected: String?,
    nothing_found_change_search_criteria: String?,
    notifications: String?,
    NotRequired: String?,
    Number: String?,
    number_of_pp: String?,
    offices_and_terminals: String?,
    only_1_time: String?,
    open_date: String?,
    open_deposit: String?,
    open_link_in_safari: String?,
    OpenDepositTitle: String?,
    opening_deposit: String?,
    operation_by_template: String?,
    operation_complete: String?,
    operation_date: String?,
    operation_from_template: String?,
    operation_history: String?,
    operation_list: String?,
    operation_name: String?,
    operation_status: String?,
    operation_type: String?,
    OperationConfirmationMessager: String?,
    OperationConfirmRemove: String?,
    OperationFormatClearingGross: String?,
    OperationFormatInternal: String?,
    OperationImportInfo: String?,
    OperationOnPage: String?,
    OperationSystem: String?,
    operations_from: String?,
    operations_to: String?,
    or_more_characters: String?,
    out_balance: String?,
    OutBalance: String?,
    Outgoing: String?,
    overdue_days: String?,
    OverdueDays: String?,
    partial_early_repayment: String?,
    passcode_is_not_set_on_the_device: String?,
    partial_early_repayment_of_principal: String?,
    password: String?,
    PasswordAverage: String?,
    PasswordExpiredMessage: String?,
    PasswordGood: String?,
    PasswordRequired: String?,
    PasswordsNotEquals: String?,
    PasswordSuccessfullyChanged: String?,
    PasswordWeak: String?,
    pay_from: String?,
    pay_sum: String?,
    PayDate: String?,
    payment: String?,
    payment_amount: String?,
    payment_code: String?,
    payment_comment: String?,
    payment_n: String?,
    payment_purpose_codes: String?,
    payment_sum: String?,
    PaymentCommission: String?,
    PaymentDirection: String?,
    PaymentParameters: String?,
    PaymentSaveSuccess: String?,
    PaymentsFrom: String?,
    PayUtilities: String?,
    percent_account_statement: String?,
    percent_rate: String?,
    percent_repayment: String?,
    percents_to_pay: String?,
    PercentsSumm: String?,
    perform_an_operation_on: String?,
    PerformChangeButton: String?,
    PerformLogin: String?,
    Period: String?,
    period_and_percent: String?,
    period_of_operations: String?,
    phone_for_information: String?,
    pin_not_confirmed: String?,
    PinRequired: String?,
    planned_maturity_date: String?,
    planned_payment: String?,
    please_enter_a_number_greater_than_or_equal_to: String?,
    please_enter_a_number_less_than_or_equal_to: String?,
    pos_terminals: String?,
    press_back_again_to_exit: String?,
    principal_balance: String?,
    principal_repayment: String?,
    Print: String?,
    ProcessDay: String?,
    Product: String?,
    ProductName: String?,
    props_number: String?,
    loyalty_silver_status: String?,
    loyalty_silver_status_info_mb: String?,
    loyalty_gold_status: String?,
    loyalty_gold_status_info_mb: String?,
    loyalty_platinum_status: String?,
    loyalty_platinum_status_info_mb: String?,
    loyalty_vip_status: String?,
    loyalty_vip_status_info_mb: String?,
    RateWarning: String?,
    receipt: String?,
    receiver: String?,
    receiver_bank_account_number: String?,
    receiver_bank_bic: String?,
    receiver_bank_country_code: String?,
    receiver_bank_name: String?,
    recipient_account: String?,
    recipients_card_number: String?,
    recommendations_for_a_new_password: String?,
    recurring_type: String?,
    Refresh: String?,
    refuse: String?,
    Region: String?,
    reject_reason: String?,
    reject_request: String?,
    RejectConfirmMessage: String?,
    RejectConfirmTitle: String?,
    repayment: String?,
    repayment_amount: String?,
    repeat_new_password: String?,
    repeat_pin: String?,
    replenish_deposit: String?,
    replenishment: String?,
    RequestLoansTitle: String?,
    Required: String?,
    requisite: String?,
    resend_sms: String?,
    ResendSmsCode: String?,
    reset_counter_of_entered_pin_codes: String?,
    reset_filters: String?,
    Resources_Auth_Common_UserName: String?,
    Resources_ClearingGross_Common_Clearing: String?,
    Resources_ClearingGross_Common_Gross: String?,
    Resources_ClearingGross_Common_PaymentDate: String?,
    Resources_ClearingGross_Common_PaymentNumber: String?,
    Resources_ClearingGross_Common_PaymentType: String?,
    Resources_ClearingGross_Common_SenderFullName: String?,
    Resources_ClearingGross_Common_Title: String?,
    Resources_ClearingGross_Operations_PaymentCode: String?,
    Resources_ClearingGross_Operations_ReceiverFullName: String?,
    Resources_Common_AccountName: String?,
    Resources_Common_Calculate: String?,
    Resources_Common_Comment: String?,
    Resources_Common_FromAccount: String?,
    Resources_Common_Purpose: String?,
    Resources_Common_Save: String?,
    Resources_Common_Send: String?,
    Resources_Deposits_Conversion_BuyAccount: String?,
    Resources_Deposits_Conversion_BuySum: String?,
    Resources_Deposits_DepositAccounts_Conversion: String?,
    Resources_Internal_Customer_Transaction: String?,
    Resources_Internal_Transaction: String?,
    Resources_Swift_Swift_ReceiverAccountNo: String?,
    Resources_Swift_Swift_ReceiverBankBic: String?,
    Resources_Swift_Swift_TransferSum: String?,
    Resources_Swift_Swift_DateVal: String?,
    DatePPFull: String?,
    Resources_Utilities_Pay: String?,
    retry: String?,
    retry_operation: String?,
    retry_query: String?,
    RunCommand: String?,
    russian_language: String?,
    SameAccountError: String?,
    sat: String?,
    save: String?,
    save_as_template: String?,
    save_template: String?,
    SaveSuccess: String?,
    schedule_auto_payment: String?,
    scheduled: String?,
    ScheduleDateEnd: String?,
    ScheduleDateStart: String?,
    search: String?,
    select_a_category: String?,
    select_account_or_card: String?,
    select_confirmation_form: String?,
    select_credit: String?,
    select_currency: String?,
    select_language: String?,
    select_operation_type: String?,
    select_service_category: String?,
    select_the_type_of_collateral: String?,
    SelectAccount: String?,
    SelectApproveType: String?,
    SelectCodeVO: String?,
    SelectCurrency: String?,
    SelectCurrentAccountMessage: String?,
    SelectMortrageType: String?,
    SelectProductType: String?,
    selling: String?,
    send_request: String?,
    send_sms_again_after: String?,
    send_to_bank: String?,
    SenderBank: String?,
    SenderBankBic: String?,
    SenderBankCountry: String?,
    SenderBankLoroAccountNo: String?,
    sent_to_bank: String?,
    Service: String?,
    service_categories_caps: String?,
    service_providers_caps: String?,
    ServiceNotFound: String?,
    Services: String?,
    settings: String?,
    show_on_the_map: String?,
    show_only_issued: String?,
    show_only_opened: String?,
    show_operations: String?,
    skip: String?,
    sms_code_command: String?,
    sms_with_code_was_sent_to: String?,
    SmsCodeRequired: String?,
    start_day: String?,
    statement_of_interest_account: String?,
    StatementFrom: String?,
    status: String?,
    status_date: String?,
    SuccessSaveTemplate: String?,
    sum_above_max_unavailable_with_clearing: String?,
    гросс: String?,
    sum_must_be_greater: String?,
    swift: String?,
    swift_payment: String?,
    swift_transfer: String?,
    swift_transfer_no_hyphen: String?,
    SwiftTransferList: String?,
    TargetGroup: String?,
    tax: String?,
    template_description: String?,
    template_name: String?,
    TemplateConfirmRemove: String?,
    TemplateDescription: String?,
    TemplateList: String?,
    TemplateName: String?,
    templates: String?,
    TemplateSchedule: String?,
    full_name: String?,
    th: String?,
    the_context_is_invalid: String?,
    the_amount_of_the_operation_can_not_be_more_than_the_current_balance: String?,
    the_list_of_operations_is_empty: String?,
    the_operation_for_the_selected_date_is_not_available: String?,
    the_products_name: String?,
    the_sun: String?,
    the_user_failed_to_provide_valid_credentials: String?,
    to_account: String?,
    to_pay: String?,
    to_repeat: String?,
    total: String?,
    total_amount: String?,
    total_for_payment: String?,
    total_for_transfer: String?,
    total_to_pay: String?,
    TotalSumm: String?,
    Tranche: String?,
    TrancheAdditionalInfo: String?,
    Tranches: String?,
    transaction_number: String?,
    transfer: String?,
    transfer_between_accounts: String?,
    transfer_sum_buy: String?,
    transfer_to: String?,
    transfer_to_account: String?,
    transfer_to_card_prefix: String?,
    transfer_to_client_account: String?,
    transfer_to_sum: String?,
    TransferParameters: String?,
    transferring: String?,
    transfers: String?,
    TransferSaveSuccess: String?,
    translation_commentary: String?,
    tue: String?,
    Unlock: String?,
    UploadOperationsExcel: String?,
    uppercase_and_lowercase_latin_letters: String?,
    use: String?,
    use_iphone_password: String?,
    use_touch_id_or_face_id_for_quick_access: String?,
    utilities: String?,
    about_app: String?,
    help_support: String?,
    utilities_pay_format: String?,
    utilities_pay_status: String?,
    utilities_payment: String?,
    utilities_provider: String?,
    utility_payment: String?,
    utility_payment_repayment: String?,
    utility_provider: String?,
    utlitity_category: String?,
    ValidateMaxAmount: String?,
    GetMiniStatement: String?,
    ValidateMessageGreaterThanZero: String?,
    ValidateMinAmount: String?,
    verification_code: String?,
    Verify: String?,
    View: String?,
    view_loan_application: String?,
    waiver_of_application: String?,
    wed: String?,
    week_day: String?,
    whole_story: String?,
    WillBeCharged: String?,
    WillBeCredited: String?,
    withdrawal: String?,
    work_schedule: String?,
    write_off: String?,
    write_off_account: String?,
    write_off_from_account: String?,
    write_off_sum: String?,
    write_off_sum_buy: String?,
    wrong_pin_code: String?,
    you_have_successfully_confirmed_the_sms_code_to_complete_the_payment: String?,
    you_have_successfully_confirmed_your_phone_number: String?,
    YouDontHaveAccounts: String?,
    YouDontHaveCardAccounts: String?,
    YouDontHaveDeposits: String?,
    YourPasswordWillExpiredIn: String?,
    YourPasswordWillExpiredSoon: String?,
    zoom_in_to_view_markers: String?,
    _72_additional_info: String?,
    resources_Common_Additional_info: String?,
    gold_status: String?,
    silver_status: String?,
    platinum_status: String?,
    vip_status: String?,
    loyalty_status: String?,
    Resources_Swift_Swift_ReceiverAccountNo_Placeholder: String?
    
    
    
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        gold_status <- map ["gold_status"]
        silver_status <- map ["silver_status"]
        platinum_status <- map ["platinum_status"]
        vip_status <- map ["vip_status"]
        
        _32_amount <- map["_32_amount"]
        _50_originator <- map["_50_originator"]
        _56_intermediary_bank <- map["_56_intermediary_bank"]
        _57_pay_thru <- map["_57_pay_thru"]
        _59_beneficiary <- map["_59_beneficiary"]
        _70_benefinfo <- map["_70_benefinfo"]
        _72_additional_info <- map["_72_additional_info"]
        resources_Common_Additional_info <- map["Resources_Common_Additional_info"]
        acc_card_to_soft_stop_list <- map["acc_card_to_soft_stop_list"]
        acc_len_must_be_greater_16 <- map["acc_len_must_be_greater_16"]
        AccBalance <- map["AccBalance"]
        access_to_mobile_bank <- map["access_to_mobile_bank"]
        account <- map["account"]
        account_already_active <- map["account_already_active"]
        account_for_loan_repayment <- map["account_for_loan_repayment"]
        account_for_replenishment <- map["account_for_replenishment"]
        account_information <- map["account_information"]
        account_operations <- map["account_operations"]
        account_statement <- map["account_statement"]
        AccountNo <- map["AccountNo"]
        AccountNotFound <- map["AccountNotFound"]
        accounts <- map["accounts"]
        AccountStatement <- map["AccountStatement"]
        accrued_penalties <- map["accrued_penalties"]
        add_user <- map["add_user"]
        AdditionInfo <- map["AdditionInfo"]
        Aggregators <- map["Aggregators"]
        GetMiniStatement <- map["GetMiniStatement"]
        agreement_number <- map["agreement_number"]
        All <- map["All"]
        all_directions <- map["all_directions"]
        all_operations <- map["all_operations"]
        all_payment_types <- map["all_payment_types"]
        all_statuses <- map["all_statuses"]
        all_templates <- map["all_templates"]
        all_utilities <- map["all_utilities"]
        allowable_amount <- map["allowable_amount"]
        allowable_interest_rate <- map["allowable_interest_rate"]
        amount <- map["amount"]
        amount_to_be_credited <- map["amount_to_be_credited"]
        Apply <- map["Apply"]
        apply_filter <- map["apply_filter"]
        approve_type <- map["approve_type"]
        loyalty_status <- map["loyalty_status"]
        approved <- map["approved"]
        loyalty_silver_status <- map["loyalty_silver_status"]
        loyalty_silver_status_info_mb <- map["loyalty_silver_status_info_mb"]
        loyalty_gold_status <- map["loyalty_gold_status"]
        loyalty_gold_status_info_mb <- map["loyalty_gold_status_info_mb"]
        loyalty_platinum_status <- map["loyalty_platinum_status"]
        loyalty_platinum_status_info_mb <- map["loyalty_platinum_status_info_mb"]
        loyalty_vip_status <- map["loyalty_vip_status"]
        loyalty_vip_status_info_mb <- map["loyalty_vip_status_info_mb"]
        are_you_sure <- map["are_you_sure"]
        ApprovedLoansTitle <- map["ApprovedLoansTitle"]
        are_you_sure_you_want_a_deposit_with_the_chosen_conditions <- map["are_you_sure_you_want_a_deposit_with_the_chosen_conditions"]
        at_period <- map["at_period"]
        atms <- map["atms"]
        CloseDepositReason <- map["CloseDepositReason"]
        authentication_was_cancelled_by_application <- map["authentication_was_cancelled_by_application"]
        authentication_was_cancelled_by_the_system <- map["authentication_was_cancelled_by_the_system"]
        autopayment <- map["autopayment"]
        available_time <- map["available_time"]
        Back <- map["Back"]
        Resources_Common_Additional_info <- map["Resources_Common_Additional_info"]
        full_name <- map["full_name"]
        balance <- map["balance"]
        bank_day <- map["bank_day"]
        BankSpecialist <- map["BankSpecialist"]
        BeneficiaryBank <- map["BeneficiaryBank"]
        BeneficiaryBankBic <- map["BeneficiaryBankBic"]
        BeneficiaryBankCountry <- map["BeneficiaryBankCountry"]
        BeneficiaryBankLoroAccountNo <- map["BeneficiaryBankLoroAccountNo"]
        bic_codes <- map["bic_codes"]
        borrower <- map["borrower"]
        branches <- map["branches"]
        buying <- map["buying"]
        calculate <- map["calculate"]
        cancel <- map["cancel"]
        caps_from <- map["caps_from"]
        card_accounts <- map["card_accounts"]
        card_expire_date <- map["card_expire_date"]
        card_information <- map["card_information"]
        card_number <- map["card_number"]
        CardProduct <- map["CardProduct"]
        cards <- map["cards"]
        cashing <- map["cashing"]
        Categories <- map["Categories"]
        change_language <- map["change_language"]
        change_user <- map["change_user"]
        ChangePassword <- map["ChangePassword"]
        Check <- map["Check"]
        check_nir_code <- map["check_nir_code"]
        check_operation_for_correctness <- map["check_operation_for_correctness"]
        check_requisite_for_correctness <- map["check_requisite_for_correctness"]
        CheckBalance <- map["CheckBalance"]
        CheckSuccess <- map["CheckSuccess"]
        choose_a_supplier <- map["choose_a_supplier"]
        choose_confirmation_form <- map["choose_confirmation_form"]
        clearing_gross <- map["clearing_gross"]
        clearing_gross_transfer <- map["clearing_gross_transfer"]
        clearing_payment <- map["clearing_payment"]
        ClearingGrossOperations <- map["ClearingGrossOperations"]
        close <- map["close"]
        close_date <- map["close_date"]
        close_deposit <- map["close_deposit"]
        closed <- map["closed"]
        closing_deposit <- map["closing_deposit"]
        Code <- map["Code"]
        code_is_confirmed <- map["code_is_confirmed"]
        CodeWord <- map["CodeWord"]
        Comment <- map["Comment"]
        commission <- map["commission"]
        commission_calculation <- map["commission_calculation"]
        commission_repayment <- map["commission_repayment"]
        CompanyName <- map["CompanyName"]
        completion_day <- map["completion_day"]
        confirm <- map["confirm"]
        confirm_loan_operation <- map["confirm_loan_operation"]
        confirm_type_confirm <- map["confirm_type_confirm"]
        confirm_type_error <- map["confirm_type_error"]
        confirm_type_in_confirm <- map["confirm_type_in_confirm"]
        confirm_type_save <- map["confirm_type_save"]
        confirmation <- map["confirmation"]
        ConfirmConversion <- map["ConfirmConversion"]
        ConfirmConversionMessage <- map["ConfirmConversionMessage"]
        ConfirmConversionSuccess <- map["ConfirmConversionSuccess"]
        ConfirmError <- map["ConfirmError"]
        ConfirmNewPasswordRequired <- map["ConfirmNewPasswordRequired"]
        ConfirmOperationSuccess <- map["ConfirmOperationSuccess"]
        ConfirmPayment <- map["ConfirmPayment"]
        ConfirmPaymentError <- map["ConfirmPaymentError"]
        ConfirmPaymentSuccess <- map["ConfirmPaymentSuccess"]
        ConfirmRemoveSchedule <- map["ConfirmRemoveSchedule"]
        ConfirmTransfer <- map["ConfirmTransfer"]
        ConfirmTransferError <- map["ConfirmTransferError"]
        ConfirmTransferMessage <- map["ConfirmTransferMessage"]
        ConfirmTransferSuccess <- map["ConfirmTransferSuccess"]
        contain_numbers <- map["contain_numbers"]
        contains_special_characters <- map["contains_special_characters"]
        continue_title <- map["continue_title"]
        conversion_rate <- map["conversion_rate"]
        ConversionParameters <- map["ConversionParameters"]
        ConversionSuccessSave <- map["ConversionSuccessSave"]
        could_not_find_credit <- map["could_not_find_credit"]
        Create <- map["Create"]
        create_clearing <- map["create_clearing"]
        create_gross <- map["create_gross"]
        create_operation_request <- map["create_operation_request"]
        create_operation_template <- map["create_operation_template"]
        create_pin_code <- map["create_pin_code"]
        create_swift <- map["create_swift"]
        create_template <- map["create_template"]
        create_transfer <- map["create_transfer"]
        created <- map["created"]
        CreateLoanRequest <- map["CreateLoanRequest"]
        CreateOperation <- map["CreateOperation"]
        CreateOperationBasedOnPayment <- map["CreateOperationBasedOnPayment"]
        CreateTemplateBasedOnPayment <- map["CreateTemplateBasedOnPayment"]
        credit <- map["credit"]
        credit_application <- map["credit_application"]
        credit_graph <- map["credit_graph"]
        credit_information <- map["credit_information"]
        credit_purpose <- map["credit_purpose"]
        credit_rate <- map["credit_rate"]
        credit_schedules <- map["credit_schedules"]
        credit_score <- map["credit_score"]
        credit_term <- map["credit_term"]
        CreditInfoFull <- map["CreditInfoFull"]
        CreditInfoShort <- map["CreditInfoShort"]
        credits <- map["credits"]
        currencies <- map["currencies"]
        currency <- map["currency"]
        currency_codes <- map["currency_codes"]
        currency_conversion <- map["currency_conversion"]
        currency_exchange <- map["currency_exchange"]
        currency_operation_code <- map["currency_operation_code"]
        currency_quotes_from <- map["currency_quotes_from"]
        currency_rates <- map["currency_rates"]
        current_accounts <- map["current_accounts"]
        current_balance <- map["current_balance"]
        current_card <- map["current_card"]
        current_language <- map["current_language"]
        current_password <- map["current_password"]
        current_percents <- map["current_percents"]
        CurrentAccountNo <- map["CurrentAccountNo"]
        CurrentPasswordRequired <- map["CurrentPasswordRequired"]
        Date <- map["Date"]
        date_and_time <- map["date_and_time"]
        DateEnd <- map["DateEnd"]
        DateStart <- map["DateStart"]
        DateVal <- map["DateVal"]
        DayGenetiv <- map["DayGenetiv"]
        DayNominativ <- map["DayNominativ"]
        DayPlural <- map["DayPlural"]
        days_count <- map["days_count"]
        dd_mm_yyyy <- map["dd_mm_yyyy"]
        debit <- map["debit"]
        DeferredPercentsSumm <- map["DeferredPercentsSumm"]
        delete <- map["delete"]
        delete_operation <- map["delete_operation"]
        delete_template <- map["delete_template"]
        delete_template_format <- map["delete_template_format"]
        Deposit <- map["Deposit"]
        deposit_accounts <- map["deposit_accounts"]
        deposit_information <- map["deposit_information"]
        deposit_replenishment <- map["deposit_replenishment"]
        deposit_sum <- map["deposit_sum"]
        deposits <- map["deposits"]
        DepositsManagementTitle <- map["DepositsManagementTitle"]
        Description <- map["Description"]
        detail_info <- map["detail_info"]
        detailed <- map["detailed"]
        detailed_information <- map["detailed_information"]
        details_of_operation <- map["details_of_operation"]
        do_oyu_really_want_card_add_to_stop_list <- map["do_oyu_really_want_card_add_to_stop_list"]
        do_oyu_really_want_card_rebase_pin <- map["do_oyu_really_want_card_rebase_pin"]
        do_you_reallly_want_request_credit <- map["do_you_reallly_want_request_credit"]
        do_you_realy_want_close_deposit <- map["do_you_realy_want_close_deposit"]
        Documents <- map["Documents"]
        download_check <- map["download_check"]
        edit <- map["edit"]
        edit_template <- map["edit_template"]
        effective_percent_rate <- map["effective_percent_rate"]
        eight_or_more_characters <- map["eight_or_more_characters"]
        ElcardTransfer <- map["ElcardTransfer"]
        Empty <- map["Empty"]
        end_day <- map["end_day"]
        english_language <- map["english_language"]
        enter <- map["enter"]
        enter_etoken <- map["enter_etoken"]
        enter_pin_code <- map["enter_pin_code"]
        Error <- map["Error"]
        ErrorMaxConversionSum <- map["ErrorMaxConversionSum"]
        ErrorMaxPaymentSum <- map["ErrorMaxPaymentSum"]
        etoken_code_is_confirmed <- map["etoken_code_is_confirmed"]
        every_month <- map["every_month"]
        every_week <- map["every_week"]
        exchange <- map["exchange"]
        exchange_at_rate <- map["exchange_at_rate"]
        exchange_rates_of_the_head_office <- map["exchange_rates_of_the_head_office"]
        Execute <- map["Execute"]
        execute_operation <- map["execute_operation"]
        ExecutingRequest <- map["ExecutingRequest"]
        execution_time <- map["execution_time"]
        expense_type <- map["expense_type"]
        Export <- map["Export"]
        export_statement <- map["export_statement"]
        field_cannot_be_blank <- map["field_cannot_be_blank"]
        fill_in_all_the_fields <- map["fill_in_all_the_fields"]
        Filter <- map["Filter"]
        filters <- map["filters"]
        finances <- map["finances"]
        find_bik <- map["find_bik"]
        find_currency_operation_code <- map["find_currency_operation_code"]
        find_expense_type <- map["find_expense_type"]
        find_payment_code <- map["find_payment_code"]
        find_the_bic_code <- map["find_the_bic_code"]
        Fines <- map["Fines"]
        fines_repayment <- map["fines_repayment"]
        first_3_must_be_same_as_in_bik <- map["first_3_must_be_same_as_in_bik"]
        for_your_financial_security_passowrd_text <- map["for_your_financial_security_passowrd_text"]
        forgot_pin_message <- map["forgot_pin_message"]
        forgot_pin_question <- map["forgot_pin_question"]
        fri <- map["fri"]
        from_account <- map["from_account"]
        FromTo <- map["FromTo"]
        full_early_repayment <- map["full_early_repayment"]
        full_receiver_name <- map["full_receiver_name"]
        full_sender_name <- map["full_sender_name"]
        further <- map["further"]
        go_out <- map["go_out"]
        GoToPasswordChangePage <- map["GoToPasswordChangePage"]
        graph <- map["graph"]
        gross_payment <- map["gross_payment"]
        GroupName <- map["GroupName"]
        HasToChangePasswordAfterLogin <- map["HasToChangePasswordAfterLogin"]
        history <- map["history"]
        Import <- map["Import"]
        ImportDragFileHere <- map["ImportDragFileHere"]
        ImportFileContainsError <- map["ImportFileContainsError"]
        ImportMaxFileSize <- map["ImportMaxFileSize"]
        in_balance <- map["in_balance"]
        in_processing <- map["in_processing"]
        InBalance <- map["InBalance"]
        Incoming <- map["Incoming"]
        incorrectly_specified_props_check_again <- map["incorrectly_specified_props_check_again"]
        instructions <- map["instructions"]
        insufficient_account_balance <- map["insufficient_account_balance"]
        insufficient_full_loan_repayment <- map["insufficient_full_loan_repayment"]
        insufficient_loan_repayment <- map["insufficient_loan_repayment"]
        interest <- map["interest"]
        interest_rate <- map["interest_rate"]
        intermediary_bank_account_number <- map["intermediary_bank_account_number"]
        intermediary_bank_bic <- map["intermediary_bank_bic"]
        intermediary_bank_country <- map["intermediary_bank_country"]
        intermediary_bank_name <- map["intermediary_bank_name"]
        IntermediaryBankLoroAccountNo <- map["IntermediaryBankLoroAccountNo"]
        internal_bank_transfer <- map["internal_bank_transfer"]
        Internal_operation <- map["Internal_operation"]
        invalid_payment_code <- map["invalid_payment_code"]
        invalid_payment_code_entered <- map["invalid_payment_code_entered"]
        invalid_recipient_account <- map["invalid_recipient_account"]
        invalid_recipient_bik <- map["invalid_recipient_bik"]
        invalid_requisite <- map["invalid_requisite"]
        is_done <- map["is_done"]
        issued <- map["issued"]
        IssueDate <- map["IssueDate"]
        kilometers_dot <- map["kilometers_dot"]
        kirghiz_language <- map["kirghiz_language"]
        language_settings <- map["language_settings"]
        last_operation <- map["last_operation"]
        last_operations <- map["last_operations"]
        LastOperationDate <- map["LastOperationDate"]
        Later <- map["Later"]
        less_than_one_km <- map["less_than_one_km"]
        list <- map["list"]
        loading <- map["loading"]
        loan_account_statement <- map["loan_account_statement"]
        loan_amount <- map["loan_amount"]
        loan_create_request <- map["loan_create_request"]
        loan_of_account <- map["loan_of_account"]
        loan_repayment <- map["loan_repayment"]
        loan_summ <- map["loan_summ"]
        LoanCreateRequest <- map["LoanCreateRequest"]
        LoanDetailInfo <- map["LoanDetailInfo"]
        LoanIndex <- map["LoanIndex"]
        LoanIsOverdueMessage <- map["LoanIsOverdueMessage"]
        LoanNotCalculationStatusMessage <- map["LoanNotCalculationStatusMessage"]
        LoanProduct <- map["LoanProduct"]
        LoanState <- map["LoanState"]
        LoanStopCalculationStatusMessage <- map["LoanStopCalculationStatusMessage"]
        location_permission_text <- map["location_permission_text"]
        Lock <- map["Lock"]
        LockReason <- map["LockReason"]
        LockUnlock <- map["LockUnlock"]
        log_in <- map["log_in"]
        login <- map["login"]
        login_to_mobile_bank <- map["login_to_mobile_bank"]
        LoginRequired <- map["LoginRequired"]
        logout <- map["logout"]
        look <- map["look"]
        main <- map["main"]
        main_dot_summ <- map["main_dot_summ"]
        MainSumm <- map["MainSumm"]
        MoneyTransfers <- map["MoneyTransfers"]
        MoneyTransfer <- map["MoneyTransfer"]
        manage_users <- map["manage_users"]
        map_information <- map["map_information"]
        map_title <- map["map_title"]
        meters_dot <- map["meters_dot"]
        MFO <- map["MFO"]
        minimal_sum <- map["minimal_sum"]
        MinPaymentDate <- map["MinPaymentDate"]
        mon <- map["mon"]
        month <- map["month"]
        month_day <- map["month_day"]
        more <- map["more"]
        more_than_one_km <- map["more_than_one_km"]
        mortrage_type <- map["mortrage_type"]
        my_finances <- map["my_finances"]
        my_templates <- map["my_templates"]
        n_of_operation <- map["n_of_operation"]
        NameTitle <- map["NameTitle"]
        near_you <- map["near_you"]
        NewPasswordRequired <- map["NewPasswordRequired"]
        next_repayment <- map["next_repayment"]
        NextPayDate <- map["NextPayDate"]
        no_active_accounts_found <- map["no_active_accounts_found"]
        no_data <- map["no_data"]
        no_operations_for_the_selected_period <- map["no_operations_for_the_selected_period"]
        not_defined <- map["not_defined"]
        not_interactive <- map["not_interactive"]
        not_planned <- map["not_planned"]
        not_selected <- map["not_selected"]
        nothing_found_change_search_criteria <- map["nothing_found_change_search_criteria"]
        notifications <- map["notifications"]
        NotRequired <- map["NotRequired"]
        Number <- map["Number"]
        number_of_pp <- map["number_of_pp"]
        offices_and_terminals <- map["offices_and_terminals"]
        only_1_time <- map["only_1_time"]
        open_date <- map["open_date"]
        open_deposit <- map["open_deposit"]
        open_link_in_safari <- map["open_link_in_safari"]
        OpenDepositTitle <- map["OpenDepositTitle"]
        opening_deposit <- map["opening_deposit"]
        operation_by_template <- map["operation_by_template"]
        operation_complete <- map["operation_complete"]
        operation_date <- map["operation_date"]
        operation_from_template <- map["operation_from_template"]
        operation_history <- map["operation_history"]
        operation_list <- map["operation_list"]
        OperationSystem <- map["OperationSystem"]
        operation_name <- map["operation_name"]
        operation_status <- map["operation_status"]
        operation_type <- map["operation_type"]
        OperationConfirmationMessager <- map["OperationConfirmationMessager"]
        OperationConfirmRemove <- map["OperationConfirmRemove"]
        OperationFormatClearingGross <- map["OperationFormatClearingGross"]
        OperationFormatInternal <- map["OperationFormatInternal"]
        OperationImportInfo <- map["OperationImportInfo"]
        OperationOnPage <- map["OperationOnPage"]
        operations_from <- map["operations_from"]
        operations_to <- map["operations_to"]
        or_more_characters <- map["or_more_characters"]
        out_balance <- map["out_balance"]
        OutBalance <- map["OutBalance"]
        Outgoing <- map["Outgoing"]
        overdue_days <- map["overdue_days"]
        OverdueDays <- map["OverdueDays"]
        partial_early_repayment <- map["partial_early_repayment"]
        passcode_is_not_set_on_the_device <- map["passcode_is_not_set_on_the_device"]
        partial_early_repayment_of_principal <- map["partial_early_repayment_of_principal"]
        password <- map["password"]
        PasswordAverage <- map["PasswordAverage"]
        PasswordExpiredMessage <- map["PasswordExpiredMessage"]
        PasswordGood <- map["PasswordGood"]
        PasswordRequired <- map["PasswordRequired"]
        PasswordsNotEquals <- map["PasswordsNotEquals"]
        PasswordSuccessfullyChanged <- map["PasswordSuccessfullyChanged"]
        PasswordWeak <- map["PasswordWeak"]
        pay_from <- map["pay_from"]
        pay_sum <- map["pay_sum"]
        PayDate <- map["PayDate"]
        payment <- map["payment"]
        payment_amount <- map["payment_amount"]
        payment_code <- map["payment_code"]
        payment_comment <- map["payment_comment"]
        payment_n <- map["payment_n"]
        payment_purpose_codes <- map["payment_purpose_codes"]
        payment_sum <- map["payment_sum"]
        PaymentCommission <- map["PaymentCommission"]
        PaymentDirection <- map["PaymentDirection"]
        PaymentParameters <- map["PaymentParameters"]
        PaymentSaveSuccess <- map["PaymentSaveSuccess"]
        PaymentsFrom <- map["PaymentsFrom"]
        PayUtilities <- map["PayUtilities"]
        percent_account_statement <- map["percent_account_statement"]
        percent_rate <- map["percent_rate"]
        percent_repayment <- map["percent_repayment"]
        percents_to_pay <- map["percents_to_pay"]
        PercentsSumm <- map["PercentsSumm"]
        perform_an_operation_on <- map["perform_an_operation_on"]
        PerformChangeButton <- map["PerformChangeButton"]
        PerformLogin <- map["PerformLogin"]
        Period <- map["Period"]
        period_and_percent <- map["period_and_percent"]
        period_of_operations <- map["period_of_operations"]
        phone_for_information <- map["phone_for_information"]
        pin_not_confirmed <- map["pin_not_confirmed"]
        PinRequired <- map["PinRequired"]
        planned_maturity_date <- map["planned_maturity_date"]
        planned_payment <- map["planned_payment"]
        please_enter_a_number_greater_than_or_equal_to <- map["please_enter_a_number_greater_than_or_equal_to"]
        please_enter_a_number_less_than_or_equal_to <- map["please_enter_a_number_less_than_or_equal_to"]
        pos_terminals <- map["pos_terminals"]
        press_back_again_to_exit <- map["press_back_again_to_exit"]
        principal_balance <- map["principal_balance"]
        principal_repayment <- map["principal_repayment"]
        Print <- map["Print"]
        ProcessDay <- map["ProcessDay"]
        Product <- map["Product"]
        ProductName <- map["ProductName"]
        props_number <- map["props_number"]
        RateWarning <- map["RateWarning"]
        receipt <- map["receipt"]
        receiver <- map["receiver"]
        receiver_bank_account_number <- map["receiver_bank_account_number"]
        receiver_bank_bic <- map["receiver_bank_bic"]
        receiver_bank_country_code <- map["receiver_bank_country_code"]
        receiver_bank_name <- map["receiver_bank_name"]
        recipient_account <- map["recipient_account"]
        recipients_card_number <- map["recipients_card_number"]
        recommendations_for_a_new_password <- map["recommendations_for_a_new_password"]
        recurring_type <- map["recurring_type"]
        Refresh <- map["Refresh"]
        refuse <- map["refuse"]
        Region <- map["Region"]
        reject_reason <- map["reject_reason"]
        reject_request <- map["reject_request"]
        RejectConfirmMessage <- map["RejectConfirmMessage"]
        RejectConfirmTitle <- map["RejectConfirmTitle"]
        repayment <- map["repayment"]
        repayment_amount <- map["repayment_amount"]
        repeat_new_password <- map["repeat_new_password"]
        repeat_pin <- map["repeat_pin"]
        replenish_deposit <- map["replenish_deposit"]
        replenishment <- map["replenishment"]
        RequestLoansTitle <- map["RequestLoansTitle"]
        Required <- map["Required"]
        requisite <- map["requisite"]
        resend_sms <- map["resend_sms"]
        ResendSmsCode <- map["ResendSmsCode"]
        reset_counter_of_entered_pin_codes <- map["reset_counter_of_entered_pin_codes"]
        reset_filters <- map["reset_filters"]
        Resources_Auth_Common_UserName <- map["Resources_Auth_Common_UserName"]
        Resources_ClearingGross_Common_Clearing <- map["Resources_ClearingGross_Common_Clearing"]
        Resources_ClearingGross_Common_Gross <- map["Resources_ClearingGross_Common_Gross"]
        Resources_ClearingGross_Common_PaymentDate <- map["Resources_ClearingGross_Common_PaymentDate"]
        Resources_ClearingGross_Common_PaymentNumber <- map["Resources_ClearingGross_Common_PaymentNumber"]
        Resources_ClearingGross_Common_PaymentType <- map["Resources_ClearingGross_Common_PaymentType"]
        Resources_ClearingGross_Common_SenderFullName <- map["Resources_ClearingGross_Common_SenderFullName"]
        Resources_ClearingGross_Common_Title <- map["Resources_ClearingGross_Common_Title"]
        Resources_ClearingGross_Operations_PaymentCode <- map["Resources_ClearingGross_Operations_PaymentCode"]
        Resources_ClearingGross_Operations_ReceiverFullName <- map["Resources_ClearingGross_Operations_ReceiverFullName"]
        Resources_Common_AccountName <- map["Resources_Common_AccountName"]
        Resources_Common_Calculate <- map["Resources_Common_Calculate"]
        Resources_Common_Comment <- map["Resources_Common_Comment"]
        Resources_Common_FromAccount <- map["Resources_Common_FromAccount"]
        Resources_Common_Purpose <- map["Resources_Common_Purpose"]
        Resources_Common_Save <- map["Resources_Common_Save"]
        Resources_Common_Send <- map["Resources_Common_Send"]
        Resources_Deposits_Conversion_BuyAccount <- map["Resources_Deposits_Conversion_BuyAccount"]
        Resources_Deposits_Conversion_BuySum <- map["Resources_Deposits_Conversion_BuySum"]
        Resources_Deposits_DepositAccounts_Conversion <- map["Resources_Deposits_DepositAccounts_Conversion"]
        Resources_Internal_Customer_Transaction <- map["Resources_Internal_Customer_Transaction"]
        Resources_Internal_Transaction <- map["Resources_Internal_Transaction"]
        Resources_Swift_Swift_ReceiverAccountNo <- map["Resources_Swift_Swift_ReceiverAccountNo"]
        Resources_Swift_Swift_ReceiverBankBic <- map["Resources_Swift_Swift_ReceiverBankBic"]
        Resources_Swift_Swift_TransferSum <- map["Resources_Swift_Swift_TransferSum"]
        Resources_Swift_Swift_DateVal <- map["Resources_Swift_Swift_DateVal"]
        DatePPFull <- map["DatePPFull"]
        currency_rate <- map["currency_rate"]
        Resources_Utilities_Pay <- map["Resources_Utilities_Pay"]
        retry <- map["retry"]
        retry_operation <- map["retry_operation"]
        retry_query <- map["retry_query"]
        RunCommand <- map["RunCommand"]
        russian_language <- map["russian_language"]
        SameAccountError <- map["SameAccountError"]
        sat <- map["sat"]
        save <- map["save"]
        save_as_template <- map["save_as_template"]
        save_template <- map["save_template"]
        SaveSuccess <- map["SaveSuccess"]
        schedule_auto_payment <- map["schedule_auto_payment"]
        scheduled <- map["scheduled"]
        ScheduleDateEnd <- map["ScheduleDateEnd"]
        ScheduleDateStart <- map["ScheduleDateStart"]
        search <- map["search"]
        select_a_category <- map["select_a_category"]
        select_account_or_card <- map["select_account_or_card"]
        select_confirmation_form <- map["select_confirmation_form"]
        select_credit <- map["select_credit"]
        select_currency <- map["select_currency"]
        select_language <- map["select_language"]
        select_operation_type <- map["select_operation_type"]
        select_service_category <- map["select_service_category"]
        select_the_type_of_collateral <- map["select_the_type_of_collateral"]
        SelectAccount <- map["SelectAccount"]
        SelectApproveType <- map["SelectApproveType"]
        SelectCodeVO <- map["SelectCodeVO"]
        SelectCurrency <- map["SelectCurrency"]
        SelectCurrentAccountMessage <- map["SelectCurrentAccountMessage"]
        SelectMortrageType <- map["SelectMortrageType"]
        SelectProductType <- map["SelectProductType"]
        selling <- map["selling"]
        send_request <- map["send_request"]
        send_sms_again_after <- map["send_sms_again_after"]
        send_to_bank <- map["send_to_bank"]
        SenderBank <- map["SenderBank"]
        SenderBankBic <- map["SenderBankBic"]
        SenderBankCountry <- map["SenderBankCountry"]
        SenderBankLoroAccountNo <- map["SenderBankLoroAccountNo"]
        sent_to_bank <- map["sent_to_bank"]
        Service <- map["Service"]
        service_categories_caps <- map["service_categories_caps"]
        service_providers_caps <- map["service_providers_caps"]
        ServiceNotFound <- map["ServiceNotFound"]
        Services <- map["Services"]
        settings <- map["settings"]
        show_on_the_map <- map["show_on_the_map"]
        show_only_issued <- map["show_only_issued"]
        show_only_opened <- map["show_only_opened"]
        show_operations <- map["show_operations"]
        skip <- map["skip"]
        sms_code_command <- map["sms_code_command"]
        sms_with_code_was_sent_to <- map["sms_with_code_was_sent_to"]
        SmsCodeRequired <- map["SmsCodeRequired"]
        start_day <- map["start_day"]
        statement_of_interest_account <- map["statement_of_interest_account"]
        StatementFrom <- map["StatementFrom"]
        status <- map["status"]
        status_date <- map["status_date"]
        SuccessSaveTemplate <- map["SuccessSaveTemplate"]
        sum_above_max_unavailable_with_clearing <- map["sum_above_max_unavailable_with_clearing"]
        гросс <- map["гросс"]
        sum_must_be_greater <- map["sum_must_be_greater"]
        swift <- map["swift"]
        swift_payment <- map["swift_payment"]
        swift_transfer <- map["swift_transfer"]
        swift_transfer_no_hyphen <- map["swift_transfer_no_hyphen"]
        SwiftTransferList <- map["SwiftTransferList"]
        TargetGroup <- map["TargetGroup"]
        tax <- map["tax"]
        template_description <- map["template_description"]
        template_name <- map["template_name"]
        TemplateConfirmRemove <- map["TemplateConfirmRemove"]
        TemplateDescription <- map["TemplateDescription"]
        TemplateList <- map["TemplateList"]
        TemplateName <- map["TemplateName"]
        templates <- map["templates"]
        TemplateSchedule <- map["TemplateSchedule"]
        th <- map["th"]
        the_context_is_invalid <- map["the_context_is_invalid"]
        the_amount_of_the_operation_can_not_be_more_than_the_current_balance <- map["the_amount_of_the_operation_can_not_be_more_than_the_current_balance"]
        the_list_of_operations_is_empty <- map["the_list_of_operations_is_empty"]
        the_operation_for_the_selected_date_is_not_available <- map["the_operation_for_the_selected_date_is_not_available"]
        the_products_name <- map["the_products_name"]
        the_sun <- map["the_sun"]
        the_user_failed_to_provide_valid_credentials <- map["the_user_failed_to_provide_valid_credentials"]
        to_account <- map["to_account"]
        to_pay <- map["to_pay"]
        to_repeat <- map["to_repeat"]
        total <- map["total"]
        total_amount <- map["total_amount"]
        total_for_payment <- map["total_for_payment"]
        total_for_transfer <- map["total_for_transfer"]
        total_to_pay <- map["total_to_pay"]
        TotalSumm <- map["TotalSumm"]
        Tranche <- map["Tranche"]
        TrancheAdditionalInfo <- map["TrancheAdditionalInfo"]
        Tranches <- map["Tranches"]
        transaction_number <- map["transaction_number"]
        transfer <- map["transfer"]
        transfer_between_accounts <- map["transfer_between_accounts"]
        transfer_sum_buy <- map["transfer_sum_buy"]
        transfer_to <- map["transfer_to"]
        transfer_to_account <- map["transfer_to_account"]
        transfer_to_card_prefix <- map["transfer_to_card_prefix"]
        transfer_to_client_account <- map["transfer_to_client_account"]
        transfer_to_sum <- map["transfer_to_sum"]
        TransferParameters <- map["TransferParameters"]
        transferring <- map["transferring"]
        transfers <- map["transfers"]
        TransferSaveSuccess <- map["TransferSaveSuccess"]
        translation_commentary <- map["translation_commentary"]
        tue <- map["tue"]
        Unlock <- map["Unlock"]
        UploadOperationsExcel <- map["UploadOperationsExcel"]
        uppercase_and_lowercase_latin_letters <- map["uppercase_and_lowercase_latin_letters"]
        use <- map["use"]
        use_iphone_password <- map["use_iphone_password"]
        use_touch_id_or_face_id_for_quick_access <- map["use_touch_id_or_face_id_for_quick_access"]
        utilities <- map["utilities"]
        about_app <- map["about_app"]
        help_support <- map["help_support"]
        utilities_pay_format <- map["utilities_pay_format"]
        utilities_pay_status <- map["utilities_pay_status"]
        utilities_payment <- map["utilities_payment"]
        utilities_provider <- map["utilities_provider"]
        utility_payment <- map["utility_payment"]
        utility_payment_repayment <- map["utility_payment_repayment"]
        utility_provider <- map["utility_provider"]
        utlitity_category <- map["utlitity_category"]
        ValidateMaxAmount <- map["ValidateMaxAmount"]
        ValidateMessageGreaterThanZero <- map["ValidateMessageGreaterThanZero"]
        ValidateMinAmount <- map["ValidateMinAmount"]
        verification_code <- map["verification_code"]
        Verify <- map["Verify"]
        View <- map["View"]
        view_loan_application <- map["view_loan_application"]
        waiver_of_application <- map["waiver_of_application"]
        wed <- map["wed"]
        week_day <- map["week_day"]
        whole_story <- map["whole_story"]
        WillBeCharged <- map["WillBeCharged"]
        WillBeCredited <- map["WillBeCredited"]
        withdrawal <- map["withdrawal"]
        work_schedule <- map["work_schedule"]
        write_off <- map["write_off"]
        write_off_account <- map["write_off_account"]
        write_off_from_account <- map["write_off_from_account"]
        write_off_sum <- map["write_off_sum"]
        write_off_sum_buy <- map["write_off_sum_buy"]
        wrong_pin_code <- map["wrong_pin_code"]
        you_have_successfully_confirmed_the_sms_code_to_complete_the_payment <- map["you_have_successfully_confirmed_the_sms_code_to_complete_the_payment"]
        you_have_successfully_confirmed_your_phone_number <- map["you_have_successfully_confirmed_your_phone_number"]
        YouDontHaveAccounts <- map["YouDontHaveAccounts"]
        YouDontHaveCardAccounts <- map["YouDontHaveCardAccounts"]
        YouDontHaveDeposits <- map["YouDontHaveDeposits"]
        YourPasswordWillExpiredIn <- map["YourPasswordWillExpiredIn"]
        YourPasswordWillExpiredSoon <- map["YourPasswordWillExpiredSoon"]
        zoom_in_to_view_markers <- map["zoom_in_to_view_markers"]
        Resources_Swift_Swift_ReceiverAccountNo_Placeholder <- map["Resources_Swift_Swift_ReceiverAccountNo_Placeholder"]
    }
    
}

