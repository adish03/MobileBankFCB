//
//  Currencies.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/6/19.
//

import ObjectMapper
//  модель CurrencyRate
open class CurrencyRate: Mappable {
    
    open var currencyID:Int?,
    crossCurrencyID: Int?,
    sellRate: Double?,
    buyRate: Double?,
    diffSellRate:  Double?,
    diffBuyRate:  Double?,
    currency:Currency?,
    crossCurrency: Currency?
    
    
    init(){}
    required public init(currencyID:Int?,
                         crossCurrencyID: Int?,
                         sellRate: Double?,
                         buyRate: Double?,
                         diffSellRate:  Double?,
                         diffBuyRate:  Double?,
                         currency:Currency?,
                         crossCurrency: Currency?) {
        self.currencyID = currencyID
        self.crossCurrencyID = crossCurrencyID
        self.sellRate = sellRate
        self.buyRate = buyRate
        self.diffSellRate = diffSellRate
        self.diffBuyRate = diffBuyRate
        self.currency = currency
        self.crossCurrency = crossCurrency
    }
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        currencyID <- map["CurrencyID"]
        crossCurrencyID <- map["CrossCurrencyID"]
        sellRate <- map["SellRate"]
        buyRate <- map["BuyRate"]
        diffSellRate <- map["DiffSellRate"]
        diffBuyRate <- map["DiffBuyRate"]
    }
}
//  модель Currency
open class Currency: Mappable {
    
    open var
    currencyID:Int?,
    currencyName: String?,
    symbol: String?,
    name1: String?,
    name2: String?,
    name3: String?,
    nameC1: String?,
    nameC2: String?,
    nameC3: String?,
    countryID: String?,
    genderTypeID: Int?,
    currencyRate: CurrencyRate?
    
    
    init(){}
    required public init(currencyID:Int?,
                         currencyName: String?,
                         symbol: String?,
                         name1: String?,
                         name2: String?,
                         name3: String?,
                         nameC1: String?,
                         nameC2: String?,
                         nameC3: String?,
                         countryID: String?,
                         genderTypeID: Int?,
                         currencyRate: CurrencyRate?) {
        self.currencyID = currencyID
        self.currencyName = currencyName
        self.symbol = symbol
        self.name1 = name1
        self.name2 = name2
        self.name3 = name3
        self.nameC1 = nameC1
        self.nameC2 = nameC2
        self.nameC3 = nameC3
        self.countryID = countryID
        self.genderTypeID = genderTypeID
        self.currencyRate = currencyRate
    }
    required public init(currencyID:Int?,
                         currencyName: String?,
                         symbol: String?
        ){
        self.currencyID = currencyID
        self.currencyName = currencyName
        self.symbol = symbol
    }
    
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        currencyID <- map["CurrencyID"]
        currencyName <- map["CurrencyName"]
        symbol <- map["Symbol"]
        name1 <- map["Name1"]
        name2 <- map["Name1"]
        name3 <- map["Name1"]
        nameC1 <- map["NameC1"]
        nameC2 <- map["NameC1"]
        nameC3 <- map["NameC1"]
        countryID <- map["CountryID"]
        genderTypeID <- map["GenderTypeID"]
    }
    
    //    функция для создания копии справочника городов X-Finca
    public func copy(with zone: NSZone? = nil) -> Any {
        let copy = Currency(currencyID: currencyID, currencyName: currencyName, symbol: symbol, name1: name1, name2: name2, name3: name3, nameC1: nameC1, nameC2: nameC2, nameC3: nameC3, countryID: countryID, genderTypeID: genderTypeID, currencyRate: currencyRate)
        return copy
    }
}

//  модель CurrencyNational
open class CurrencyNational: Mappable {
    
    open var
    currencyID:Int?,
    currencyName: String?,
    symbol: String?,
    
    currency:Currency?
    
    
    init(){}
    required public init?(map: Map) {}
    
    public func mapping(map: Map) {
        currencyID <- map["CurrencyID"]
        currencyName <- map["currencyName"]
        symbol <- map["Symbol"]
    }
}
