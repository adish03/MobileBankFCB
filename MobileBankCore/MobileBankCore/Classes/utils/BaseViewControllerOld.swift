//
//  BaseViewController.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//
//
import UIKit
import RxSwift
import SVProgressHUD


open class BaseViewControllerOld: UIViewController, BaseViewControllerProtocol{
    public var alertController: UIAlertController?
    public func showAlert(withError e: Error) {
        (self as BaseViewControllerProtocol).showAlert(withError: e)
    }
//override var preferredStatusBarStyle: UIStatusBarStyle{return .lightContent}

    override open func viewDidLoad() {
        super.viewDidLoad()
        Logger.logViewDidLoad(self)

    }

    public var PhoneIdVendor = UIDevice.current.identifierForVendor!.uuidString
    let disposeBag = DisposeBag()
    func refresh(){}
    deinit {
        Logger.logDeinit(self)
    }

    public func showLoading(){
        SVProgressHUD.show()
    }
    public func hideLoading(){
        SVProgressHUD.dismiss()
    }

    open func showController(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)

    }
    open func showControllerWithoutAnimation(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)

    }
    open func pushController(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    open func popControllerWithoutAnimation(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        _ = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        self.navigationController?.popViewController(animated: false)
    }
    open func pushControllerWithoutAnimation(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    open func alertNotRegistrationUser(error: String){
        let alert = UIAlertController(title: error, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }

    open func showAlertController(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }

}
protocol BaseViewControllerProtocol: class {

    var disposeBag:DisposeBag {get}
    var alertController: UIAlertController? {get set}
    func showAlert(withError e:Error)
    func showAlert(withError e:Error, shouldRefresh:Bool, canGoBack:Bool)
    //for overriding
    func refresh()
    func showLoading()
    func hideLoading()
}


extension BaseViewControllerProtocol where Self: UIViewController{

    func showAlert(withError e:Error){
        showAlert(withError: e, shouldRefresh: false, canGoBack: false)
    }
    func showAlert(withError e:Error, shouldRefresh:Bool, canGoBack:Bool){
        hideLoading()
        self.alertController?.dismiss(animated: false, completion: nil)
        let alertController = UIAlertController(title: "ERROR".localized,
                                                message: e.localizedDescription + "\n" + (e.apiErrors?.stringDescription ?? ""),
                                                preferredStyle: .alert)
        if shouldRefresh{
            alertController
                .addAction(UIAlertAction(title: "TRY_AGAIN".localized,
                                         style: UIAlertAction.Style.default,
                                         handler: {_ in
                                            self.refresh()
                }))
            if canGoBack{
                alertController
                    .addAction(UIAlertAction(title: "CLOSE".localized,
                                             style: UIAlertAction.Style.default,
                                             handler: {_ in
                                                self.dismissVC()
                    }))
            }
        } else {
            alertController
                .addAction(UIAlertAction(title: "CLOSE".localized,
                                         style: UIAlertAction.Style.default,
                                         handler: nil))
        }
        present(alertController, animated: true, completion: nil)
        self.alertController = alertController
    }



    func showLoading(){
        SVProgressHUD.show()
    }
    func hideLoading(){
        SVProgressHUD.dismiss()
    }

    fileprivate func dismissVC(){
        if let navVc = navigationController{
            navVc.popViewController(animated: true)
        } else{
            dismiss(animated: true, completion: nil)
        }
    }
}
