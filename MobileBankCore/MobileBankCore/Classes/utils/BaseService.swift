//
//  BaseService.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import RxSwift
import ObjectMapper
import Alamofire
import RxAlamofire
import SVProgressHUD

open class BaseService {
    
    init(disableEvaluationUrl:String) {       
        // Create the server trust policies
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            disableEvaluationUrl: .disableEvaluation  //тестовый
        ]
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        managerApi = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
    }
    
    // ManagerApi для запросов
    private var managerApi : Alamofire.SessionManager
    var encoding:ParameterEncoding{ get{ URLEncoding.default } }
    
    func createRequest(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<String>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return Alamofire.SessionManager.default.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .rx
            .stringWithBodyInError()
    }
    
    func createRequest<T:Mappable>(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<T>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return Alamofire.SessionManager.default.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .rx
            .mappable()
    }
    func createRequest<T:Mappable>(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<[T]>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return Alamofire.SessionManager.default.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .rx
            .mappableArray()
    }
    
    func createRequest<T:Mappable>(data:Data,url:String) -> Observable<T>{
        Logger.logRequest(url: url, parameters: ["data" : String(describing: data)], headers: nil)
        return Alamofire.upload(
            data,
            to: url)
            .rx
            .mappable()
    }
    func createRequest<T:Mappable>(fileUrl:URL,url:String) -> Observable<T>{
        Logger.logRequest(url: url, parameters: ["fileUrl" : fileUrl.absoluteString], headers: nil)
        return Alamofire.upload(
            fileUrl,
            to: url)
            .rx
            .mappable()
    }
    
    
    
    //MARK Request witout certificate==================================
    func createRequestWithoutCertificate<T:Mappable> (
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<T>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return managerApi.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .validate()
            .rx
            .mappable()
    }
    
    func createRequestWithoutCertificate<T:Codable>(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<T>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return managerApi.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .validate()
            .rx
            .codable()
    }
    
    func createRequest<T:Codable>(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding? = nil) -> Observable<T>{
        
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        managerApi.session.configuration.timeoutIntervalForRequest = 120
        
        return managerApi.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding ?? self.encoding,
            headers: headers)
            .rx
            .codable()
    }
    
    //url encoded
    func createRequest<T:Codable>(data:Data,url:String) -> Observable<T>{
        Logger.logRequest(url: url, parameters: ["Data to upload:" : String(describing: data)], headers: nil)
        return managerApi.upload(
            data,
            to: url)
            .rx
            .codable()
    }
    func createRequest<T:Codable>(fileUrl:URL,url:String) -> Observable<T>{
        Logger.logRequest(url: url, parameters: ["FileUrl to upload:" : fileUrl.absoluteString], headers: nil)
        return managerApi.upload(
            fileUrl,
            to: url)
            .rx
            .codable()
    }
    
    
//    создание запроса со строкой в body
    func createRequestWithoutCertificateBodyString(
        url:String,
        body: String,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get) -> Observable<String>{
        
        return managerApi.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: [:],
            encoding: body,
            headers: headers)
            .validate()
            .rx
            .stringWithBodyInError()
    }
//  создание запроса при получении строки с ошибкой
    func createRequestWithoutCertificate(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<String>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return managerApi.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .validate()
            .rx
            .stringWithBodyInError()
    }
    
    

    
    func createRequestWithoutCertificate(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<Data>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return managerApi.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .validate()
            .rx
            .data()
    }
    
    func createRequestWithoutCertificate<T:Mappable>(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<[T]>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return managerApi.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .validate()
            .rx
            .mappableArray()
    }
//  раширение для RxAlamofire, получение массива строк
    func createRequestWithoutCertificate(
        url:String,
        parameters: [String: Any]? = nil,
        headers: HTTPHeaders? = nil,
        method: HTTPMethod = .get,
        encoding: ParameterEncoding = URLEncoding.default) -> Observable<[String]>{
        Logger.logRequest(url: url, parameters: parameters, headers: headers, method: method)
        
        return managerApi.request(
            url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) ?? "",
            method: method,
            parameters: parameters,
            encoding: encoding,
            headers: headers)
            .validate()
            .rx
            .mappableArrayString()
    }
   
}



extension BaseService{
    public func showLoading(){
        SVProgressHUD.show()
    }
    public func hideLoading(){
        SVProgressHUD.dismiss()
    }
    open func showController(StoryBoard: String, ViewControllerID: String, controller: UIViewController){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        vc.modalPresentationStyle = .fullScreen
        controller.present(vc, animated: true, completion: nil)
        
    }
}

extension String: ParameterEncoding {
    
    public func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var request = try urlRequest.asURLRequest()
        request.httpBody = data(using: .utf8, allowLossyConversion: false)
        return request
    }
    
}
