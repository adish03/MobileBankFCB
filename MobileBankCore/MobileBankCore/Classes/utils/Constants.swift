//
//  Constants.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import Foundation

open class Constants{
    // максимальная сумма для ClearingGross
    public static var SUM_CLEARING_GROS:Int{
        get {
            return UserDefaultsHelper.clearingMaxAmount ?? 0
        }
    }
    //  количество операций на main page
    public static var MAIN_PAGE_OPERATION_COUNT:Int{
        get {
            return  UserDefaultsHelper.mainPageOperationCount ?? 0
        }
    }
    //  количество символов в счете
    public static var ACCOUNTS_CHARACTERS_COUNT:Int{
        get {
            return  UserDefaultsHelper.AccountLength ?? 0
        }
    }
    // Base URL
    public static var API_MAIN:String{
        get {
            return ApiStringFromConfigFile()
        }
    }
    // Base URL
    public static var API_MAIN_URL:String{
        get {
            return ApiUrlStringFromConfigFile()
        }
    }
    
    // Base BGColor
    public static var COLOR_BACKGROUND:String{
        get {
            return hexColorBackgroundFromConfigFile()
        }
    }
    // Base MainColor
    public static var MAIN_COLOR:String{
        get {
            return hexMainColorFromConfigFile()
        }
    }
    //  количество цифр в pin code
    public static var PIN_LENGTH:Int{
        get {
            return  UserDefaultsHelper.PinLength ?? 0
        }
    }
    public static let
    NATIONAL_CURRENCY = 417
    
    public static let
    
    SHORT_DATE_FORMAT = "MMM dd, yyyy",
    API_DATE_FORMAT = "dd-MM-yyyy",
    
    VIDEOS_DIRECTORY = NSTemporaryDirectory() + "item_videos/",
    MAX_VIDEO_PER_ITEM = 1,
    MAX_PHOTO_PER_ITEM = 3,
    
    ANIMATION_DURATION = 0.3,
    OBJ_RECOG_ENABLED = true,
    ARKIT_ENABLED = false
    
    // получение данных с ConfigFile
    static func ApiStringFromConfigFile() -> String {
        var APIString = ""
        let path: String = Bundle.main.path(forResource: "ConfigFile", ofType: "plist")!
        let sampleConf: NSDictionary = NSDictionary(contentsOfFile: path)!
        APIString = sampleConf.object(forKey: "API_MAIN") as! String
        return APIString
    }
    // получение данных с ConfigFile
    static func ApiUrlStringFromConfigFile() -> String {
        var APIString = ""
        let path: String = Bundle.main.path(forResource: "ConfigFile", ofType: "plist")!
        let sampleConf: NSDictionary = NSDictionary(contentsOfFile: path)!
        APIString = sampleConf.object(forKey: "API_MAIN_URL") as! String
        return APIString
    }
    // получение данных с ConfigFile
    static func hexColorBackgroundFromConfigFile() -> String {
        var ColorBG = ""
        let path: String = Bundle.main.path(forResource: "ConfigFile", ofType: "plist")!
        let sampleConf: NSDictionary = NSDictionary(contentsOfFile: path)!
        ColorBG = sampleConf.object(forKey: "COLOR_BACKGROUND") as! String
        return ColorBG
    }
    // получение данных с ConfigFile
    static func hexMainColorFromConfigFile() -> String {
        var ColorBG = ""
        let path: String = Bundle.main.path(forResource: "ConfigFile", ofType: "plist")!
        let sampleConf: NSDictionary = NSDictionary(contentsOfFile: path)!
        ColorBG = sampleConf.object(forKey: "MAIN_COLOR") as! String
        return ColorBG
    }
    
}
