//
//  LocalizeViews.swift
//  Alamofire
//
//  Created by Oleg Ten on 5/31/19.
//

import Foundation

// расширение для UILabel, для переводов
extension UILabel{
    
    @IBInspectable var localizeKey: String{
        set {
            text = localizedText(key: newValue)
        }
        get{return ""}
    }
    @IBInspectable var localizeKeyWithColon: String{
        set {
            text = localizedText(key: newValue) + ":"
        }
        get{return ""}
    }
    @IBInspectable var Uppercase: String{
        set {
            text = localizedText(key: newValue).uppercased() + ":"
        }
        get{return ""}
    }
   
}

// расширение для UIButton, для переводов
extension UIButton{
    
    @IBInspectable var localizeKey: String{
        set {
            setTitle(localizedText(key: newValue), for: .normal)
        }
        get{return ""}
    }
    
}
// расширение для UIButton, для переводов
extension UITextField{
    
    @IBInspectable var localizeKey: String{
        set {
            text = localizedText(key: newValue)
        }
        get{return ""}
    }
    @IBInspectable var localizeKeyPlaceholder: String{
        set {
            placeholder = localizedText(key: newValue)
        }
        get{return ""}
    }
    
}
// расширение для UISearchBar, для переводов
extension UISearchBar{
    
    @IBInspectable var localizeKeyPlaceholder: String{
        set {
            placeholder = localizedText(key: newValue)
        }
        get{return ""}
    }
    
}

// переводы для строк по ключу
 func localizedText(key: String) -> String {
    var string = ""
    let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    let customPlistURL = docsBaseURL.appendingPathComponent("LocalizeStrings.plist")
    let dictRoot = NSDictionary(contentsOf: customPlistURL) as? [String : AnyObject]
    if let dict = dictRoot {
        string = dict[key] as? String ?? ""
    }
    return string
}
