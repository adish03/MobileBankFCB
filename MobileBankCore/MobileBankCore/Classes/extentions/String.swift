//
//  String.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import Foundation

extension String{
    public var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    var isBlank:Bool{
        let trimmed = trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmed.isEmpty
    }
}

extension String{
    func getHtml(withFontSize size:CGFloat) -> String{
        let helpers = try! String(contentsOfFile:  Bundle.main.path(forResource: "helpers", ofType: "css")!, encoding: .utf8)
        let typography = try! String(contentsOfFile:  Bundle.main.path(forResource: "typography", ofType: "css")!, encoding: .utf8)
        
        let body = self
        var html = "<!DOCTYPE html>"
        html += "<style type=\"text/css\">"
        html += helpers
        html += "</style>"
        html += "<style type=\"text/css\">"
        html += typography
        html += "</style>"
        html += "</head>"
        html += "<body>"
        html += body
        html += "</body>"
        html += "</html>"
        
        return html
    }
    func getFormattedTest(size:CGFloat) -> NSMutableAttributedString?{
        let input = getHtml(withFontSize: size)
        let result = try? NSMutableAttributedString(
            data: input.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil
        )
        return result ?? NSMutableAttributedString(string: input)
    }
    
    public func localizeWithFormat(arguments: CVarArg...) -> String{
        return String(format: self.localized, arguments: arguments)
    }
    
     public var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
}
//  Расширение для перевода даты в секунды, для сравнения дат
extension String {
    static let shortDateUS: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateStyle = .short
        return formatter
    }()
    public var shortDateUS: Date? {
        return String.shortDateUS.date(from: self)
    }
}





