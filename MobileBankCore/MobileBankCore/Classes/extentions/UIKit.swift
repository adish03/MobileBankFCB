//
//  UIKit.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import UIKit
import AVFoundation

extension UIView{
    public func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -15.0, 15.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    public func stateIfEmpty(selectedDeposit: Deposit?, view: UIView, labelError: UILabel){
        if selectedDeposit != nil {
            view.layer.borderColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = localizedText(key: "field_cannot_be_blank")
        }else{
            view.layer.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = ""
        }
    }
    
    public func stateAccountIfEmpty(deposit: Deposit?, view: UIView, labelError: UILabel){
        if deposit != nil{
            view.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = ""
        }else{
            view.layer.borderColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = localizedText(key:"select_account_or_card")
        }
        
    }
    
    // кастомизация UIView, закругление сторон
    public func roundView(view: UIView, imageView: UIImageView, imageName: String){
        
        imageView.contentMode = .scaleAspectFill
        view.layer.backgroundColor = UIColor.white.cgColor
        
        view.layer.cornerRadius = view.frame.size.height / 2
        view.clipsToBounds = true
        let image = UIImage(named: imageName)
        let tintedImage = image?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor =  UIColor(hexFromString: Constants.MAIN_COLOR)
        imageView.image = tintedImage
        
    }

}

extension UITextField{
    public func shakeIfEmpty() -> Bool{
        
        if text == nil || text == ""{
            shake()
            becomeFirstResponder()
            return true
        }
        return false
    }
    
    public func stateIfSizeIncorrect(view: UIView, maxTextSize: Int, labelError: UILabel){
        if text!.count > maxTextSize {
            view.layer.borderColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = "Назначение платежа должно содержать не более 140 символов."
            
        }else{
            view.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = ""
        }
    }

    public func stateIfEmpty(view: UIView, labelError: UILabel){
        if text == nil || text == "" || text == "0" || text == "0.00" || text == "0.0" {
            view.layer.borderColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = localizedText(key: "field_cannot_be_blank")
            
        }else{
            view.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = ""
        }
    }
    
    public func stateAccountIfEmptyForTextField(deposit: Deposit?, view: UIView, labelError: UILabel){
        if deposit != nil{
            view.layer.borderColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = ""
        }else{
            view.layer.borderColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            view.layer.borderWidth = 2.0
            labelError.text = localizedText(key:"select_account_or_card")
        }
        
    }
   
}

extension UIScrollView{
    public func scrollToBottom(animated: Bool = true) {
        if let tableView = self as? UITableView{
            
            if let tableFooterView = tableView.tableFooterView{
                tableView.scrollRectToVisible(convert(tableFooterView.bounds, from:tableFooterView), animated:animated)
            } else {
                let indexPath = NSIndexPath(row: tableView.numberOfRows(inSection: 0) - 1, section: 0)
                tableView.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: animated)
            }
        } else {
            if self.contentSize.height < self.bounds.size.height { return }
            let bottomOffset = CGPoint(x: 0, y: self.contentSize.height - self.bounds.size.height)
            self.setContentOffset(bottomOffset, animated: animated)
        }
    }
}

extension UIImage {
    public func imageWithColor(color: UIColor) -> UIImage? {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}

extension UIDevice {
   public static func vibrate() {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
    }

}
// расширение для получения цвета по hex
extension UIColor {
    public convenience init(hexFromString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt32 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}

