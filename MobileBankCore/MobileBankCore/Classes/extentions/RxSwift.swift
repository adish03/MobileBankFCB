//
//  RxSwift.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import RxSwift

extension ObservableType{
    public func observeOnBackground() -> Observable<E>{
        return observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
    }
    
    public func subscribeMain(onNext: @escaping ((Self.E) -> Swift.Void), onError:@escaping ((Error) -> Void)) -> Disposable{
        return observeOn(MainScheduler.instance)
            .subscribe(onNext: onNext, onError: onError)
    }
}



extension ObservableType{
    public func updateTokenIfNeeded() -> Observable<E>{
        return SessionManager.shared
            .getUpdateTokenIfNeedObservable()
            .flatMap{self}
    }
}
