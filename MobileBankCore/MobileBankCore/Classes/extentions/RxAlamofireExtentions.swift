//
//  RxAlamofireExtentions.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/4/19.
//

import Alamofire
import RxSwift
import ObjectMapper
import RxAlamofire

// расширение для библиотеки RxAlamofire для получения массива строк
extension Reactive where Base: DataRequest {
    public func mappable<T:Mappable>() -> Observable<T>{
        return stringWithBodyInError()
            .flatMap{ response -> Observable<T> in
                let response = response.isBlank ? "{}" : response
                return Observable.create{ observer in
                    print(response)
                    if let result = Mapper<T>().map(JSONString: response){
                        print(result)
                        observer.onNext(result)
                    } else {
                        let
                        userInfo = [NSLocalizedFailureReasonErrorKey: "JSON_PARSE_ERROR".localized],
                        error = NSError(domain: "com.google", code: 1, userInfo: userInfo)
                        observer.onError(error)
                    }
                    return Disposables.create()
                }
        }
    }
    
    public func codable<T:Codable>(decoder:JSONDecoder = JSONDecoder()) -> Observable<T>{
        return stringWithBodyInError()
            .flatMap{ response -> Observable<T> in
                return Observable.create{ observer in
                    if T.self == String.self{
                        observer.onNext(response as! T)
                    } else{
                        let response = response.isBlank ? "{}" : response
                        if
                            let jsonData = response.data(using: .utf8),
                            let result = try? decoder.decode(T.self,from: jsonData){
                            observer.onNext(result)
                        } else {
                            let
                            userInfo = [NSLocalizedFailureReasonErrorKey: "JSON_PARSE_ERROR".localized],
                            error = NSError(domain: "com.google", code: 1, userInfo: userInfo)
                            observer.onError(error)
                        }
                    }
                    return Disposables.create()
                }
        }
    }
    
    // расширение для библиотеки RxAlamofire для получения массива строк
    public func mappableArray<T:Mappable>() -> Observable<[T]>{
        return stringWithBodyInError()
            .flatMap{ response -> Observable<[T]> in
                let response = response.isBlank ? "{}" : response
                return Observable.create{ observer in
                    print(response)
                    if let result = Mapper<T>().mapArray(JSONString: response){
                        print(result)
                        observer.onNext(result)
                    } else {
                        let
                        userInfo = [NSLocalizedFailureReasonErrorKey: "JSON_PARSE_ERROR".localized],
                        error = NSError(domain: "com.com.google", code: 1, userInfo: userInfo)
                        observer.onError(error)
                    }
                    return Disposables.create()
                }
        }
    }
    // расширение для библиотеки RxAlamofire для получения массива строк
    public func mappableArrayString() -> Observable<[String]>{
        return stringWithBodyInError()
            .flatMap{ response -> Observable<[String]> in
                let response = response.isBlank ? "{}" : response
                return Observable.create{ observer in
                    print(response)
                    if let result = try? JSONSerialization.jsonObject(with: response.data(using: .utf8)!, options: []) as! [String]{
                        print(result)
                        observer.onNext(result)
                    } else {
                        let
                        userInfo = [NSLocalizedFailureReasonErrorKey: "JSON_PARSE_ERROR".localized],
                        error = NSError(domain: "com.com.google", code: 1, userInfo: userInfo)
                        observer.onError(error)
                    }
                    return Disposables.create()
                }
        }
    }
}
fileprivate let
FailureResponseBodyErrorKey = "FailureResponseBodyErrorKey",
FailureStatusCodeErrorKey   = "FailureStatusCodeErrorKey"
fileprivate extension Error{
    var responseBody:String?{
        return ((self as Any) as? NSError)?.userInfo[FailureResponseBodyErrorKey] as? String
    }
    var localizedFailureReason:String?{
        return ((self as Any) as? NSError)?.localizedFailureReason
    }
    var statusCode:NSInteger?{
        return ((self as Any) as? NSError)?.userInfo[FailureStatusCodeErrorKey] as? NSInteger
    }
}

fileprivate func getError() -> NSError{
    let
    userInfo = [NSLocalizedFailureReasonErrorKey: "JSON_PARSE_ERROR".localized],
    error = NSError(domain: "com.google", code: 1, userInfo: userInfo)
    return error
}

// расширение для библиотеки RxAlamofire 
extension Reactive where Base: DataRequest {
    func stringWithBodyInError(encoding: String.Encoding? = nil) -> Observable<String> {
        return resultWithBodyInError(responseSerializer: Base.stringResponseSerializer(encoding: encoding))
    }
    func resultWithBodyInError<T: DataResponseSerializerProtocol>(
        queue: DispatchQueue? = nil,
        responseSerializer: T)
        -> Observable<T.SerializedObject>{
            return Observable.create { observer in
                let dataRequest = self.validateSuccessfulResponseExt()
                    .response(queue: queue, responseSerializer: responseSerializer) { (packedResponse) -> Void in
                        Logger.logResponse(packedResponse:packedResponse)
                        switch packedResponse.result {
                        case .success(let result):
                            if let _ = packedResponse.response {
                                observer.on(.next(result))
                            }
                            else {
                                observer.on(.error(RxAlamofireUnknownError))
                            }
                            observer.on(.completed)
                        case .failure(let error):
                            //add body in to error.userInfo
                            var
                            nserror = (error as NSError),
                            userInfo = nserror.userInfo
                            userInfo.updateValue(error.localizedDescription, forKey: NSLocalizedFailureReasonErrorKey)
                            if let statusCode = packedResponse.response?.statusCode{
                                userInfo.updateValue(statusCode, forKey: FailureStatusCodeErrorKey)
                            }
                            if let data = packedResponse.data,
                                let string = String(data: data, encoding: String.Encoding.utf8){
                                userInfo.updateValue(string, forKey: FailureResponseBodyErrorKey)
                            }
                            let
                            newerror = NSError(domain: nserror.domain, code: nserror.code, userInfo: userInfo)
                            observer.on(.error(newerror as Error))
                        }
                }
                return Disposables.create {
                    dataRequest.cancel()
                }
            }
    }
    /// - returns: A validated request based on the status code
    func validateSuccessfulResponseExt() -> DataRequest {
        return self.base.validate(statusCode: 200 ..< 300)
    }
}

//расширение для обработки ошибок
extension Error{
    // обработка ошибок
    public var apiErrors:ApiErrors?{
        let errors = ApiErrors(JSONString: responseBody ?? "{}")
        errors?.statusCode = statusCode
        return errors
    }
     // обработка ошибок
    public var apiError:ApiError?{
        let error = ApiError(JSONString: responseBody ?? "{}")
        error?.statusCode = statusCode
        return error
    }
     // обработка ошибок
    public var pinError:PinError?{
        let error = PinError(JSONString: responseBody ?? "{}")
        error?.statusCode = statusCode
        return error
    }
    
}


