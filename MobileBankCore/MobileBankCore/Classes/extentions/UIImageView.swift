//
//  UIImageView.swift
//  Alamofire
//
//  Created by Oleg Ten on 3/6/19.
//

import Alamofire
import AlamofireImage


//cashing image
extension UIImageView {
    public func af_setImageWithCache(
        withURL url: URL,
        placeholderImage: UIImage? = nil,
        filter: ImageFilter? = nil,
        progress: ImageDownloader.ProgressHandler? = nil,
        progressQueue: DispatchQueue = DispatchQueue.main,
        imageTransition: ImageTransition = .noTransition,
        runImageTransitionIfCached: Bool = false,
        completion: ((DataResponse<UIImage>) -> Void)? = nil,
        isShowActivityIndicator: Bool = false
        )
    {
        af_setImage(
            withURLRequest: urlRequest(with: url),
            placeholderImage: placeholderImage,
            filter: filter,
            progress: progress,
            progressQueue: progressQueue,
            imageTransition: imageTransition,
            runImageTransitionIfCached: runImageTransitionIfCached,
            completion: completion,
            isShowActivityIndicator:isShowActivityIndicator
        )
    }
    
    public func af_setImage(
        withURL url: URL,
        placeholderImage: UIImage? = nil,
        filter: ImageFilter? = nil,
        progress: ImageDownloader.ProgressHandler? = nil,
        progressQueue: DispatchQueue = DispatchQueue.main,
        imageTransition: ImageTransition = .noTransition,
        runImageTransitionIfCached: Bool = false,
        completion: ((DataResponse<UIImage>) -> Void)? = nil,
        isShowActivityIndicator: Bool = false
        )
    {
        af_setImage(
            withURLRequest: urlRequest(with: url),
            placeholderImage: placeholderImage,
            filter: filter,
            progress: progress,
            progressQueue: progressQueue,
            imageTransition: imageTransition,
            runImageTransitionIfCached: runImageTransitionIfCached,
            completion: completion,
            isShowActivityIndicator:isShowActivityIndicator
        )
    }
    
    
    public func af_setImage(
        withURLRequest url: URLRequest,
        placeholderImage: UIImage? = nil,
        filter: ImageFilter? = nil,
        progress: ImageDownloader.ProgressHandler? = nil,
        progressQueue: DispatchQueue = DispatchQueue.main,
        imageTransition: ImageTransition = .noTransition,
        runImageTransitionIfCached: Bool = false,
        completion: ((DataResponse<UIImage>) -> Void)? = nil,
        isShowActivityIndicator: Bool = false
        )
    {
        var indicator:UIActivityIndicatorView?
        if isShowActivityIndicator{
            indicator = addActivityIndicator()
        }
        af_setImage(
            withURLRequest: url,
            placeholderImage: placeholderImage,
            filter: filter,
            progress: progress,
            progressQueue: progressQueue,
            imageTransition: imageTransition,
            runImageTransitionIfCached: runImageTransitionIfCached,
            completion: {[weak self] data in
                completion?(data)
                if isShowActivityIndicator{
                    self?.removeActivityIndicator(indicator)
                }
            }
        )
    }
    
    
    func addActivityIndicator() -> UIActivityIndicatorView{
        superview?.setNeedsLayout()
        superview?.layoutIfNeeded()
        
        let myActivityIndicator = UIActivityIndicatorView(style: .whiteLarge)
        myActivityIndicator.center = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        myActivityIndicator.startAnimating()
        
        
        addSubview(myActivityIndicator)
        return myActivityIndicator
        
    }
    func removeActivityIndicator(_ indicator:UIActivityIndicatorView?){
        indicator?.removeFromSuperview()
    }
    
    
}

fileprivate func urlRequest(with url: URL) -> URLRequest {
    var urlRequest = URLRequest(url: url)
    
    
    let acceptableImageContentTypes: Set<String> = [
        "image/tiff",
        "image/jpeg",
        "image/gif",
        "image/png",
        "image/ico",
        "image/x-icon",
        "image/bmp",
        "image/x-bmp",
        "image/x-xbitmap",
        "image/x-ms-bmp",
        "image/x-win-bitmap"
    ]
    
    for mimeType in acceptableImageContentTypes {
        urlRequest.addValue(mimeType, forHTTPHeaderField: "Accept")
    }
    urlRequest.cachePolicy = cachePolicy
    return urlRequest
}

