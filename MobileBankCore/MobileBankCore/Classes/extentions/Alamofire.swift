//
//  Alamofire.swift
//  Alamofire
//
//  Created by Oleg Ten on 3/6/19.
//

import Alamofire


//cashing data

extension Alamofire.SessionManager{
    @discardableResult
    open func requestWithCachePolicy(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil,
        cachePolicy:URLRequest.CachePolicy = cachePolicy)
        -> DataRequest
    {
        do {
            var urlRequest = try URLRequest(url: url, method: method, headers: headers)
            urlRequest.cachePolicy = cachePolicy
            let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
            return request(encodedURLRequest)
        } catch {
            return request(URLRequest(url: URL(string: "http://example.com/wrong_request")!))
        }
    }
}

public var cachePolicy:URLRequest.CachePolicy {
    return isNetworkReachable ?
        .useProtocolCachePolicy:
        .returnCacheDataElseLoad
    
}
fileprivate var isNetworkReachable:Bool{
    return NetworkReachabilityManager()?.isReachable ?? false
}

