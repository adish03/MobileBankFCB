//
//  bundle.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/9/19.
//

import Foundation
import UIKit

public var bundleKey: UInt8 = 0

open class AnyLanguageBundle: Bundle {
    
    static public let shared = AnyLanguageBundle()

    override open func localizedString(forKey key: String,
                                       value: String?,
                                       table tableName: String?) -> String {
        
        guard let path = objc_getAssociatedObject(self, &bundleKey) as? String,
            let bundle = Bundle(path: path) else {
                
                return super.localizedString(forKey: key, value: value, table: tableName)
        }
        
        return bundle.localizedString(forKey: key, value: value, table: tableName)
    }
}

extension Bundle {
    
    
    open func setLanguage(_ language: String) {
        
        defer {
            
            object_setClass(Bundle.main, AnyLanguageBundle.self)
        }
        
        objc_setAssociatedObject(Bundle.main, &bundleKey,    Bundle.main.path(forResource: language, ofType: "lproj"), .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
}

