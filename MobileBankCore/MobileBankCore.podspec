#
# Be sure to run `pod lib lint MobileBankCore.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MobileBankCore'
  s.version          = '0.1.2'
  s.summary          = 'A short description of MobileBankCore.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/tennet0505/MobileBankCore'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'tennet0505' => 'tennet0505@gmail.com' }
  s.source           = { :git => 'https://github.com/tennet0505/MobileBankCore.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.3'

  s.source_files = 'MobileBankCore/Classes/**/*'
  
  # s.resource_bundles = {
  #   'MobileBankCore' => ['MobileBankCore/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'Alamofire', '= 4.8.0'
  s.dependency 'RxSwift', '~> 5'
  s.dependency 'RxCocoa', '~> 5'
  s.dependency 'ObjectMapper', '= 3.4.2'
  s.dependency 'RxAlamofire', '= 5.0.0'
  s.dependency 'SVProgressHUD', '= 2.2.5'
  s.dependency 'IQKeyboardManagerSwift', '= 6.2.0'
  s.dependency 'CryptoSwift', '~> 1.4.1'
  s.dependency 'SwiftKeychainWrapper', '= 3.2.0'
  s.dependency 'AlamofireImage', '= 3.5.0'
  s.dependency 'SDWebImage', '~> 5.0'
  s.dependency 'Kingfisher', '= 4.10.1'
  s.dependency 'PDFReader', '= 2.5.1'
  
 

end
