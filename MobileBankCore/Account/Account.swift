//
//  Account.swift
//  Alamofire
//
//  Created by Oleg Ten on 2/20/19.
//

import ObjectMapper

open class Deposit: Mappable {
    open var
    openDate: String?,
    endDate: String?,
    closeDate: String?,
    officeID: Int?,
    balance: Double?,
    lastOperationDate: String?,
    canWithdraw: Bool?,
    name: String?,
    accountNo: String?,
    currencyID: Int?,
    currencySymbol: String?
    
    init(){}
    required public init?(map: Map) {}
    public func mapping(map: Map) {
        openDate <- map["OpenDate"]
        endDate <- map["EndDate"]
        closeDate <- map["CloseDate"]
        officeID <- map["OfficeID"]
        balance <- map["Balance"]
        lastOperationDate <- map["LastOperationDate"]
        canWithdraw <- map["CanWithdraw"]
        name <- map["Name"]
        accountNo <- map["AccountNo"]
        currencyID <- map["CurrencyID"]
        currencySymbol <- map["CurrencySymbol"]
    }
}


