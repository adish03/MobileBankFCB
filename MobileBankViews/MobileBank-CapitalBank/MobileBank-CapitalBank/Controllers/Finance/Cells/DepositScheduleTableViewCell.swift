//
//  DepositScheduleTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 18/8/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation

class DepositScheduleTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var daysCountLabel: UILabel!
    @IBOutlet weak var mainSummLabel: UILabel!
    @IBOutlet weak var replanishmentAmountLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var bonusLabel: UILabel!
    @IBOutlet weak var capitalizationSummLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
// кастомизация ячейки при выборе
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
