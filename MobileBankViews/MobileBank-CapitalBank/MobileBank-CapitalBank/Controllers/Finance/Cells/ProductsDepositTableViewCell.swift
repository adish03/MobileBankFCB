//
//  ProductsDepositTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/20/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

class ProductsDepositTableViewCell: UITableViewCell {

    
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
