//
//  CardTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/13/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      customImageCardCell(view: view, image: accountImage)
    }
    // кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.accessoryType = selected ? .checkmark : .none
        if selected {
            self.contentView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        } else {
            self.contentView.backgroundColor = .white
        }
    }


}
