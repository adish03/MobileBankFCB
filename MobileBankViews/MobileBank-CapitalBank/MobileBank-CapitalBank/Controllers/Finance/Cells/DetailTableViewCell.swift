//
//  DetailTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/14/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var documentNoLabel: UILabel!
    
    @IBOutlet weak var operationDateLabel: UILabel!
    @IBOutlet weak var dtSummLabel: UILabel!
    @IBOutlet weak var ctSummLabel: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }

}
