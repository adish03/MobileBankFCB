//
//  CreditTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/20/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore

class CreditTableViewCell: UITableViewCell {

    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var viewButton: UIView!
    
    @IBOutlet weak var openDepositButton: UIButton!
    @IBOutlet weak var viewData: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    //    скрытие кнопки
    func hiddenButton(isHidden: Bool) {
        if isHidden{
            viewButton.isHidden = true
            viewData.isHidden = false
        }else{
            viewButton.isHidden = false
            viewData.isHidden = true
        }
    }
}
