//
//  OpenDepositViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/20/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

//класс открытие депозита
class OpenDepositViewController: BaseViewController, DepositProtocol {
    
    @IBOutlet weak var labelErrorCurrency: UILabel!
    @IBOutlet weak var labelErrorPercent: UILabel!
    @IBOutlet weak var labelErrorAmount: UILabel!
    @IBOutlet weak var labelErrorFromAccount: UILabel!
    @IBOutlet weak var viewFromAccount: UIView!
    
    @IBOutlet weak var viewCurrencyTextFields: UIView!
    @IBOutlet weak var viewPercentTextField: UIView!
    @IBOutlet weak var viewAmountTextField: UIView!
    @IBOutlet weak var viewPurposeTextField: UIView!
    
    @IBOutlet weak var currencyLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var currencyTextField: UITextField!
    @IBOutlet weak var percentTextField: UITextField!
    @IBOutlet weak var purposeTextFiled: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var payFrom_accountsLabel: UILabel!
    @IBOutlet weak var choiceCard_accountsLabel: UILabel!
    
    @IBOutlet weak var sumView: UIView!
    
    @IBOutlet weak var minAmountLabel: UILabel!
    // MARK: Working Dialog
    lazy var confirmDialog: OpenDepositConfirmAlert = {
        let view = OpenDepositConfirmAlert()
        return view
    }()
    
    var pickerView = UIPickerView()
    var pickerViewPercent = UIPickerView()
    var pickerViewPurpose = UIPickerView()
    
    var daysList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
    var product: Product!
    var periods: [Period]! = []
    var accounts: [Accounts]!
    var purposes: [DepositPurposeReferenceItemModel]! = []
    
    var selectedDeposit: Deposit!
    
    var openDepositModel: OpenDepositModel!
    
    var currencyID = 0
    var period = 0
    var productID = 0
    var currencySymbol = ""
    var periodString = ""
    var navigationTitle = ""
    var selectedMonthCount = 0
    var percentage = "2.0%"
    var purposeID = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        nameLabel.text = product.name
        descriptionLabel.text = product.productDescription
        descriptionLabel.textColor = .lightGray
        
        let descriptionLabelView = UILabel()
        descriptionLabelView.text = product.productDescription
        descriptionLabelView.numberOfLines = 0
        descriptionLabelView.textColor = .gray
        descriptionLabelView.font = UIFont.systemFont(ofSize: 15)
        
        view.addSubview(descriptionLabelView)
        descriptionLabelView.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(16)
            make.left.right.equalToSuperview().inset(16)
        }
        
        getDepositPurposes()
        if product.isGraphic ?? false {
            sumView.isHidden = true
        }
        
        if selectedDeposit != nil{
            let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
            payFrom_accountsLabel.text = fullAccountsText
            choiceCard_accountsLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
        }else{
            payFrom_accountsLabel.text = localizedText(key: "account_for_replenishment")
            choiceCard_accountsLabel.text = localizedText(key: "select_account_or_card")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        
        let buttonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(buttonAttributes , for: .normal)
        
        currencyTextField.delegate = self
        percentTextField.delegate = self
        purposeTextFiled.delegate = self
        pickerSettings()
        
        self.productID = product.productID ?? 0
        currencyTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControl.Event.editingDidEnd)
        percentTextField.addTarget(self, action: #selector(textFieldEditingEnd(_:)), for: UIControl.Event.editingDidEnd)
        amountTextField.addTarget(self, action: #selector(textFieldEditingEndAmount(_:)), for: UIControl.Event.editingDidEnd)
        
    }
    
    func getAccounts(){
        showLoading()
        managerApi.getAccounts(currencyID: Constants.NATIONAL_CURRENCY)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (accounts) in
                    guard let `self` = self else { return }
                    self.hideLoading()
                    
                    self.accounts = accounts.filter{$0.accountType! != InternetBankingAccountType.Card.rawValue}
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    // Проверка поля "валюта" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChange(_ sender: Any) {
        currencyTextField.stateIfEmpty(view: viewCurrencyTextFields, labelError: labelErrorCurrency)
    }
    // Проверка поля "проценты" на наличие данных, после потери фокуса
    @objc func textFieldEditingEnd(_ sender: Any) {
        percentTextField.stateIfEmpty(view: viewPercentTextField, labelError: labelErrorPercent)
    }
    // Проверка поля "Сумма" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndAmount(_ sender: Any) {
        amountTextField.stateIfEmpty(view: viewAmountTextField, labelError: labelErrorAmount)
       
    }
//    кнопка открытия депозита
    @IBAction func openDepostitButton(_ sender: Any) {
        if  currencyTextField.text != "" && percentTextField.text != "" && (amountTextField.text != "" || product.isGraphic ?? false) && (purposeTextFiled.text != "") && selectedDeposit != nil
        {
            // MARK: Working   Dialog
            confirmDialog.showAlert(model: createConfirmModel(), on: self)
        }else{
            viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
            currencyTextField.stateIfEmpty(view: viewCurrencyTextFields, labelError: labelErrorCurrency)
            percentTextField.stateIfEmpty(view: viewPercentTextField, labelError: labelErrorPercent)
            amountTextField.stateIfEmpty(view: viewAmountTextField, labelError: labelErrorAmount)
        }
    }
    
    func createConfirmModel() -> ConfirmModel {
        let date = Date()
        var dateComponent = DateComponents()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yy"
        let currentDate = formatter.string(from: date)
        
        dateComponent.month = self.period
        
        let closeDate = Calendar.current.date(byAdding: dateComponent, to: date)
        let dayDifference = Date.daysBetween(start: date, end: closeDate ?? date)
        
        return ConfirmModel(
            depositName: nameLabel.text!,
            currency: currencySymbol,
            openDate: currentDate,
            closeDate: formatter.string(from: closeDate!),
            termDay: " \(dayDifference) дн.",
            percentageBet: percentage,
            copitalization: self.product.capitalizationAgreementMessage!,
            agreement:  self.product.confirmationAgreementMessage!)
    }
    
    func postOpenDeposit() {
        let amount = Double(self.amountTextField.text ?? "0")
        let openDepositModel = OpenDepositModel(
            accountNo: self.selectedDeposit.accountNo,
            currencyID: self.currencyID,
            period: self.period,
            productID: self.productID,
            sumV: amount,
            purposeID: nil,
            scheduleParameters: nil)
        
        let minSumm = self.periods.filter{$0.periodCount == self.period}[0].minSummOnAccount
        if(product.isGraphic == false && (amount! < Double(minSumm!))){
            self.showAlertController("Сумма пополнения депозита должна быть минимум \(minSumm!) ")
        }else{
            if product.isGraphic ?? false {
                self.postGraphic(OpenDepositModel: openDepositModel)
            } else {
                self.postOpenDeposit(OpenDepositModel: openDepositModel)
            }
        }

    }
    
    //    обработка переходов
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if periods.count != 0{
            if segue.identifier == "toAccountsSegue"{
                let vc = segue.destination as! AccountsViewController
                
                for account in self.accounts{
                    account.items = account.items?.filter{$0.currencyID == self.currencyID && $0.balance != 0 && $0.canWithdraw == true}
                }
                
                vc.accounts = self.accounts
                vc.selectedDeposit = self.selectedDeposit
                vc.delegate = self
                vc.navigationTitle = self.localizedText(key: "account_for_replenishment")
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            }
            if segue.identifier == "toOpenDepositScheduleSegue"{
                let vc = segue.destination as! DepositGraphicViewController
                vc.openDepositModel = self.openDepositModel
                vc.selectedDeposit = self.selectedDeposit
                vc.period = self.periods.filter{$0.periodCount == self.period}[0]
            }
        }else{
            showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
        }
    }
    
    //    установака текущего счета получателя
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
    }
}
// выбор процентов
extension OpenDepositViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //  установка дефолтных значений периода
    func pickerSettings() {
        pickerView.delegate = self
        pickerViewPercent.delegate = self
        pickerViewPurpose.delegate = self
        
        currencyTextField.inputView = pickerView
        percentTextField.inputView = pickerViewPercent
        purposeTextFiled.inputView = pickerViewPurpose
        
        
        pickerView.selectRow(0, inComponent: 0, animated: true)
        pickerViewPercent.selectRow(0, inComponent: 0, animated: true)
        
        currencyTextField.text = product.currencies?[0].symbol
    }
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count = 0
        if pickerView == self.pickerView {
            count = product.currencies?.count ?? 0
        } else if pickerView == self.pickerViewPercent{
            if periods.count != 0{
                count = periods.count
            }
        } else if pickerView == self.pickerViewPurpose{
            count = purposes.count
        }
        return count
    }
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var list = ""
        if pickerView == self.pickerView {
            list = product.currencies?[row].symbol ?? ""
        } else if pickerView == self.pickerViewPercent{
            if periods.count != 0{
                let periodString = "\(periods[row].periodCount ?? 0) \(self.localizedText(key: "month"))., \(periods[row].rate ?? 0)%"
                list = periodString
            }
        } else if pickerView == self.pickerViewPurpose{
            list = purposes?[row].text ?? ""
        }
        return list
    }
    // выбор полей
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.pickerView{
            if(row != 0){
                currencyTextField.text = product.currencies?[row].symbol
                self.currencyLabel.text = product.currencies?[row].symbol
                getAccounts()
                periods = product.currencies?[row].periods
                currencySymbol = product.currencies?[row].symbol ?? ""
                self.currencyID = product.currencies?[row].currencyID ?? 0
                if periods.count != 0{
                    percentTextField.text = "\(periods[0].periodCount ?? 0) \(self.localizedText(key: "month"))., \(periods[0].rate ?? 0)%"
                    self.period = periods[0].periodCount ?? 0
                }else{
                    percentTextField.text = ""
                }
            }
        } else if pickerView == self.pickerViewPercent{
            if periods.count == 0{
                showAlertController(localizedText(key: "select_currency"))
            }else{
                // MARK: PICK DATE PERCENTAGE
                periodString = "\(periods[row].periodCount ?? 0) \(self.localizedText(key: "month"))., \(periods[row].rate ?? 0)%"
                percentTextField.text = periodString
                self.period = periods[row].periodCount ?? 0
                self.productID = periods[row].productID ?? 0
                self.percentage = "\(Double(periods[row].rate ?? 0))%"
            }
        } else if pickerView == self.pickerViewPurpose{
            purposeTextFiled.text = purposes[row].text ?? ""
            purposeID = (purposes[row].value ?? 0)
        }
    }
    
    func postGraphic(OpenDepositModel: OpenDepositModel){
        openDepositModel = OpenDepositModel
        performSegue(withIdentifier: "toOpenDepositScheduleSegue", sender: self)
    }
    
    //   Открытие депозита
    func postOpenDeposit(OpenDepositModel: OpenDepositModel){
        showLoading()
        managerApi
            .postOpenDeposit(OpenDepositModel: OpenDepositModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (result) in
                    dump(result)
                    
                    
                    guard let `self` = self else { return }
                    if(result.state == 1){
                        let alertController = UIAlertController(title: nil, message: "\(result.message!)", preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                        self.hideLoading()
                    }else{
                        let accountString = result.accountNo!.replacingOccurrences(of: "\"", with: "")
                        let amount = Double(self.amountTextField.text ?? "0")
                        let submitModelInternalOperation = InternalOperationModel(
                            dtAccountNo: self.selectedDeposit.accountNo,
                            ctAccountNo: accountString,
                            operationType: InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue,
                            operationID: 0,
                            currencyID: self.currencyID,
                            sum: amount,
                            comment: "\(self.localizedText(key: "OpenDepositTitle")) (\(self.product.name ?? ""): \(self.currencySymbol),\(self.periodString))",
                            isSchedule: false,
                            isTemplate: false,
                            templateName: nil,
                            templateDescription: nil,
                            scheduleID: nil,
                            schedule: nil)
                        
                        let documents = result.reportDocuments
                        
                        self.postCreatpostInternalOperationCustomerAccounts(internalOperationModel: submitModelInternalOperation, accountNo:accountString, documents: documents)
                        print(result.accountNo!)
                        
                        self.hideLoading()
                    }
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func getDepositPurposes() {
        showLoading()
        managerApi
            .getDepositPurposes()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (purposes) in
                    self?.purposes = purposes
                    self?.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                }).disposed(by: disposeBag)
    }
    
    //    создание перевода между счетами для получения operationID
    func postCreatpostInternalOperationCustomerAccounts(internalOperationModel: InternalOperationModel, accountNo: String, documents: [Documents]?){
        showLoading()
        managerApi
            .postInternalOperationCustomerAccounts(InternalOperationModel: internalOperationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                    guard let `self` = self else { return }
                    let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "SubmitTransactionViewController") as! SubmitTransactionViewController
                    
                    let amount = Double(self.amountTextField.text ?? "0")
                    let internalOperationModel = InternalOperationModel(
                        dtAccountNo: self.selectedDeposit.accountNo,
                        ctAccountNo: accountNo,
                        operationType: InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue,
                        operationID: operation.operationID,
                        currencyID: self.currencyID,
                        sum: amount,
                        comment: internalOperationModel.comment,
                        isSchedule: false,
                        isTemplate: false,
                        templateName: nil,
                        templateDescription: nil,
                        scheduleID: nil,
                        schedule: nil)
                    
                    vc.submitModelInternalOperation = internalOperationModel
                    vc.operationID = operation.operationID ?? 0
                    vc.code = ""
                    vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
                    vc.currencyCurrent = self.currencySymbol
                    vc.selectedDeposit = self.selectedDeposit
                    vc.documents = documents

                    self.navigationController?.pushViewController(vc, animated: true)
                    self.hideLoading()
                    
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension OpenDepositViewController: UITextFieldDelegate{
    //    проверка поля на начало ввода информации
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.isEqual(percentTextField){
            if periods.count == 0{
                showAlertController(localizedText(key: "select_currency"))
            }
        }
        return true
    }
    //    проверка поля на начало ввода информации на кол-во символов
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        if textField.isEqual(amountTextField) {
            maxLength = 11
        }
        return newLength <= maxLength
    }
//    переход на следкующее поле
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == currencyTextField {
            textField.resignFirstResponder()
            percentTextField.becomeFirstResponder()
        }else if textField == percentTextField {
            textField.resignFirstResponder()
            amountTextField.becomeFirstResponder()
        }else if textField == amountTextField {
            textField.resignFirstResponder()
        }
        return true
    }
}
