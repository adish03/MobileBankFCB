//
//  ProductsDepositViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/20/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

// класс виды депозитов
class ProductsDepositViewController: BaseViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var filterImage: UIImageView!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    var selectableProductItems: [SelectableProductItem] = []
    var selectableProductCreditItems: [SelectableProductItem] = []
    var productsAll = [Product]()
    var productsCreditAll = [LoanProductModel]()
    
    var currencies: Currency!
    var accounts = [Accounts]()
    var isCreateRequestCredit = false
    
    var isSearching = false
    var filteredArray = [Product]()
    var filteredCreditArray = [LoanProductModel]()
    var pickerView = UIPickerView()
    var organizationType = 0
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !isCreateRequestCredit{
            getProductsDeposit()
        }else{
            getProductsCredit()
        }
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        hideKeyboardWhenTappedAround()
        emptyStateStack.isHidden = true
        if isCreateRequestCredit{
            navigationItem.title = localizedText(key: "credit_application")
        }else{
            navigationItem.title = localizedText(key: "open_deposit")
        }
        let buttonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(buttonAttributes , for: .normal)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    
    }
    
    //получение продуктов
    func  getProductsDeposit() {
      
        showLoading()
        managerApi
            .getAllProductsDeposit()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (products) in
                    guard let `self` = self else { return }
                    self.productsAll.removeAll()
                    self.selectableProductItems = products.result?.selectableProductItems ?? []
                    self.selectableProductItems.forEach({ (products) in
                        for product in products.products ?? []{
                            self.productsAll.append(product)
                        }
                    })
                    if self.productsAll.isEmpty{
                        self.tableView.isHidden = true
                        self.emptyStateStack.isHidden = false
                    }else{
                        self.tableView.isHidden = false
                        self.emptyStateStack.isHidden = true
                        self.pickerSettings()
                    }
                    self.tableView.reloadData()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //получение видов кредитов
    func  getProductsCredit() {
        
        showLoading()
        managerApi
            .getLoansAvailableProducts()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (products) in
                    guard let `self` = self else { return }
                    
                    self.productsCreditAll.removeAll()
                    self.productsCreditAll = products
                    
                    if self.productsCreditAll.isEmpty{
                        self.tableView.isHidden = true
                        self.emptyStateStack.isHidden = false
                    }else{
                        self.tableView.isHidden = false
                        self.emptyStateStack.isHidden = true
                    }
                    self.tableView.reloadData()
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
   
    
}

extension ProductsDepositViewController: UITableViewDataSource, UITableViewDelegate{
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            if isCreateRequestCredit{
                return filteredCreditArray.count
            }else{
                return filteredArray.count
            }
        }else{
            if isCreateRequestCredit{
                return self.productsCreditAll.count
            }else{
               return self.productsAll.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ProductsDepositTableViewCell
        
        if isSearching{
            if isCreateRequestCredit{
                let product = self.filteredCreditArray[indexPath.row]
                cell.nameLabel.text = product.name
                cell.descriptionLabel.text = product.description
            }else{
                let product = self.filteredArray[indexPath.row]
                cell.nameLabel.text = product.name
                cell.descriptionLabel.text = product.productDescription
            }
        }else{
            if isCreateRequestCredit{
                let product = self.productsCreditAll[indexPath.row]
                cell.nameLabel.text = product.name
                cell.descriptionLabel.text = product.description
                self.getImageProduct(image: product.imageName ?? "", imageView: cell.productImage, cell: cell)
            }else{
                let product = self.productsAll[indexPath.row]
                cell.nameLabel.text = product.name
                cell.descriptionLabel.text = product.productDescription
                self.getImageProduct(image: product.imageName ?? "", imageView: cell.productImage, cell: cell)
            }
        }
        selectedCellCustomColor(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if filteredArray.count != 0 || filteredCreditArray.count != 0 {
            if isCreateRequestCredit{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanRequestViewController") as! LoanRequestViewController
                
                let titleMortrageTypes = LoanProductMortrageTypeModel(name: localizedText(key: "select_the_type_of_collateral"))
                filteredCreditArray[indexPath.row].mortrageTypes?.insert(titleMortrageTypes, at: 0)
                
                vc.product = productsCreditAll[indexPath.row]
                vc.accounts = self.accounts
                vc.navigationTitle = localizedText(key: "credit_application")
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "OpenDepositViewController") as! OpenDepositViewController
                
                let titleProduct = PeriodByCurrency(
                    symbol: localizedText(key: "select_currency"))
                filteredArray[indexPath.row].currencies?.insert(titleProduct, at: 0)
                vc.product = filteredArray[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
           
        }else{
            if isCreateRequestCredit{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanRequestViewController") as! LoanRequestViewController
                
                let titleMortrageTypes = LoanProductMortrageTypeModel(name: localizedText(key: "select_the_type_of_collateral"))
                productsCreditAll[indexPath.row].mortrageTypes?.insert(titleMortrageTypes, at: 0)
              
                vc.product = productsCreditAll[indexPath.row]
                vc.accounts = self.accounts
                vc.navigationTitle = localizedText(key: "credit_application")
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "OpenDepositViewController") as! OpenDepositViewController
                
                let titleProduct = PeriodByCurrency(symbol: localizedText(key: "select_currency"))
                productsAll[indexPath.row].currencies?.insert(titleProduct, at: 0)
                vc.product = productsAll[indexPath.row]
                vc.navigationTitle = localizedText(key: "open_deposit")
                self.navigationController?.pushViewController(vc, animated: true)
            }
          
        }
    }
}
extension ProductsDepositViewController: UISearchBarDelegate{
    //поиск
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            
            isSearching = false
            if isCreateRequestCredit{
                if productsCreditAll.isEmpty{
                    tableView.isHidden = true
                    emptyStateStack.isHidden = false
                    self.emptyStateLabel.text = localizedText(key: "no_data")
                    self.emptyImage.image = UIImage(named: "empty_icn_white")
                }else{
                    tableView.isHidden = false
                    emptyStateStack.isHidden = true
                }
            }else{
                if productsAll.isEmpty{
                    tableView.isHidden = true
                    emptyStateStack.isHidden = false
                    self.emptyStateLabel.text = localizedText(key: "no_data")
                    self.emptyImage.image = UIImage(named: "empty_icn_white")
                }else{
                    tableView.isHidden = false
                    emptyStateStack.isHidden = true
                }
            }
            
            tableView.reloadData()
            
        }else{
            if isCreateRequestCredit{
                filteredCreditArray = productsCreditAll.filter({ (product: LoanProductModel) -> Bool in
                    return product.name?.lowercased().contains(searchText.lowercased()) ?? true
                })
            }else{
                filteredArray = productsAll.filter({ (product: Product) -> Bool in
                    return product.name?.lowercased().contains(searchText.lowercased()) ?? true
                })
            }
            if filteredArray.isEmpty{
                self.tableView.isHidden = true
                self.emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.tableView.isHidden = false
                self.emptyStateStack.isHidden = true
            }
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    // обработка кнопки search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        filteredArray.removeAll()
        filteredCreditArray.removeAll()
        isSearching = false
        searchBar.text = ""
        if isCreateRequestCredit{
            if productsCreditAll.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "no_data")
                self.emptyImage.image = UIImage(named: "empty_icn_white")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
        }else{
            if productsAll.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "no_data")
                self.emptyImage.image = UIImage(named: "empty_icn_white")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
        }
        tableView.reloadData()
        view.endEditing(true)
    }
}

// выбор периода планировщика
extension ProductsDepositViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //  установка дефолтных значений периода
    func pickerSettings() {
        pickerView.delegate = self
        stateTextField.inputView = pickerView
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
        if !selectableProductItems.isEmpty{
            stateTextField.text = selectableProductItems[0].organizationTypeName
        }
        //        percentTextField.text = daysList[0]
        
    }
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return selectableProductItems.count
    }
    
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return selectableProductItems[row].organizationTypeName
        
    }
    // выбор полей
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        getProductsDeposit()
        organizationType = selectableProductItems[row].organizationType ?? 0
        stateTextField.text = selectableProductItems[row].organizationTypeName ?? ""
    }
}

