//
//  DepositGraphicViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 12/8/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift

class DepositGraphicViewController : BaseViewController,UIPickerViewDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    
    @IBOutlet weak var firstSumTextField: UITextField!
    @IBOutlet weak var everyMonthSumTextField: UITextField!
    
    @IBOutlet weak var workField: UITextField!
    @IBOutlet weak var label: UILabel!
    let datePicker = UIDatePicker()
    
    var schedule: [DepositScheduleResult]!
    var depositSchedule: ScheduleParameters!
    var selectedDeposit: Deposit!
    var period: Period?
    
    var dateFrom = ""
    var dateTo = ""
    
    var openDepositModel: OpenDepositModel!
    
    public static var testTest: String = ""
    
    override func viewDidLoad() {
        title = "Депозитный график"
        
        workField.isUserInteractionEnabled = false
        
        if let bankDate = UserDefaultsHelper.bankDate{
            dateFrom = dateFormaterBankDay(date: bankDate)
        }
        
        let calendar = Calendar.current
        if let date = calendar.date(byAdding: .month, value: 1, to: stringToDateFormatterFiltered(date: dateFrom)) {
            
            dateTo = dateToStringFormatter(date: date)
        }
        
        fromDateTextField.delegate = self
        toDateTextField.delegate = self
        showDatePicker()
        
        firstSumTextField.text = String(period?.minSummOnAccount ?? 0)
        
        if period?.minSummOnAccount != nil {
            label.text = "Минимальная сумма ежемесячного пополнения: \(period!.minSummOnAccount ?? 0)"
        }
    }
    
    // выборка даты
    func showDatePicker(){
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        
        datePicker.maximumDate = Calendar.current.date(byAdding: .month, value: 2, to: Date())
        
        if let bankDate = UserDefaultsHelper.bankDate{
            datePicker.minimumDate = stringToDateFormatterFilteredDate(date: bankDate)
        }
        
        managerApi
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        fromDateTextField.inputAccessoryView = toolbar
        fromDateTextField.inputView = datePicker
        fromDateTextField.isUserInteractionEnabled = false
        toDateTextField.inputAccessoryView = toolbar
        toDateTextField.inputView = datePicker
        fromDateTextField.text = dateFrom
        toDateTextField.text = dateTo
    }
    
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        if fromDateTextField.isFirstResponder {
            dateFrom = formatterToServer.string(from: datePicker.date)
            
            fromDateTextField.text = formatter.string(from: datePicker.date)
        }
        if toDateTextField.isFirstResponder {
            dateTo = formatterToServer.string(from: datePicker.date)
            
            toDateTextField.text = formatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    @IBAction func clearButton(_ sender: UIButton) {
        firstSumTextField.text = ""
        everyMonthSumTextField.text = ""
        
        if let bankDate = UserDefaultsHelper.bankDate{
            dateFrom = dateFormaterBankDay(date: bankDate)
        }
        
        let calendar = Calendar.current
        if let date = calendar.date(byAdding: .month, value: 1, to: stringToDateFormatterFiltered(date: dateFrom)) {
            
            dateTo = dateToStringFormatter(date: date)
        }
        
        fromDateTextField.delegate = self
        toDateTextField.delegate = self
        
        showDatePicker()
    }
    
    @IBAction func showGraphic(_ sender: Any) {
        if Int(firstSumTextField.text ?? "0") ?? 0 >= (period?.minSummOnAccount ?? 0) && Int(everyMonthSumTextField.text ?? "0") ?? 0 >= (period?.minSummOnAccount ?? 0) {
            let depositSchedule = ScheduleParameters(
                productID: openDepositModel.productID,
                currencyID: openDepositModel.currencyID,
                period: openDepositModel.period,
                initialFee: Double(firstSumTextField.text ?? "0"),
                replanishmentAmount: Double(everyMonthSumTextField.text ?? "0"),
                refillBalanceDate: dateFormaterForDeposit(date: dateTo),
                date: dateFormaterForDeposit(date: dateFrom),
                considerWorkingDays: true
            )
            self.depositSchedule = depositSchedule
            buildSchedule(depositSchedule: depositSchedule)
        } else {
            if (Int(firstSumTextField.text ?? "0") ?? 0 >= (period?.minSummOnAccount ?? 0)) == false && (Int(everyMonthSumTextField.text ?? "0") ?? 0 >= (period?.minSummOnAccount ?? 0)) == false {
                showAlertController("Неверная минимальная сумма первоначального взноса и ежемесячного платежа")
                
                return
            }
            
            if (Int(firstSumTextField.text ?? "0") ?? 0 >= (period?.minSummOnAccount ?? 0)) == false {
                showAlertController("Неверная минимальная сумма первоначального взноса")
            }
            
            if (Int(everyMonthSumTextField.text ?? "0") ?? 0 >= (period?.minSummOnAccount ?? 0)) == false {
                showAlertController("Неверная минимальная сумма ежемесячного платежа")
            }
            
        
        }
    }
    
    // проверка на фокус поля ввода
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fromDateTextField {
            datePicker.datePickerMode = .date
        }
        if textField == toDateTextField {
            datePicker.datePickerMode = .date
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DepositScheduleSegue"{
            let vc = segue.destination as! DepositScheduleViewController
            vc.schedulePayments = schedule
            self.openDepositModel.scheduleParameters = depositSchedule
            openDepositModel.sumV = depositSchedule.initialFee
            vc.openDepositModel = openDepositModel
            vc.depositSchedule = depositSchedule
            vc.selectedDeposit = selectedDeposit
        }
    }
    
    func buildSchedule(depositSchedule: ScheduleParameters) {
        showLoading()
        managerApi
            .buildDepositSchedule(schedule: depositSchedule)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: { [weak self] (result) in
                        guard let `self` = self else { return }
                        self.schedule = result.result
                    DepositGraphicViewController.testTest = self.everyMonthSumTextField.text ?? ""
                        self.performSegue(withIdentifier: "DepositScheduleSegue", sender: self)
                        self.hideLoading()
                    },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
