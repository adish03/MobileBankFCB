//
//  FilteredDetailsAccount.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/14/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift

// протокол для фильтрации
protocol FilterDetailDelegate {
    func property(dateFrom: String?, dateTo: String?, isFilter: Bool?)
    func resetFilter(isFilter: Bool?)
}
//  класс для фильтрации
class FilteredDetailsAccount: BaseViewController, UIPickerViewDelegate, UITextFieldDelegate{
    
    
    @IBOutlet weak var operationTypeTextField: UITextField!
    @IBOutlet weak var statusTypeTextField: UITextField!
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    
    var delegate: FilterDetailDelegate?
    let datePicker = UIDatePicker()
    
    var dateFrom = ""
    var dateTo = ""
    var isNeedData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = localizedText(key: "show_operations")
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        
        if let bankDate = UserDefaultsHelper.bankDate{
            dateTo = dateFormaterBankDay(date: bankDate)
        }
        
        let calendar = Calendar.current
        if let date = calendar.date(byAdding: .day, value: -7, to: stringToDateFormatterFiltered(date: dateTo)){
            
            dateFrom = dateToStringFormatter(date: date)
        }
        
        fromDateTextField.delegate = self
        toDateTextField.delegate = self
        showDatePicker()
        
    }
    
    //    кнопка сброса параметров фильтрации
    @IBAction func resetFilterButton(_ sender: Any) {
        resetFilterData()
        navigationController?.popViewController(animated: true)
    }
    //    кнопка применения фильтрации
    @IBAction func applyFilterButton(_ sender: Any) {
        filteredData()
        navigationController?.popViewController(animated: true)
    }
    
    //  установка значений фильтрации
    func filteredData(){
        delegate?.property(dateFrom: dateFrom, dateTo: dateTo, isFilter: true)
    }
    //  установка сброса фильтрации
    func resetFilterData(){
        delegate?.resetFilter(isFilter: false)
    }
    
    // выборка даты
    func showDatePicker(){
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.maximumDate = Date()
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        fromDateTextField.inputAccessoryView = toolbar
        fromDateTextField.inputView = datePicker
        toDateTextField.inputAccessoryView = toolbar
        toDateTextField.inputView = datePicker
        toDateTextField.text = dateTo
        //        dateTo = dateToStringFormatter(date: Date())//ScheduleHelper.shared.dateCurrentForServer()
        if isNeedData{
            if self.dateTo != ""{
                toDateTextField.text = dateFormaterForFilter(date: self.dateTo)
            }
            if self.dateFrom != ""{
                fromDateTextField.text = dateFormaterForFilter(date: self.dateFrom)
            }
        }
    }
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        
        if fromDateTextField.isFirstResponder {
            dateFrom = formatterToServer.string(from: datePicker.date)
            filteredData()
            
            fromDateTextField.text = formatter.string(from: datePicker.date)
        }
        if toDateTextField.isFirstResponder {
            dateTo = formatterToServer.string(from: datePicker.date)
            filteredData()
            
            toDateTextField.text = formatter.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    // проверка на фокус поля ввода
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fromDateTextField {
            datePicker.datePickerMode = .date
            
        }
        if textField == toDateTextField {
            datePicker.datePickerMode = .date
            
        }
    }
}
