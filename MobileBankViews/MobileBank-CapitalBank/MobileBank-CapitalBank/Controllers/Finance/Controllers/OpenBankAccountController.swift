//
//  OpenBankAccountController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 13.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import MobileBankCore
import SnapKit

class OpenBankAccountController: BaseViewController {
    
    @IBOutlet weak var valite: UITextField!
    @IBOutlet weak var product: UITextField!
    
    var pickerView = UIPickerView()
    var pickerViewPercent = UIPickerView()
    
    var selectOne: Currency?
    var selectTwo: Product?
    
    override func viewDidLoad() {
        title = "Открыть счет"
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        pickerView.delegate = self
        pickerViewPercent.delegate = self
        
        pickerView.selectRow(0, inComponent: 0, animated: true)
        pickerViewPercent.selectRow(0, inComponent: 0, animated: true)
        
        valite.inputView = pickerView
        product.inputView = pickerViewPercent
        
        getOne()
        getTwo()
    }
    
    @IBAction func create(_ sender: Any) {
        if selectOne != nil && selectTwo != nil {
            let alertController = UIAlertController(title: "Открытие счета", message: "Вы действительно хотите открыть счет?", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
            alertController.addAction(UIAlertAction(title: "Подтвердить", style: .default, handler: { [self] _ in
                let model = OpenCurrentAccountModel(
                    IsNew: true,
                    OfficeID: nil,
                    ProductID: selectTwo?.productID,
                    AccountNo: nil,
                    ProductType: 9,
                    Currencies: String(selectOne?.currencyID ?? 0),
                    OrganizationType: 16)
                            
                showLoading()
                
                managerApi
                    .openCurrentAccounts(model: model)
                    .updateTokenIfNeeded()
                    .subscribe(
                        onNext: {[weak self] details in
                            self?.hideLoading()

                            if details.state == 1 {
                                self?.showAlertController(details.message ?? "")
                            }
                            
                            self?.navigationController?.popViewController(animated: true)

                    },
                        onError: {[weak self] error in
                            self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                            self?.hideLoading()
                    })
                    .disposed(by: disposeBag)
            }))
            present(alertController, animated: true, completion: nil)
            
            
        } else {
            showAlertController("Заполните поля")
        }
    }
    
    var test1: [Currency] = []
    var test2: [Product] = []
    
    func getOne() {
        showLoading()

        managerApi
            .getAllProductsDepositModel()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] details in
                    dump(details)
                    
                    self?.test1 = details
                    
                    self?.hideLoading()

            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func getTwo() {
        showLoading()

        managerApi
            .protoTest()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] details in
                    dump(details)
                    var result: [Product] = []
                    
                    details.result?.selectableProductItems?.forEach({ item in
                        item.products?.forEach({ item in
                            result.append(item)
                        })
                    })
                    
                    self?.test2 = result
                    
                    self?.hideLoading()

            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension OpenBankAccountController: UIPickerViewDataSource, UIPickerViewDelegate{

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == self.pickerView {
            return test1.count
        } else if pickerView == self.pickerViewPercent{
            return test2.count
        }
        
        return 10
    }
    
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == self.pickerView {
            return test1[row].symbol
        } else if pickerView == self.pickerViewPercent{
            return test2[row].name
        }
        
        return ""
    }
    
    // выбор полей
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == self.pickerView{
            valite.text = test1[row].symbol
            
            selectOne = test1[row]
        } else if pickerView == self.pickerViewPercent{
            product.text = test2[row].name
            
            selectTwo = test2[row]
        }
    }
}
