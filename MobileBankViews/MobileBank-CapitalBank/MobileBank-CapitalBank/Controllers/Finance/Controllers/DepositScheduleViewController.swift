//
//  DepositScheduleViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 18/8/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

class DepositScheduleViewController: BaseViewController {
    
    var schedulePayments = [DepositScheduleResult]()
    var openDepositModel: OpenDepositModel!
    var depositSchedule: ScheduleParameters!
    var selectedDeposit: Deposit!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        title = "Депозитный график"
        
        super.viewDidLoad()
    }
    
    @IBAction func openDeposit(_ sender: Any){
        openDepositModel.scheduleParameters = depositSchedule
        showLoading()
        managerApi
            .postOpenDeposit(OpenDepositModel: openDepositModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (result) in
                    dump(result)
                    
                    guard let `self` = self else { return }
                        let accountString = result.accountNo!.replacingOccurrences(of: "\"", with: "")
                        let amount = self.openDepositModel.sumV
                        let submitModelInternalOperation = InternalOperationModel(
                            dtAccountNo: self.openDepositModel.accountNo,
                            ctAccountNo: accountString,
                            operationType: InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue,
                            operationID: 0,
                            currencyID: self.openDepositModel.currencyID,
                            sum: amount,
                            comment: "\(self.localizedText(key: "OpenDepositTitle")) (\(self.selectedDeposit.name ?? ""): \(self.selectedDeposit.currencySymbol!),\(self.openDepositModel.period))",
                            isSchedule: false,
                            isTemplate: false,
                            templateName: nil,
                            templateDescription: nil,
                            scheduleID: nil,
                            schedule: nil)
                        
                        self.postCreatpostInternalOperationCustomerAccounts(internalOperationModel: submitModelInternalOperation, accountNo:accountString )
                        
                        print(result.accountNo!)
                        
                        self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    создание перевода между счетами для получения operationID
    func postCreatpostInternalOperationCustomerAccounts(internalOperationModel: InternalOperationModel, accountNo: String){
        showLoading()
        managerApi
            .postInternalOperationCustomerAccounts(InternalOperationModel: internalOperationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                    guard let `self` = self else { return }
                    let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "SubmitTransactionViewController") as! SubmitTransactionViewController
                    
                    let amount = self.openDepositModel.sumV
                    let internalOperationModel = InternalOperationModel(
                        dtAccountNo: self.openDepositModel.accountNo,
                        ctAccountNo: accountNo,
                        operationType: InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue,
                        operationID: operation.operationID,
                        currencyID: self.openDepositModel.currencyID,
                        sum: amount,
                        comment: internalOperationModel.comment,
                        isSchedule: false,
                        isTemplate: false,
                        templateName: nil,
                        templateDescription: nil,
                        scheduleID: nil,
                        schedule: nil)
                    
                    vc.submitModelInternalOperation = internalOperationModel
                    vc.operationID = operation.operationID ?? 0
                    vc.code = ""
                    vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
                    vc.currencyCurrent = self.selectedDeposit.currencySymbol!
                    vc.selectedDeposit = self.selectedDeposit

                    self.navigationController?.pushViewController(vc, animated: true)
                    self.hideLoading()
                    
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension DepositScheduleViewController: UITableViewDataSource, UITableViewDelegate{
    
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schedulePayments.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 350
    }
    
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DepositScheduleTableViewCell
        
        let detail = schedulePayments[indexPath.row]
        
        cell.dateLabel.text = dateFormaterForFilter(date: detail.date!)
        cell.daysCountLabel.text = "\(detail.daysCount!)"
        cell.mainSummLabel.text = "\(detail.mainSumm!)"
        cell.replanishmentAmountLabel.text = "\(detail.replanishmentAmount ?? 0)"
        cell.rateLabel.text = "\(detail.rate!)"
        cell.bonusLabel.text = "\(detail.bonus!)"
        cell.capitalizationSummLabel.text = "\(detail.capitalizationSumm!)"
        
        
        return cell
    }
}
