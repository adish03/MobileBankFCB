//
//  CloseDepositViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/17/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

//    закрытие депозита
class CloseDepositViewController: BaseViewController, DepositProtocol {
    
    @IBOutlet weak var payTo_accountLabel: UILabel!
    @IBOutlet weak var choiceCardTo_accountLabel: UILabel!
    
    @IBOutlet weak var reason: UITextField!
    
    var navigationTitle = "Закрытие депозита"
    var selectedDeposit: Deposit!
    var accounts = [Accounts]()
    var currentDeposit: DetailDepositsModel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if selectedDeposit != nil{
            let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
            payTo_accountLabel.text = fullAccountsText
            choiceCardTo_accountLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
        }else{
            payTo_accountLabel.text = "Cчет для пополнения"
            choiceCardTo_accountLabel.text = localizedText(key: "select_account_or_card")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
    }
    //    обработка переходов
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toAccountsSegue"{
            let vc = segue.destination as! AccountsViewController
           
            accounts = accounts.filter{ $0.accountType == InternetBankingAccountType.Current.rawValue}
            for account in self.accounts{
                account.items = account.items?.filter{$0.currencyID == self.currentDeposit.currencyID}
            }
            vc.accounts = accounts
            vc.delegate = self
            vc.selectedDeposit = selectedDeposit
            vc.navigationTitle = "Cчет для пополнения:"
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
    }
    
    //    установака текущего счета получателя
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
    }
    
    //    кнопка закрытия
    @IBAction func closeDepositButton(_ sender: Any) {
        
        if selectedDeposit != nil{
            let alertController = UIAlertController(title: nil, message: "Вы действительно хотите закрыть данный депозит?", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                let closeDepositModel = CloseDepositModel(
                    accountNo: self.currentDeposit.accountNo,
                    currencyID: self.currentDeposit.currencyID,
                    corrAccountNo: self.selectedDeposit.accountNo ?? "",
                    reason: self.reason.text)
                
               self.postCloseDeposit(CloseDepositModel: closeDepositModel)
            }
            let actionCancel = UIAlertAction(title: localizedText(key: "cancel"), style: .default, handler: nil)
            alertController.addAction(actionCancel)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            self.showAlertController("Выберите счет")
        }
        
    }
    //    кнопка закрытия
    @IBAction func closeButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //    Закрытие депозита
    func postCloseDeposit(CloseDepositModel: CloseDepositModel){
        showLoading()
        managerApi
            .postCloseDeposit(CloseDepositModel: CloseDepositModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (reportsExecuteResult) in
                    guard let `self` = self else { return }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                    self.view.window?.rootViewController = vc
                    
                    let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
                    let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "MyAccountsViewController") as! MyAccountsViewController
                    let navigationController = UINavigationController(rootViewController: destinationController)
                    vc.pushFrontViewController(navigationController, animated: true)
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
