//
//  MyAccountsViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/11/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

// Мои счета
class MyAccountsViewController: BaseViewController {
    
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var navigationTitle = ""
    var currencies = [Currency]()
    var accounts = [Accounts]()
    var allFinance = [AllFinance]()
    var allFinanceCopy = [AllFinance]()
    var allFinanceCopy2 = [AllFinance]()
    var allAccounts: AllFinance!
    var allCards: AllFinance!
    var allDeposits: AllFinance!
    var allDepositsCopy: AllFinance!
    var allCredits: AllFinance!
    var credits: [CreditModel] = []
    var isOnSwitchDeposit = true
    var isOnSwitchCredit = true
    var currentCredit: CreditModel!
    var isOnSwitchTestDeposit: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.estimatedRowHeight = 140
        getAccountsWithCurrencies()
        getAllFinance()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = "Мои финансы"
        tableView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
        if  menu != nil{
            menu.target = self.revealViewController()
            menu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        tableView.reloadData()
        
    }
}
// таблица счетов
extension MyAccountsViewController: UITableViewDataSource, UITableViewDelegate, HeaderDelegate{
    
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return  allFinance.count
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let type = allFinance[section].accountsType
        switch type {
            
        case InternetBankingAccountType.Current.rawValue:
            if UserDefaults.standard.bool(forKey: "Header\(section)"){
                return allFinance[section].items.count
                
            }else{
                return 0
            }
            
        case InternetBankingAccountType.Deposit.rawValue:
            if UserDefaults.standard.bool(forKey: "Header\(section)"){
                
                return allFinance[section].items.count + 1
                
            }else{
                return 0
            }
            
        case InternetBankingAccountType.Card.rawValue:
            if UserDefaults.standard.bool(forKey: "Header\(section)"){
                
                return allFinance[section].items.count
                
            }else{
                return 0
            }
            
        case InternetBankingAccountType.Credit.rawValue:
            if UserDefaults.standard.bool(forKey: "Header\(section)"){
                
                return allFinance[section].items.count + 1
                
            }else{
                return 0
            }
            
        default:
            return 0
        }
    }
    // наименование header
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return allFinance[section].text
    }
    //  изменение цвета Header и title
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = HeaderView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height))
        headerView.delegate = self
        headerView.sectionIndex = section
        
        let type = allFinance[section].accountsType
        switch type {
            
        case InternetBankingAccountType.Current.rawValue:
            headerView.imageHeader.image = UIImage(named: "strongboxFin")
            if UserDefaults.standard.bool(forKey: "Header\(section)"){
                headerView.buttonHeader.setImage(UIImage(named: "chevron-top"), for: .normal)
                headerView.buttonHeader.contentHorizontalAlignment = .right
            }else{
                headerView.buttonHeader.setImage(UIImage(named: "chevron-bottom"), for: .normal)
                headerView.buttonHeader.contentHorizontalAlignment = .right
            }
            
        case InternetBankingAccountType.Deposit.rawValue:
            headerView.imageHeader.image = UIImage(named: "case")
            if UserDefaults.standard.bool(forKey: "Header\(section)"){
                headerView.buttonHeader.setImage(UIImage(named: "chevron-top"), for: .normal)
                headerView.buttonHeader.contentHorizontalAlignment = .right
            }else{
                headerView.buttonHeader.setImage(UIImage(named: "chevron-bottom"), for: .normal)
                headerView.buttonHeader.contentHorizontalAlignment = .right
            }
            
        case InternetBankingAccountType.Card.rawValue:
            headerView.imageHeader.image = UIImage(named: "creditcard")
            if UserDefaults.standard.bool(forKey: "Header\(section)"){
                headerView.buttonHeader.setImage(UIImage(named: "chevron-top"), for: .normal)
                headerView.buttonHeader.contentHorizontalAlignment = .right
            }else{
                headerView.buttonHeader.setImage(UIImage(named: "chevron-bottom"), for: .normal)
                headerView.buttonHeader.contentHorizontalAlignment = .right
            }
            
        case InternetBankingAccountType.Credit.rawValue:
            headerView.imageHeader.image = UIImage(named: "discount")
            if UserDefaults.standard.bool(forKey: "Header\(section)"){
                headerView.buttonHeader.setImage(UIImage(named: "chevron-top"), for: .normal)
                headerView.buttonHeader.contentHorizontalAlignment = .right
            }else{
                headerView.buttonHeader.setImage(UIImage(named: "chevron-bottom"), for: .normal)
                headerView.buttonHeader.contentHorizontalAlignment = .right
            }
            
        default:
            headerView.buttonHeader.setImage(UIImage(named: "chevron-top"), for: .normal)
            headerView.buttonHeader.contentHorizontalAlignment = .right
        }
        
        headerView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        let label = UILabel(frame: CGRect(x: 44, y: 13, width: view.frame.size.width, height: 25))
        label.text = self.allFinance[section].text.uppercased()
        label.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        headerView.addSubview(label)
        
        return headerView
    }
    
    
    //  изменение высоты Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let type = allFinance[section].accountsType
        switch type {
        case InternetBankingAccountType.Current.rawValue:
            
            return 50
        case InternetBankingAccountType.Deposit.rawValue:
            
            return 50
        case InternetBankingAccountType.Card.rawValue:
            
            return 50
        default:
            return 50
        }
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        let vc = OpenBankAccountController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyAccountTableViewCell
        let cellCard = tableView.dequeueReusableCell(withIdentifier: "CellCard", for: indexPath) as! CardTableViewCell
        let cellDeposit = tableView.dequeueReusableCell(withIdentifier: "CellDeposit", for: indexPath) as! DepositTableViewCell
        let cellCredit = tableView.dequeueReusableCell(withIdentifier: "CellCredit", for: indexPath) as! CreditTableViewCell
        let cellSwitch = tableView.dequeueReusableCell(withIdentifier: "CellSwitch", for: indexPath) as! CellSwitchTableViewCell
        let cellSwitchCredit = tableView.dequeueReusableCell(withIdentifier: "CellSwitchCredit", for: indexPath) as! CellSwitchCreditTableViewCell
        
        let type = allFinance[indexPath.section].accountsType
        
        switch type {
        case InternetBankingAccountType.Current.rawValue:
            if let item = allFinance[indexPath.section].items[indexPath.row] as? DetailAccountsModel{
                let stringBalance = String(format:"%.2f",item.balance ?? 0.0)
                let balance = stringBalance + " \(String(describing: item.currencySymbol ?? ""))"
                cell.accountNameLabel.text = "\(item.name ?? "") ∙∙ \(String(describing: item.accountNo?.suffix(4) ?? ""))"
                
                if item.DepositAccountStatusID == 4 {
                    cell.sumLabel.text = "Неактивный"
                    cell.sumLabel.textColor = UIColor(hexFromString: "B30931")
                } else {
                    cell.sumLabel.text = balance
                    cell.sumLabel.textColor = .black
                }
                
                self.getImageMyAccount(image: item.imageName ?? "", imageView: cell.accountImage, cell: cell)
                
                if item.isShowButton == true {
                    cellDeposit.titleButton.text = "Открыть счет"
                    cellDeposit.openDepositButton.addTarget(self, action: #selector(oneTestTapped(_:)), for: .touchUpInside)
                    cellDeposit.hiddenButton(isHidden: false)
                    selectedCellCustomColor(cell: cellDeposit)
                    return cellDeposit
                }
                
                if item.isShowSwithButton == true {
                    let switcher = UISwitch(frame: CGRect(x: 1, y: 1, width: 20, height: 20))
                    switcher.tintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                    switcher.onTintColor = UIColor(hexFromString: Constants.MAIN_COLOR)
                    switcher.isOn = isOnSwitchTestDeposit
                    cellSwitch.accessoryView = switcher
                    cellSwitch.label.text = "Показать только активные"
                    switcher.addTarget(self, action: #selector(MyAccountsViewController.switchOnTestOff(_:)), for: .valueChanged)
                    cellSwitch.selectionStyle = .none
                    selectedCellCustomColor(cell: cellSwitch)
                    return cellSwitch
                }
                
            }
            selectedCellCustomColor(cell: cell)
            return cell
            
        case InternetBankingAccountType.Deposit.rawValue:
            
            if indexPath.row == 0{
                let switcher = UISwitch(frame: CGRect(x: 1, y: 1, width: 20, height: 20))
                switcher.tintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                switcher.onTintColor = UIColor(hexFromString: Constants.MAIN_COLOR)
                switcher.isOn = isOnSwitchDeposit
                cellSwitch.accessoryView = switcher
                switcher.addTarget(self, action: #selector(switchOnOff), for: .valueChanged)
                cellSwitch.selectionStyle = .none
                selectedCellCustomColor(cell: cellSwitch)
                return cellSwitch
            }else{
                
                if let item = allFinance[indexPath.section].items[indexPath.row - 1] as? DetailDepositsModel{
                    let stringBalance = String(format:"%.2f",item.balance ?? 0.0)
                    let balance = stringBalance + " \(String(describing: item.currencySymbol ?? ""))"
                    cellDeposit.accountNameLabel.text = "\(item.name ?? "") ∙∙ \(String(describing: item.accountNo?.suffix(4) ?? ""))"
                   
                    cellDeposit.percentLabel.text = "\(item.interestRate ?? 0.0)%"
                    cellDeposit.openDepositButton.addTarget(self, action: #selector(MyAccountsViewController.oneTapped(_:)), for: .touchUpInside)
                    if item.closeDate != nil  {
                        cellDeposit.sumLabel.text = ""
                        cellDeposit.dateLabel.text = ""
                        cellDeposit.stateLabel.text = localizedText(key: "closed")
                    }else{
                        cellDeposit.sumLabel.text = balance
                        cellDeposit.dateLabel.text = dateFormaterBankDay(date: item.openDate ?? "")
                        cellDeposit.stateLabel.text = ""
                    }
                    if item.isShowButton != nil {
                        cellDeposit.hiddenButton(isHidden: false)
                        
                    }else{
                        cellDeposit.hiddenButton(isHidden: true)
                    }
                }
                selectedCellCustomColor(cell: cellDeposit)
                return cellDeposit
            }
        case InternetBankingAccountType.Card.rawValue:
            if let item = allFinance[indexPath.section].items[indexPath.row] as? DetailCardsModel{
                let stringBalance = String(format:"%.2f",item.balance ?? 0.0)
                let balance = stringBalance + " \(String(describing: ValueHelper.shared.getCurrentCurrency(currnecyID: item.currencyID ?? 0)))"
                cellCard.accountNameLabel.text = "\(item.name ?? "") ∙∙ \(String(describing: item.accountNo?.suffix(4) ?? ""))"
                cellCard.sumLabel.text = balance
                self.getImageCardAccounts(image: item.imageName ?? "", imageView: cell.accountImage, cell: cellCard)
            }
            selectedCellCustomColor(cell: cellCard)
            return cellCard
            
        case InternetBankingAccountType.Credit.rawValue:
            
            if indexPath.row == 0{
                let switcher = UISwitch(frame: CGRect(x: 1, y: 1, width: 20, height: 20))
                switcher.isOn = isOnSwitchCredit
                switcher.tintColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                switcher.onTintColor = UIColor(hexFromString: Constants.MAIN_COLOR)
                cellSwitchCredit.accessoryView = switcher
                switcher.addTarget(self, action: #selector(switchCreditOnOff), for: .valueChanged)
                cellSwitchCredit.selectionStyle = .none
                selectedCellCustomColor(cell: cellSwitchCredit)
                return cellSwitchCredit
            }else{
                if let item = allFinance[indexPath.section].items[indexPath.row - 1] as? CreditModel{
                    var stringDate = ""
                    var effectiveRate = ""
                    var accBalance = ""
                    if item.IsRequest ?? true{
                        stringDate = ""
                        accBalance = ""
                    }else{
                        stringDate = "\(localizedText(key: "next_repayment")): \(dateFormaterBankDay(date: item.NextPaymentDate ?? ""))"
                        accBalance = String(format:"%.2f",item.ApprovedSumm ?? 0.0) + " \(String(describing: item.ApprovedCurrency ?? ""))"
                    }
                    if item.EffectiveRate != nil{
                        effectiveRate = "\(item.ApprovedRate ?? 0.0)%"
                    }else{
                        effectiveRate = ""
                    }
                    cellCredit.accountNameLabel.text = "\(item.ProductName ?? "")"
                    cellCredit.sumLabel.text = accBalance
                    cellCredit.dateLabel.text = stringDate
                    cellCredit.stateLabel.text = item.StatusName
                    cellCredit.percentLabel.text = effectiveRate
                    cellCredit.openDepositButton.addTarget(self, action: #selector(MyAccountsViewController.oneTappedCredit(_:)), for: .touchUpInside)
                    
                    if item.isShowButton != nil {
                        cellCredit.hiddenButton(isHidden: false)
                        cellCredit.openDepositButton.titleLabel?.text = localizedText(key: "credit_application")
                        
                    }else{
                        cellCredit.hiddenButton(isHidden: true)
                    }
                }
                selectedCellCustomColor(cell: cellCredit)
                return cellCredit
            }
        default:
            return cell
        }
    }
    // обработка Switcher
    @objc func switchOnOff(_ sender: UISwitch) {
        
        if sender.isOn{
            isOnSwitchDeposit = true
            getAllFinance()
        }else{
            isOnSwitchDeposit = false
            getAllFinance()
        }
        
    }
        
    @objc func switchOnTestOff(_ sender: UISwitch) {
        
        if sender.isOn{
            isOnSwitchTestDeposit = true
            getAllFinance()
        }else{
            isOnSwitchTestDeposit = false
            getAllFinance()
        }
        
    }
    
    @objc func switchCreditOnOff(_ sender: UISwitch) {
        
        if sender.isOn{
            isOnSwitchCredit = true
            getAllFinance()
        }else{
            isOnSwitchCredit = false
            getAllFinance()
        }
    }
    
    // обработка нажатия кнопки на ячейке открыть депозит
    
    @objc func oneTestTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OpenBankAccountController") as! OpenBankAccountController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @objc func oneTapped(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductsDepositViewController") as! ProductsDepositViewController
        vc.accounts = self.accounts
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // обработка нажатия кнопки на ячейке кредит
    @objc func oneTappedCredit(_ sender: UIButton) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProductsDepositViewController") as! ProductsDepositViewController
        vc.accounts = self.accounts
        vc.isCreateRequestCredit = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let type = allFinance[indexPath.section].accountsType
        
        switch type {
        case InternetBankingAccountType.Current.rawValue:
            let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
            
            let item = allFinance[indexPath.section].items[indexPath.row] as? DetailAccountsModel
            
            if item?.DepositAccountStatusID != nil {
                vc.currentAccount = item
                vc.operationType = InternetBankingAccountType.Current.rawValue
                vc.navigationTitle = localizedText(key: "account_information")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        case InternetBankingAccountType.Deposit.rawValue:
            if indexPath.row != 0{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
                vc.currentDeposit = allFinance[indexPath.section].items[indexPath.row - 1] as? DetailDepositsModel
                self.navigationController?.pushViewController(vc, animated: true)
                vc.operationType = InternetBankingAccountType.Deposit.rawValue
                vc.accounts = self.accounts
                vc.navigationTitle = localizedText(key: "deposit_information")
            }
            
        case InternetBankingAccountType.Card.rawValue:
            let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
            vc.currentCard = allFinance[indexPath.section].items[indexPath.row] as? DetailCardsModel
            self.navigationController?.pushViewController(vc, animated: true)
            vc.operationType = InternetBankingAccountType.Card.rawValue
            vc.navigationTitle = localizedText(key: "card_information")
            
        case InternetBankingAccountType.Credit.rawValue:
            if indexPath.row != 0{
                self.currentCredit = allFinance[indexPath.section].items[indexPath.row - 1] as? CreditModel
                if self.currentCredit.StatusID == StatusType.Denied.rawValue{
                    showAlertController(self.localizedText(key: "could_not_find_credit"))
                }else if self.currentCredit.StatusID == StatusType.inProgress.rawValue{
                    
                    let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "CreditInProcessViewController") as! CreditInProcessViewController
                    vc.currentCredit = allFinance[indexPath.section].items[indexPath.row - 1] as? CreditModel
                    vc.navigationTitle = localizedText(key: "credit_information")
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
                    vc.currentCredit = allFinance[indexPath.section].items[indexPath.row - 1] as? CreditModel
                    vc.accounts = self.accounts
                    vc.operationType = InternetBankingAccountType.Credit.rawValue
                    vc.navigationTitle = localizedText(key: "credit_information")
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        default:
            print("")
        }
    }
    //    обработка нажатия на header
    func callHeader(index: Int) {
        
        setHeaderExpandable(allFinance:allFinance, index: index)
        
        allFinance[index].isExpandable = !(allFinance[index].isExpandable ?? true)
        if allFinance[index].isExpandable ?? false{
            UserDefaults.standard.set(true, forKey: "Header\(index)")
        }else{
            UserDefaults.standard.set(false, forKey: "Header\(index)")
        }
        
        tableView.reloadSections([index], with: .automatic)
    }
    
    func setHeaderExpandable(allFinance: [AllFinance], index: Int){
        for finance in allFinance{
            finance.isExpandable = UserDefaults.standard.bool(forKey: "Header\(index)")
        }
    }
}

extension MyAccountsViewController{
    //получение депозитов
    func  getAccountsWithCurrencies() {
        
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        
        let depositObservable:Observable<[Accounts]> = managerApi
            .getAccounts(currencyID: Constants.NATIONAL_CURRENCY)
        
        showLoading()
        
        Observable
            .zip(currenciesObservable, depositObservable)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (Currencies, Accounts) in
                    guard let `self` = self else { return }
                    self.currencies = Currencies
                    self.accounts = self.accountsCurrencies(currency: self.currencies, accounts: Accounts)
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
        
    }
    // добавление соответсвующей валюты в депозит
    func accountsCurrencies(currency: [Currency], accounts:[Accounts]) -> [Accounts]{
        
        for account in accounts{
            for item in account.items!{
                for idCurrency in currency{
                    if item.currencyID == idCurrency.currencyID{
                        item.currency = idCurrency
                    }
                }
            }
        }
        return accounts
    }
    
    // получение всех финансов
    func getAllFinance(){
     
        let getDetailsCards: Observable<[DetailCardsModel]>!
        let getDetailsDeposits:Observable<[DetailDepositsModel]>!
        let getDetailsCredits:Observable<DetailCreditsModel>!
        
        
        let getDetailsAccounts:Observable<[DetailAccountsModel]>  = managerApi
            .getDetailsAccounts()
        
        
        if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.DepositAccountSecurityOperations.IsViewAllowed"){
            getDetailsDeposits = managerApi
                .getDetailsDeposits()
        } else {
            getDetailsDeposits = Observable.empty()
        }
        
        if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsViewAllowed"){
            getDetailsCards = managerApi
                .getDetailsCards()
        } else {
            getDetailsCards = Observable.empty()
        }
        
        if AllowHelper.shared.loanAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsViewAllowed"){
            getDetailsCredits = managerApi
                .getDetailsCredits()
        } else {
            getDetailsCredits = Observable.empty()
        }

        
        let getAllProductsDepositModel = managerApi.getAllProductsDepositModel()
            
        
        
        
        
        showLoading()
        Observable
            .zip(getDetailsAccounts,
                 getDetailsDeposits,
                 getDetailsCards,
                 getDetailsCredits,
                 getAllProductsDepositModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (accounts, deposits, cards, credits, getAllProductsDepositModel) in
                    guard let `self` = self else { return }
                    self.allFinance.removeAll()
                    
                    
                    self.credits.removeAll()
                    
                    self.credits.append(contentsOf: credits.credits ?? [])
                    self.credits.append(contentsOf: credits.requests ?? [])
                    
                    dump(credits)
                    
                    
                    var result: [AnyObject] = []
                    
                    if self.isOnSwitchTestDeposit == true {
                        accounts.forEach { item in
                            if item.DepositAccountStatusID == 1 || item.DepositAccountStatusID == 5 {
                                result.append(item)
                            }
                        }
                    } else {
                        result = accounts
                    }
                    
                    self.allAccounts = AllFinance(text: self.localizedText(key: "current_accounts"), accountsType: 0, items: result)
                    self.allAccounts.items.insert(DetailAccountsModel(isShowSwithButton: true), at: 0)
                    
                    if getAllProductsDepositModel.count > 0 {
                        self.allAccounts.items.append(DetailAccountsModel(isShowButton: true))
                    }
                                        
                    if self.isOnSwitchDeposit{
                        self.allDeposits = AllFinance(text: self.localizedText(key: "deposit_accounts"), accountsType: 1, items: deposits.filter({$0.closeDate == nil}))
                    }else{
                        self.allDeposits = AllFinance(text: self.localizedText(key: "deposit_accounts"), accountsType: 1, items: deposits)
                    }
                    
                    self.allCards = AllFinance(text: self.localizedText(key: "cards"), accountsType: 2, items: cards)
                    if self.isOnSwitchCredit{
                        self.allCredits = AllFinance(text: self.localizedText(key: "credits"), accountsType: 3, items: self.credits.filter({$0.StatusID == 5}))
                    }else{
                        self.allCredits = AllFinance(text: self.localizedText(key: "credits"), accountsType: 3, items: self.credits)
                    }
                    
                    let lastDepositWithButton: DetailDepositsModel = DetailDepositsModel(isShowButton: true)
                    let _ = self.allDeposits.map({ if $0.accountsType == 1{ $0.items.append(lastDepositWithButton) }})
                    
                    let lastCreditWithButton: CreditModel = CreditModel(isShowButton: true)
                    let _ = self.allCredits.map({ $0.items.append(lastCreditWithButton) })
                    
                    if !accounts.isEmpty{
                        self.allFinance.append(self.allAccounts)
                    }
                    if !cards.isEmpty{
                        self.allFinance.append(self.allCards)
                    }
                    self.allFinance.append(self.allDeposits)
                    self.allFinance.append(self.allCredits)
                    
                    self.tableView.reloadData()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
}
//  делегат для headerView
protocol HeaderDelegate {
    func callHeader(index: Int)
}
// кастомизация headerView
class HeaderView: UIView {
    
    var sectionIndex: Int?
    var delegate: HeaderDelegate?
    
    override init(frame: CGRect) {
        super .init(frame: frame)
        self.addSubview(buttonHeader)
        self.addSubview(imageHeader)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    lazy var buttonHeader: UIButton = {
        
        let buttonHeader = UIButton(frame: CGRect(x:  -16, y: self.frame.origin.y, width: self.frame.size.width, height: 48))
        
        buttonHeader.addTarget(self, action: #selector(headerTapped(_:)), for: .touchUpInside)
        
        return buttonHeader
    }()
    lazy var imageHeader: UIImageView = {
        
        let imageHeader = UIImageView(frame: CGRect(x:  16, y: 15, width: 22, height: 22))
        
        return imageHeader
    }()
    
//    нажатие на header
    @objc func headerTapped(_ sender: UIButton) {
        
        if let indexHeader = sectionIndex{
            delegate?.callHeader(index: indexHeader)
        }
    }
}

extension MyAccountsViewController{
  //    права доступа
    func allowOperations(){
        
        if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsViewAllowed"){
            
        }
    }
}

