//
//  CardInfoModalViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/17/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

//  инфо по картам
class CardInfoModalViewController: BaseViewController {
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var accountNoLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var expiredCardDate: UILabel!
    
    let direction: UISwipeGestureRecognizer.Direction = .down
    var currencySymbol = ""
    var accountModel: AccountModel!
    var infoDetail: InfoDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        getDetailCard(accountNo: accountModel.accountNo ?? "", currencyID: accountModel.currencyID ?? 0)
        
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(CardInfoModalViewController.handleSwipe(gesture:)))
        gesture.direction = direction
        self.view?.addGestureRecognizer(gesture)
        
    }
    //    закрытие карты
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
        print("down gesture")
    }
    //    закрытие карты
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    //    запрос для получения деталей текущей карты
    func  getDetailCard(accountNo: String, currencyID: Int){
        
        showLoading()
        managerApi
            .getInfoCard(accountNo: accountNo,
                         currencyID: currencyID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] details in
                    guard let `self` = self else { return }
                    
                    self.accountNoLabel.text = details.cardNo
                    self.balanceLabel.text = "\(details.balance ?? 0.0) \(self.currencySymbol)"
                    self.expiredCardDate.text = self.dateFormaterBankDay(date: details.expiryDate ?? "")
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
