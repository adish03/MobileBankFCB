//
//  LoanScheduleTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/22/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

class LoanScheduleTableViewCell: UITableViewCell {

    @IBOutlet weak var graphicIdLabel: UILabel!
    @IBOutlet weak var payDateLabel: UILabel!
    @IBOutlet weak var mainSummLabel: UILabel!
    @IBOutlet weak var percentsSummLabel: UILabel!
    
    @IBOutlet weak var salesTaxOnPercentsSummLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
