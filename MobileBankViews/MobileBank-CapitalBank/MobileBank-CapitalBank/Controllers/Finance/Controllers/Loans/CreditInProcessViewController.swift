//
//  CreditInProcessViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/4/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper

class CreditInProcessViewController: BaseViewController {

    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var mortrageType: UILabel!
    @IBOutlet weak var incomeApproveTypes: UILabel!
    @IBOutlet weak var currency: UILabel!
    @IBOutlet weak var period: UILabel!
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var summ: UILabel!
    @IBOutlet weak var creditPurpose: UILabel!
    @IBOutlet weak var statusName: UILabel!
    
    var navigationTitle = ""
    var currentCredit: CreditModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle

        getLoansDetails(creditID: currentCredit.CreditID ?? 0)
      
    }
    @IBAction func AlertSheetControl(_ sender: Any) {
        
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
       
        let actionRenouncement = UIAlertAction(title: self.localizedText(key: "refuse"), style: .default) { (action) in
            var renouncementText = ""
            let alert = UIAlertController(title: self.localizedText(key: "waiver_of_application"), message: self.localizedText(key: "reject_reason"), preferredStyle: .alert)
            
            let actionOK = UIAlertAction(title: "OK", style: .default) { (action) in
               
                let rejectCreditRequestModel = RejectCreditRequestModel(creditID: self.currentCredit.CreditID ?? 0, reason: renouncementText)
                self.postRejectRequest(rejectCreditRequestModel: rejectCreditRequestModel)
                self.navigationController?.popViewController(animated: true)
                
            }
            let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .default, handler: nil)
            
            actionOK.isEnabled = false
            
            alert.addTextField {
                (textFieldName: UITextField!) in
                textFieldName.placeholder = self.localizedText(key: "reject_reason")
            }
            
            // adding the notification observer here
            NotificationCenter.default.addObserver(forName: UITextField.textDidChangeNotification, object:alert.textFields?[0],
                                                   queue: OperationQueue.main) { (notification) -> Void in
                                                    
                                                    let textFieldName = alert.textFields?[0] as! UITextField
                                                    if textFieldName.text!.count > 0{
                                                        renouncementText = textFieldName.text ?? ""
                                                        actionOK.isEnabled = true
                                                    }else{
                                                        actionOK.isEnabled = false
                                                    }
            }
            
            alert.addAction(actionCancel)
            alert.addAction(actionOK)
    
            self.present(alert, animated: true, completion: nil)
        }
        
        let actionCancel  = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel, handler: nil)
        
        alertSheet.addAction(actionRenouncement)
        alertSheet.addAction(actionCancel)
        present(alertSheet, animated: true, completion: nil)
        
    }
    
    
    //  получение деталей кредита
    func getLoansDetails(creditID: Int){
        showLoading()
        managerApi
            .getLoansInProcessDetails(creditID: creditID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (creditDetail) in
                    guard let `self` = self else { return }
                    let credit = creditDetail[0]
                    self.productName.text = credit.productName
                    self.mortrageType.text = credit.mortrageType
                    self.incomeApproveTypes.text = credit.incomeApproveTypes
                    self.currency.text = credit.currency
                    self.period.text = "\(credit.period ?? 0) \(self.localizedText(key: "meters_dot"))."
                    self.rate.text = "\(credit.rate ?? 0) %"
                    self.summ.text = "\(credit.summ ?? 0)"
                    self.creditPurpose.text = credit.creditPurpose
                    self.statusName.text = credit.statusName
                   
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    Отказ клиента от заявки на получение кредита
    func postRejectRequest(rejectCreditRequestModel: RejectCreditRequestModel){
        showLoading()
        managerApi
            .postRejectRequest(RejectCreditRequestModel: rejectCreditRequestModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    guard let `self` = self else { return }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

