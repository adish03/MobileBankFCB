//
//  CreditDetailViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/25/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper

class CreditDetailViewController: BaseViewController {

    @IBOutlet weak var operationTypeLabel: UILabel!
    @IBOutlet weak var accountNoLabel: UILabel!
    @IBOutlet weak var nextPaymentDateLabel: UILabel!
    @IBOutlet weak var currentAccoutNoLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var accBalanceLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var currency: UILabel!
    
    var navigationTitle = ""
    var creditDetail: LoanPaymentModel!
    var operationCreditDetail: LoanPaymentModel!
    var operationTypeID = 0
    var paymentType = 0
    var confirmType = 0
    var operationID = 0
    var delegate: NoNeedLoadDataDelegate?
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.isNeedData(isNeed: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        switch paymentType {
            
        case  InternetBankingLoanPaymentType.OnSchedule.rawValue:
            self.operationTypeLabel.text = localizedText(key: "Internal_operation")
            
        case  InternetBankingLoanPaymentType.FullEarlyPayment.rawValue:
            self.operationTypeLabel.text = localizedText(key: "full_early_repayment")
            
        case  InternetBankingLoanPaymentType.PartialEarlyPayment.rawValue:
            self.operationTypeLabel.text = localizedText(key: "loan_repayment")
            
        case  InternetBankingLoanPaymentType.PartialEarlyPaymentMainDeptOnly.rawValue:
            self.operationTypeLabel.text = localizedText(key: "partial_early_repayment_of_principal")
            
        default:
            self.operationTypeLabel.text = ""
        }
        getOperation(operationID: operationID)
        getCreditID(operationID: operationID)
        
       
    }
//    сохранение шаблона
    @IBAction func saveTemplateButton(_ sender: Any) {
        
        if paymentType == InternetBankingLoanPaymentType.OnSchedule.rawValue{
            let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoanPlanePaymentViewController") as! LoanPlanePaymentViewController
            vc.isCreateTemplate = true
            vc.creditDetail = creditDetail
            vc.navigationTitle = localizedText(key: "create_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "LoanPaymentOperationsViewController") as! LoanPaymentOperationsViewController
            vc.isCreateTemplate = true
            vc.creditDetail = creditDetail
            vc.operationCreditDetail = self.operationCreditDetail
            vc.navigationTitle = localizedText(key: "create_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    //    сохранение шаблона 
    @IBAction func AlertSheetControl(_ sender: Any) {
        
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionRepeatOperation = UIAlertAction(title: localizedText(key: "create_operation_request"), style: .default) { (action) in
            if self.paymentType == InternetBankingLoanPaymentType.OnSchedule.rawValue{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanPlanePaymentViewController") as! LoanPlanePaymentViewController
                vc.creditDetail = self.creditDetail
                vc.isRepeatPay = true
                vc.isNewPay = true
                vc.navigationTitle = self.localizedText(key: "loan_repayment")

                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanPaymentOperationsViewController") as! LoanPaymentOperationsViewController
                vc.creditDetail = self.creditDetail
                vc.operationCreditDetail = self.operationCreditDetail
                vc.isRepeatPay = true
                vc.isNewPay = true
                vc.navigationTitle = self.localizedText(key: "loan_repayment")

                self.navigationController?.pushViewController(vc, animated: true)
            }

        }
        let actionHSaveTemplate = UIAlertAction(title: self.localizedText(key: "save_as_template"), style: .default) { (action) in
            if self.paymentType == InternetBankingLoanPaymentType.OnSchedule.rawValue{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanPlanePaymentViewController") as! LoanPlanePaymentViewController
                vc.isCreateTemplate = true
                vc.navigationTitle = self.localizedText(key: "create_template")
                vc.operationCreditDetail = self.operationCreditDetail
                vc.creditDetail = self.creditDetail
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanPaymentOperationsViewController") as! LoanPaymentOperationsViewController
                vc.isCreateTemplate = true
                vc.navigationTitle = self.localizedText(key: "create_template")
                vc.operationCreditDetail = self.operationCreditDetail
                vc.creditDetail = self.creditDetail
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        let changeData = UIAlertAction(title: self.localizedText(key: "edit"), style: .default) { (action) in
            
            if self.paymentType == InternetBankingLoanPaymentType.OnSchedule.rawValue{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanPlanePaymentViewController") as! LoanPlanePaymentViewController
                vc.creditDetail = self.creditDetail
                vc.isRepeatPay = true
                vc.navigationTitle = self.localizedText(key: "loan_repayment")
                
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanPaymentOperationsViewController") as! LoanPaymentOperationsViewController
                vc.creditDetail = self.creditDetail
                vc.operationCreditDetail = self.operationCreditDetail
                vc.isRepeatPay = true
                vc.navigationTitle = self.localizedText(key: "loan_repayment")
                
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        let actionDocuments = UIAlertAction(title: self.localizedText(key: "download_check"), style: .default) { (action) in
            
            ApiHelper.shared.getInfoOperationFile(operationID: self.operationID,
                                                  code: 00000,
                                                  operationTypeID: self.creditDetail.operationType ?? 0, vc: self)
            
        }
        
        let actionRemove = UIAlertAction(title: self.localizedText(key: "delete"), style: .default) { (action) in
            let alert = UIAlertController(title:  self.localizedText(key: "delete_operation"), message: "", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                ApiHelper.shared.deleteTemplate(operationID: "\(self.operationID)")
                self.navigationController?.popViewController(animated: true)
            })
            let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)
            alert.addAction(actionOk)
            alert.addAction(actionCancel)
            self.present(alert, animated: true, completion: nil)
        }
        
        let sendToBank = UIAlertAction(title: self.localizedText(key: "send_to_bank"), style: .default) { (action) in
            
        }
        
       let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)

        // отображение действий в зависимости от confirmType
        switch confirmType {
        case ConfirmType.Save.rawValue:
            if AllowHelper.shared.loanAllowOpertaion(nameOperation: ""){
                alertSheet.addAction(actionRepeatOperation)
            }
            alertSheet.addAction(actionRepeatOperation)
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsEditAllowed"){
                alertSheet.addAction(changeData)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsRemoveAllowed"){
                alertSheet.addAction(actionRemove)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Confirm.rawValue:
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            alertSheet.addAction(actionDocuments)
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.InConfirm.rawValue:
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            alertSheet.addAction(actionRepeatOperation)
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsEditAllowed"){
                alertSheet.addAction(changeData)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsRemoveAllowed"){
                alertSheet.addAction(actionRemove)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Error.rawValue:
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            alertSheet.addAction(actionRepeatOperation)
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsEditAllowed"){
                alertSheet.addAction(changeData)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsRemoveAllowed"){
                alertSheet.addAction(actionRemove)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Verified.rawValue:
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            alertSheet.addAction(actionRepeatOperation)
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsEditAllowed"){
                alertSheet.addAction(changeData)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsRemoveAllowed"){
                alertSheet.addAction(actionRemove)
            }
            alertSheet.addAction(actionCancel)
            
        default:
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            alertSheet.addAction(actionRepeatOperation)
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsEditAllowed"){
                alertSheet.addAction(changeData)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsRemoveAllowed"){
                alertSheet.addAction(actionRemove)
            }
            alertSheet.addAction(actionCancel)
        }
        
        
        present(alertSheet, animated: true, completion: nil)
    }
    
    //    получение creditID
    func  getCreditID(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationCredit(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operationCreditDetail) in
                    guard let `self` = self else { return }
                    self.operationCreditDetail = operationCreditDetail
                    self.confirmType = operationCreditDetail.confirmType ?? 0

                    (self.statusLabel.text, self.statusLabel.textColor) = ValueHelper.shared.confirmTypeDefine(confirmType: operationCreditDetail.confirmType ?? 0)
                    self.getLoansDetails(creditID: operationCreditDetail.creditID ?? 0)
                   
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    получение инфо об операции
    func  getLoansDetails(creditID: Int) {
        showLoading()
        
        managerApi
            .getLoansDetails(creditID: creditID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (creditDetail) in
                    guard let `self` = self else { return }
                    self.creditDetail = creditDetail
                    
                    self.nextPaymentDateLabel.text = self.dateFormaterBankDay(date: creditDetail.loan?.nextPaymentDate ?? "")

                    self.currency.text = ValueHelper.shared.getCurrentCurrency(currnecyID: creditDetail.currencyID ?? 0)
                    
                    self.accBalanceLabel.text = "\(creditDetail.loan?.loanBalance! ?? 0.0)"
                   
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    получение инфо об операции
    func  getOperation(operationID: Int) {
        showLoading()
        
        managerApi
            .getOperation(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operationDetail) in
                    guard let `self` = self else { return }
                    self.accountNoLabel.text = "\(operationDetail.accountNo!)"
                    self.currentAccoutNoLabel.text = "\(operationDetail.сurrentAccountNo!)"
                    self.amountLabel.text = "\(operationDetail.paymentSum!)"
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
