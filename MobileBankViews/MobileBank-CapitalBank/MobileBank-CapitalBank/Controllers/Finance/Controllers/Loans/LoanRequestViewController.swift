//
//  LoanRequestViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/22/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

// класс для запроса кредита
class LoanRequestViewController: BaseViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    //error
    @IBOutlet weak var labelErrorMortage: UILabel!
    @IBOutlet weak var labelErrorApproved: UILabel!
    @IBOutlet weak var labelErrorCurrencyt: UILabel!
    @IBOutlet weak var labelErrorPeriod: UILabel!
    @IBOutlet weak var labelErrorAmount: UILabel!
    @IBOutlet weak var labelErrorRate: UILabel!
    @IBOutlet weak var labelErrorDescription: UILabel!
    
    @IBOutlet weak var viewMortageTypeTextField: UIView!
    @IBOutlet weak var viewApprovedTypeTextField: UIView!
    @IBOutlet weak var viewCurrencyTextField: UIView!
    @IBOutlet weak var viewPeriodTextField: UIView!
    @IBOutlet weak var viewAmountTextField: UIView!
    @IBOutlet weak var viewRateTextField: UIView!
    @IBOutlet weak var viewDescriptionTextField: UIView!
    
    
    @IBOutlet weak var periodAdditionalLabel: UILabel!
    @IBOutlet weak var amountAdditionalLabel: UILabel!
    @IBOutlet weak var rateAdditionalLabel: UILabel!
    
    @IBOutlet weak var mortageTypeTextField: UITextField!
    @IBOutlet weak var approvedTypeTextField: UITextField!
    @IBOutlet weak var currencyTextField: UITextField!
    @IBOutlet weak var periodTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var currencyLabel: UILabel!
    
    @IBOutlet weak var rateTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    
    
    var pickerViewMortrage = UIPickerView()
    var pickerViewApprove = UIPickerView()
    var pickerViewCurrency = UIPickerView()
    
    var mortrageTypes = [LoanProductMortrageTypeModel]()
    var approveTypes = [LoanProductApproveTypeModel]()
    var currencies = [LoanProductCurrencyModel]()
    var interests = [LoanProductInterestModel]()
    var loansMortrageTypes = [ReferenceCreditItemModel]()
    var loansIncomeApproveTypes = [ReferenceCreditItemModel]()
    
    var mortrageTypeID = 0
    var approveTypeID = 0
    var currencyID = 0
    var period = 0
    var amount = 0.0
    var rate = 0.0
    
    var minPeriod = 0
    var maxPeriod = 0
    var minAmount = 0.0
    var maxAmount = 0.0
    var minRate = 0.0
    var maxRate = 0.0
    
    var navigationTitle = ""
    var product: LoanProductModel!
    var accounts = [Accounts]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        nameLabel.text = product.name
        descriptionLabel.text = product.description
        
        mortageTypeTextField.delegate = self
        approvedTypeTextField.delegate = self
        currencyTextField.delegate = self
        periodTextField.delegate = self
        amountTextField.delegate = self
        rateTextField.delegate = self
        descriptionTextField.delegate = self
        
        mortrageTypes = product.mortrageTypes ?? []
        
        let titleApproveTypes = LoanProductApproveTypeModel(name: self.localizedText(key: "select_confirmation_form"))
        approveTypes.insert(titleApproveTypes, at: 0)
        
        let titleCurrency = LoanProductCurrencyModel(name: localizedText(key: "select_currency"))
        currencies.insert(titleCurrency, at: 0)
        
        getApproveTypesMortrageTypes()
        
        mortageTypeTextField.addTarget(self, action: #selector(textFieldEditingmortageTypeTextField(_:)), for: UIControl.Event.editingDidEnd)
        approvedTypeTextField.addTarget(self, action: #selector(textFieldEditingapprovedTypeTextField(_:)), for: UIControl.Event.editingDidEnd)
        currencyTextField.addTarget(self, action: #selector(textFieldEditingcurrencyTextField(_:)), for: UIControl.Event.editingDidEnd)
        periodTextField.addTarget(self, action: #selector(textFieldEditingperiodTextField(_:)), for: UIControl.Event.editingDidEnd)
        amountTextField.addTarget(self, action: #selector(textFieldEditingamountTextField(_:)), for: UIControl.Event.editingDidEnd)
        rateTextField.addTarget(self, action: #selector(textFieldEditingrateTextField(_:)), for: UIControl.Event.editingDidEnd)
        descriptionTextField.addTarget(self, action: #selector(textFieldEditingdescriptionTextField(_:)), for: UIControl.Event.editingDidEnd)
        
    }
    
    // Проверка поля "Вид обеспечения" на наличие данных, после потери фокуса
    @objc func textFieldEditingmortageTypeTextField(_ sender: Any) {
        mortageTypeTextField.stateIfEmpty(view:  viewMortageTypeTextField, labelError: labelErrorMortage)
    }
    // Проверка поля "Форма подтверждения" на наличие данных, после потери фокуса
    @objc func textFieldEditingapprovedTypeTextField(_ sender: Any) {
        approvedTypeTextField.stateIfEmpty(view: viewApprovedTypeTextField, labelError: labelErrorApproved)
    }
    // Проверка поля "Валюта" на наличие данных, после потери фокуса
    @objc func textFieldEditingcurrencyTextField(_ sender: Any) {
        currencyTextField.stateIfEmpty(view: viewCurrencyTextField, labelError: labelErrorCurrencyt)
    }
    // Проверка поля "На срок" на наличие данных, после потери фокуса
    @objc func textFieldEditingperiodTextField(_ sender: Any) {
        periodTextField.stateIfEmpty(view: viewPeriodTextField, labelError: labelErrorPeriod)
        
        if let period = periodTextField.text{
            if period != ""{
                periodTextField.text = period + " " + localizedText(key: "month")
            }
        }
        if let number = Int.parse(from: periodTextField.text ?? "") {
            print("number \(number)")
            
            //        let period = Int(periodTextField.text ?? "") ?? 0
            if number >= minPeriod && number <= maxPeriod{
                
                self.period = number
            }else{
                if number < minPeriod{
                    
                    let alertController = UIAlertController(title: nil, message: "\(localizedText(key: "please_enter_a_number_greater_than_or_equal_to")) \(minPeriod).", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                        self.periodTextField.text = ""
                        self.periodTextField.becomeFirstResponder()
                        
                    }))
                    present(alertController, animated: true, completion: nil)
                    
                }
                if number > maxPeriod{
                    let alertController = UIAlertController(title: nil, message: "\(localizedText(key: "please_enter_a_number_less_than_or_equal_to")) \(maxPeriod).", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        
                        self.periodTextField.text = ""
                        self.periodTextField.becomeFirstResponder()
                        
                    }))
                    present(alertController, animated: true, completion: nil)

                }
            }
        }
    }
    
    // Проверка поля "Сумма кредитования" на наличие данных, после потери фокуса
    @objc func textFieldEditingamountTextField(_ sender: Any) {
        amountTextField.stateIfEmpty(view: viewAmountTextField, labelError: labelErrorAmount)
        
        let summ = Double(amountTextField.text ?? "") ?? 0.0
        if summ >= minAmount && summ <= maxAmount{
            self.amount = summ
        }else{
            if summ < minAmount{
                showAlertController("\(localizedText(key: "please_enter_a_number_greater_than_or_equal_to")) \(minAmount)")
            }
            if summ > maxAmount{
                showAlertController("\(localizedText(key: "please_enter_a_number_less_than_or_equal_to")) \(maxAmount)")
            }
        }
    }
    // Проверка поля "Процентная ставка" на наличие данных, после потери фокуса
    @objc func textFieldEditingrateTextField(_ sender: Any) {
        rateTextField.stateIfEmpty(view: viewRateTextField, labelError: labelErrorRate)
        
        if let rate = rateTextField.text{
            if rate != ""{
                rateTextField.text = rate + " %"
            }
        }
        let doubleRate = rateTextField.text?.replacingOccurrences(of: " %", with: "")
      
        let rate = Double(doubleRate ?? "") ?? 0.0
        if rate >= minRate && rate <= maxRate{
            self.rate = rate
        }else{
            if rate < minRate{
                showAlertController("\(localizedText(key: "please_enter_a_number_greater_than_or_equal_to")) \(minRate)")
            }
            if rate > maxRate{
                showAlertController("\(localizedText(key: "please_enter_a_number_less_than_or_equal_to")) \(maxRate)")
            }
        }
    }
    // Проверка поля "Цель кредита" на наличие данных, после потери фокуса
    @objc func textFieldEditingdescriptionTextField(_ sender: Any) {
        if descriptionTextField.text == ""{
            descriptionTextField.stateIfEmpty(view: viewDescriptionTextField, labelError: labelErrorDescription)
        }
    
    }
    
    @IBAction func RequestCreditButton(_ sender: Any) {
        
        if periodTextField.text != "" &&
            amountTextField.text != "" &&
            rateTextField.text != "" &&
            descriptionTextField.text != ""{
            
            let alertController = UIAlertController(title: nil, message: self.localizedText(key: "do_you_reallly_want_request_credit"), preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.amount = Double(self.amountTextField.text ?? "") ?? 0.0
                let сreateRequestLoanModel = CreateRequestLoanModel(
                    productID: self.product.iD,
                    mortrageTypeID: self.mortrageTypeID,
                    approveTypeID: self.approveTypeID,
                    currencyID: self.currencyID,
                    period: self.period,
                    amount: self.amount,
                    rate: self.rate,
                    creditPurpose: self.descriptionLabel.text ?? "")
                
                self.postCreateRequest(сreateRequestLoanModel: сreateRequestLoanModel)
            }
            let actionCancel = UIAlertAction(title: localizedText(key: "cancel"), style: .default, handler: nil)
            alertController.addAction(actionCancel)
            alertController.addAction(action)
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            periodTextField.stateIfEmpty(view: viewPeriodTextField, labelError: labelErrorPeriod)
            amountTextField.stateIfEmpty(view: viewAmountTextField, labelError: labelErrorAmount)
            rateTextField.stateIfEmpty(view: viewRateTextField, labelError: labelErrorRate)
            descriptionTextField.stateIfEmpty(view: viewDescriptionTextField, labelError: labelErrorDescription)
        }
    }
    
    
    
}
// выбор процентов
extension LoanRequestViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    //  установка дефолтных значений pickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //  установка дефолтных значений периода
    func pickerSettings() {
        
        pickerViewMortrage.delegate = self
        pickerViewApprove.delegate = self
        pickerViewCurrency.delegate = self
        
        mortageTypeTextField.inputView = pickerViewMortrage
        approvedTypeTextField.inputView = pickerViewApprove
        currencyTextField.inputView = pickerViewCurrency
        
        pickerViewMortrage.selectRow(0, inComponent: 0, animated: true)
        pickerViewApprove.selectRow(0, inComponent: 0, animated: true)
        pickerViewCurrency.selectRow(0, inComponent: 0, animated: true)
        
        mortageTypeTextField.text = mortrageTypes[0].name
        approvedTypeTextField.text = approveTypes[0].name
        currencyTextField.text = currencies[0].name
    }
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count = 0
        if pickerView == self.pickerViewMortrage {
            count = mortrageTypes.count
        }else if pickerView == self.pickerViewApprove{
            if approveTypes.count != 0{
                count = approveTypes.count
            }
        }else if pickerView == self.pickerViewCurrency{
            if currencies.count != 0{
                count = currencies.count
            }
        }
        return count
    }
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var list = ""
        
        if pickerView == self.pickerViewMortrage {
            list = mortrageTypes[row].name ?? ""
        }else if pickerView == self.pickerViewApprove{
            if approveTypes.count != 0{
                list = approveTypes[row].name ?? ""
            }
        }else if pickerView == self.pickerViewCurrency{
            if currencies.count != 0{
                list = "\(ValueHelper.shared.getCurrentCurrency(currnecyID: self.currencies[row].currencyID ?? 0))"
            }
        }
        return list
    }
    // выбор полей
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.pickerViewMortrage{
            self.approveTypes.removeAll()
            self.currencies.removeAll()
            let titleApproveTypes = LoanProductApproveTypeModel(name: self.localizedText(key: "select_confirmation_form"))
            approveTypes.insert(titleApproveTypes, at: 0)
            let titleCurrency = LoanProductCurrencyModel(name: localizedText(key: "select_currency"))
            currencies.insert(titleCurrency, at: 0)
                self.mortrageTypeID = mortrageTypes[row].mortrageTypeID ?? 0
                self.approveTypes.append(contentsOf: mortrageTypes[row].approveTypes ?? [])
                self.approvedTypeTextField.text = ""
                self.approveTypeID = 0
                self.currencyTextField.text = ""
                self.currencyID = 0
                self.periodTextField.text = ""
                self.period = 0
                self.amountTextField.text = ""
                self.amount = 0.0
                self.rateTextField.text = ""
                self.rate = 0
                
                for type in self.approveTypes {
                    for id in self.loansIncomeApproveTypes{
                        if type.approveTypeID == id.value{
                            type.name = id.text
                        }
                    }
                self.mortageTypeTextField.text = mortrageTypes[row].name ?? ""
            }
        } else if pickerView == self.pickerViewApprove{
            self.currencies.removeAll()
            let titleCurrency = LoanProductCurrencyModel(name: localizedText(key: "select_currency"))
            currencies.insert(titleCurrency, at: 0)
                self.approveTypeID = approveTypes[row].approveTypeID ?? 0
                self.currencyTextField.text = ""
                self.currencyID = 0
                self.amountTextField.text = ""
                self.amount = 0.0
                self.rateTextField.text = ""
                self.rate = 0
                self.currencies.append(contentsOf: approveTypes[row].currencies ?? [])
                self.approvedTypeTextField.text = approveTypes[row].name ?? ""
            
        }else if pickerView == self.pickerViewCurrency{
            if(row != 0){
                self.currencyID = currencies[row].currencyID ?? 0
                self.currencyLabel.text = "\(ValueHelper.shared.getCurrentCurrency(currnecyID: self.currencies[row].currencyID ?? 0))"
                self.interests = currencies[row].interests ?? []
                self.currencyTextField.text = "\(ValueHelper.shared.getCurrentCurrency(currnecyID: self.currencies[row].currencyID ?? 0))"
                self.amountTextField.text = ""
                self.amount = 0.0
                self.rateTextField.text = ""
                self.rate = 0
                minPeriod = interests.first?.minPeriod ?? 0
                maxPeriod = interests.first?.maxPeriod ?? 0
                minAmount = interests.first?.minSumm ?? 0
                maxAmount = interests.first?.maxSumm ?? 0
                minRate = interests.first?.minRate ?? 0.0
                maxRate = interests.first?.maxRate ?? 0.0
                periodAdditionalLabel.text = "\(localizedText(key: "available_time")) \(interests.first?.minPeriod ?? 0) - \(interests.first?.maxPeriod ?? 0) \(localizedText(key: "month"))"
                amountAdditionalLabel.text = "\(localizedText(key: "allowable_amount")) \(interests.first?.minSumm ?? 0) - \(interests.first?.maxSumm ?? 0) \(ValueHelper.shared.getCurrentCurrency(currnecyID: currencyID))"
                rateAdditionalLabel.text = "\(localizedText(key: "allowable_interest_rate")) \(interests.first?.minRate ?? 0) - \(interests.first?.maxRate ?? 0)%"
            }
        }
    }
    // кастомизация pickerView
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if pickerView == self.pickerViewMortrage {
            if let v = view as? UILabel { label = v }
            label.font = UIFont (name: "Helvetica Neue", size: 20)
            label.text =  mortrageTypes[row].name
            label.textAlignment = .center
        }else if pickerView == self.pickerViewApprove{
            if let v = view as? UILabel { label = v }
            label.font = UIFont (name: "Helvetica Neue", size: 20)
            label.text =  approveTypes[row].name
            label.textAlignment = .center
        }
        else if pickerView == self.pickerViewCurrency{
            if let v = view as? UILabel { label = v }
            label.font = UIFont (name: "Helvetica Neue", size: 20)
            label.text = "\(ValueHelper.shared.getCurrentCurrency(currnecyID: self.currencies[row].currencyID ?? 0))"
            label.textAlignment = .center
        }
        return label
    }
    
    //   Запрос на кредит
    func postCreateRequest(сreateRequestLoanModel: CreateRequestLoanModel){
        showLoading()
        managerApi
            .postCreateRequest(CreateRequestLoanModel: сreateRequestLoanModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](response) in
                    guard let `self` = self else { return }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                    self.view.window?.rootViewController = vc
                    
                    let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
                    let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "MyAccountsViewController") as! MyAccountsViewController
                    let navigationController = UINavigationController(rootViewController: destinationController)
                    vc.pushFrontViewController(navigationController, animated: true)
                    
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //   Запрос на Получение списка Форма подтверждения и Получение списка Вид обеспечения кредита
    func getApproveTypesMortrageTypes(){
        showLoading()
        
        let getLoansMortrageTypes:Observable<[ReferenceCreditItemModel]> = managerApi
            .getLoansMortrageTypes()
        let getLoansIncomeApproveTypes:Observable<[ReferenceCreditItemModel]> = managerApi
            .getLoansIncomeApproveTypes()
        
        Observable
            .zip(getLoansMortrageTypes, getLoansIncomeApproveTypes)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (loansMortrageTypes, loansIncomeApproveTypes ) in
                    guard let `self` = self else { return }
                    self.loansMortrageTypes = loansMortrageTypes
                    self.loansIncomeApproveTypes = loansIncomeApproveTypes
                    
                    for type in self.mortrageTypes {
                        for id in loansMortrageTypes{
                            if type.mortrageTypeID == id.value{
                                type.name = id.text
                            }
                        }
                    }
                    
                    self.pickerSettings()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension LoanRequestViewController: UITextFieldDelegate{
    //    проверка поля на начало ввода информации
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField.isEqual(approvedTypeTextField){
            if mortrageTypeID == 0{
                showAlertController(localizedText(key: "select_the_type_of_collateral"))
            }
        }
        if textField.isEqual(currencyTextField){
            if  approveTypeID == 0{
                showAlertController(self.localizedText(key: "select_confirmation_form"))
            }
        }
        if textField.isEqual(periodTextField){
            if currencyID == 0{
                showAlertController(localizedText(key: "select_currency"))
            }
        }
        if textField.isEqual(amountTextField){
            if currencyID == 0{
                showAlertController(localizedText(key: "select_currency"))
            }
        }
        if textField.isEqual(rateTextField){
            if currencyID == 0{
                showAlertController(localizedText(key: "select_currency"))
            }
        }
        if textField.isEqual(descriptionTextField){
            
        }
        return true
    }
    
    
    
    //    проверка поля на начало ввода информации на кол-во символов
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        if textField.isEqual(amountTextField) {
            maxLength = 11
        }
        if textField.isEqual(periodTextField) {
            maxLength = 3
        }
        if textField.isEqual(rateTextField) {
            maxLength = 3
        }
        if textField.isEqual(descriptionTextField) {
            maxLength = 100
        }
        return newLength <= maxLength
    }
    
}

extension Int {
    static func parse(from string: String) -> Int? {
        return Int(string.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())
    }
}






