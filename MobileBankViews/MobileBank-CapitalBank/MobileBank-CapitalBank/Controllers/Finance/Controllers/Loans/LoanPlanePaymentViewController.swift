//
//  LoanPlanePaymentViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/22/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

class LoanPlanePaymentViewController: BaseViewController, DepositProtocol, UITextFieldDelegate {
    
    @IBOutlet weak var payTo_accountLabel: UILabel!
    @IBOutlet weak var choiceCardTo_accountLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    
    @IBOutlet weak var creditNameTextField: UITextField!
    
    
    @IBOutlet weak var mainSummLabel: UILabel!
    @IBOutlet weak var percentSummLabel: UILabel!
    @IBOutlet weak var fineSummLabel: UILabel!
    @IBOutlet weak var totalSummLabel: UILabel!
    
    //error
    @IBOutlet weak var labelErrorTemplateName: UILabel!
    @IBOutlet weak var viewTemplateNameTextFields: UIView!
    
    @IBOutlet weak var viewToAccount: UIView!
    @IBOutlet weak var viewAmountTextField: UIView!
    @IBOutlet weak var labelErrorToAccount: UILabel!
    @IBOutlet weak var labelErrorAmount: UILabel!
    
    var creditDetail: LoanPaymentModel!
    var operationCreditDetail: LoanPaymentModel!
    var accounts = [Accounts]()
    var selectedDeposit: Deposit!
    var datePicker = UIDatePicker()
    var planDateToserver = ""
    var loanPaymentType = 0
    var navigationTitle = ""
    var credits: [CreditModel] = []
    var allCredits: [CreditModel] = []
    var currencyTemplate = 0
    var creditNoTemplate = ""
    var isNewPay = false
    
   
    var pickerView = UIPickerView()
    
    //template
    var isCreateTemplate = false
    @IBOutlet weak var TemplateDescriptionView: UIView!
    @IBOutlet weak var templateNameTextField: UITextField!
    
    var template: ContractModelTemplate!
    var isRepeatPay = false
    var isEditTemplate = false
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if selectedDeposit != nil{
            fullSelectedDeposit(selectedDeposit: selectedDeposit)
        }else{
            payTo_accountLabel.text = localizedText(key: "from_account")
            choiceCardTo_accountLabel.text = localizedText(key: "select_account_or_card")
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        amountTextField.delegate = self
        dateTextField.delegate = self
        showDatePicker()
        
        if isCreateTemplate || isEditTemplate{
            TemplateDescriptionView.isHidden = false
            getCreditsOpenForTemplate()
        }else{
            TemplateDescriptionView.isHidden = true
        }
        
        if isRepeatPay{
             getCreditsOpenForTemplate()
        }
        
        amountTextField.addTarget(self, action: #selector(textFieldEditingAmountTextField(_:)), for: UIControl.Event.editingDidEnd)
        templateNameTextField.addTarget(self, action: #selector(textFieldEditingEndtemplateName(_:)), for: UIControl.Event.editingDidEnd)
        
    }
    // Проверка поля "Название шаблона" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndtemplateName(_ sender: Any) {
        templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
    }
    
    // Проверка поля "Сумма" на наличие данных, после потери фокуса
    @objc func textFieldEditingAmountTextField(_ sender: Any) {
        amountTextField.stateIfEmpty(view:  viewAmountTextField, labelError: labelErrorAmount)
        let amount = Double(amountTextField.text ?? "") ?? 0.0
        if amount > selectedDeposit?.balance ?? 0.0{
            showAlertController(self.localizedText(key: "the_amount_of_the_operation_can_not_be_more_than_the_current_balance"))
        }
    }
    
    // кнопка расчета
    @IBAction func calculateButton(_ sender: Any) {
        
        let сalculatePlanLoanPayment = CalculatePlanLoanPayment(planDate: dateFormaterToServer(date: planDateToserver),
                                                                 creditID: creditDetail.creditID,
                                                                 tranchID: creditDetail.tranchID,
                                                                 paymentType: loanPaymentType)
        
        postCalculatePlanLoanPayment(сalculatePlanLoanPayment: сalculatePlanLoanPayment)
        
    }
    
    // кнопка  сохранения
    @IBAction func saveButton(_ sender: Any) {
        
        if selectedDeposit != nil && amountTextField.text != ""{
            
            if isCreateTemplate{
                if selectedDeposit != nil && amountTextField.text != "" && templateNameTextField.text != "" {
                    let amount = Double(self.amountTextField.text ?? "") ?? 0.0
                    let loanPaymentModel = LoanPaymentModel(
                        creditID: self.creditDetail.creditID,
                        tranchID: self.creditDetail.tranchID,
                        accountNo: self.selectedDeposit.accountNo,
                        currentAccountNo: self.creditDetail.currentAccountNo,
                        currentAccountBalance: self.creditDetail.currentAccountBalance,
                        paymentSumm: amount,
                        currencyID: self.creditDetail.currencyID,
                        paymentType: self.loanPaymentType,
                        programName: self.creditDetail.programName,
                        operationType: nil,
                        operationID: 0,
                        isSchedule: false,
                        isTemplate: self.isCreateTemplate,
                        templateName: self.templateNameTextField.text,
                        templateDescription: "Погашение кредита")
                    
                    self.postProcess(loanPaymentModel: loanPaymentModel)
                }else{
                    viewToAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewToAccount, labelError: labelErrorToAccount)
                    amountTextField.stateIfEmpty(view:  viewAmountTextField, labelError: labelErrorAmount)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
                
            }else if isEditTemplate{
                if selectedDeposit != nil && amountTextField.text != "" && templateNameTextField.text != ""{
                    let amount = Double(self.amountTextField.text ?? "") ?? 0.0
                    let loanPaymentModel = LoanPaymentModel(
                        creditID: self.creditDetail.creditID,
                        tranchID: self.creditDetail.tranchID,
                        accountNo: self.selectedDeposit.accountNo,
                        currentAccountNo: self.creditDetail.currentAccountNo,
                        currentAccountBalance: self.creditDetail.currentAccountBalance,
                        paymentSumm: amount,
                        currencyID: self.creditDetail.currencyID,
                        paymentType: self.loanPaymentType,
                        programName: self.creditDetail.programName,
                        operationType: nil,
                        operationID: self.template.operationID,
                        isSchedule: false,
                        isTemplate: true,
                        templateName: self.templateNameTextField.text,
                        templateDescription: "Погашение кредита")
                    
                    self.postProcess(loanPaymentModel: loanPaymentModel)
                }else{
                    viewToAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewToAccount, labelError: labelErrorToAccount)
                    amountTextField.stateIfEmpty(view:  viewAmountTextField, labelError: labelErrorAmount)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }else{
                let amount = Double(self.amountTextField.text ?? "") ?? 0.0
                let loanPaymentModel = LoanPaymentModel(
                    creditID: self.creditDetail.creditID,
                    tranchID: self.creditDetail.tranchID,
                    accountNo: self.selectedDeposit.accountNo,
                    currentAccountNo: self.creditDetail.currentAccountNo,
                    currentAccountBalance: self.creditDetail.currentAccountBalance,
                    paymentSumm: amount,
                    currencyID: self.creditDetail.currencyID,
                    paymentType: self.loanPaymentType,
                    programName: self.creditDetail.programName,
                    operationType: nil,
                    operationID: nil,
                    isSchedule: nil,
                    isTemplate: nil,
                    templateName: nil,
                    templateDescription: nil)
                
                self.postProcess(loanPaymentModel: loanPaymentModel)
            }
        }else{
            viewToAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewToAccount, labelError: labelErrorToAccount)
            amountTextField.stateIfEmpty(view:  viewAmountTextField, labelError: labelErrorAmount)
            if isCreateTemplate{
                templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
            }
        }
        
    }
    
    
    //    обработка переходов
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toAccountsSegue"{
            let vc = segue.destination as! AccountsViewController
            
            if isCreateTemplate{
                for account in self.accounts{
                    account.items = account.items?.filter{$0.currencyID == currencyTemplate && $0.balance != 0 && $0.accountNo != creditNoTemplate }
                }
            }else{
                for account in self.accounts{
                    account.items = account.items?.filter{$0.currencyID == self.creditDetail.currencyID && $0.balance != 0 && $0.accountNo != creditDetail.currentAccountNo  }
                }
            }
            vc.accounts = accounts.filter{$0.accountType != 1}
            vc.selectedDeposit = selectedDeposit
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "from_account")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
    }
    //    установака текущего счета получателя
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
    }
    
    // выборка даты
    func showDatePicker(){
        
        var bankDate = ""
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        let calendar = Calendar.current
        if let date = UserDefaultsHelper.bankDate{
            bankDate = dateFormaterBankDay(date: date)
        }
        let monthsToAdd = 1
        let dayToAdd = 1
        
        var dateComponent = DateComponents()
        
        dateComponent.month = monthsToAdd
        dateComponent.day = dayToAdd
        if let date = calendar.date(byAdding: dateComponent, to: stringToDateFormatterFiltered(date: bankDate)){
            
            planDateToserver = formatterToServer.string(from: date)
        }
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        
        dateTextField.inputAccessoryView = toolbar
        dateTextField.inputView = datePicker
        dateTextField.text = dateFormaterForLoan(date: planDateToserver)
        
    }
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        formatter.locale = Locale(identifier: "language".localized)
        
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        dateTextField.text = formatter.string(from: datePicker.date)
        planDateToserver = formatterToServer.string(from: datePicker.date)
        
        
        self.view.endEditing(true)
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    //  сохранение платежа
    func  postProcess(loanPaymentModel: LoanPaymentModel){
        showLoading()
        managerApi
            . postProcess(LoanPaymentModel: loanPaymentModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](operationID) in
                    guard let `self` = self else { return }
                    if self.isCreateTemplate || self.isEditTemplate{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                        self.view.window?.rootViewController = vc
                        
                        let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                        let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                        let navigationController = UINavigationController(rootViewController: destinationController)
                        vc.pushFrontViewController(navigationController, animated: true)
                        
                    }else{
                        
                        let alertController = UIAlertController(title: nil, message: self.localizedText(key: "ConfirmTransferMessage"), preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: self.localizedText(key: "confirm"), style: .default) { (action) in
                            
                            let operationModel = OperationModel(operationID: Int(operationID) ?? 0,
                                                                code: "",
                                                                operationTypeID: InternetBankingOperationType.LoanPayment.rawValue)
                            
                            self.postConfirmPay(operationModel: operationModel)
                        }
                        let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .default, handler: nil)
                        alertController.addAction(actionCancel)
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    // получение расчета кредита
    func postCalculatePlanLoanPayment(сalculatePlanLoanPayment: CalculatePlanLoanPayment){
        showLoading()
        managerApi
            .postCalculatePlanLoanPayment(CalculatePlanLoanPayment: сalculatePlanLoanPayment)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](creditOperationDetail) in
                    guard let `self` = self else { return }
                    self.mainSummLabel.text = "\(String(format:"%.2f", creditOperationDetail.mainSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    self.percentSummLabel.text = "\(String(format:"%.2f", creditOperationDetail.percentSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    self.fineSummLabel.text = "\(String(format:"%.2f", creditOperationDetail.fineSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    self.totalSummLabel.text = "\(String(format:"%.2f", creditOperationDetail.totalSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    // Подтверждение операции
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                    self.view.window?.rootViewController = vc
                    
                    let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
                    let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
                    let navigationController = UINavigationController(rootViewController: destinationController)
               
                    vc.pushFrontViewController(navigationController, animated: true)
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    получение открытых кредитов для шаблона
    func getCreditsOpenForTemplate(){
        
        let depositObservable:Observable<[Accounts]> = managerApi
            .getAccounts(currencyID: Constants.NATIONAL_CURRENCY)
        
        let getDetailsCredits:Observable<DetailCreditsModel>  = managerApi
            .getDetailsCredits()
        
        showLoading()
        Observable
            .zip(depositObservable,
                 getDetailsCredits)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](accounts, credits) in
                    guard let `self` = self else { return }
                    self.credits.removeAll()
                    self.allCredits.append(contentsOf: credits.credits ?? [])
                    self.credits = self.allCredits.filter({$0.NextPaymentDate != nil})
                    self.accounts = accounts.filter({$0.accountType != 1})
                    self.pickerSettings()
                  
                    if self.isRepeatPay{
                        if self.template != nil{
                            self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.template.to ?? "", currency: self.template.currencyID ?? 0, accounts: self.accounts)
                            if self.selectedDeposit != nil{
                                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                            }
                            self.amountTextField.text = "\(self.template.amount ?? 0.0)"
                        }
                        if self.creditDetail != nil{
                            self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.creditDetail.accountNo ?? "", currency: self.creditDetail.currencyID ?? 0, accounts: self.accounts)
                            if self.selectedDeposit != nil{
                                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                            }
                            self.amountTextField.text = "\(self.creditDetail.paymentSumm ?? 0.0)"
                        }
                    }
                    if self.isEditTemplate{
                        self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.template.from ?? "", currency: self.template.currencyID ?? 0, accounts: self.accounts)
                        if self.selectedDeposit != nil{
                            self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                        }
                        self.amountTextField.text = "\(self.template.amount ?? 0.0)"
                        self.templateNameTextField.text = self.template.name
                    }
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //  получение деталей кредита
    func getLoansDetails(creditID: Int){
        showLoading()
        managerApi
            .getLoansDetails(creditID: creditID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](creditDetail) in
                    guard let `self` = self else { return }
                    self.creditDetail = creditDetail
                  
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
}

extension LoanPlanePaymentViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    // установка дефолтных значений pickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //  установка дефолтных значений периода
    func pickerSettings() {
        
        pickerView.delegate = self
        creditNameTextField.inputView = pickerView
        if !self.credits.isEmpty {
            pickerView.selectRow(0, inComponent: 0, animated: true)
            creditNameTextField.text = credits[0].ProductName ?? "" + ValueHelper.shared.getCurrentCurrency(currnecyID: credits[0].CurrencyID ?? 0)
            currencyTemplate = credits[0].CurrencyID ?? 0
            creditNoTemplate = credits[0].AccountNo ?? ""
            getLoansDetails(creditID: credits[0].CreditID ?? 0)
        }
        
    }
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return credits.count
    }
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return credits[row].ProductName ?? "" + ValueHelper.shared.getCurrentCurrency(currnecyID: credits[0].CurrencyID ?? 0)
        
    }
    // выбор полей
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        creditNameTextField.text = credits[row].ProductName ?? "" + ValueHelper.shared.getCurrentCurrency(currnecyID: credits[0].CurrencyID ?? 0)
        currencyTemplate = credits[row].CurrencyID ?? 0
        creditNoTemplate = credits[row].AccountNo ?? ""
        getLoansDetails(creditID: credits[0].CreditID ?? 0)

    }
    // кастомизация pickerView
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view as? UILabel { label = v }
        label.font = UIFont (name: "Helvetica Neue", size: 20)
        label.text =  credits[row].ProductName ?? "" + ValueHelper.shared.getCurrentCurrency(currnecyID: credits[0].CurrencyID ?? 0)
        label.textAlignment = .center
        
        return label
    }
    
    //    заполнение текущего счета
    func fullSelectedDeposit(selectedDeposit: Deposit){
        
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        payTo_accountLabel.text = fullAccountsText
        choiceCardTo_accountLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
        currencyLabel.text = "\(ValueHelper.shared.getCurrentCurrency(currnecyID: self.selectedDeposit.currencyID ?? 0))"
        viewToAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewToAccount, labelError: labelErrorToAccount)
    }
}
