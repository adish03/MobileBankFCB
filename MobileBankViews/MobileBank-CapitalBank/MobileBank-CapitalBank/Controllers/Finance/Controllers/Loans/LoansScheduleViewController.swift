//
//  LoansScheduleViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/22/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

//класс для планового погашения кредита
class LoansScheduleViewController: BaseViewController {
    
    var pickerView = UIPickerView()
    var navigationTitle = ""
    var loanId = 0
    var trancheId = 0
    var schedulePayments = [LoanScheduleModel]()
    var grafics = [LoanScheduleDeferredPercent]()
    var currencyID = 0
    var graficID = 1
    
    @IBOutlet weak var customerFullNameLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var graficTextField: UITextField!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        emptyStateStack.isHidden = true
        getLoansSchedule(loanId: loanId, trancheId: trancheId)
    }
    
    
    
    //получение депозитов
    func  getLoansSchedule(loanId: Int, trancheId: Int) {
        
        showLoading()
        managerApi
            .getLoansSchedule(loanId: loanId, trancheId: trancheId)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self]schedule in
                    guard let `self` = self else { return }
                    self.schedulePayments = schedule.loanScheduleModel ?? []
                    self.schedulePayments = self.schedulePayments.filter{ $0.graphicId == self.graficID }
                    self.customerFullNameLabel.text = schedule.customerFullName
                    self.rateLabel.text = "\(schedule.scheduleSettings?.rate ?? 0)"
                    self.grafics = schedule.loanScheduleDeferredPercent ?? []
                    self.pickerSettings()
                  
                    if self.schedulePayments.isEmpty{
                        self.tableView.isHidden = true
                        self.emptyStateStack.isHidden = false

                    }else{
                        self.tableView.isHidden = false
                        self.emptyStateStack.isHidden = true

                    }
                    self.tableView.reloadData()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
}
extension LoansScheduleViewController: UITableViewDataSource, UITableViewDelegate{
    
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return schedulePayments.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 180 //or whatever you need
    }
    
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! LoanScheduleTableViewCell
        
        let detail = schedulePayments[indexPath.row]
        
        cell.graphicIdLabel.text = "\(indexPath.row + 1)"
        cell.payDateLabel.text = dateFormaterBankDay(date: detail.payDate ?? "")
        cell.mainSummLabel.text = "\(String(format:"%.2f", detail.totalSumm ?? 0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.currencyID))"
        cell.percentsSummLabel.text = "\(String(format:"%.2f", detail.baseDeptToPay ?? 0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.currencyID))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
}
// выбор периода планировщика
extension LoansScheduleViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //  установка дефолтных значений периода
    func pickerSettings() {
        pickerView.delegate = self
        graficTextField.inputView = pickerView
        pickerView.selectRow(0, inComponent: 0, animated: true)
        
        graficTextField.text = "\(grafics[0].graphicId ?? 0)"
        //        percentTextField.text = daysList[0]
        
    }
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return grafics.count
    }
    
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return "\(grafics[row].graphicId ?? 0)"
        
    }
    // выбор полей
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        graficID = grafics[row].graphicId ?? 1
        getLoansSchedule(loanId: loanId, trancheId: trancheId)
        graficTextField.text = "\(grafics[row].graphicId ?? 0)"
    }
}
