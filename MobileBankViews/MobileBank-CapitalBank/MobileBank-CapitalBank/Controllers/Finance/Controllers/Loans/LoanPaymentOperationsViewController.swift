//
//  LoanPaymentOperationsViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/22/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

class LoanPaymentOperationsViewController: BaseViewController {
    
    @IBOutlet weak var currentAccountBalanceTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var paymentSummTextField: UITextField!
    
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var currencyAmountLabel: UILabel!
    @IBOutlet weak var currencyPaymentSummLabel: UILabel!
    
    @IBOutlet weak var viewSumm: UIView!
    
    
    @IBOutlet weak var mainSummLabel: UILabel!
    @IBOutlet weak var percentSummLabel: UILabel!
    @IBOutlet weak var fineSummLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var totalSummLabel: UILabel!
    
    @IBOutlet weak var viewAmountTextField: UIView!
    @IBOutlet weak var labelErrorAmount: UILabel!
    
    @IBOutlet weak var labelErrorTemplateName: UILabel!
    @IBOutlet weak var viewTemplateNameTextFields: UIView!
    
    var creditDetail: LoanPaymentModel!
    var operationCreditDetail: LoanPaymentModel!
    var accounts = [Accounts]()
    var datePicker = UIDatePicker()
    var planDateToserver = ""
    var loanPaymentType = 0
    var navigationTitle = ""
    var accountNo = ""
    
    var template: ContractModelTemplate!
    var isRepeatPay = false
    var isEditTemplate = false
    var isNewPay = false
    
    //template
    var isCreateTemplate = false
    @IBOutlet weak var TemplateDescriptionView: UIView!
    @IBOutlet weak var templateNameTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
      
        loanOperationtype(loanPaymentType: loanPaymentType)
        
        if isCreateTemplate || isEditTemplate{
            TemplateDescriptionView.isHidden = false
            if isEditTemplate{
//                self.currentAccountBalanceTextField.text = "\(String(format:"%.2f",creditDetail.currentAccountBalance ?? 0.0))"
            }
        }else{
            TemplateDescriptionView.isHidden = true
        }
        
        if isRepeatPay{
             self.currentAccountBalanceTextField.text = "\(String(format:"%.2f",creditDetail.currentAccountBalance ?? 0.0))"
        }
        
        if operationCreditDetail != nil{
            amountTextField.text = "\(String(format:"%.2f",operationCreditDetail.paymentSumm ?? 0.0))"
            accountNo = operationCreditDetail.accountNo ?? ""
        }
        
        if template != nil{
            accountNo = template.from ?? ""
            templateNameTextField.text = template.name
            amountTextField.text = "\(String(format:"%.2f",template.amount ?? 0.0))"
            currencyLabel.text = ValueHelper.shared.getCurrentCurrency(currnecyID: template.currencyID ?? 0)
            currencyAmountLabel.text = ValueHelper.shared.getCurrentCurrency(currnecyID: template.currencyID ?? 0)
            currencyPaymentSummLabel.text = ValueHelper.shared.getCurrentCurrency(currnecyID: template.currencyID ?? 0)
        }
        if creditDetail != nil{
            currentAccountBalanceTextField.text = "\(creditDetail.currentAccountBalance ?? 0.0)"
            currencyLabel.text = ValueHelper.shared.getCurrentCurrency(currnecyID: creditDetail.currencyID ?? 0)
            currencyAmountLabel.text = ValueHelper.shared.getCurrentCurrency(currnecyID: creditDetail.currencyID ?? 0)
            currencyPaymentSummLabel.text = ValueHelper.shared.getCurrentCurrency(currnecyID: creditDetail.currencyID ?? 0)
            
        }
      
        if loanPaymentType == InternetBankingLoanPaymentType.FullEarlyPayment.rawValue{
            amountTextField.text = "\(String(format:"%.2f",creditDetail.currentAccountBalance ?? 0.0))"
        }
        
        amountTextField.addTarget(self, action: #selector(textFieldEditingAmountTextField(_:)), for: UIControl.Event.editingDidEnd)
        templateNameTextField.addTarget(self, action: #selector(textFieldEditingEndtemplateName(_:)), for: UIControl.Event.editingDidEnd)
        
    }
    // Проверка поля "Название шаблона" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndtemplateName(_ sender: Any) {
        templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
    }
    
    // Проверка поля "Сумма" на наличие данных, после потери фокуса
    @objc func textFieldEditingAmountTextField(_ sender: Any) {
        paymentSummTextField.text = amountTextField.text
    }
    
    // кнопка расчета
    @IBAction func calculateButton(_ sender: Any) {
        
        let amount = Double(amountTextField.text ?? "") ?? 0.0
        if amount > 0.0{
        let calculateLoanPaymentModel = CalculateLoanPaymentModel(paymentSumm: amount,
                                                                 creditID: creditDetail.creditID,
                                                                 tranchID: creditDetail.tranchID,
                                                                 paymentType: loanPaymentType)
        
         postCalculateLoanPayment(calculateLoanPaymentModel: calculateLoanPaymentModel)
        }else{
            showAlertController(localizedText(key: "insufficient_loan_repayment"))
        }
    }
    // кнопка  сохранения
    @IBAction func saveButton(_ sender: Any) {
        
        let amount = Double(amountTextField.text ?? "") ?? 0.0
        if amount > 0.0{
            if isCreateTemplate{
                if amount > 0.0 && templateNameTextField.text != ""{
                    let amount = Double(self.amountTextField.text ?? "") ?? 0.0
                    let loanPaymentModel = LoanPaymentModel(
                        creditID: self.creditDetail.creditID,
                        tranchID: self.creditDetail.tranchID,
                        accountNo: self.operationCreditDetail.currentAccountNo,
                        currentAccountNo: self.creditDetail.currentAccountNo,
                        currentAccountBalance: self.creditDetail.currentAccountBalance,
                        paymentSumm: amount,
                        currencyID: self.creditDetail.currencyID,
                        paymentType: self.loanPaymentType,
                        programName: self.creditDetail.programName,
                        operationType: nil,
                        operationID: nil,
                        isSchedule: nil,
                        isTemplate: true,
                        templateName: self.templateNameTextField.text,
                        templateDescription: "Погашение кредита")
                    
                    self.postProcess(loanPaymentModel: loanPaymentModel)
                }else{
                    amountTextField.stateIfEmpty(view:  viewAmountTextField, labelError: labelErrorAmount)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
                
            }else if isEditTemplate{
                if amount > 0.0 && templateNameTextField.text != "" {
                    let amount = Double(self.amountTextField.text ?? "") ?? 0.0
                    let loanPaymentModel = LoanPaymentModel(
                        creditID: self.creditDetail.creditID,
                        tranchID: self.creditDetail.tranchID,
                        accountNo: self.accountNo,
                        currentAccountNo: self.creditDetail.currentAccountNo,
                        currentAccountBalance: self.creditDetail.currentAccountBalance,
                        paymentSumm: amount,
                        currencyID: self.creditDetail.currencyID,
                        paymentType: self.loanPaymentType,
                        programName: self.creditDetail.programName,
                        operationType: nil,
                        operationID: self.template.operationID,
                        isSchedule: false,
                        isTemplate: true,
                        templateName: self.templateNameTextField.text,
                        templateDescription: "Погашение кредита")
                    
                    self.postProcess(loanPaymentModel: loanPaymentModel)
                    
                }
            }else{
                
                let amount = Double(self.amountTextField.text ?? "") ?? 0.0
                let loanPaymentModel = LoanPaymentModel(
                    creditID: self.creditDetail.creditID,
                    tranchID: self.creditDetail.tranchID,
                    accountNo: self.accountNo,
                    currentAccountNo: self.creditDetail.currentAccountNo,
                    currentAccountBalance: self.creditDetail.currentAccountBalance,
                    paymentSumm: amount,
                    currencyID: self.creditDetail.currencyID,
                    paymentType: self.loanPaymentType,
                    programName: self.creditDetail.programName,
                    operationType: nil,
                    operationID: nil,
                    isSchedule: nil,
                    isTemplate: nil,
                    templateName: nil,
                    templateDescription: nil)
                
                self.postProcess(loanPaymentModel: loanPaymentModel)
            }
        }else{
            amountTextField.stateIfEmpty(view:  viewAmountTextField, labelError: labelErrorAmount)
        }
    }
    
    func loanOperationtype(loanPaymentType: Int) {
        
        switch loanPaymentType {
        case InternetBankingLoanPaymentType.PartialEarlyPayment.rawValue:
            currentAccountBalanceTextField.isEnabled = false
            paymentSummTextField.isEnabled = false
            
        case InternetBankingLoanPaymentType.PartialEarlyPaymentMainDeptOnly.rawValue:
            currentAccountBalanceTextField.isEnabled = false
            paymentSummTextField.isEnabled = false
            
        case InternetBankingLoanPaymentType.FullEarlyPayment.rawValue:
            currentAccountBalanceTextField.isEnabled = false
            amountTextField.isEnabled = false
            viewSumm.isHidden = true
        default:
            currentAccountBalanceTextField.isEnabled = false
            paymentSummTextField.isEnabled = false
        }
        
        
    }
    //  сохранение платежа
    func  postProcess(loanPaymentModel: LoanPaymentModel){
        showLoading()
        managerApi
            .postProcess(LoanPaymentModel: loanPaymentModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operationID) in
                    guard let `self` = self else { return }
                    if self.isCreateTemplate{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                        self.view.window?.rootViewController = vc
                        
                        let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                        let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                        let navigationController = UINavigationController(rootViewController: destinationController)
                        vc.pushFrontViewController(navigationController, animated: true)
                        
                    }else{
                        
                        let alertController = UIAlertController(title: nil, message: self.localizedText(key: "ConfirmTransferMessage"), preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: self.localizedText(key: "confirm"), style: .default) { (action) in
                            
                            let operationModel = OperationModel(operationID: Int(operationID) ?? 0,
                                                                code: "",
                                                                operationTypeID: InternetBankingOperationType.LoanPayment.rawValue)
                            
                            self.postConfirmPay(operationModel: operationModel)
                        }
                        let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .default, handler: nil)
                        alertController.addAction(actionCancel)
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                    }
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }

    
    // получение расчета кредита
    func postCalculateLoanPayment(calculateLoanPaymentModel: CalculateLoanPaymentModel){
        showLoading()
        managerApi
            .postCalculateLoanPayment(CalculateLoanPaymentModel: calculateLoanPaymentModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self ](creditOperationDetail) in
                    guard let `self` = self else { return }
                    self.mainSummLabel.text = "\(String(format:"%.2f", creditOperationDetail.mainSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    self.percentSummLabel.text = "\(String(format:"%.2f", creditOperationDetail.percentSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    self.fineSummLabel.text = "\(String(format:"%.2f", creditOperationDetail.fineSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    self.totalSummLabel.text = "\(String(format:"%.2f", creditOperationDetail.totalSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    self.commissionLabel.text = "\(String(format:"%.2f", creditOperationDetail.comissionSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.creditDetail.currencyID ?? 0))"
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    // Подтверждение операции
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                    self.view.window?.rootViewController = vc
                    
                    let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
                    let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
                    let navigationController = UINavigationController(rootViewController: destinationController)
                    vc.pushFrontViewController(navigationController, animated: true)
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
