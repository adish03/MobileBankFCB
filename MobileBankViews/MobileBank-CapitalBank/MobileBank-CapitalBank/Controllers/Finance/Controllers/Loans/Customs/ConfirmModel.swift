//
//  ConfirmModel.swift
//  MobileBank-CapitalBank-Release
//
//  Created by Atai Ismailov on 05/08/2021.
//  Copyright © 2021 Spalmalo. All rights reserved.
//

import Foundation
struct ConfirmModel {
    var depositName: String
    var currency: String
    var openDate: String
    var closeDate: String
    var termDay: String
    var percenteageBet: String
    var copitalization: String
    var agreement: String
}
