//
//  File.swift
//  MobileBank-CapitalBank
//
//  Created by Atai Ismailov on 04/08/2021.
//  Copyright © 2021 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import SnapKit
import UIKit

class OpenDepositConfirmAlert {
    
    struct Constants {
        static let backgroundAlphaTo: CGFloat = 0.6
        
    }
    
    private  var targetView: OpenDepositViewController? = nil
    
    // MARK: VIEWS
    private let alertView: UIView = {
        let alert = UIView()
        alert.backgroundColor = .white
        alert.layer.masksToBounds = true
        alert.layer.cornerRadius = 12
    
        return alert
    }()
    
    lazy var contentViewSize = CGSize(width: self.alertView.frame.width, height: self.alertView.frame.height + 200)
      
      lazy var scrollView: UIScrollView = {
          let view = UIScrollView(frame: alertView.bounds)
          view.backgroundColor = .white
          view.contentSize = contentViewSize
          view.frame = self.alertView.bounds
          view.showsVerticalScrollIndicator = false
          return view
      }()
    
    private let backgroundView: UIView = {
        let backgroundView = UIView()
        backgroundView.backgroundColor = .black
        backgroundView.alpha = 0.0
        return backgroundView
    }()

    lazy var mainViewHolder: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        
        return view
    }()
    
    lazy var areYouSureLable: UILabel = {
        let view = UILabel()
        view.text = "Вы уверены?"
        view.textAlignment = .left
        view.font = UIFont.boldSystemFont(ofSize: 18)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var depositName: UILabel = {
        let view = UILabel()
        view.text = "Название депозита: "
        view.textAlignment = .left
        view.font = UIFont.boldSystemFont(ofSize: 12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var depositNameValue: UILabel = {
        let view = UILabel()
        view.textAlignment = .justified
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = view.font.withSize(12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var currencyName: UILabel = {
        let view = UILabel()
        view.text = "Валюта: "
        view.textAlignment = .left
        view.font = UIFont.boldSystemFont(ofSize: 12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var currencyValue: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.text = "default........"
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = view.font.withSize(12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var openDate: UILabel = {
        let view = UILabel()
        view.text = "Дата открытия: "
        view.textAlignment = .left
        view.font = UIFont.boldSystemFont(ofSize: 12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var openDateValue: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.text = "default........"
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = view.font.withSize(12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var closeDate: UILabel = {
        let view = UILabel()
        view.text = "Дата закрытия: "
        view.textAlignment = .left
        view.font = UIFont.boldSystemFont(ofSize: 12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var closeDateValue: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.text = "default........"
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = view.font.withSize(12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var dayTerms: UILabel = {
        let view = UILabel()
        view.text = "Срок в днях: "
        view.textAlignment = .left
        view.font = UIFont.boldSystemFont(ofSize: 12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var dayTermsValue: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.text = "234 дн"
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = view.font.withSize(12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var betPercentage: UILabel = {
        let view = UILabel()
        view.text = "Процентная ставка: "
        view.textAlignment = .left
        view.font = UIFont.boldSystemFont(ofSize: 12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var betPercentageValue: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.text = "6.0%"
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = view.font.withSize(12)
        view.textColor = UIColor.black
        return view
    }()

    lazy var copitalizationProj: UILabel = {
        let view = UILabel()
        view.text = "Капитализация процентов: "
        view.textAlignment = .left
        view.font = UIFont.boldSystemFont(ofSize: 12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var copitalizationProjValue: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = view.font.withSize(12)
        view.textColor = UIColor.black
        return view
    }()
    
    lazy var agreementInfo: UILabel = {
        let view = UILabel()
        view.textAlignment = .left
        view.lineBreakMode = .byWordWrapping
        view.numberOfLines = 0
        view.font = view.font.withSize(12)
        view.textColor = UIColor.black
        return view
    }()
    
    
    lazy var confirmButton: UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.systemPink
        view.setTitle("Подтвердить", for: .normal)
        view.titleLabel?.textColor = UIColor.white
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(functionConfirme), for: .touchUpInside)
        return view
    }()
    
    
    lazy var dismissButton: UIButton = {
        let view = UIButton()
        view.backgroundColor = UIColor.lightGray
        view.setTitle("Отмена", for: .normal)
        view.titleLabel?.textColor = UIColor.black
        view.layer.masksToBounds = true
        view.layer.cornerRadius = 8
        view.addTarget(self, action: #selector(dismissAlert), for: .touchUpInside)
        return view
    }()
    
    // MARK: FUNCTIONS
    func showAlert(model: ConfirmModel, on viewController: OpenDepositViewController) {
        
        guard let targetView = viewController.view else { return }
        backgroundView.frame = targetView.bounds
        self.targetView = viewController
        setupView(targetView: targetView)
        makeConstraints(targetView: targetView)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.backgroundView.alpha = Constants.backgroundAlphaTo
        })
        
        setValueAccordingly(model: model)
        
    }
    
    func setValueAccordingly(model: ConfirmModel) {
        depositNameValue.text =  model.depositName
        currencyValue.text = model.currency
        openDateValue.text = model.openDate
        closeDateValue.text = model.closeDate
        dayTermsValue.text = model.termDay
        betPercentageValue.text = model.percenteageBet
        copitalizationProjValue.text = model.copitalization
        agreementInfo.text = model.agreement
    }
    
    @objc func dismissAlert() {
        removeFromParent()
    }
    
    @objc func functionConfirme() {
        confirmButton.backgroundColor = .lightGray
        self.targetView?.postOpenDeposit()
        removeFromParent()
        confirmButton.backgroundColor = .systemPink
        
    }

    func removeFromParent() {
        backgroundView.removeFromSuperview()
        alertView.removeFromSuperview()
    }
    
    // MARK: INIT Views
    func setupView(targetView: UIView) {
        
        targetView.addSubview(backgroundView)
        targetView.addSubview(alertView)
        alertView.addSubview(scrollView)
        scrollView.addSubview(mainViewHolder)
        mainViewHolder.addSubview(areYouSureLable)
        mainViewHolder.addSubview(depositName)
        mainViewHolder.addSubview(depositNameValue)
        mainViewHolder.addSubview(currencyName)
        mainViewHolder.addSubview(currencyValue)
        mainViewHolder.addSubview(openDate)
        mainViewHolder.addSubview(openDateValue)
        mainViewHolder.addSubview(closeDate)
        mainViewHolder.addSubview(closeDateValue)
        mainViewHolder.addSubview(dayTerms)
        mainViewHolder.addSubview(dayTermsValue)
        mainViewHolder.addSubview(betPercentage)
        mainViewHolder.addSubview(betPercentageValue)
        mainViewHolder.addSubview(copitalizationProj)
        mainViewHolder.addSubview(copitalizationProjValue)
        mainViewHolder.addSubview(agreementInfo)
        mainViewHolder.addSubview(confirmButton)
        mainViewHolder.addSubview(dismissButton)
    }

    func makeConstraints(targetView: UIView) {
        
        alertView.snp.makeConstraints { make in
            make.top.equalTo(targetView.safeArea.top).offset(10)
            make.bottom.equalToSuperview().offset(-60)
            make.left.equalToSuperview().offset(30)
            make.trailing.equalToSuperview().offset(-30)
        }
        
        scrollView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().offset(6)
            make.trailing.equalToSuperview().offset(-6)
            make.bottom.equalToSuperview().offset(-6)
        }
        
        mainViewHolder.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.width.equalToSuperview()
            make.height.equalToSuperview()

        }
        
        areYouSureLable.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(10)
            make.height.equalTo(50)
            make.width.equalToSuperview()
        }

        depositName.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalTo(areYouSureLable.snp.bottom).offset(10)
            make.height.equalTo(30)
            make.width.equalTo(130)
        }

        depositNameValue.snp.makeConstraints { make in
            make.leading.equalTo(depositName.snp.trailing)
            make.trailing.equalToSuperview()
            make.top.equalTo(depositName.snp.top)
            make.height.equalTo(35)
        }

        currencyName.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalTo(depositName.snp.bottom).offset(5)
            make.height.equalTo(30)
            make.width.equalTo(130)
        }

        currencyValue.snp.makeConstraints { make in
            make.leading.equalTo(currencyName.snp.trailing)
            make.trailing.equalToSuperview()
            make.top.equalTo(currencyName.snp.top)
            make.height.equalTo(30)
        }
        
        openDate.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalTo(currencyName.snp.bottom).offset(5)
            make.height.equalTo(30)
            make.width.equalTo(130)
        }

        openDateValue.snp.makeConstraints { make in
            make.leading.equalTo(openDate.snp.trailing)
            make.trailing.equalToSuperview()
            make.top.equalTo(openDate.snp.top)
            make.height.equalTo(30)
        }
        
        closeDate.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalTo(openDate.snp.bottom).offset(5)
            make.height.equalTo(30)
            make.width.equalTo(130)
        }

        closeDateValue.snp.makeConstraints { make in
            make.leading.equalTo(closeDate.snp.trailing)
            make.trailing.equalToSuperview()
            make.top.equalTo(closeDate.snp.top)
            make.height.equalTo(30)
        }
        
        dayTerms.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalTo(closeDate.snp.bottom).offset(5)
            make.height.equalTo(30)
            make.width.equalTo(130)
        }

        dayTermsValue.snp.makeConstraints { make in
            make.leading.equalTo(dayTerms.snp.trailing)
            make.trailing.equalToSuperview()
            make.top.equalTo(dayTerms.snp.top)
            make.height.equalTo(30)
        }
        
        betPercentage.snp.makeConstraints { make in
            make.leading.equalToSuperview()
            make.top.equalTo(dayTerms.snp.bottom).offset(5)
            make.height.equalTo(30)
            make.width.equalTo(130)
        }

        betPercentageValue.snp.makeConstraints { make in
            make.leading.equalTo(betPercentage.snp.trailing)
            make.trailing.equalToSuperview()
            make.top.equalTo(betPercentage.snp.top)
            make.height.equalTo(30)
        }
        
        copitalizationProj.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(betPercentage.snp.bottom).offset(5)
            make.height.equalTo(30)
        }

        copitalizationProjValue.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(copitalizationProj.snp.bottom)
        }
        
        agreementInfo.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.top.equalTo(copitalizationProjValue.snp.bottom)
        }
        
        dismissButton.snp.makeConstraints { make in
            make.bottom.trailing.equalToSuperview().offset(-10)
            make.width.equalTo(100)
            make.height.equalTo(40)
        }
        
        confirmButton.snp.makeConstraints { make in
            make.trailing.equalTo(dismissButton.snp.leading).offset(-20)
            make.bottom.equalTo(dismissButton.snp.bottom)
            make.height.equalTo(40)
            make.width.equalTo(150)
        }
    }
}
