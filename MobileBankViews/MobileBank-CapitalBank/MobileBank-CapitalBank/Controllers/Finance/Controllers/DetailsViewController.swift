//
//  DetailsViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/14/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import PDFReader

//   отобажение деталей текущего счета
class DetailsViewController: BaseViewController, FilterDetailDelegate {
    
    @IBOutlet weak var nameAccountLabel: UILabel!
    @IBOutlet weak var noAccountLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var currentBalance: UILabel!
    
    @IBOutlet weak var accountView: UIView!
    
    @IBOutlet weak var viewExtrDetail: UIView!
    @IBOutlet weak var viewExtraDetailCredit: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var filterImage: UIImageView!
    
    @IBOutlet weak var statusLabel: UILabel!
    // extraDetail
    @IBOutlet weak var inBalanceLabel: UILabel!
    @IBOutlet weak var outBalance: UILabel!
    
    @IBOutlet weak var openDateLabel: UILabel!
    @IBOutlet weak var closeDateLabel: UILabel!
    @IBOutlet weak var endDateLabel: UILabel!
    @IBOutlet weak var lastDateLabel: UILabel!
    @IBOutlet weak var closeDateTitleLabel: UILabel!
    @IBOutlet weak var lastOperationTitleLabel: UILabel!
    @IBOutlet weak var cardImage: UIImageView!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var cardLimitInfoField: UILabel!
    
    @IBOutlet weak var currencyImage: UIImageView!
    
    
    // extraDetailCredit
    
    @IBOutlet weak var productNameLAbel: UILabel!
    
    @IBOutlet weak var statusNameLabel: UILabel!
    @IBOutlet weak var approvedRateLabel: UILabel!
    @IBOutlet weak var creditIDLabel: UILabel!
    @IBOutlet weak var nextPaymentDateLabel: UILabel!
    @IBOutlet weak var overdueDaysLabel: UILabel!
    @IBOutlet weak var statusDateLabel: UILabel!
    @IBOutlet weak var approvedPeriodLabel: UILabel!
    @IBOutlet weak var creditFinesLabel: UILabel!
    @IBOutlet weak var accBalanceLabel: UILabel!
    @IBOutlet weak var effectiveRateLabel: UILabel!
    
    @IBOutlet weak var percentLabel: UILabel!
    @IBOutlet weak var approvedSumm: UILabel!
    @IBOutlet weak var alertSheetButton: UIBarButtonItem!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    @IBAction func cardInfo(_ sender: Any){
        self.performSegue(withIdentifier: "toCardInfoSegue", sender: self)
    }
    
    var accounts = [Accounts]()
    var currentAccount: DetailAccountsModel!
    var currentDeposit: DetailDepositsModel!
    var currentCard: DetailCardsModel!
    var currentCredit: CreditModel!
    var operationType = 0
    var detailsOfAccount = [AccountOperation]()
    var accountNo = ""
    var currencyID = 0
    var amount = ""
    var navigationTitle = ""
    var dateFrom = "2019-04-10"
    var dateTo = ""
    var isFiltered = false
    var fetchingMore = false
    var totalItems = 0
    var pageSize = 0
    var page = 1
    var isNeedOnLine = "false"
    var creditDetail: LoanPaymentModel!
    var isGoToGrafic = false
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //       проверка фильтрации
        
        if isFiltered{
            detailsOfAccount.removeAll()
            filterImage.image = UIImage(named: "noFilter")
            getDetails(accountNo: accountNo,
                       currencyID: currencyID,
                       startDate: dateFrom,
                       endDate: dateTo,
                       page: page,
                       pageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                       totalItems: 100)
        }else{
            filterImage.image = UIImage(named: "filterDetail")
            if !isGoToGrafic{
                getDetails(accountNo: accountNo,
                           currencyID: currencyID,
                           startDate: dateFrom,
                           endDate: dateTo,
                           page: page,
                           pageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                           totalItems: 100)
                isGoToGrafic = false
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        print((Int(((getCurrentTimeStap() ?? 0) - ((getDateTimeStap(date: currentAccount.openDate ?? "") ?? 0))) / 86400)))
        
       

        
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        emptyStateStack.isHidden = true

        if let bankDate = UserDefaultsHelper.bankDate{
            dateTo = dateFormaterBankDay(date: bankDate)
        }
        
        if(dateTo == ""){
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "dd.MM.yyyy"
            dateTo = dateFormaterBankDay(date: "\(formatter.string(from: date))")
        }
        
        let calendar = Calendar.current
        if let date = calendar.date(byAdding: .day, value: -7, to: stringToDateFormatterFiltered(date: dateTo)){
            
            dateFrom = dateToStringFormatter(date: date)
        }
        getCurrentSettings(typeID: operationType)
        
        if currentCard != nil{
            getCheckIsUsedOnlineCard()
        }
        if currentCredit != nil{
            accountNo = currentCredit.AccountNo ?? ""
            currencyID = currentCredit.CurrencyID ?? 0
        }

        if self.currentAccount != nil {
            switch self.currentAccount.DepositAccountStatusID {
            case 1:
                self.statusLabel.text = "Активный"
                break
            case 2:
                self.statusLabel.text = "Закрыт"
                break
            case 3:
                self.statusLabel.text = "Заблокирован"
                break
            case 4:
                self.statusLabel.text = "Дормант"
                break
            case 5:
                self.statusLabel.text = "Открыт удалённо"
                break
            case .none:
                break
            case .some(_):
                break
            }
//            self.currentAccount.DepositAccountStatusID == 2 || self.currentAccount.DepositAccountStatusID == 3 ||
            if self.currentAccount.DepositAccountStatusID == 4 {
                
                showAlertController("Вы не использовали данный счет более 6 месяцев. Вам необходимо активировать его для проведения операций!")
            }
        }
    }
    
    // проверка при скроллинге до конца страницы
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height{
            if !fetchingMore{
                fetchingMore = true
                
                
                if isFiltered{
                    if !(totalItems <= detailsOfAccount.count){
                        page += 1
                        filterImage.image = UIImage(named: "noFilter")
                        getDetails(accountNo: accountNo,
                                   currencyID: currencyID,
                                   startDate: dateFrom,
                                   endDate: dateTo,
                                   page: page,
                                   pageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                   totalItems: 100)
                    }
                }else{
                    if !(totalItems <= detailsOfAccount.count){
                        page += 1
                        filterImage.image = UIImage(named: "filterDetail")
                        getDetails(accountNo: accountNo,
                                   currencyID: currencyID,
                                   startDate: dateFrom,
                                   endDate: dateCurrent(),
                                   page: page,
                                   pageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                   totalItems: 100)
                    }
                }
            }
        }
    }
    //    кнопка фильтр
    @IBAction func filterButton(_ sender: Any) {
        performSegue(withIdentifier: "toFilterSegue", sender: nil)
    }
    
    
    //    кнопка показа дополнительной информации
    @IBAction func detailButton(_ sender: Any) {
        
        if self.operationType == InternetBankingAccountType.Credit.rawValue{
            if self.viewExtraDetailCredit.isHidden{
                self.viewExtraDetailCredit.isHidden = false
                self.isShowCreditExtraDetail(isShow: true)
                self.tableView.reloadData()
            }else{
                self.viewExtraDetailCredit.isHidden = true
                self.isShowCreditExtraDetail(isShow: false)
                self.tableView.reloadData()
            }
        }
        else{
            if self.viewExtrDetail.isHidden{
                self.viewExtrDetail.isHidden = false
                self.isShowExtraDetail(isShow: true)
                self.tableView.reloadData()
                
            }else{
                self.viewExtrDetail.isHidden = true
                self.isShowExtraDetail(isShow: false)
                self.tableView.reloadData()
            }
        }
    }
    
    // MARK: - extra detail arrow
    
    //    показ дополнительной информации
    func isShowExtraDetail(isShow: Bool){
        
        if isShow{
            var frame = self.viewHeader.frame
            frame.size.height = self.viewHeader.frame.height + self.viewExtrDetail.frame.height
            self.viewHeader.frame = frame
            self.arrowImage.image = UIImage(named: "chevron-top")
            if self.detailsOfAccount.isEmpty{
                self.emptyStateStack.isHidden = true
            }
        }else{
            var frame = self.viewHeader.frame
            frame.size.height = self.viewHeader.frame.height - self.viewExtrDetail.frame.height
            self.viewHeader.frame = frame
            self.arrowImage.image = UIImage(named: "chevron-bottom")
            if self.detailsOfAccount.isEmpty{
                self.emptyStateStack.isHidden = false
            }
        }
    }
    //    показ дополнительной информации по кредитам
    func isShowCreditExtraDetail(isShow: Bool){
        if isShow{
            var frame = viewHeader.frame
            frame.size.height = viewHeader.frame.height + viewExtraDetailCredit.frame.height
            viewHeader.frame = frame
            arrowImage.image = UIImage(named: "chevron-top")
            if self.detailsOfAccount.isEmpty{
                self.emptyStateStack.isHidden = true
            }
        }else{
            var frame = viewHeader.frame
            frame.size.height = viewHeader.frame.height - viewExtraDetailCredit.frame.height
            viewHeader.frame = frame
            arrowImage.image = UIImage(named: "chevron-bottom")
            if self.detailsOfAccount.isEmpty{
                self.emptyStateStack.isHidden = true
            }
        }
    }
    
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFilterSegue"{
            let vc = segue.destination as! FilteredDetailsAccount
            
            vc.dateFrom = self.dateFrom
            vc.dateTo = self.dateTo
            vc.isNeedData = true
            vc.delegate = self
        }
        if segue.identifier == "toCardInfoSegue"{
            let vc = segue.destination as! CardInfoModalViewController
            let accountModel = AccountModel(accountNo: self.accountNo, currencyID: self.currencyID)
            vc.accountModel = accountModel
            vc.currencySymbol = currentCard.currencySymbol ?? ""
        }
        
        if segue.identifier == "toInternalSegue"{
            let vc = segue.destination as! TransactionViewController
            vc.operationTypeID = InternetBankingOperationType.InternalOperation.rawValue
            vc.currencyIDcurrent = currentAccount.currencyID ?? Constants.NATIONAL_CURRENCY
            vc.accountNoCurrent = currentAccount.accountNo ?? ""
            vc.navigationTitle = localizedText(key: "Internal_operation")
        }
        
        if segue.identifier == "toInternalBetweenAccountSegue"{
            let vc = segue.destination as! TransactionViewController
            vc.isWithdrawAccount = true
            vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
            vc.currencyIDcurrent = currentAccount.currencyID ?? Constants.NATIONAL_CURRENCY
            vc.accountNoCurrent = currentAccount.accountNo ?? ""
            vc.navigationTitle = self.localizedText(key: "transfer_between_accounts")
        }
        
        if segue.identifier == "toFullInternalCustomerSegue"{
            let vc = segue.destination as! TransactionViewController
            if currentCard != nil{
                if currentCard.balance != 0.0{
                    vc.isFullAccount = true
                    vc.isFullZeroAccount = false
                }else{
                    vc.isFullAccount = false
                    vc.isFullZeroAccount = true
                }
                vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
                vc.currencyIDcurrent = currentCard.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentCard.accountNo ?? ""
            }
            if currentDeposit != nil{
                vc.isFullAccount = true
                vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
                vc.currencyIDcurrent = currentDeposit.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentDeposit.accountNo ?? ""
            }
            
            
            vc.navigationTitle = localizedText(key: "Resources_Internal_Customer_Transaction")
            
        }
        
        if segue.identifier == "toWithdrawInternalCustomerSegue"{
            let vc = segue.destination as! TransactionViewController
            if currentCard != nil{
                vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
                vc.currencyIDcurrent = currentCard.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentCard.accountNo ?? ""
                vc.navigationTitle = localizedText(key: "Resources_Internal_Customer_Transaction")
                vc.isWithdrawAccount = true
            }
            if currentDeposit != nil{
                vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
                vc.currencyIDcurrent = currentDeposit.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentDeposit.accountNo ?? ""
                vc.navigationTitle = localizedText(key: "Resources_Internal_Customer_Transaction")
                vc.isWithdrawAccount = true
            }
            
            
        }
        
        if segue.identifier == "toConversionSegue"{
            let vc = segue.destination as! CurrencyViewController
            vc.currencyIDcurrent = currentAccount.currencyID ?? Constants.NATIONAL_CURRENCY
            vc.accountNoCurrent = currentAccount.accountNo ?? ""
            vc.navigationTitle = localizedText(key: "currency_conversion")
            vc.isNewPayFromFianance = true
            
        }
        
        if segue.identifier == "toCliringSegue"{
            let vc = segue.destination as! ClearingGrossViewController
            if currentAccount != nil{
                vc.currencyIDcurrent = currentAccount.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentAccount.accountNo ?? ""
            }
            if currentCard != nil{
                vc.currencyIDcurrent = currentCard.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentCard.accountNo ?? ""
            }
            vc.operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
            vc.navigationTitle = localizedText(key: "clearing_payment")
            
        }
        if segue.identifier == "toGrossSegue"{
            let vc = segue.destination as! ClearingGrossViewController
            if currentAccount != nil{
                vc.currencyIDcurrent = currentAccount.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentAccount.accountNo ?? ""
            }
            if currentCard != nil{
                vc.currencyIDcurrent = currentCard.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentCard.accountNo ?? ""
            }
            vc.operationTypeID = InternetBankingOperationType.GrossTransfer.rawValue
            vc.navigationTitle = localizedText(key: "gross_payment")
            
        }
        
        if segue.identifier == "toSwiftSegue"{
            let vc = segue.destination as! SWIFTViewController
            vc.currencyIDcurrent = currentAccount.currencyID ?? Constants.NATIONAL_CURRENCY
            vc.accountNoCurrent = currentAccount.accountNo ?? ""
            vc.navigationTitle = localizedText(key: "swift_payment")
        }
        if segue.identifier == "toUtilitiesSegue"{
            let vc = segue.destination as! CategoriesViewController
            if currentAccount != nil{
                vc.currencyIDcurrent = currentAccount.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentAccount.accountNo ?? ""
            }
            if currentCard != nil{
                vc.currencyIDcurrent = currentCard.currencyID ?? Constants.NATIONAL_CURRENCY
                vc.accountNoCurrent = currentCard.accountNo ?? ""
            }
            vc.isNewPayFromFianance = true
        }
        //LoanSegue
        if segue.identifier == "toScheduleLoanSegue"{
            let vc = segue.destination as! LoansScheduleViewController
            vc.navigationTitle = localizedText(key: "credit_schedules")
            vc.trancheId = self.creditDetail.tranchID ?? 0
            vc.loanId = self.creditDetail.creditID ?? 0
            vc.currencyID = self.creditDetail.currencyID ?? 0
            isGoToGrafic = true
        }
        if segue.identifier == "toLoanPlanePaymentSegue"{
            let vc = segue.destination as! LoanPlanePaymentViewController
            vc.creditDetail = creditDetail
            vc.accounts = self.accounts
            vc.navigationTitle = localizedText(key: "planned_payment")
            vc.loanPaymentType = InternetBankingLoanPaymentType.OnSchedule.rawValue
            isGoToGrafic = true
        }
        if segue.identifier == "toLoanPartialEarlyRepaymentPaymentsOperation"{
            let vc = segue.destination as! LoanPaymentOperationsViewController
            vc.navigationTitle = localizedText(key: "partial_early_repayment")
            vc.loanPaymentType = InternetBankingLoanPaymentType.PartialEarlyPayment.rawValue
            vc.creditDetail = creditDetail
            vc.accounts = self.accounts
        }
        if segue.identifier == "toLoanPartialEarlyRepaymentBasicPaymentsOperation"{
            let vc = segue.destination as! LoanPaymentOperationsViewController
            vc.navigationTitle = localizedText(key: "partial_early_repayment_of_principal")
            vc.loanPaymentType = InternetBankingLoanPaymentType.PartialEarlyPaymentMainDeptOnly.rawValue
            vc.creditDetail = creditDetail
            vc.accounts = self.accounts
        }
        if segue.identifier == "toFullEarlyRepaymentLoanPaymentsOperation"{
            let vc = segue.destination as! LoanPaymentOperationsViewController
            vc.navigationTitle = localizedText(key: "full_early_repayment")
            vc.loanPaymentType = InternetBankingLoanPaymentType.FullEarlyPayment.rawValue
            vc.creditDetail = creditDetail
            vc.accounts = self.accounts
        }
    }
    
    //    установка фильтра
    func property(dateFrom: String?, dateTo: String?, isFilter: Bool?) {
        self.dateFrom = dateFrom ?? "2019-04-10"
        self.dateTo = dateFormaterForFilterReverse(date: dateTo ?? "")
        self.isFiltered = isFilter ?? false
        self.page = 1
    }
    //    сброс фильтра
    func resetFilter(isFilter: Bool?) {
        self.isFiltered = isFilter ?? false
        self.page = 1
        detailsOfAccount.removeAll()
    }
    
    private var dateBank: String = ""
    
    //    FAB меню
    @IBAction func AlertSheetControl(_ sender: Any) {
        showLoading()
        
        managerApi
            .getBankDate()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[self] dateBank in
                    self.dateBank = dateBank
                    
                    self.hideLoading()
                    
                    self.setupShellAlert()
            },
                onError: {[self] error in
                    self.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    private func setupShellAlert() {
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let actionCancel  = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)

        if currentAccount != nil {
            if self.currentAccount.DepositAccountStatusID == 2 || self.currentAccount.DepositAccountStatusID == 3 || self.currentAccount.DepositAccountStatusID == 4 {
                self.currentBalance.text = "Статус счета"
                
                let actionInternal = UIAlertAction(title: "Активировать счет", style: .default) { [self] (action) in
                    let alertController = UIAlertController(title: "Активировать счет", message: "Вы действительно хотите активировать счет?", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
                    alertController.addAction(UIAlertAction(title: "Подтвердить", style: .default, handler: { [self] _ in
                        showLoading()
                        managerApi
                            .activeDemart(accountNo: accountNo, currencyID: currencyID)
                            .updateTokenIfNeeded()
                            .subscribe(
                                onNext: {[weak self] details in
                                    self?.navigationController?.popViewController(animated: true)
                                    
                                    self?.hideLoading()

                            },
                                onError: {[weak self] error in
                                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                                    self?.hideLoading()
                            })
                            .disposed(by: disposeBag)
                    }))
                    present(alertController, animated: true, completion: nil)
                    
                    
                }
                
                alertSheet.addAction(actionInternal)
                alertSheet.addAction(actionCancel)
                
                present(alertSheet, animated: true, completion: nil)
                return
            }
        }
        
        let actionExportDoc = UIAlertAction(title: localizedText(key: "AccountStatement"), style: .default) { (action) in
           
            self.postExportStatement(
                accountNo: self.accountNo,
                currencyID: self.currencyID,
                startDate: self.dateFormaterToServer(date: self.dateFrom),
                endDate:self.dateFormaterToServer(date: self.dateTo),
                toExcel: true)
        }
        
        let actionExportMiniStatement = UIAlertAction(title: localizedText(key: "GetMiniStatement"), style: .default) { (action) in
            self.postExportMiniStatement(
                accountNo: self.accountNo,
                currencyID: self.currencyID)
        }
    
        
        let actionInternal = UIAlertAction(title: self.localizedText(key: "internal_bank_transfer"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toInternalSegue", sender: self)
        }
        let actionBetweenAccounts = UIAlertAction(title: self.localizedText(key: "transfer_between_accounts"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toInternalBetweenAccountSegue", sender: self)
        }
        let actionConversion = UIAlertAction(title: self.localizedText(key: "Resources_Deposits_DepositAccounts_Conversion"), style: .default){ (action) in
            self.performSegue(withIdentifier: "toConversionSegue", sender: self)
        }
        let actionUtilities = UIAlertAction(title: self.localizedText(key: "utilities_payment"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toUtilitiesSegue", sender: self)
        }
        let actionCliring = UIAlertAction(title: self.localizedText(key: "create_clearing"), style: .default){ (action) in
            self.performSegue(withIdentifier: "toCliringSegue", sender: self)
        }
        let actionGross = UIAlertAction(title: self.localizedText(key: "create_gross"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toGrossSegue", sender: self)
        }
        let actionSwift = UIAlertAction(title: self.localizedText(key: "create_swift"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toSwiftSegue", sender: self)
        }
        let actionFull = UIAlertAction(title: self.localizedText(key: "replenishment"), style: .default){ (action) in
            self.performSegue(withIdentifier: "toFullInternalCustomerSegue", sender: self)
        }
        let actionWithdraw = UIAlertAction(title: self.localizedText(key: "withdrawal"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toWithdrawInternalCustomerSegue", sender: self)
        }
        let actionInfo = UIAlertAction(title: self.localizedText(key: "card_information"), style: .default){ (action) in
            self.performSegue(withIdentifier: "toCardInfoSegue", sender: self)
        }
        let actionStopList = UIAlertAction(title: self.localizedText(key: "acc_card_to_soft_stop_list"), style: .default) { (action) in
            
            let alertController = UIAlertController(title: nil, message: self.localizedText(key: "acc_card_to_soft_stop_list"), preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                let accountModel = AccountModel(accountNo: self.accountNo, currencyID: self.currencyID)
                self.postAddToStopList(AccountModel: accountModel)
            }
            let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .default, handler: nil)
            alertController.addAction(action)
            alertController.addAction(actionCancel)
            self.present(alertController, animated: true, completion: nil)
            
        }
        let actionPinReset = UIAlertAction(title: self.localizedText(key: "reset_counter_of_entered_pin_codes"), style: .default) { (action) in
            
            let alertController = UIAlertController(title: nil, message: self.localizedText(key: "do_oyu_really_want_card_rebase_pin"), preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                let accountModel = AccountModel(accountNo: self.accountNo, currencyID: self.currencyID)
                self.postRebasePin(AccountModel: accountModel)
            }
            let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .default, handler: nil)
            alertController.addAction(action)
            alertController.addAction(actionCancel)
            self.present(alertController, animated: true, completion: nil)
            
        }
        //  Депозиты
        let actionCheckDeposit = UIAlertAction(title: self.localizedText(key: "statement_of_interest_account"), style: .default) { (action) in
            self.detailsOfAccount.removeAll()
            self.getDetails(accountNo: self.currentDeposit.percentAccountNo ?? "",
                           currencyID: self.currencyID,
                           startDate: self.dateFrom,
                           endDate: self.dateTo,
                           page: self.page,
                           pageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                           totalItems: 100)
    
        }
        let actionFullDeposit = UIAlertAction(title: self.localizedText(key: "replenish_deposit"), style: .default){ (action) in
            self.performSegue(withIdentifier: "toFullInternalCustomerSegue", sender: self)
        }
        let actionCloseDeposit = UIAlertAction(title: self.localizedText(key: "close_deposit"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CloseDepositViewController") as! CloseDepositViewController
            vc.accounts = self.accounts
            vc.currentDeposit = self.currentDeposit
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        //  Кредиты
        let actionSchedule = UIAlertAction(title: self.localizedText(key: "graph"), style: .default) { (action) in
                        self.performSegue(withIdentifier: "toScheduleLoanSegue", sender: self)
        }
        let actionPlannedRepayment = UIAlertAction(title: self.localizedText(key: "planned_payment"), style: .default){ (action) in
                        self.performSegue(withIdentifier: "toLoanPlanePaymentSegue", sender: self)
        }
        
        
        
        let actionPartialEarlyRepayment = UIAlertAction(title: self.localizedText(key: "partial_early_repayment"), style: .default) { (action) in
                        self.performSegue(withIdentifier: "toLoanPartialEarlyRepaymentPaymentsOperation", sender: self)
        }
        let actionPartialEarlyRepaymentBasic = UIAlertAction(title: self.localizedText(key: "partial_early_repayment_of_principal"), style: .default) { (action) in
                        self.performSegue(withIdentifier: "toLoanPartialEarlyRepaymentBasicPaymentsOperation", sender: self)
        }
        let actionFullEarlyRepayment = UIAlertAction(title: self.localizedText(key: "full_early_repayment"), style: .default) { (action) in
                        self.performSegue(withIdentifier: "toFullEarlyRepaymentLoanPaymentsOperation", sender: self)
        }
        
        let actionPercentDoc = UIAlertAction(title: self.localizedText(key: "percent_account_statement"), style: .default) { (action) in
            self.detailsOfAccount.removeAll()
            self.getDetails(accountNo: self.currentCredit.PercentAccountNo ?? "",
                            currencyID: self.currencyID,
                            startDate: self.dateFrom,
                            endDate: self.dateTo,
                            page: self.page,
                            pageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                            totalItems: 100)

        }
        
        let actionLoanStatment = UIAlertAction(title: self.localizedText(key: "loan_account_statement"), style: .default) { (action) in
            self.detailsOfAccount.removeAll()
            self.getDetails(accountNo: self.currentCredit.LoanAccountNo ?? "",
                            currencyID: self.currencyID,
                            startDate: self.dateFrom,
                            endDate: self.dateTo,
                            page: self.page,
                            pageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                            totalItems: 100)
        }
                
        //        отрисовка действий FAB
        switch operationType {
        case InternetBankingAccountType.Current.rawValue:
            //MARK: - That's whats heppening here
            if currentAccount.currencyID == Constants.NATIONAL_CURRENCY{
                
                alertSheet.addAction(actionExportDoc)
               
                if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed"){
                     alertSheet.addAction(actionInternal)
                     alertSheet.addAction(actionBetweenAccounts)
                }
                if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsAddAllowed"){
                     alertSheet.addAction(actionConversion)
                }
                if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsAddAllowed"){
                   alertSheet.addAction(actionUtilities)
                }
                if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionCliring)
                    alertSheet.addAction(actionGross)
                }
                alertSheet.addAction(actionCancel)
                
            }else{
                
                alertSheet.addAction(actionExportDoc)
                
                if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionInternal)
                    alertSheet.addAction(actionBetweenAccounts)
                }
                if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionConversion)
                }
                if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionSwift)
                }
                alertSheet.addAction(actionCancel)
            }
            
        case InternetBankingAccountType.Deposit.rawValue:
           
            alertSheet.addAction(actionExportDoc)
            alertSheet.addAction(actionCheckDeposit)
            
            if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.DepositAccountSecurityOperations.IsAddAllowed"){
                
                alertSheet.addAction(actionFullDeposit)
            }
            if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.DepositAccountSecurityOperations.IsCloseDepositAllowed"){
               alertSheet.addAction(actionCloseDeposit)
            }
            
            alertSheet.addAction(actionCancel)
            
        case InternetBankingAccountType.Card.rawValue:
            
            if currentCard.balance != 0.0{
                alertSheet.addAction(actionExportDoc)
                alertSheet.addAction(actionExportMiniStatement)
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsReplenishmentAllowed"){
                    alertSheet.addAction(actionFull)
                }
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsWithdrawalAllowed"){
                     alertSheet.addAction(actionWithdraw)
                }
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsCheckActualBalanceAllowed"){
                    alertSheet.addAction(actionInfo)
                }
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsAddToStopListAllowed"){
                    alertSheet.addAction(actionStopList)
                }
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsRebasePinAllowed"){
                    alertSheet.addAction(actionPinReset)
                }
                if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionUtilities)
                }
                if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionCliring)
                    alertSheet.addAction(actionGross)
                }
                alertSheet.addAction(actionCancel)
                
            }else{
                alertSheet.addAction(actionExportDoc)
                alertSheet.addAction(actionExportMiniStatement)
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsReplenishmentAllowed"){
                    alertSheet.addAction(actionFull)
                }
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsCheckActualBalanceAllowed"){
                    alertSheet.addAction(actionInfo)
                }
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsAddToStopListAllowed"){
                    alertSheet.addAction(actionStopList)
                }
                if AllowHelper.shared.cardsAllowOpertaion(nameOperation: "Cards.CardsSecurityOperations.IsRebasePinAllowed"){
                    alertSheet.addAction(actionPinReset)
                }
                alertSheet.addAction(actionCancel)
            }
            
        case InternetBankingAccountType.Credit.rawValue:
            if currentCredit.StatusID != nil{
                //MARK: - credit payment
                if AllowHelper.shared.loanAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsViewScheduleAllowed"){
                  alertSheet.addAction(actionSchedule)
                }
                if AllowHelper.shared.loanAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsOnSchedulePaymentAllowed"){
                     alertSheet.addAction(actionPlannedRepayment)
                }
                if AllowHelper.shared.loanAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsPartialEarlyPaymentAllowed"){
                   alertSheet.addAction(actionPartialEarlyRepayment)
                }
                if AllowHelper.shared.loanAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsPartialEarlyPaymentMainDeptOnlyAllowed"){
                     alertSheet.addAction(actionPartialEarlyRepaymentBasic)
                }
                if AllowHelper.shared.loanAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsFullEarlyPaymentAllowed"){
                     alertSheet.addAction(actionFullEarlyRepayment)
                }
                alertSheet.addAction(actionExportDoc)
                alertSheet.addAction(actionPercentDoc)
                alertSheet.addAction(actionLoanStatment)
                alertSheet.addAction(actionCancel)
            }else{
                alertSheet.addAction(actionCancel)
            }
        default:
            print("")
        }
        
        //Saves docx to device
        let requisites = UIAlertAction(title: "Реквизиты", style: .default) { [self] (action) in
            showLoading()
            managerApi
                .getAccountRequisites(accountNo: accountNo,
                                      currencyID: currencyID)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self] details in

                        dump(details)

                        if let stringPDF = details.reportDocuments?[0].fileBody{
                            let name = details.reportDocuments?[0].fileName
                            if let decodeData = Data(base64Encoded: stringPDF, options: .ignoreUnknownCharacters) {
                                let actionButtonImage = UIImage(named: "Share")

                                var filesToShare = [Any]()

                                // Add the path of the file to the Array
                                filesToShare.append(ExcelFileHelper.shared.saveFileDirectory(docxFile: decodeData, name ?? "document.docx"))

                                // Make the activityViewContoller which shows the share-view
                                let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)

                                // Show the share-view
                                self?.present(activityViewController, animated: true, completion: nil)
                            }
                        }

                        self?.hideLoading()
                    },
                    onError: {[weak self] error in
                        self?.showAlertController(ApiHelper.shared.errorHelper(error: error))

                        self?.hideLoading()
                    })
                .disposed(by: disposeBag)
        }
        
        if currentAccount != nil {
            dateBank = String(dateBank.dropLast())
            dateBank = String(dateBank.dropFirst())
            
            let currentTime = (getDateTimeStap(date: dateBank)  ?? 0.0)
            //let openDate = (getDateTimeStap(date: currentAccount.openDate ?? "") ?? 0.0)
            let openDate = (getDateTimeStap(date: currentAccount?.openDate ?? "") ?? 0.0)
            
            let dayTimeMilis = 86400.0
            let day = Int((currentTime - openDate) / dayTimeMilis)
            
            if day == 0 {
                let actionInternal = UIAlertAction(title: "Удалить счет", style: .default) { [self] (action) in
                    let alertController = UIAlertController(title: "Удалить счет", message: "Вы действительно хотите удалить счет?", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "Отмена", style: .default, handler: nil))
                    alertController.addAction(UIAlertAction(title: "Подтвердить", style: .default, handler: { [self] _ in
                        showLoading()
                        managerApi
                            .deleteDemart(accountNo: accountNo, currencyID: currencyID)
                            .updateTokenIfNeeded()
                            .subscribe(
                                onNext: {[weak self] details in
                                    self?.navigationController?.popViewController(animated: true)
                                    
                                    self?.hideLoading()
                                    
                                },
                                onError: {[weak self] error in
                                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                                    self?.hideLoading()
                                })
                            .disposed(by: disposeBag)
                    }))
                    present(alertController, animated: true, completion: nil)
                }
                
                alertSheet.addAction(actionInternal)
                
            }
        }
        
        alertSheet.addAction(requisites)
        present(alertSheet, animated: true, completion: nil)
    }
    //    заполнение данных карты
    //MARK: - Detail Field Setting
    func getCurrentSettings(typeID: Int)  {
        switch typeID {
        case InternetBankingAccountType.Current.rawValue:
            accountNo = currentAccount.accountNo ?? ""
            currencyID = currentAccount.currencyID ?? 0
            amount = "\(String(format:"%.2f",currentAccount.balance ?? 0.0)) \(currentAccount.currencySymbol ?? "")"
            openDateLabel.text = dateFormaterBankDay(date: currentAccount.openDate ?? "")
            closeDateLabel.text = dateFormaterBankDay(date: currentAccount.closeDate ?? "")
            endDateLabel.text = dateFormaterBankDay(date: currentAccount.endDate ?? "")
            lastDateLabel.text = dateFormaterBankDay(date: currentAccount.lastOperationDate ?? "")
            customImageAccount(view: viewImage, image: cardImage)
            getImageFromUrl(imageString: currentAccount.imageName ?? "", imageview: cardImage)

            
        case InternetBankingAccountType.Deposit.rawValue:
            closeDateTitleLabel.text = localizedText(key: "percent_rate")
            lastOperationTitleLabel.text = localizedText(key: "interest")
            accountNo = currentDeposit.accountNo ?? ""
            currencyID = currentDeposit.currencyID ?? 0
            amount = "\(String(format:"%.2f",currentDeposit.balance ?? 0.0)) \(currentDeposit.currencySymbol ?? "")"
            openDateLabel.text = dateFormaterBankDay(date: currentDeposit.openDate ?? "")
            closeDateLabel.text = "\(currentDeposit.interestRate ?? 0)"
            endDateLabel.text = dateFormaterBankDay(date: currentDeposit.endDate ?? "")
            lastDateLabel.text = "\(String(format:"%.2f",currentDeposit.percentsSumm ?? 0.0))"
            customImageAccount(view: viewImage, image: cardImage)
            getImageFromUrl(imageString: currentDeposit.imageName ?? "", imageview: cardImage)
            
        case InternetBankingAccountType.Card.rawValue:
            accountNo = currentCard.accountNo ?? ""
            currencyID = currentCard.currencyID ?? 0
            amount = "\(String(format:"%.2f",currentCard.balance ?? 0.0)) \(currentCard.currencySymbol ?? "")"
            openDateLabel.text = dateFormaterBankDay(date: currentCard.openDate ?? "")
            closeDateLabel.text = dateFormaterBankDay(date: currentCard.closeDate ?? "Hе определено")
            endDateLabel.text = dateFormaterBankDay(date: currentCard.endDate ?? "")
            lastDateLabel.text = dateFormaterBankDay(date: currentCard.lastOperationDate ?? "")
            customImageAccount(view: viewImage, image: cardImage)
            getImageFromUrl(imageString: currentCard.imageName ?? "", imageview: cardImage)
            
            cardLimitInfoField.text = "\(String(format:"%.2f",currentCard.availableLimit ?? 0.0)) \(currentCard.currencySymbol ?? "")"
            
        case InternetBankingAccountType.Credit.rawValue:
            accountNo = currentCredit.AccountNo ?? ""
            getLoansDetails(creditID: currentCredit.CreditID ?? 0)
            currencyID = currentCredit.CurrencyID ?? 0
          
        default:
            print("")
        }
    }
//    функция поделиться с ..
    func shareData() {
        let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let customPlistURL = docsBaseURL.appendingPathComponent("excelFile.xls")
       
        var filesToShare = [Any]()
        filesToShare.append(customPlistURL)
        
        let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
        
        self.present(activityViewController, animated: true, completion: nil)
        
    }
}

extension DetailsViewController: UITableViewDataSource, UITableViewDelegate{
    
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return detailsOfAccount.count
        
    }
    
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailTableViewCell
        
        let detail = detailsOfAccount[indexPath.row]
        cell.documentNoLabel.text = detail.documentNo ?? ""
        cell.transactionDate.text = dateFormaterBankDay(date: detail.transactionDate ?? "2000-01-01T00:00:00")
        if detail.dtSumm != nil {
            cell.dtSummLabel.text = "- \(String(format:"%.2f", detail.dtSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: currencyID)) "
        } else {
            cell.dtSummLabel.text = "+ \(String(format:"%.2f", detail.ctSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: currencyID)) "
        }
        cell.operationDateLabel.text = dateFormaterOperationDay(date: detail.operationDate ?? "2000-01-01T00:00:00")
        cell.commentLabel.text = detail.comment ?? ""
        
        return cell
    }
}

extension DetailsViewController{
    //получение депозитов
    func  getDetails(accountNo: String, currencyID: Int, startDate: String, endDate: String, page: Int, pageSize: Int, totalItems: Int) {
        
        showLoading()
        managerApi
            .getInfoDetails(accountNo: accountNo,
                            currencyID: currencyID,
                            startDate: startDate,
                            endDate: endDate,
                            page: page,
                            pageSize: pageSize,
                            totalItems: totalItems)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] details in
                    guard let `self` = self else { return }
                    self.detailsOfAccount.append(contentsOf: details.operations ?? [])
                    self.totalItems = details.totalOperationsCount ?? 0
                    self.fetchingMore = false
                    
                    if self.currentAccount?.name != nil {
                        self.nameAccountLabel.text = self.currentAccount.name
                    } else {
                        self.nameAccountLabel.text = details.accountName ?? ""
                    }
                    
                    self.inBalanceLabel.text = "\(String(format:"%.2f",details.inBalance ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID:self.currencyID))"
                    self.outBalance.text = "\(String(format:"%.2f",details.outBalance ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID:self.currencyID))"
                    
                    self.amountLabel.text = self.amount
                    self.noAccountLabel.text = self.accountNo
                    //MARK: - Enabling and Desebling more button
                    if self.detailsOfAccount.isEmpty{
                        self.emptyStateStack.isHidden = false
                        self.alertSheetButton.isEnabled = true
                        if self.isFiltered{
                            self.emptyStateLabel.text = self.localizedText(key: "nothing_found_change_search_criteria")
                            self.emptyImage.image = UIImage(named: "empty_search_icn")
                        }
                    }else{
                        self.emptyStateStack.isHidden = true
                        self.alertSheetButton.isEnabled = true
                        self.emptyImage.isHidden = true
                    }
                    
                    self.tableView.reloadData()
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    
    //    Занесение карты в мягкий стоп-лист
    func postAddToStopList(AccountModel: AccountModel){
        showLoading()
        managerApi
            .postAddToStopList(AccountModel: AccountModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    guard let `self` = self else { return }
                    self.showAlertController(response)
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //   Сброс счетчика введеных ПИН-кодов
    func postRebasePin(AccountModel: AccountModel){
        showLoading()
        managerApi
            .postRebasePin(AccountModel: AccountModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    guard let `self` = self else { return }
                    self.showAlertController(response)
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //   Проверка используется ли онлайн-интеграция карт
    func getCheckIsUsedOnlineCard(){
        showLoading()
        managerApi
            .getCheckIsUsedOnlineCard()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (isNeedOnLine) in
                    guard let `self` = self else { return }
                    self.isNeedOnLine = isNeedOnLine
                    print(self.isNeedOnLine )
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //  получение деталей кредита
    func getLoansDetails(creditID: Int){
        showLoading()
        managerApi
            .getLoansDetails(creditID: creditID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (creditDetail) in
                    guard let `self` = self else { return }
                    
                    self.creditDetail = creditDetail
                    self.accountNo = creditDetail.currentAccountNo ?? ""
                    self.currencyID = creditDetail.currencyID ?? 0
                    self.amount = "\(String(format:"%.2f",creditDetail.loan?.loanSumm ?? 0.0)) \(self.currentCredit.ApprovedCurrency ?? "")"
                    self.approvedSumm.text = "\(String(format:"%.2f",creditDetail.loan?.loanSumm ?? 0.0)) \(self.currentCredit.ApprovedCurrency ?? "")"
                    self.productNameLAbel.text = creditDetail.programName
                    self.statusNameLabel.text = self.currentCredit.StatusName
                    self.approvedRateLabel.text = "\(creditDetail.loan?.interestRate ?? 0)"
                    self.creditIDLabel.text = "\(creditDetail.loan?.agreementNo ?? "")"
                    self.nextPaymentDateLabel.text = self.dateFormaterBankDay(date: creditDetail.loan?.nextPaymentDate ?? "")
                    self.overdueDaysLabel.text = "\(creditDetail.loan?.overdueDays ?? 0)"
                    self.statusDateLabel.text = self.dateFormaterBankDay(date: creditDetail.loan?.statusDate ?? "")
                    self.approvedPeriodLabel.text = "\(creditDetail.loan?.period ?? 0) \(self.localizedText(key: "month"))."
                    self.creditFinesLabel.text = "\(String(format:"%.2f",creditDetail.loan?.finesSumm ?? 0)) \(self.currentCredit.ApprovedCurrency ?? "")"
                    self.accBalanceLabel.text = "\(String(format:"%.2f",creditDetail.loan?.loanBalance ?? 0)) \(self.currentCredit.ApprovedCurrency ?? "")"
                    self.effectiveRateLabel.text = "\(self.currentCredit.EffectiveRate ?? 0) \(self.currentCredit.ApprovedCurrency ?? "")"
                    self.percentLabel.text = "\(String(format:"%.2f",creditDetail.loan?.percentsToPay ?? 0)) \(self.currentCredit.ApprovedCurrency ?? "")"
                    
                    self.amountLabel.text = "\(String(format:"%.2f",creditDetail.currentAccountBalance ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID:self.currencyID))"

                    
                    self.noAccountLabel.text = self.accountNo
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
//    Экспорт выписки
    func postExportStatement(accountNo: String,
                             currencyID: Int,
                             startDate: String,
                             endDate: String,
                             toExcel: Bool){
        showLoading()
        managerApi
            .getExportStatement(accountNo: accountNo, currencyID: currencyID, startDate: startDate, endDate: endDate, toExcel: toExcel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](excelFile) in
                    guard let `self` = self else { return }
                    
                        var filesToShare = [Any]()
                        filesToShare.append(ExcelFileHelper.shared.saveFileInDocDirectory(excelFile: excelFile, accountNo))
                        let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
                        self.present(activityViewController, animated: true, completion: nil)
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func postExportMiniStatement(accountNo: String,
                             currencyID: Int){
        showLoading()
        managerApi
            .getExportMiniStatement(accountNo: accountNo, currencyID: currencyID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](excelFile) in
                    guard let `self` = self else { return }
                    if  let stringPDF = excelFile.reportDocuments?[0].fileBody{
                        let name = excelFile.reportDocuments?[0].fileName
                        let path = self.shareFile(fileBody: stringPDF, fileName: name!)
                        let fileManager = FileManager.default
                        if fileManager.fileExists(atPath: path){
                            let document = NSData(contentsOfFile: path)
                            
                            ExcelFileHelper.shared.saveFileInDocDirectory(excelFile: document! as Data)
                            self.shareData()
                            self.hideLoading()
                        }
                        else {
                            print("document was not found")
                        }
                        self.hideLoading()
                    }
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    запрос на просмотр заявки
    func getViewRequest(creditID: Int){
        showLoading()
        managerApi
            .getViewRequest(creditID: creditID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](response) in
                    guard let `self` = self else { return }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func shareFile(fileBody: String, fileName: String) -> String{
        let fileManager = FileManager.default
        
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(fileName)
        
        let doc = NSData(base64Encoded: fileBody, options: NSData.Base64DecodingOptions(rawValue: 0))
        
        fileManager.createFile(atPath: path as String, contents: doc as Data?, attributes: nil)
        return path
   }
}
