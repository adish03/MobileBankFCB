//
//  MainViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/6/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import SafariServices

class MainViewController: BaseViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var bannerView: UIView!
    @IBOutlet weak var bannerDivider: UIView!
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    
    @IBOutlet weak var mainBakground: UIImageView!
    @IBOutlet weak var viewButtonBox: UIView!
    @IBOutlet weak var viewButtonWallet: UIView!
    @IBOutlet weak var viewButtonHistory: UIView!
    @IBOutlet weak var viewButtonTransaction: UIView!
    @IBOutlet weak var viewButtonTemplate: UIView!
    @IBOutlet weak var viewButtonConverssion: UIView!
    
    @IBOutlet weak var viewImageFinance: UIImageView!
    @IBOutlet weak var viewImageUtilities: UIImageView!
    @IBOutlet weak var viewImageHistory: UIImageView!
    @IBOutlet weak var viewImageTransaction: UIImageView!
    @IBOutlet weak var viewImageQr: UIImageView!
    @IBOutlet weak var viewImageConverssion: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var financeLabel: UILabel!
    @IBOutlet weak var utilitiesLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var transactionsLabel: UILabel!
    @IBOutlet weak var templatesLabel: UILabel!
    @IBOutlet weak var conversionLabel: UILabel!
    
    @IBOutlet weak var financeButton: UIButton!
    @IBOutlet weak var utilitiesButton: UIButton!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var transactionsButton: UIButton!
    @IBOutlet weak var templatesButton: UIButton!
    @IBOutlet weak var conversionButton: UIButton!
    
    @IBOutlet weak var historyHeaderView: UIView!
    @IBOutlet weak var historyTableView: UIView!
    
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    
    var categories: Categories!
    var modifyDateCategories = ""
    var modifyDateUtilities = ""
    
    
    var user : User?
    
    var historyOperationArray = [ContractModel]()
    var currencies = [Currency]()
    struct Objects {
        var sectionName : String!
        var sectionObjects : [ContractModel]!
    }
    var historyOperationArrayNew = [ContractModel]()
    var pageCount = 0
    var page = 1
    var banners = [Banner]()
    var payments = [(key: String, value: [ContractModel])]()
    var dateFrom = ""
    var dateTo = ""
    var operationType = ""
    var operationState = ""
    private var allowedOperations = [String]()
    
    lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = pageCount
        pc.hidesForSinglePage = true
        pc.currentPageIndicatorTintColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        pc.pageIndicatorTintColor = #colorLiteral(red: 0.86022228, green: 0.8639231324, blue: 0.8765630126, alpha: 1)
        pc.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        return pc
    }()
    var pageControlDotCount = 0
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        CustomStatusBar(color: UIColor(hexFromString: Constants.MAIN_COLOR))
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        menu.tintColor = .white
        CustomStatusBar(color: UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(scrollView.contentOffset.y / 60))
        navigationBar_Gradients()

        customNavBar(offset: 15.0)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        tableView.reloadData()
                
    }
    
    // sets resizedImage to navBar, sets gradient to main background
    func navigationBar_Gradients() {
        if (self.navigationController?.navigationBar) != nil {
            mainBakground.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            
//            let image =  self.resizeImage(image: UIImage(named: "LogoLaunch")!, targetSize: CGSize(width: 130.0, height: 130.0))
            let imageView = UIImageView(image: UIImage(named: "LogoLaunch")!)
            imageView.contentMode = UIView.ContentMode.scaleAspectFit
            imageView.snp.makeConstraints { make in
                make.height.equalTo(30)
            }
            navigationItem.titleView = imageView
        }
    }
    // converts CAGradientLayer as an image
    func getImageFrom(gradientLayer:CAGradientLayer) -> UIImage? {
        var gradientImage:UIImage?
        UIGraphicsBeginImageContext(gradientLayer.frame.size)
        if let context = UIGraphicsGetCurrentContext() {
            gradientLayer.render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
    
    let refreshControl = UIRefreshControl()
    
    @objc func refreshStart(_ sender: AnyObject) {
        refreshControl.endRefreshing()
        showLoading()
        
        Timer.scheduledTimer(withTimeInterval: 0.05, repeats: false) { [self] timer in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
            self.view.window?.rootViewController = vc
            
            let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
            let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            let navigationController = UINavigationController(rootViewController: destinationController)
            vc.pushFrontViewController(navigationController, animated: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.tintColor = .white
//        refreshControl.bounds = CGRect(x: refreshControl.bounds.origin.x,
//                                       y: -50,
//                                       width: refreshControl.bounds.size.width,
//                                       height: 100)
        refreshControl.addTarget(self, action: #selector(self.refreshStart(_:)), for: .valueChanged)

        scrollView.refreshControl = refreshControl

        allowView()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        scrollView.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        UserDefaults.standard.set(true, forKey: "isNeedDismissPinCintroller")
        UserDefaults.standard.set(false, forKey: "isNeedChangeUser")
        
        viewButtonBox.roundView(view: viewButtonBox, imageView: viewImageFinance, imageName: "strongbox1")
        emptyStateStack.isHidden = true
        scrollView.delegate = self
        scrollView.addBackground(imageName: "main_background", contentMode: .scaleToFill)
        getBanners()
        
        if UserDefaultsHelper.bikCodes != nil{
            print("DataStorage is full")
        }else{
            ApiHelper.shared.getSwiftData()
        }
        
        managerApi.checkURL(url: "https://185.67.254.141:4444/OnlineBank.WebAPI/api/Utilities/Categories", method: .get)
        
        tableView.reloadData()
                        
        if  menu != nil{
            menu.target = self.revealViewController()
            menu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        if let categories = UserDefaultsHelper.categories{
            self.categories = categories
        }
        
        if let date = UserDefaultsHelper.modifyUtlitiesDate{
            self.modifyDateUtilities = date
        }
        if let date = UserDefaultsHelper.modifyCategoriesDate{
            self.modifyDateCategories = date
        }
        
//        qrCode.target = self
//        qrCode.action = #selector(qrCodeTapped)
//
        MainViewController.navigationControllerTest = navigationController
    }
    
//    @objc func qrCodeTapped() {
//        performSegue(withIdentifier: "", sender: nil)
//    }
    
    // начало анимации  Navigation Bar
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if let viewControllers = self.navigationController?.viewControllers {
            viewControllers.forEach({ controller in
                if controller == self {
                    setNavBarHidden(scrollView.contentOffset.y < -5)
                } else{
                    customNavBar(offset: 3.0)
                }
            })
        }
    }
    //анимация  Navigation Bar
    func setNavBarHidden(_ isHidden:Bool){
        let offset = scrollView.contentOffset.y - 5
        
        if !isHidden{
            customNavBar(offset: offset)
        }
    }
    // resizing the image in from the navigationController
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage? {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio, height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: .zero, size: newSize)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    public static var navigationControllerTest: UINavigationController?
    
    // кастомизация  NavigationBar, плавное скрытие и появление
    func customNavBar(offset: CGFloat){
//        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
//        navigationController?.navigationBar.shadowImage = UIImage()
//        navigationController?.navigationBar.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(offset)
        
        if #available(iOS 13.0, *) {
            CustomStatusBar(color: UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(offset))
        }else{
            UIApplication.shared.statusBarView.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(offset)
        }
    }
    // Нотификации для выбора раздела меню Финансы
    @IBAction func buttonHelp(_ sender: UIButton) {
    
    }
    
    @IBAction func buttonInfoApp(_ sender: UIButton) {
        
    }
    
    @IBAction func buttonToMyFinance(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuFinance"), object: nil)
    }
    // Нотификации для выбора раздела меню История
    @IBAction func buttonToHistoryOperations(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuHistoryOperation"), object: nil)
    }
    // Нотификации для выбора раздела меню Перевод
    @IBAction func buttonToTransactions(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuTransactions"), object: nil)
    }
    // Нотификации для выбора раздела меню Оплата
    @IBAction func buttonToUtilities(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuUtilities"), object: nil)
    }
    // Нотификации для выбора раздела меню Конвертация
    @IBAction func buttonToCoverssion(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuConverssion"), object: nil)
    }
    // Нотификации для выбора раздела меню Шаблоны
    @IBAction func buttonToQR(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "menuTemplate"), object: nil)
    }
    
}
// отображение истории платежей в таблице
extension MainViewController: UITableViewDelegate, UITableViewDataSource{
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return  payments.count
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return payments[section].value.count
    }
    
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AccountsMainTableViewCell
        let object = payments[indexPath.section].value[indexPath.row]
        cell.nameLabel.text = object.description
        cell.typeOperationLabel.text = object.operationType
        cell.currencyLabel.text = object.currency?.symbol
        cell.statusLabel.text = object.confirmType
        cell.sumLabel.text = "\(String(format:"%.2f", object.amount ?? 0.0))"
        cell.statusType = object.confirmType
        self.getImageMainAccount(image: object.imageName ?? "", imageView: cell.accountImage, cell: cell)
        getStatusForHistoryPaymentMainPage(confirmTypeID: object.confirmTypeID ?? 0, cell: cell )
        
        selectedCellCustomColor(cell: cell)
        
        return cell
    }
    //  изменение высоты таблицы под контент
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.tableHeight?.constant = self.tableView.contentSize.height + 80
    }
    //  изменение цвета Header и title
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        
        let firstPaymentInSection =  self.payments[section].key
        let label = UILabel(frame: CGRect(x: 12, y: 9, width: view.frame.size.width, height: 20))
        label.text = firstPaymentInSection
        label.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        headerView.addSubview(label)
        headerView.backgroundColor = .clear
        return headerView
    }
    //  изменение высоты Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    //    выбор ячейки таблицы
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = payments[indexPath.section].value[indexPath.row]
        
        switch  object.operationTypeID {
            
        case InternetBankingOperationType.UtilityPayment.rawValue:
            let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateViewController") as! TemplateViewController
            vc.imageUtility = object.imageName ?? ""
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.InternalOperation.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.InternalOperation.rawValue
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.CliringTransfer.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
            vc.currencyCurrentID = object.currencyID ?? 0
            vc.confirmType = object.confirmTypeID ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.GrossTransfer.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.GrossTransfer.rawValue
            vc.currencyCurrentID = object.currencyID ?? 0
            vc.confirmType = object.confirmTypeID ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.SwiftTransfer.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemaplateSwiftViewController") as! TemaplateSwiftViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.SwiftTransfer.rawValue
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.Conversion.rawValue:
            let storyboard = UIStoryboard(name: "Conversion", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateCurreciesViewController") as! TemplateCurreciesViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.Conversion.rawValue
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.LoanPayment.rawValue:
            let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CreditDetailViewController") as! CreditDetailViewController
            vc.operationID = object.operationID ?? 0
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.paymentType = InternetBankingOperationType.LoanPayment.rawValue
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.MoneyTransfer.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "XFincaDetailOperationViewController") as! XFincaDetailOperationViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            print("")
        }
        
    }
    
}

//  создание коллекции для баннеров
extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    // количество секций  коллекции
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return banners.count
    }
    // количество ячеек коллекции
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! BannerCollectionViewCell
        
        self.pageControlDotCount = indexPath.row + 1
        
        self.getImageMainAccount(image: banners[indexPath.row].fileName ?? "" , imageView: cell.bannerImage, cell: cell)
        
        return cell
    }
    // количество точек pageControl
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControl.currentPage = indexPath.row
    }
    // изменение размера коллекции
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
    }
    // переход по ссылке при нажатии на баннер, с уведомлением
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let alertController = UIAlertController(title: nil, message: localizedText(key: "open_link_in_safari"), preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            if let urlString = self.banners[indexPath.row].link{
                let svc = SFSafariViewController(url: URL(string: urlString)!)
                self.present(svc, animated: true, completion: nil)
            }
        }
        let actionCancel = UIAlertAction(title: localizedText(key: "cancel"), style:  .cancel, handler: nil)
        alertController.addAction(action)
        alertController.addAction(actionCancel)
        present(alertController, animated: true, completion: nil)
        
    }
    
    // создание PageControll для баннеров
    func setupButtonControll()  {
        let buttonControllStackView = UIStackView(arrangedSubviews: [pageControl])
        buttonControllStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonControllStackView.distribution = .fillEqually
        bannerView.addSubview(buttonControllStackView)
        
        NSLayoutConstraint.activate([
            buttonControllStackView.bottomAnchor.constraint(equalTo: bannerView.bottomAnchor),
            buttonControllStackView.leadingAnchor.constraint(equalTo: bannerView.leadingAnchor),
            buttonControllStackView.trailingAnchor.constraint(equalTo: bannerView.trailingAnchor),
            buttonControllStackView.heightAnchor.constraint(equalToConstant: 4)
            ])
        
    }
    // количество точек в PageControll
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        pageControl.currentPage = Int(x / view.frame.width)
        
    }
    
    // таймер для перелистования баннеров
    func startTimer() {
        _ =  Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
    }
    // автоматическое перелистование баннеров
    @objc func autoScroll() {
        self.pageControl.currentPage = pageControlDotCount
        if self.pageControlDotCount < banners.count {
            let indexPath = IndexPath(item: pageControlDotCount, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
            self.pageControlDotCount = self.pageControlDotCount + 1
            setNavBarHidden(scrollView.contentOffset.y < -60)
            
        } else {
            self.pageControlDotCount = 0
            self.collectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: false)
            setNavBarHidden(scrollView.contentOffset.y < -60)
        }
    }
}

extension MainViewController{
    
    //запрос на список историй операций
    func getHistoryOfPayments(operationType: String?, operationState: String?, dateFrom: String?, dateTo: String?, filter: String?, page: Int, pageSize: Int, sortField: String, sortType: Int) {
        showLoading()
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        let getOperations:Observable<ListContractModelExecuteResult> = managerApi
            .getOperations(operationType: operationType, operationState: operationState, dateFrom: dateFrom, dateTo: dateTo, filter: filter, page: 1, pageSize: 7, sortField: sortField, sortType: sortType)
        let getBankDate:Observable<String> = managerApi
            .getBankDate()
        
        Observable
            .zip(currenciesObservable, getOperations, getBankDate)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] currencies, historyPayments, bankDate in
                    guard let `self` = self else { return }
                    self.currencies = currencies
                    
                    UserDefaultsHelper.currencies = currencies
                    UserDefaultsHelper.bankDate = bankDate.replacingOccurrences(of: "\"", with: "", options: NSString.CompareOptions.literal, range: nil)
                    if let paymentsArray = historyPayments.result{
                        var downloadPayments = [(key: String, value: [ContractModel])]()
                        downloadPayments.removeAll()
                        self.payments.removeAll()
                        self.historyOperationArray.removeAll()
                        self.historyOperationArray.append(contentsOf: paymentsArray)
                        self.tableView.tableFooterView?.isHidden = true
                        self.historyOperationArray = self.accountsCurrencies(currency: self.currencies, ContractModel: paymentsArray)
                        self.historyOperationArrayNew.append(contentsOf: self.historyOperationArray )
                        
                        let historyOperations = self.sortingArrayOf(payments: self.historyOperationArrayNew)
                        
                        let groupedPayments = Dictionary(grouping: historyOperations, by: { (element) -> Date in
                            return self.stringToDateFormatterForHistory(date: element.key)
                        })
                        
                        let sortedKeys = groupedPayments.keys.sorted()
                        sortedKeys.forEach({ (key) in
                            let values = groupedPayments[key]
                            downloadPayments.append(contentsOf: values ?? [])
                        })
                        
                        downloadPayments.reverse()
                        
                        self.payments = downloadPayments
                        
                        if self.payments.isEmpty{
                            self.tableView.isHidden = true
                            self.historyHeaderView.isHidden = true
                            self.emptyStateStack.isHidden = false
                        }else{
                            self.tableView.isHidden = false
                            self.historyHeaderView.isHidden = false
                            self.emptyStateStack.isHidden = true
                            
                        }
                        
                        self.tableView.reloadData()
                    }
                    self.tableView.reloadData()
                    self.hideLoading()
                    self.refreshControl.endRefreshing()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                    self?.refreshControl.endRefreshing()
            })
            .disposed(by: disposeBag)
    }
    //     сортировка массива
    func sortingArrayOf(payments: [ContractModel]) ->  [(key: String, value: [ContractModel])] {
        
        var sortedPaymentsUpdate = [(key: String, value: [ContractModel])]()
        sortedPaymentsUpdate.removeAll()
        for item in payments{
            item.date = self.dateFormater(date: item.date!)
        }
        var sortedPayments = [String:[ContractModel]]()
        for payment in payments {
            
            if sortedPayments.keys.contains(payment.date ?? "") {
                var paymentsByDate = sortedPayments[payment.date ?? ""]
                paymentsByDate?.append(payment)
                sortedPayments.updateValue(paymentsByDate ?? [], forKey: payment.date ?? "")
                
            } else {
                sortedPayments.updateValue([payment], forKey: payment.date ?? "")
            }
        }
        for payment in sortedPayments {
            let paymentForDate = payment.value
            
            let sortedArray = paymentForDate.sorted(by: {$0.date?.compare($1.date!) == .orderedDescending })
            sortedPayments.updateValue(sortedArray, forKey: payment.key)
        }
        
        sortedPaymentsUpdate = sortedPayments.sorted(by: {$0.value.first?.date?.compare(($1.value.first?.date)!) == .orderedDescending})
        
        return sortedPaymentsUpdate
    }
    
    // получение валют
    func accountsCurrencies(currency: [Currency], ContractModel:[ContractModel]) -> [ContractModel]{
        
        for item in ContractModel{
            for idCurrnecy in currency{
                if item.currencyID == idCurrnecy.currencyID{
                    item.currency = idCurrnecy
                }
            }
        }
        return ContractModel
    }
    // получение  баннеров
    func  getBanners() {
        showLoading()
        
        managerApi
            .getBanners(location: 0)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] banners in
                    guard let `self` = self else { return }
                    if banners.count > 0{
                        self.bannerView.isHidden = false
                        self.bannerDivider.isHidden = false
                    }else{
                        self.bannerView.isHidden = true
                        self.bannerDivider.isHidden = true
                    }
                    self.banners = banners
                    self.pageCount = banners.count
                    self.setupButtonControll()
                    self.collectionView.reloadData()
                    self.startTimer()
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.bannerView.isHidden = true
                    //                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    права доступа
    func allowView(){
        if AllowHelper.shared.viewAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsViewAllowed"){
            viewButtonTemplate.roundView(view: viewButtonTemplate, imageView: viewImageQr, imageName: "note-text-main")
            templatesLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            templatesButton.isEnabled = true
        }else{
            viewButtonTemplate.roundView(view: viewButtonTemplate, imageView: viewImageQr, imageName: "note-textDisable")
            templatesLabel.textColor = #colorLiteral(red: 0, green: 0.5294117647, blue: 0.7921568627, alpha: 1)
            templatesButton.isEnabled = false
        }
        
        if AllowHelper.shared.viewAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsViewAllowed"){
            viewButtonWallet.roundView(view: viewButtonWallet, imageView: viewImageUtilities, imageName: "wallet1")
            utilitiesLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            utilitiesButton.isEnabled = true
        }else{
            viewButtonWallet.roundView(view: viewButtonWallet, imageView: viewImageUtilities, imageName: "walletDisable")
            utilitiesLabel.textColor = #colorLiteral(red: 0, green: 0.5294117647, blue: 0.7921568627, alpha: 1)
            utilitiesButton.isEnabled = false
        }
        
        if AllowHelper.shared.viewAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsViewAllowed"){
            viewButtonConverssion.roundView(view: viewButtonConverssion, imageView: viewImageConverssion, imageName: "exchange1")
            conversionLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            conversionButton.isEnabled = true
        }else{
            viewButtonConverssion.roundView(view: viewButtonConverssion, imageView: viewImageConverssion, imageName: "exchangeDisable")
            conversionLabel.textColor = #colorLiteral(red: 0, green: 0.5294117647, blue: 0.7921568627, alpha: 1)
            conversionButton.isEnabled = false
        }
        if AllowHelper.shared.viewAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsViewAllowed"){
            
            if categories != nil{
                isModifyCategories(modifyDate: modifyDateCategories)
                isModifyUtilities(modifyDate: modifyDateUtilities)
            }else{
                getCategoriesAndUtilities()
            }
            
            
            getHistoryOfPayments(operationType: operationType, operationState: operationState, dateFrom: "null", dateTo: "null", filter: "", page: self.page, pageSize: Constants.MAIN_PAGE_OPERATION_COUNT, sortField: "", sortType: 0)
            
            viewButtonHistory.roundView(view: viewButtonHistory, imageView: viewImageHistory, imageName: "time-history1")
            historyLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            historyButton.isEnabled = true
            historyTableView.isHidden = false
            historyHeaderView.isHidden = false
            
        }else{
            viewButtonHistory.roundView(view: viewButtonHistory, imageView: viewImageHistory, imageName: "time-historyDisable")
            historyLabel.textColor = #colorLiteral(red: 0, green: 0.5294117647, blue: 0.7921568627, alpha: 1)
            historyButton.isEnabled = false
            historyTableView.isHidden = true
            historyHeaderView.isHidden = true
        }
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed") &&
            AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed") &&
            AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed"){
            
            viewButtonTransaction.roundView(view: viewButtonTransaction, imageView: viewImageTransaction, imageName: "creditcard-outcome1")
            transactionsLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            transactionsButton.isEnabled = true
        }else{
            viewButtonTransaction.roundView(view: viewButtonTransaction, imageView: viewImageTransaction, imageName: "creditcard-outcomeDisable")
            transactionsLabel.textColor = #colorLiteral(red: 0, green: 0.5294117647, blue: 0.7921568627, alpha: 1)
            transactionsButton.isEnabled = false
        }
    }
}

extension MainViewController{
    //    получение категорий и утилит
    func  getCategoriesAndUtilities(){
        let categoriesObservable = managerApi
            .getCategories()
        
        let utilitiesObservable = managerApi
            .getUtilities()
        
        let getListUtilitiesObservable = managerApi
            .getListUtilities()
        
        showLoading()
        
        Observable
            .zip(categoriesObservable, utilitiesObservable, getListUtilitiesObservable)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](categories, utilities, listUtilities) in
                    guard let `self` = self else { return }
                    
                    self.modifyDateCategories = categories.date ?? ""
                    self.modifyDateUtilities = utilities.date ?? ""
                    UserDefaultsHelper.listUtilities = listUtilities
                    UserDefaultsHelper.categories = categories
                    UserDefaultsHelper.utilities = utilities
                    UserDefaultsHelper.modifyUtlitiesDate = self.modifyDateUtilities
                    UserDefaultsHelper.modifyCategoriesDate = self.modifyDateCategories
                    
                    self.tableView.reloadData()
                    self.hideLoading()
                    
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    проверка на изменение в категории
    func  isModifyCategories(modifyDate: String){
        showLoading()
        managerApi
            .getIsModifyCategories(date: modifyDate)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](isModify) in
                    guard let `self` = self else { return }
                    
                    if isModify == "true"{
                        self.getCategoriesAndUtilities()
                    }
                    
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    проверка на обновление списка поставщиков услуг
    func  isModifyUtilities(modifyDate: String){
        showLoading()
        managerApi
            .getIsModifyUtilities(date: modifyDate)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (isModify) in
                    guard let `self` = self else { return }
                    
                    if isModify == "true"{
                        self.getCategoriesAndUtilities()
                    }
                    
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

