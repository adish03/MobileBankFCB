//
//  ChangeAvatarController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 22.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import MobileBankCore

class ChangeAvatarController: BaseViewController {
    
    lazy var contenerAvatar: BaseItemView = {
        let view = BaseItemView()
        return view
    }()
    
    lazy var imageIcon: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(hexFromString: "#B30931")
        view.layer.cornerRadius = 25
        return view
    }()
    
    lazy var imageUpload: UIImageView = UIImageView(image: UIImage(named: "camera-create"))
    
    lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.text = "Загрузить новое фото"
        return view
    }()
    
    var pickerController: UIImagePickerController? = nil
    
    override func viewDidLoad() {
        title = "Загрузить фото"
        
        view.addSubview(contenerAvatar)
        contenerAvatar.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(10)
            make.left.right.equalToSuperview().inset(16)
            make.height.equalTo(50)
        }
        
        contenerAvatar.setClickListener { [self] in
            pickerController = UIImagePickerController()

            pickerController?.delegate = self
            pickerController?.allowsEditing = true
            pickerController?.mediaTypes = ["public.image"]
            
            if let pickerController = pickerController {
                present(pickerController, animated: true)
            }
        }
        
        contenerAvatar.addSubview(imageIcon)
        imageIcon.snp.makeConstraints { make in
            make.height.width.equalTo(50)
            make.left.equalToSuperview()
            make.top.equalToSuperview()
        }
        
        contenerAvatar.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(imageIcon.snp.right).offset(10)
        }
        
        imageIcon.addSubview(imageUpload)
        imageUpload.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
}

extension ChangeAvatarController: UIImagePickerControllerDelegate {

    public func imagePickerController(_ picker: UIImagePickerController,
                                      didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        guard let image = info[.editedImage] as? UIImage else {
            return
        }
        
        print(image)
        picker.dismiss(animated: true, completion: nil)
        
        showLoading()
        
        managerApi
            .uploadImage(Content: image.jpegData(compressionQuality: 100)?.base64EncodedString() ?? "", FileName: SessionManager.shared.user?.loginID ?? "")
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    self.showAlertController("Фото загружено")
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension ChangeAvatarController: UINavigationControllerDelegate {
    
}
