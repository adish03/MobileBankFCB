//
//  ChangePasswordViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/21/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
//  класс для изменение пароля пользователя
protocol ChangeOldPasswordProtocol {
    func isClearPasswordTextField(isClear: Bool)
}
class ChangePasswordViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var menu: UIBarButtonItem!
    
    @IBOutlet weak var currentPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var repeatNewPasswordTextField: UITextField!
    @IBOutlet weak var changePasswordButton: RoundedButton!
    @IBOutlet weak var countSymbolsImage: UIImageView!
    @IBOutlet weak var typeSymbolImage: UIImageView!
    @IBOutlet weak var numberExistSymbolImage: UIImageView!
    
    @IBOutlet weak var specSymbolsImage: UIImageView!
    
    @IBOutlet weak var viewCurrentPasswordTextField: UIView!
    @IBOutlet weak var viewNewPasswordTextField: UIView!
    @IBOutlet weak var viewRepeatNewPasswordTextField: UIView!
    
    //label error
    @IBOutlet weak var labelErrorCurrentPassword: UILabel!
    @IBOutlet weak var labelErrorNewPassword: UILabel!
    @IBOutlet weak var labelErrorRepeatNewPassword: UILabel!
    
    @IBOutlet weak var eyeCurrentPasswordImage: UIImageView!
    @IBOutlet weak var eyeNewPasswordImage: UIImageView!
    @IBOutlet weak var eyeRepeatNewPasswordImage: UIImageView!
    
    @IBOutlet weak var changeButton: RoundedButton!
    @IBOutlet weak var specSymbolsStack: UIView!
    @IBOutlet weak var numbersStack: UIView!
    @IBOutlet weak var lettersStack: UIView!
    @IBOutlet weak var countStack: UIView!
    
    @IBOutlet weak var countSymbolsLabel: UILabel!
    
    
    var isNeedChangePassword = false
    var delegate: ChangeOldPasswordProtocol?
    var passwordLength = 0
    var passwordStrength = 0
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = localizedText(key: "ChangePassword")
        hideKeyboardWhenTappedAround()
        
        if let Length = UserDefaultsHelper.PasswordLength{
            passwordLength = Length
        }
        if let Strength = UserDefaultsHelper.PasswordStrength{
            passwordStrength = Strength
        }
        countSymbolsLabel.text = LocalizeHelper.shared.addWord("\(passwordLength)", localizedText(key: "or_more_characters"))
        
        if isNeedChangePassword{
            if  menu != nil{
                menu.image = UIImage(named: "ArrowBack")
                menu.target = self
                menu.action = #selector(closeButtonPressed)
            }
        }else{
            if  menu != nil{
                menu.target = self.revealViewController()
                menu.action = #selector(SWRevealViewController.revealToggle(_:))
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
        }
        
        removeFieldsCheckByPasswordStrengthType(passwordStrengthType: passwordStrength)
        
        currentPasswordTextField.delegate = self
        newPasswordTextField.delegate = self
        repeatNewPasswordTextField.delegate = self
        
        currentPasswordTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControl.Event.editingDidEnd)
        newPasswordTextField.addTarget(self, action: #selector(textFieldEditingEnd(_:)), for: UIControl.Event.editingDidEnd)
        repeatNewPasswordTextField.addTarget(self, action: #selector(textFieldEditingEndRepeat(_:)), for: UIControl.Event.editingDidEnd)
    }
    
    // Проверка поля "Текущий пароль" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChange(_ sender: Any) {
        currentPasswordTextField.stateIfEmpty(view: viewCurrentPasswordTextField, labelError: labelErrorCurrentPassword)
    }
    // Проверка поля "Введите новый пароль" на наличие данных, после потери фокуса
    @objc func textFieldEditingEnd(_ sender: Any) {
        newPasswordTextField.stateIfEmpty(view: viewNewPasswordTextField, labelError: labelErrorNewPassword)
        checkPasswordLengthStrength(passwordStrengthType: passwordStrength, textField: newPasswordTextField)
    }
    // Проверка поля "Повторите новый пароль" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndRepeat(_ sender: Any) {
        repeatNewPasswordTextField.stateIfEmpty(view: viewRepeatNewPasswordTextField, labelError: labelErrorRepeatNewPassword)
    }
    //    кнопка закрытия
    @objc func closeButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeButton(_ sender: Any) {
        
        if currentPasswordTextField.text != "" &&
            newPasswordTextField.text != "" &&
            repeatNewPasswordTextField.text != ""{
            
            if newPasswordTextField.text != repeatNewPasswordTextField.text{
                showAlertController(localizedText(key:"PasswordsNotEquals"))
            }else{
                if let oldPassword = currentPasswordTextField.text, let newPassword = newPasswordTextField.text{
                    let oldCryptoPassword = CryptoManager.stringToIVWithAES128EncodedBase64(text: oldPassword)
                    let newCryptoPassword = CryptoManager.stringToIVWithAES128EncodedBase64(text: newPassword)
                    self.postChangePassword(old_password: oldCryptoPassword,
                                            new_password: newCryptoPassword)
                }
            }
        }else{
            currentPasswordTextField.stateIfEmpty(view: viewCurrentPasswordTextField, labelError: labelErrorCurrentPassword)
            newPasswordTextField.stateIfEmpty(view: viewNewPasswordTextField, labelError: labelErrorNewPassword)
            repeatNewPasswordTextField.stateIfEmpty(view: viewRepeatNewPasswordTextField, labelError: labelErrorRepeatNewPassword)
        }
        
    }
    
    @IBAction func currentPasswordSecureButton(_ sender: Any) {
        
        if currentPasswordTextField.isSecureTextEntry == true {
            currentPasswordTextField.isSecureTextEntry = false
            eyeCurrentPasswordImage.image = UIImage(named: "eyeOpen")
        }else{
            currentPasswordTextField.isSecureTextEntry = true
            eyeCurrentPasswordImage.image = UIImage(named: "eyeClose")
        }
    }
    @IBAction func newPasswordSecureButton(_ sender: Any) {
        
        if newPasswordTextField.isSecureTextEntry == true {
            newPasswordTextField.isSecureTextEntry = false
            eyeNewPasswordImage.image = UIImage(named: "eyeOpen")
        }else{
            newPasswordTextField.isSecureTextEntry = true
            eyeNewPasswordImage.image = UIImage(named: "eyeClose")
        }
    }
    @IBAction func repeatNewPasswordSecureButton(_ sender: Any) {
        
        if repeatNewPasswordTextField.isSecureTextEntry == true {
            repeatNewPasswordTextField.isSecureTextEntry = false
            eyeRepeatNewPasswordImage.image = UIImage(named: "eyeOpen")
        }else{
            repeatNewPasswordTextField.isSecureTextEntry = true
            eyeRepeatNewPasswordImage.image = UIImage(named: "eyeClose")
        }
    }
    
    
    //    проверка на количество символов
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.isEqual(currentPasswordTextField){
            
        }
        if textField.isEqual(newPasswordTextField){
            
        }
        if textField.isEqual(repeatNewPasswordTextField){
            
        }
    }
    
    //    проверка при наличии фокуса
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    func checkPassword(password: String) {
        
        
        
        
    }
    
    //    провека на наличие regularExpression
    func isValidPassword(password: String, regularExpression: String) -> Bool {
        
        let regularExpression = regularExpression
        let valid = NSPredicate(format: "SELF MATCHES %@", regularExpression).evaluate(with: password)
        
        print(valid)
        return valid
        
    }
    
    func removeFieldsCheckByPasswordStrengthType(passwordStrengthType: Int){
        
        switch passwordStrengthType {
        case PasswordStrengthType.Lowest.rawValue:
            
            countStack.isHidden = true
            lettersStack.isHidden = true
            numbersStack.isHidden = true
            specSymbolsStack.isHidden = true
            
        case PasswordStrengthType.VeryWeak.rawValue:
            
            lettersStack.isHidden = true
            numbersStack.isHidden = true
            specSymbolsStack.isHidden = true
            
        case PasswordStrengthType.Weak.rawValue:
            
            numbersStack.isHidden = true
            specSymbolsStack.isHidden = true
            
        case PasswordStrengthType.Medium.rawValue:
            
            specSymbolsStack.isHidden = true
            
        case PasswordStrengthType.High.rawValue:
            print("No removeFields")
        default:
            
            print("No validate")
        }
    }
    
    //  проверка на сложность и длину пароля
    func checkPasswordLengthStrength(passwordStrengthType: Int, textField: UITextField) {
        
        switch passwordStrengthType {
        case PasswordStrengthType.Lowest.rawValue:
            
            print("No validate")
            
        case PasswordStrengthType.VeryWeak.rawValue:
            
            if textField.text?.count ?? 0 > passwordLength{
                countSymbolsImage.image = UIImage(named: "circle-checked")
                changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
                changeButton.isEnabled = true
                viewNewPasswordTextField.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
            }else{
                countSymbolsImage.image = UIImage(named: "circle-nochecked")
                changeButton.isEnabled = false
                changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
                viewNewPasswordTextField.backgroundColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            }
            
        case PasswordStrengthType.Weak.rawValue:
            
            checkRegularExpressionWeakFields(password: textField.text ?? "")
            
        case PasswordStrengthType.Medium.rawValue:
            
            checkRegularExpressionMediumFields(password: textField.text ?? "")
            
        case PasswordStrengthType.High.rawValue:
            
            checkRegularExpressionHighFields(password: textField.text ?? "")
            
        default:
            
            print("No validate")
        }
    }
    //    проверка пароля на валидность
    func checkRegularExpressionHighFields(password: String){
        
        let regularExpressionLowerCase = "^(?=.*[a-z]).{\(passwordLength),}$"
        let regularExpressionUppercase =  "^(?=.*[A-Z]).{\(passwordLength),}$"
        let regularExpressionNumber =  "^(?=.*[0-9]).{\(passwordLength),}$"
        let regularExpressionSpecialSymbols =  "^(?=.*[!\"#$%&'()*+,-./:;<=>?@\\[\\\\\\]^_`{|}~]).{\(passwordLength),}$"
        
        if password.count < passwordLength{
            countSymbolsImage.image = UIImage(named: "circle-nochecked")
        }else{
            countSymbolsImage.image = UIImage(named: "circle-checked")
        }
        
        
        if isValidPassword(password: password, regularExpression: regularExpressionLowerCase) && isValidPassword(password: password, regularExpression: regularExpressionUppercase){
            typeSymbolImage.image = UIImage(named: "circle-checked")
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            changeButton.isEnabled = true
        }else{
            
            typeSymbolImage.image = UIImage(named: "circle-nochecked")
            changeButton.isEnabled = false
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
        }
        
        
        if isValidPassword(password: password, regularExpression: regularExpressionNumber){
            numberExistSymbolImage.image = UIImage(named: "circle-checked")
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            changeButton.isEnabled = true
            
        }else{
            numberExistSymbolImage.image = UIImage(named: "circle-nochecked")
            changeButton.isEnabled = false
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
        }
        
        if isValidPassword(password: password, regularExpression: regularExpressionSpecialSymbols){
            specSymbolsImage.image = UIImage(named: "circle-checked")
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            changeButton.isEnabled = true
        }else{
            specSymbolsImage.image = UIImage(named: "circle-nochecked")
            changeButton.isEnabled = false
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
            
        }
        
        if isValidPassword(password: password, regularExpression: regularExpressionLowerCase) &&
            isValidPassword(password: password, regularExpression: regularExpressionUppercase) &&
            isValidPassword(password: password, regularExpression: regularExpressionNumber ) &&
            isValidPassword(password: password, regularExpression: regularExpressionSpecialSymbols) &&
            password.count >= passwordLength{
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            changeButton.isEnabled = true
            viewNewPasswordTextField.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
        }else{
            changeButton.isEnabled = false
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
            viewNewPasswordTextField.backgroundColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
        }
    }
    //    проверка пароля на валидность
    func checkRegularExpressionMediumFields(password: String){
        
        let regularExpressionLowerCase = "^(?=.*[a-z]).{0,}$"
        let regularExpressionUppercase =  "^(?=.*[A-Z]).{0,}$"
        let regularExpressionNumber =  "^(?=.*[0-9]).{0,}$"
        
        if password.count < passwordLength{
            countSymbolsImage.image = UIImage(named: "circle-nochecked")
        }else{
            countSymbolsImage.image = UIImage(named: "circle-checked")
        }
        
        if isValidPassword(password: password, regularExpression: regularExpressionLowerCase) || isValidPassword(password: password, regularExpression: regularExpressionUppercase){
            typeSymbolImage.image = UIImage(named: "circle-checked")
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            changeButton.isEnabled = true
        }else{
            typeSymbolImage.image = UIImage(named: "circle-nochecked")
            changeButton.isEnabled = false
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
            
        }
        
        if isValidPassword(password: password, regularExpression: regularExpressionNumber){
            numberExistSymbolImage.image = UIImage(named: "circle-checked")
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            changeButton.isEnabled = true
        }else{
            numberExistSymbolImage.image = UIImage(named: "circle-nochecked")
            changeButton.isEnabled = false
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
            
        }
        if isValidPassword(password: password, regularExpression: regularExpressionLowerCase) &&
            isValidPassword(password: password, regularExpression: regularExpressionUppercase) &&
            isValidPassword(password: password, regularExpression: regularExpressionNumber ) &&
            password.count >= passwordLength{
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            changeButton.isEnabled = true
            viewNewPasswordTextField.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
        }else{
            changeButton.isEnabled = false
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
            viewNewPasswordTextField.backgroundColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
        }
    }
    //    проверка пароля на валидность
    func checkRegularExpressionWeakFields(password: String){
        
        let regularExpressionLowerCase = "^(?=.*[a-z]).{0,}$"
        let regularExpressionUppercase =  "^(?=.*[A-Z]).{0,}$"
        
        if password.count < passwordLength{
            countSymbolsImage.image = UIImage(named: "circle-nochecked")
        }else{
            countSymbolsImage.image = UIImage(named: "circle-checked")
        }
        
        
        if isValidPassword(password: password, regularExpression: regularExpressionLowerCase) || isValidPassword(password: password, regularExpression: regularExpressionUppercase){
            typeSymbolImage.image = UIImage(named: "circle-checked")
        }else{
            typeSymbolImage.image = UIImage(named: "circle-nochecked")
        }
        
        
        if isValidPassword(password: password, regularExpression: regularExpressionLowerCase) &&
            isValidPassword(password: password, regularExpression: regularExpressionUppercase) &&
            password.count >= passwordLength{
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            changeButton.isEnabled = true
            viewNewPasswordTextField.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
        }else{
            changeButton.isEnabled = false
            changeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).withAlphaComponent(0.5)
            viewNewPasswordTextField.backgroundColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
        }
    }
    
}

extension ChangePasswordViewController{
    public func postChangePassword(old_password: String,
                                   new_password: String){
        showLoading()
        if let refreshToken = SessionManager.shared.refreshToken{
            managerApi
                .postChangePassword(grant_type: "refresh_token",
                                    refresh_token: refreshToken,
                                    command: "change_password",
                                    old_password: old_password,
                                    new_password: new_password)
                .subscribe(
                    onNext: {[weak self] (user) in
                        guard let `self` = self else { return }
                        let alert = UIAlertController(title: self.localizedText(key: "PasswordSuccessfullyChanged"), message: "", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default) { (action) in
                            SessionManager.shared.user = nil
                            UserDefaultsHelper.PIN_new = ""
                            UserDefaultsHelper.pinIsEnabled = false
                            UserDefaultsHelper.pinNewIsEnabled = false
                            UserDefaultsHelper.PinLength = nil
                            UserDefaultsHelper.AuthType = nil
                            UserDefaultsHelper.SmsCodeLength = nil
                            UserDefaultsHelper.EtokenCodeLength = nil
                            UserDefaultsHelper.SmsCodeAuthExpires = nil
                            UserDefaultsHelper.IsInvalidSmsCodeAuthOnIncorrect = nil
                            self.delegate?.isClearPasswordTextField(isClear: true)
                            let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                            let appDelegate = UIApplication.shared.delegate as! AppDelegate
                            appDelegate.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            appDelegate.window?.rootViewController = vc
                        }
                        alert.addAction(action)
                        self.present(alert, animated: true, completion: nil)
                        
                        self.hideLoading()
                    },
                    onError: {[weak self] error in
                        self?.showAlertController(ApiHelper.shared.errorHelperChangePassword(error: error))
                        self?.hideLoading()
                })
                .disposed(by: disposeBag)
        }
    }
}
extension ChangePasswordViewController {
    
    //    переход на следкующее поле
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == currentPasswordTextField {
            textField.resignFirstResponder()
            newPasswordTextField.becomeFirstResponder()
        }else if textField == newPasswordTextField {
            textField.resignFirstResponder()
            repeatNewPasswordTextField.becomeFirstResponder()
        }else if textField == repeatNewPasswordTextField {
            textField.resignFirstResponder()
        }
        return true
    }
}
