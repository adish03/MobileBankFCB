//
//  ChangePinCode.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 22.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit
import MobileBankCore

class ChangePinCodeController: BaseViewController {
    @IBOutlet weak var oldPin: UITextField!
    @IBOutlet weak var newPin: UITextField!

    @IBOutlet weak var oldImage: UIImageView!
    @IBOutlet weak var newTestImage: UIImageView!

    
    override func viewDidLoad() {
        title = "Смена пина"
        
        oldPin.delegate = self
        newPin.delegate = self
    }
    
    @IBAction func oldPinView(_ sender: Any) {
        if oldPin.isSecureTextEntry == true {
            oldPin.isSecureTextEntry = false
            oldImage.image = UIImage(named: "eyeOpen")
        }else{
            oldPin.isSecureTextEntry = true
            oldImage.image = UIImage(named: "eyeClose")
        }
    }
    
    @IBAction func newPinAction(_ sender: Any) {
        if newPin.isSecureTextEntry == true {
            newPin.isSecureTextEntry = false
            newTestImage.image = UIImage(named: "eyeOpen")
        }else{
            newPin.isSecureTextEntry = true
            newTestImage.image = UIImage(named: "eyeClose")
        }
    }
    
    @IBAction func cardInfo(_ sender: UIButton) {
        
        let one = CryptoManager.stringToIVWithAES128EncodedBase64(text: (newPin.text ?? ""))
        let two = CryptoManager.stringToIVWithAES128EncodedBase64(text: (oldPin.text ?? ""))
        
        managerApi.postPinChange(
            ApplicationID: PhoneIdVendor,
            NewPin: one,
            OldPin: two)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: { [self] (avatarString) in
                    let alertController = UIAlertController(title: nil, message: "Пин код успешно сменен", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { view in
                    
                        self.navigationController?.popViewController(animated: true)
                    }))
                    present(alertController, animated: true, completion: nil)
                    
                    
                    self.hideLoading()
                },
                onError: {[weak self](error) in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension ChangePinCodeController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 4
        let currentString = (textField.text ?? "") as NSString
        let newString = currentString.replacingCharacters(in: range, with: string)

        return newString.count <= maxLength
    }
}
