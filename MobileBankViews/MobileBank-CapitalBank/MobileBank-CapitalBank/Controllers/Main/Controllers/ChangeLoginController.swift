//
//  ChangeLoginController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 22.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit
import MobileBankCore

class ChangeLoginController: BaseViewController {
    @IBOutlet weak var login: UITextField!
    @IBOutlet weak var password: UITextField!

    @IBOutlet weak var testDatadata: UIImageView!
    
    @IBAction func testData(_ sender: Any) {
        if password.isSecureTextEntry == true {
            password.isSecureTextEntry = false
            testDatadata.image = UIImage(named: "eyeOpen")
        }else{
            password.isSecureTextEntry = true
            testDatadata.image = UIImage(named: "eyeClose")
        }
    }

    override func viewDidLoad() {
        title = "Смена логина"
    }
    
    @IBAction func cardInfo(_ sender: UIButton){
        showLoading()
        
        let two = CryptoManager.stringToIVWithAES128EncodedBase64(text: (password.text ?? ""))

        managerApi.changeLogin(NewUserLogin: login.text ?? "", Password: two)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: { [self] (avatarString) in
                    let alertController = UIAlertController(title: nil, message: "Логин успешно сменен", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { view in
                    
                        self.navigationController?.popViewController(animated: true)
                    }))
                    present(alertController, animated: true, completion: nil)
                    self.hideLoading()
                },
                onError: {[weak self](error) in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
