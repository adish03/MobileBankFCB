//
//  SettingsController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 21.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import UIKit
import Alamofire
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa
import Foundation

class SettingsViewController: BaseViewController {
    
    var arrayOfUsers = [User]()
    var currentUserIndex = 0
    var currentUserLoginID = ""

    override func viewDidLoad() {
        title = "Настройки"
        
        
        if let user = UserDefaultsHelper.user{
            
            currentUserLoginID = user.loginID ?? ""
        }
        
        let image = UIImage(named: "Menu")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        self.arrayOfUsers.removeAll()
        if let users = UserDefaultsHelper.users{
            self.arrayOfUsers = users.filter({$0.loginID != currentUserLoginID})
            
        }
        
        let saveTwo = ItemSettings()
        saveTwo.view.text = "Загрузить фото"
        
        view.addSubview(saveTwo)
        saveTwo.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        
        saveTwo.setClickListener { [self] in
            performSegue(withIdentifier: "ShowChangeAvatar", sender: self)
        }
        
        let saveThree = ItemSettings()
        saveThree.view.text = "Персональные данные"
        
        view.addSubview(saveThree)
        saveThree.snp.makeConstraints { make in
            make.top.equalTo(saveTwo.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        
        
        saveThree.setClickListener { [self] in
            performSegue(withIdentifier: "ShowPersonalData", sender: self)
        }
        
        let save = ItemSettings()
        save.view.text = "Безопасность"
        
        view.addSubview(save)
        save.snp.makeConstraints { make in
            make.top.equalTo(saveThree.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        
        save.setClickListener { [self] in
            performSegue(withIdentifier: "ShowSecyrity", sender: self)
        }
        
        let password = ItemSettings()
        password.view.text = "Язык/Тил/Language"
        
        view.addSubview(password)
        password.snp.makeConstraints { make in
            make.top.equalTo(save.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        
        password.setClickListener { [self] in
            performSegue(withIdentifier: "testDaDaTest", sender: nil)
        }
        
        let login = ItemSettings()
        login.view.text = "Выйти"
        
        login.setClickListener { [self] in
            let alert = UIAlertController(title: localizedText(key: "are_you_sure"), message: "", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.postPinRemove(applicationID: self.PhoneIdVendor)
            }
            let actionCancel = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel)
            alert.addAction(action)
            alert.addAction(actionCancel)
            present(alert, animated: true, completion: nil)
        }
        
        view.addSubview(login)
        login.snp.makeConstraints { make in
            make.top.equalTo(password.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
    }
    
    func  postPinRemove(applicationID: String){
        
        showLoading()
        managerApi
            .postPinRemove(ApplicationID: applicationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    guard let `self` = self else { return }
                    if let loginId = SessionManager.shared.user?.loginID{
                        if self.arrayOfUsers.count == 0{
                            //self.user?.access_token = ""
                            SessionManager.shared.user = nil
                            UserDefaultsHelper.PIN_new = ""
                            UserDefaultsHelper.pinIsEnabled = false
                            UserDefaultsHelper.pinNewIsEnabled = false
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOut"), object: nil)
                        }else{
                            for user in self.arrayOfUsers{
                                if loginId == user.loginID{
                                    if let index = self.arrayOfUsers.index(where: { $0.loginID == loginId }) {
                                        print(index)
                                        self.arrayOfUsers.remove(at: index)
                                        UserDefaultsHelper.user = self.arrayOfUsers.last
                                        //self.nameLabel.text = self.arrayOfUsers.last?.userdisplayname
                                    }
                                }
                            }
                            if let loginID = self.arrayOfUsers[self.currentUserIndex].loginID{
                                SessionManager.shared.current(loginID: loginID)
                                SessionManager.shared.updateUser(user: self.arrayOfUsers[self.currentUserIndex])
                                SessionManager.shared.setIndexPath(userIndex: self.currentUserIndex)
                                //self.user = self.arrayOfUsers[self.currentUserIndex]
                                if let user =  SessionManager.shared.user{
                                    self.updateToken(user: user)
                                }
                            }
                        }
                        SessionManager.shared.logOutUser(loginID: loginId)
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
}

class ItemSettings: BaseItemView {
    
    lazy var view: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        return view
    }()
 
    override func layoutSubviews() {
        addSubview(view)
        view.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
        }
    }
}


class BaseItemView: UIView {
    
    var isLoaded = false
    var isSetupConfig = false
    
    private var clickListener: () -> Void = {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        configure()
    }
    
    private func configure() {
        if !isLoaded {
            isLoaded = true
            
            addSubViews()
            setupUI()
            onViewLoaded()
        }
    }
    
    func setClickListener(_ clickListener: @escaping () -> Void) {
        self.clickListener = clickListener
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        addGestureRecognizer(gesture)
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        clickListener()
    }
    
    open func onViewLoaded() {}
    
    open func addSubViews() {}
    
    open func setupUI() {}
}
