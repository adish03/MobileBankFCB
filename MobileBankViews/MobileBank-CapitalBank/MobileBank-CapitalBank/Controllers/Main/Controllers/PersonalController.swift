//
//  PersonalController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 22.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import MobileBankCore

class PersonalController: BaseViewController {
    
    lazy var labelName: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 16)
        return view
    }()
    
    override func viewDidLoad() {
        title = "Персональные данные"
        
        view.addSubview(labelName)
        labelName.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(16)
            make.left.equalToSuperview().offset(16)
        }
                
        showLoading()
        
        
        managerApi
            .getSenderFullName()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    self?.labelName.text = "\(String(decoding: response, as: UTF8.self) ?? "")\n\n\(UserDefaultsHelper.user?.phone ?? "")\n\n\(UserDefaultsHelper.user?.user_email ?? "")"
                    
                    self?.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
}
