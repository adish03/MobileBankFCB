//
//  UIStackItem.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 19.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit

class BaseView: UIView {
    
    var isLoaded = false
    var isSetupConfig = false
    
    private var clickListener: () -> Void = {}
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        configure()
    }
    
    private func configure() {
        if !isLoaded {
            isLoaded = true
            
            addSubViews()
            setupUI()
            onViewLoaded()
        }
    }
    
    func setClickListener(_ clickListener: @escaping () -> Void) {
        self.clickListener = clickListener
        
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.checkAction))
        addGestureRecognizer(gesture)
    }
    
    @objc func checkAction(sender : UITapGestureRecognizer) {
        clickListener()
    }
    
    open func onViewLoaded() {}
    
    open func addSubViews() {}
    
    open func setupUI() {}
}
