//
//  TarrifAndLimitsController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 21.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//


import Foundation
import UIKit
import SnapKit
import MobileBankCore
import RxSwift

class TarrifAndLimitsController: BaseViewController {
    
    private lazy var tableView: UITableView = {
        let view = UITableView(frame: .zero, style: .grouped)
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    var itemsOne: [LimitsModel] = []
    var itemsTwo: [String] = []
    
    override func viewDidLoad() {
        title = "Тарифи и лимиты"
        
        let image = UIImage(named: "Menu")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(10)
            make.left.right.equalToSuperview()
            make.bottom.equalTo(view.safeArea.bottom)
        }
        
        managerApi.getLimits()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (limits) in
                    self?.itemsOne = limits

                    self?.hideLoading()
                    
                    self?.tableView.reloadData()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    var one = true
    var two = true
}

extension TarrifAndLimitsController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 && one {
            return itemsOne.count
        } else if section == 0 && one == false {
            return 0
        }
        
        if section == 1 {
            return itemsTwo.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let model = itemsOne[indexPath.row]
            let cell = UITableViewCell()
            cell.textLabel?.text = "Название лимита: \(model.limitName ?? "")\nCумма лимита: \(model.limitSum ?? 0.0)\n\n\(model.comment ?? "")\n\n\(model.date ?? "")"
            cell.textLabel?.numberOfLines = 0
            return cell
        } else {
            let cell = UITableViewCell()
            cell.textLabel?.text = "test1"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            let view = BaseItemView()
            let label = UILabel()
            let image = UIImageView(image: UIImage(named: "chevron-top"))
            view.backgroundColor = .white
            
            if one {
                image.image = UIImage(named: "chevron-bottom")
            } else {
                image.image = UIImage(named: "chevron-top")
            }
            
            view.addSubview(label)
            label.textColor = .black
            label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            
            view.setClickListener { [self] in
                one = !one
                
                if one {
                    image.image = UIImage(named: "chevron-bottom")
                } else {
                    image.image = UIImage(named: "chevron-top")
                }
                
                tableView.reloadSections([0], with: .automatic)
            }
            
            label.text = "Лимиты"
            
            label.snp.makeConstraints { make in
                make.center.equalToSuperview()
            }
            
            view.addSubview(image)
            image.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.right.equalToSuperview().offset(-16)
            }
            
            return view
        }
        
        if section == 1 {
            let view = BaseItemView()
            let label = UILabel()
            let image = UIImageView(image: UIImage(named: "chevron-top"))
            view.backgroundColor = .white
            
            view.addSubview(label)
            label.textColor = .black
            label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
            
            if two {
                image.image = UIImage(named: "chevron-bottom")
            } else {
                image.image = UIImage(named: "chevron-top")
            }
            
            view.setClickListener { [self] in
                two = !two
                
                if two {
                    image.image = UIImage(named: "chevron-bottom")
                } else {
                    image.image = UIImage(named: "chevron-top")
                }
                
                tableView.reloadSections([0], with: .automatic)
            }
            
            label.text = "Тарифи"
            
            label.snp.makeConstraints { make in
                make.center.equalToSuperview()
            }
            
            view.addSubview(image)
            image.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.right.equalToSuperview().offset(-16)
            }
            
            return view
        }
        
        return nil
    }
}
