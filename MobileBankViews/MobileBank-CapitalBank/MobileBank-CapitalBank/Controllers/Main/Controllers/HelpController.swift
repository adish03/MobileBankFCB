//
//  HelpController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 19.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import MobileBankCore


class HelpController: BaseViewController {
        
    var imageLogo: UIImageView = UIImageView(image: UIImage(named: "logoNew"))
    
    var titleLabel: UILabel = {
        let view = UILabel()
        view.text = "ЗАО «ФИНКА Банк»"
        view.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        return view
    }()
    
    var titleLabelTwo: UILabel = {
        let view = UILabel()
        view.text = #"Лицензия НБКР №051. © ЗАО "ФИНКА БАНК" 2022"#
        view.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        view.textAlignment = .center
        view.numberOfLines = 2
        return view
    }()
    
    lazy var titleLabelSite: UILabel = {
        let view = UILabel()
        view.text = "www.fincabank.kg"
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
        return view
    }()
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        if let url = URL(string: "https://www.fincabank.kg/") {
            UIApplication.shared.open(url)
        }
    }
    
    var button: UIButton = {
        let view = UIButton()
        view.setTitle("Проверить обновления", for: .normal)
        view.setTitleColor(UIColor.black, for: .normal)
        view.layer.cornerRadius = 7
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.7098039216, green: 0.03529411765, blue: 0.2196078431, alpha: 1)
        view.addTarget(nil, action: #selector(clickListnerer(view:)), for: .touchUpInside)
        return view
    }()
    
    @objc func clickListnerer(view: UIButton) {
        if let url = URL(string: "https://apps.apple.com/ru/app/finca-%D0%B1%D0%B0%D0%BD%D0%BA-%D0%BA%D1%8B%D1%80%D0%B3%D1%8B%D0%B7%D1%81%D1%82%D0%B0%D0%BD/id1475895701") {
            UIApplication.shared.open(url)
        }
    }
    
    var version = InfoView()
    var model = InfoView()
    var versionOs = InfoView()
    var number = InfoView()

    
    
    override func viewDidLoad() {
        title = "О приложении"
        
        let image = UIImage(named: "Menu")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        view.addSubview(imageLogo)
        imageLogo.snp.makeConstraints { make in
            make.height.width.equalTo(200)
            make.top.equalTo(view.safeArea.top).offset(20)
            make.centerX.equalToSuperview()
        }
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(imageLogo.snp.bottom)
            make.centerX.equalToSuperview()
        }
        
        view.addSubview(titleLabelTwo)
        titleLabelTwo.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom)
            make.centerX.equalToSuperview()
            make.left.right.equalToSuperview().inset(4)
        }
        
        view.addSubview(titleLabelSite)
        titleLabelSite.snp.makeConstraints { make in
            make.top.equalTo(titleLabelTwo.snp.bottom).offset(20)
            make.centerX.equalToSuperview()
        }
        
        let lineView = UIView()
        lineView.backgroundColor = .lightGray
        view.addSubview(lineView)
        lineView.snp.makeConstraints { make in
            make.top.equalTo(titleLabelSite.snp.bottom).offset(20)
            make.left.right.equalToSuperview().inset(8)
            make.height.equalTo(1)
        }
        
        view.addSubview(version)
        view.addSubview(button)
        button.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.width.equalTo(240)
            make.centerX.equalToSuperview()
            make.top.equalTo(lineView.snp.bottom).offset(24)
            make.bottom.equalTo(version.snp.top).offset(-24)
        }
        
        view.addSubview(number)
        view.addSubview(versionOs)
        versionOs.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(number.snp.top)
            make.height.equalTo(40)
        }
        
        number.title.text = "Номер клиента"
        number.value.text = UserDefaultsHelper.user?.phone
        
        versionOs.title.text = "Версия OS"
        versionOs.value.text = UIDevice.current.systemVersion
        
        
        
        model.title.text = "Модель телефона"
        model.value.text = UIDevice.modelName
        
        version.title.text = "Версия приложения"
        version.value.text = "\(Bundle.main.releaseVersionNumber ?? "") (\(Bundle.main.buildVersionNumber ?? ""))"
        
        
        
        view.addSubview(model)
        model.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(versionOs.snp.top)
            make.height.equalTo(50)
        }
        
        version.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(model.snp.top)
            make.height.equalTo(50)
        }
        
        
        number.snp.makeConstraints { make in
            make.left.right.equalToSuperview()
            make.bottom.equalTo(view.safeArea.bottom).offset(-20)
            make.height.equalTo(50)
        }
    }
}

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
}

class InfoView: UIView {
    
    lazy var title: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16)

        return view
    }()
    
    lazy var value: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 16)
        view.textColor = .gray
        return view
    }()
    
    
    override func layoutSubviews() {
        addSubview(title)
        title.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalToSuperview().offset(16)
        }
        
        addSubview(value)
        value.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-16)
        }
        
        let viewS = UIView()
        viewS.backgroundColor = .lightGray
        addSubview(viewS)
        viewS.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.left.right.equalToSuperview().inset(8)
            make.height.equalTo(1)
        }
    }
}

public extension UIDevice {

    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }

        func mapToDevice(identifier: String) -> String { // swiftlint:disable:this cyclomatic_complexity
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                       return "iPod touch (5th generation)"
            case "iPod7,1":                                       return "iPod touch (6th generation)"
            case "iPod9,1":                                       return "iPod touch (7th generation)"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":           return "iPhone 4"
            case "iPhone4,1":                                     return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                        return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                        return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                        return "iPhone 5s"
            case "iPhone7,2":                                     return "iPhone 6"
            case "iPhone7,1":                                     return "iPhone 6 Plus"
            case "iPhone8,1":                                     return "iPhone 6s"
            case "iPhone8,2":                                     return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                        return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                        return "iPhone 7 Plus"
            case "iPhone10,1", "iPhone10,4":                      return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                      return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                      return "iPhone X"
            case "iPhone11,2":                                    return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                      return "iPhone XS Max"
            case "iPhone11,8":                                    return "iPhone XR"
            case "iPhone12,1":                                    return "iPhone 11"
            case "iPhone12,3":                                    return "iPhone 11 Pro"
            case "iPhone12,5":                                    return "iPhone 11 Pro Max"
            case "iPhone13,1":                                    return "iPhone 12 mini"
            case "iPhone13,2":                                    return "iPhone 12"
            case "iPhone13,3":                                    return "iPhone 12 Pro"
            case "iPhone13,4":                                    return "iPhone 12 Pro Max"
            case "iPhone14,4":                                    return "iPhone 13 mini"
            case "iPhone14,5":                                    return "iPhone 13"
            case "iPhone14,2":                                    return "iPhone 13 Pro"
            case "iPhone14,3":                                    return "iPhone 13 Pro Max"
            case "iPhone14,7":                                    return "iPhone 14"
            case "iPhone14,8":                                    return "iPhone 14 Plus"
            case "iPhone15,2":                                    return "iPhone 14 Pro"
            case "iPhone15,3":                                    return "iPhone 14 Pro Max"
            case "iPhone8,4":                                     return "iPhone SE"
            case "iPhone12,8":                                    return "iPhone SE (2nd generation)"
            case "iPhone14,6":                                    return "iPhone SE (3rd generation)"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":      return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":                 return "iPad (3rd generation)"
            case "iPad3,4", "iPad3,5", "iPad3,6":                 return "iPad (4th generation)"
            case "iPad6,11", "iPad6,12":                          return "iPad (5th generation)"
            case "iPad7,5", "iPad7,6":                            return "iPad (6th generation)"
            case "iPad7,11", "iPad7,12":                          return "iPad (7th generation)"
            case "iPad11,6", "iPad11,7":                          return "iPad (8th generation)"
            case "iPad12,1", "iPad12,2":                          return "iPad (9th generation)"
            case "iPad4,1", "iPad4,2", "iPad4,3":                 return "iPad Air"
            case "iPad5,3", "iPad5,4":                            return "iPad Air 2"
            case "iPad11,3", "iPad11,4":                          return "iPad Air (3rd generation)"
            case "iPad13,1", "iPad13,2":                          return "iPad Air (4th generation)"
            case "iPad13,16", "iPad13,17":                        return "iPad Air (5th generation)"
            case "iPad2,5", "iPad2,6", "iPad2,7":                 return "iPad mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":                 return "iPad mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":                 return "iPad mini 3"
            case "iPad5,1", "iPad5,2":                            return "iPad mini 4"
            case "iPad11,1", "iPad11,2":                          return "iPad mini (5th generation)"
            case "iPad14,1", "iPad14,2":                          return "iPad mini (6th generation)"
            case "iPad6,3", "iPad6,4":                            return "iPad Pro (9.7-inch)"
            case "iPad7,3", "iPad7,4":                            return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":      return "iPad Pro (11-inch) (1st generation)"
            case "iPad8,9", "iPad8,10":                           return "iPad Pro (11-inch) (2nd generation)"
            case "iPad13,4", "iPad13,5", "iPad13,6", "iPad13,7":  return "iPad Pro (11-inch) (3rd generation)"
            case "iPad6,7", "iPad6,8":                            return "iPad Pro (12.9-inch) (1st generation)"
            case "iPad7,1", "iPad7,2":                            return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":      return "iPad Pro (12.9-inch) (3rd generation)"
            case "iPad8,11", "iPad8,12":                          return "iPad Pro (12.9-inch) (4th generation)"
            case "iPad13,8", "iPad13,9", "iPad13,10", "iPad13,11":return "iPad Pro (12.9-inch) (5th generation)"
            case "AppleTV5,3":                                    return "Apple TV"
            case "AppleTV6,2":                                    return "Apple TV 4K"
            case "AudioAccessory1,1":                             return "HomePod"
            case "AudioAccessory5,1":                             return "HomePod mini"
            case "i386", "x86_64", "arm64":                       return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                              return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }

        return mapToDevice(identifier: identifier)
    }()

}
