//
//  SecyrityController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 22.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import MobileBankCore
import UIKit


class SecyrityController: BaseViewController {
    
    override func viewDidLoad() {
        title = "Безопастность"
        
        let saveTwo = ItemSettings()
        saveTwo.view.text = "Изменить логин"
        
        view.addSubview(saveTwo)
        saveTwo.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        
        saveTwo.setClickListener {
            self.performSegue(withIdentifier: "ToChangePassword", sender: self)
        }
        
        let saveThree = ItemSettings()
        saveThree.view.text = "Изменить пароль"
        
        view.addSubview(saveThree)
        saveThree.snp.makeConstraints { make in
            make.top.equalTo(saveTwo.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        
        saveThree.setClickListener { [self] in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
            vc.isNeedChangePassword = true
            vc.delegate = self as? ChangeOldPasswordProtocol
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let saveFour = ItemSettings()
        saveFour.view.text = "Изменить PIN"
        
        view.addSubview(saveFour)
        saveFour.snp.makeConstraints { make in
            make.top.equalTo(saveThree.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        
        saveFour.setClickListener {
            self.performSegue(withIdentifier: "ChangePinCode", sender: self)
            
        }
        
        let saveTous = ItemSettings()
        saveTous.view.text = "Вход по Face/Touch ID"
        
        let swetdf = UISwitch()
        swetdf.tintColor = UIColor.init(hexFromString: "#B30931")
        swetdf.onTintColor = UIColor.init(hexFromString: "#B30931")
        swetdf.isOn = UserDefaultsHelper.TouchIDisEnabled ?? false
        swetdf.addTarget(self, action: #selector(switchValueDidChange(_:)), for: .valueChanged)

        view.addSubview(saveTous)
        saveTous.snp.makeConstraints { make in
            make.top.equalTo(saveFour.snp.bottom)
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
        }
        
        saveTous.addSubview(swetdf)
        swetdf.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.right.equalToSuperview().offset(-20)
        }
    }
    
    @objc func switchValueDidChange(_ sender: UISwitch) {
        if let test = UserDefaultsHelper.TouchIDisEnabled {
            UserDefaultsHelper.TouchIDisEnabled = !test
        }
    }
}
