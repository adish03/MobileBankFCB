//
//  DocxController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 21.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import MobileBankCore

class DocxController: BaseViewController {
    
    private lazy var tableView: UITableView = {
        let view = UITableView()
        view.delegate = self
        view.dataSource = self
        return view
    }()
    
    var model: TermsTypeData? = nil
    
    override func viewDidLoad() {
        title = "Условия"
        
        let image = UIImage(named: "Menu")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top)
            make.left.right.equalToSuperview()
            make.bottom.equalTo(view.safeArea.bottom)
        }
        
        showLoading()
        
        managerApi
            .getTermsDocument()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    dump(response)
                    
                    self?.model = response
                    
                    self?.tableView.reloadData()
                    self?.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)

    }
}

extension DocxController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model?.result?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = model?.result?[indexPath.row]

        showLoading()
        
        managerApi
            .getDowloadTermsDocument(documentID: model?.documentID ?? 0)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    dump(response)
                    
                    if let stringPDF = response.result?.fileBody {
                        let name = response.result?.fileName
                        if let decodeData = Data(base64Encoded: stringPDF, options: .ignoreUnknownCharacters) {
                            
                            var filesToShare = [Any]()
                            filesToShare.append(ExcelFileHelper.shared.saveFileDirectory(docxFile: decodeData, name ?? "document.docx"))
                            let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
                            self?.present(activityViewController, animated: true, completion: nil)
                        }
                    }
                    self?.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = model?.result?[indexPath.row]
        let cell = UITableViewCell()
        
        cell.textLabel?.text = model?.fileName
        cell.imageView?.image = UIImage(named: "download")
        
        return cell
    }
    
    
}
