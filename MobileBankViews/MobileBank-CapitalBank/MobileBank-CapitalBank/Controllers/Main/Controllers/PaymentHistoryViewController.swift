//
//  PaymentHistoryViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/10/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift



// класс История платежей
class PaymentHistoryViewController: BaseViewController, FilterPropertyDelegate, NoNeedLoadDataDelegate{
  
  
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    
    var filteredArray = [ContractModel]()
    var isSearching = false
    var isFiltered = false
    var historyOperationArray = [ContractModel]()
    var historyOperationArrayNew = [ContractModel]()
    var currencies = [Currency]()
    var navigationTitle = ""
    
    var templates: [ContractModelTemplate] = []
    var refreshControl:UIRefreshControl!
    var text = ""
    var fetchingMore = false
    var page = 1
    var totalItems = 0
    var pageSize = 0
    var pageCount = 0
    
    var dateFrom = ""
    var dateTo = ""
    var operationType = ""
    var operationState = ""
  
    var payments = [(key: String, value: [ContractModel])]()
    var sortedPaymentsUpdate = [(key: String, value: [ContractModel])]()
    var isNeedData = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        barButtonCustomize()
        if isNeedData{
            if isFiltered{
                payments.removeAll()
                historyOperationArrayNew.removeAll()
                getHistoryOfPayments(operationType: operationType, operationState: operationState, dateFrom: dateFrom, dateTo: dateTo, filter: "", page: self.page, pageSize: Constants.MAIN_PAGE_OPERATION_COUNT, sortField: "", sortType: 0)
            }else{
                payments.removeAll()
                getHistoryOfPayments(operationType: operationType, operationState: operationState, dateFrom: "null", dateTo: "null", filter: "", page: self.page, pageSize: Constants.MAIN_PAGE_OPERATION_COUNT, sortField: "", sortType: 0)
                tableView.reloadData()
            }
        }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = localizedText(key: "operation_history")
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        emptyStateStack.isHidden = true
        if  menu != nil{
            menu.target = self.revealViewController()
            menu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
    
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        hideKeyboardWhenTappedAround()
        tableView.reloadData()
    }
    // обработка обновления списка
    @objc func refresh(_ sender: Any){
        refreshBegin(newtext: "Refresh",
                     refreshEnd: {(x:Int) -> () in
                        if !self.isFiltered{
                            self.getHistoryOfPayments(operationType: self.operationType, operationState: self.operationState, dateFrom: "null", dateTo: "null", filter: "", page: self.page, pageSize: Constants.MAIN_PAGE_OPERATION_COUNT, sortField: "", sortType: 0)
                        }
                        self.tableView.reloadData()
                        self.refreshControl.endRefreshing()
        })
    }
    // обработка обновления списка
    func refreshBegin(newtext:String, refreshEnd:@escaping (Int) -> ()) {
        DispatchQueue.global().async {
            print("refreshing")
            self.text = newtext            
            DispatchQueue.main.async  {
                refreshEnd(0)
            }
        }
    }
    // проверка при скроллинге до конца страницы
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height{
            if !fetchingMore{
                fetchingMore = true
                page += 1
                
                if isFiltered{
                    print("total Items: \(totalItems) ---- payments.count: \(payments.count * 10)")
                    if !(totalItems < pageCount * pageSize){
                        getHistoryOfPayments(operationType: operationType, operationState: operationState, dateFrom: dateFrom, dateTo: dateTo, filter: "", page: self.page, pageSize: Constants.MAIN_PAGE_OPERATION_COUNT, sortField: "", sortType: 0)
                    }
                }else{
                    if !(totalItems < pageCount * pageSize){
                        self.activityIndicator.startAnimating()
                        self.tableView.tableFooterView?.isHidden = false
                        getHistoryOfPayments(operationType: operationType, operationState: operationState, dateFrom: "null", dateTo: "null", filter: "", page: self.page, pageSize: Constants.MAIN_PAGE_OPERATION_COUNT, sortField: "", sortType: 0)
                    }
                }
            }
        }
    }
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFilterSegue"{
            let vc = segue.destination as! FilterPaymentHistoryViewController
            
            vc.dateFrom = self.dateFrom
            vc.dateTo = self.dateTo
            vc.operationType = self.operationType
            vc.operationState = self.operationState
            vc.isNeedData = true
            vc.delegate = self
        }
    }
    //    установка значений при фильтрации
    func property(dateFrom: String?, dateTo: String?, operationType: String?, page: Int, operationState: String?, isFilter: Bool?) {
        self.dateFrom = dateFrom ?? "null"
        self.dateTo = dateTo ?? "null"
        self.page = page
        self.operationType = operationType ?? "null"
        self.operationState = operationState ?? "null"
        self.isFiltered = isFilter ?? true
    }
    //    установка значений при сбросе фильтрации
    func resetFilter(isFilter: Bool?) {
        self.isFiltered = isFilter ?? false
//        self.dateFrom = "01.01.2000"
        self.dateFrom = ""
        self.dateTo = ""
        self.operationType = "null"
        self.operationState = "null"
        self.page = 1
        payments.removeAll()
        historyOperationArrayNew.removeAll()
        
    }
//      перегрузка данных
    func isNeedData(isNeed: Bool) {
        isNeedData = isNeed
    }
    
    
    
}
// отображение истории платежей в таблице
extension PaymentHistoryViewController: UITableViewDelegate, UITableViewDataSource{
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearching{
            return 1
        }
        return  payments.count
    }
    
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredArray.count
        }else{
            return payments[section].value.count
        }
        
    }
   
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PaymentHistoryTableViewCell
        
        if isSearching{
            
            cell.nameLabel.text = filteredArray[indexPath.row].description
            cell.typeOperationLabel.text = filteredArray[indexPath.row].operationType
            cell.currencyLabel.text = filteredArray[indexPath.row].currency?.symbol
            cell.statusLabel.text = filteredArray[indexPath.row].confirmType
            cell.sumLabel.text = "\(String(format:"%.2f", filteredArray[indexPath.row].amount ?? 0.0))"
            
            self.getImageHistoryPayment(image: filteredArray[indexPath.row].imageName ?? "", imageView: cell.accountImage, cell: cell)
            getStatusForHistoryPayment(confirmTypeID: filteredArray[indexPath.row].confirmTypeID ?? 0, cell: cell )
            
        }else{
            let object = payments[indexPath.section].value[indexPath.row]
            cell.nameLabel.text = object.description
            cell.typeOperationLabel.text = object.operationType
            cell.currencyLabel.text = object.currency?.symbol
            cell.statusLabel.text = object.confirmType
            cell.sumLabel.text = "\(String(format:"%.2f", object.amount ?? 0.0))"
            self.getImageHistoryPayment(image: object.imageName ?? "", imageView: cell.accountImage, cell: cell)
            getStatusForHistoryPayment(confirmTypeID: object.confirmTypeID ?? 0, cell: cell )
        }
        
        selectedCellCustomColor(cell: cell)
        
        return cell
    }
    // кастомизация хедера таблицы
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        if !self.payments.isEmpty{
            let firstPaymentInSection =  self.payments[section].key
        
            let label = UILabel(frame: CGRect(x: 12, y: 7, width: view.frame.size.width, height: 25))
            label.text = firstPaymentInSection
            label.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
            headerView.addSubview(label)
        }
        return headerView
    }
    //  изменение высоты Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    // обработка действия при выборе ячейки
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let object = payments[indexPath.section].value[indexPath.row]
        
        switch  object.operationTypeID {
            
        case InternetBankingOperationType.UtilityPayment.rawValue:
            let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateViewController") as! TemplateViewController
            vc.imageUtility = object.imageName ?? ""
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "details_of_operation")
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.InternalOperation.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.InternalOperation.rawValue
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.CliringTransfer.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
            let object = payments[indexPath.section].value[indexPath.row]
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
            vc.confirmType = object.confirmTypeID ?? 0
            vc.currencyCurrentID = object.currencyID ?? 0
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.GrossTransfer.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.GrossTransfer.rawValue
            vc.currencyCurrentID = object.currencyID ?? 0
            vc.confirmType = object.confirmTypeID ?? 0
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.SwiftTransfer.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemaplateSwiftViewController") as! TemaplateSwiftViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.SwiftTransfer.rawValue
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.Conversion.rawValue:
            let storyboard = UIStoryboard(name: "Conversion", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateCurreciesViewController") as! TemplateCurreciesViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.Conversion.rawValue
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
            
        case InternetBankingOperationType.LoanPayment.rawValue:
            let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CreditDetailViewController") as! CreditDetailViewController
            vc.operationID = object.operationID ?? 0
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.paymentType = InternetBankingOperationType.LoanPayment.rawValue
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        case InternetBankingOperationType.MoneyTransfer.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "XFincaDetailOperationViewController") as! XFincaDetailOperationViewController
            vc.operationID = object.operationID ?? 0
            vc.isNeedOperationDetail = true
            vc.delegate = self
            self.navigationController?.pushViewController(vc, animated: true)
        default:
            print("")
        }
    }
}

extension PaymentHistoryViewController{
    
    //запрос на список историй операций
    func  getHistoryOfPayments(operationType: String?, operationState: String?, dateFrom: String?, dateTo: String?, filter: String?, page: Int, pageSize: Int, sortField: String, sortType: Int) {
        showLoading()
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        let getOperations:Observable<ListContractModelExecuteResult> = managerApi
            .getOperations(operationType: operationType, operationState: operationState, dateFrom: dateFrom, dateTo: dateTo, filter: filter, page: page, pageSize: pageSize, sortField: sortField, sortType: sortType)
        Observable
            .zip(currenciesObservable, getOperations)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] currencies, historyPayments in
                    guard let `self` = self else { return }
                    self.currencies = currencies
                   
                    self.fetchingMore = false
                    
                    if let paymentsArray = historyPayments.result{
                        var downloadPayments = [(key: String, value: [ContractModel])]()
                        downloadPayments.removeAll()
                        self.payments.removeAll()
                        self.historyOperationArray.removeAll()
                        self.historyOperationArray.append(contentsOf: paymentsArray)
                        
                        if !self.isFiltered{
                        
                        }
                
                        self.activityIndicator.startAnimating()
                        self.tableView.tableFooterView?.isHidden = true
                        self.historyOperationArray = self.accountsCurrencies(currency: self.currencies, ContractModel: paymentsArray)
                        self.historyOperationArrayNew.append(contentsOf: self.historyOperationArray )
                        
                        let historyOperations = self.sortingArrayOf(payments: self.historyOperationArrayNew)
                        
                        let groupedPayments = Dictionary(grouping: historyOperations, by: { (element) -> Date in
                            return self.stringToDateFormatterForHistory(date: element.key)
                        })

                        let sortedKeys = groupedPayments.keys.sorted()
                        sortedKeys.forEach({ (key) in
                            let values = groupedPayments[key]
                            downloadPayments.append(contentsOf: values ?? [])
                        })

                        downloadPayments.reverse()
                        self.payments = downloadPayments
                        
                        self.totalItems = historyPayments.paginationInfo?.totalItems ?? 0
                        self.pageCount = historyPayments.paginationInfo?.page ?? 0
                        self.pageSize = historyPayments.paginationInfo?.pageSize ?? 0
                        
                        if self.payments.isEmpty{
                            self.tableView.isHidden = true
                            self.emptyStateStack.isHidden = false
                            if self.isFiltered{
                                self.emptyStateLabel.text = self.localizedText(key: "nothing_found_change_search_criteria")
                                self.emptyImage.image = UIImage(named: "empty_search_icn")
                            }
                            
                        }else{
                            self.tableView.isHidden = false
                            self.emptyStateStack.isHidden = true
                        }
                        
                        self.tableView.reloadData()
                    }
                    self.tableView.reloadData()
                self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
//    сортировка массива [ContractModel]
    func sortingArrayOf(payments: [ContractModel]) ->  [(key: String, value: [ContractModel])] {
        
        var sortedPaymentsUpdate = [(key: String, value: [ContractModel])]()
        sortedPaymentsUpdate.removeAll()
        for item in payments{
            item.date = self.dateFormater(date: item.date!)
        }
        var sortedPayments = [String:[ContractModel]]()
        for payment in payments {
            
            if sortedPayments.keys.contains(payment.date ?? "") {
                var paymentsByDate = sortedPayments[payment.date ?? ""]
                paymentsByDate?.append(payment)
                sortedPayments.updateValue(paymentsByDate ?? [], forKey: payment.date ?? "")
                
            } else {
                sortedPayments.updateValue([payment], forKey: payment.date ?? "")
            }
        }
        for payment in sortedPayments {
            let paymentForDate = payment.value
            
            let sortedArray = paymentForDate.sorted(by: {$0.date?.compare($1.date!) == .orderedDescending })
            sortedPayments.updateValue(sortedArray, forKey: payment.key)
        }
        
        sortedPaymentsUpdate = sortedPayments.sorted(by: {$0.value.first?.date?.compare(($1.value.first?.date)!) == .orderedDescending})
        
        return sortedPaymentsUpdate
    }
    // получение валют
    func accountsCurrencies(currency: [Currency], ContractModel:[ContractModel]) -> [ContractModel]{
        
        for item in ContractModel{
            for idCurrency in currency{
                if item.currencyID == idCurrency.currencyID{
                    item.currency = idCurrency
                }
            }
        }
        return ContractModel
    }
}
extension PaymentHistoryViewController: UISearchBarDelegate{
    //поиск по истории платежей
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            
            isSearching = false
            
            if payments.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
                self.emptyImage.image = UIImage(named: "empty_icn_white")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
            tableView.reloadData()
            
        }else{
            
            filteredArray = historyOperationArray.filter({ (payment: ContractModel ) -> Bool in
                return payment.confirmType?.lowercased().contains(searchText.lowercased()) ?? true
            })
            if filteredArray.isEmpty{
                self.tableView.isHidden = true
                self.emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.tableView.isHidden = false
                self.emptyStateStack.isHidden = true
                fetchingMore = true
            }
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    // обработка кнопки search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        filteredArray.removeAll()
        isSearching = false
        search.text = ""
        if payments.isEmpty{
            tableView.isHidden = true
            emptyStateStack.isHidden = false
            self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
            self.emptyImage.image = UIImage(named: "empty_icn_white")
        }else{
            tableView.isHidden = false
            emptyStateStack.isHidden = true
        }
        fetchingMore = false
        if self.isFiltered{
            if self.payments.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
        }
        tableView.reloadData()
        view.endEditing(true)
    }
    // кастомизация кнопки navigationabar
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        var image = ""
        if isFiltered{
            image = "selected"
            self.historyOperationArray.removeAll()
        }else{
            image = "options"
            self.historyOperationArray.removeAll()
        }
        button.setImage(UIImage(named: image), for: .normal)
        button.addTarget(self, action: #selector(filterButtonPressed), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
//    обработка кнопки фильтр
    @objc func filterButtonPressed() {
        performSegue(withIdentifier: "toFilterSegue", sender: self)
    }
}

