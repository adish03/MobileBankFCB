//
//  NotificationController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 21.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class NotificationController: BaseViewController {
    
    override func viewDidLoad() {
        title = "Уведомления"
        
        let image = UIImage(named: "Menu")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        let viewTest = UILabel()
        viewTest.text = "Cкоро"
        viewTest.font = UIFont.systemFont(ofSize: 22, weight: .bold)
        
        view.addSubview(viewTest)
        viewTest.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
}
