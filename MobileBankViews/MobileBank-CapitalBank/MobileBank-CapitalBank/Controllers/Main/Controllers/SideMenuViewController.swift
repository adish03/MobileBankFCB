//
//  SideMenuViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/24/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import Alamofire
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa
import Foundation

protocol UserCurrentDelegate{
    func ChangeUser(user: User)
}
struct menuButton{
    var idMenu: Int!
    var image: UIImageView!
    var view: UIView!
}

class SideMenuViewController: BaseViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var userAvatar: UIImageView!
    
    @IBOutlet weak var imageMain: UIImageView!
    @IBOutlet weak var imageFinance: UIImageView!
    @IBOutlet weak var imageHistory: UIImageView!
    @IBOutlet weak var imageTransactions: UIImageView!
    @IBOutlet weak var imageUtilities: UIImageView!
    @IBOutlet weak var imageTemplates: UIImageView!
    @IBOutlet weak var imageConverssions: UIImageView!
    @IBOutlet weak var imageOffices: UIImageView!
    @IBOutlet weak var imageLang: UIImageView!
    @IBOutlet weak var imageLogout: UIImageView!
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewFinance: UIView!
    @IBOutlet weak var viewHistory: UIView!
    @IBOutlet weak var viewTransactions: UIView!
    @IBOutlet weak var viewUtilities: UIView!
    @IBOutlet weak var viewTemplates: UIView!
    @IBOutlet weak var viewConverssions: UIView!
    @IBOutlet weak var viewOffices: UIView!
    @IBOutlet weak var viewLang: UIView!
    @IBOutlet weak var viewLogout: UIView!
    
    @IBOutlet weak var viewHelp: UIView!
    @IBOutlet weak var viewInfo: UIView!

    
    @IBOutlet weak var titleSub: UIView!

    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var sideMenuBackground: UIImageView!
    
    
    @IBOutlet weak var testItem: UIStackView!
    
    @IBOutlet weak var testitemtwo: UIStackView!
    
    var arrayOfUsers = [User]()
    
    @IBOutlet weak var helpImage: UIImageView!
    
    
    
    @IBOutlet weak var viewNotification: UIView!
    @IBOutlet weak var imageNatification: UIImageView!
    
    var user : User?
    var currentUserIndex = 0
    var delegate: UserCurrentDelegate?
    var selectedMenuNo = 1
    var currentUserLoginID = ""
    
    var main = UIStackItem("Главная", "home", .main)
    lazy var myFinanse = UIStackItem(localizedText(key: "my_finances"), "strongbox", .myFinanse)
    var myHistory = UIStackItem("История операции", "time-history", .myHistory)
    var transfer = UIStackItem("Переводы", "creditcard-outcome", .transfer)
    var paynentServes = UIStackItem("Оплата услуг", "wallet", .paynentServes)
    var selected = UIStackItem("Избраное", "note-text", .selected)
    var faluteObmen = UIStackItem("Обмен валют", "wallet", .faluteObmen)
    var podtershaAndPomosh = UIStackItem("Помощь и поддержка", "exchange", .podtershaAndPomosh)
    lazy var oProloshenii = UIStackItem(localizedText(key: "about_app"), "note-text", .oProloshenii)
    
    lazy var allItems: [UIStackItem] = {
        return [main, myFinanse, myHistory, transfer, paynentServes, selected, faluteObmen, podtershaAndPomosh, oProloshenii]
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.revealViewController().frontViewController.view.isUserInteractionEnabled = false
        self.revealViewController().view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.revealViewController().frontViewController.view.isUserInteractionEnabled = true
        sideMenuBackground.backgroundColor = UIColor(named: "circleButton")
    }
    
    func loadUserAvatar(user: User){
        self.getUserAvatar(loginID: user.loginID ?? "", userAvatar: userAvatar)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        testitemtwo.isHidden = true
        
        navigationController?.navigationBar.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        allowViewHidden()
        tableView.reloadData()
        arrowImage.image = UIImage(named: "small_arrow_down")
        tableView.isHidden = true
        if let user = UserDefaultsHelper.user{
            self.user = user
            nameLabel.text = self.user?.userdisplayname
            loadUserAvatar(user: user)
            currentUserLoginID = user.loginID ?? ""
        }
        self.arrayOfUsers.removeAll()
        if let users = UserDefaultsHelper.users{
            self.arrayOfUsers = users.filter({$0.loginID != currentUserLoginID})
            
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(menuStatus), name: NSNotification.Name(rawValue: "menuStatus"), object: nil)
        // Вызов нотификации при выборе раздела Финансы с Главной страницы
        NotificationCenter.default.addObserver(self, selector: #selector (menuFinance), name: NSNotification.Name(rawValue: "menuFinance"), object: nil)
        // Вызов нотификации при выборе раздела История с Главной страницы
        NotificationCenter.default.addObserver(self, selector: #selector (menuHistoryOperation), name: NSNotification.Name(rawValue: "menuHistoryOperation"), object: nil)
        // Вызов нотификации при выборе раздела Перевод с Главной страницы
        NotificationCenter.default.addObserver(self, selector: #selector (menuTransactions), name: NSNotification.Name(rawValue: "menuTransactions"), object: nil)
        // Вызов нотификации при выборе раздела Оплата с Главной страницы
        NotificationCenter.default.addObserver(self, selector: #selector (menuUtilities), name: NSNotification.Name(rawValue: "menuUtilities"), object: nil)
        // Вызов нотификации при выборе раздела Конвертация с Главной страницы
        NotificationCenter.default.addObserver(self, selector: #selector (menuConverssion), name: NSNotification.Name(rawValue: "menuConverssion"), object: nil)
        // Вызов нотификации при выборе раздела Шаблон с Главной страницы
        NotificationCenter.default.addObserver(self, selector: #selector (menuTemplate), name: NSNotification.Name(rawValue: "menuTemplate"), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector (toViewController), name: NSNotification.Name(rawValue: "LogOut"), object: nil)
        
        selectItems()
    }
    
    func selectItems() {
        
    }
    
    // обработка нотификации при выборе раздела Финансы
    @objc func menuFinance(notification: Notification) {
        selectedMenuNo = 2
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    // обработка нотификации при выборе раздела История
    @objc func menuHistoryOperation(notification: Notification) {
        selectedMenuNo = 3
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    // обработка нотификации при выборе раздела Перевод
    @objc func menuTransactions(notification: Notification) {
        selectedMenuNo = 4
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    // обработка нотификации при выборе раздела Оплата
    @objc func menuUtilities(notification: Notification) {
        selectedMenuNo = 5
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    // обработка нотификации при выборе раздела Конвертация
    @objc func menuConverssion(notification: Notification) {
        selectedMenuNo = 7
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    // обработка нотификации при выборе раздела QR
    @objc func menuTemplate(notification: Notification) {
        selectedMenuNo = 6
        selectMenu(selectedMenuNo: selectedMenuNo)
        
    }
    
    @IBAction func menuNotification(_ sender: Any) {
        selectedMenuNo = 11
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    
    @objc func menuStatus(notification: Notification) {
        selectedMenuNo = 10
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    // переход на  скрин авторизации при LogOut
    @objc func toViewController() {
        
        user?.access_token = ""
        UserDefaultsHelper.users = nil
        SessionManager.shared.user = nil
        UserDefaultsHelper.PIN_new = ""
        UserDefaultsHelper.pinIsEnabled = false
        UserDefaultsHelper.pinNewIsEnabled = false
        
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    
    // кнопка смены языка
    @IBAction func changeLanguageButton(_ sender: Any) {
        
        
    }
    
    
    // кнопка смены user
    @IBAction func userChangeButton(_ sender: Any) {
        
        testItem.isHidden = !testItem.isHidden
        testitemtwo.isHidden = !testItem.isHidden
        
        if testItem.isHidden{
            arrowImage.image = UIImage(named: "small_arrow_up")
        }else{
            arrowImage.image = UIImage(named: "small_arrow_down")
        }
    }
    // кнопка добавления user
    @IBAction func addNewUserButton(_ sender: Any) {
        
        //        showController(StoryBoard: "Login", ViewControllerID: "NavLogin")
    }
    // кнопка LogOut
    @IBAction func LogOut(_ sender: Any) {
        let alert = UIAlertController(title: localizedText(key: "are_you_sure"), message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            self.postPinRemove(applicationID: self.PhoneIdVendor)
        }
        let actionCancel = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel)
        alert.addAction(action)
        alert.addAction(actionCancel)
        present(alert, animated: true, completion: nil)
        
    }
    // удаление ПИН кода
    func  postPinRemove(applicationID: String){
        
        showLoading()
        managerApi
            .postPinRemove(ApplicationID: applicationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    guard let `self` = self else { return }
                    if let loginId = SessionManager.shared.user?.loginID{
                        if self.arrayOfUsers.count == 0{
                            self.user?.access_token = ""
                            SessionManager.shared.user = nil
                            UserDefaultsHelper.PIN_new = ""
                            UserDefaultsHelper.pinIsEnabled = false
                            UserDefaultsHelper.pinNewIsEnabled = false
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOut"), object: nil)
                        }else{
                            for user in self.arrayOfUsers{
                                if loginId == user.loginID{
                                    if let index = self.arrayOfUsers.index(where: { $0.loginID == loginId }) {
                                        print(index)
                                        self.arrayOfUsers.remove(at: index)
                                        UserDefaultsHelper.user = self.arrayOfUsers.last
                                        self.nameLabel.text = self.arrayOfUsers.last?.userdisplayname
                                    }
                                }
                            }
                            if let loginID = self.arrayOfUsers[self.currentUserIndex].loginID{
                                SessionManager.shared.current(loginID: loginID)
                                SessionManager.shared.updateUser(user: self.arrayOfUsers[self.currentUserIndex])
                                SessionManager.shared.setIndexPath(userIndex: self.currentUserIndex)
                                self.user = self.arrayOfUsers[self.currentUserIndex]
                                if let user =  SessionManager.shared.user{
                                    self.updateToken(user: user)
                                }
                            }
                        }
                        SessionManager.shared.logOutUser(loginID: loginId)
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    
    
    @IBAction func buttonToMainScreen(_ sender: UIButton) {
        selectedMenuNo = 1
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    @IBAction func buttonToMyFinance(_ sender: Any) {
        selectedMenuNo = 2
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    @IBAction func buttonToHistoryOperations(_ sender: UIButton) {
        selectedMenuNo = 3
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    @IBAction func buttonToTransactions(_ sender: Any) {
        selectedMenuNo = 4
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    @IBAction func buttonToUtilities(_ sender: UIButton) {
        selectedMenuNo = 5
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    @IBAction func buttonToTemplates(_ sender: Any) {
        selectedMenuNo = 6
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    @IBAction func buttonToCoverssion(_ sender: UIButton) {
        selectedMenuNo = 7
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    
    @IBAction func officeButton(_ sender: Any) {
        selectedMenuNo = 8
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    
    @IBAction func langButton(_ sender: Any) {
        selectedMenuNo = 9
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    
    @IBAction func buttonToStatus(_ sender: Any) {
        selectedMenuNo = 10
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    
    @IBAction func helpButton(_ sender: Any) {
        selectedMenuNo = 9
        selectMenu(selectedMenuNo: selectedMenuNo)
    }
    
    @IBAction func infoAppButton(_ sender: Any) {
        selectedMenuNo = 10
        selectMenu(selectedMenuNo: selectedMenuNo)
    }

    
    @IBAction func changePasswordButton(_ sender: Any) {
        
    }
    
    
    
    // кастомизация бокового меню, при выборе соответсвующего пункта меню
    func selectMenu(selectedMenuNo: Int){
        viewHelp.backgroundColor = .clear
        viewInfo.backgroundColor = .clear
        viewNotification.backgroundColor = .clear
        
        switch selectedMenuNo {
        case 1:
            imageMain.image = UIImage(named: "home-1")
            viewMain.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 2:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox-1")
            viewFinance.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 3:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history-1")
            viewHistory.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 4:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome-1")
            viewTransactions.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 5:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet-1")
            viewUtilities.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 6:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text-1")
            viewTemplates.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 7:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange-1")
            viewConverssions.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 8:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map-1")
            viewOffices.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 9:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
            viewHelp.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin-1")
//            viewLang.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
//            statusImage.image = UIImage(named: "star_no")
//            statusView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            viewNotification.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        case 10:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star-1")
//            statusView.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            viewInfo.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
            
        case 11:
            imageMain.image = UIImage(named: "home")
            viewMain.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageFinance.image = UIImage(named: "strongbox")
            viewFinance.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageHistory.image = UIImage(named: "time-history")
            viewHistory.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTransactions.image = UIImage(named: "creditcard-outcome")
            viewTransactions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageUtilities.image = UIImage(named: "wallet")
            viewUtilities.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageTemplates.image = UIImage(named: "note-text")
            viewTemplates.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageConverssions.image = UIImage(named: "exchange")
            viewConverssions.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            imageOffices.image = UIImage(named: "map")
            viewOffices.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            imageLang.image = UIImage(named: "language_cin")
//            viewLang.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            statusImage.image = UIImage(named: "star-1")
//            statusView.backgroundColor = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
        default:
            break
        }
    }
    
}
extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfUsers.count
    }
  
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ProfileTableViewCell
        cell.nameLabel.text = arrayOfUsers[indexPath.row].userdisplayname
        return cell
    }
    // обработка действия при выборе ячейки
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let loginID = arrayOfUsers[indexPath.row].loginID{
            
            SessionManager.shared.current(loginID: loginID)
            SessionManager.shared.updateUser(user: arrayOfUsers[indexPath.row])
            SessionManager.shared.setIndexPath(userIndex: indexPath.row)
            user = arrayOfUsers[indexPath.row]
            SessionManager.shared.user = user
            if let user =  SessionManager.shared.user{
                updateTokenForSideMenu(user: user)
            }
        }
        nameLabel.text = arrayOfUsers[indexPath.row].userdisplayname
        tableView.isHidden = true
        arrowImage.image = UIImage(named: "small_arrow_down")
        currentUserIndex = indexPath.row
    }
    //    права доступа
    func allowViewHidden(){
        if AllowHelper.shared.viewAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsViewAllowed"){
            viewTemplates.isHidden = false
        }else{
            viewTemplates.isHidden = true
        }
        
        if AllowHelper.shared.viewAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsViewAllowed"){
            viewUtilities.isHidden = false
        }else{
            viewUtilities.isHidden = true
        }
        
        if AllowHelper.shared.viewAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsViewAllowed"){
            viewConverssions.isHidden = false
        }else{
            viewConverssions.isHidden = true
        }
        if AllowHelper.shared.viewAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsViewAllowed") &&
            AllowHelper.shared.viewAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsViewAllowed"){
            
            viewHistory.isHidden = false
        }else{
            viewHistory.isHidden = true
        }
        
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed") &&
            AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed") &&
            AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed"){
            
            viewTransactions.isHidden = false
        }else{
            viewTransactions.isHidden = true
        }
    }
}


