//
//  UIStackItem.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 19.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

public enum StackEnum {
    case main
    case myFinanse
    case myHistory
    case transfer
    case paynentServes
    case selected
    case faluteObmen
    case oficeAndBankomate
    case falute
    case podtershaAndPomosh
    case oProloshenii
}

class UIStackItem: BaseView {
    
    lazy var imageTest: UIImageView = {
        let view = UIImageView()
        return view
    }()
    
    lazy var titleTest: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 15)
        return view
    }()
    
    var type: StackEnum
    
    init(_ title: String, _ image: String, _ type: StackEnum) {
        self.type = type

        super.init(frame: .zero)
        
        self.titleTest.text = title
        self.imageTest.image = UIImage(named: image)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func addSubViews() {
        addSubview(imageTest)
        imageTest.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(16)
            make.left.equalToSuperview().offset(16)
            make.bottom.equalToSuperview().offset(-16)
            make.size.equalTo(CGSize(width: 24, height: 24))
        }
        
        addSubview(titleTest)
        titleTest.snp.makeConstraints { make in
            make.centerY.equalToSuperview()
            make.left.equalTo(imageTest.snp.right).offset(16)
        }
    }
}
