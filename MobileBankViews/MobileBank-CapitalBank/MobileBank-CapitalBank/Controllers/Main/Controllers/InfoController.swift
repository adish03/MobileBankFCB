//
//  InfoController.swift
//  MobileBank-CapitalBank
//
//  Created by Eldar Akkozov on 19.09.2022.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import UIKit

class InfoController: BaseViewController {
    
    let leftOffset = 30
        
    var titleLabel: UILabel = {
        let view = UILabel()
        view.text = "ЗАО «ФИНКА Банк»"
        view.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        return view
    }()
    
    var addressTitle: UILabel = {
        let view = UILabel()
        view.text = "Адрес"
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return view
    }()
    
    var addressImage: UIImageView = {
        return UIImageView(image: UIImage(named: "map_pin-bank"))
    }()
    
    var numberOneImage: UIImageView = {
        return UIImageView(image: UIImage(named: "Phone Button"))
    }()
    
    var numberTwoImage: UIImageView = {
        return UIImageView(image: UIImage(named: "Phone Button"))
    }()
    
    lazy var numberThreeImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "whatsapp_red"))
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(tap)
        return view
    }()
    
    lazy var numberFourImage: UIImageView = {
        let view = UIImageView(image: UIImage(named: "mail_red"))
        return view
    }()
    
    @objc func tapFunction(sender:UITapGestureRecognizer) {
        let phoneNumber =  "996505440044" // you need to change this number
        let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
        if UIApplication.shared.canOpenURL(appURL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
            }
            else {
                UIApplication.shared.openURL(appURL)
            }
        }
    }
    
    //creditcard
    var address: UILabel = {
        let view = UILabel()
        view.text = "ул. Шопокова, 93/2, г. Бишкек, 720021 Кыргызстан"
        view.numberOfLines = 0
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        view.textColor = .gray
        return view
    }()
    
    var test: UILabel = {
        let view = UILabel()
        view.text = "Служба поддержки"
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return view
    }()
    
    var numberOne: UILabelView = {
        let view = UILabelView()
        view.text = "4400"
        view.textColor = UIColor(hexFromString: "#B30931")
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return view
    }()
    
    var numberTwo: UILabelView = {
        let view = UILabelView()
        view.text = "+996 312 440 440"
        view.textColor = UIColor(hexFromString: "#B30931")
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return view
    }()
    
    var numberThree: UILabelView = {
        let view = UILabelView()
        view.text = "finca@fincabank.kg"
        view.textColor = UIColor(hexFromString: "#B30931")
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return view
    }()
    
    var numberFour: UILabelView = {
        let view = UILabelView()
        view.text = "Написать в WhatsApp"
        view.textColor = UIColor(hexFromString: "#B30931")
        view.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        return view
    }()
    
    override func viewDidLoad() {
        title = "Помощь и поддержка"
        
        let image = UIImage(named: "Menu")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        
        
        view.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeArea.top).offset(30)
            make.left.equalToSuperview().offset(leftOffset)
        }
        
        view.addSubview(addressImage)
        addressImage.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(28)
            make.left.equalToSuperview().offset(leftOffset)
            make.height.width.equalTo(36)
        }
        
        view.addSubview(addressTitle)
        addressTitle.snp.makeConstraints { make in
            make.centerY.equalTo(addressImage)
            make.left.equalTo(addressImage.snp.right).offset(15)
        }
        
        view.addSubview(address)
        address.snp.makeConstraints { make in
            make.top.equalTo(addressImage.snp.bottom).offset(28)
            make.left.equalToSuperview().offset(leftOffset)
            make.right.equalToSuperview().offset(-16)
        }
        
        view.addSubview(test)
        test.snp.makeConstraints { make in
            make.top.equalTo(address.snp.bottom).offset(28)
            make.left.equalToSuperview().offset(leftOffset)
        }
        
        
        view.addSubview(numberOneImage)
        numberOneImage.snp.makeConstraints { make in
            make.top.equalTo(test.snp.bottom).offset(28)
            make.left.equalToSuperview().offset(leftOffset)
            make.width.height.equalTo(36)
        }
        
        view.addSubview(numberOne)
        numberOne.snp.makeConstraints { make in
            make.centerY.equalTo(numberOneImage)
            make.left.equalTo(numberOneImage.snp.right).offset(15)
        }
        
        numberOne.setClickListener {
            if let url = URL(string: "tel://4400") {
                 UIApplication.shared.open(url)
             }
        }
        
        view.addSubview(numberTwoImage)
        numberTwoImage.snp.makeConstraints { make in
            make.top.equalTo(numberOne.snp.bottom).offset(28)
            make.left.equalToSuperview().offset(leftOffset)
            make.width.height.equalTo(36)
        }
        
        view.addSubview(numberTwo)
        numberTwo.snp.makeConstraints { make in
            make.centerY.equalTo(numberTwoImage)
            make.left.equalTo(numberTwoImage.snp.right).offset(15)
        }
        
        numberTwo.setClickListener {
            if let url = URL(string: "tel://0312440440") {
                 UIApplication.shared.open(url)
             }
        }
        
//        numberFourImage
        
        
        
        view.addSubview(numberFourImage)
        numberFourImage.snp.makeConstraints { make in
            make.top.equalTo(numberTwo.snp.bottom).offset(28)
            make.left.equalToSuperview().offset(leftOffset)
            make.width.height.equalTo(36)
        }
        
        view.addSubview(numberThree)
        numberThree.snp.makeConstraints { make in
            make.centerY.equalTo(numberFourImage)
            make.left.equalTo(numberFourImage.snp.right).offset(15)
        }
        
        numberThree.setClickListener {
            let mailtoString = "mailto:finca@fincabank.kg".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            let mailtoUrl = URL(string: mailtoString!)!
            if UIApplication.shared.canOpenURL(mailtoUrl) {
                    UIApplication.shared.open(mailtoUrl, options: [:])
            }
        }
        
        view.addSubview(numberThreeImage)
        numberThreeImage.snp.makeConstraints { make in
            make.top.equalTo(numberThree.snp.bottom).offset(28)
            make.left.equalToSuperview().offset(leftOffset)
            make.width.height.equalTo(36)
        }
        
        view.addSubview(numberFour)
        numberFour.snp.makeConstraints { make in
            make.centerY.equalTo(numberThreeImage)
            make.left.equalTo(numberThreeImage.snp.right).offset(10)
        }
        
        numberFour.setClickListener {
            let phoneNumber =  "996505440044" // you need to change this number
            let appURL = URL(string: "https://api.whatsapp.com/send?phone=\(phoneNumber)")!
            if UIApplication.shared.canOpenURL(appURL) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(appURL, options: [:], completionHandler: nil)
                }
                else {
                    UIApplication.shared.openURL(appURL)
                }
            }
        }
    }
}

class UILabelView: UILabel {
    
    private lazy var listener: () -> Void = {}
    private lazy var viewListener: (UILabel) -> Void = {_ in }

    func setClickListener(_ listener: @escaping () -> Void) {
        self.listener = listener
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapFunction))
        isUserInteractionEnabled = true
        addGestureRecognizer(tap)
    }
    
    func setClickListener(_ listener: @escaping (UILabel) -> Void) {
        self.viewListener = listener
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapViewFunction))
        isUserInteractionEnabled = true
        addGestureRecognizer(tap)
    }
    
    @objc func tapFunction(sender: UITapGestureRecognizer) {
        listener()
    }
    
    @objc func tapViewFunction(sender: UITapGestureRecognizer) {
        viewListener(self)
    }
}
