//
//  FilterPaymentHistoryViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/21/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift

    // протокол для фильтрации
protocol FilterPropertyDelegate {
    func property(dateFrom: String?, dateTo: String?, operationType: String?, page: Int, operationState: String?, isFilter: Bool?)
    func resetFilter(isFilter: Bool?)
}
    //  класс для фильтрации
class FilterPaymentHistoryViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
   

    @IBOutlet weak var operationTypeTextField: UITextField!
    @IBOutlet weak var statusTypeTextField: UITextField!
    
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    
    var delegate: FilterPropertyDelegate?
    let pickerViewOperation = UIPickerView()
    let pickerViewStates = UIPickerView()
    var arrayOfStates = ["Все статусы"]
    var arrayOfOperations = ["Все операции"]
    var States = [expenseTypeModel]()
    var Operations = [expenseTypeModel]()
    let datePicker = UIDatePicker()
    
    var operationType = "null"
    var operationState = "null"
    var dateFrom = ""
    var dateTo = ""
    var isNeedData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = localizedText(key: "show_operations")
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        arrayOfStates = [localizedText(key: "all_statuses")]
        arrayOfOperations = [localizedText(key: "all_operations")]
        
        operationTypeTextField.delegate = self
        statusTypeTextField.delegate = self
        pickerViewOperation.delegate = self
        pickerViewStates.delegate = self
        fromDateTextField.delegate = self
        toDateTextField.delegate = self
        showDatePicker()
        getStatesAndOperations()
        
    }
    
    //    кнопка сброса параметров фильтрации
    @IBAction func resetFilterButton(_ sender: Any) {
        resetFilterData()
    }
    //    кнопка применения фильтрации
    @IBAction func applyFilterButton(_ sender: Any) {
        filteredData()
        navigationController?.popViewController(animated: true)
    }
    
    //    количество компонентов pickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //    количество строк компонентов pickerView
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return arrayOfOperations.count
        } else if pickerView.tag == 1 {
            return arrayOfStates.count
        }
        return 1
    }
    //    название компонентов pickerView
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return arrayOfOperations[row]
        } else if pickerView.tag == 1 {
            return arrayOfStates[row]
        }
            return ""
    }
    //   обработка нажатия  pickerView
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      
        if pickerView.tag == 0 {
            for operation in Operations{
                if operation.text == arrayOfOperations[row]{
                    operationType = "\(String(describing: operation.value ?? 0))"
                }
            }
            filteredData()
            
            return operationTypeTextField.text = arrayOfOperations[row]
            
        } else if pickerView.tag == 1 {
            for state in States{
                if state.text == arrayOfStates[row]{
                    operationState = "\(String(describing: state.value ?? 0))"
                }
            }
            filteredData()
            
            return statusTypeTextField.text = arrayOfStates[row]
        }
    }
//    проверка на фокус поля ввода
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == statusTypeTextField) {
            statusTypeTextField.text = arrayOfStates.first
            operationState = "null"
            
        }
        if (textField == operationTypeTextField) {
            operationTypeTextField.text = arrayOfOperations.first
            operationType = "null"
        }
        
        return true
    }

    

}
extension FilterPaymentHistoryViewController{
    // получение статусов и операций
    func getStatesAndOperations() {
        
        showLoading()
        let getOperationStates: Observable<[expenseTypeModel]> = managerApi
            .getOperationStates()
        
        let getOperationTypes: Observable<[expenseTypeModel]> = managerApi
            .getOperationTypes()
        
        Observable
            .zip(getOperationStates, getOperationTypes)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: { [weak self] (states, operations) in
                    guard let `self` = self else { return }
                    self.States = states
                    self.Operations = operations
                    for state in states{
                        self.arrayOfStates.append(state.text ?? "")
                    }
                    for operation in operations{
                        self.arrayOfOperations.append(operation.text ?? "")
                    }
                    self.pickerSettings()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    установка pickerView
    func pickerSettings(){
        
        operationTypeTextField.inputView = pickerViewOperation
        statusTypeTextField.inputView = pickerViewStates
        pickerViewOperation.tag = 0
        pickerViewStates.tag = 1
        pickerViewStates.selectRow(0, inComponent: 0, animated: true)
        if isNeedData{
            for state in States{
                if state.value == Int(self.operationState) ?? 0{
                    operationState = "\(String(describing: state.value ?? 0))"
                    statusTypeTextField.text = state.text
                }
            }
            for operation in Operations{
                if operation.value == Int(self.operationType) ?? 0{
                    operationType = "\(String(describing: operation.value ?? 0))"
                    operationTypeTextField.text = operation.text
                }
            }
        }

        
    }
    
    //  установка значений фильтрации
    func filteredData(){
        delegate?.property(dateFrom: dateFrom, dateTo: dateTo, operationType: operationType, page: 1, operationState: operationState, isFilter: true)
    }
    
    //  установка сброса фильтрации
    func resetFilterData(){
        fromDateTextField.text = ""
        dateFrom = ""
        
        toDateTextField.text = ""
        dateTo = ""
        
        operationTypeTextField.text = ""
        operationType = ""
        
        statusTypeTextField.text = ""
        operationState = ""
        
        delegate?.resetFilter(isFilter: false)
    }
    
    // выборка даты
    func showDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.maximumDate = Date()
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        fromDateTextField.inputAccessoryView = toolbar
        fromDateTextField.inputView = datePicker
        toDateTextField.inputAccessoryView = toolbar
        toDateTextField.inputView = datePicker
        
       
        if isNeedData{
            if self.dateTo != ""{
                toDateTextField.text = dateFormaterForFilter(date: self.dateTo)
            }else{
//                toDateTextField.text = dateToStringFormatter(date: Date())
                dateTo = formatterToServer.string(from: Date())
            }
            if self.dateFrom != ""{
                fromDateTextField.text = dateFormaterForFilter(date: self.dateFrom)
            }else{
                fromDateTextField.text = dateFrom
            }
        }
    }
    
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        
        if fromDateTextField.isFirstResponder {
            dateFrom = formatterToServer.string(from: datePicker.date)
            filteredData()
           
            fromDateTextField.text = formatter.string(from: datePicker.date)
        }
        if toDateTextField.isFirstResponder {
            dateTo = formatterToServer.string(from: datePicker.date)
            filteredData()
           
            toDateTextField.text = formatter.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    // проверка на фокус поля ввода
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fromDateTextField {
            datePicker.datePickerMode = .date
            
        }
        if textField == toDateTextField {
            datePicker.datePickerMode = .date
           
        }
    }
    
    // сравнение дат для фильтрации
    func compareTwoDates(dateFrom: String, dateTo: String, formatter: DateFormatter){
        let dateFrom: Date = stringToDateFormatterFiltered(date: dateFrom)
        let dateTo: Date = stringToDateFormatterFiltered(date: dateTo)
        if dateFrom.compare(dateTo) == .orderedDescending {
            toDateTextField.text = dateToStringFormatter(date: Date())
        }else{
            toDateTextField.text = formatter.string(from: datePicker.date)
        }
    }
}
