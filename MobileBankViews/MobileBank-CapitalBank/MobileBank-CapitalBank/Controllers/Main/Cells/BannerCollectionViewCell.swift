//
//  BannerCollectionViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/9/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

// класс для кастомизации ячейки для отображения баннеров
class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImage: UIImageView!
    @IBOutlet weak var viewImage: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
        bannerImage.layer.cornerRadius = 7
        bannerImage.clipsToBounds = true
    }
}
