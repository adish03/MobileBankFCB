//
//  PaymentHistoryTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/10/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
// класс для кастомизации ячейки таблицы
class PaymentHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var view: UIView!
    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeOperationLabel: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
         customImageCell(view: view, image: accountImage)
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.contentView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        } else {
            self.contentView.backgroundColor = .clear
        }
    }

}
