//
//  AccountsMainTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/25/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore

// класс для кастомизации ячейки таблицы
class AccountsMainTableViewCell: UITableViewCell {
 
    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var typeOperationLabel: UILabel!
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var viewImageBorder: UIView!
    
    var statusType: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //  кастомизация иконки ячейки
        customImageCell(view: viewImageBorder, image: accountImage)
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        backgroundColor = .clear
        viewImageBorder.backgroundColor = .clear
    }
}
