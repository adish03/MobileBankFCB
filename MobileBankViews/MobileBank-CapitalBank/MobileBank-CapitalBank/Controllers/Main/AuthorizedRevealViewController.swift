//
//  SWRevealViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 7/10/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation


class AuthorizedRevealViewController: SWRevealViewController {
    static var authorizedViewControllerPresented = false
    
    deinit {
        AuthorizedRevealViewController.authorizedViewControllerPresented = false
    }
    
    override func viewDidLoad() {
        AuthorizedRevealViewController.authorizedViewControllerPresented = true
        super.viewDidLoad()
    }
}
