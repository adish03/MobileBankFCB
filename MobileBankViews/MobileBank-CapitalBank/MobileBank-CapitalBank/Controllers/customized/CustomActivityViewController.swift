//
//  CustomActivityViewController.swift
//  MobileBank-CapitalBank
//
//  Created by admin on 19/10/21.
//  Copyright © 2021 Spalmalo. All rights reserved.
//

import Foundation
import UIKit
import MobileBankCore

class CustomActivityViewController: UIActivityViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            view.overrideUserInterfaceStyle = .dark
        }
        UINavigationBar.appearance().backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        let normalAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black]
        UIBarButtonItem.appearance().setTitleTextAttributes(normalAttributes, for: .normal)
        
        let disabledAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.gray]
        UIBarButtonItem.appearance().setTitleTextAttributes(disabledAttributes, for: .disabled)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if #available(iOS 13.0, *) {
            view.overrideUserInterfaceStyle = .dark
        }
        UINavigationBar.appearance().backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        let normalAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(normalAttributes, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}
