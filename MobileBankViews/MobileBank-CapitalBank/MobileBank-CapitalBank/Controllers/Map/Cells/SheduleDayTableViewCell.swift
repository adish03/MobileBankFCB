//
//  SheduleDayTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/12/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

class SheduleDayTableViewCell: UITableViewCell {

    @IBOutlet weak var dayLabel: UILabel!
    
    @IBOutlet weak var scheduleLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
