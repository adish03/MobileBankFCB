//
//  MapFilterModalViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/6/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift

protocol FilterOfficeDelegate {
    func filterOffices(isBankSelected: Bool, isAtmSelected: Bool, isPosSelected: Bool, isFiltered: Bool)
}

class MapFilterModalViewController: BaseViewController {

    @IBOutlet weak var filterView: UIView!
    
    @IBOutlet weak var selectBankImage: UIImageView!
    @IBOutlet weak var selectAtmImage: UIImageView!
    @IBOutlet weak var selectPosImage: UIImageView!
    
    var isBankSelected = false
    var isAtmSelected = false
    var isPosSelected = false
    var isFiltered = false
    
    var delegate: FilterOfficeDelegate?
    
    
    
    let direction: UISwipeGestureRecognizer.Direction = .down
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        stateFilter()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.35)
        view.isOpaque = false
        
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(CardInfoModalViewController.handleSwipe(gesture:)))
        gesture.direction = direction
        self.view?.addGestureRecognizer(gesture)

        
    }
    //    закрытие карты
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
        print("down gesture")
    }
    
   
    //    закрытие карты
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectBankButton(_ sender: Any) {
        if isBankSelected{
            isBankSelected = false
            selectBankImage.image = UIImage(named: "empty")
        }else {
            isBankSelected = true
            selectBankImage.image = UIImage(named: "Ok")
        }
        if !isBankSelected && !isAtmSelected && !isPosSelected{
            self.isFiltered = false
        }else{
            self.isFiltered = true
        }
         delegate?.filterOffices(isBankSelected: isBankSelected, isAtmSelected: isAtmSelected, isPosSelected: isPosSelected, isFiltered: true)
    }
    @IBAction func selectAtmButton(_ sender: Any) {
        if isAtmSelected{
            isAtmSelected = false
            selectAtmImage.image = UIImage(named: "empty")
        }else {
            isAtmSelected = true
            selectAtmImage.image = UIImage(named: "Ok")
        }
        if !isBankSelected && !isAtmSelected && !isPosSelected{
            self.isFiltered = false
        }else{
            self.isFiltered = true
        }
        delegate?.filterOffices(isBankSelected: isBankSelected, isAtmSelected: isAtmSelected, isPosSelected: isPosSelected, isFiltered: true)
    }
    @IBAction func selectPosButton(_ sender: Any) {
        if isPosSelected{
            isPosSelected = false
            selectPosImage.image = UIImage(named: "empty")
        }else {
            isPosSelected = true
            selectPosImage.image = UIImage(named: "Ok")
        }
        if !isBankSelected && !isAtmSelected && !isPosSelected{
            self.isFiltered = false
        }else{
            self.isFiltered = true
        }
         delegate?.filterOffices(isBankSelected: isBankSelected, isAtmSelected: isAtmSelected, isPosSelected: isPosSelected, isFiltered: self.isFiltered)
    }
//    состояние фильтра
    func stateFilter(){
        if isBankSelected{
            selectBankImage.image = UIImage(named: "Ok")
        }else {
            selectBankImage.image = UIImage(named: "empty")
        }
        if isAtmSelected{
            selectAtmImage.image = UIImage(named: "Ok")
        }else {
            selectAtmImage.image = UIImage(named: "empty")
        }
        if isPosSelected{
            selectPosImage.image = UIImage(named: "Ok")
        }else {
            selectPosImage.image = UIImage(named: "empty")
        }
    }
}
