//
//  OfficeDetailViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/6/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import MapKit
import CoreLocation

class OfficeDetailViewController: UIViewController {
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var PhoneLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewSchedule: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
     @IBOutlet weak var scheduleLabel: UILabel!
    
    var selectedOffice: OfficeDeviceModel!
    var scheduleDay = [ScheduleDevice]()
    
    
    var placeMark: CLPlacemark?
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 1000
    var previousLocation: CLLocation?
    
    let office = CustomPointAnnotation()
    let devicePos = CustomPointAnnotation()
    let deviceAtm = CustomPointAnnotation()
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
      
        self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor.clear

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.tintColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        self.navigationController?.navigationBar.backItem?.title = ""
        
        if selectedOffice.iDDevice == nil{
            addressLabel.text = selectedOffice.address
            PhoneLabel.text = selectedOffice.phone
            numberLabel.text = selectedOffice.name
            if scheduleDay.count == 0{
                scheduleLabel.isHidden = true
                heightConstraint.constant = 300
            }else{
                scheduleLabel.isHidden = false
                heightConstraint.constant = 450
            }
            
            office.title = selectedOffice.name
            office.subtitle = selectedOffice.name
            office.coordinate = CLLocationCoordinate2D(latitude: selectedOffice.latitude ?? 0, longitude: selectedOffice.longitude ?? 0)
            office.imageName = "map_pin-bank"
            self.mapView.addAnnotation(office)
            
        }else{
            
            if selectedOffice.iDDevice == 1{
                addressLabel.text = selectedOffice.adress
                viewPhone.isHidden = true
                viewSchedule.isHidden = true
                numberLabel.text = selectedOffice.nameDevice
                heightConstraint.constant = 300
                
                devicePos.title = selectedOffice.nameDevice
                devicePos.subtitle = selectedOffice.name
                devicePos.coordinate = CLLocationCoordinate2D(latitude: selectedOffice.latitude ?? 0, longitude: selectedOffice.longitude ?? 0)
                devicePos.imageName = "map_pin-POS"
                self.mapView.addAnnotation(devicePos)
            }else{
                addressLabel.text = selectedOffice.adress
                viewPhone.isHidden = true
                viewSchedule.isHidden = true
                numberLabel.text = selectedOffice.nameDevice
                heightConstraint.constant = 300
                
                deviceAtm.title = selectedOffice.nameDevice
                deviceAtm.subtitle = selectedOffice.name
                deviceAtm.coordinate = CLLocationCoordinate2D(latitude: selectedOffice.latitude ?? 0, longitude: selectedOffice.longitude ?? 0)
                deviceAtm.imageName = "map_pin-ATM"
                self.mapView.addAnnotation(deviceAtm)
            }
            
        }
        
        checkLocationServices()
        tableView.reloadData()
        
    }
    //    менеджер карты
    func setupLocationManager() {
        locationManager.delegate = self as CLLocationManagerDelegate
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    //    проверка сервиса локации
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkLocationAuthtorization()
        }else{
            //show alert
        }
    }
    //    текущий пользователь
    func centerViewOnUserLocation() {
         let coordinate = CLLocationCoordinate2D(latitude: selectedOffice.latitude ?? 0.0, longitude: selectedOffice.longitude ?? 0.0)
            let region = MKCoordinateRegion.init(center: coordinate, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        
    }
    //    проверку на авторизацию
    func checkLocationAuthtorization()  {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            startTrackingUserLocation()
        case .denied:
            //show alert turn on permission
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            //show alert
            break
        case .authorizedAlways:
            break
        }
    }
    //    трекинг от текущей локации
    func startTrackingUserLocation() {
        centerViewOnUserLocation()
       
        locationManager.startUpdatingLocation()
        previousLocation = getCenterLocation(for: mapView)
    }
    //    координаты текущей локации
    func getCenterLocation(for MapView: MKMapView) -> CLLocation {
        let latitude = MapView.centerCoordinate.latitude
        let longitude = MapView.centerCoordinate.longitude
        
        return CLLocation(latitude: latitude, longitude: longitude)
        
    }
//    обновление текущей точки
    func updateMapForCoordinate(coordinate: CLLocationCoordinate2D) {
        var center = coordinate;
        center.latitude -= self.mapView.region.span.latitudeDelta / 6.0;
        mapView.setCenter(center, animated: true);
    }
    
    
    @IBAction func phoneButton(_ sender: Any) {
        dialNumber(number: selectedOffice.phone ?? "")
    }
//    набор нрмера
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
        }
    }

}
extension OfficeDetailViewController: CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthtorization()
    }
}
extension OfficeDetailViewController: MKMapViewDelegate{
    //    цвета линий для маршрутов
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = .blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
//    изменение рисунка pin
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        
        let cpa = annotation as! CustomPointAnnotation
        anView?.image = UIImage(named:cpa.imageName)
        
        return anView
    }
}
extension OfficeDetailViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return scheduleDay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SheduleDayTableViewCell
        cell.dayLabel.text = ScheduleHelper.shared.weekDayHelper(day: scheduleDay[indexPath.row].day)
        cell.scheduleLabel.text = scheduleDay[indexPath.row].workTime
        
        return cell
    }
    
  
    
}
