import UIKit
import MobileBankCore
import RxSwift
import MapKit
import CoreLocation


class MapViewController: BaseViewController, FilterOfficeDelegate {
   
    @IBOutlet weak var filterButton: UIBarButtonItem!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    var officesLoaded = false
    var offices: [OfficeDeviceModel] = []
    var devices: [OfficeDeviceModel] = []
    var officesDevices: [OfficeDeviceModel] = []
    var resultSearchController = UISearchController(searchResultsController: nil)
    var placeMark: CLPlacemark?
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 12000
    var previousLocation: CLLocation?
    var filteredArray = [(key: String, value: [OfficeDeviceModel])]()
    var isSearching = false
    var searchFromMapView = true
    //
    var officesArray = [OfficeDeviceModel]()
    var officesArrayNew = [OfficeDeviceModel]()
    var officesDict = [(key: String, value: [OfficeDeviceModel])]()
    var isFromFirstController = false
    
    //
    var isBankSelected = false
    var isAtmSelected = false
    var isPosSelected = false
    var isFiltered = false
    
    var selectedOffice: OfficeDeviceModel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        barButtonCustomize()
       UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        emptyStateStack.isHidden = true
        
        if isFromFirstController{
            if  menu != nil{
                menu.image = UIImage(named: "ArrowBack")
                menu.target = self
                menu.action = #selector(closeButtonPressed)
            }
        }else{
            if  menu != nil{
                menu.target = self.revealViewController()
                menu.action = #selector(revealViewController().revealToggle(_:))
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
        }
        
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        addMapTrackingButton()
        segmentController.setTitle(localizedText(key: "map_title"), forSegmentAt: 0)
        segmentController.setTitle(localizedText(key: "list"), forSegmentAt: 1)
        mapView.delegate = self
        locationManager.delegate = self
        
        checkLocationServices()
        
        getOfficesAndDevices()
        
    }
    @objc func closeButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
//    менеджер карты
    func setupLocationManager() {
        locationManager.delegate = self as CLLocationManagerDelegate
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
//    проверка сервиса локации
    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkLocationAuthtorization()
        }else{
            //show alert
        }
    }
//    текущий пользователь
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate{
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
        }
        
    }
//    проверку на авторизацию
    func checkLocationAuthtorization()  {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            starttrackingUserLocation()
        case .denied:
            //show alert turn on permission
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            //show alert
            break
        case .authorizedAlways:
            break
        }
    }
//    трекинг от текущей локации
    func starttrackingUserLocation() {
        mapView.showsUserLocation = true
        centerViewOnUserLocation()
        locationManager.startUpdatingLocation()
        previousLocation = getCenterLocation(for: mapView)
    }
//    координаты текущей локации
    func getCenterLocation(for MapView: MKMapView) -> CLLocation {
        let latitude = MapView.centerCoordinate.latitude
        let longitude = MapView.centerCoordinate.longitude
        
        return CLLocation(latitude: latitude, longitude: longitude)
        
    }
//    фильтрация офисов
    func filterOffices(isBankSelected: Bool, isAtmSelected: Bool, isPosSelected: Bool, isFiltered: Bool) {
        self.isBankSelected = isBankSelected
        self.isAtmSelected = isAtmSelected
        self.isPosSelected = isPosSelected
        self.isFiltered = isFiltered
        if isBankSelected == false &&
            isAtmSelected == false &&
            isPosSelected == false{
            self.isFiltered = false
            barButtonCustomize()
        }else{
            self.isFiltered = true
            barButtonCustomize()
        }
        getOfficesAndDevices()
        print("let's see if it's heppening bank: \(isBankSelected)  atm: \(isAtmSelected) pos: \(isPosSelected)")
        
    }
    
    
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toFilterSegue"{
            let vc = segue.destination as! MapFilterModalViewController
            vc.delegate = self
            vc.isPosSelected = isPosSelected
            vc.isAtmSelected = isAtmSelected
            vc.isBankSelected = isBankSelected
            
        }
        if segue.identifier == "toDetailSegue"{
            let vc = segue.destination as! MapDetailOfficeViewController
            vc.selectedOffice = self.selectedOffice
            vc.scheduleDay = self.selectedOffice.schedules ?? []
        }
        
    }
    
  

    @IBAction func segmentController(_ sender: Any) {
        switch segmentController.selectedSegmentIndex {
        case 0:
            tableView.isHidden = true
            searchFromMapView = true
            
        case 1:
            tableView.isHidden = false
            searchFromMapView = false
            getOfficesAndDevices()
            
        default:
            print("no selected")
        }
    }
    
    //    запрос на получение офисов
    func getOfficesAndDevices(){
        if (!officesLoaded){
            showLoading()
        }
        
        let observable:Observable<([OfficeDeviceModel]?,[OfficeDeviceModel]?)>
        
        if officesLoaded{
            observable = Observable.just((self.offices,self.devices))
        }else{
            observable = Observable.zip(
                managerApi.getOffices().map{$0.result},
                managerApi.getDevices().map{$0.result})
        }
        
        observable
            .subscribe(
                onNext: {[weak self](offices, devices) in
                    guard let `self` = self else { return }
                    self.offices.removeAll()
                    self.devices.removeAll()
                    self.officesDevices.removeAll()
                    
                    self.offices = offices ?? []
                    self.devices = devices ?? []
                    self.officesLoaded = true
                    
                    if !self.isBankSelected && !self.isAtmSelected && !self.isPosSelected{
                        self.officesDevices.append(contentsOf: self.offices)
                        self.officesDevices.append(contentsOf: self.devices)
                    }
                    
                    if self.isBankSelected{
                        self.officesDevices.append(contentsOf: self.offices)
                    }
                    if self.isPosSelected{
                        let posDevices = self.devices.filter{ $0.iDDevice == 1 || $0.iDDevice == 3}
                        self.officesDevices.append(contentsOf: posDevices)
                    }
                    if self.isAtmSelected{
                        let atmDevices = self.devices.filter{ $0.iDDevice == 2 }
                        self.officesDevices.append(contentsOf: atmDevices)
                    }
                    
                    self.filteredAnnotations(officesDevices: self.officesDevices)
                    
                    var downloadOffices = [(key: String, value: [OfficeDeviceModel])]()
                    downloadOffices.removeAll()
                    self.officesDict.removeAll()
                    self.officesArray.removeAll()
                    self.officesArrayNew.removeAll()
                    self.officesArray.append(contentsOf:  self.officesDevices)
                    self.tableView.tableFooterView?.isHidden = true
                    self.officesArrayNew.append(contentsOf: self.officesArray )
                    
                    let arrayOffices = self.sortingArrayOf(offices: self.officesArrayNew)
                    
                    let groupedOffices = Dictionary(grouping: arrayOffices, by: { (element) -> String in
                        return  element.key
                    })
                    
                    let sortedKeys = groupedOffices.keys.sorted()
                    sortedKeys.forEach({ (key) in
                        let values = groupedOffices[key]
                        downloadOffices.append(contentsOf: values ?? [])
                    })
                    
                    downloadOffices.reverse()
                    self.officesDict = downloadOffices
                    
                    if self.officesDict.isEmpty{
                        self.emptyStateStack.isHidden = false
                    }else{
                        self.emptyStateStack.isHidden = true
                    }
                    
                    self.tableView.reloadData()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    
//    расчет дистанции от текущего положения
    private func userDistance(from point: MKPointAnnotation) -> Double? {
        guard let userLocation = mapView.userLocation.location else {
            return nil
        }
        let pointLocation = CLLocation(
            latitude:  point.coordinate.latitude,
            longitude: point.coordinate.longitude
        )
        return userLocation.distance(from: pointLocation)
    }
//    сортировка массива офисов и девайсов
    func sortingArrayOf(offices: [OfficeDeviceModel]) ->  [(key: String, value: [OfficeDeviceModel])] {
        
        var sortedOfficesUpdate = [(key: String, value: [OfficeDeviceModel])]()
        sortedOfficesUpdate.removeAll()
        var officesNear = [OfficeDeviceModel]()
        var officesLess1km = [OfficeDeviceModel]()
        var officesMore1km = [OfficeDeviceModel]()
        var sortedOffices = [String:[OfficeDeviceModel]]()
        for office in offices {
            
            if Int(office.distance ?? 0) <= 500{
                officesNear.append(office)
                sortedOffices.updateValue(officesNear, forKey: localizedText(key: "near_you"))
            }else if Int(office.distance ?? 0) >= 501 && Int(office.distance ?? 0) <= 1000{
                officesLess1km.append(office)
                sortedOffices.updateValue(officesLess1km, forKey: localizedText(key: "less_than_one_km"))
            }else if Int(office.distance ?? 0) >= 1001{
                officesMore1km.append(office)
                sortedOffices.updateValue(officesMore1km, forKey: localizedText(key: "more_than_one_km"))
            }
        }
        for office in sortedOffices {
            let officesByDistance = office.value
            
            let sortedArray = officesByDistance.sorted(by: {Int($0.distance ?? 0) < Int($1.distance ?? 0)})
            sortedOffices.updateValue(sortedArray, forKey: office.key)
        }
        
        sortedOfficesUpdate = sortedOffices.sorted(by: {Int($0.value.first?.distance ?? 0) > Int($1.value.first?.distance ?? 0)})
        
        return sortedOfficesUpdate
    }
//    возврат на текущую точку
    func addMapTrackingButton(){
        let image = UIImage(named: "my_position") as UIImage?
        let button   = UIButton(type: UIButton.ButtonType.custom) as UIButton
        let frameWidth = view.frame.width
       
        button.frame = CGRect(origin: CGPoint(x: frameWidth - 58, y: 40), size: CGSize(width: 50, height: 50))
        button.setImage(image, for: .normal)
        button.backgroundColor = .clear
        button.addTarget(self, action: #selector(MapViewController.centerMapOnUserButtonClicked), for:.touchUpInside)
        mapView.addSubview(button)
    }
    
//    возврат на текущую точку
    @objc func centerMapOnUserButtonClicked() {
        mapView.setUserTrackingMode(MKUserTrackingMode.follow, animated: true)
    }
    
    // кастомизация кнопки navigationabar
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        var image = ""
        if isFiltered{
            image = "selected"
//            self.historyOperationArray.removeAll()
        }else{
            image = "options"
//            self.historyOperationArray.removeAll()
        }
        button.setImage(UIImage(named: image), for: .normal)
        button.addTarget(self, action: #selector(filterButtonPressed), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    //    обработка кнопки фильтр
    @objc func filterButtonPressed() {
        performSegue(withIdentifier: "toFilterSegue", sender: self)
    }


}

extension MapViewController: CLLocationManagerDelegate{
//    авторизация для доступа к карте
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthtorization()
    }
}
extension MapViewController: MKMapViewDelegate{
//    цвета линий для маршрутов
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
        renderer.strokeColor = .blue
        renderer.lineWidth = 4.0
        return renderer
    }
//    выбор точки
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let coordinate = CLLocationCoordinate2D(latitude: 0000, longitude: 0000)
        let location = view.annotation
        self.placeMark = MKPlacemark(coordinate: location?.coordinate ?? coordinate)
        
        for location in officesDevices{
            if view.annotation?.title == location.name || view.annotation?.title == location.nameDevice{
               self.selectedOffice = location
            }
        }
        if selectedOffice != nil{
            performSegue(withIdentifier: "toDetailSegue", sender: self)
        }
       
    }
//    изменение рисунка pin
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        
        let cpa = annotation as! CustomPointAnnotation
        anView?.image = UIImage(named:cpa.imageName)
        
        return anView
    }
}
extension MapViewController{
    
}

extension MapViewController: UITableViewDelegate, UITableViewDataSource {
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        if isSearching{
            return filteredArray.count
        }else{
            return officesDict.count
        }
    }
    
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredArray[section].value.count
        }else{
            return officesDict[section].value.count
        }
    }
    //  изменение цвета Header и title
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height))
        headerView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        let firstPaymentInSection =  self.officesDict[section].key
        let label = UILabel(frame: CGRect(x: 16, y: 10, width: view.frame.size.width, height: 25))
        label.text = firstPaymentInSection
        label.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        label.font = UIFont.systemFont(ofSize: 15, weight: .medium)
        headerView.addSubview(label)
        return headerView
    }
    //  изменение высоты Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 46
    }
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MapTableViewCell
        var distance = ""
        
        if isSearching{
            let office = filteredArray[indexPath.section].value[indexPath.row]//filteredArray[indexPath.row]
            if office.iDDevice == nil{
                cell.nameLabel.text = office.name
                cell.addressLabel.text = office.address
                cell.officeImage.image = UIImage(named: "map_pin-bank")
            }else{
                cell.nameLabel.text = office.nameDevice
                cell.addressLabel.text = office.adress
                if office.iDDevice == 1{
                    cell.officeImage.image = UIImage(named: "map_pin-POS")
                    
                }else{
                    cell.officeImage.image = UIImage(named: "map_pin-ATM")
                }
                
            }
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: office.latitude ?? 0.0, longitude: office.longitude ?? 0.0)
           
            let dist: Int = Int(office.distance ?? 0.0)
            if dist > 999{
                distance = "\(dist/1000) \(localizedText(key: "kilometers_dot"))"
            }else{
                distance = "\(dist) \(localizedText(key: "meters_dot"))"
            }
            cell.distanceLabel.text = distance
            
        }else{
            let office = officesDict[indexPath.section].value[indexPath.row]//offices[indexPath.row]
            if office.iDDevice == nil{
                cell.nameLabel.text = office.name
                cell.addressLabel.text = office.address
                cell.officeImage.image = UIImage(named: "map_pin-bank")
            }else{
                cell.nameLabel.text = office.nameDevice
                cell.addressLabel.text = office.adress
                if office.iDDevice == 1{
                      cell.officeImage.image = UIImage(named: "map_pin-POS")
                }else{
                     cell.officeImage.image = UIImage(named: "map_pin-ATM")
                }
            }
            
            
            if office.locationCoordinate != nil{
                let dist: Int = Int(office.distance ?? 0.0)
                if dist > 999{
                    distance = "\(dist/1000) \(localizedText(key: "kilometers_dot"))"
                }else{
                    distance = "\(dist) \(localizedText(key: "meters_dot"))"
                }
            }else{
                distance = ""
            }
        
            cell.distanceLabel.text = distance
        }
        selectedCellCustomColor(cell: cell)
        return cell
    }
    // обработка действия при выборе ячейки
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if filteredArray.count != 0{
            let storyboard = UIStoryboard(name: "Map", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OfficeDetailViewController") as! OfficeDetailViewController
            vc.selectedOffice = filteredArray[indexPath.section].value[indexPath.row]
            vc.scheduleDay = filteredArray[indexPath.section].value[indexPath.row].schedules ?? []
            self.navigationController?.pushViewController(vc, animated: true)
            
        }else{
            let storyboard = UIStoryboard(name: "Map", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "OfficeDetailViewController") as! OfficeDetailViewController
            vc.selectedOffice = officesDict[indexPath.section].value[indexPath.row]
            vc.scheduleDay = officesDict[indexPath.section].value[indexPath.row].schedules ?? []
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension MapViewController: UISearchBarDelegate{
    //поиск
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            
            if !searchFromMapView{
                isSearching = false
                self.emptyStateStack.isHidden = true
                tableView.reloadData()
            }else{
                self.filteredAnnotations(officesDevices: self.officesDevices)
                self.emptyStateStack.isHidden = true
                tableView.reloadData()
            }
            
        }else{

            if !searchFromMapView{
                let filtered = officesDevices.filter({ (office: OfficeDeviceModel) -> Bool in
                    if office.name != nil{
                        return office.name?.lowercased().contains(searchText.lowercased()) ?? true
                    }else{
                        return office.address?.lowercased().contains(searchText.lowercased()) ?? true
                    }
                })
                var downloadOffices = [(key: String, value: [OfficeDeviceModel])]()
                downloadOffices.removeAll()
                
                let groupedOffices = Dictionary(grouping: sortingArrayOf(offices: filtered), by: { (element) -> String in
                    return  element.key
                })
                let sortedKeys = groupedOffices.keys.sorted()
                sortedKeys.forEach({ (key) in
                    let values = groupedOffices[key]
                    downloadOffices.append(contentsOf: values ?? [])
                })
                
                downloadOffices.reverse()
                filteredArray = downloadOffices
                if filteredArray.isEmpty{
                    self.emptyStateStack.isHidden = false
                    self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                    self.emptyImage.image = UIImage(named: "empty_search_icn")
                }else{
                    self.emptyStateStack.isHidden = true
                }
                
            }else{
                let filtered = officesDevices.filter({ (office: OfficeDeviceModel) -> Bool in
                    if office.name != nil{
                        return office.name?.lowercased().contains(searchText.lowercased()) ?? true
                    }else{
                        return office.nameDevice?.lowercased().contains(searchText.lowercased()) ?? true
                    }
                })
                
                  filteredAnnotations(officesDevices: filtered)
                
            }
            isSearching = true
            tableView.reloadData()

        }
        searchBar.setShowsCancelButton(true, animated: true)
        
    }
    // обработка кнопки search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        if !searchFromMapView{
            filteredArray.removeAll()
            if searchFromMapView == false{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
            if searchFromMapView == true{
                tableView.isHidden = true
                emptyStateStack.isHidden = true
            }
            tableView.reloadData()
            
        }else{
            self.filteredAnnotations(officesDevices: self.officesDevices)
        }
        
        isSearching = false
        self.search.text = ""
        tableView.reloadData()
        view.endEditing(true)
    }
//     фильтрация pin на карте
    func filteredAnnotations(officesDevices: [OfficeDeviceModel]){
        
        for officeDevice in officesDevices{
            let annotation = MKPointAnnotation()
            annotation.coordinate = CLLocationCoordinate2D(latitude: officeDevice.latitude ?? 0.0, longitude: officeDevice.longitude ?? 0.0)
            officeDevice.distance = self.userDistance(from: annotation) ?? 0.0
        }
        
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        
        for officeDevice in officesDevices{
            
            let office = CustomPointAnnotation()
            let devicePos = CustomPointAnnotation()
            let deviceAtm = CustomPointAnnotation()
            
            if officeDevice.iDDevice == nil{
                office.title = officeDevice.name
                office.subtitle = officeDevice.name
                office.coordinate = CLLocationCoordinate2D(latitude: officeDevice.latitude ?? 0, longitude: officeDevice.longitude ?? 0)
                office.imageName = "map_pin-bank"
                self.mapView.addAnnotation(office)
            }
            
            if officeDevice.iDDevice == 1 || officeDevice.iDDevice == 3{
                devicePos.title = officeDevice.name
                devicePos.subtitle = officeDevice.name
                devicePos.coordinate = CLLocationCoordinate2D(latitude: officeDevice.latitude ?? 0, longitude: officeDevice.longitude ?? 0)
                devicePos.imageName = "map_pin-POS"
                self.mapView.addAnnotation(devicePos)
            }
            if officeDevice.iDDevice == 2{
                deviceAtm.title = officeDevice.name
                deviceAtm.subtitle = officeDevice.name
                deviceAtm.coordinate = CLLocationCoordinate2D(latitude: officeDevice.latitude ?? 0, longitude: officeDevice.longitude ?? 0)
                deviceAtm.imageName = "map_pin-ATM"
                self.mapView.addAnnotation(deviceAtm)
            }
        }
    }
}

class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}
