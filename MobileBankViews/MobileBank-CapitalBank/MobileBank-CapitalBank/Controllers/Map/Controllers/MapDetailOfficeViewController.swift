//
//  MapDetailOfficeViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/6/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift


class MapDetailOfficeViewController: BaseViewController {

    @IBOutlet weak var addressLabel: UILabel!
   
    @IBOutlet weak var PhoneLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewSchedule: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    var scheduleDay = [ScheduleDevice]()
    
    var selectedOffice: OfficeDeviceModel!
    let direction: UISwipeGestureRecognizer.Direction = .down
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.35)
        view.isOpaque = false
        
        if selectedOffice.iDDevice == nil{
            addressLabel.text = selectedOffice.address
            PhoneLabel.text = selectedOffice.phone
            numberLabel.text = selectedOffice.name
            if scheduleDay.count == 0{
                scheduleLabel.isHidden = true
                heightConstraint.constant = 300
            }else{
                scheduleLabel.isHidden = false
                heightConstraint.constant = 450
            }
        }else{
           
            if selectedOffice.iDDevice == 1{
                addressLabel.text = selectedOffice.adress
                viewPhone.isHidden = true
                viewSchedule.isHidden = true
                numberLabel.text = selectedOffice.nameDevice
                heightConstraint.constant = 300
            }else{
                addressLabel.text = selectedOffice.adress
                viewPhone.isHidden = true
                viewSchedule.isHidden = true
                numberLabel.text = selectedOffice.nameDevice
                heightConstraint.constant = 300
            }
       
        }
        
        tableView.reloadData()
        let gesture = UISwipeGestureRecognizer(target: self, action: #selector(CardInfoModalViewController.handleSwipe(gesture:)))
        gesture.direction = direction
        self.view?.addGestureRecognizer(gesture)
        
        
    }
    //    закрытие карты
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
        print("down gesture")
    }
    
    
    //    закрытие карты
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func phoneButton(_ sender: Any) {
        dialNumber(number: selectedOffice.phone ?? "")
    }
//    набор номера
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
        }
    }
    
}

extension MapDetailOfficeViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return scheduleDay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SheduleDayTableViewCell
        cell.dayLabel.text = ScheduleHelper.shared.weekDayHelper(day: scheduleDay[indexPath.row].day)
        cell.scheduleLabel.text = scheduleDay[indexPath.row].workTime
        
        return cell
    }
    
    
}
