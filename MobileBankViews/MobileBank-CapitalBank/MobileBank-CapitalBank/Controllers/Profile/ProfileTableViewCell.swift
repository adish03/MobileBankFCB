//
//  ProfileTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 6/27/20.
//  Copyright © 2020 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore

// класс для кастомизации ячейки таблицы
class ProfileTableViewCell: UITableViewCell {
    @IBOutlet weak var imageAvatar: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // кастомизация ячейки при выборе
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            self.contentView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        } else {
            self.contentView.backgroundColor = .white
        }
    }
}
