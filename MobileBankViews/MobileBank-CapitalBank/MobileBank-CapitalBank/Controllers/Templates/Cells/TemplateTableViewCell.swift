//
//  TemplateTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/16/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import RxSwift

// класс для кастомизации ячейки таблицы
class TemplateTableViewCell: UITableViewCell {

    var imageDisposable:Disposable? = nil
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var templateNameLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var templateImage: UIImageView!
    
    @IBOutlet weak var autoPayImage: UIImageView!
    
    @IBOutlet weak var menuButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        customImageCell(view: view, image: templateImage)
    }

    // кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
