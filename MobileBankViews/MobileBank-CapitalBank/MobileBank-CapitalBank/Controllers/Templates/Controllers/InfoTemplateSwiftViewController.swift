//
//  InfoTemplateSwiftViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/6/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
// класс отображени информации по шаблону
class InfoTemplateSwiftViewController: BaseViewController {
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var OperatioTypeLabel: UILabel!
    @IBOutlet weak var AvtoPaymentLabel: UILabel!
    @IBOutlet weak var avtoPaymentDescription: UILabel!
    
    @IBOutlet weak var SenderFullNameLabel: UILabel!
    @IBOutlet weak var SenderAccountNoLabel: UILabel!
    @IBOutlet weak var PNumberLabel: UILabel!
    @IBOutlet weak var TransferSummLabel: UILabel!
    @IBOutlet weak var PaymentCodeLabel: UILabel!
    @IBOutlet weak var TarifLabel: UILabel!
    @IBOutlet weak var PaymentCommentLabel: UILabel!
    @IBOutlet weak var PaymentCodeDescriptionLabel: UILabel!
    @IBOutlet weak var VOcodeLabel: UILabel!
    @IBOutlet weak var VOcodeDescriptionLabel: UILabel!
    
    @IBOutlet weak var ReceiverFullNameLabel: UILabel!
    @IBOutlet weak var ReceiverAccountNoLabel: UILabel!
    @IBOutlet weak var IntermediaryBankCountryLabel: UILabel!
    @IBOutlet weak var IntermediaryBankNameLabel: UILabel!
    @IBOutlet weak var AccountNoIntermediaryBankLabel: UILabel!
    @IBOutlet weak var ReceiverBIKIntermediaryBankLabel: UILabel!
    @IBOutlet weak var ReceiverBIKPayThruLabel: UILabel!
    
    @IBOutlet weak var PayThruCountryLabel: UILabel!
    @IBOutlet weak var PayThruCountryNameLabel: UILabel!
    @IBOutlet weak var AccountNoPayThruBankLabel: UILabel!
    @IBOutlet weak var viewIntermediaryBank: UIView!
    @IBOutlet weak var voCodeStackView: UIStackView!
    
    var template: ContractModelTemplate!
    var submitModel: SubmitForPayModel!
    var swiftTransferModel: SwiftTransferModel!
    var navigationTitle = ""
    var operationTypeID = 0
    var operationID = 0
    var scheduleDescription = ""
    var expenseTypes: [expenseTypeModel]!
    var voCodes: [ReferenceItemModel]!
    var voCode: ReferenceItemModel!
    var voCodeID: String!
    var refOpers: [ReferenceItemModel]!
    var currencies = [Currency]()

    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        if navigationTitle == ""{
            navigationTitle = localizedText(key: "operation_status")
        }else{
            navigationItem.title = navigationTitle
        }
        
        if let expenseTypes = UserDefaultsHelper.expenseTypes {
            self.expenseTypes = expenseTypes
        }
        if let currencs =  UserDefaultsHelper.currencies{
            currencies = currencs
        }
        if let refOpers = UserDefaultsHelper.refOpers {
            self.refOpers = refOpers
        }
        if let voCodes = UserDefaultsHelper.voCodes {
            self.voCodes = voCodes
        }
        
        if template.isSchedule ?? false{
            avtoPaymentDescription.isHidden = false
            avtoPaymentDescription.text = ScheduleHelper.shared.scheduleDescriptionHelper(
                StartDate: template.schedule?.startDate,
                EndDate: template.schedule?.endDate,
                RecurringTypeID: template.schedule?.recurringTypeID,
                ProcessDay: template.schedule?.processDay)
        }
        NameLabel.text = template.name
        OperatioTypeLabel.text = template.operationType
        AvtoPaymentLabel.text = translator(isSchedule: template.isSchedule ?? false)
        
        getInfoOperationTransactions(operationID: template.operationID ?? 0)

    }
    //    обработка кнопки Редактировать шаблон
    @IBAction func editButton(_ sender: Any) {
        
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SWIFTViewController") as! SWIFTViewController
            vc.isEditTemplate = true
            vc.template = template
            vc.navigationTitle = localizedText(key: "edit_template")
            self.navigationController?.pushViewController(vc, animated: true)
    }
    //    обработка кнопки выполнения операции
    @IBAction func operationButton(_ sender: Any) {
        
        performSegue(withIdentifier: "toSwiftSegue", sender: self)
    }
    //    перевод состояния планировщика
    func translator(isSchedule: Bool) -> String{
        if template.isSchedule ?? false{
            return localizedText(key: "scheduled")
        }else{
            return localizedText(key: "not_planned")
        }
    }
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toSwiftSegue"{
            let vc = segue.destination as! SWIFTViewController
            vc.template = template
            vc.isRepeatPay = true
            vc.navigationTitle = localizedText(key: "operation_by_template")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
    }
    //    получение инфо об операции
    func  getInfoOperationTransactions(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationSwift(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.swiftTransferModel = operation
                    if self.swiftTransferModel.paymentCode != ""{
                        for code in self.refOpers{
                            if self.swiftTransferModel.paymentCode == code.value{
                                self.PaymentCodeDescriptionLabel.text = code.text
                            }
                        }
                    }
                    if self.swiftTransferModel.codeVO == "" || self.swiftTransferModel.codeVO == nil{
                        self.voCodeStackView.isHidden = true
                    }else{
                        self.voCodeStackView.isHidden = false
                        for code in self.voCodes{
                            if self.swiftTransferModel.codeVO == code.value{
                                self.VOcodeDescriptionLabel.text = code.text
                            }
                        }
                    }
                    if self.swiftTransferModel.intermediaryBankBic == nil{
                        self.viewIntermediaryBank.isHidden = true
                    }
                    
                    let transferSumm = "\(String(format:"%.2f",self.swiftTransferModel.transferSum ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.swiftTransferModel.currencyID ?? 0))"
                    self.SenderFullNameLabel.text = SessionManager.shared.user?.userdisplayname
                    self.SenderAccountNoLabel.text = self.swiftTransferModel.accountNo
                    self.PNumberLabel.text = self.swiftTransferModel.numberPP
                    self.TransferSummLabel.text = transferSumm
                    self.PaymentCodeLabel.text = self.swiftTransferModel.paymentCode
                    
                    self.TarifLabel.text = "\((self.swiftTransferModel.expenseType ?? 1)) - \(String(describing: self.expenseTypes[(self.swiftTransferModel.expenseType ?? 1) - 1].text ?? ""))"
                    self.PaymentCommentLabel.text = self.swiftTransferModel.detailsOfPayment
                    self.VOcodeLabel.text = self.swiftTransferModel.codeVO
                
                    self.ReceiverBIKIntermediaryBankLabel.text = self.swiftTransferModel.intermediaryBankBic
                    self.ReceiverBIKPayThruLabel.text = self.swiftTransferModel.receiverBankBic
                    self.ReceiverFullNameLabel.text = self.swiftTransferModel.fullNameReceiver
                    self.ReceiverAccountNoLabel.text = self.swiftTransferModel.receiverAccountNo
                    self.IntermediaryBankCountryLabel.text = self.swiftTransferModel.intermediaryBankCountry
                    self.IntermediaryBankNameLabel.text = self.swiftTransferModel.intermediaryBank
                    self.AccountNoIntermediaryBankLabel.text = self.swiftTransferModel.intermediaryBankLoroAccountNo
                    self.PayThruCountryLabel.text = self.swiftTransferModel.receiverBankCountryCode
                    self.PayThruCountryNameLabel.text = self.swiftTransferModel.receiverBank
                    self.AccountNoPayThruBankLabel.text = self.swiftTransferModel.receiverBankLoroAccountNo
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }


}
