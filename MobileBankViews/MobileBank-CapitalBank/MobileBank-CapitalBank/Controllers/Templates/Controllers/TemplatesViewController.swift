//
//  TemplatesViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/16/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa
import Alamofire
import Kingfisher

// класс Список Шаблонов
class TemplatesViewController: BaseViewController {

    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!

    var isSearching = false
    
    var filteredArray = [ContractModelTemplate]()
    var templates = [ContractModelTemplate]()
    var refreshControl:UIRefreshControl!
    var text = ""
    var fetchingMore = false
    var page = 1
    var totalItems = 0
    var pageSize = 0
    var pageCount = 0
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        navigationItem.title = localizedText(key: "my_templates")
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        tableView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        hideKeyboardWhenTappedAround()
        emptyStateStack.isHidden = true

        if  menu != nil{
            menu.target = self.revealViewController()
            menu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
       
        getTemplates(name: nil , operationType: nil , page: page, pageSize: 10, sortField: nil , sortType: 0)
    }
   
    @IBAction func addNewTemplateButton(_ sender: Any) {
        
    }
    // проверка при скроллинге до конца страницы
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height{
            if !fetchingMore{
                fetchingMore = true
                page += 1
                
                if !(totalItems < pageCount * pageSize){
                    self.tableView.tableFooterView?.isHidden = false
                    getTemplates(name: nil , operationType: nil , page: self.page, pageSize: 10, sortField: nil , sortType: 0)
                }
            }
        }
    }
}
   // Создание таблицы Шаблонов
extension TemplatesViewController: UITableViewDataSource, UITableViewDelegate{
   
    // количество секций таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredArray.count
        }else{
            return  templates.count
        }
    }
    // количество строк таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
     
        return  1
    }
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TemplateTableViewCell
        
        if isSearching{
            cell.templateNameLabel.text = filteredArray[indexPath.row].name
            cell.titleLabel.text = filteredArray[indexPath.row].description
            cell.menuButton.addTarget(self, action: #selector(TemplatesViewController.oneTapped(_:)), for: .touchUpInside)
            cell.menuButton.tag = indexPath.row
            if filteredArray[indexPath.row].isSchedule ?? false{
                cell.autoPayImage.isHidden = false
            }else{
                cell.autoPayImage.isHidden = true
            }
            self.getImageMyTemplates(image: self.filteredArray[indexPath.row].imageName ?? "", imageView: cell.templateImage, cell: cell)
        }else{
            cell.templateNameLabel.text = templates[indexPath.row].name
            cell.titleLabel.text = templates[indexPath.row].description
            cell.menuButton.addTarget(self, action: #selector(TemplatesViewController.oneTapped(_:)), for: .touchUpInside)
            cell.menuButton.tag = indexPath.row
            if templates[indexPath.row].isSchedule ?? false{
                cell.autoPayImage.isHidden = false
            }else{
                cell.autoPayImage.isHidden = true
            }
            self.getImageMyTemplates(image: self.templates[indexPath.row].imageName ?? "", imageView: cell.templateImage, cell: cell)
            
        }
        selectedCellCustomColor(cell: cell)
        return cell
    }
    // переход при выборе шаблона на "Подробную информацию"
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if self.templates[indexPath.row].operationTypeID == InternetBankingOperationType.SwiftTransfer.rawValue{
            
            let storyboard = UIStoryboard(name: "Templates", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "InfoTemplateSwiftViewController") as! InfoTemplateSwiftViewController
            vc.template = self.templates[indexPath.row]
            vc.navigationTitle = self.localizedText(key: "detail_info")
            self.navigationController?.pushViewController(vc, animated: true)
        }else if self.templates[indexPath.row].operationTypeID == InternetBankingOperationType.LoanPayment.rawValue {
            let storyboard = UIStoryboard(name: "Templates", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "InfoTemplateLoanViewController") as! InfoTemplateLoanViewController
            vc.template = self.templates[indexPath.row]
            vc.navigationTitle = self.localizedText(key: "detail_info")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard(name: "Templates", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "InfoTemplateViewController") as! InfoTemplateViewController
            vc.operationTypeID = self.templates[indexPath.row].operationTypeID ?? 0
            vc.template = self.templates[indexPath.row]
            vc.navigationTitle = self.localizedText(key: "detail_info")
            self.navigationController?.pushViewController(vc, animated: true)
        }
//        toInfoTemplateCreditSegue
    }
    // обработка нажатия кнопки на ячейке
    @objc func oneTapped(_ sender: UIButton) {
        
        let buttonTag = sender.tag
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionInfo  = UIAlertAction(title:  self.localizedText(key: "detail_info"), style: .default) { (action) in
            if self.templates[buttonTag].operationTypeID == InternetBankingOperationType.SwiftTransfer.rawValue{
                let storyboard = UIStoryboard(name: "Templates", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "InfoTemplateSwiftViewController") as! InfoTemplateSwiftViewController
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "detail_info")
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if self.templates[buttonTag].operationTypeID == InternetBankingOperationType.LoanPayment.rawValue {
                
                let storyboard = UIStoryboard(name: "Templates", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "InfoTemplateLoanViewController") as! InfoTemplateLoanViewController
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "detail_info")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                let storyboard = UIStoryboard(name: "Templates", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "InfoTemplateViewController") as! InfoTemplateViewController
                vc.operationTypeID = self.templates[buttonTag].operationTypeID ?? 0
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "detail_info")
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        
        let actionEdit = UIAlertAction(title: localizedText(key: "edit_template"), style: .default) { (action) in
            
            switch self.templates[buttonTag].operationTypeID {
                
            case InternetBankingOperationType.Conversion.rawValue:
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SWIFTViewController") as! SWIFTViewController
                vc.isEditTemplate = true
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "edit_template")
                self.navigationController?.pushViewController(vc, animated: true)
                
            case InternetBankingOperationType.UtilityPayment.rawValue:
                let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                vc.isEditTemplate = true
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "edit_template")
                self.navigationController?.pushViewController(vc, animated: true)
                
            case InternetBankingOperationType.InternalOperation.rawValue:
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
                vc.isEditTemplate = true
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "edit_template")
                self.navigationController?.pushViewController(vc, animated: true)
                
            case InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue:
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
                vc.isEditTemplate = true
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "edit_template")
                self.navigationController?.pushViewController(vc, animated: true)
                
            case InternetBankingOperationType.CliringTransfer.rawValue:
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
                vc.isEditTemplate = true
                vc.template = self.templates[buttonTag]
                vc.clearingGrossCurrency =  self.templates[buttonTag].currencyID ?? 0
                vc.navigationTitle = self.localizedText(key: "edit_template")
                self.navigationController?.pushViewController(vc, animated: true)
                
            case InternetBankingOperationType.GrossTransfer.rawValue:
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
                vc.isEditTemplate = true
                vc.template = self.templates[buttonTag]
                vc.clearingGrossCurrency =  self.templates[buttonTag].currencyID ?? 0
                vc.navigationTitle = self.localizedText(key: "edit_template")
                self.navigationController?.pushViewController(vc, animated: true)
                
            case InternetBankingOperationType.SwiftTransfer.rawValue:
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SWIFTViewController") as! SWIFTViewController
                vc.isEditTemplate = true
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "edit_template")
                self.navigationController?.pushViewController(vc, animated: true)
                
            case InternetBankingOperationType.LoanPayment.rawValue:

                let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LoanPlanePaymentViewController") as! LoanPlanePaymentViewController
                vc.isEditTemplate = true
                vc.template = self.templates[buttonTag]
                vc.navigationTitle = self.localizedText(key: "edit_template")
                self.navigationController?.pushViewController(vc, animated: true)
            default:
                print("not template")
            }
        }
        let actionRemove = UIAlertAction(title: self.localizedText(key: "delete_template"), style: .default) { (action) in
            if let operationID = self.templates[buttonTag].operationID{
                
                let alertController = UIAlertController(title: nil, message: "Вы действительно хотите удалить шаблон?", preferredStyle: .alert)
                
                let action = UIAlertAction(title: "OK", style: .default) { (action) in
                    self.templates.remove(at:buttonTag)
                    let indexPath = IndexPath(item: buttonTag, section: 0)
                    self.tableView.deleteRows(at: [indexPath], with: .fade)
                    ApiHelper.shared.deleteTemplate(operationID: "\(operationID)")
                    self.tableView.reloadData()
                }
                let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .default, handler: nil)
                alertController.addAction(actionCancel)
                alertController.addAction(action)
                self.present(alertController, animated: true, completion: nil)
            }
        }
        let actionCancel  = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel, handler: nil)
        
        if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsRemoveAllowed"){
            alertSheet.addAction(actionRemove)
        }
        if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsEditAllowed"){
            alertSheet.addAction(actionEdit)
        }
        alertSheet.addAction(actionInfo)
        alertSheet.addAction(actionCancel)
        
        present(alertSheet, animated: true, completion: nil)
        
    }
}

extension TemplatesViewController{
    // получение списка шаблонов
    func  getTemplates(name: String?, operationType: Int?, page: Int, pageSize: Int, sortField: String?, sortType: Int) {
        
        showLoading()
        
        managerApi
            .getTemplateOperations(name: name ?? "", operationType: operationType, page: page, pageSize: pageSize, sortField: sortField ?? "", sortType: sortType)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: { [weak self] templates in
                    guard let `self` = self else { return }
                    
                    if let templates = templates.result{
                       
                        self.templates.append(contentsOf: templates)
                        self.fetchingMore = false
                        self.tableView.tableFooterView?.isHidden = true
                        self.tableView.reloadData()
                    }
                    self.totalItems = templates.paginationInfo?.totalItems ?? 0
                    self.pageCount = templates.paginationInfo?.page ?? 0
                    self.pageSize = templates.paginationInfo?.pageSize ?? 0
                    
                    if self.templates.isEmpty{
                        self.tableView.isHidden = true
                        self.emptyStateStack.isHidden = false
                    }else{
                        self.tableView.isHidden = false
                        self.emptyStateStack.isHidden = true
                    }
                    
                    self.tableView.reloadData()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
        
    }
   
   
}

extension TemplatesViewController: UISearchBarDelegate{
//   настройка поиска
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            
            isSearching = false
            if templates.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
                self.emptyImage.image = UIImage(named: "empty_icn")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
            tableView.reloadData()
            
        }else{
           
            filteredArray = templates.filter({ (template: ContractModelTemplate ) -> Bool in
                return template.operationType?.lowercased().contains(searchText.lowercased()) ?? true
            })
            if filteredArray.isEmpty{
                self.tableView.isHidden = true
                self.emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
            }else{
                self.tableView.isHidden = false
                self.emptyStateStack.isHidden = true
            }
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
//    обработка кнопки отмены при поиске
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        filteredArray.removeAll()
        isSearching = false
        search.text = ""
        if templates.isEmpty{
            tableView.isHidden = true
            emptyStateStack.isHidden = false
            self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
            self.emptyImage.image = UIImage(named: "empty_icn")
        }else{
            tableView.isHidden = false
            emptyStateStack.isHidden = true
        }
        tableView.reloadData()
        view.endEditing(true)
    }
//   обработка кнопки поиск
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
