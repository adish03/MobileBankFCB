//
//  InfoTemplateViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/16/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
// класс отображени информации по шаблону
class InfoTemplateViewController: BaseViewController {
    
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var OperatioTypeLabel: UILabel!
    @IBOutlet weak var RekvizitLabel: UILabel!
    @IBOutlet weak var SummLabel: UILabel!
    @IBOutlet weak var DepositFromLabel: UILabel!
    @IBOutlet weak var AvtoPaymentLabel: UILabel!
    @IBOutlet weak var avtoPaymentDescription: UILabel!
    
    var template: ContractModelTemplate!
    var submitModel: SubmitForPayModel!
    var swiftTransferModel: SwiftTransferModel!
    var operation: ConversionOperationModel!
    var utilityOperation: UtilityPaymentOperationModel!
    var navigationTitle = ""
    var operationTypeID = 0
    var operationID = 0
    var scheduleDescription = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        
        if template.isSchedule ?? false{
        avtoPaymentDescription.isHidden = false
            avtoPaymentDescription.text = ScheduleHelper.shared.scheduleDescriptionHelper(
                StartDate: template.schedule?.startDate,
                EndDate: template.schedule?.endDate,
                RecurringTypeID: template.schedule?.recurringTypeID,
                ProcessDay: template.schedule?.processDay)
        }
        if operationTypeID == InternetBankingOperationType.Conversion.rawValue{
            getInfoOperationConversion(operationID: template.operationID ?? 0)}
        else{
            getOperationInfo(operationID: template.operationID ?? 0)
        }
        
        NameLabel.text = template.name
        OperatioTypeLabel.text = template.operationType
        RekvizitLabel.text = template.to
        SummLabel.text = "\(String(format:"%.2f", template.amount ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: template.currencyID ?? 0)) "
        DepositFromLabel.text = template.from
        AvtoPaymentLabel.text = translator(isSchedule: template.isSchedule ?? false)
        operationTypeID = template.operationTypeID ?? 0
    }
  

//    обработка кнопки Редактировать шаблон
    @IBAction func editButton(_ sender: Any) {
        if self.operationTypeID == InternetBankingOperationType.UtilityPayment.rawValue{
            let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            vc.isEditTemplate = true
            vc.template = template
            vc.navigationTitle = localizedText(key: "edit_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            vc.isEditTemplate = true
            vc.template = template
            vc.navigationTitle = localizedText(key: "edit_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            vc.isEditTemplate = true
            vc.template = template
            vc.navigationTitle = localizedText(key: "edit_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
            vc.clearingGrossCurrency = template.currencyID ?? 0
            vc.isEditTemplate = true
            vc.template = template
            vc.navigationTitle = localizedText(key: "edit_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if self.operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue{
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
            vc.clearingGrossCurrency = template.currencyID ?? 0
            vc.isEditTemplate = true
            vc.template = template
            vc.navigationTitle = localizedText(key: "edit_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if self.operationTypeID == InternetBankingOperationType.SwiftTransfer.rawValue{
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SWIFTViewController") as! SWIFTViewController
            vc.isEditTemplate = true
            vc.template = template
            vc.navigationTitle = localizedText(key: "edit_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
//    обработка кнопки выполнения операции
    @IBAction func operationButton(_ sender: Any) {
        
        if self.operationTypeID == InternetBankingOperationType.UtilityPayment.rawValue{
            performSegue(withIdentifier: "toPaymentSegue", sender: self)
        }
        if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
            performSegue(withIdentifier: "toTransactionsSegue", sender: self)
        }
        if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
            performSegue(withIdentifier: "toTransactionsSegue", sender: self)
        }
        if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
            performSegue(withIdentifier: "toClearingGrossSegue", sender: self)
            
        }
        if self.operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue{
             performSegue(withIdentifier: "toClearingGrossSegue", sender: self)
            
        }
        if self.operationTypeID == InternetBankingOperationType.SwiftTransfer.rawValue{
             performSegue(withIdentifier: "toSwiftSegue", sender: self)
        }
        if self.operationTypeID == InternetBankingOperationType.Conversion.rawValue{
           self.performSegue(withIdentifier: "toCurrencySegue", sender: self)
        }
        
        
        
    }
//    перевод состояния планировщика
    func translator(isSchedule: Bool) -> String{
        if template.isSchedule ?? false{
            return localizedText(key: "scheduled")
        }else{
            return localizedText(key: "not_planned")
        }
    }
//    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toPaymentSegue"{
            let vc = segue.destination as! PaymentViewController
            vc.operationModel = utilityOperation
            vc.template = template
            vc.isRepeatPay = true
            vc.navigationTitle = localizedText(key: "operation_by_template")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        if segue.identifier == "toTransactionsSegue"{
            let vc = segue.destination as! TransactionViewController
            vc.template = template
            vc.isRepeatPay = true
            vc.navigationTitle = localizedText(key: "operation_by_template")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        if segue.identifier == "toClearingGrossSegue"{
            let vc = segue.destination as! ClearingGrossViewController
            vc.template = template
            vc.clearingGrossCurrency = template.currencyID ?? 0
            vc.isRepeatPay = true
            vc.navigationTitle = localizedText(key: "operation_by_template")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        if segue.identifier == "toSwiftSegue"{
            let vc = segue.destination as! SWIFTViewController
            vc.template = template
            vc.isRepeatPay = true
            vc.navigationTitle = localizedText(key: "operation_by_template")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        if segue.identifier == "toCurrencySegue"{
            let vc = segue.destination as! CurrencyViewController
            vc.conversionOperationModel = operation
            vc.isRepeatPay = true
            vc.isRepeatFromStory = true
            vc.navigationTitle = self.localizedText(key: "create_operation_request")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        
    }
    //    получение инфо операции 
    func  getInfoOperationConversion(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationConversion(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.operation = operation
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func getOperationInfo(operationID: Int){
        showLoading()
        
        managerApi
            .getOperation(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    self?.utilityOperation = operation
                    if self?.utilityOperation.comment != nil {
                        self?.template.comment = self?.utilityOperation.comment
                    }
                    self?.hideLoading()
                }, onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
}
