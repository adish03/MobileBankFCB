//
//  KindsTemplatesViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/17/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper

//  класс виды шаблонов
class KindsTemplatesViewController: BaseViewController {

    @IBOutlet weak var utilitiesView: UIView!
    @IBOutlet weak var loanView: UIView!
    @IBOutlet weak var cliringGrossView: UIView!
    @IBOutlet weak var swiftView: UIView!
    @IBOutlet weak var internalTransactionView: UIView!
    @IBOutlet weak var internalView: UIView!
    
    var isCreateTemplate = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = localizedText(key: "create_template")
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        allowView()
        
        
    }
    
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSwiftTemplate"{
            let vc = segue.destination as! SWIFTViewController
            vc.navigationTitle = localizedText(key: "create_template")
            vc.isCreateTemplate = isCreateTemplate
        }
        if segue.identifier == "toClearingGrossTemplate"{
            let vc = segue.destination as! ClearingGrossViewController
            vc.navigationTitle = localizedText(key: "create_template")
            vc.isCreateTemplate = isCreateTemplate
        }
        if segue.identifier == "toTransactionTemplateFromInternal"{
            let vc = segue.destination as! TransactionViewController
            vc.navigationTitle = localizedText(key: "create_template")
            vc.isCreateTemplate = isCreateTemplate
            vc.operationTypeID = InternetBankingOperationType.InternalOperation.rawValue
        }
        if segue.identifier == "toTransactionTemplate"{
            let vc = segue.destination as! TransactionViewController
            vc.navigationTitle = localizedText(key: "create_template")
            vc.isCreateTemplate = isCreateTemplate
            vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
        }
        if segue.identifier == "toUtilitiesTemplate"{
            let vc = segue.destination as! PaymentViewController
            vc.navigationTitle = localizedText(key: "create_template")
            vc.isCreateTemplate = isCreateTemplate
            vc.operationTypeID = InternetBankingOperationType.UtilityPayment.rawValue
        }
        if segue.identifier == "toCreditTemplate"{
            let vc = segue.destination as! LoanPlanePaymentViewController
            vc.navigationTitle = localizedText(key: "create_template")
            vc.isCreateTemplate = isCreateTemplate
           
        }
       
       
    }
    
//    кастомизация картинки
    func customImageView(view: UIView, image: UIImageView){
        view.layer.borderWidth = 0.5
        view.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        view.layer.shadowRadius = 1.0
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds = false
        view.layer.shadowPath = UIBezierPath(roundedRect:view.bounds, cornerRadius:view.layer.cornerRadius).cgPath
        image.layer.cornerRadius = image.frame.width / 2
        image.clipsToBounds = true
    }
    
    //    права доступа
    func allowView(){
        if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsCreateTemplateAllowed"){
            utilitiesView.isHidden = false
        }else{
            utilitiesView.isHidden = true
        }
        if AllowHelper.shared.loanAllowOpertaion(nameOperation: "Loans.LoansSecurityOperations.IsCreateTemplateAllowed"){
            loanView.isHidden = false
        }else{
            loanView.isHidden = true
        }
        if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsCreateTemplateAllowed"){
            cliringGrossView.isHidden = false
        }else{
            cliringGrossView.isHidden = true
        }
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsCreateTemplateAllowed"){
            swiftView.isHidden = false
        }else{
            swiftView.isHidden = true
        }
        if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsCreateTemplateAllowed"){
            internalView.isHidden = false
            internalTransactionView.isHidden = false
        }else{
            internalView.isHidden = true
            internalTransactionView.isHidden = true
        }
        
    }


}
