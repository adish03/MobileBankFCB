//
//  InfoTemplateLoanViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/25/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper

class InfoTemplateLoanViewController: BaseViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var currencyIDLabel: UILabel!
    @IBOutlet weak var accountFromLabel: UILabel!
    @IBOutlet weak var accountToLabel: UILabel!
    @IBOutlet weak var operationTypeLabel: UILabel!
    
    var navigationTitle = ""
    var creditDetail: LoanPaymentModel!
    var operationTypeID = 0
    var paymentType = 0
    var template: ContractModelTemplate!
    var confirmType = 0
    var operationID = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        if navigationTitle == ""{
            navigationTitle = localizedText(key: "utilities_pay_status")
        }else{
            navigationItem.title = navigationTitle
            
        }
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        self.nameLabel.text = template.name
        self.amountLabel.text = "\(String(format:"%.2f",template.amount ?? 0))"
        self.currencyIDLabel.text = ValueHelper.shared.getCurrentCurrency(currnecyID: template.currencyID ?? 0)
        self.accountFromLabel.text = template.from
        self.accountToLabel.text = template.to
        self.operationTypeLabel.text = template.operationType
        
        
        getCreditID(operationID: template.operationID ?? 0)
        
    }
    
    //    обработка кнопки Редактировать шаблон
    @IBAction func editButton(_ sender: Any) {
 
        let storyboard = UIStoryboard(name: "MyFinance", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LoanPlanePaymentViewController") as! LoanPlanePaymentViewController
        vc.isEditTemplate = true
        vc.template = template
        vc.navigationTitle = localizedText(key: "edit_template")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //    обработка кнопки выполнения операции
    @IBAction func operationButton(_ sender: Any) {
        if  paymentType == InternetBankingLoanPaymentType.OnSchedule.rawValue{
            performSegue(withIdentifier: "toLoanSegue", sender: self)
        }else{
            performSegue(withIdentifier: "toLoanOperationsSegue", sender: self)
        }
    }
    
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toLoanSegue"{
                
                let vc = segue.destination as! LoanPlanePaymentViewController
                vc.template = template
                vc.isRepeatPay = true
                vc.navigationTitle = localizedText(key: "operation_by_template")
            }
            
            if segue.identifier == "toLoanOperationsSegue"{
                
                let vc = segue.destination as!  LoanPaymentOperationsViewController
                vc.template = template
                vc.creditDetail = creditDetail
                vc.isRepeatPay = true
                vc.navigationTitle = localizedText(key: "operation_by_template")
                
            }
        }
        

    //    получение creditID
    func  getCreditID(operationID: Int) {
        showLoading()

        managerApi
            .getInfoOperationCredit(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (creditDetail) in
                    guard let `self` = self else { return }
                    self.confirmType = creditDetail.confirmType ?? 0
                    self.getLoansDetails(creditID: creditDetail.creditID ?? 0)

            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    получение инфо об операции
    func  getLoansDetails(creditID: Int) {
        showLoading()

        managerApi
            .getLoansDetails(creditID: creditID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (creditDetail) in
                    guard let `self` = self else { return }
                    self.paymentType = creditDetail.paymentType ?? 0
                    self.creditDetail = creditDetail
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }


}
