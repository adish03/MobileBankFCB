////
////  ClearingGrossTemplateViewController.swift
////  MobileBank-CapitalBank
////
////  Created by Oleg Ten on 4/16/19.
////  Copyright © 2019 Spalmalo. All rights reserved.
////
//
//import UIKit
//import MobileBankCore
//import RxSwift
//
//// класс 
//class ClearingGrossTemplateViewController: BaseViewController {
//    @IBOutlet weak var nameOperation: UILabel!
//    
//    @IBOutlet weak var SenderFullNameLabel: UILabel!
//    @IBOutlet weak var PaymentTypeIDTextField: UITextField!
//    @IBOutlet weak var PNumberTextFields: UITextField!
//    @IBOutlet weak var paymentDateTextField: UITextField!
//    @IBOutlet weak var TransferSummTextField: UITextField!
//    @IBOutlet weak var PaymentCodeTextField: UITextField!
//    @IBOutlet weak var PaymentCodeDescriptionLabel: UILabel!//
//    @IBOutlet weak var PaymentCommentTextField: UITextField!
//    @IBOutlet weak var ReceiverFullNameTextField: UITextField!
//    @IBOutlet weak var ReceiverAccountNoTextField: UITextField!
//    @IBOutlet weak var ReceiverBIKTextField: UITextField!
//    @IBOutlet weak var BikDescriptionLabel: UILabel!
//    
//    @IBOutlet weak var payFrom_accountsLabel: UILabel!
//    @IBOutlet weak var choiceCard_accountsLabel: UILabel!
//    @IBOutlet weak var ccurrencySymbolLabel: UILabel!
//    
//    // views error
//    @IBOutlet weak var viewPNumberTextFields: UIView!
//    @IBOutlet weak var viewFromAccount: UIView!
//    @IBOutlet weak var viewTransferSummTextField: UIView!
//    @IBOutlet weak var viewPaymentCodeTextField: UIView!
//    @IBOutlet weak var viewPaymentCommentTextField: UIView!
//    @IBOutlet weak var viewReceiverFullNameTextField: UIView!
//    @IBOutlet weak var viewReceiverAccountNoTextField: UIView!
//    @IBOutlet weak var viewReceiverBIKTextField: UIView!
//    
//    //label error
//    @IBOutlet weak var labelErrorPNumber: UILabel!
//    @IBOutlet weak var labelErrorFromAccount: UILabel!
//    @IBOutlet weak var labelErrorTransferSumm: UILabel!
//    @IBOutlet weak var labelErrorPaymentCode: UILabel!
//    @IBOutlet weak var labelErrorPaymentComment: UILabel!
//    @IBOutlet weak var labelErrorReceiverFullName: UILabel!
//    @IBOutlet weak var labelErrorReceiverAccountNo: UILabel!
//    @IBOutlet weak var labelErrorReceiverBIK: UILabel!
//    
//    
//    @IBOutlet weak var switchPlanner: UISwitch!
//    @IBOutlet weak var viewPlanner: UIView!
//    @IBOutlet weak var periodTextField: UITextField!
//    
//    @IBOutlet weak var viewDateBegin: UIView!
//    @IBOutlet weak var viewDateFinish: UIView!
//    @IBOutlet weak var viewWeekDay: UIView!
//    @IBOutlet weak var viewDay: UIView!
//    @IBOutlet weak var viewDateOperation: UIView!
//    
//    @IBOutlet weak var dateBeginTextField: UITextField!
//    @IBOutlet weak var dateFinishTextField: UITextField!
//    
//    @IBOutlet weak var dayNumberTextField: UITextField!
//    @IBOutlet weak var dateOperationTextField: UITextField!
//    @IBOutlet weak var weekDayTextField: UITextField!
//    
//    var typeOperation = ""
//    
//    var clearingGrossOperationModel: ClearingGrossOperationModel!
//    var selectedDeposit: Deposit!
//    var navigationTitle = ""
//    var pickerView = UIPickerView()
//    var pickerViewDayOperation = UIPickerView()
//    var pickerViewWeekDay = UIPickerView()
//    
//    var periodList = ["OneTime".localized, "EachWeek".localized, "EachMonth".localized]
//    var daysList = ["1-го", "2-го", "3-го", "4-го", "5-го", "6-го", "7-го", "8-го", "9-го", "10-го"]
//    var weekList = ["monday".localized, "tuesday".localized, "wednesday".localized, "thursday".localized, "friday".localized, "saturday".localized, "sunday".localized]
//    let datePicker = UIDatePicker()
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        navigationItem.title = navigationTitle
//        view.backgroundColor = Constants.BACKGROUND_COLOR
//        nameOperation.text = typeOperation
//
//        
//        viewPlanner.isHidden = true
//        viewDateBegin.isHidden = true
//        viewDateFinish.isHidden = true
//        viewWeekDay.isHidden = true
//        viewDay.isHidden = true
//        
//        viewDateOperation.isHidden = true
//        dateBeginTextField.delegate = self
//        dateFinishTextField.delegate = self
//        dayNumberTextField.delegate = self
//        weekDayTextField.delegate = self
//        
//        pickerPeriodDate()
//        showDatePicker()
//        
//    }
//    
//    // кнопка включения планировщика
//    @IBAction func switchPlanner(_ sender: UISwitch) {
//        if sender.isOn{
//            viewPlanner.isHidden = false
//        }else{
//            viewPlanner.isHidden = true
//        }
//    }
//}
//// выбор периода планировщика
//extension ClearingGrossTemplateViewController: UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate{
//    
//    func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 1
//    }
//    //  установка дефолтных значений периода
//    func pickerPeriodDate() {
//        pickerView.delegate = self
//        pickerViewDayOperation.delegate = self
//        pickerViewWeekDay.delegate = self
//        periodTextField.inputView = pickerView
//        dayNumberTextField.inputView = pickerViewDayOperation
//        weekDayTextField.inputView = pickerViewWeekDay
//        pickerView.selectRow(0, inComponent: 0, animated: true)
//        pickerViewDayOperation.selectRow(0, inComponent: 0, animated: true)
//        pickerViewWeekDay.selectRow(0, inComponent: 0, animated: true)
//        periodTextField.text = periodList[0]
//        dayNumberTextField.text = daysList[0]
//        weekDayTextField.text = weekList[0]
//        viewDateBegin.isHidden = false
//        viewDateFinish.isHidden = false
//        viewWeekDay.isHidden = false
//        viewDay.isHidden = true
//        viewDateOperation.isHidden = true
//    }
//    // количество значений в "барабане"
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        var count = 0
//        if pickerView == self.pickerView {
//            count = periodList.count
//        } else if pickerView == self.pickerViewDayOperation{
//            count = daysList.count
//        } else if pickerView == self.pickerViewWeekDay{
//            count = weekList.count
//        }
//        
//        return count
//    }
//    //  название полей "барабана"
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        var list = ""
//        if pickerView == self.pickerView {
//            list = periodList[row]
//        } else if pickerView == self.pickerViewDayOperation{
//            list = daysList[row]
//        } else if pickerView == self.pickerViewWeekDay{
//            list = weekList[row]
//        }
//        return list
//    }
//    // скрытие ненужных полей
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        
//        if pickerView == self.pickerView {
//            periodTextField.text = periodList[row]
//            switch row {
//            case 0:
//                viewDateBegin.isHidden = false
//                viewDateFinish.isHidden = false
//                viewWeekDay.isHidden = false
//                viewDay.isHidden = true
//                viewDateOperation.isHidden = true
//            case 1:
//                viewDateBegin.isHidden = false
//                viewDateFinish.isHidden = false
//                viewWeekDay.isHidden = true
//                viewDay.isHidden = false
//                viewDateOperation.isHidden = true
//            case 2:
//                viewDateBegin.isHidden = true
//                viewDateFinish.isHidden = true
//                viewWeekDay.isHidden = true
//                viewDay.isHidden = true
//                viewDateOperation.isHidden = false
//            default:
//                viewDateBegin.isHidden = true
//                viewDateFinish.isHidden = true
//                viewWeekDay.isHidden = true
//                viewDay.isHidden = true
//                viewDateOperation.isHidden = true
//            }
//        } else if pickerView == self.pickerViewDayOperation{
//            dayNumberTextField.text = daysList[row]
//        } else if pickerView == self.pickerViewWeekDay{
//            weekDayTextField.text = weekList[row]
//        }
//        
//    }
//    // выборка даты
//    func showDatePicker(){
//        
//        datePicker.datePickerMode = .date
//        let toolbar = UIToolbar();
//        toolbar.sizeToFit()
//        let cancelButton = UIBarButtonItem(title: "Отмена", style: .plain, target: self, action: #selector(cancelDatePicker));
//        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//        let doneButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(doneDatePicker));
//        
//        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
//        
//        dateBeginTextField.inputAccessoryView = toolbar
//        dateBeginTextField.inputView = datePicker
//        dateFinishTextField.inputAccessoryView = toolbar
//        dateFinishTextField.inputView = datePicker
//        dateOperationTextField.inputAccessoryView = toolbar
//        dateOperationTextField.inputView = datePicker
//        
//    }
//    // обработка кнопки ГОТОВО
//    @objc func doneDatePicker(){
//        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd MMMM yyyy"///////////change date format
//        formatter.locale = Locale(identifier: "language".localized)
//        let formatterDayOnly = DateFormatter()
//        formatterDayOnly.dateFormat = "dd"///////////change date format
//        formatterDayOnly.locale = Locale(identifier: "language".localized)
//        
//        
//        if dateBeginTextField.isFirstResponder {
//            dateBeginTextField.text = formatter.string(from: datePicker.date)
//        }
//        if dateFinishTextField.isFirstResponder {
//            dateFinishTextField.text = formatter.string(from: datePicker.date)
//        }
//        if dateOperationTextField.isFirstResponder {
//            dateOperationTextField.text = formatter.string(from: datePicker.date)
//        }
//        
//        self.view.endEditing(true)
//    }
//    // обработка кнопки ОТМЕНА
//    @objc func cancelDatePicker(){
//        self.view.endEditing(true)
//    }
//    
//    // проверка на фокус поля ввода
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == dateBeginTextField {
//            datePicker.datePickerMode = .date
//        }
//        if textField == dateFinishTextField {
//            datePicker.datePickerMode = .date
//        }
//        if textField == dateOperationTextField {
//            datePicker.datePickerMode = .date
//        }
//    }
//}
//
