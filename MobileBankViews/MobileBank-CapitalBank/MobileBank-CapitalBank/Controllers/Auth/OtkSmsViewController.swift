//
//  OtkSmsViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 7/2/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift

class OtkSmsViewController: BaseViewController, UITextFieldDelegate {
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var smsTextField: UITextField!
    @IBOutlet weak var timeSmsLabel: UILabel!
    @IBOutlet weak var smsSendButtonOutlet: UIButton!
    @IBOutlet weak var eTokenTextField: UITextField!
    @IBOutlet weak var confirmButton: UIButton!
    
    
    var codeLengthSms = 6
    var codeLengthEtoken = 6
    var countdownTimer: Timer!
    var totalTime = 60
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
        smsTextField.delegate = self
        eTokenTextField.delegate = self
        
        if let time = UserDefaultsHelper.SmsCodeAuthExpires {
            totalTime = time
        }
        if let Length = UserDefaultsHelper.EtokenCodeLength {
            codeLengthEtoken = Length
        }
        if let Length = UserDefaultsHelper.SmsCodeLength {
            codeLengthSms = Length
        }
        
        startTimer()
        
        if let name = SessionManager.shared.user?.phone{
            phoneLabel.text = name
        }
        
    }
    
    @IBAction func sendSmsButton(_ sender: Any) {
        resendSms()
        if let time = UserDefaultsHelper.SmsCodeAuthExpires {
            totalTime = time
        }
        startTimer()
    }
    
    @IBAction func sendEtokenSmsButton(_ sender: Any) {
        if smsTextField.text != "" && eTokenTextField.text != ""{
            if let sms =  smsTextField.text, let etoken = eTokenTextField.text{
                updateToken(etoken_code: etoken, sms_code: sms)
            }
        }
    }
    //    проверка поля на начало ввода информации на кол-во символов
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        if textField.isEqual(smsTextField) {
            maxLength = codeLengthSms
        }else if textField.isEqual(eTokenTextField) {
            maxLength = codeLengthEtoken
        }
        return newLength <= maxLength
        
    }
}

extension OtkSmsViewController{
    
    
    //    проверка на наличие пин кода
    func  isPinExist(){
        
        showLoading()
        managerApi
            .getIsPinExist(ApplicationID: UIDevice.current.identifierForVendor!.uuidString)
            .subscribe(
                onNext: {[weak self] (pinIsTrue) in
                    guard let `self` = self else { return }
                    let pinIsTrue = pinIsTrue
                    if pinIsTrue == "true"{
                        UserDefaultsHelper.pinIsEnabled = true
                    }else{
                        UserDefaultsHelper.pinIsEnabled = false
                    }
                    self.hideLoading()
                },
                onError: {[weak self](error) in
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
extension OtkSmsViewController{
    // таймер обратного отсчета
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        timeSmsLabel.isHidden = false
        smsSendButtonOutlet.isHidden = true
    }
    // таймер обратного отсчета
    @objc func updateTime() {
        timeSmsLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
            
        }
    }
    // таймер обратного отсчета
    func endTimer() {
        
        timeSmsLabel.isHidden = true
        smsSendButtonOutlet.isHidden = false
        countdownTimer.invalidate()
        //        isPinExist()
        
    }
    // таймер обратного отсчета формат даты
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        return LocalizeHelper.shared.addWord("\(String(format: "%02d", seconds))", localizedText(key: "send_sms_again_after"))
    }
}

extension OtkSmsViewController{
    //    обновление токена
    public func updateToken(etoken_code: String, sms_code: String?){
        showLoading()
        if let refreshToken = SessionManager.shared.refreshToken{
            managerApi
                .getUpdateTokenWithAdditionalAuth(grant_type: "refresh_token", refresh_token: refreshToken, command: "additional_auth", sms_code: sms_code, pin: etoken_code)
                .subscribe(
                    onNext: {[weak self](user) in
                        guard let `self` = self else { return }
                        if user.change_pwd_after_login == "True" || user.pwd_expiry_notify == "True"{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                            vc.isNeedChangePassword = true
                            vc.delegate = self as? ChangeOldPasswordProtocol
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            SessionManager.shared.updateUser(user: user)
                            SessionManager.shared.addNewUser(user: user)
                            SessionManager.shared.user = user
                            SessionManager.shared.current(loginID: user.loginID ?? "")
                            self.isPinExist()
                            ApiHelper.shared.getAllowedOperations()
                            self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                        }
                        self.hideLoading()
                    },
                    onError: {[weak self] error in
                        self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                        self?.smsTextField.text = ""
                        self?.eTokenTextField.text = ""
                        self?.hideLoading()
                })
                .disposed(by: disposeBag)
        }
    }
}
