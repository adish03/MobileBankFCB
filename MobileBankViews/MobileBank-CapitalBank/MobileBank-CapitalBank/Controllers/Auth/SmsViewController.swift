//
//  SmsViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/13/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift

class SmsViewController: BaseViewController {
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var smsTextField: UITextField!
    @IBOutlet weak var timeSmsLabel: UILabel!
    @IBOutlet weak var smsSendButtonOutlet: UIButton!
    
    
    var countdownTimer: Timer!
    var totalTime = 60
    var codeLength = 0
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        countdownTimer.invalidate()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = localizedText(key: "verification_code")
        hideKeyboardWhenTappedAround()

        if let time = UserDefaultsHelper.SmsCodeAuthExpires {
            totalTime = time
            print("pinNumberCount: \(totalTime)")
        }
        if let Length = UserDefaultsHelper.SmsCodeLength {
            codeLength = Length
            print("pinNumberCount: \(codeLength)")
        }
        
        smsTextField.defaultTextAttributes.updateValue(5.0, forKey: NSAttributedString.Key.kern)
        smsTextField.becomeFirstResponder()
        smsTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged) // make sure it is the desired textField
        
        startTimer()
   
        if let name = SessionManager.shared.user?.phone{
             phoneLabel.text = name
        }
       
    }
 
    
    
    @IBAction func sendSmsButton(_ sender: Any) {
        resendSms()
        if let time = UserDefaultsHelper.SmsCodeAuthExpires {
            totalTime = time
        }
        startTimer()
    }
}
extension SmsViewController{
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.count  == codeLength{
            
            if let SMS = textField.text{
                updateToken(sms_code: SMS)
            }
        }
    }
}

extension SmsViewController{
    public func updateToken(sms_code: String){
        showLoading()
        if let refreshToken = SessionManager.shared.refreshToken{
            managerApi
                .getUpdateTokenWithAdditionalAuth(grant_type: "refresh_token", refresh_token: refreshToken, command: "additional_auth", sms_code: sms_code, pin: nil)
                .subscribe(
                    onNext: {[weak self] (user) in
                        guard let `self` = self else { return }
                        
                        if user.change_pwd_after_login == "True" || user.pwd_expiry_notify == "True"{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                            vc.isNeedChangePassword = true
                            vc.delegate = self as? ChangeOldPasswordProtocol
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            
                            SessionManager.shared.updateUser(user: user)
                            SessionManager.shared.addNewUser(user: user)
                            SessionManager.shared.user = user
                            SessionManager.shared.current(loginID: user.loginID ?? "")
                            self.isPinExist()
                            ApiHelper.shared.getAllowedOperations()
                        }
                        self.hideLoading()
                    },
                    onError: {[weak self] error in
                        self?.showAlertController(ApiHelper.shared.errorHelperSMS(error: error))
                       
                        self?.smsTextField.text = ""
                        self?.hideLoading()
                })
                .disposed(by: disposeBag)
        }
    }
    //    проверка на наличие пин кода
    func  isPinExist(){
        
        showLoading()
        managerApi
            .getIsPinExist(ApplicationID: UIDevice.current.identifierForVendor!.uuidString)
            .subscribe(
                onNext: {[weak self] (pinIsTrue) in
                    guard let `self` = self else { return }
                    let pinIsTrue = pinIsTrue
                    if pinIsTrue == "true"{
                        UserDefaultsHelper.pinIsEnabled = true
                    }else{
                        UserDefaultsHelper.pinIsEnabled = false
                    }
                    self.hideLoading()
                    self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")

            },
                onError: {(error) in
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension SmsViewController{
    // таймер обратного отсчета
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        timeSmsLabel.isHidden = false
        smsSendButtonOutlet.isHidden = true
    }
    // таймер обратного отсчета
    @objc func updateTime() {
        timeSmsLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
            
        }
    }
    // таймер обратного отсчета
    func endTimer() {
        
        timeSmsLabel.isHidden = true
        smsSendButtonOutlet.isHidden = false
        countdownTimer.invalidate()

        //send sms to server for confirm
       
    }
    // таймер обратного отсчета формат даты
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
     
        return LocalizeHelper.shared.addWord("\(String(format: "%02d", seconds))", localizedText(key: "send_sms_again_after"))
    }

    
}


