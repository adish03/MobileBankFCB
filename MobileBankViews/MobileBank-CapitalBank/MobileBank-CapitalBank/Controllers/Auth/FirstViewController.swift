//
//  FirstViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/13/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import SafariServices

class FirstViewController: BaseViewController, SFSafariViewControllerDelegate {
    
    
    @IBOutlet weak var enterButton: RoundedButton!
    @IBOutlet weak var officeButton: UIButton!
    
    //@IBOutlet weak var TopInsetConstraint: NSLayoutConstraint!
   
    @IBOutlet weak var TopInset: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    
//    override var prefersStatusBarHidden: Bool{
//        return true
//    }
    
    @IBAction func openAccount(_ sender: Any){
        if let actualUrl = URL(string: "https://fincabank.kg/%d0%be%d0%bd%d0%bb%d0%b0%d0%b9%d0%bd-%d0%b7%d0%b0%d1%8f%d0%b2%d0%ba%d0%b0-%d0%bd%d0%b0-%d0%b8%d0%b4%d0%b5%d0%bd%d1%82%d0%b8%d1%84%d0%b8%d0%ba%d0%b0%d1%86%d0%b8%d1%8e/") {
            let vc = SFSafariViewController(url: actualUrl, entersReaderIfAvailable: true)
            vc.delegate = self
            present(vc, animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = false
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        officeButton.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        UserDefaultsHelper.PinLength = 0
        UserDefaultsHelper.PIN_new = ""
        UserDefaultsHelper.pinNewIsEnabled = false
        UserDefaultsHelper.smsIsEnabledForPayment = false

//        en-us, ru-ru, ky-kg
        if UserDefaultsHelper.localize == nil{
            LocalizeHelper.shared.getLocalization(culture: "ru-ru")
            
        }
        
        //qweqwe
//        // this is to update the dictionary, once you log in
//        LocalizeHelper.shared.getLocalization(culture: "ru-ru")
//        LocalizeHelper.shared.getLocalization(culture: "en-us")
//        LocalizeHelper.shared.getLocalization(culture: "ky-kg")
        
        if UserDefaultsHelper.currentLanguage == nil{
            //languageLabel.text = "Русский язык"
//            languageImage.image = UIImage(named: "Flag-Ru")
            enterButton.titleLabel?.text = "Вход в Мобильный банк"
            officeButton.titleLabel?.text = "Офисы и банкоматы"
        }else{
            //languageLabel.text = UserDefaultsHelper.currentLanguage
//            languageImage.image = UIImage(named: UserDefaultsHelper.currentFlagLanguage ?? "")
            enterButton.titleLabel?.text = localizedText(key: "login_to_mobile_bank")
            officeButton.titleLabel?.text = localizedText(key: "offices_and_terminals")
        }
        var stackViewHeight:CGFloat = 0
        for view in stackView.subviews{
            stackViewHeight += view.frame.height
        }
        //            stackViewHeight += stackView.subviews.last!.frame.height
        if stackViewHeight < view.frame.height{

            //TopInset.constant = view.frame.height - stackViewHeight + 20
        }
    }
    @IBAction func officeButton(_ sender: Any) {
        performSegue(withIdentifier: "toMapVcSegue", sender: self)
    }
    
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMapVcSegue"{
            let vc = segue.destination as! MapViewController
            vc.isFromFirstController = true
        }
    }
}
