//
//  PinViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/4/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import CommonCrypto
import RxSwift
import AudioToolbox
import LocalAuthentication


class PinViewController: BaseViewController {
    
    
    //    MARK - PIN FIELD
    
    @IBOutlet weak var pinView: UIView!
    @IBOutlet weak var pin1view: UIView!
    @IBOutlet weak var pin2view: UIView!
    @IBOutlet weak var pin3view: UIView!
    @IBOutlet weak var pin4view: UIView!
    @IBOutlet weak var pin5view: UIView!
    @IBOutlet weak var pin6view: UIView!
    @IBOutlet weak var pinLabel: UILabel!
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var view4: UIView!
    @IBOutlet weak var view5: UIView!
    @IBOutlet weak var view6: UIView!
    
    @IBOutlet var views: [UIView]!
    
    @IBOutlet weak var backButtonView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    
    @IBOutlet weak var forgetPinButton: UIButton!
    @IBOutlet weak var pinDescriptionLabel: UILabel!
    
    @IBOutlet weak var faceIdLabel: UILabel!
    
    var pinString = "" //Need
    var pinNumberCount = 4
    var PIN_new = ""//Need
    var checkWrongPin = 0
    var pin = ""
    
    let impact = UIImpactFeedbackGenerator()
    let selection = UISelectionFeedbackGenerator()
    
    var isNeedDismissPinCintroller = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        buttonsRound()
        backButtonEnable()
    }
    
    @objc func tapFaceId(sender: UITapGestureRecognizer) {
        authenticationWithTouchID()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        backgroundImage.backgroundColor = UIColor(patternImage: UIImage(named: "Background")!)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(PinViewController.tapFaceId))
        faceIdLabel.isUserInteractionEnabled = true
        faceIdLabel.addGestureRecognizer(tap)
        
        
        if let pinLength = UserDefaultsHelper.PinLength {
            pinNumberCount = pinLength
            print("pinNumberCount: \(pinNumberCount)")
        }
        if let user = UserDefaultsHelper.user{
            
            
            print(user.userdisplayname)
            print(user.username)
        }
        
        isNeedDismissPinCintroller = UserDefaults.standard.bool(forKey: "isNeedDismissPinCintroller")
        self.forgetPinButton.isHidden = false
        isPinExist()
        
    }
    //    обработка нажатия на кнопки при вводе пин
    func tapButtonNumber(number: String){
        pinString.append(number)
        backButton.isEnabled = true
        colorPinViewByTap()
        print(pinString)
    }
    
    //    проверка длины пин
    func checkPinLength(PIN: String){
        
        if pinNumberCount == Constants.PIN_LENGTH{
            
            if UserDefaultsHelper.pinIsEnabled{
                checkPIN(PIN: PIN)
                
            }else{
                if UserDefaultsHelper.pinNewIsEnabled{
                    
                    checkPinLengthConfirmPIN(PIN: PIN)
                }else{
                    UserDefaultsHelper.pinNewIsEnabled = true
                    UserDefaultsHelper.PIN_new = PIN
                    showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                }
            }
        }
        if pinNumberCount == 5{
            
            if UserDefaultsHelper.pinIsEnabled{
                checkPIN(PIN: PIN)
                
            }else{
                if UserDefaultsHelper.pinNewIsEnabled{
                    
                    checkPinLengthConfirmPIN(PIN: PIN)
                }else{
                    UserDefaultsHelper.pinNewIsEnabled = true
                    UserDefaultsHelper.PIN_new = PIN
                    showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                }
            }
        }
        if pinNumberCount == 6{
            
            if UserDefaultsHelper.pinIsEnabled{
                checkPIN(PIN: PIN)
                
            }else{
                if UserDefaultsHelper.pinNewIsEnabled{
                    
                    checkPinLengthConfirmPIN(PIN: PIN)
                }else{
                    UserDefaultsHelper.pinNewIsEnabled = true
                    UserDefaultsHelper.PIN_new = PIN
                    showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                }
            }
        }
    }
    //    подтверждение при проверке пин
    func checkPinLengthConfirmPIN(PIN: String){
        
        if pinNumberCount == Constants.PIN_LENGTH{
            
            let pin = UserDefaultsHelper.PIN_new
            if PIN == pin{
                //Make request create pin if success showController(StoryBoard: "Main", ViewControllerID: "NavMainController")
                self.pin = CryptoManager.stringToIVWithAES128EncodedBase64(text: PIN)
                createPin(ApplicationID: PhoneIdVendor, Pin: self.pin, StoryBoard: "Login", ViewControllerID: "BiometricViewController")
                UserDefaultsHelper.pinIsEnabled = false
                UserDefaultsHelper.pinNewIsEnabled = false
                
                
            }else{
                pinView.shake()
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                checkWrongPin += 1
                alertNotConfirmPin()
            }
        }
        if pinNumberCount == 5{
            
            let pin = UserDefaultsHelper.PIN_new
            if PIN == pin{
                //Make request create pin if success showController(StoryBoard: "Main", ViewControllerID: "NavMainController")
                self.pin = CryptoManager.stringToIVWithAES128EncodedBase64(text: PIN)
                createPin(ApplicationID: PhoneIdVendor, Pin: self.pin, StoryBoard: "Login", ViewControllerID: "BiometricViewController")
                UserDefaultsHelper.pinIsEnabled = false
                UserDefaultsHelper.pinNewIsEnabled = false
                
                
            }else{
                pinView.shake()
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                checkWrongPin += 1
                alertNotConfirmPin()
            }
        }
        if pinNumberCount == 6{
            
            let pin = UserDefaultsHelper.PIN_new
            if PIN == pin{
                //Make request create pin if success showController(StoryBoard: "Main", ViewControllerID: "NavMainController")
                self.pin = CryptoManager.stringToIVWithAES128EncodedBase64(text: PIN)
                createPin(ApplicationID: PhoneIdVendor, Pin: self.pin, StoryBoard: "Login", ViewControllerID: "BiometricViewController")
                UserDefaultsHelper.pinIsEnabled = false
                UserDefaultsHelper.pinNewIsEnabled = false
                
                
            }else{
                pinView.shake()
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                checkWrongPin += 1
                alertNotConfirmPin()
            }
        }
    }
    //    проверка pin
    func checkPIN(PIN: String){
        
        self.pin = CryptoManager.stringToIVWithAES128EncodedBase64(text: PIN)
        
        validatePin(ApplicationID: PhoneIdVendor, Pin: self.pin, StoryBoard: "Main", ViewControllerID: "NavMainController")
        
    }
    
    //    переход на MainVC
    func showController(ViewControllerID: String){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
        
    }
    //    алерт при неверном вводе пин
    func alertWrongPin(errorMessage: String){
        
        let alert = UIAlertController(title: errorMessage, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
            
            self.pinString = ""
            self.pin1view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin2view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin3view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin4view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin5view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin6view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.colorPinBorder(view: self.view1, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view2, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view3, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view4, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view5, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view6, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.backButton.isEnabled = false
            
            
            self.hideLoading()
            
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
        
    }
    //    алерт при неверном вводе пин
    func alertBlockedPin(errorMessage: String){
        let alert = UIAlertController(title: errorMessage, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
            
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
            self.postPinRemove(applicationID: self.PhoneIdVendor)
            
            
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //    алерт при не подтверждении пин
    func alertNotConfirmPin(){
        let alert = UIAlertController(title: self.localizedText(key: "pin_not_confirmed"), message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
            
            UserDefaultsHelper.pinNewIsEnabled = false
            UserDefaultsHelper.PIN_new = ""
            self.pinString = ""
            self.pin1view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin2view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin3view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin4view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin5view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.pin6view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.colorPinBorder(view: self.view1, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view2, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view3, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view4, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view5, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.colorPinBorder(view: self.view6, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            self.backButton.isEnabled = false
            
            
            self.navigationController?.popViewController(animated: false)
            
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //    обработка кнопки удаления введеных цифр пин
    func backButtonEnable(){
        
        self.pinString = ""
        self.pin1view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.pin2view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.pin3view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.pin4view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.pin5view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.pin6view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//        self.colorPinBorder(view: self.view1, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        self.colorPinBorder(view: self.view1, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        self.colorPinBorder(view: self.view2, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        self.colorPinBorder(view: self.view3, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        self.colorPinBorder(view: self.view4, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        self.colorPinBorder(view: self.view5, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        self.colorPinBorder(view: self.view6, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        backButton.isEnabled = false
        if pinNumberCount == Constants.PIN_LENGTH{
            pin5view.isHidden = true
            view5.isHidden = true
            pin6view.isHidden = true
            view6.isHidden = true
        }
        if pinNumberCount == 5{
            pin6view.isHidden = true
            view6.isHidden = true
            
        }
    }
    //    закругление кнопки
    func buttonsRound(){
        
        buttonDesignView(views: views)
    }
    //    кастомизация дизайна
    func buttonDesignView(views: [UIView]) {
        for view in views{
//            view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
//            view.backgroundColor = #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
//            view.layer.borderWidth = 1
//            view.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            colorPinBorder(view: view1, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            colorPinBorder(view: view2, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            colorPinBorder(view: view3, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            colorPinBorder(view: view4, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            colorPinBorder(view: view5, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
            colorPinBorder(view: view6, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        }
    }
    //    кнопка забыли код доступа
    @IBAction func forgetButton(_ sender: Any) {
        
        let alert = UIAlertController(title: localizedText(key:"forgot_pin_question"), message: self.localizedText(key: "forgot_pin_message"), preferredStyle: .alert)
        
        let action = UIAlertAction(title: self.localizedText(key: "log_in"), style: .default) { (action) in
            self.postPinRemove(applicationID: self.PhoneIdVendor)
        }
        let actionCancel = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel, handler: nil)
        
        alert.addAction(action)
        alert.addAction(actionCancel)
        present(alert, animated: true, completion: nil)
        
    }
    
}

extension PinViewController{
    
    @IBAction func button1(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "1")
    }
    @IBAction func button2(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "2")
    }
    @IBAction func button3(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "3")
    }
    @IBAction func button4(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "4")
    }
    @IBAction func button5(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "5")
    }
    @IBAction func button6(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "6")
    }
    @IBAction func button7(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "7")
    }
    @IBAction func button8(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "8")
    }
    @IBAction func button9(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "9")
    }
    @IBAction func button0(_ sender: Any) {
        impact.impactOccurred()
        tapButtonNumber(number: "0")
    }
    @IBAction func buttonBack(_ sender: Any) {
        pinString.remove(at: pinString.index(before: pinString.endIndex))
        if pinString.count > 0{
            backButton.isEnabled = true
            removeColorByTap()
        }else{
            pin1view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            backButton.isEnabled = false
        }
        print(pinString)
    }
    
    func colorPinViewByTap(){
        if pinString.count == 1{
            pin1view.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            colorPinBorder(view: view1, color: UIColor(hexFromString: Constants.MAIN_COLOR).cgColor)
            
        }
        if pinString.count == 2{
            pin2view.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            colorPinBorder(view: view2, color: UIColor(hexFromString: Constants.MAIN_COLOR).cgColor)
        }
        if pinString.count == 3{
            pin3view.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            colorPinBorder(view: view3, color: UIColor(hexFromString: Constants.MAIN_COLOR).cgColor)
        }
        if pinString.count == 4{
            pin4view.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            colorPinBorder(view: view4, color: UIColor(hexFromString: Constants.MAIN_COLOR).cgColor)
            checkPinLength(PIN: pinString)
        }
        if pinString.count == 5{
            pin5view.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            colorPinBorder(view: view5, color: UIColor(hexFromString: Constants.MAIN_COLOR).cgColor)
            checkPinLength(PIN: pinString)
        }
        if pinString.count == 6{
            pin6view.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            colorPinBorder(view: view6, color:UIColor(hexFromString: Constants.MAIN_COLOR).cgColor)
            checkPinLength(PIN: pinString)
        }
    }
    func removeColorByTap(){
        if pinString.count == 1{
            pin2view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            colorPinBorder(view: view2, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        }
        if pinString.count == 2{
            pin3view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            colorPinBorder(view: view3, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        }
        if pinString.count == 3{
            pin4view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            colorPinBorder(view: view4, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        }
        if pinString.count == 4{
            pin5view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            colorPinBorder(view: view5, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        }
        if pinString.count == 5{
            pin6view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            colorPinBorder(view: view6, color: #colorLiteral(red: 0.8274509804, green: 0.831372549, blue: 0.8470588235, alpha: 1))
        }
        
    }
    func colorPinBorder(view: UIView, color: CGColor){
        
        view.backgroundColor = .clear
        view.layer.borderWidth = 2
        view.layer.borderColor = color
    }
}


extension PinViewController{
    //    проверка на пин
    func  isPinExist(){
        
        showLoading()
        managerApi
            .getIsPinExist(ApplicationID: UIDevice.current.identifierForVendor!.uuidString)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (pinIsTrue) in
                    guard let `self` = self else { return }
                    let pinIsTrue = pinIsTrue
                    
                    if pinIsTrue == "true"{
                        UserDefaultsHelper.pinIsEnabled = true
                        self.pinLabel.text = self.localizedText(key: "enter_pin_code")
                        self.forgetPinButton.isHidden = false
                        self.pinDescriptionLabel.isHidden = true
                    }else{
                        self.forgetPinButton.isHidden = true
                        self.pinDescriptionLabel.isHidden = false
                        UserDefaultsHelper.pinIsEnabled = false
                        if !UserDefaultsHelper.pinNewIsEnabled{
                            self.pinLabel.text = self.localizedText(key: "create_pin_code")
                            self.forgetPinButton.isHidden = true
                            self.pinDescriptionLabel.isHidden = false
                        }else{
                            self.pinLabel.text = self.localizedText(key: "repeat_pin")
                            self.forgetPinButton.isHidden = true
                            self.pinDescriptionLabel.isHidden = false
                        }
                    }
                    
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
                    
            })
            .disposed(by: disposeBag)
    }
    
    //    создание пин
    public func createPin(ApplicationID: String, Pin: String, StoryBoard: String, ViewControllerID: String ){
        showLoading()
        
        managerApi.postPinCreate(ApplicationID: ApplicationID, Pin: Pin)
            .subscribe(
                onNext: {[weak self] (Pin) in
                    guard let `self` = self else { return }
                    
                    UserDefaultsHelper.pinNewIsEnabled = true
                    if let user = SessionManager.shared.user{
                        SessionManager.shared.updateUser(user: user)
                        SessionManager.shared.addNewUser(user: user)
                        SessionManager.shared.user = user
                        SessionManager.shared.current(loginID: user.loginID ?? "")
                        
                    }
                    self.showController(StoryBoard: StoryBoard, ViewControllerID: ViewControllerID)
                    self.hideLoading()
                },
                onError: {(error) in
                    print(error.localizedDescription)
                    self.alertNotConfirmPin()
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    валидация пин
    public func validatePin(ApplicationID: String, Pin: String, StoryBoard: String, ViewControllerID: String ){
        showLoading()
        
        managerApi
            .postPinValidate(ApplicationID: ApplicationID, Pin: Pin)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (Pin) in
                    guard let `self` = self else { return }
                    
                    switch Pin.state {
                        
                    case ValidateStatePin.Ok.rawValue:
                        
                        if self.isNeedDismissPinCintroller{
                            if  AuthorizedRevealViewController.authorizedViewControllerPresented{
                                UserDefaults.standard.set(true, forKey: "isNeedDismissLaunchController")
                                self.dismiss(animated: false, completion: nil)
                            }else{
                                self.showController(StoryBoard: StoryBoard, ViewControllerID: ViewControllerID)
                            }
                        }else{
                            
                            self.showController(StoryBoard: StoryBoard, ViewControllerID: ViewControllerID)
                            
                        }
                    case ValidateStatePin.Error.rawValue:
                        self.alertWrongPin(errorMessage: Pin.message ?? "")
                        
                    case ValidateStatePin.Blocked.rawValue:
                        self.alertBlockedPin(errorMessage: Pin.message ?? "")
                        
                    default:
                        self.alertBlockedPin(errorMessage: Pin.message ?? "")
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.alertWrongPin(errorMessage: ApiHelper.shared.errorHelperApi(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    // удаление ПИН кода
    func  postPinRemove(applicationID: String){
        
        showLoading()
        managerApi
            .postPinRemove(ApplicationID: applicationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (response) in
                    guard let `self` = self else { return }
                    
                    self.logOut()
                    
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelperApi(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func logOut(){
        if let loginId = SessionManager.shared.user?.loginID{
            
            SessionManager.shared.user = nil
            UserDefaultsHelper.PIN_new = ""
            UserDefaultsHelper.pinIsEnabled = false
            UserDefaultsHelper.pinNewIsEnabled = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOut"), object: nil)
            SessionManager.shared.logOutUser(loginID: loginId)
        }
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}


extension PinViewController{
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = localizedText(key: "use_iphone_password")
        
        var authError: NSError?
        var reasonString = localizedText(key: "access_to_mobile_bank")
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            if #available(iOS 11.0, *){
                
                switch localAuthenticationContext.biometryType {
                case .faceID:
                    reasonString = "Unlock using FaceID"
                case .touchID:
                    reasonString = "Unlock using TouchID"
                case .none:
                    reasonString = "Unlock Pin"
                }
            }
            
        } else {
            
            guard let error = authError else {
                return
            }
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
            print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
           
            let alert = UIAlertController(title: self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code), message: "", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.authenticateUser()
            }
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
        
        localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
            
            
            if success {
                
                //TODO: User authenticated successfully, take appropriate action
                DispatchQueue.main.async {[unowned self] in
                    
                    if self.isNeedDismissPinCintroller{
                        if  AuthorizedRevealViewController.authorizedViewControllerPresented{
                            UserDefaults.standard.set(true, forKey: "isNeedDismissLaunchController")
                            self.dismiss(animated: false, completion: nil)
                        }else{
                            self.showController(StoryBoard: "Main", ViewControllerID: "NavMainController")
                        }
                    }else{
                        
                        self.showController(StoryBoard: "Main", ViewControllerID: "NavMainController")
                        
                    }
                    
                    //TODO: Send pin to server???
                }
            } else {
                //TODO: User did not authenticate successfully, look at error and take appropriate action
                guard let error = evaluateError else {
                    return
                }
                
                print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                var message = ""
                switch error._code {
                    
                case LAError.authenticationFailed.rawValue:
                    message = self.localizedText(key: "the_user_failed_to_provide_valid_credentials")//"The user failed to provide valid credentials"
                    self.authenticateUser()
                    
                case LAError.appCancel.rawValue:
                    message = self.localizedText(key: "authentication_was_cancelled_by_application")//"Authentication was cancelled by application"
                    
                case LAError.invalidContext.rawValue:
                    message = self.localizedText(key: "the_context_is_invalid")
                    
                case LAError.notInteractive.rawValue:
                    message = self.localizedText(key: "not_interactive")//"Not interactive"
                    
                case LAError.passcodeNotSet.rawValue:
                    message = self.localizedText(key: "passcode_is_not_set_on_the_device")//"Passcode is not set on the device"
                    
                case LAError.systemCancel.rawValue:
                    message = self.localizedText(key: "authentication_was_cancelled_by_the_system")
                    
                case LAError.userCancel.rawValue:
                    message = self.localizedText(key: "enter_pin_code")
                    DispatchQueue.main.async {
                        self.showAlertError(error: self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code), storyBoard: "Login", ViewControllerID: "PinViewController")
                    }
                case LAError.userFallback.rawValue:
                    message = "The user chose to use the fallback"
                    self.authenticateUser()
                    
                default:
                    message = self.evaluatePolicyFailErrorMessageForLA(errorCode: error._code)
                }
            }
        }
    }
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                
            default:
                message = "Did not find error code on LAError object"
            }
        }
        
        return message;
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = self.localizedText(key: "the_user_failed_to_provide_valid_credentials")
            
        case LAError.appCancel.rawValue:
            message = self.localizedText(key: "authentication_was_cancelled_by_application")
            
        case LAError.invalidContext.rawValue:
            message = self.localizedText(key: "the_context_is_invalid")//"The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = self.localizedText(key: "not_interactive")
            
        case LAError.passcodeNotSet.rawValue:
            message = self.localizedText(key: "passcode_is_not_set_on_the_device")
            
        case LAError.systemCancel.rawValue:
            message = self.localizedText(key: "authentication_was_cancelled_by_the_system")//"Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = self.localizedText(key: "enter_pin_code")
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
    func showAlertError(error: String, storyBoard: String, ViewControllerID: String){
        let alert = UIAlertController(title: error+"?", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            self.showController(StoryBoard: storyBoard, ViewControllerID: ViewControllerID)
        }
        let exit = UIAlertAction(title: "Exit", style: .default) { (action) in
            UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
        }
        alert.addAction(action)
        alert.addAction(exit)
        present(alert, animated: true, completion: nil)
    }
    
    func authenticateUser() {
        let context = LAContext()
        
        context.evaluatePolicy(LAPolicy.deviceOwnerAuthentication, localizedReason: localizedText(key: "use_iphone_password")) { [weak self] (success, error) in
            
            guard success else {
                DispatchQueue.main.async {
                    self?.showController(StoryBoard: "Login", ViewControllerID: "NavLogin")
                     UserDefaultsHelper.TouchIDisEnabled = false
                }
                
                return
            }
            
                DispatchQueue.main.async {
                    self?.showController(StoryBoard: "Main", ViewControllerID: "NavMainController")

                    
            }
        }
    }
}
