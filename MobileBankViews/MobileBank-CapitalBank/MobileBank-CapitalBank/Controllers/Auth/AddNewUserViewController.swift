//
//  AddNewUserViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/25/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import LocalAuthentication

// класс для нового пользователя
class AddNewUserViewController: BaseViewController, UITextFieldDelegate {
    
    var password = ""
    var client_secret = ""
    var isNeedAddNewUser = false
    
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var eyePasswordImage: UIImageView!
    
    //label error
    @IBOutlet weak var labelErrorLogin: UILabel!
    @IBOutlet weak var labelErrorPassword: UILabel!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var menu: UIBarButtonItem!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navigationTitle = self.localizedText(key: "add_user")
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        
        if  menu != nil{
            menu.target = self.revealViewController()
            menu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        hideKeyboardWhenTappedAround()
        
        loginTextField.delegate = self
        passwordTextField.delegate = self
        
        getSettingsMobile(viewController: self)
        UserDefaults.standard.set(true, forKey: "Header0")
        UserDefaults.standard.set(true, forKey: "Header1")
        UserDefaults.standard.set(true, forKey: "Header2")
        UserDefaults.standard.set(true, forKey: "Header3")
        
        NotificationCenter.default.addObserver(self, selector: #selector (alert), name: NSNotification.Name(rawValue: "UserIsExist"), object: nil)
        
        loginTextField.addTarget(self, action: #selector(textFieldEditingDidChangeLogin(_:)), for: UIControl.Event.editingDidEnd)
        passwordTextField.addTarget(self, action: #selector(textFieldEditingDidChangePassword(_:)), for: UIControl.Event.editingDidEnd)
        
    }
    
    // Проверка поля "Login на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChangeLogin(_ sender: Any) {
        loginTextField.stateIfEmpty(view: viewLogin, labelError: labelErrorLogin)
    }
    // Проверка поля "Password" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChangePassword(_ sender: Any) {
        passwordTextField.stateIfEmpty(view: viewPassword, labelError: labelErrorPassword)
    }
    
    
    @objc func alert() {
        
        showAlertController(localizedText(key: "account_already_active"))
        showController(StoryBoard: "Login", ViewControllerID: "NavLogin")
    }
    
    @IBAction func submitButton(_ sender: Any) {
        
        client_secret = randomString(length: 20)
        
        if let PassText = passwordTextField.text{
            self.password = CryptoManager.stringToIVWithAES128EncodedBase64(text: PassText)
            
            authotization(clientSecret: client_secret, clientId: PhoneIdVendor)
            
        }
    }
    @IBAction func passwordSecureButton(_ sender: Any) {
        
        if passwordTextField.isSecureTextEntry == true {
            passwordTextField.isSecureTextEntry = false
            eyePasswordImage.image = UIImage(named: "eyeOpen")
        }else{
            passwordTextField.isSecureTextEntry = true
            eyePasswordImage.image = UIImage(named: "eyeClose")
        }
    }
    //    получение строки для кода
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    //    изменение активного поля ввода при фокусе
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == loginTextField {
            viewLogin.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        }
        if textField == passwordTextField {
            viewPassword.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        }
    }
    // изменение активного поля ввода при потере фокуса
    func textFieldDidEndEditing(_ textField: UITextField)  {
        if textField == loginTextField {
            viewLogin.backgroundColor = #colorLiteral(red: 0.9182453156, green: 0.9182668328, blue: 0.9182552695, alpha: 1)
        }
        if textField == passwordTextField {
            viewPassword.backgroundColor = #colorLiteral(red: 0.9182453156, green: 0.9182668328, blue: 0.9182552695, alpha: 1)
        }
    }
}

// расширение для класса AuthorizationViewController
extension AddNewUserViewController{
    
    //  метод авторизации юзера
    public func authotization(clientSecret: String, clientId: String ){
        if loginTextField.shakeIfEmpty() || passwordTextField.shakeIfEmpty(){
            return
        }
        showLoading()
        UserDefaults.standard.set(false, forKey: "isNeedDismissPinCintroller")
        managerApi.getUserWithoutCertificate(grant_type: "password", username: loginTextField.text ?? "test", password: password, clientId: clientId, clientSecret: clientSecret)
            .subscribe(
                onNext: {[weak self] (user) in
                    guard let `self` = self else { return }
                    SessionManager.shared.addNewUser(user: user)
                    SessionManager.shared.user = user
                    SessionManager.shared.current(loginID: user.loginID ?? "")
                    
                    UserDefaults.standard.set(true, forKey: "isNeedChangeUser")
                    UserDefaults.standard.set(false, forKey: "isNeedDismissPinCintroller")
                    
                    if user.change_pwd_after_login == "True" || user.pwd_expiry_notify == "True"{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        
                        if let additionalTypes = user.additional_auth_types{
                            switch additionalTypes {
                            case "0":
                                //  0 - нет
                                self.isExistPin()
                                
                            case "1":
                                //    1 - ПИН код etoken
                                self.sendEtoken()
                                
                            case "2":
                                //    2 - СМС код
                                self.sendSMS()
                                
                            case "3":
                                //    3 - ПИН код etoken и СМС код
                                self.sendSMSEtoken()
                                
                            default:
                                break
                            }
                        }
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelperApi(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    проверка на наличие пин кода
    func  isPinExist(){
        
        showLoading()
        managerApi
            .getIsPinExist(ApplicationID: UIDevice.current.identifierForVendor!.uuidString)
            .subscribe(
                onNext: {[weak self] (pinIsTrue) in
                    guard let `self` = self else { return }
                    
                    let pinIsTrue = pinIsTrue
                    if pinIsTrue == "true"{
                        UserDefaultsHelper.pinIsEnabled = true
                        
                    }else{
                        UserDefaultsHelper.pinIsEnabled = false
                    }
                    self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                    
                    self.hideLoading()
                },
                onError: {[weak self](error) in
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

