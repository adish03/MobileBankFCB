//
//  ViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/4/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import LocalAuthentication

// класс AuthorizationViewController
class AuthorizationViewController: BaseViewController, ChangeOldPasswordProtocol {
    
    
    var password = ""
    var client_secret = ""
    var isNeedAddNewUser = false
    var isClearOldPassword = false
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var eyePasswordImage: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        if isClearOldPassword {
            passwordTextField.becomeFirstResponder()
            passwordTextField.text = ""
            isClearOldPassword = false
            
            UserDefaultsHelper.PIN_new = ""
            UserDefaultsHelper.pinNewIsEnabled = false
            UserDefaultsHelper.smsIsEnabledForPayment = false
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        CustomStatusBar(color: UIColor(hexFromString: Constants.MAIN_COLOR))
        
        if UserDefaultsHelper.localize == nil{
            LocalizeHelper.shared.getLocalization(culture: "ru-ru")
        }
        loginTextField.delegate = self
        passwordTextField.delegate = self
        
        let navigationTitle = self.localizedText(key: "Resources_Auth_Common_UserName")
        navigationItem.title = navigationTitle
        UserDefaults.standard.set(false, forKey: "isNeedDismissPinCintroller")
        UserDefaults.standard.set(false, forKey: "isNeedDismissLaunchController")
        
        hideKeyboardWhenTappedAround()
        
        getSettingsMobile(viewController: self)
        UserDefaults.standard.set(true, forKey: "Header0")
        UserDefaults.standard.set(true, forKey: "Header1")
        UserDefaults.standard.set(true, forKey: "Header2")
        UserDefaults.standard.set(true, forKey: "Header3")
        
        NotificationCenter.default.addObserver(self, selector: #selector (alert), name: NSNotification.Name(rawValue: "UserIsExist"), object: nil)
        
    }
    
    
    
    @objc func alert() {
        
        showAlertController(localizedText(key: "account_already_active"))
        showController(StoryBoard: "Login", ViewControllerID: "NavLogin")
    }
    
    @IBAction func submitButton(_ sender: Any) {
        
        client_secret = randomString(length: 20)
        
        if let PassText = passwordTextField.text{
            self.password = CryptoManager.stringToIVWithAES128EncodedBase64(text: PassText)
            
            authotization(clientSecret: client_secret, clientId: PhoneIdVendor)
            
        }
    }
    @IBAction func passwordSecureButton(_ sender: Any) {
        
        if passwordTextField.isSecureTextEntry == true {
            passwordTextField.isSecureTextEntry = false
            eyePasswordImage.image = UIImage(named: "eyeOpen")
        }else{
            passwordTextField.isSecureTextEntry = true
            eyePasswordImage.image = UIImage(named: "eyeClose")
        }
    }
    
    func randomString(length: Int) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0...length-1).map{ _ in letters.randomElement()! })
    }
    func isClearPasswordTextField(isClear: Bool) {
        isClearOldPassword = isClear    }
    
}

// расширение для класса AuthorizationViewController
extension AuthorizationViewController{
    
    //  метод авторизации юзера
    public func authotization(clientSecret: String, clientId: String ){
        if loginTextField.shakeIfEmpty() || passwordTextField.shakeIfEmpty(){
            return
        }
        showLoading()
        
        managerApi.getUserWithoutCertificate(grant_type: "password", username: loginTextField.text ?? "test", password: password, clientId: clientId, clientSecret: clientSecret)
            .subscribe(
                onNext: {[weak self] (user) in
                    guard let `self` = self else { return }
                   
                        SessionManager.shared.updateUser(user: user)
                        SessionManager.shared.addNewUser(user: user)
                        SessionManager.shared.user = user
                        SessionManager.shared.current(loginID: user.loginID ?? "")
                    
                    if let additionalTypes = user.additional_auth_types{
                        
                        switch additionalTypes {
                        case "0":
                            //  0 - нет
                            if user.change_pwd_after_login == "True" || user.pwd_expiry_notify == "True"{
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                                vc.isNeedChangePassword = true
                                vc.delegate = self as? ChangeOldPasswordProtocol
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }else{
                               
                                self.isExistPin()
                            }
                            
                        case "1":
                            //    1 - ПИН код etoken
                            self.sendEtoken()
                            
                        case "2":
                            //    2 - СМС код
                            self.sendSMS()
                            
                        case "3":
                            //    3 - ПИН код etoken и СМС код
                            self.sendSMSEtoken()
                            
                        default:
                            break
                        }
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelperApi(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
}

extension AuthorizationViewController: UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginTextField {
            textField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            textField.resignFirstResponder()
        }
        return true
    }
}
