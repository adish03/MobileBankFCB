//
//  OtkViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/26/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift


class OtkViewController: BaseViewController {
    
    @IBOutlet weak var eTokenTextField: UITextField!
    var smsRequired = ""
    var codeLength = 6
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = localizedText(key: "verification_code")
        hideKeyboardWhenTappedAround()
        
        if let Length = UserDefaultsHelper.EtokenCodeLength {
            codeLength = Length
            print("pinNumberCount: \(codeLength)")
        }
        
        eTokenTextField.defaultTextAttributes.updateValue(5.0, forKey: NSAttributedString.Key.kern)
        eTokenTextField.becomeFirstResponder()
        eTokenTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged) // make sure it is the desired textField
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.count  == codeLength{
            
            if let etoken = eTokenTextField.text{
                print("send \(etoken) to server for confirm")
                
                updateToken(etoken_code: etoken, sms_code: nil)
            }
        }
    }
    
    
    
    @IBAction func submitButton(_ sender: Any) {
    }
    
}
extension OtkViewController{
    public func updateToken(etoken_code: String, sms_code: String?){
        showLoading()
        if let refreshToken = SessionManager.shared.refreshToken{
            managerApi
                .getUpdateTokenWithAdditionalAuth(grant_type: "refresh_token", refresh_token: refreshToken, command: "additional_auth", sms_code: sms_code, pin: etoken_code)
                .subscribe(
                    onNext: {[weak self] (user) in
                        guard let `self` = self else { return }
                        if user.change_pwd_after_login == "True" || user.pwd_expiry_notify == "True"{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "ChangePasswordViewController") as! ChangePasswordViewController
                            vc.isNeedChangePassword = true
                            vc.delegate = self as? ChangeOldPasswordProtocol
                            self.navigationController?.pushViewController(vc, animated: true)
                        }else{
                            SessionManager.shared.updateUser(user: user)
                            SessionManager.shared.addNewUser(user: user)
                            SessionManager.shared.user = user
                            SessionManager.shared.current(loginID: user.loginID ?? "")
                            
                            ApiHelper.shared.getAllowedOperations()
                            self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                        }
                        self.hideLoading()
                    },
                    onError: {[weak self] error in
                        self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                        self?.eTokenTextField.text = ""
                        self?.hideLoading()
                })
                .disposed(by: disposeBag)
        }
    }
}
