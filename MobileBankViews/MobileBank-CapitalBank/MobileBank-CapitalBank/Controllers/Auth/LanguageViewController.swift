//
//  LanguageViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/13/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import LocalAuthentication

// класс для смены языка
class LanguageViewController: BaseViewController {
    
    @IBOutlet weak var russianLabel: UILabel!
    @IBOutlet weak var englishLabel: UILabel!
    @IBOutlet weak var nativeLabel: UILabel!
    @IBOutlet weak var checkRu: UIImageView!
    @IBOutlet weak var checkKg: UIImageView!
    @IBOutlet weak var checkEn: UIImageView!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        CustomStatusBar(color: UIColor(hexFromString: Constants.MAIN_COLOR))
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = localizedText(key: "select_language")
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        russianLabel.text = localizedText(key: "russian_language")
        nativeLabel.text = localizedText(key: "kirghiz_language")
        englishLabel.text = localizedText(key: "english_language")
        if UserDefaultsHelper.currentLanguage == nil{
            checkRu.image = UIImage(named: "Active")
            checkKg.image = UIImage(named: "")
            checkEn.image = UIImage(named: "")
        }else{
            check(UserDefaultsHelper.currentLanguage ?? "")
        }
    }
//    кнопка переключения языка 
    @IBAction func ruButton(_ sender: Any) {
       
        getLocalization(culture: "ru-ru")
        UserDefaultsHelper.currentLanguage = "Русский язык"
        UserDefaultsHelper.currentFlagLanguage = "Flag-Ru"
    }
    //    кнопка переключения языка
    @IBAction func kgButton(_ sender: Any) {
        
        getLocalization(culture: "ky-kg")
        UserDefaultsHelper.currentLanguage = "Кыргызча"
        UserDefaultsHelper.currentFlagLanguage = "Flag-Ru"

    }
    //    кнопка переключения языка
    @IBAction func gbButton(_ sender: Any) {
        getLocalization(culture: "en-us")
        UserDefaultsHelper.currentLanguage = "English"
        UserDefaultsHelper.currentFlagLanguage = "Flag-Gb"
    }
    //    получение getLocalization for sideMenu
    func getLocalization(culture: String){
        
        showLoading()
        LocalizeHelper.shared.CleanDictInDocDirectory()
        
        let getLocalization:Observable<Localize> = managerApi
            .getLocalization(culture: culture)
        
        getLocalization
            .subscribe(
                onNext: {[weak self] (localization) in
                    guard let `self` = self else { return }
                    UserDefaultsHelper.localize = localization
                    LocalizeHelper.shared.addLocalizeString()
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                    self.view.window?.rootViewController = vc
                    
                    let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
                    let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                    let navigationController = UINavigationController(rootViewController: destinationController)
                    vc.pushFrontViewController(navigationController, animated: true)
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
//    проверка на текущий язык
    func check(_ currentLanguage: String) {
        
        switch currentLanguage {
        case "Кыргызча":
            checkRu.image = UIImage(named: "")
            checkKg.image = UIImage(named: "Active")
            checkEn.image = UIImage(named: "")
            
        case "Русский язык":
            checkRu.image = UIImage(named: "Active")
            checkKg.image = UIImage(named: "")
            checkEn.image = UIImage(named: "")
            
        case "English":
            checkRu.image = UIImage(named: "")
            checkKg.image = UIImage(named: "")
            checkEn.image = UIImage(named: "Active")
       
        default:
            break
        }
    }
}
