//
//  BiometricViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/14/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore

class BiometricViewController: BaseViewController {

    @IBOutlet weak var useBiometricLabel: UILabel!
    var TouchIDis = true

    
    override func viewDidLoad() {
        super.viewDidLoad()

        useBiometricLabel.text = localizedText(key: "use_touch_id_or_face_id_for_quick_access")
        
    }
    
    @IBAction func useBiometricButton(_ sender: Any) {
        self.TouchIDis = true
        UserDefaultsHelper.TouchIDisEnabled = self.TouchIDis
        UserDefaultsHelper.pinIsEnabled = true
        showController(StoryBoard: "Main", ViewControllerID: "NavMainController")
        
        print("switchTouchID is ON")
    }
    @IBAction func notUseBiometricButton(_ sender: Any) {
        self.TouchIDis = false
        UserDefaultsHelper.TouchIDisEnabled = self.TouchIDis
        UserDefaultsHelper.pinIsEnabled = true
        showController(StoryBoard: "Main", ViewControllerID: "NavMainController")
        print("switchTouchID is OFF")
    }
}


