//
//  LaunchViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/8/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import LocalAuthentication

class LaunchViewController: BaseViewController {

    var isNeedDismissLaunchController = false
    var delegate: AppDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
            
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        isNeedDismissLaunchController = UserDefaults.standard.bool(forKey: "isNeedDismissLaunchController")
        
        if SessionManager.shared.isLoggedIn{
            if let user = UserDefaultsHelper.user{
                updateTokenForPin(user: user)
            }
        }else{
            showController(StoryBoard: "Login", ViewControllerID: "NavLogin")
        }
    }
    
//       обновление токена
    public func updateTokenForPin(user: User){
        
        showLoading()
        managerApi.getUpdateTokenWithoutCertificate(grant_type: "refresh_token", refresh_token: user.refresh_token ?? "")
            .subscribe(
                onNext: {[weak self](user) in
                    guard let `self` = self else { return }
                    
                    SessionManager.shared.user = user
                    SessionManager.shared.updateUser(user: user)
                    
                    var currentIndex = 0
                    for userCurrent in SessionManager.shared.users ?? []
                    {
                        if userCurrent.loginID == user.loginID {
                            break
                        }
                        currentIndex += 1
                    }
                    UserDefaultsHelper.users?[currentIndex] = user
                    SessionManager.shared.current(loginID: user.loginID ?? "")
                    
                    if let TouchIdis = UserDefaultsHelper.TouchIDisEnabled{
                    
                        if TouchIdis{
                            if self.isNeedDismissLaunchController{
                                UserDefaults.standard.set(false, forKey: "isNeedDismissLaunchController")
                                self.dismiss(animated: true, completion: nil)
                                UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
                                
                            }else{
                                self.authenticationWithTouchID()
                            }
                        }else{
                            if self.isNeedDismissLaunchController{
                                UserDefaults.standard.set(false, forKey: "isNeedDismissLaunchController")
                                self.dismiss(animated: true, completion: nil)
                                UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
                                
                            }else{
                                self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                            }
                        }
                    }else{
                        if self.isNeedDismissLaunchController{
                            UserDefaults.standard.set(false, forKey: "isNeedDismissLaunchController")
                            self.dismiss(animated: true, completion: nil)
                            UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
                            
                        }else{
                            self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                        }
                    }
                    self.hideLoading()
            },
                onError: {(error) in
                    self.hideLoading()
                    self.showController(StoryBoard: "Login", ViewControllerID: "NavLogin")
                   
//                    self.showAlertController(ApiHelper.shared.errorHelper(error: error))
            })
            .disposed(by: disposeBag)
    }
}

    //MARK touchID/FaceID Logic

extension LaunchViewController{
    
    func authenticationWithTouchID() {
        let localAuthenticationContext = LAContext()
        localAuthenticationContext.localizedFallbackTitle = localizedText(key: "use_iphone_password")
        
        var authError: NSError?
        var reasonString = localizedText(key: "access_to_mobile_bank")
        
        if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            
            if #available(iOS 11.0, *){
                
                switch localAuthenticationContext.biometryType {
                case .faceID:
                    reasonString = "Unlock using FaceID"
                case .touchID:
                    reasonString = "Unlock using TouchID"
                case .none:
                    reasonString = "Unlock Pin"
                }
            }
            
        } else {
            
            guard let error = authError else {
                return
            }
            //TODO: Show appropriate alert if biometry/TouchID/FaceID is lockout or not enrolled
            print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code))
           
            let alert = UIAlertController(title: self.evaluateAuthenticationPolicyMessageForLA(errorCode: error.code), message: "", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default) { (action) in
                self.authenticateUser()
            }
            alert.addAction(action)
            present(alert, animated: true, completion: nil)
        }
        
        localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reasonString) { success, evaluateError in
            
            
            if success {
                
                //TODO: User authenticated successfully, take appropriate action
                DispatchQueue.main.async {[unowned self] in
                    
                    self.showController(StoryBoard: "Main", ViewControllerID: "NavMainController")
                    //TODO: Send pin to server???
                }
            } else {
                //TODO: User did not authenticate successfully, look at error and take appropriate action
                guard let error = evaluateError else {
                    return
                }
                
                print(self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code))
                var message = ""
                switch error._code {
                    
                case LAError.authenticationFailed.rawValue:
                    message = self.localizedText(key: "the_user_failed_to_provide_valid_credentials")//"The user failed to provide valid credentials"
                    self.authenticateUser()
                    
                case LAError.appCancel.rawValue:
                    message = self.localizedText(key: "authentication_was_cancelled_by_application")//"Authentication was cancelled by application"
                    
                case LAError.invalidContext.rawValue:
                    message = self.localizedText(key: "the_context_is_invalid")
                    
                case LAError.notInteractive.rawValue:
                    message = self.localizedText(key: "not_interactive")//"Not interactive"
                    
                case LAError.passcodeNotSet.rawValue:
                    message = self.localizedText(key: "passcode_is_not_set_on_the_device")//"Passcode is not set on the device"
                    
                case LAError.systemCancel.rawValue:
                    message = self.localizedText(key: "authentication_was_cancelled_by_the_system")
                    
                case LAError.userCancel.rawValue:
                    message = self.localizedText(key: "enter_pin_code")
                    DispatchQueue.main.async {
                        self.showAlertError(error: self.evaluateAuthenticationPolicyMessageForLA(errorCode: error._code), storyBoard: "Login", ViewControllerID: "PinViewController")
                    }
                case LAError.userFallback.rawValue:
                    message = "The user chose to use the fallback"
                    self.authenticateUser()
                    
                default:
                    message = self.evaluatePolicyFailErrorMessageForLA(errorCode: error._code)
                }
            }
        }
    }
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                
            default:
                message = "Did not find error code on LAError object"
            }
        }
        
        return message;
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = self.localizedText(key: "the_user_failed_to_provide_valid_credentials")
            
        case LAError.appCancel.rawValue:
            message = self.localizedText(key: "authentication_was_cancelled_by_application")
            
        case LAError.invalidContext.rawValue:
            message = self.localizedText(key: "the_context_is_invalid")//"The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = self.localizedText(key: "not_interactive")
            
        case LAError.passcodeNotSet.rawValue:
            message = self.localizedText(key: "passcode_is_not_set_on_the_device")
            
        case LAError.systemCancel.rawValue:
            message = self.localizedText(key: "authentication_was_cancelled_by_the_system")//"Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = self.localizedText(key: "enter_pin_code")
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
    func showAlertError(error: String, storyBoard: String, ViewControllerID: String){
        let alert = UIAlertController(title: error+"?", message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            self.showController(StoryBoard: storyBoard, ViewControllerID: ViewControllerID)
        }
        let exit = UIAlertAction(title: "Exit", style: .default) { (action) in
            UIControl().sendAction(#selector(NSXPCConnection.suspend), to: UIApplication.shared, for: nil)
        }
        alert.addAction(action)
        alert.addAction(exit)
        present(alert, animated: true, completion: nil)
    }
    
    func authenticateUser() {
        let context = LAContext()
        
        context.evaluatePolicy(LAPolicy.deviceOwnerAuthentication, localizedReason: localizedText(key: "use_iphone_password")) { [weak self] (success, error) in
            
            guard success else {
                DispatchQueue.main.async {
                    self?.showController(StoryBoard: "Login", ViewControllerID: "NavLogin")
                     UserDefaultsHelper.TouchIDisEnabled = false
                }
                
                return
            }
            
                DispatchQueue.main.async {
                    self?.showController(StoryBoard: "Main", ViewControllerID: "NavMainController")

                    
            }
        }
    }
}

