//
//  InternalTransactionViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/11/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa
// класс для внутрибанковских переводов, переводов между счетами и гросс-клиринг
class TransactionViewController: BaseViewController, DepositProtocol, DepositProtocolTo, EditTransactionsOperationProtocol, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var accountsTableView: UIView!
    
    @IBOutlet weak var accountToView: UIView!
    @IBOutlet weak var internalView: UIView!
    @IBOutlet weak var accountNoLabel: UILabel!
    @IBOutlet weak var accountSumLabel: UILabel!
    @IBOutlet weak var accountToTextField: UITextField!
    @IBOutlet weak var sumOfTransactionTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet var currencyLabel: [UILabel]!
    @IBOutlet weak var payTo_accountLabel: UILabel!
    @IBOutlet weak var choiceCardTo_accountLabel: UILabel!
    @IBOutlet weak var accountNameReceiver: UILabel!
    @IBOutlet weak var chooseAccountLabel: UILabel!
    
    // views error
    @IBOutlet weak var viewFromAccount: UIView!
    @IBOutlet weak var viewToAccount: UIView!
    @IBOutlet weak var viewToInternalAccount: UIView!
    @IBOutlet weak var viewSumFromTextField: UIView!
    @IBOutlet weak var viewCommentTextField: UIView!
    
    //label error
    @IBOutlet weak var labelErrorAccountFrom: UILabel!
    @IBOutlet weak var labelErrorAccount: UILabel!
    @IBOutlet weak var labelErrorInternalAccount: UILabel!
    @IBOutlet weak var labelErrorSum: UILabel!
    @IBOutlet weak var labelErrorComment: UILabel!
    
    lazy var accountTableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.delegate = self
        view.dataSource = self
        view.register(AccountsCell.self, forCellReuseIdentifier: "AccountsCell")
        return view
    }()
    
    @IBOutlet weak var accountsTableViewConstraint: NSLayoutConstraint!
    
    //Template IBOutlets
    var isCreateTemplate = false
    var isSchedule = false
    var StartDate = ""
    var EndDate: String? = ""
    var RecurringTypeID = 1
    var ProcessDay = 1
    var schedule: Schedule!
    //EditingTemplate
    var isEditTemplate = false
    var template: ContractModelTemplate!
    var internalTemplateFromStory: InternalOperationModel!
    //RepeatPayTemplate
    var isRepeatPay = false
    var isRepeatFromStory = false
    var isRepeatOperation = false
    var isChangeData = false
    var isDismissing = false
    
    @IBOutlet weak var labelErrorTemplateName: UILabel!
    @IBOutlet weak var viewTemplateNameTextFields: UIView!
    @IBOutlet weak var TemplateDescriptionView: UIView!
    @IBOutlet weak var ScheduleView: UIView!
    @IBOutlet weak var templateNameTextField: UITextField!
    
    @IBOutlet weak var switchPlanner: UISwitch!
    @IBOutlet weak var viewPlanner: UIView!
    @IBOutlet weak var periodTextField: UITextField!
    
    @IBOutlet weak var viewDateBegin: UIView!
    @IBOutlet weak var viewDateFinish: UIView!
    @IBOutlet weak var viewWeekDay: UIView!
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var viewDateOperation: UIView!
    
    @IBOutlet weak var dateBeginTextField: UITextField!
    @IBOutlet weak var dateFinishTextField: UITextField!
    
    @IBOutlet weak var dayNumberTextField: UITextField!
    @IBOutlet weak var dateOperationTextField: UITextField!
    @IBOutlet weak var weekDayTextField: UITextField!
    
    @IBOutlet weak var createSwiftButton: RoundedButton!
    var typeOperation = ""
    
    var pickerView = UIPickerView()
    var pickerViewDayOperation = UIPickerView()
    var pickerViewWeekDay = UIPickerView()
    var pickerTarif = UIPickerView()
   
    var periodList = ["EachWeek".localized, "EachMonth".localized, "OneTime".localized]
     var daysList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20","21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
    var weekList = ["monday".localized, "tuesday".localized, "wednesday".localized, "thursday".localized, "friday".localized, "saturday".localized, "sunday".localized]
    let datePicker = UIDatePicker()
    var phoneNumbers = ["5", "9", "7", "2"]
    
    var sectionAccount = 0
    var index = 0
    var accounts: [Accounts] = []
    var accountsClone: [Accounts] = []
    var currencies = [Currency]()
    var simpleAccounts = [SimpleAccountModel]()
    var simpleAccount: SimpleAccountModel!
    
    var selectedDeposit: Deposit!
    var selectedDepositTo: Deposit!
    
    var navigationTitle = ""
    var operationTypeID = 0
    var currencyIDcurrent = 0
    var accountNoCurrent = ""
    var operationID = 0
    var submitModel: InternalOperationModel!
    var accountsAfterChoice: [Accounts] = []
    var filteredArray: [Accounts] = []
    var isFullAccount = false
    var isFullZeroAccount = false
    var isWithdrawAccount = false
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        allowToCreatAndSaveOperation(button: createSwiftButton)
        
        if isCreateTemplate{
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                accountToView.isHidden = true
                if self.selectedDeposit != nil{
                    self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                }
                if submitModel != nil{
                    accountToTextField.text = submitModel.ctAccountNo
                    sumOfTransactionTextField.text = "\(String(format:"%.2f",submitModel.sum ?? 0.0 ))"
                    descriptionTextField.text = submitModel.comment
                    getAccountForInternalTransaction(accountNo: submitModel.ctAccountNo ?? "", currencyID: submitModel.currencyID ?? 0)
                }
            }
            if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                internalView.isHidden = true
                if self.selectedDeposit != nil{
                    self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                }
                if self.selectedDepositTo != nil{
                    self.fullSelectedDepositTo(selectedDeposit: self.selectedDepositTo)
                }
                if submitModel != nil{
                    sumOfTransactionTextField.text = "\(String(format:"%.2f",submitModel.sum ?? 0.0 ))"
                    descriptionTextField.text = submitModel.comment
                }
            }
            
            createSwiftButton.setTitle( localizedText(key: "save_template"),for: .normal)
            
        }else if isEditTemplate{
            TemplateDescriptionView.isHidden = false
            ScheduleView.isHidden = false
            operationTypeID = template.operationTypeID ?? 0
            isEditTemplate = true
            RecurringTypeID = template.schedule?.recurringTypeID ?? 1
            if template.isSchedule ?? false{
                isSchedule = true
                StartDate = template.schedule?.startDate ?? ""
                EndDate = template.schedule?.endDate
                RecurringTypeID = template.schedule?.recurringTypeID ?? 0
                ProcessDay = template.schedule?.processDay ?? 0
                switchPlanner.isOn = true
                viewPlanner.isHidden = false
                
                let schedule = ScheduleHelper.shared.scheduleFullScheduleHelper(
                    StartDate: template.schedule?.startDate,
                    EndDate: template.schedule?.endDate,
                    RecurringTypeID: template.schedule?.recurringTypeID,
                    ProcessDay: template.schedule?.processDay)
                
                switch RecurringTypeID {
                case ScheduleRepeat.OneTime.rawValue :
                    viewDateBegin.isHidden = true
                    viewDateFinish.isHidden = true
                    viewWeekDay.isHidden = true
                    viewDay.isHidden = true
                    viewDateOperation.isHidden = false
                    periodTextField.text = periodList[2]
                    dateOperationTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                    
                case ScheduleRepeat.EachWeek.rawValue:
                    viewDateBegin.isHidden = false
                    viewDateFinish.isHidden = false
                    viewWeekDay.isHidden = false
                    viewDay.isHidden = true
                    viewDateOperation.isHidden = true
                    periodTextField.text = periodList[0]
                    dateBeginTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                    dateFinishTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.endDate ?? "")
                    weekDayTextField.text = weekList[(schedule?.processDay ?? 1) - 1]
                    
                case ScheduleRepeat.EachMonth.rawValue:
                    viewDateBegin.isHidden = false
                    viewDateFinish.isHidden = false
                    viewWeekDay.isHidden = true
                    viewDay.isHidden = false
                    viewDateOperation.isHidden = true
                    periodTextField.text = periodList[1]
                    dateBeginTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                    dateFinishTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.endDate ?? "")
                    dayNumberTextField.text = daysList[(schedule?.processDay ?? 1) - 1]
                    
                default:
                    viewDateBegin.isHidden = true
                    viewDateFinish.isHidden = true
                    viewWeekDay.isHidden = true
                    viewDay.isHidden = true
                    viewDateOperation.isHidden = true
                    
                }
            }
            
            getInfoOperationTransactions(operationID: template.operationID ?? 0)
            
            createSwiftButton.setTitle(localizedText(key: "edit_template"),for: .normal)
            
        }else if isRepeatPay{
            TemplateDescriptionView.isHidden = true
            ScheduleView.isHidden = true
            if !isRepeatOperation{
                if isRepeatFromStory{
                    operationTypeID = internalTemplateFromStory.operationType ?? 0
                }else{
                    operationTypeID = template.operationTypeID ?? 0
                }
            }
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                accountToView.isHidden = true
                if internalTemplateFromStory != nil{
                    templateNameTextField.text = internalTemplateFromStory.templateName
                    accountToTextField.text = internalTemplateFromStory.ctAccountNo
                    sumOfTransactionTextField.text = "\(String(format:"%.2f", internalTemplateFromStory.sum ?? 0.0 ))"
                    descriptionTextField.text = internalTemplateFromStory.comment
                    getAccountForInternalTransaction(accountNo: internalTemplateFromStory.ctAccountNo ?? "", currencyID: internalTemplateFromStory.currencyID ?? 0)
                    
                }
                if template != nil{
                    templateNameTextField.text = template.name
                    accountToTextField.text = template.to
                    sumOfTransactionTextField.text = "\(String(format:"%.2f",template.amount ?? 0.0 ))"
                    descriptionTextField.text = template.comment
                    getAccountForInternalTransaction(accountNo: template.to ?? "", currencyID: template.currencyID ?? 0)
                }
                if selectedDeposit != nil{
                    let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                    accountNoLabel.text = fullAccountsText
                    accountSumLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
                    for currency in currencyLabel{
                        currency.text = selectedDeposit.currency?.symbol
                    }
                    currencyIDcurrent = selectedDeposit.currencyID ?? 0
                    if accountToTextField.text != ""{
                        checkAccountAvailable(accountNo: accountToTextField.text ?? "", currencyID: selectedDeposit.currencyID ?? 0)
                    }
                }
            }
            if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                internalView.isHidden = true
                if internalTemplateFromStory != nil{
                    templateNameTextField.text = internalTemplateFromStory.templateName
                    accountToTextField.text = internalTemplateFromStory.ctAccountNo
                    sumOfTransactionTextField.text = "\(String(format:"%.2f", internalTemplateFromStory.sum ?? 0.0 ))"
                    descriptionTextField.text = internalTemplateFromStory.comment
                }
                if template != nil{
                    templateNameTextField.text = template.name
                    sumOfTransactionTextField.text = "\(String(format:"%.2f",template.amount ?? 0.0 ))"
                    descriptionTextField.text = template.comment
                }
                if selectedDeposit != nil{
                    let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                    accountNoLabel.text = fullAccountsText
                    accountSumLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
                    for currency in currencyLabel{
                        currency.text = selectedDeposit.currency?.symbol
                    }
                }else{
                    accountNoLabel.text = localizedText(key: "write_off")
                    accountSumLabel.text = localizedText(key: "select_account_or_card")
                }
                
                if selectedDepositTo != nil{
                    
                    payTo_accountLabel.text = selectedDepositTo.name
                    choiceCardTo_accountLabel.text = "\(String(format:"%.2f", selectedDepositTo.balance ?? 0.0 )) \(String(describing: selectedDepositTo.currency?.symbol ?? ""))"
                    viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
                }else{
                    payTo_accountLabel.text = localizedText(key: "transfer_to")
                    choiceCardTo_accountLabel.text = localizedText(key: "select_account_or_card")
                }
            }
            
        }else{
            
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                if selectedDeposit != nil{
                    let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                    accountNoLabel.text = fullAccountsText
                    accountSumLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
                    for currency in currencyLabel{
                        currency.text = selectedDeposit.currency?.symbol
                    }
                    currencyIDcurrent = selectedDeposit.currencyID ?? 0
                    if accountToTextField.text != ""{
                        checkAccountAvailable(accountNo: accountToTextField.text ?? "", currencyID: selectedDeposit.currencyID ?? 0)
                    }
                }else{
                    accountNoLabel.text = localizedText(key: "write_off")
                    accountSumLabel.text = localizedText(key: "select_account_or_card")
                }
            }
            if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                if selectedDeposit != nil{
                    let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                    accountNoLabel.text = fullAccountsText
                    accountSumLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
                    for currency in currencyLabel{
                        currency.text = selectedDeposit.currency?.symbol
                    }
                }else{
                    accountNoLabel.text = localizedText(key: "write_off")
                    accountSumLabel.text = localizedText(key: "select_account_or_card")
                }
                
                if selectedDepositTo != nil{
                    
                    payTo_accountLabel.text = selectedDepositTo.name
                    choiceCardTo_accountLabel.text = "\(String(format:"%.2f", selectedDepositTo.balance ?? 0.0 )) \(String(describing: selectedDepositTo.currency?.symbol ?? ""))"
                    viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
                }else{
                    payTo_accountLabel.text = localizedText(key: "transfer_to")
                    choiceCardTo_accountLabel.text = localizedText(key: "select_account_or_card")
                }
            }
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        periodList = [localizedText(key: "every_week"),localizedText(key: "every_month"), "Однократно"]
        weekList = [localizedText(key: "mon"),
                    localizedText(key:"tue"),
                    localizedText(key:"wed"),
                    localizedText(key:"th"),
                    localizedText(key:"fri"),
                    localizedText(key:"sat"),
                    localizedText(key:"the_sun")]
        
        if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
            accountToView.isHidden = true
        }
        if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
            internalView.isHidden = true
        }
        if isCreateTemplate{
            TemplateDescriptionView.isHidden = false
            ScheduleView.isHidden = false
        }else{
            TemplateDescriptionView.isHidden = true
            ScheduleView.isHidden = true
        }
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        
        templateNameTextField.delegate = self
        accountToTextField.delegate = self
        sumOfTransactionTextField.delegate = self
        descriptionTextField.delegate = self
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        hideKeyboardWhenTappedAround()
        getAccountsWithCurrencies(vc: nil)
        
        accountToTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControl.Event.editingDidEnd)
        sumOfTransactionTextField.addTarget(self, action: #selector(textFieldEditingDidChangeSum(_:)), for: UIControl.Event.editingDidEnd)
        descriptionTextField.addTarget(self, action: #selector(textFieldEditingDidChangeDescription(_:)), for: UIControl.Event.editingDidEnd)
        
        templateNameTextField.addTarget(self, action: #selector(textFieldEditingEndtemplateName(_:)), for: UIControl.Event.editingDidEnd)
        
        //schedule
        viewPlanner.isHidden = true
        viewDateBegin.isHidden = true
        viewDateFinish.isHidden = true
        viewWeekDay.isHidden = true
        viewDay.isHidden = true
        
        viewDateOperation.isHidden = true
        dateBeginTextField.delegate = self
        dateFinishTextField.delegate = self
        dayNumberTextField.delegate = self
        weekDayTextField.delegate = self
        pickerPeriodDate()
        showDatePicker()
        
        accountsTableView.addSubview(accountTableView)
        accountTableView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
            make.right.equalToSuperview().offset(10)
            make.left.equalToSuperview().offset(-10)
        }
    }
    
    // Проверка поля "Название шаблона" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndtemplateName(_ sender: Any) {
        templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
    }
    // Проверка поля "Сумма" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChangeSum(_ sender: Any) {
        let textNonZeroFirst = sumOfTransactionTextField.text
        let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
        let numberAsInt = Double(newString ?? "0")
        sumOfTransactionTextField.text = "\(numberAsInt ?? 0)"
        if selectedDeposit != nil{
            if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                showAlertController(localizedText(key: "insufficient_account_balance"))
                sumOfTransactionTextField.text = ""
            }
        }
        sumOfTransactionTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
    }
    // Проверка поля "Описание" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChangeDescription(_ sender: Any) {
        descriptionTextField.stateIfEmpty(view: viewCommentTextField, labelError: labelErrorComment)
    }
    // проверка на количество знаков
    @objc func textFieldEditingDidChange(_ sender: Any) {
        let count = accountToTextField.text?.count

        if count == Constants.ACCOUNTS_CHARACTERS_COUNT || (accountToTextField.text?.first == "+" && count == 13) || (accountToTextField.text?.prefix(3) == "996" && count == 12) || (accountToTextField.text?.first == "0" || count == 10)
            || (accountToTextField.text?.first != nil && phoneNumbers.contains{ $0 == String((accountToTextField.text?.first)!)} && count == 9){
            getAccountForInternalTransaction(accountNo: accountToTextField.text ?? "", currencyID: currencyIDcurrent)
            
        }else if !isDismissing {
            showAlertController("Пожалуйста проверьте корректность номера счета/телефона")
            accountToTextField.becomeFirstResponder()
            accountToTextField.stateIfEmpty(view: viewToInternalAccount, labelError: labelErrorInternalAccount)
        }

    }
    // used to prevent showing alert apart from this controller
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if self.isMovingFromParent || self.isBeingDismissed {
            self.isDismissing = true
        }
    }
    
    
    //    кнопка операции
    @IBAction func transactionButton(_ sender: Any) {
        
        if isCreateTemplate{
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                if selectedDeposit != nil && accountToTextField.text != "", simpleAccount != nil, sumOfTransactionTextField.text != "", descriptionTextField.text != "", templateNameTextField.text != "" {
                    let transferSum = Double(sumOfTransactionTextField.text ?? "0.0")
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    
                    let internalOperationModel: InternalOperationModel = InternalOperationModel(
                        dtAccountNo: selectedDeposit.accountNo,
                        ctAccountNo: simpleAccount.accountNo,
                        operationType: operationTypeID,
                        operationID: nil,
                        currencyID: selectedDeposit.currencyID ?? 0,
                        sum: transferSum,
                        comment: descriptionTextField.text,
                        isSchedule: self.isSchedule,
                        isTemplate: self.isCreateTemplate,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Внутрибанковский перевод",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                    
                    postCreatInternalTrasaction(InternalOperationModel: internalOperationModel)
                }else{
                    descriptionTextField.stateIfEmpty(view: viewCommentTextField, labelError: labelErrorComment)
                    accountToTextField.stateIfEmpty(view: viewToInternalAccount, labelError:labelErrorInternalAccount)
                    sumOfTransactionTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
            if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                if selectedDeposit != nil && selectedDepositTo != nil, simpleAccount.accountNo != nil, sumOfTransactionTextField.text != "", descriptionTextField.text != "", templateNameTextField.text != ""{
                    let transferSum = Double(sumOfTransactionTextField.text ?? "0.0")
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    
                    let internalOperationModel: InternalOperationModel = InternalOperationModel(
                        dtAccountNo: selectedDeposit.accountNo,
                        ctAccountNo: selectedDepositTo.accountNo,
                        operationType: operationTypeID,
                        operationID: nil,
                        currencyID: selectedDeposit.currencyID ?? 0,
                        sum: transferSum,
                        comment: descriptionTextField.text,
                        isSchedule: self.isSchedule,
                        isTemplate: self.isCreateTemplate,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Внутрибанковский перевод",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                    
                    postCreatpostInternalOperationCustomerAccounts(InternalOperationModel: internalOperationModel)
                }
                else{
                    viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
                    descriptionTextField.stateIfEmpty(view: viewCommentTextField, labelError: labelErrorComment)
                    sumOfTransactionTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
            
        }else if isEditTemplate{
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                if selectedDeposit != nil && accountToTextField.text != "", simpleAccount != nil, sumOfTransactionTextField.text != "", templateNameTextField.text != "" {
                    let transferSum = Double(sumOfTransactionTextField.text ?? "0.0")
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: 0,startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    
                    let internalOperationModel: InternalOperationModel = InternalOperationModel(
                        dtAccountNo: selectedDeposit.accountNo,
                        ctAccountNo: simpleAccount.accountNo,
                        operationType: operationTypeID,
                        operationID: template.operationID,
                        currencyID: selectedDeposit.currencyID ?? 0,
                        sum: transferSum,
                        comment: descriptionTextField.text,
                        isSchedule: self.isSchedule,
                        isTemplate: true,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Внутрибанковский перевод",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                    
                    postCreatInternalTrasaction(InternalOperationModel: internalOperationModel)
                }
                else{
                    
                    accountToTextField.stateIfEmpty(view: viewToInternalAccount, labelError:labelErrorInternalAccount)
                    sumOfTransactionTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
            if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                if selectedDeposit != nil && selectedDepositTo != nil, sumOfTransactionTextField.text != "", templateNameTextField.text != "" {
                    let transferSum = Double(sumOfTransactionTextField.text ?? "0.0")
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    
                    let internalOperationModel: InternalOperationModel = InternalOperationModel(
                        dtAccountNo: selectedDeposit.accountNo,
                        ctAccountNo: selectedDepositTo.accountNo,
                        operationType: operationTypeID,
                        operationID: template.operationID,
                        currencyID: selectedDeposit.currencyID ?? 0,
                        sum: transferSum,
                        comment: descriptionTextField.text,
                        isSchedule: self.isSchedule,
                        isTemplate: true,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Внутрибанковский перевод",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                    
                    postCreatpostInternalOperationCustomerAccounts(InternalOperationModel: internalOperationModel)
                }
                else{
                    viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
                    sumOfTransactionTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
        }else{
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                if selectedDeposit != nil && accountToTextField.text != "", simpleAccount != nil, sumOfTransactionTextField.text != "", descriptionTextField.text != "" {
                    let transferSum = Double(sumOfTransactionTextField.text ?? "0.0")
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    let internalOperationModel: InternalOperationModel!
                    if isChangeData{
                        internalOperationModel = InternalOperationModel(
                        dtAccountNo: selectedDeposit.accountNo,
                        ctAccountNo: self.simpleAccount.accountNo,
                        operationType: operationTypeID,
                        operationID: internalTemplateFromStory.operationID,
                        currencyID: selectedDeposit.currencyID ?? 0,
                        sum: transferSum,
                        comment: descriptionTextField.text,
                        isSchedule: self.isSchedule,
                        isTemplate: self.isCreateTemplate,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Внутрибанковский перевод",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                    }else{
                        internalOperationModel = InternalOperationModel(
                        dtAccountNo: selectedDeposit.accountNo,
                        ctAccountNo: self.simpleAccount.accountNo,
                        operationType: operationTypeID,
                        operationID: nil,
                        currencyID: selectedDeposit.currencyID ?? 0,
                        sum: transferSum,
                        comment: descriptionTextField.text,
                        isSchedule: self.isSchedule,
                        isTemplate: self.isCreateTemplate,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Внутрибанковский перевод",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                    }
                    if isRepeatOperation{
                        if self.operationID != 0{
                            self.hideLoading()
                            self.allowOperation()
                        }else{
                            postCreatInternalTrasaction(InternalOperationModel: internalOperationModel)
                        }
                    }else{
                        postCreatInternalTrasaction(InternalOperationModel: internalOperationModel)
                    }
                }else{
                    sumOfTransactionTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
                    descriptionTextField.stateIfEmpty(view: viewCommentTextField, labelError: labelErrorComment)
                    accountToTextField.stateIfEmpty(view: viewToInternalAccount, labelError:labelErrorInternalAccount)
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
            if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                if selectedDeposit != nil && selectedDepositTo != nil, sumOfTransactionTextField.text != "", descriptionTextField.text != "" {
                    let transferSum = Double(sumOfTransactionTextField.text ?? "0.0")
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    
                    let internalOperationModel: InternalOperationModel = InternalOperationModel(
                        dtAccountNo: selectedDeposit.accountNo,
                        ctAccountNo: selectedDepositTo.accountNo,
                        operationType: operationTypeID,
                        operationID: nil,
                        currencyID: selectedDeposit.currencyID ?? 0,
                        sum: transferSum,
                        comment: descriptionTextField.text,
                        isSchedule: self.isSchedule,
                        isTemplate: self.isCreateTemplate,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Внутрибанковский перевод",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                    if isRepeatOperation{
                        if self.operationID != 0{
                            self.hideLoading()
                            self.allowOperation()
                        }else{
                            postCreatpostInternalOperationCustomerAccounts(InternalOperationModel: internalOperationModel)
                        }
                    }else{
                         postCreatpostInternalOperationCustomerAccounts(InternalOperationModel: internalOperationModel)
                    }
                }
                else{
                    viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
                    sumOfTransactionTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
                    descriptionTextField.stateIfEmpty(view: viewCommentTextField, labelError: labelErrorComment)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
        }
    }
    //    переход по segue

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //     переход на список счетов
        if segue.identifier == "segueToAccountsFrom"{
            let vc = segue.destination as! AccountsViewController
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "write_off")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
           
            if selectedDeposit == nil && selectedDepositTo == nil{
                accounts = self.accountsClone.map({$0.copy() as! Accounts}).filter{$0.accountType != InternetBankingAccountType.Deposit.rawValue}
                for account in accounts{
                    account.items = account.items?.filter{$0.balance != 0.0 && $0.closeDate == nil}
                }
                vc.accounts = accounts
                vc.accounts = checkAccountsForZeroBalance(accounts: accounts).filter{$0.items?.count != 0}
            }
           
            if selectedDeposit != nil && selectedDepositTo == nil{
                accounts = self.accountsClone.map({$0.copy() as! Accounts}).filter{$0.accountType != InternetBankingAccountType.Deposit.rawValue}
                vc.accounts = checkAccountsForZeroBalance(accounts: accounts).filter{$0.items?.count != 0}
            }
            
            if selectedDeposit == nil && selectedDepositTo != nil{
                accounts = self.accountsClone.map({$0.copy() as! Accounts}).filter{$0.accountType != InternetBankingAccountType.Deposit.rawValue}
                for account in accounts{
                    account.items = account.items?.filter{$0.currencyID == selectedDepositTo.currencyID && $0.closeDate == nil}
                }
                for account in accounts{
                    account.items = account.items?.filter{$0.balance != selectedDepositTo.balance && $0.closeDate == nil}
                }
                vc.accounts = accounts.filter{$0.items?.count != 0}
            }
            if selectedDeposit != nil && selectedDepositTo != nil{
                accounts = self.accountsClone.map({$0.copy() as! Accounts}).filter{$0.accountType != InternetBankingAccountType.Deposit.rawValue}
                selectedDepositTo = nil
                accounts = accounts.filter{$0.items?.count != 0}
                vc.accounts = accounts
                vc.accounts = self.checkAccountsForZeroBalance(accounts: accounts).filter{$0.items?.count != 0}
            }
            
            vc.selectedDeposit = selectedDeposit
        }
        //      переход на список счетов
        if segue.identifier == "segueToAccountsTo"{
            let vc = segue.destination as! AccountsViewController
            vc.delegateTo = self
            vc.navigationTitle = localizedText(key: "transfer_to")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            
            accounts = self.accountsClone.map({$0.copy() as! Accounts})
            
            if selectedDeposit != nil && selectedDepositTo == nil{
                for account in accounts{
                    account.items = account.items?.filter{$0.currencyID == selectedDeposit.currencyID && $0.closeDate == nil}
                }
                for account in accounts{
                    account.items = account.items?.filter{$0.balance != selectedDeposit.balance && $0.closeDate == nil}
                }
                accounts = accounts.filter{$0.items?.count != 0}
                vc.accounts = accounts
            }
            
            if selectedDeposit == nil && selectedDepositTo != nil{
                vc.accounts = accounts.filter{$0.items?.count != 0}
            }
            
            if selectedDeposit != nil && selectedDepositTo != nil{
                for account in accounts{
                    account.items = account.items?.filter{$0.currencyID == selectedDeposit.currencyID && $0.closeDate == nil}
                }
                for account in accounts{
                    account.items = account.items?.filter{$0.balance != selectedDeposit.balance && $0.closeDate == nil}
                }
                vc.accounts = accounts.filter{$0.items?.count != 0}
            }
            vc.selectedDeposit = selectedDepositTo
        }
        //        переход на скрин подтверждения
        if segue.identifier == "toSubmitSegue"{
            let vc = segue.destination as! SubmitTransactionViewController
            vc.delegateTransactions = self
            if self.isSchedule{
                self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
            }else{
                self.schedule = nil
            }
            
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                submitModel = InternalOperationModel(
                    dtAccountNo: selectedDeposit.accountNo,
                    ctAccountNo: simpleAccount.accountNo ,
                    operationType: operationTypeID,
                    operationID: operationID,
                    currencyID: selectedDeposit.currencyID,
                    sum: Double(sumOfTransactionTextField.text ?? "0.0"),
                    comment: descriptionTextField.text,
                    isSchedule: self.isSchedule,
                    isTemplate: self.isCreateTemplate,
                    templateName: self.templateNameTextField.text ?? nil,
                    templateDescription: "Внутрибанковский перевод",
                    scheduleID: 0,
                    schedule: self.schedule ?? nil)
            }
            if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                submitModel = InternalOperationModel(
                    dtAccountNo: selectedDeposit.accountNo,
                    ctAccountNo: selectedDepositTo.accountNo,
                    operationType: operationTypeID,
                    operationID: operationID,
                    currencyID: selectedDeposit.currencyID,
                    sum: Double(sumOfTransactionTextField.text ?? "0.0"),
                    comment: descriptionTextField.text,
                    isSchedule: self.isSchedule,
                    isTemplate: self.isCreateTemplate,
                    templateName: self.templateNameTextField.text ?? nil,
                    templateDescription: "Внутрибанковский перевод",
                    scheduleID: 0,
                    schedule: self.schedule ?? nil)
            }
            
            vc.submitModelInternalOperation = submitModel
            vc.operationID = operationID
            vc.operationTypeID = operationTypeID
            vc.currencyCurrent = selectedDeposit.currencySymbol ?? ""
            vc.selectedDeposit = selectedDeposit
            vc.selectedDepositTo = selectedDepositTo
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            
        }
    }
    //    установака текущего счета
    func selectedDepositUser(deposit: Deposit) {
        
        selectedDeposit = deposit
        if selectedDeposit != nil && selectedDepositTo != nil{
            if self.selectedDepositTo == self.selectedDeposit{
                self.selectedDepositTo = nil
            }
        }else{
            selectedDeposit = deposit
        }
        
    }
    //    установака текущего счета получателя
    func selectedDepositUserTo(deposit: Deposit) {
        
        selectedDepositTo = deposit
        
    }
    // Получение данных для редактирования оперции
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool, operationTypeID: Int) {
        self.operationID = operationID
        self.isRepeatPay = isRepeatPay
        self.isRepeatOperation = isRepeatOperation
        self.operationTypeID = operationTypeID
    }

    // кнопка включения планировщика
    @IBAction func switchPlanner(_ sender: UISwitch) {
        if sender.isOn{
            viewPlanner.isHidden = false
            isSchedule = true
            StartDate = ScheduleHelper.shared.dateCurrentForServer()
            EndDate = ScheduleHelper.shared.dateCurrentForServer()
            dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
            dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
            RecurringTypeID = ScheduleRepeat.EachWeek.rawValue
            
        }else{
            viewPlanner.isHidden = true
            isSchedule = false
        }
    }
}

// выбор периода планировщика
extension TransactionViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //  установка дефолтных значений периода
    func pickerPeriodDate() {
        pickerView.delegate = self
        pickerViewDayOperation.delegate = self
        pickerTarif.delegate = self
        pickerViewWeekDay.delegate = self
        periodTextField.inputView = pickerView
        dayNumberTextField.inputView = pickerViewDayOperation
        weekDayTextField.inputView = pickerViewWeekDay
        pickerView.selectRow(0, inComponent: 0, animated: true)
        pickerViewDayOperation.selectRow(0, inComponent: 0, animated: true)
        pickerViewWeekDay.selectRow(0, inComponent: 0, animated: true)
        pickerTarif.selectRow(0, inComponent: 0, animated: true)
        periodTextField.text = periodList[0]
        dayNumberTextField.text = daysList[0]
        weekDayTextField.text = weekList[0]
        viewDateBegin.isHidden = false
        viewDateFinish.isHidden = false
        viewWeekDay.isHidden = false
        viewDay.isHidden = true
        viewDateOperation.isHidden = true
    }
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count = 0
        if pickerView == self.pickerView {
            count = periodList.count
        } else if pickerView == self.pickerViewDayOperation{
            count = daysList.count
        } else if pickerView == self.pickerViewWeekDay{
            count = weekList.count
        }
        
        return count
    }
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var list = ""
        if pickerView == self.pickerView {
            list = periodList[row]
        } else if pickerView == self.pickerViewDayOperation{
            list = daysList[row]
        } else if pickerView == self.pickerViewWeekDay{
            list = weekList[row]
        }
        return list
    }
    // скрытие ненужных полей
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.pickerView {
            periodTextField.text = periodList[row]
            let formatterToServer = DateFormatter()
            formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"///////////change date format
            formatterToServer.locale = Locale(identifier: "language".localized)
            let dateNow = Date()
            switch row {
                
            case SchedulePickerView.EachWeek.rawValue:
                viewDateBegin.isHidden = false
                viewDateFinish.isHidden = false
                viewWeekDay.isHidden = false
                viewDay.isHidden = true
                viewDateOperation.isHidden = true
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = formatterToServer.string(from: dateNow )
                RecurringTypeID = ScheduleRepeat.EachWeek.rawValue
                ProcessDay = 1
                dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
                dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
                
            case SchedulePickerView.EachMonth.rawValue:
                viewDateBegin.isHidden = false
                viewDateFinish.isHidden = false
                viewWeekDay.isHidden = true
                viewDay.isHidden = false
                viewDateOperation.isHidden = true
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = formatterToServer.string(from: dateNow )
                RecurringTypeID = ScheduleRepeat.EachMonth.rawValue
                ProcessDay = 1
                dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
                dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
                
            case SchedulePickerView.OneTime.rawValue:
                viewDateBegin.isHidden = true
                viewDateFinish.isHidden = true
                viewWeekDay.isHidden = true
                viewDay.isHidden = true
                viewDateOperation.isHidden = false
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = nil
                RecurringTypeID = ScheduleRepeat.OneTime.rawValue
                ProcessDay = 0
                dateOperationTextField.text = ScheduleHelper.shared.dateSchedule()
                
            default:
                viewDateBegin.isHidden = true
                viewDateFinish.isHidden = true
                viewWeekDay.isHidden = true
                viewDay.isHidden = true
                viewDateOperation.isHidden = true
                
            }
        } else if pickerView == self.pickerViewDayOperation{
            dayNumberTextField.text = daysList[row]
            ProcessDay =  row + 1
        } else if pickerView == self.pickerViewWeekDay{
            weekDayTextField.text = weekList[row]
            ProcessDay =  row + 1
        }
        
    }
    // выборка даты
    func showDatePicker(){
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.minimumDate = Date()
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        dateBeginTextField.inputAccessoryView = toolbar
        dateBeginTextField.inputView = datePicker
        dateFinishTextField.inputAccessoryView = toolbar
        dateFinishTextField.inputView = datePicker
        dateOperationTextField.inputAccessoryView = toolbar
        dateOperationTextField.inputView = datePicker
        
    }
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"///////////change date format
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"///////////change date format
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        
        if dateBeginTextField.isFirstResponder {
            dateBeginTextField.text = formatter.string(from: datePicker.date)
            StartDate = formatterToServer.string(from: datePicker.date)
        }
        if dateFinishTextField.isFirstResponder {
            dateFinishTextField.text = formatter.string(from: datePicker.date)
            EndDate = formatterToServer.string(from: datePicker.date)
        }
        if dateOperationTextField.isFirstResponder {
            dateOperationTextField.text = formatter.string(from: datePicker.date)
            StartDate = formatterToServer.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    // проверка на фокус поля ввода
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dateBeginTextField {
            datePicker.datePickerMode = .date
        }
        if textField == dateFinishTextField {
            datePicker.datePickerMode = .date
        }
        if textField == dateOperationTextField {
            datePicker.datePickerMode = .date
        }
    }
}

extension TransactionViewController: UITextFieldDelegate{
    //    проверка поля на начало ввода информации
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField{
        case self.accountToTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewToInternalAccount, labelError: labelErrorInternalAccount)
            
        case self.sumOfTransactionTextField:
            
            viewTextFieldShouldBeginEditing(viewTextField: viewSumFromTextField, labelError: labelErrorSum)
            
        case self.descriptionTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewCommentTextField, labelError: labelErrorComment)
            
        default:
            break
        }
        return true
    }
    //    проверка поля на начало ввода информации на кол-во символов
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        if textField.isEqual(accountToTextField) {
            maxLength = Constants.ACCOUNTS_CHARACTERS_COUNT
        }else if textField.isEqual(sumOfTransactionTextField) {
            maxLength = 9
        }else if textField.isEqual(descriptionTextField) {
            maxLength = 100
        }else if textField.isEqual(templateNameTextField) {
            maxLength = 100
        }
        
        return newLength <= maxLength
        
    }
    //    переход на следкующее поле
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == templateNameTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
}

extension Deposit: Equatable{
    //    проверка на сравнение полей
    public static func == (lhs: Deposit, rhs: Deposit) -> Bool {
        return lhs.currencyID == rhs.currencyID && lhs.accountNo == rhs.accountNo
    }
}

extension TransactionViewController{
    //    получение счетов
    func  getAccountsWithCurrencies(vc: AccountsViewController?) {
        
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        
        let depositObservable:Observable<[Accounts]> = managerApi
            .getAccounts(currencyID:  Constants.NATIONAL_CURRENCY)
        
        
        showLoading()
        
        Observable
            .zip(currenciesObservable, depositObservable)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (Currencies, Accounts) in
                    guard let `self` = self else { return }
                    self.currencies = Currencies
                    if Accounts.count > 0{
                        self.accounts = self.accountsCurrencies(currency: self.currencies, accounts: Accounts)
                        
                        let accCopy = self.accounts.map({$0.copy() as! Accounts})
                        self.accountsClone = accCopy
                        
                        if self.isFullZeroAccount{
                            self.isFullZeroAccount = false
                            if self.currencyIDcurrent != 0 && self.accountNoCurrent != ""{
                                self.selectedDepositTo = self.getCurrentTemplateAccount(accountNo: self.accountNoCurrent, currency: self.currencyIDcurrent, accounts: self.accounts)
                                if self.selectedDepositTo != nil{
                                    self.fullSelectedDepositTo(selectedDeposit: self.selectedDepositTo)
                                }
                            }
                        }
                        if self.isCreateTemplate{
                            if self.submitModel != nil{
                                if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.submitModel.dtAccountNo ?? "", currency: self.submitModel.currencyID ?? 0, accounts: Accounts)
                                    if self.selectedDeposit != nil{
                                        self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                    }
                                    self.selectedDepositTo = self.getCurrentTemplateAccount(accountNo: self.submitModel.ctAccountNo ?? "", currency: self.submitModel.currencyID ?? 0, accounts: Accounts)
                                    if self.selectedDepositTo != nil{
                                        self.fullSelectedDepositTo(selectedDeposit: self.selectedDepositTo)
                                    }
                                }
                            }
                            if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                                if self.submitModel != nil{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.submitModel.dtAccountNo ?? "", currency: self.submitModel.currencyID ?? 0, accounts: Accounts)
                                    
                                    self.selectedDepositTo = self.getCurrentTemplateAccount(accountNo: self.submitModel.ctAccountNo ?? "", currency: self.submitModel.currencyID ?? 0, accounts: Accounts)
                                    
                                    if self.selectedDeposit != nil{
                                        self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                    }
                                    if self.selectedDepositTo != nil{
                                        self.fullSelectedDepositTo(selectedDeposit: self.selectedDepositTo)
                                    }
                                }
                            }
                        }
                        if self.isEditTemplate || self.isRepeatPay{
                            if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                                if self.isRepeatFromStory{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.internalTemplateFromStory.dtAccountNo ?? "", currency: self.internalTemplateFromStory.currencyID ?? 0, accounts: Accounts)
                                    
                                }else{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.template.from ?? "", currency: self.template.currencyID ?? 0, accounts: Accounts)
                                    
                                }
                                if self.selectedDeposit != nil{
                                    self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                }
                                
                            }
                            if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                                if self.isRepeatFromStory{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.internalTemplateFromStory.dtAccountNo ?? "", currency: self.internalTemplateFromStory.currencyID ?? 0, accounts: Accounts)
                                    
                                    self.selectedDepositTo = self.getCurrentTemplateAccount(accountNo: self.internalTemplateFromStory.ctAccountNo ?? "", currency: self.internalTemplateFromStory.currencyID ?? 0, accounts: Accounts)
                                    
                                }else{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.template.from ?? "", currency: self.template.currencyID ?? 0, accounts: Accounts)
                                    
                                    self.selectedDepositTo = self.getCurrentTemplateAccount(accountNo: self.template.to ?? "", currency: self.template.currencyID ?? 0, accounts: Accounts)
                                }
                                if self.selectedDeposit != nil{
                                    self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                }
                                if self.selectedDepositTo != nil{
                                    self.fullSelectedDepositTo(selectedDeposit: self.selectedDepositTo)
                                }
                                
                            }
                        }else{
                            if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                                if self.currencyIDcurrent != 0 && self.accountNoCurrent != ""{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.accountNoCurrent, currency: self.currencyIDcurrent, accounts: self.accounts)
                                    if self.selectedDeposit != nil{
                                        self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                    }
                                }
                            }
                            if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                                if self.isFullAccount{
                                    self.isFullAccount = false
                                    if self.currencyIDcurrent != 0 && self.accountNoCurrent != ""{
                                        self.selectedDepositTo = self.getCurrentTemplateAccount(accountNo: self.accountNoCurrent, currency: self.currencyIDcurrent, accounts: self.accounts)
                                        if self.selectedDepositTo != nil{
                                            self.fullSelectedDepositTo(selectedDeposit: self.selectedDepositTo)
                                        }
                                    }
                                }
                                if self.isWithdrawAccount{
                                    self.isWithdrawAccount = false
                                    if self.currencyIDcurrent != 0 && self.accountNoCurrent != ""{
                                        self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.accountNoCurrent, currency: self.currencyIDcurrent, accounts: self.accounts)
                                        if self.selectedDeposit != nil{
                                            self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                        }
                                    }
                                }
                                
                                if self.selectedDeposit != nil && self.selectedDepositTo == nil{
                                    for account in self.accounts{
                                        account.items = account.items?.filter{$0 != self.selectedDeposit}
                                    }
                                }
                                if self.selectedDeposit != nil && self.selectedDepositTo != nil{
                                    
                                    for account in self.accounts{
                                        account.items = account.items?.filter{$0 != self.selectedDeposit}
                                    }
                                }
                                if self.selectedDeposit == nil && self.selectedDepositTo != nil{
                                    
                                    for account in self.accounts{
                                        account.items = account.items?.filter{$0 != self.selectedDepositTo}
                                    }
                                }
                            }
                        }
                    }else{
                       
                        self.showAlertController(self.localizedText(key: "no_active_accounts_found"))
                    }

                    vc?.accounts = self.accounts
                    vc?.tableView.reloadData()
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    установка валюты
    func accountsCurrencies(currency: [Currency], accounts:[Accounts]) -> [Accounts]{
        
        for account in accounts{
            for item in account.items!{
                for id2 in currency{
                    if item.currencyID == id2.currencyID{
                        item.currency = id2
                    }
                }
            }
        }
        return accounts
    }
    //  получение счета для внутреннего перевода
    func getAccountForInternalTransaction(accountNo: String, currencyID: Int) {
        showLoading()
        var account = accountNo
        if let symbol = accountNo.firstIndex(of: "+") {
            account.remove(at: symbol)
        }
        managerApi
            .getAccountsByAccountNoOrPhone(account: account)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (accounts) in
                    guard let `self` = self else { return }
                    if accounts.isEmpty {
                        self.showAlertController("Клиент не найден")
                        self.hideLoading()
                    } else {
                        self.simpleAccounts = accounts.filter{$0.currencyID == currencyID}
                        self.accountsTableView.isHidden = false
                        self.chooseAccountLabel.isHidden = false
                        self.accountsTableViewConstraint.constant = CGFloat(self.simpleAccounts.count * 100)
                        self.accountTableView.reloadData()
                        self.hideLoading()
                    }
                    
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    проверка счета на доступность
    func checkAccountAvailable(accountNo: String, currencyID: Int){
        managerApi
            .getCheckAccountAvailable(accountNo: accountNo, currencyID: currencyID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (isCheck) in
                    guard let `self` = self else { return }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    создание перевода
    func postCreatInternalTrasaction(InternalOperationModel: InternalOperationModel){
        showLoading()
        managerApi
            .postInternalTransaction(InternalOperationModel: InternalOperationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                    guard let `self` = self else { return }
                    if self.isCreateTemplate || self.isEditTemplate{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                        self.view.window?.rootViewController = vc
                        
                        let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                        let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                        let navigationController = UINavigationController(rootViewController: destinationController)
                        vc.pushFrontViewController(navigationController, animated: true)
                        
                    }else{
                        if operation.operationID != nil{
                            self.operationID = operation.operationID ?? 0
                        }
                            self.allowOperation()
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    создание перевода между счетами
    func postCreatpostInternalOperationCustomerAccounts(InternalOperationModel: InternalOperationModel){
        showLoading()
        managerApi
            .postInternalOperationCustomerAccounts(InternalOperationModel: InternalOperationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                    guard let `self` = self else { return }
                    if self.isCreateTemplate || self.isEditTemplate{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                        self.view.window?.rootViewController = vc
                        
                        let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                        let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                        let navigationController = UINavigationController(rootViewController: destinationController)
                        vc.pushFrontViewController(navigationController, animated: true)
                        
                    }else{
                        if operation.operationID != nil{
                            self.operationID = operation.operationID ?? 0
                        }
                        self.allowOperation()
                    }
                    self.hideLoading()
                    
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    // получение данных операции перевода
    func  getInfoOperationTransactions(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationTransactions(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    
                    if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                       self.accountToView.isHidden = true
                        
                            self.templateNameTextField.text = operation.templateName
                            self.accountToTextField.text = operation.dtAccountNo
                            self.sumOfTransactionTextField.text = "\(String(format:"%.2f",operation.sum ?? 0.0 ))"
                            self.descriptionTextField.text = operation.comment
                            self.getAccountForInternalTransaction(accountNo: operation.dtAccountNo ?? "", currencyID: operation.currencyID ?? 0)
                    }
                    if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                        self.internalView.isHidden = true
                            self.templateNameTextField.text = operation.templateName
                            self.sumOfTransactionTextField.text = "\(String(format:"%.2f", operation.sum ?? 0.0 ))"
                            self.descriptionTextField.text = operation.comment
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    
    //    заполнение текущего счета
    func fullSelectedDeposit(selectedDeposit: Deposit){
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        self.accountNoLabel.text = fullAccountsText
        self.accountSumLabel.text = "\(String(format:"%.2f", selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
        for currency in self.currencyLabel{
            currency.text = selectedDeposit.currency?.symbol
        }
        self.currencyIDcurrent = selectedDeposit.currencyID ?? 0
        
    }
    //    заполнение текущего счета
    func fullSelectedDepositTo(selectedDeposit: Deposit){
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        self.payTo_accountLabel.text = fullAccountsText
        self.choiceCardTo_accountLabel.text = "\(String(format:"%.2f", selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
        for currency in self.currencyLabel{
            currency.text = selectedDeposit.currency?.symbol
        }
        self.currencyIDcurrent = selectedDeposit.currencyID ?? 0
        
    }
}

extension TransactionViewController{
    // проверка для прав на создание и подтверждние
    func  allowToCreatAndSaveOperation(button: UIButton) {
        
        
        if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed") && AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "execute_operation"),for: .normal)
        }else if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed") && !AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "Resources_Common_Save"),for: .normal)
        }
    }
    
//    проверка прав
    func  allowOperation() {
        
        if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed") && AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsApproveAllowed"){
            self.performSegue(withIdentifier: "toSubmitSegue", sender: self)
            
        }else if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed") && !AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsApproveAllowed"){
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
            self.view.window?.rootViewController = vc
            
            let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
            let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
            let navigationController = UINavigationController(rootViewController: destinationController)
            vc.pushFrontViewController(navigationController, animated: true)
            
        }
    }
    
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return simpleAccounts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountsCell", for: indexPath) as! AccountsCell
        
        cell.accountNameLabel.text = simpleAccounts[indexPath.row].name
        let copy = simpleAccounts[indexPath.row].copy() as! SimpleAccountModel
        let firstSymbols = copy.accountNo?.prefix(2)
        let lastSymbols = copy.accountNo?.suffix(2)
        cell.accountNumberLabel.text = "\(firstSymbols!)*********\(lastSymbols!) \(copy.currencySymbol!)"
        if simpleAccount != nil {
            if simpleAccounts[indexPath.row].accountNo == simpleAccount.accountNo{
                cell.isSelected = true
                cell.setSelected(true, animated: false)
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
        return cell
    }
    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        simpleAccount = self.simpleAccounts[indexPath.row]
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
