//
//  DetailCliringGrossTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/13/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

class DetailClearingGrossTableViewCell: UITableViewCell {
    
    @IBOutlet weak var paymentTypeLabel: UILabel!
    @IBOutlet weak var paymentDirectionLabel: UILabel!
    @IBOutlet weak var PNumberLabel: UILabel!
    @IBOutlet weak var paymentDateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusDateLabel: UILabel!
    @IBOutlet weak var transferSummLabel: UILabel!
    @IBOutlet weak var FAButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
