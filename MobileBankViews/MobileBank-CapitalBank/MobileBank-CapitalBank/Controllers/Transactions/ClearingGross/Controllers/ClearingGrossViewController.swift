//
//  ClearingGrossViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/5/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa


class ClearingGrossViewController: BaseViewController, DepositProtocol, ClearingGrossAccountsProtocol, BikCodesProtocol, EditCliringOperationProtocol {
    
    @IBOutlet weak var SenderFullNameLabel: UILabel!
    @IBOutlet weak var PaymentTypeIDTextField: UITextField!
    @IBOutlet weak var PNumberTextFields: UITextField!
    @IBOutlet weak var paymentDateTextField: UITextField!
    @IBOutlet weak var TransferSummTextField: UITextField!
    @IBOutlet weak var PaymentCodeTextField: UITextField!
    @IBOutlet weak var PaymentCodeDescriptionLabel: UILabel!
    @IBOutlet weak var PaymentCommentTextField: UITextField!
    @IBOutlet weak var ReceiverFullNameTextField: UITextField!
    @IBOutlet weak var ReceiverAccountNoTextField: UITextField!
    @IBOutlet weak var ReceiverBIKTextField: UITextField!
    @IBOutlet weak var BikDescriptionLabel: UILabel!
    @IBOutlet weak var payFrom_accountsLabel: UILabel!
    @IBOutlet weak var choiceCard_accountsLabel: UILabel!
    @IBOutlet weak var ccurrencySymbolLabel: UILabel!
    
    // views error
    @IBOutlet weak var viewPNumberTextFields: UIView!
    @IBOutlet weak var viewFromAccount: UIView!
    @IBOutlet weak var viewTransferSummTextField: UIView!
    @IBOutlet weak var viewPaymentCodeTextField: UIView!
    @IBOutlet weak var viewPaymentCommentTextField: UIView!
    @IBOutlet weak var viewReceiverFullNameTextField: UIView!
    @IBOutlet weak var viewReceiverAccountNoTextField: UIView!
    @IBOutlet weak var viewReceiverBIKTextField: UIView!
    
    //label error
    @IBOutlet weak var labelErrorPNumber: UILabel!
    @IBOutlet weak var labelErrorFromAccount: UILabel!
    @IBOutlet weak var labelErrorTransferSumm: UILabel!
    @IBOutlet weak var labelErrorPaymentCode: UILabel!
    @IBOutlet weak var labelErrorPaymentComment: UILabel!
    @IBOutlet weak var labelErrorReceiverFullName: UILabel!
    @IBOutlet weak var labelErrorReceiverAccountNo: UILabel!
    @IBOutlet weak var labelErrorReceiverBIK: UILabel!
    
    //Template IBOutlets
    var isCreateTemplate = false
    var isSchedule = false
    var StartDate = ""
    var EndDate: String? = ""
    var RecurringTypeID = 2
    var ProcessDay = 1
    var schedule: Schedule!
    
    //EditingTemplate
    var isEditTemplate = false
    var template: ContractModelTemplate!
    //RepeatPayTemplate
    var isRepeatPay = false
    var isRepeatFromStory = false
    var isRepeatOperation = false
    var isChangeData = false
    var paymentID = 0
    
    @IBOutlet weak var labelErrorTemplateName: UILabel!
    @IBOutlet weak var viewTemplateNameTextFields: UIView!
    @IBOutlet weak var TemplateDescriptionView: UIView!
    @IBOutlet weak var ScheduleView: UIView!
    @IBOutlet weak var templateNameTextField: UITextField!
    
    @IBOutlet weak var switchPlanner: UISwitch!
    @IBOutlet weak var viewPlanner: UIView!
    @IBOutlet weak var periodTextField: UITextField!
    
    @IBOutlet weak var viewDateBegin: UIView!
    @IBOutlet weak var viewDateFinish: UIView!
    @IBOutlet weak var viewWeekDay: UIView!
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var viewDateOperation: UIView!
    
    @IBOutlet weak var dateBeginTextField: UITextField!
    @IBOutlet weak var dateFinishTextField: UITextField!
    
    @IBOutlet weak var dayNumberTextField: UITextField!
    @IBOutlet weak var dateOperationTextField: UITextField!
    @IBOutlet weak var weekDayTextField: UITextField!
    
    @IBOutlet weak var createSwiftButton: RoundedButton!
    var typeOperation = ""
    
    var pickerView = UIPickerView()
    var pickerViewDayOperation = UIPickerView()
    var pickerViewWeekDay = UIPickerView()
    var pickerTarif = UIPickerView()
    
    var periodList = ["EachWeek".localized, "EachMonth".localized, "OneTime".localized]
    var daysList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20","21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
    var weekList = ["monday".localized, "tuesday".localized, "wednesday".localized, "thursday".localized, "friday".localized, "saturday".localized, "sunday".localized]
    var datePicker = UIDatePicker()
    
    
    var countOfPaymentTypeID = PaymentTypeID.allCases.count
    var clearingGrossOperationModel: ClearingGrossOperationModel!
    var clearingGrossNeedChangeDateModel: ClearingGrossNeedChangeDateModel!
    var clearingGrossValidateData: InternetBankingClearingGrossValidateDataModel!
    var clearingGrossPaymenModel: ClearingGrossPaymenModel!
    
    var depositsWithCurrencies = [Deposit]()
    var currencies = [Currency]()
    var deposits = [Deposit]()
    var clearingGrossAccounts: [Accounts]!
    var selectedDeposit: Deposit!
    
    var paymentsCodes: [PaymentCode]!
    var paymentCode: PaymentCode!
    var bikCodes: [BikCode]!
    var bikCode: BikCode!
    
    var clearingGrossCurrency = 0 //не присылают в getInfoOperation
    var operationID = 0
    var operationTypeID = 0
    var submitModel: ClearingGrossOperationModel!
    var navigationTitle = ""
    
    var checkSum = 0
    var checkSumString = ""
    var paymentType = 1
    var newDate = ""
    var clearingMaxAmount: Int!
    var dateFrom: String!
    var dateTo: String!
    
    var currencyIDcurrent = 0
    var accountNoCurrent = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        allowToCreatAndSaveOperation(button: createSwiftButton)
        
        if isCreateTemplate{
            if bikCode != nil{
                ReceiverBIKTextField.text = bikCode.code
                BikDescriptionLabel.text = bikCode.name
                ReceiverBIKTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                
                if ReceiverBIKTextField.text?.prefix(3) != ReceiverAccountNoTextField.text?.prefix(3){
                    showAlertController(localizedText(key: "first_3_must_be_same_as_in_bik"))
                }
            }else{
                BikDescriptionLabel.text = ""
            }
            if paymentCode != nil{
                PaymentCodeTextField.text = paymentCode.code
                PaymentCodeDescriptionLabel.text = paymentCode.description
                PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                
            }else{
                PaymentCodeDescriptionLabel.text = ""
            }
            
            if submitModel != nil{
                if submitModel.paymentTypeID == 1{
                    PaymentTypeIDTextField.text = "CliringTransfer".localized
                }else{
                    PaymentTypeIDTextField.text = "GrossTransfer".localized
                }
                TransferSummTextField.text = "\(String(format:"%.2f",submitModel.transferSumm ?? 0.0 ))"
                SenderFullNameLabel.text = submitModel.senderFullName
                PNumberTextFields.text = submitModel.pNumber
                paymentDateTextField.text =  ScheduleHelper.shared.dateForFullSchedule(stringDate: self.submitModel.paymentDate ?? "") //submitModel.paymentDate
                PaymentCodeTextField.text = submitModel.paymentCode
                PaymentCommentTextField.text = submitModel.paymentComment
                ReceiverFullNameTextField.text = submitModel.receiverFullName
                ReceiverAccountNoTextField.text = submitModel.receiverAccountNo
                ReceiverBIKTextField.text = submitModel.receiverBIK
                
                operationTypeID = submitModel.operationType ?? 0
                paymentType = submitModel.paymentTypeID ?? 0
            }
            
            if self.selectedDeposit != nil{
                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
            }
            createSwiftButton.setTitle(localizedText(key: "save_template"),for: .normal)
            
        }else if isEditTemplate{
            TemplateDescriptionView.isHidden = false
            ScheduleView.isHidden = false
           
            RecurringTypeID = template.schedule?.recurringTypeID ?? 1
            if template.isSchedule ?? false{
                isSchedule = true
                StartDate = template.schedule?.startDate ?? ""
                EndDate = template.schedule?.endDate
                RecurringTypeID = template.schedule?.recurringTypeID ?? 0
                ProcessDay = template.schedule?.processDay ?? 0
                switchPlanner.isOn = true
                viewPlanner.isHidden = false
                
                let schedule = ScheduleHelper.shared.scheduleFullScheduleHelper(
                    StartDate: template.schedule?.startDate,
                    EndDate: template.schedule?.endDate,
                    RecurringTypeID: template.schedule?.recurringTypeID,
                    ProcessDay: template.schedule?.processDay)
                
                switch RecurringTypeID {
                case ScheduleRepeat.OneTime.rawValue:
                    viewDateBegin.isHidden = true
                    viewDateFinish.isHidden = true
                    viewWeekDay.isHidden = true
                    viewDay.isHidden = true
                    viewDateOperation.isHidden = false
                    periodTextField.text = periodList[2]
                    dateOperationTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                    
                case ScheduleRepeat.EachWeek.rawValue:
                    viewDateBegin.isHidden = false
                    viewDateFinish.isHidden = false
                    viewWeekDay.isHidden = false
                    viewDay.isHidden = true
                    viewDateOperation.isHidden = true
                    periodTextField.text = periodList[0]
                    dateBeginTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                    dateFinishTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.endDate ?? "")
                    weekDayTextField.text = weekList[(schedule?.processDay ?? 1) - 1]
                    
                case ScheduleRepeat.EachMonth.rawValue:
                    viewDateBegin.isHidden = false
                    viewDateFinish.isHidden = false
                    viewWeekDay.isHidden = true
                    viewDay.isHidden = false
                    viewDateOperation.isHidden = true
                    periodTextField.text = periodList[1]
                    dateBeginTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                    dateFinishTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.endDate ?? "")
                    dayNumberTextField.text = daysList[(schedule?.processDay ?? 1) - 1]
                    
                default:
                    viewDateBegin.isHidden = true
                    viewDateFinish.isHidden = true
                    viewWeekDay.isHidden = true
                    viewDay.isHidden = true
                    viewDateOperation.isHidden = true
                }
            }
            
            
            if bikCode != nil{
                ReceiverBIKTextField.text = bikCode.code
                BikDescriptionLabel.text = bikCode.name
                ReceiverBIKTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                
                if ReceiverBIKTextField.text?.prefix(3) != ReceiverAccountNoTextField.text?.prefix(3){
                    showAlertController(localizedText(key: "first_3_must_be_same_as_in_bik"))
                }
            }else{
                BikDescriptionLabel.text = ""
            }
            if paymentCode != nil{
                PaymentCodeTextField.text = paymentCode.code
                PaymentCodeDescriptionLabel.text = paymentCode.description
                PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                
            }else{
                PaymentCodeDescriptionLabel.text = ""
            }
            
            if self.selectedDeposit != nil{
                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
            }
            createSwiftButton.setTitle(localizedText(key: "edit_template"),for: .normal)
            
        }else if isRepeatPay{
            TemplateDescriptionView.isHidden = true
            ScheduleView.isHidden = true

            if clearingGrossOperationModel != nil{
                if clearingGrossOperationModel.paymentTypeID == 1{
                    PaymentTypeIDTextField.text = "CliringTransfer".localized
                }else{
                    PaymentTypeIDTextField.text = "GrossTransfer".localized
                }
            }
            
            if self.selectedDeposit != nil{
                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
            }
            if bikCode != nil{
                ReceiverBIKTextField.text = bikCode.code
                BikDescriptionLabel.text = bikCode.name
                ReceiverBIKTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                
                if ReceiverBIKTextField.text?.prefix(3) != ReceiverAccountNoTextField.text?.prefix(3){
                    showAlertController(localizedText(key: "first_3_must_be_same_as_in_bik"))
                }
            }else{
                if clearingGrossOperationModel != nil{
                    for code in self.bikCodes{
                        if code.code == self.clearingGrossOperationModel.receiverBIK{
                            ReceiverBIKTextField.text = code.code
                            BikDescriptionLabel.text = code.name
                        }
                    }
                }
            }
            if paymentCode != nil{
                PaymentCodeTextField.text = paymentCode.code
                PaymentCodeDescriptionLabel.text = paymentCode.description
                PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                
            }else{
                if clearingGrossOperationModel != nil{
                    for code in self.paymentsCodes{
                        if code.code == self.clearingGrossOperationModel.paymentCode{
                            PaymentCodeTextField.text = code.code
                            PaymentCodeDescriptionLabel.text = code.description
                        }
                    }
                }
            }
          
            if self.selectedDeposit != nil{
                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
            }
            createSwiftButton.setTitle(localizedText(key: "transfer"),for: .normal)
            
        }else{
            
            if bikCode != nil{
                ReceiverBIKTextField.text = bikCode.code
                BikDescriptionLabel.text = bikCode.name
                ReceiverBIKTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                
                if ReceiverBIKTextField.text?.prefix(3) != ReceiverAccountNoTextField.text?.prefix(3){
                    showAlertController(localizedText(key: "first_3_must_be_same_as_in_bik"))
                }
            }else{
                BikDescriptionLabel.text = ""
            }
            if paymentCode != nil{
                PaymentCodeTextField.text = paymentCode.code
                PaymentCodeDescriptionLabel.text = paymentCode.description
                PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                
            }else{
                PaymentCodeDescriptionLabel.text = ""
            }
           
//            PaymentTypeIDTextField.text = PaymentTypeID.allCases[0].rawValue
            
            if selectedDeposit != nil{
                fullSelectedDeposit(selectedDeposit: selectedDeposit)
            }else{
                payFrom_accountsLabel.text = localizedText(key: "AccountNo")
                choiceCard_accountsLabel.text = localizedText(key: "select_account_or_card")
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        periodList = [localizedText(key: "every_week"),localizedText(key: "every_month"), NSLocalizedString("_retry", comment: "Однократно")]
        weekList = [localizedText(key: "mon"),
                    localizedText(key:"tue"),
                    localizedText(key:"wed"),
                    localizedText(key:"th"),
                    localizedText(key:"fri"),
                    localizedText(key:"sat"),
                    localizedText(key:"the_sun")]
       
        dateFrom = "2000-01-01T00:00:00"
        dateTo = "2000-01-01T00:00:00"
        if navigationTitle == ""{
            navigationItem.title = localizedText(key: "clearing_gross_transfer")
        }else{
            navigationItem.title = navigationTitle
        }
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        self.paymentType = 1
        self.operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
        self.PaymentTypeIDTextField.text = self.localizedText(key: "clearing_payment")
        
        if isCreateTemplate{
            TemplateDescriptionView.isHidden = false
            ScheduleView.isHidden = false
        }else{
            TemplateDescriptionView.isHidden = true
            ScheduleView.isHidden = true
        }
        
        if let bikCodes = UserDefaultsHelper.bikCodes {
            self.bikCodes = bikCodes
        }
        if let paymentsCodes = UserDefaultsHelper.paymentsCodes {
            self.paymentsCodes = paymentsCodes
        }
        
       
        
        if self.currencyIDcurrent != 0 && self.accountNoCurrent != ""{
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue {
                self.paymentType = 1
                self.operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
                self.PaymentTypeIDTextField.text = self.localizedText(key: "clearing_payment")
            }
            if self.operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue {
                self.paymentType = 2
                self.operationTypeID = InternetBankingOperationType.GrossTransfer.rawValue
                self.PaymentTypeIDTextField.text = self.localizedText(key: "gross_payment")
            }
        }else{
             operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
        }
        
        hideKeyboardWhenTappedAround()
        templateNameTextField.delegate = self
        TransferSummTextField.delegate = self
        PaymentCodeTextField.delegate = self
        PaymentCodeTextField.delegate = self
        PNumberTextFields.delegate = self
        PaymentCommentTextField.delegate = self
        ReceiverFullNameTextField.delegate = self
        ReceiverAccountNoTextField.delegate = self
        ReceiverBIKTextField.delegate = self
        
        getClearingGrossAccounts()
        SenderFullNameLabel.text = SessionManager.shared.user?.userdisplayname
        
        TransferSummTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControl.Event.editingDidEnd)
        PaymentCodeTextField.addTarget(self, action: #selector(textFieldEditingEnd(_:)), for: UIControl.Event.editingDidEnd)
        ReceiverBIKTextField.addTarget(self, action: #selector(textFieldEditingEndBik(_:)), for: UIControl.Event.editingDidEnd)
        PNumberTextFields.addTarget(self, action: #selector(textFieldEditingDidEndPNumber(_:)), for: UIControl.Event.editingDidEnd)
        PaymentCommentTextField.addTarget(self, action: #selector(textFieldEditingEndPaymentComment(_:)), for: UIControl.Event.editingDidEnd)
        ReceiverFullNameTextField.addTarget(self, action: #selector(textFieldEditingDidEndReceiverFullName(_:)), for: UIControl.Event.editingDidEnd)
        ReceiverAccountNoTextField.addTarget(self, action: #selector(textFieldEditingEndReceiverAccountNo(_:)), for: UIControl.Event.editingDidEnd)
        
        templateNameTextField.addTarget(self, action: #selector(textFieldEditingEndtemplateName(_:)), for: UIControl.Event.editingDidEnd)
        
        //schedule
        viewPlanner.isHidden = true
        viewDateBegin.isHidden = true
        viewDateFinish.isHidden = true
        viewWeekDay.isHidden = true
        viewDay.isHidden = true
        
        viewDateOperation.isHidden = true
        dateBeginTextField.delegate = self
        dateFinishTextField.delegate = self
        dayNumberTextField.delegate = self
        paymentDateTextField.delegate = self
        weekDayTextField.delegate = self
        pickerPeriodDate()
        if clearingGrossOperationModel != nil{
            if clearingGrossOperationModel.operationID == 0{
              
                if  self.clearingGrossOperationModel != nil{
                    if self.clearingGrossOperationModel.paymentTypeID == 1{
                        self.PaymentTypeIDTextField.text = self.localizedText(key: "clearing_payment")
                    }else{
                        self.PaymentTypeIDTextField.text = self.localizedText(key: "gross_payment")
                    }
                    self.paymentType =  self.clearingGrossOperationModel.paymentTypeID ?? 1
                    self.TransferSummTextField.text = "\(String(format:"%.2f", self.clearingGrossOperationModel.transferSumm ?? 0.0 ))"
                    self.SenderFullNameLabel.text = self.clearingGrossOperationModel.senderFullName
                    self.paymentDateTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: self.clearingGrossOperationModel.paymentDate ?? "")
                    self.PNumberTextFields.text = self.clearingGrossOperationModel.pNumber
                    self.PaymentCodeTextField.text = self.clearingGrossOperationModel.paymentCode
                    self.PaymentCommentTextField.text = self.clearingGrossOperationModel.paymentComment
                    self.ReceiverFullNameTextField.text = self.clearingGrossOperationModel.receiverFullName
                    self.ReceiverAccountNoTextField.text = self.clearingGrossOperationModel.receiverAccountNo
                    self.ReceiverBIKTextField.text = self.clearingGrossOperationModel.receiverBIK
                    self.templateNameTextField.text = self.clearingGrossOperationModel.templateName
                    self.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
                    self.operationID =  self.clearingGrossOperationModel.operationID ?? 0
                    for code in self.bikCodes{
                        if code.code == self.clearingGrossOperationModel.receiverBIK{
                            self.BikDescriptionLabel.text = code.name
                        }
                    }
                    for code in self.paymentsCodes{
                        if code.code == self.clearingGrossOperationModel.paymentCode{
                            self.PaymentCodeDescriptionLabel.text = code.description
                        }
                    }
                }
            }else{
                getClearingGrossInfo(operationID: clearingGrossOperationModel.operationID ?? 0)
            }
        }else{
            if paymentID != 0{
                getInfoOperationClearingGrossbyPaymentID(paymentID: paymentID)
            }
        }
        if template != nil{
            getClearingGrossInfo(operationID: template.operationID ?? 0)
        }
    }
    
    // Проверка поля "Название шаблона" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndtemplateName(_ sender: Any) {
        templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
    }
    // Проверка поля "Сумма" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChange(_ sender: Any) {
        TransferSummTextField.stateIfEmpty(view: viewTransferSummTextField, labelError: labelErrorTransferSumm)
        let textNonZeroFirst = TransferSummTextField.text
        let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
        let numberAsInt = Double(newString ?? "0")
        TransferSummTextField.text = "\(numberAsInt ?? 0)"
        if selectedDeposit != nil{
            if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                showAlertController(localizedText(key: "insufficient_account_balance"))
                TransferSummTextField.text = ""
            }
        }
        
        if let checkSumm: Int = Int(TransferSummTextField.text ?? "0"){
            if clearingMaxAmount > 0 {
                if PaymentTypeIDTextField.text == localizedText(key: "clearing_payment") && checkSumm >= clearingMaxAmount{
                    
                    let alertController = UIAlertController(title: nil, message: LocalizeHelper.shared.addWord("\(clearingMaxAmount ?? 0)", localizedText(key: "sum_above_max_unavailable_with_clearing")) , preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.PaymentTypeIDTextField.becomeFirstResponder()
                    }
                    alertController.addAction(action)
                    
                    present(alertController, animated: true, completion: nil)
                }
            }
        }
        
    }
    //    проверка на потерю фокуса
    @objc func textFieldEditingEnd(_ sender: Any) {
        
        if paymentsCodes.contains(where: { $0.code == PaymentCodeTextField.text }) {
            for paymentCode in paymentsCodes{
                if paymentCode.code == PaymentCodeTextField.text{
                    self.paymentCode = paymentCode
                    self.PaymentCodeDescriptionLabel.text = paymentCode.description
                }
            }
        } else {
            showAlertController(localizedText(key: "invalid_payment_code"))
        }
        PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
        
    }
    //    проверка на потерю фокуса BIK
    @objc func textFieldEditingEndBik(_ sender: Any) {
        if bikCodes.contains(where: { $0.code == ReceiverBIKTextField.text }) {
            print("success")
            for bikCode in bikCodes{
                if bikCode.code == ReceiverBIKTextField.text{
                    self.bikCode = bikCode
                    self.BikDescriptionLabel.text = bikCode.name
                }
            }
            if ReceiverBIKTextField.text?.prefix(3) != ReceiverAccountNoTextField.text?.prefix(3){
                showAlertController(localizedText(key: "first_3_must_be_same_as_in_bik"))
                ReceiverBIKTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
            }
        } else {
            if ReceiverBIKTextField.text != ""{
                showAlertController(localizedText(key: "invalid_recipient_bik"))
            }
        }
        ReceiverBIKTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
        
    }
    //    проверка на потерю фокуса PNumber
    @objc func textFieldEditingDidEndPNumber(_ sender: Any) {
//        self.paymentCode.code = PNumberTextFields.text
        PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
        
    }
    //    проверка на потерю фокуса PaymentComment
    @objc func textFieldEditingEndPaymentComment(_ sender: Any) {
        PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
        
    }
    //    проверка на потерю фокуса ReceiverFullName
    @objc func textFieldEditingDidEndReceiverFullName(_ sender: Any) {
        ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
        
    }
    //    проверка на потерю фокуса AccountNo
    @objc func textFieldEditingEndReceiverAccountNo(_ sender: Any) {
        ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
        
    }
    //    обработка кнопки сохранить
    @IBAction func buttonSave(_ sender: Any) {
        
        if  paymentDateTextField.text != "" &&
            PNumberTextFields.text != "" &&
            SenderFullNameLabel.text != "" &&
            ReceiverFullNameTextField.text != "" &&
            ReceiverBIKTextField.text != "" &&
            selectedDeposit != nil &&
            ReceiverAccountNoTextField.text != "" &&
            PaymentCommentTextField.text != "" &&
            PaymentCodeTextField.text != ""
        {
            if isCreateTemplate{
                if  paymentDateTextField.text != "" &&
                    PNumberTextFields.text != "" &&
                    SenderFullNameLabel.text != "" &&
                    ReceiverFullNameTextField.text != "" &&
                    ReceiverBIKTextField.text != "" &&
                    selectedDeposit != nil &&
                    ReceiverAccountNoTextField.text != "" &&
                    PaymentCommentTextField.text != "" &&
                    PaymentCodeTextField.text != "" &&
                    templateNameTextField.text != ""
                {
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: self.operationID , startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    clearingGrossOperationModel = ClearingGrossOperationModel(
                        paymentDate: convertDateFormatter(date: paymentDateTextField.text ?? ""),
                        pNumber: PNumberTextFields.text,
                        paymentTypeID: Int(paymentType),
                        senderFullName: SenderFullNameLabel.text,
                        receiverFullName: ReceiverFullNameTextField.text,
                        receiverBIK: ReceiverBIKTextField.text,
                        senderAccountNo: selectedDeposit.accountNo,
                        receiverAccountNo: ReceiverAccountNoTextField.text,
                        paymentComment: PaymentCommentTextField.text,
                        paymentCode: PaymentCodeTextField.text,
                        transferSumm: Double(TransferSummTextField?.text ?? "0.0"),
                        operationType: self.operationTypeID,
                        operationID: 0,
                        isSchedule: false,
                        isTemplate: true,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Клиринг/Гросс",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                    
                    self.postCreatClearingGross(clearingGrossOperationModel: clearingGrossOperationModel)
                }else{
                    viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
                    TransferSummTextField.stateIfEmpty(view: viewTransferSummTextField, labelError: labelErrorTransferSumm)
                    PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                    ReceiverBIKTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                    PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
                    PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
                    ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
                    ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }else if isEditTemplate{
                if self.isSchedule{
                    self.schedule = Schedule(operationID: self.operationID , startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                }else{
                    self.schedule = nil
                }
                clearingGrossOperationModel = ClearingGrossOperationModel(
                    paymentDate: convertDateFormatter(date: paymentDateTextField.text ?? ""),
                    pNumber: PNumberTextFields.text,
                    paymentTypeID: Int(paymentType),
                    senderFullName: SenderFullNameLabel.text,
                    receiverFullName: ReceiverFullNameTextField.text,
                    receiverBIK: ReceiverBIKTextField.text,
                    senderAccountNo: selectedDeposit.accountNo,
                    receiverAccountNo: ReceiverAccountNoTextField.text,
                    paymentComment: PaymentCommentTextField.text,
                    paymentCode: PaymentCodeTextField.text,
                    transferSumm: Double(TransferSummTextField?.text ?? "0.0"),
                    operationType: self.operationTypeID,
                    operationID: template.operationID,
                    isSchedule: false,
                    isTemplate: true,
                    templateName: self.templateNameTextField.text ?? nil,
                    templateDescription: "Клиринг/Гросс",
                    scheduleID: 0,
                    schedule: self.schedule ?? nil)
                
                self.postCreatClearingGross(clearingGrossOperationModel: clearingGrossOperationModel)
                
            }else{
                if self.isSchedule{
                    self.schedule = Schedule(operationID: self.operationID , startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                }else{
                    self.schedule = nil
                }
                if isChangeData{
                    clearingGrossOperationModel = ClearingGrossOperationModel(
                        paymentDate: convertDateFormatter(date: paymentDateTextField.text ?? ""),
                        pNumber: PNumberTextFields.text,
                        paymentTypeID: Int(paymentType),
                        senderFullName: SenderFullNameLabel.text,
                        receiverFullName: ReceiverFullNameTextField.text,
                        receiverBIK: ReceiverBIKTextField.text,
                        senderAccountNo: selectedDeposit.accountNo,
                        receiverAccountNo: ReceiverAccountNoTextField.text,
                        paymentComment: PaymentCommentTextField.text,
                        paymentCode: PaymentCodeTextField.text,
                        transferSumm: Double(TransferSummTextField?.text ?? "0.0"),
                        operationType: self.operationTypeID,
                        operationID: self.clearingGrossOperationModel.operationID,
                        isSchedule: false,
                        isTemplate: self.isCreateTemplate,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Клиринг/Гросс",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                }else{
                    clearingGrossOperationModel = ClearingGrossOperationModel(
                        paymentDate: convertDateFormatter(date: paymentDateTextField.text ?? ""),
                        pNumber: PNumberTextFields.text,
                        paymentTypeID: Int(paymentType),
                        senderFullName: SenderFullNameLabel.text,
                        receiverFullName: ReceiverFullNameTextField.text,
                        receiverBIK: ReceiverBIKTextField.text,
                        senderAccountNo: selectedDeposit.accountNo,
                        receiverAccountNo: ReceiverAccountNoTextField.text,
                        paymentComment: PaymentCommentTextField.text,
                        paymentCode: PaymentCodeTextField.text,
                        transferSumm: Double(TransferSummTextField?.text ?? "0.0"),
                        operationType: self.operationTypeID,
                        operationID: 0,
                        isSchedule: false,
                        isTemplate: self.isCreateTemplate,
                        templateName: self.templateNameTextField.text ?? nil,
                        templateDescription: "Клиринг/Гросс",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil)
                }
                
                clearingGrossNeedChangeDateModel = ClearingGrossNeedChangeDateModel(
                    CreateDate: currentDateForServer(),
                    PNumber: PNumberTextFields.text,
                    PaymentCode: PaymentCodeTextField.text,
                    PaymentComment: PaymentCommentTextField.text,
                    PaymentDate: convertDateFormatter(date: paymentDateTextField.text ?? ""),
                    PaymentTypeID: String(paymentType),
                    ReceiverAccountNo: ReceiverAccountNoTextField.text,
                    ReceiverBIK: ReceiverBIKTextField.text,
                    ReceiverFullName: ReceiverFullNameTextField.text,
                    SenderAccountNo: selectedDeposit.accountNo,
                    SenderFullName: SenderFullNameLabel.text,
                    TransferSumm: Double(TransferSummTextField.text ?? "0.0"))
                
                
                postIsNeedChangeDate(
                    clearingGrossNeedChangeDateModel: clearingGrossNeedChangeDateModel,
                    clearingGrossOperationModel: clearingGrossOperationModel)
            }
            
        }else{
            viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
            TransferSummTextField.stateIfEmpty(view: viewTransferSummTextField, labelError: labelErrorTransferSumm)
            PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
            ReceiverBIKTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
            PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
            PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
            ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
            ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
            
            
            showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
        }
    }
    //MARK: - Prepare Segue
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCGAccountsSegue"{
            let vc = segue.destination as! AccountsViewController
            if clearingGrossAccounts != nil {
                for account in clearingGrossAccounts{
                    account.items = account.items?.filter{$0.balance != 0.0}
                }
            }
            clearingGrossAccounts = clearingGrossAccounts.filter{$0.items?.count != 0}
            vc.accounts = clearingGrossAccounts
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "write_off")
            vc.selectedDeposit = selectedDeposit
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            
        }
        if segue.identifier == "toCGPaymentsCodeSegue"{
            let vc = segue.destination as! PaymentCodesViewController
            vc.paymentsCodes = paymentsCodes
            vc.paymentCode = paymentCode
            vc.delegate = self
        }
        
        if segue.identifier == "toCGBikCodeSegue"{
            let vc = segue.destination as! BikCodesViewController
            vc.bikCodes = bikCodes
            vc.bikCode = bikCode
            vc.delegate = self
        }
        
        if segue.identifier == "toSubmitSegue"{
            let vc = segue.destination as! SubmitTransactionViewController
            vc.delegate = self
            if operationID != 0{
                submitModel = ClearingGrossOperationModel(
                    paymentDate: newDate,
                    pNumber: PNumberTextFields.text,
                    paymentTypeID: Int(paymentType),
                    senderFullName: SenderFullNameLabel.text,
                    receiverFullName: ReceiverFullNameTextField.text,
                    receiverBIK: ReceiverBIKTextField.text,
                    senderAccountNo:  selectedDeposit.accountNo,
                    receiverAccountNo: ReceiverAccountNoTextField.text,
                    paymentComment: PaymentCommentTextField.text,
                    paymentCode: PaymentCodeTextField.text,
                    transferSumm:  Double(TransferSummTextField.text  ?? "0.0"),
                    operationType: 0,
                    operationID: operationID,
                    isSchedule: false,
                    isTemplate: self.isCreateTemplate,
                    templateName: self.templateNameTextField.text ?? nil,
                    templateDescription: "Клиринг/Гросс",
                    scheduleID: 0,
                    schedule: self.schedule ?? nil)
            }else{
                submitModel = ClearingGrossOperationModel(
                    paymentDate: newDate,
                    pNumber: PNumberTextFields.text,
                    paymentTypeID: Int(paymentType),
                    senderFullName: SenderFullNameLabel.text,
                    receiverFullName: ReceiverFullNameTextField.text,
                    receiverBIK: ReceiverBIKTextField.text,
                    senderAccountNo:  selectedDeposit.accountNo,
                    receiverAccountNo: ReceiverAccountNoTextField.text,
                    paymentComment: PaymentCommentTextField.text,
                    paymentCode: PaymentCodeTextField.text,
                    transferSumm:  Double(TransferSummTextField.text  ?? "0.0"),
                    operationType: 0,
                    operationID: 0,
                    isSchedule: false,
                    isTemplate: self.isCreateTemplate,
                    templateName: self.templateNameTextField.text ?? nil,
                    templateDescription: "Клиринг/Гросс",
                    scheduleID: 0,
                    schedule: self.schedule ?? nil)
            }
            vc.clearingGrossOperationModel = submitModel
            vc.operationID = operationID
            vc.operationTypeID = operationTypeID
            vc.currencyCurrentID = selectedDeposit.currencyID ?? 0
            vc.selectedDeposit = self.selectedDeposit
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            
        }
        
    }
    //    выбор счета
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
    }
    //    выбор bikCode
    func selectedClearingGrossAccountUser(bikCode: BikCode) {
        self.bikCode = bikCode
    }
    //    выбор paymentCode
    func selectedClearingGrossAccountUser(paymentCode: PaymentCode) {
        self.paymentCode = paymentCode
    }
    // Получение данных для редактирования оперции
    
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool) {
        self.operationID = operationID
        self.isRepeatPay = isRepeatPay
        self.isRepeatOperation = isRepeatOperation
    }
    
    @IBAction func clearingGrossButton(_ sender: Any) {
        showAlertSheetClearingGross()
    }
    
    // кнопка включения планировщика
    @IBAction func switchPlanner(_ sender: UISwitch) {
        if sender.isOn{
            viewPlanner.isHidden = false
            isSchedule = true
            StartDate = ScheduleHelper.shared.dateCurrentForServer()
            EndDate = ScheduleHelper.shared.dateCurrentForServer()
            dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
            dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
        }else{
            viewPlanner.isHidden = true
            isSchedule = false
        }
    }
    
}
// выбор периода планировщика
extension ClearingGrossViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //  установка дефолтных значений периода
    func pickerPeriodDate() {
        pickerView.delegate = self
        pickerViewDayOperation.delegate = self
        pickerTarif.delegate = self
        pickerViewWeekDay.delegate = self
        periodTextField.inputView = pickerView
        dayNumberTextField.inputView = pickerViewDayOperation
        weekDayTextField.inputView = pickerViewWeekDay
        pickerView.selectRow(0, inComponent: 0, animated: true)
        pickerViewDayOperation.selectRow(0, inComponent: 0, animated: true)
        pickerViewWeekDay.selectRow(0, inComponent: 0, animated: true)
        pickerTarif.selectRow(0, inComponent: 0, animated: true)
        periodTextField.text = periodList[0]
        dayNumberTextField.text = daysList[0]
        weekDayTextField.text = weekList[0]
        viewDateBegin.isHidden = false
        viewDateFinish.isHidden = false
        viewWeekDay.isHidden = false
        viewDay.isHidden = true
        viewDateOperation.isHidden = true
    }
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count = 0
        if pickerView == self.pickerView {
            count = periodList.count
        } else if pickerView == self.pickerViewDayOperation{
            count = daysList.count
        } else if pickerView == self.pickerViewWeekDay{
            count = weekList.count
        }
        
        return count
    }
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var list = ""
        if pickerView == self.pickerView {
            list = periodList[row]
        } else if pickerView == self.pickerViewDayOperation{
            list = daysList[row]
        } else if pickerView == self.pickerViewWeekDay{
            list = weekList[row]
        }
        return list
    }
    // скрытие ненужных полей
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.pickerView {
            periodTextField.text = periodList[row]
            let formatterToServer = DateFormatter()
            formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"///////////change date format
            formatterToServer.locale = Locale(identifier: "language".localized)
            let dateNow = Date()
            switch row {
                
            case SchedulePickerView.EachWeek.rawValue:
                viewDateBegin.isHidden = false
                viewDateFinish.isHidden = false
                viewWeekDay.isHidden = false
                viewDay.isHidden = true
                viewDateOperation.isHidden = true
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = formatterToServer.string(from: dateNow )
                RecurringTypeID = ScheduleRepeat.EachWeek.rawValue
                ProcessDay = 1
                dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
                dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
                
            case SchedulePickerView.EachMonth.rawValue:
                viewDateBegin.isHidden = false
                viewDateFinish.isHidden = false
                viewWeekDay.isHidden = true
                viewDay.isHidden = false
                viewDateOperation.isHidden = true
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = formatterToServer.string(from: dateNow )
                RecurringTypeID = ScheduleRepeat.EachMonth.rawValue
                ProcessDay = 1
                dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
                dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
                
            case SchedulePickerView.OneTime.rawValue:
                viewDateBegin.isHidden = true
                viewDateFinish.isHidden = true
                viewWeekDay.isHidden = true
                viewDay.isHidden = true
                viewDateOperation.isHidden = false
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = nil
                RecurringTypeID = ScheduleRepeat.OneTime.rawValue
                ProcessDay = 0
                dateOperationTextField.text = ScheduleHelper.shared.dateSchedule()
                
            default:
                viewDateBegin.isHidden = true
                viewDateFinish.isHidden = true
                viewWeekDay.isHidden = true
                viewDay.isHidden = true
                viewDateOperation.isHidden = true
                
            }
        } else if pickerView == self.pickerViewDayOperation{
            dayNumberTextField.text = daysList[row]
            ProcessDay =  row + 1
        } else if pickerView == self.pickerViewWeekDay{
            weekDayTextField.text = weekList[row]
            ProcessDay =  row + 1
        }
        
    }
    // выборка даты
    func showDatePicker(){
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        paymentDateTextField.inputAccessoryView = toolbar
        paymentDateTextField.inputView = datePicker
        dateBeginTextField.inputAccessoryView = toolbar
        dateBeginTextField.inputView = datePicker
        dateFinishTextField.inputAccessoryView = toolbar
        dateFinishTextField.inputView = datePicker
        dateOperationTextField.inputAccessoryView = toolbar
        dateOperationTextField.inputView = datePicker
        
    }
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"///////////change date format
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"///////////change date format
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        
        if dateBeginTextField.isFirstResponder {
            dateBeginTextField.text = formatter.string(from: datePicker.date)
            StartDate = formatterToServer.string(from: datePicker.date)
        }
        if dateFinishTextField.isFirstResponder {
            dateFinishTextField.text = formatter.string(from: datePicker.date)
            EndDate = formatterToServer.string(from: datePicker.date)
        }
        if dateOperationTextField.isFirstResponder {
            dateOperationTextField.text = formatter.string(from: datePicker.date)
        }
        if paymentDateTextField.isFirstResponder {
            paymentDateTextField.text = formatter.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    // проверка на фокус поля ввода
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dateBeginTextField {
            datePicker.datePickerMode = .date
        }
        if textField == dateFinishTextField {
            datePicker.datePickerMode = .date
        }
        if textField == dateOperationTextField {
            datePicker.datePickerMode = .date
        }
    }
}

extension ClearingGrossViewController{
    //    получение счетов
    func  getClearingGrossAccounts(){
        let clearingGrossValidateObservable:Observable<InternetBankingClearingGrossValidateDataModel> = managerApi
            .getClearingGrossValidateData()
        
        let  clearingGrossAccontsObservabel =  managerApi
            .getClearingGrossAcconts()
        
        showLoading()
        Observable
            .zip(clearingGrossValidateObservable, clearingGrossAccontsObservabel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[ weak self] clearingGrossValidateData, clearingGrossAcconts in
                    guard let `self` = self else { return }
                    self.clearingGrossValidateData = clearingGrossValidateData
                    self.clearingMaxAmount = clearingGrossValidateData.clearingMaxAmount
                    
                    self.dateFrom = clearingGrossValidateData.dateFrom
                    self.dateTo = clearingGrossValidateData.dateTo
                    self.clearingGrossAccounts = clearingGrossAcconts
                    self.clearingGrossAccounts = self.checkAccountsForZeroBalance(accounts: self.clearingGrossAccounts)
                    self.showDatePicker()
                    self.dateCurrent(textField: self.paymentDateTextField, stringDate: clearingGrossValidateData.dateFrom ?? "")
                    
                    if self.isCreateTemplate{
                        if self.selectedDeposit == nil{
                            if self.submitModel != nil{
                                self.selectedDeposit = self.getCurrentTemplateAccount(
                                    accountNo: self.submitModel.senderAccountNo ?? "",
                                    currency: self.clearingGrossCurrency ,
                                    accounts: self.clearingGrossAccounts)
                            }
                        }
                    }
                    if self.isEditTemplate || self.isRepeatPay{
                        if self.isRepeatFromStory{
                            
                            
                            self.selectedDeposit = self.getCurrentTemplateAccount(
                                accountNo: self.clearingGrossOperationModel.senderAccountNo ?? "",
                                currency: self.clearingGrossCurrency,
                                accounts: self.clearingGrossAccounts)
                            // нет currencyID в модельке  clearingGrossOperationModel!!!!!!!
                        }else{
                            if self.template != nil{
                                self.selectedDeposit = self.getCurrentTemplateAccount(
                                    accountNo: self.template.from ?? "",
                                    currency: self.template.currencyID ?? 0,
                                    accounts: self.clearingGrossAccounts)
                            }
                            if self.clearingGrossOperationModel != nil {
                                self.selectedDeposit = self.getCurrentTemplateAccount(
                                    accountNo: self.clearingGrossOperationModel.senderAccountNo ?? "",
                                    currency: self.clearingGrossCurrency,
                                    accounts: self.clearingGrossAccounts)
                            }
                        }
                    }
                    
                    if self.currencyIDcurrent != 0 && self.accountNoCurrent != ""{
                        self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.accountNoCurrent, currency: self.currencyIDcurrent, accounts: self.clearingGrossAccounts)
                        
                        if self.selectedDeposit != nil{
                            self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                        }
                    }
                    if self.selectedDeposit != nil{
                        self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit )
                    }
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    обработка даты
    func dateCurrent(textField: UITextField, stringDate: String){
        
        let dateFormater = DateFormatter()
        let dateFormater1 = DateFormatter()
        let dateFormater2 = DateFormatter()
        
        dateFormater1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormater1.locale = Locale(identifier: "language".localized)
        
        dateFormater2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        dateFormater.dateFormat = "dd MMMM yyyy"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        if let newDate: Date = dateFormater1.date(from:stringDate){
            
            textField.text = dateFormater.string(from: (newDate))
        }
        if let newDate: Date = dateFormater2.date(from:stringDate){
            
            textField.text = dateFormater.string(from: (newDate))
        }
        
    }
    //    создание перевода ClearingGross
    func postCreatClearingGross(clearingGrossOperationModel: ClearingGrossOperationModel){
        
        showLoading()
        managerApi
            .postClearinGrossCreate(ClearingGrossOperationModel: clearingGrossOperationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                   guard let `self` = self else { return }
                    
                    if self.isCreateTemplate || self.isEditTemplate{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                        self.view.window?.rootViewController = vc
                        
                        let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                        let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                        let navigationController = UINavigationController(rootViewController: destinationController)
                        vc.pushFrontViewController(navigationController, animated: true)
                        
                    } else{
                        if operation.operationID != nil{
                            self.operationID = operation.operationID ?? 0
                        }
                        self.allowOperation()
                        
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    // Проверка сессии клиринга
    func postIsNeedChangeDate(clearingGrossNeedChangeDateModel: ClearingGrossNeedChangeDateModel, clearingGrossOperationModel: ClearingGrossOperationModel ){
        
        showLoading()
        managerApi
//            .postNeedChangeDateWithoutBody(clearingGrossNeedChangeDateModel: clearingGrossNeedChangeDateModel)
            .postNeedChangeDate(clearingGrossNeedChangeDateModel: clearingGrossNeedChangeDateModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    self.showLoading()
                    
                    if result.isSessionExpired == true {
                        self.hideLoading()
                        self.newDate = ScheduleHelper.shared.dateForFullSchedule(stringDate: result.nextDate ?? "") //self.convertDate(date: result.nextDate ?? "")
                        let alertController = UIAlertController(title: self.localizedText(key: "the_operation_for_the_selected_date_is_not_available"), message: "\(self.localizedText(key:"perform_an_operation_on")) \(String(describing: self.newDate))?", preferredStyle: .alert)
                        
                        let action = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                            if self.isSchedule{
                                self.schedule = Schedule(operationID: 0 , startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                            }else{
                                self.schedule = nil
                            }
                            
                            let clearingGrossOperationModelNewDate = ClearingGrossOperationModel(
                                paymentDate: result.nextDate,
                                pNumber: self.PNumberTextFields.text,
                                paymentTypeID: Int(self.paymentType),
                                senderFullName: self.SenderFullNameLabel.text,
                                receiverFullName: self.ReceiverFullNameTextField.text,
                                receiverBIK: self.ReceiverBIKTextField.text,
                                senderAccountNo: self.selectedDeposit.accountNo,
                                receiverAccountNo: self.ReceiverAccountNoTextField.text,
                                paymentComment: self.PaymentCommentTextField.text,
                                paymentCode: self.PaymentCodeTextField.text,
                                transferSumm: Double(self.TransferSummTextField?.text ?? "0.0"),
                                operationType: self.operationTypeID,
                                operationID: 0,
                                isSchedule: false,
                                isTemplate: self.isCreateTemplate,
                                templateName: self.templateNameTextField.text ?? nil,
                                templateDescription: "Клиринг/Гросс",
                                scheduleID: 0,
                                schedule: self.schedule ?? nil)
                            
                            self.newDate = result.nextDate ?? ""
                            if self.isRepeatPay{
                                self.postCreatClearingGross(clearingGrossOperationModel: clearingGrossOperationModelNewDate)
                            }else{
                                if self.operationID != 0{
                                    self.hideLoading()
                                    self.allowOperation()
                                }else{
                                    self.postCreatClearingGross(clearingGrossOperationModel: clearingGrossOperationModelNewDate)
                                }
                            }
                        })
                        let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .default, handler:nil)
                        
                        alertController.addAction(action)
                        alertController.addAction(actionCancel)
                        
                        self.present(alertController, animated: true, completion: nil)
                        
                    }else{
                        if self.isSchedule{
                            self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                        }else{
                            self.schedule = nil
                        }
                        
                        let clearingGrossOperationModelNewDate = ClearingGrossOperationModel(
                            paymentDate: self.convertDateFormatter(date: self.paymentDateTextField.text ?? ""),
                            pNumber: self.PNumberTextFields.text,
                            paymentTypeID: Int(self.paymentType),
                            senderFullName: self.SenderFullNameLabel.text,
                            receiverFullName: self.ReceiverFullNameTextField.text,
                            receiverBIK: self.ReceiverBIKTextField.text,
                            senderAccountNo: self.selectedDeposit.accountNo,
                            receiverAccountNo: self.ReceiverAccountNoTextField.text,
                            paymentComment: self.PaymentCommentTextField.text,
                            paymentCode: self.PaymentCodeTextField.text,
                            transferSumm: Double(self.TransferSummTextField?.text ?? "0.0"),
                            operationType: self.operationTypeID,
                            operationID: 0,
                            isSchedule: false,
                            isTemplate: self.isCreateTemplate,
                            templateName: self.templateNameTextField.text ?? nil,
                            templateDescription: "Клиринг/Гросс",
                            scheduleID: 0,
                            schedule: self.schedule ?? nil)
                        self.newDate = self.convertDateFormatter(date: self.paymentDateTextField.text ?? "")
                        
                        if self.isRepeatPay{
                            self.postCreatClearingGross(clearingGrossOperationModel: clearingGrossOperationModelNewDate)
                        }else{
                            if self.operationID != 0{
                                self.hideLoading()
                                self.allowOperation()
                            }else{
                                self.postCreatClearingGross(clearingGrossOperationModel: clearingGrossOperationModelNewDate)
                            }
                        }
                    }
            },
                onError: {[weak self] error in
                    
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    // Получение данных для проверки создания клиринга/гросса
    func  getClearingGrossInfo(operationID: Int){
        self.showLoading()
        managerApi
            .getInfoCliringGross(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] clearingGrossOperationModel in
                    guard let `self` = self else { return }
                    if self.isRepeatPay{
                        self.template = nil
                    }
                    self.clearingGrossOperationModel = clearingGrossOperationModel
                    if  self.clearingGrossOperationModel != nil{
                        if self.clearingGrossOperationModel.paymentTypeID == 1{
                            self.PaymentTypeIDTextField.text = self.localizedText(key: "clearing_payment")
                        }else{
                            self.PaymentTypeIDTextField.text = self.localizedText(key: "gross_payment")
                        }
                        self.paymentType =  self.clearingGrossOperationModel.paymentTypeID ?? 1
                        self.TransferSummTextField.text = "\(String(format:"%.2f", self.clearingGrossOperationModel.transferSumm ?? 0.0 ))"
                        self.SenderFullNameLabel.text = self.clearingGrossOperationModel.senderFullName
                        self.paymentDateTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: self.clearingGrossOperationModel.paymentDate ?? "")
                        //self.convertDate(date:self.clearingGrossOperationModel.paymentDate ?? "")
                        self.PNumberTextFields.text = self.clearingGrossOperationModel.pNumber
                        self.PaymentCodeTextField.text = self.clearingGrossOperationModel.paymentCode
                        self.PaymentCommentTextField.text = self.clearingGrossOperationModel.paymentComment
                        self.ReceiverFullNameTextField.text = self.clearingGrossOperationModel.receiverFullName
                        self.ReceiverAccountNoTextField.text = self.clearingGrossOperationModel.receiverAccountNo
                        self.ReceiverBIKTextField.text = self.clearingGrossOperationModel.receiverBIK
                        self.templateNameTextField.text = self.clearingGrossOperationModel.templateName
                        self.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
                        self.operationID =  self.clearingGrossOperationModel.operationID ?? 0
                        for code in self.bikCodes{
                            if code.code == self.clearingGrossOperationModel.receiverBIK{
                                self.BikDescriptionLabel.text = code.name
                            }
                        }
                        for code in self.paymentsCodes{
                            if code.code == self.clearingGrossOperationModel.paymentCode{
                                self.PaymentCodeDescriptionLabel.text = code.description
                            }
                        }
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    // получение данных операции перевода
    func  getInfoOperationClearingGrossbyPaymentID(paymentID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationClearingGrossbyPaymentID(paymentID: paymentID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.clearingGrossPaymenModel = operation
                    self.clearingGrossOperationModel = ClearingGrossOperationModel(
                        paymentDate: self.clearingGrossPaymenModel.paymentDate,
                        pNumber: self.clearingGrossPaymenModel.pNumber,
                        paymentTypeID: self.clearingGrossPaymenModel.paymentTypeID,
                        senderFullName: self.clearingGrossPaymenModel.senderFullName,
                        receiverFullName: self.clearingGrossPaymenModel.receiverFullName,
                        receiverBIK: self.clearingGrossPaymenModel.receiverBIK,
                        senderAccountNo: self.clearingGrossPaymenModel.senderAccountNo,
                        receiverAccountNo: self.clearingGrossPaymenModel.receiverAccountNo,
                        paymentComment: self.clearingGrossPaymenModel.paymentComment,
                        paymentCode: self.clearingGrossPaymenModel.paymentCode,
                        transferSumm: self.clearingGrossPaymenModel.transferSumm,
                        operationType: self.operationTypeID,
                        operationID: 0,
                        isSchedule: false,
                        isTemplate: false,
                        templateName: nil,
                        templateDescription: nil,
                        scheduleID: nil,
                        schedule: nil)
             
                    if self.clearingGrossPaymenModel.paymentTypeID == 1{
                        self.PaymentTypeIDTextField.text = self.localizedText(key: "clearing_payment")
                    }else{
                        self.PaymentTypeIDTextField.text = self.localizedText(key: "gross_payment")
                    }
                    self.paymentType =  self.clearingGrossOperationModel.paymentTypeID ?? 1
                    self.TransferSummTextField.text = "\(String(format:"%.2f", self.clearingGrossOperationModel.transferSumm ?? 0.0 ))"
                    self.SenderFullNameLabel.text = self.clearingGrossOperationModel.senderFullName
                    self.paymentDateTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: self.clearingGrossOperationModel.paymentDate ?? "")
                    self.PNumberTextFields.text = self.clearingGrossOperationModel.pNumber
                    self.PaymentCodeTextField.text = self.clearingGrossOperationModel.paymentCode
                    self.PaymentCommentTextField.text = self.clearingGrossOperationModel.paymentComment
                    self.ReceiverFullNameTextField.text = self.clearingGrossOperationModel.receiverFullName
                    self.ReceiverAccountNoTextField.text = self.clearingGrossOperationModel.receiverAccountNo
                    self.ReceiverBIKTextField.text = self.clearingGrossOperationModel.receiverBIK
                    self.templateNameTextField.text = self.clearingGrossOperationModel.templateName
                    self.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
                    self.operationID =  self.clearingGrossOperationModel.operationID ?? 0
                    for code in self.bikCodes{
                        if code.code == self.clearingGrossOperationModel.receiverBIK{
                            self.BikDescriptionLabel.text = code.name
                        }
                    }
                    for code in self.paymentsCodes{
                        if code.code == self.clearingGrossOperationModel.paymentCode{
                            self.PaymentCodeDescriptionLabel.text = code.description
                        }
                    }
    

                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension ClearingGrossViewController: UITextFieldDelegate{
    //    модалка для выбора ClearingGross
    func showAlertSheetClearingGross() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle:  .actionSheet)
        
        let actionClearing = UIAlertAction(title: self.localizedText(key: "Resources_ClearingGross_Common_Clearing"), style: .default) { (action) in
            
            self.paymentType = 1
            self.operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
            self.PaymentTypeIDTextField.text = self.localizedText(key: "clearing_payment")
        }
        let actionGross = UIAlertAction(title: self.localizedText(key: "Resources_ClearingGross_Common_Gross"), style: .default) { (action) in
            
            self.paymentType = 2
            self.operationTypeID = InternetBankingOperationType.GrossTransfer.rawValue
            self.PaymentTypeIDTextField.text = self.localizedText(key: "gross_payment")
        }
        let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel)
        
        alert.addAction(actionClearing)
        alert.addAction(actionGross)
        alert.addAction(actionCancel)
        present(alert, animated: true, completion: nil)
    }
    //    обработка даты
    func convertDateFormatter(date: String) -> String {
        let dateFormatter = DateFormatter()
        let dateFormatterTo = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"//this your string date format
        dateFormatterTo.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "language".localized)
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }
        let timeStamp = dateFormatterTo.string(from: convertedDate!)
        
        return timeStamp
    }
    
    func currentDateForServer() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "language".localized)
        let convertedDate = dateFormatter.string(from: Date())

        return convertedDate
    }
    
    //    проверка на количество символов
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        if textField.isEqual(PaymentCodeTextField) {
            maxLength = 8
        } else if textField.isEqual(ReceiverAccountNoTextField) {
            maxLength = Constants.ACCOUNTS_CHARACTERS_COUNT
        } else if textField.isEqual(ReceiverBIKTextField) {
            maxLength = 6
        }
        else if textField.isEqual(TransferSummTextField) {
            maxLength = 10
        }
        else if textField.isEqual(PNumberTextFields) {
            maxLength = 10
        }
        else if textField.isEqual(PaymentCommentTextField) {
            maxLength = 100
        }
        else if textField.isEqual(ReceiverFullNameTextField) {
            maxLength = 100
        }
        else if textField.isEqual(ReceiverBIKTextField) {
            maxLength = 6
        }
        else if textField.isEqual(templateNameTextField) {
            maxLength = 100
        }
        return newLength <= maxLength
        
    }
    //    проверка на количество символов
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.isEqual(ReceiverAccountNoTextField){
            if textField.text?.count == Constants.ACCOUNTS_CHARACTERS_COUNT{
                checkControlDigit(acconttNo: textField.text ?? "0")
            }else{
                showAlertController(localizedText(key: "acc_len_must_be_greater_16"))
                ReceiverAccountNoTextField.becomeFirstResponder()
                ReceiverAccountNoTextField.shake()
            }
        }
    }
    //    проверка при наличии фокуса
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField{
        case self.TransferSummTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewTransferSummTextField, labelError: labelErrorTransferSumm)
        case self.PaymentCodeTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
        case self.ReceiverBIKTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
        case self.PNumberTextFields:
            viewTextFieldShouldBeginEditing(viewTextField: viewPNumberTextFields, labelError: labelErrorPNumber)
        case self.PaymentCommentTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
        case self.ReceiverFullNameTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
        case self.ReceiverAccountNoTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
        case self.ReceiverAccountNoTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
            
        default:
            break
        }
        return true
    }
    
    //    переход на следкующее поле
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == templateNameTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    
    // контрольная сумма
    func checkControlDigit(acconttNo: String){
        
        if let checkFirstFourteen: Int = Int(ReceiverAccountNoTextField.text?.prefix(14) ?? "0"){
            checkSum = checkFirstFourteen % 97
            if checkSum < 10{
                if checkSum == 0 {
                    checkSumString = "97"
                    print("checkSumString: \(checkSumString)" )
                }else{
                    checkSumString = "0"+"\(checkSum)"
                    print("checkSumString: \(checkSumString)" )
                }
            }else{
                checkSumString = "\(checkSum)"
                print("checkSumString: \(checkSumString)" )
            }
        }
        
        if checkSumString != String(ReceiverAccountNoTextField.text?.suffix(2) ?? "0"){
            showAlertController(localizedText(key: "invalid_recipient_account"))
        }
    }
    //    заполнение текущего счета
    func fullSelectedDeposit(selectedDeposit: Deposit){
        let symbolString = " " + (selectedDeposit.currencySymbol ?? "")
        
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        payFrom_accountsLabel.text = fullAccountsText
        choiceCard_accountsLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currencySymbol ?? ""))"
        
        ccurrencySymbolLabel.text = symbolString
        viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
    }
}

extension ClearingGrossViewController{
    // проверка для прав на создание и подтверждние
    func  allowToCreatAndSaveOperation(button: UIButton) {
        
        
        if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed") && AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "execute_operation"),for: .normal)
        }else if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed") && !AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "Resources_Common_Save"),for: .normal)
        }
    }
    
    //    проверка прав
    func  allowOperation() {
        
        if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed") && AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed"){
            self.performSegue(withIdentifier: "toSubmitSegue", sender: self)
            
        }else if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed") && !AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed"){
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
            self.view.window?.rootViewController = vc
            
            let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
            let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
            let navigationController = UINavigationController(rootViewController: destinationController)
            vc.pushFrontViewController(navigationController, animated: true)
            
        }
    }
}

