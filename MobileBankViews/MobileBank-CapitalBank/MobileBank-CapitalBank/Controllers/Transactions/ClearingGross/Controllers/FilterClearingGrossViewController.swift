//
//  FilterClearingGrossViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/17/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift

// протокол для фильтрации
protocol FilterClearingGrossPropertyDelegate {
    func property(dateFrom: String?, dateTo: String?, operationPayment: String?, operationDirection: String?, operationState: String?, isFilter: Bool?)
    func resetFilter(isFilter: Bool?)
}

//  класс для фильтрации
class FilterClearingGrossViewController:  BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var paymentTypeTextField: UITextField!
    @IBOutlet weak var directionTypeTextField: UITextField!
    @IBOutlet weak var statusTypeTextField: UITextField!
    
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    
    var delegate: FilterClearingGrossPropertyDelegate?
    let pickerViewPaymentType = UIPickerView()
    let pickerViewDirection = UIPickerView()
    let pickerViewStates = UIPickerView()
    var arrayOfPayments = ["Все типы платежа"]
    var arrayOfDirections = ["Все направления"]
    var arrayOfStates = ["Все статусы"]
    
    
    var Payments = [expenseTypeModel]()
    var Directions = [expenseTypeModel]()
    var States = [expenseTypeModel]()
//    var Operations = [expenseTypeModel]()
    let datePicker = UIDatePicker()
    
    var operationPayment = "null"
    var operationState = "null"
    var operationDirection = "null"
    
    var dateFrom = ""
    var dateTo = ""
    var isNeedData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = localizedText(key: "show_operations")
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        arrayOfStates = [localizedText(key: "all_statuses")]
        arrayOfPayments  = [localizedText(key: "all_payment_types"),
                            localizedText(key: "Resources_ClearingGross_Common_Clearing"),
                            localizedText(key: "Resources_ClearingGross_Common_Gross") ]
        arrayOfDirections  = [localizedText(key: "all_directions"),
                              localizedText(key: "Outgoing"),
                              localizedText(key: "Incoming") ]
        
        paymentTypeTextField.delegate = self
        statusTypeTextField.delegate = self
        directionTypeTextField.delegate = self
        pickerViewPaymentType.delegate = self
        pickerViewDirection.delegate = self
        pickerViewStates.delegate = self
        fromDateTextField.delegate = self
        toDateTextField.delegate = self
        showDatePicker()
        getStates()

    }
    
    //    кнопка сброса параметров фильтрации
    @IBAction func resetFilterButton(_ sender: Any) {
        resetFilterData()
        navigationController?.popViewController(animated: true)
    }
    //    кнопка применения фильтрации
    @IBAction func applyFilterButton(_ sender: Any) {
        filteredData()
        navigationController?.popViewController(animated: true)
    }
    
    //    количество компонентов pickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //    количество строк компонентов pickerView
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return arrayOfPayments.count
        }else if pickerView.tag == 1 {
            return arrayOfDirections.count
        }else if pickerView.tag == 2 {
            return arrayOfStates.count
        }
        return 1
    }
    //    название компонентов pickerView
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return arrayOfPayments[row]
        }else if pickerView.tag == 1 {
            return arrayOfDirections[row]
        }else if pickerView.tag == 2 {
            return arrayOfStates[row]
        }
        return ""
    }
    //   обработка нажатия  pickerView
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView.tag == 0 {
            for payment in Payments{
                if payment.text == arrayOfPayments[row]{
                    operationPayment = "\(String(describing: payment.value ?? 0))"
                }
            }
            filteredData()
            
            return paymentTypeTextField.text = arrayOfPayments[row]
            
        }else if pickerView.tag == 1 {
            for direction in Directions{
                if direction.text == arrayOfDirections[row]{
                    operationDirection = "\(String(describing: direction.value ?? 0))"
                }
            }
            filteredData()
            
            return directionTypeTextField.text = arrayOfDirections[row]
            
        }else if pickerView.tag == 2 {
            for state in States{
                if state.text == arrayOfStates[row]{
                    operationState = "\(String(describing: state.value ?? 0))"
                }
            }
            filteredData()
            
            return statusTypeTextField.text = arrayOfStates[row]
        }
    }
    //    проверка на фокус поля ввода
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        if (textField == paymentTypeTextField) {
            paymentTypeTextField.text = arrayOfPayments.first
            operationPayment = "null"
        }
        
        if (textField == directionTypeTextField) {
            directionTypeTextField.text = arrayOfDirections.first
            operationDirection = "null"
        }
        
        if (textField == statusTypeTextField) {
            statusTypeTextField.text = arrayOfStates.first
            operationState = "null"
        }
        
        return true
    }
    
    
    
}

extension FilterClearingGrossViewController{
    // получение статусов и операций
    func getStates() {
        
        showLoading()
        managerApi
            .getOperationStates()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] states in
                    guard let `self` = self else { return }
                    self.States = states
                    for state in states{
                        self.arrayOfStates.append(state.text ?? "")
                    }
                    self.pickerSettings()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    установка pickerView
    func pickerSettings(){
        
        paymentTypeTextField.inputView = pickerViewPaymentType
        directionTypeTextField.inputView = pickerViewDirection
        statusTypeTextField.inputView = pickerViewStates
        pickerViewPaymentType.tag = 0
        pickerViewDirection.tag = 1
        pickerViewStates.tag = 2
        pickerViewStates.selectRow(0, inComponent: 0, animated: true)
        if isNeedData{
            for payment in Payments{
                if payment.value == Int(self.operationPayment) ?? 0{
                    operationPayment = "\(String(describing: payment.value ?? 0))"
                    paymentTypeTextField.text = payment.text
                }
            }
            for direction in Directions{
                if direction.value == Int(self.operationDirection) ?? 0{
                    operationDirection = "\(String(describing: direction.value ?? 0))"
                    directionTypeTextField.text = direction.text
                }
            }
            for state in States{
                if state.value == Int(self.operationState) ?? 0{
                    operationState = "\(String(describing: state.value ?? 0))"
                    statusTypeTextField.text = state.text
                }
            }
        }
        
        
    }
    //  установка значений фильтрации
    func filteredData(){
        delegate?.property(dateFrom: dateFrom,
                           dateTo: dateTo,
                           operationPayment: operationPayment,
                           operationDirection: operationDirection,
                           operationState: operationState,
                           isFilter: true)
        
    }
    //  установка сброса фильтрации
    func resetFilterData(){
        delegate?.resetFilter(isFilter: false)
    }
    
    
    
    // выборка даты
    func showDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.maximumDate = Date()
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        fromDateTextField.inputAccessoryView = toolbar
        fromDateTextField.inputView = datePicker
        toDateTextField.inputAccessoryView = toolbar
        toDateTextField.inputView = datePicker
        
        
        if isNeedData{
            if self.dateTo != ""{
                toDateTextField.text = dateFormaterForFilter(date: self.dateTo)
            }else{
                toDateTextField.text = dateToStringFormatter(date: Date())
                dateTo = formatterToServer.string(from: Date())//ScheduleHelper.shared.dateCurrentForServer()
            }
            if self.dateFrom != ""{
                fromDateTextField.text = dateFormaterForFilter(date: self.dateFrom)
            }else{
                fromDateTextField.text = dateFrom
            }
        }
    }
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        
        if fromDateTextField.isFirstResponder {
            dateFrom = formatterToServer.string(from: datePicker.date)
            filteredData()
            
            fromDateTextField.text = formatter.string(from: datePicker.date)
        }
        if toDateTextField.isFirstResponder {
            dateTo = formatterToServer.string(from: datePicker.date)
            filteredData()
            
            toDateTextField.text = formatter.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    // проверка на фокус поля ввода
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == fromDateTextField {
            datePicker.datePickerMode = .date
            
        }
        if textField == toDateTextField {
            datePicker.datePickerMode = .date
            
        }
    }
    // сравнение дат для фильтрации
    func compareTwoDates(dateFrom: String, dateTo: String, formatter: DateFormatter){
        let dateFrom: Date = stringToDateFormatterFiltered(date: dateFrom)
        let dateTo: Date = stringToDateFormatterFiltered(date: dateTo)
        if dateFrom.compare(dateTo) == .orderedDescending {
            toDateTextField.text = dateToStringFormatter(date: Date())
        }else{
            toDateTextField.text = formatter.string(from: datePicker.date)
        }
    }
    
    
}

