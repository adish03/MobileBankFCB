//
//  DetailCliringGrossViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/13/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift

class DetailClearingGrossViewController: BaseViewController, FilterClearingGrossPropertyDelegate {
  

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var filterButton: UIBarButtonItem!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    
    var navigationTitle = ""
    var dateFrom = ""
    var dateTo = ""
    var isFiltered = false
    var operationID = 0
    
    var text = ""
    var fetchingMore = false
    var totalItems = 0
    var pageCount = 0
    var pageSize = 0
    var page = 1
    
    var isSearching = false
    var clearingGrossOperationModel: ClearingGrossOperationModel!
    var filteredArray = [TransfersModel]()
    
    var operationPayment = "null"
    var operationState = "null"
    var operationDirection = "null"
    
    var detailsOfClearingGross = [TransfersModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        barButtonCustomize()
        if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsViewAllowed"){
            if isFiltered{
                
                detailsOfClearingGross.removeAll()
                getClearingGrossTransfers(paymentType: operationPayment,
                                          paymentDirection: operationDirection,
                                          startDate: dateFrom,
                                          endDate: dateTo,
                                          status: operationState,
                                          text: nil,
                                          PageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                          Page: self.page,
                                          TotalItems: nil)
            }else{
                detailsOfClearingGross.removeAll()
                getClearingGrossTransfers(paymentType: "",
                                          paymentDirection: "",
                                          startDate: dateFrom,
                                          endDate: dateTo,
                                          status: nil,
                                          text: nil,
                                          PageSize: 10,
                                          Page: 1,
                                          TotalItems: nil)
            }
            tableView.reloadData()
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        emptyStateStack.isHidden = true
        if let bankDate = UserDefaultsHelper.bankDate{
            dateTo = dateFormaterBankDay(date: bankDate)
        }
        
        let calendar = Calendar.current
        if let date = calendar.date(byAdding: .day, value: -7, to: stringToDateFormatterFiltered(date: dateTo)){
            
            dateFrom = dateToStringFormatter(date: date)
        }
        
        hideKeyboardWhenTappedAround()
        
        tableView.reloadData()
    }
    // проверка при скроллинге до конца страницы
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height{
            if !fetchingMore{
                fetchingMore = true
                page += 1
                
                if isFiltered{
                    print("total Items: \(totalItems) ---- payments.count: \(detailsOfClearingGross.count * 10)")
                    if !(totalItems < pageCount * pageSize){
                        getClearingGrossTransfers(paymentType: operationPayment,
                                                  paymentDirection: operationDirection,
                                                  startDate: dateFrom,
                                                  endDate: dateTo,
                                                  status: operationState,
                                                  text: nil,
                                                  PageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                                  Page: self.page,
                                                  TotalItems: nil)
                    }
                }else{
                    if !(totalItems < pageCount * pageSize){
                        self.activityIndicator.startAnimating()
                        self.tableView.tableFooterView?.isHidden = false
                        getClearingGrossTransfers(paymentType: "",
                                                  paymentDirection: "",
                                                  startDate: dateFrom,
                                                  endDate: dateTo,
                                                  status: nil,
                                                  text: nil,
                                                  PageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                                  Page: self.page,
                                                  TotalItems: nil)
                        
                    }
                }
            }
        }
    }
    
    
    
    @IBAction func creatClearinGrossButton(_ sender: Any) {
        performSegue(withIdentifier: "toCreateClearingGross", sender: self)
    }
    
    @IBAction func filterButton(_ sender: Any) {
    }
    
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        if segue.identifier == "toCreateClearingGross"{
            let vc = segue.destination as! ClearingGrossViewController
            vc.navigationTitle =  self.localizedText(key:"Resources_ClearingGross_Common_Title")
            vc.operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
        }
        
        if segue.identifier == "toFilterSegue"{
                        let vc = segue.destination as! FilterClearingGrossViewController
            
                        vc.dateFrom = self.dateFrom
                        vc.dateTo = self.dateTo
                        vc.operationPayment = self.operationPayment
                        vc.operationDirection = self.operationDirection
                        vc.operationState = self.operationState
                        vc.isNeedData = true
                        vc.delegate = self
                    }
       
    }
    //    установка значений при фильтрации
    func property(dateFrom: String?,
                  dateTo: String?,
                  operationPayment: String?,
                  operationDirection: String?,
                  operationState: String?,
                  isFilter: Bool?) {
        self.dateFrom = dateFrom ?? "null"
        self.dateTo = dateTo ?? "null"
        self.operationPayment = operationPayment ?? ""
        self.operationState = operationState ?? ""
        self.operationDirection = operationDirection ?? ""
        self.isFiltered = isFilter ?? true
    }
    //    установка значений при сбросе фильтрации
    func resetFilter(isFilter: Bool?) {
        
        self.isFiltered = isFilter ?? false
        self.operationPayment = ""
        self.operationState = ""
        self.operationDirection = ""
        self.page = 1
        
        detailsOfClearingGross.removeAll()
    }
    
    
    // получение  DetailClearingGross
    
    func  getClearingGrossTransfers(paymentType: String?,
                                    paymentDirection: String?,
                                    startDate: String?,
                                    endDate: String?,
                                    status: String?,
                                    text: String?,
                                    PageSize: Int,
                                    Page: Int,
                                    TotalItems: Int?) {
        showLoading()
        
        managerApi
            .getClearingGrossTransfers(paymentType: paymentType, paymentDirection: paymentDirection, startDate: startDate, endDate: endDate, status: status, text: text, PageSize: PageSize, Page: Page, TotalItems: TotalItems)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] transfers in
                    guard let `self` = self else { return }
                    self.detailsOfClearingGross.append(contentsOf:  transfers.result ?? [])
                    self.fetchingMore = false
                    self.totalItems = transfers.paginationInfo?.totalItems ?? 0
                    self.pageCount = transfers.paginationInfo?.page ?? 0
                    self.pageSize = transfers.paginationInfo?.pageSize ?? 0
                   
                    self.activityIndicator.stopAnimating()
                    
                    if self.detailsOfClearingGross.isEmpty{
                        self.tableView.isHidden = true
                        self.emptyStateStack.isHidden = false
                        if self.isFiltered{
                            self.emptyStateLabel.text = self.localizedText(key: "nothing_found_change_search_criteria")
                            self.emptyImage.image = UIImage(named: "empty_search_icn")
                        }
                        
                    }else{
                        self.tableView.isHidden = false
                        self.emptyStateStack.isHidden = true
                    }
                    
                    self.tableView.reloadData()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }

}
extension DetailClearingGrossViewController: UITableViewDataSource, UITableViewDelegate{
    
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredArray.count
        }else{
            return detailsOfClearingGross.count
        }
        
    }
    
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailClearingGrossTableViewCell
        
        if isSearching{
            let detail = filteredArray[indexPath.row]
            cell.paymentTypeLabel.text = detail.paymentType
            cell.paymentDirectionLabel.text = detail.paymentDirection
            cell.PNumberLabel.text = detail.pNumber
            cell.paymentDateLabel.text = dateFormaterBankDay(date: detail.paymentDate ?? "")
            cell.statusLabel.text = detail.status
            cell.statusDateLabel.text = dateFormaterBankDay(date: detail.statusDate ?? "")
            cell.transferSummLabel.text = "\(String(format:"%.2f", detail.transferSumm ?? 0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: Constants.NATIONAL_CURRENCY))"
            cell.FAButton.tag = indexPath.row
            cell.FAButton.addTarget(self, action: #selector(DetailClearingGrossViewController.oneTapped(_:)), for: .touchUpInside)
            cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action: #selector(DetailClearingGrossViewController.moreDetailTapped(_:)), for: .touchUpInside)
        }else{
            let detail = detailsOfClearingGross[indexPath.row]
            cell.paymentTypeLabel.text = detail.paymentType
            cell.paymentDirectionLabel.text = detail.paymentDirection
            cell.PNumberLabel.text = detail.pNumber
            cell.paymentDateLabel.text = dateFormaterBankDay(date: detail.paymentDate ?? "")
            cell.statusLabel.text = detail.status
            cell.statusDateLabel.text = dateFormaterBankDay(date: detail.statusDate ?? "")
            cell.transferSummLabel.text = "\(String(format:"%.2f", detail.transferSumm ?? 0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: Constants.NATIONAL_CURRENCY))"
            cell.FAButton.tag = indexPath.row
            cell.FAButton.addTarget(self, action: #selector(DetailClearingGrossViewController.oneTapped(_:)), for: .touchUpInside)
            cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action: #selector(DetailClearingGrossViewController.moreDetailTapped(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    
    // обработка нажатия кнопки на ячейке
    @objc func moreDetailTapped(_ sender: UIButton) {
        
        let buttonTag = sender.tag
        
        let object = detailsOfClearingGross[buttonTag]
        
        switch  object.paymentTypeID {
        
        case PaymentTypeID.Clearing.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
//            vc.operationID = object.operationID ?? 0
            vc.confirmType = object.statusID ?? 1
            vc.isNeedOperationDetail = true
            vc.isNeedOperationDetailByPayment = true
            vc.paymentID = object.paymentID ?? 0
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.CliringTransfer.rawValue
            vc.currencyCurrentID = Constants.NATIONAL_CURRENCY
            self.navigationController?.pushViewController(vc, animated: true)
            
        case PaymentTypeID.Gross.rawValue:
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
//            vc.operationID = object.operationID ?? 0
            vc.confirmType = object.statusID ?? 1
            vc.isNeedOperationDetail = true
            vc.isNeedOperationDetailByPayment = true
            vc.paymentID = object.paymentID ?? 0
            vc.navigationTitle = localizedText(key: "details_of_operation")
            vc.operationTypeID = InternetBankingOperationType.GrossTransfer.rawValue
            vc.currencyCurrentID = Constants.NATIONAL_CURRENCY
            self.navigationController?.pushViewController(vc, animated: true)
            
        default:
            print("")
        }
        
    }
    // обработка нажатия кнопки на ячейке
    @objc func oneTapped(_ sender: UIButton) {
        
        let buttonTag = sender.tag
        let object = self.detailsOfClearingGross[buttonTag]
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionDoc = UIAlertAction(title: localizedText(key: "Documents"), style: .default) { (action) in
            ApiHelper.shared.getInfoOperationFileClearingGross(paymentID: object.paymentID ?? 0, vc: self)
        }
        
         let actionHSaveTemplate = UIAlertAction(title: localizedText(key: "save_as_template"), style: .default) { (action) in
       
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
                vc.isCreateTemplate = true
                vc.paymentID = object.paymentID ?? 0
                vc.clearingGrossCurrency = Constants.NATIONAL_CURRENCY
                vc.navigationTitle = self.localizedText(key: "create_template")
                self.navigationController?.pushViewController(vc, animated: true)
           
        }
        
        
         let actionRepeatOperation  = UIAlertAction(title: self.localizedText(key: "create_operation_request"), style: .default) { (action) in
       
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
            vc.paymentID = object.paymentID ?? 0
            vc.isRepeatPay = true
            vc.clearingGrossCurrency = Constants.NATIONAL_CURRENCY
            vc.navigationTitle = self.localizedText(key: "create_operation_request")
            self.navigationController?.pushViewController(vc, animated: true)
        }
       
        let actionCancel  = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel, handler: nil)
       
        if object.paymentDirectionID == 2{
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
        }
        
        if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsPrintDocumentsAllowed"){
            alertSheet.addAction(actionDoc)
        }
        
        alertSheet.addAction(actionCancel)
        
        present(alertSheet, animated: true, completion: nil)
        
    }

}

extension DetailClearingGrossViewController: UISearchBarDelegate{
    //поиск по истории платежей
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            
            isSearching = false
            if detailsOfClearingGross.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
                self.emptyImage.image = UIImage(named: "empty_icn_white")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
            tableView.reloadData()
            
        }else{
            
            filteredArray = detailsOfClearingGross.filter({ (clearingGross: TransfersModel ) -> Bool in
                return clearingGross.paymentType?.lowercased().contains(searchText.lowercased()) ?? true
            })
            if filteredArray.isEmpty{
                self.tableView.isHidden = true
                self.emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.tableView.isHidden = false
                self.emptyStateStack.isHidden = true
                fetchingMore = true
            }
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    // обработка кнопки search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        filteredArray.removeAll()
        isSearching = false
        if detailsOfClearingGross.isEmpty{
            tableView.isHidden = true
            emptyStateStack.isHidden = false
            self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
            self.emptyImage.image = UIImage(named: "empty_icn_white")
        }else{
            tableView.isHidden = false
            emptyStateStack.isHidden = true
        }
        fetchingMore = false
        if self.isFiltered{
            if self.detailsOfClearingGross.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
        }
        searchBar.text = ""
        tableView.reloadData()
        view.endEditing(true)
    }
    // кастомизация кнопки navigationabar
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        var image = ""
        if isFiltered{
            image = "selected"
            self.detailsOfClearingGross.removeAll()
        }else{
            image = "options"
            self.detailsOfClearingGross.removeAll()
        }
        button.setImage(UIImage(named: image), for: .normal)
        button.addTarget(self, action: #selector(filterButtonPressed), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    //    обработка кнопки фильтр
    @objc func filterButtonPressed() {
        performSegue(withIdentifier: "toFilterSegue", sender: self)
    }
    
    
}

