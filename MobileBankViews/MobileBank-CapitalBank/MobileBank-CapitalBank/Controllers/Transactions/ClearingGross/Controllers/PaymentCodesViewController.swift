//
//  PaymentCodesViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/5/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa


protocol ClearingGrossAccountsProtocol {
    
    func selectedClearingGrossAccountUser(paymentCode: PaymentCode)
}


class PaymentCodesViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    var isSearching = false
    var paymentsCodes = [PaymentCode]()
    var filteredArray = [PaymentCode]()
    var operationID = 0
    var paymentCode: PaymentCode!
    
    var delegate: ClearingGrossAccountsProtocol?
    @IBOutlet weak var search: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        navigationItem.title = localizedText(key: "payment_purpose_codes")
        hideKeyboardWhenTappedAround()
        emptyStateStack.isHidden = true
        tableView.separatorStyle = .singleLine
        tableView.reloadData()

    }

}

extension PaymentCodesViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if isSearching{
            return filteredArray.count
        }
        return paymentsCodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PaymentCodeTableViewCell
        
        if isSearching{
            cell.codeLabel.text = filteredArray[indexPath.row].code
            cell.descriptionLabel.text =  filteredArray[indexPath.row].description
            if paymentCode != nil{
                if filteredArray[indexPath.row].code == paymentCode.code{
                    cell.isSelected = true
                }
            }
        }else{
            cell.codeLabel.text = paymentsCodes[indexPath.row].code
            cell.descriptionLabel.text =  paymentsCodes[indexPath.row].description
            if paymentCode != nil{
                if paymentsCodes[indexPath.row].code == paymentCode.code{
                    cell.isSelected = true
                }
            }
        }
        return cell
    }
    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        if filteredArray.count != 0{
            let filteredCode = filteredArray[indexPath.row]
            delegate?.selectedClearingGrossAccountUser(paymentCode: filteredCode)
        }else{
            let paymentCode = paymentsCodes[indexPath.row]
            delegate?.selectedClearingGrossAccountUser(paymentCode: paymentCode)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isSelected {
            cell.setSelected(true, animated: false)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
}
extension PaymentCodesViewController: UISearchBarDelegate{
    //поиск
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            
            isSearching = false
            emptyStateStack.isHidden = true
            tableView.separatorStyle = .singleLine
            tableView.reloadData()
            
        }else{
            filteredArray = paymentsCodes.filter({ (code: PaymentCode) -> Bool in
                
                if searchText.isNumeric{
                    return code.code?.lowercased().contains(searchText.lowercased()) ?? true
                }else{
                    return code.description?.lowercased().contains(searchText.lowercased()) ?? true
                }
            })
            if filteredArray.isEmpty{
                self.emptyStateStack.isHidden = false
                self.tableView.separatorStyle = .none
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn_white")
            }else{
                self.emptyStateStack.isHidden = true
                self.tableView.separatorStyle = .singleLine
            }
            searchBar.setShowsCancelButton(true, animated: true)
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false
        filteredArray.removeAll()
        search.text = ""
        emptyStateStack.isHidden = true
        tableView.separatorStyle = .singleLine
        tableView.reloadData()
        view.endEditing(true)
    }
}


