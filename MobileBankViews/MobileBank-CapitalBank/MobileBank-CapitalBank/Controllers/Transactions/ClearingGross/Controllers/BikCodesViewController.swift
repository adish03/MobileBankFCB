//
//  BikCodesViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/5/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

protocol BikCodesProtocol {
    
    func selectedClearingGrossAccountUser(bikCode: BikCode)
}


class BikCodesViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    @IBOutlet weak var search: UISearchBar!
    var bikCodes = [BikCode]()
    var delegate: BikCodesProtocol?
    
    var isSearching = false
    var filteredArray = [BikCode]()
    var bikCode: BikCode!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        emptyStateStack.isHidden = true
        tableView.separatorStyle = .singleLine
        navigationItem.title = localizedText(key: "bic_codes")
        tableView.reloadData()
    }
    
}

extension BikCodesViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching{
            return filteredArray.count
        }else{
            return bikCodes.count
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BikCodeTableViewCell
        
        if isSearching{
            cell.codeLabel.text = filteredArray[indexPath.row].code
            cell.nameLabel.text =  filteredArray[indexPath.row].name
            if bikCode != nil{
                if filteredArray[indexPath.row].code == bikCode.code{
                    cell.isSelected = true
                }
            }

        }else{
            cell.codeLabel.text = bikCodes[indexPath.row].code
            cell.nameLabel.text = bikCodes[indexPath.row].name
            if bikCode != nil{
                if bikCodes[indexPath.row].code == bikCode.code{
                    cell.isSelected = true
                }
            }

        }
        return cell
    }
  
    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        if filteredArray.count != 0{
            let filteredCode = filteredArray[indexPath.row]
            delegate?.selectedClearingGrossAccountUser(bikCode: filteredCode)
        }else{
            let bikCode = bikCodes[indexPath.row]
            delegate?.selectedClearingGrossAccountUser(bikCode: bikCode)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isSelected {
            cell.setSelected(true, animated: false)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
}
extension BikCodesViewController: UISearchBarDelegate{
    //поиск
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            
            isSearching = false
            emptyStateStack.isHidden = true
            tableView.separatorStyle = .singleLine
            tableView.reloadData()
            
        }else{
            filteredArray = bikCodes.filter({ (bik: BikCode) -> Bool in
                
                if searchText.isNumeric{
                    return bik.code?.lowercased().contains(searchText.lowercased()) ?? true
                }else{
                    return bik.name?.lowercased().contains(searchText.lowercased()) ?? true
                }
            })
            
            if filteredArray.isEmpty{
                self.emptyStateStack.isHidden = false
                tableView.separatorStyle = .none
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn_white")
            }else{
                self.emptyStateStack.isHidden = true
                tableView.separatorStyle = .singleLine
            }
            searchBar.setShowsCancelButton(true, animated: true)
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false
        filteredArray.removeAll()
        search.text = ""
        emptyStateStack.isHidden = true
        tableView.separatorStyle = .singleLine
        tableView.reloadData()
        view.endEditing(true)
    }
}




