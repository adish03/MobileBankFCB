//
//  ClientStatusViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 7/10/21.
//  Copyright © 2021 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import Kingfisher

class ClientStatusViewController: BaseViewController {

    var typeOfButton = TypeButton.Gold
    
    @IBOutlet weak var silverView: UIView!
    @IBOutlet weak var goldView: UIView!
    @IBOutlet weak var platinumView: UIView!
    @IBOutlet weak var vipView: UIView!
    var viewArray = [UIView]()
    
    @IBOutlet weak var silverViewBackground: UIView!
    @IBOutlet weak var goldViewBackground: UIView!
    @IBOutlet weak var platinumViewBackground: UIView!
    @IBOutlet weak var vipViewBackground: UIView!
    
    @IBOutlet weak var silverLable: UILabel!
    @IBOutlet weak var goldLable: UILabel!
    @IBOutlet weak var platinumLable: UILabel!
    @IBOutlet weak var vipLable: UILabel!
    
    @IBOutlet weak var menu: UINavigationItem!
    
    @IBOutlet weak var clientStatus: UILabel!
    
    @IBAction func vipButton(_ sender: Any) {
        idetifyCleckedButton(clickedButton: TypeButton.VIP)
    }
    @IBAction func platinumButton(_ sender: Any) {
        idetifyCleckedButton(clickedButton: TypeButton.Platinium)
    }
    @IBAction func goldButton(_ sender: Any) {
        idetifyCleckedButton(clickedButton: TypeButton.Gold)
    }
    @IBAction func silverButton(_ sender: Any) {
        idetifyCleckedButton(clickedButton: TypeButton.Silver)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toDiscriptionSegue" {
            if let nextController = segue.destination as? StatusDescriptionViewController {
                nextController.typeOfButton = typeOfButton
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        costomizing()
        getStatus()
    }
    
    func idetifyCleckedButton(clickedButton: TypeButton) {
        switch clickedButton {
        case .Gold:
            typeOfButton = .Gold
        case .Silver:
            typeOfButton = .Silver
        case .VIP:
            typeOfButton = .VIP
        case .Platinium:
            typeOfButton = .Platinium
        }
        performSegue(withIdentifier: "toDiscriptionSegue", sender: self)
        
    }
   
    func costomizing() {
        viewArray = [silverView, goldView, platinumView, vipView]
        
        self.menu.title = localizedText(key: "status")
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "Menu"), for: .normal)
        backButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for: .touchUpInside)
        
        menu.leftBarButtonItem = UIBarButtonItem(customView: backButton)

    }
    
    // get's status
    func getStatus(){
        showLoading()
        managerApi.getStatus()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (status) in
                    guard let `self` = self else { return }
                    self.clientStatus.text = status.result == nil ? status.message : status.result?.first
                                        
                
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
