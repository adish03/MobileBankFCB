//
//  StatusDescriptionViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 8/10/21.
//  Copyright © 2021 Spalmalo. All rights reserved.

import UIKit

class StatusDescriptionViewController: BaseViewController {
    
    var typeOfButton: TypeButton?

    @IBOutlet weak var navigationTitle: UINavigationItem!
    @IBOutlet weak var statusDescriptionLable: UITextView!
    @IBAction func backButton(_ sender: Any) {
        performSegue(withIdentifier: "backToStatusСontroller", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let backButton = UIButton(type: .custom)
        backButton.setImage(UIImage(named: "ArrowBack"), for: .normal)
        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        navigationTitle.title = localizedText(key: "status")
        navigationTitle.leftBarButtonItem = UIBarButtonItem(customView: backButton)
        
        let helpers = try! String(contentsOfFile:  Bundle.main.path(forResource: "helpers", ofType: "css")!, encoding: .utf8)
        let typography = try! String(contentsOfFile:  Bundle.main.path(forResource: "typography", ofType: "css")!, encoding: .utf8)
        
        var html = "<!DOCTYPE html>"
        html += "<style type=\"text/css\">"
        html += helpers
        html += "</style>"
        html += "<style type=\"text/css\">"
        html += typography
        html += "</style>"
        html += getStatusText()
        statusDescriptionLable.attributedText = html.getFormattedText()
        statusDescriptionLable.sizeToFit()
    }
    
    func getStatusText() -> String {
        switch typeOfButton {
                case .Silver:
                    return localizedText(key: "loyalty_silver_status_info_mb")
                case .Gold:
                    return localizedText(key: "loyalty_gold_status_info_mb")
                case .Platinium:
                    return localizedText(key: "loyalty_platinum_status_info_mb")
                case .VIP:
                    return localizedText(key: "loyalty_vip_status_info_mb")
        default: break
            
        }
        return "";
    }
    
    
    @objc func backButtonPressed() {
        performSegue(withIdentifier: "backToStatusСontroller", sender: nil)
    }
}

public extension String{
    func getFormattedText() -> NSMutableAttributedString?{
        let input = self
        let result = try? NSMutableAttributedString(
            data: input.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options: [.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil
        )
        return result ?? NSMutableAttributedString(string: input)
    }
}

enum TypeButton {
    case Silver
    case Gold
    case Platinium
    case VIP
}
