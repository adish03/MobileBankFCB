//
//  CardViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/17/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

class CardViewController: BaseViewController, DepositProtocol {
   
    @IBOutlet weak var accountToView: UIView!
    @IBOutlet weak var internalView: UIView!
    @IBOutlet weak var accountNoLabel: UILabel!
    @IBOutlet weak var accountSumLabel: UILabel!
    @IBOutlet weak var accountToTextField: UITextField!
    @IBOutlet weak var sumOfTransactionTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet var currencyLabel: [UILabel]!
    @IBOutlet weak var accountNameReceiver: UILabel!
    
    // views error
    @IBOutlet weak var viewFromAccount: UIView!
   
    @IBOutlet weak var viewSumFromTextField: UIView!
    @IBOutlet weak var viewCommentTextField: UIView!
    @IBOutlet weak var viewToAccount: UIView!
    @IBOutlet weak var viewToInternalAccount: UIView!
    
    //label error
    @IBOutlet weak var labelErrorAccountFrom: UILabel!
    @IBOutlet weak var labelErrorAccount: UILabel!
    @IBOutlet weak var labelErrorInternalAccount: UILabel!
    @IBOutlet weak var labelErrorSum: UILabel!
    @IBOutlet weak var labelErrorComment: UILabel!
    
    var accounts: [Accounts] = []
    var accountsClone: [Accounts] = []
    var currencies = [Currency]()
    var operationID = 0
    var currencyIDcurrent = 0
    var selectedDeposit: Deposit!
    
    //Template IBOutlets
    var isCreateTemplate = false
    var isSchedule = false
    var StartDate = ""
    var EndDate: String? = ""
    var RecurringTypeID = 1
    var ProcessDay = 1
    var schedule: Schedule!
    //EditingTemplate
    var isEditTemplate = false
    var template: ContractModelTemplate!
    var internalTemplateFromStory: InternalOperationModel!
    //RepeatPayTemplate
    var isRepeatPay = false
    var isRepeatFromStory = false
    var isRepeatOperation = false
    var isChangeData = false
    
    @IBOutlet weak var labelErrorTemplateName: UILabel!
    @IBOutlet weak var labelErrorTemplateDescription: UILabel!
    @IBOutlet weak var viewTemplateNameTextFields: UIView!
    @IBOutlet weak var viewTemplateDescriptionTextField: UIView!
    @IBOutlet weak var TemplateDescriptionView: UIView!
    @IBOutlet weak var ScheduleView: UIView!
    @IBOutlet weak var templateNameTextField: UITextField!
    @IBOutlet weak var smsTemplateTextField: UITextField!
    @IBOutlet weak var templateDescriptionTextField: UITextField!
    
    @IBOutlet weak var switchPlanner: UISwitch!
    @IBOutlet weak var viewPlanner: UIView!
    @IBOutlet weak var periodTextField: UITextField!
    
    @IBOutlet weak var viewDateBegin: UIView!
    @IBOutlet weak var viewDateFinish: UIView!
    @IBOutlet weak var viewWeekDay: UIView!
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var viewDateOperation: UIView!
    
    @IBOutlet weak var dateBeginTextField: UITextField!
    @IBOutlet weak var dateFinishTextField: UITextField!
    
    @IBOutlet weak var dayNumberTextField: UITextField!
    @IBOutlet weak var dateOperationTextField: UITextField!
    @IBOutlet weak var weekDayTextField: UITextField!
    
    @IBOutlet weak var createCardButton: RoundedButton!
    var navigationTitle = ""
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if selectedDeposit != nil{
            let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
            accountNoLabel.text = fullAccountsText
            accountSumLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
            for currency in currencyLabel{
                currency.text = selectedDeposit.currency?.symbol
            }
            currencyIDcurrent = selectedDeposit.currencyID ?? 0
            if accountToTextField.text != ""{
                checkAccountAvailable(accountNo: accountToTextField.text ?? "", currencyID: selectedDeposit.currencyID ?? 0)
            }
        }else{
            accountNoLabel.text = localizedText(key: "write_off")
            accountSumLabel.text = localizedText(key: "select_account_or_card")
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = localizedText(key: "ElcardTransfer")//navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        hideKeyboardWhenTappedAround()
        getAccountsWithCurrencies(vc: nil)
        
        accountToTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControl.Event.editingDidEnd)
        sumOfTransactionTextField.addTarget(self, action: #selector(textFieldEditingDidChangeSum(_:)), for: UIControl.Event.editingDidEnd)
        
        accountToTextField.delegate = self
        sumOfTransactionTextField.delegate = self
        descriptionTextField.delegate = self
        
    }
    
    // Проверка поля "Сумма" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChangeSum(_ sender: Any) {
        let textNonZeroFirst = sumOfTransactionTextField.text
        let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
        let numberAsInt = Double(newString ?? "0")
        sumOfTransactionTextField.text = "\(numberAsInt ?? 0)"
        if selectedDeposit != nil{
            if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                showAlertController(localizedText(key: "insufficient_account_balance"))
                sumOfTransactionTextField.text = ""
            }
        }
        sumOfTransactionTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
    }
    
    // проверка на количество знаков
    @objc func textFieldEditingDidChange(_ sender: Any) {
        
        if accountToTextField.text?.count == Constants.ACCOUNTS_CHARACTERS_COUNT{
            getAccountForInternalTransaction(accountNo: accountToTextField.text ?? "", currencyID: currencyIDcurrent)
        }else{
            showAlertController(localizedText(key: "acc_len_must_be_greater_16"))
            accountToTextField.becomeFirstResponder()
            accountToTextField.stateIfEmpty(view: viewToInternalAccount, labelError: labelErrorInternalAccount)
        }
        
    }
    
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToAccount"{
            let vc = segue.destination as! AccountsViewController
            vc.delegate = self
            vc.accounts = accounts
            vc.selectedDeposit = selectedDeposit
            vc.navigationTitle = localizedText(key: "write_off")
        }
    }
    
    //    установака текущего счета
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
    }
    
    @IBAction func createButton(_ sender: Any) {
        showAlertController("Этот продукт в  режиме разработки.")
    }
    
}
extension CardViewController: UITextFieldDelegate{
    //    проверка поля на начало ввода информации
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField{
        case self.accountToTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewToInternalAccount, labelError: labelErrorInternalAccount)
            
        case self.sumOfTransactionTextField:
            
            viewTextFieldShouldBeginEditing(viewTextField: viewSumFromTextField, labelError: labelErrorSum)
            
        case self.descriptionTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewCommentTextField, labelError: labelErrorComment)
            
        default:
            break
        }
        return true
    }
    //    проверка поля на начало ввода информации на кол-во символов
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        if textField.isEqual(accountToTextField) {
            maxLength = Constants.ACCOUNTS_CHARACTERS_COUNT
        }else if textField.isEqual(sumOfTransactionTextField) {
            maxLength = 9
        }else if textField.isEqual(descriptionTextField) {
            maxLength = 100
        }
        return newLength <= maxLength
    }
}
extension CardViewController{
    //    получение счетов
    func  getAccountsWithCurrencies(vc: AccountsViewController?) {
        
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        
        let depositObservable:Observable<[Accounts]> = managerApi
            .getAccounts(currencyID:  Constants.NATIONAL_CURRENCY)
        
        
        showLoading()
        
        Observable
            .zip(currenciesObservable, depositObservable)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (Currencies, Accounts) in
                    guard let `self` = self else { return }
                    self.currencies = Currencies
                    if Accounts.count > 0{
                        self.accounts = self.accountsCurrencies(currency: self.currencies, accounts: Accounts.filter({$0.accountType == 2}))
                    }
                    vc?.accounts = self.accounts
                    vc?.tableView.reloadData()
                    
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    установка валюты
    func accountsCurrencies(currency: [Currency], accounts:[Accounts]) -> [Accounts]{
        
        for account in accounts{
            for item in account.items!{
                for id2 in currency{
                    if item.currencyID == id2.currencyID{
                        item.currency = id2
                    }
                }
            }
        }
        return accounts
    }
    //  получение счета для внутреннего перевода
    func getAccountForInternalTransaction(accountNo: String, currencyID: Int){
        managerApi
            .getAccountForInternalTransaction(accountNo: accountNo, currencyID: currencyID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (account) in
                    guard let `self` = self else { return }
                    self.accountNameReceiver.text = account
                    self.checkAccountAvailable(accountNo: accountNo, currencyID: currencyID)
                    self.hideLoading()
                    
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    проверка счета на доступность
    func checkAccountAvailable(accountNo: String, currencyID: Int){
        managerApi
            .getCheckAccountAvailable(accountNo: accountNo, currencyID: currencyID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (isCheck) in
                    guard let `self` = self else { return }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    создание перевода
    func postCreatInternalTrasaction(InternalOperationModel: InternalOperationModel){
        showLoading()
        managerApi
            .postInternalTransaction(InternalOperationModel: InternalOperationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                    guard let `self` = self else { return }
                    if self.isCreateTemplate || self.isEditTemplate{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                        self.view.window?.rootViewController = vc
                        
                        let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                        let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                        let navigationController = UINavigationController(rootViewController: destinationController)
                        vc.pushFrontViewController(navigationController, animated: true)
                        
                    }else{
                        if operation.operationID != nil{
                            self.operationID = operation.operationID ?? 0
                        }
                        self.allowOperation()
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    создание перевода между счетами
    func postCreatpostInternalOperationCustomerAccounts(InternalOperationModel: InternalOperationModel){
        showLoading()
        managerApi
            .postInternalOperationCustomerAccounts(InternalOperationModel: InternalOperationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                    guard let `self` = self else { return }
                    if self.isCreateTemplate || self.isEditTemplate{
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                        self.view.window?.rootViewController = vc
                        
                        let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                        let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                        let navigationController = UINavigationController(rootViewController: destinationController)
                        vc.pushFrontViewController(navigationController, animated: true)
                        
                    }else{
                        if operation.operationID != nil{
                            self.operationID = operation.operationID ?? 0
                        }
                        self.allowOperation()
                    }
                    self.hideLoading()
                    
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    заполнение текущего счета
    func fullSelectedDeposit(selectedDeposit: Deposit){
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        self.accountNoLabel.text = fullAccountsText
        self.accountSumLabel.text = "\(String(format:"%.2f", selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
        for currency in self.currencyLabel{
            currency.text = selectedDeposit.currency?.symbol
        }
        self.currencyIDcurrent = selectedDeposit.currencyID ?? 0
        
    }
   
    //    проверка прав
    func  allowOperation() {
        
    }
}
