//
//  AccountsCell.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 19/7/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import Foundation
import SnapKit
class AccountsCell: UITableViewCell {
    
    lazy var accountNameLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.font = .systemFont(ofSize: 13.0, weight: .bold)
        view.textAlignment = .left
        return view
    }()
    
    lazy var accountNumberLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 0
        view.textAlignment = .left
        return view
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    // кастомизация ячейки при выборе
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.accessoryType = selected ? .checkmark : .none
        
    }
    
    override func layoutSubviews() {
        addSubview(accountNameLabel)
        accountNameLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.right.equalToSuperview().offset(-30)
            make.left.equalToSuperview().offset(30)
            make.height.equalTo(50)
        }
        addSubview(accountNumberLabel)
        accountNumberLabel.snp.makeConstraints { make in
            make.top.equalTo(accountNameLabel.snp.bottom)
            make.right.equalToSuperview().offset(-30)
            make.left.equalToSuperview().offset(30)
            make.height.equalTo(40)
        }
    }
}
