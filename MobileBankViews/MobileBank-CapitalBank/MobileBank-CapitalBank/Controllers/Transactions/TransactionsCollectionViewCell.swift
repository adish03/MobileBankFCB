//
//  TransactionsCollectionViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/15/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

class TransactionsCollectionViewCell: UICollectionViewCell {
   
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    
}
