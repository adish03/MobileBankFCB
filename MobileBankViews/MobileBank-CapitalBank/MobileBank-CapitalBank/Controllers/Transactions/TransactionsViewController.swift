//
//  TransactionsViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/21/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore

class TransactionsViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout  {
   
    struct Transaction {
        var image: String
        var name: String
        var operationTypeId: Int
    }

    @IBOutlet weak var menu: UIBarButtonItem!

    @IBOutlet weak var collectionView: UICollectionView!
    var transactions = [Transaction]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = self.localizedText(key: "transfers")
        if  menu != nil{
            menu.target = self.revealViewController()
            menu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        let transaction = Transaction(image: "banking_transfer-1",
                                      name: self.localizedText(key: ("Resources_Internal_Customer_Transaction")),
                                      operationTypeId: InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue)
        let transactionInternal = Transaction(image: "banking_transfer-2",
                                              name: self.localizedText(key: ("internal_bank_transfer")),
                                              operationTypeId: InternetBankingOperationType.InternalOperation.rawValue)
        let transactionClearingGross = Transaction(image: "Clearing_GrossCreat",
                                                   name: self.localizedText(key: ("clearing_gross_transfer")),
                                                   operationTypeId: InternetBankingOperationType.CliringTransfer.rawValue)
        let transactionSwift = Transaction(image: "SWIFTCreate",
                                           name: self.localizedText(key: ("swift_transfer")),
                                           operationTypeId:  InternetBankingOperationType.SwiftTransfer.rawValue)

        let transactionCard = Transaction(image: "transfer_to_elcart",
                                          name: self.localizedText(key: ("ElcardTransfer")),
                                          operationTypeId: InternetBankingOperationType.CardOperation.rawValue)
        
        let xFincaTransfers = Transaction(image: "banking_transfer-2", name: self.localizedText(key: "MoneyTransfers"), operationTypeId: InternetBankingOperationType.MoneyTransfer.rawValue)
        
        allowOperations(transaction: transaction,
                        transactionInternal: transactionInternal,
                        transactionClearingGross: transactionClearingGross,
                        transactionSwift: transactionSwift,
                        transactionCard: transactionCard,
                        trasactionX_Finca: xFincaTransfers
        )
        
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return transactions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! TransactionsCollectionViewCell
       
        cell.image.image = UIImage(named: transactions[indexPath.row].image)
        cell.name.text =  transactions[indexPath.row].name
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionView.frame.width / 2 - 16, height: 160)
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let object = transactions[indexPath.row]
      
        switch  object.operationTypeId {
        case InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue:
            performSegue(withIdentifier: "toAccountsTransactionSegue", sender: self)
            
        case InternetBankingOperationType.InternalOperation.rawValue:
            performSegue(withIdentifier: "toInternalTransactionSegue", sender: self)
            
        case InternetBankingOperationType.CliringTransfer.rawValue:
            performSegue(withIdentifier: "toClearingSegue", sender: self)
            
        case InternetBankingOperationType.SwiftTransfer.rawValue:
            performSegue(withIdentifier: "toSwiftSegue", sender: self)
            
        case InternetBankingOperationType.CardOperation.rawValue:
            performSegue(withIdentifier: "toCardSegue", sender: self)
            
        case InternetBankingOperationType.MoneyTransfer.rawValue:
            performSegue(withIdentifier: "toXFincaSegue", sender: self)
            
        default:
            break
        }
       
    }
    
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toInternalTransactionSegue"{
            let vc = segue.destination as! TransactionViewController
            vc.navigationTitle = self.localizedText(key: "internal_bank_transfer")
            vc.operationTypeID = InternetBankingOperationType.InternalOperation.rawValue
        }
        if segue.identifier == "toAccountsTransactionSegue"{
            let vc = segue.destination as! TransactionViewController
            vc.navigationTitle = self.localizedText(key: "Resources_Internal_Customer_Transaction")
            vc.operationTypeID = InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue
        }
        if segue.identifier == "toClearingSegue"{
            let vc = segue.destination as! DetailClearingGrossViewController
            vc.navigationTitle =  self.localizedText(key:"Resources_ClearingGross_Common_Title")
        }
        if segue.identifier == "toSwiftSegue"{
            let vc = segue.destination as! DetailSwiftViewController
            vc.navigationTitle =  self.localizedText(key:"swift_transfer")
            
        }
        if segue.identifier == "toCardSegue"{
            let vc = segue.destination as! CardViewController
            vc.navigationTitle =   self.localizedText(key: ("ElcardTransfer"))
        }
        if segue.identifier == "toXFincaSegue"{
            let vc = segue.destination as! XFincaViewController
            // vc.navigationTitle = NSLocalizedString("x_transfer", comment: "Быстрые переводы")
        }
    }
}

extension TransactionsViewController{
    //    права доступа
    func allowOperations(transaction: Transaction, transactionInternal: Transaction, transactionClearingGross: Transaction, transactionSwift: Transaction, transactionCard: Transaction, trasactionX_Finca: Transaction){
        transactions.removeAll()
        
        if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsAddAllowed"){
            transactions.append(transaction)
            transactions.append(transactionInternal)
        }
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed"){
            transactions.append(transactionSwift)
        }
        
        if  AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed"){
            transactions.append(transactionClearingGross)
        }
        transactions.append(trasactionX_Finca)
    }
}

