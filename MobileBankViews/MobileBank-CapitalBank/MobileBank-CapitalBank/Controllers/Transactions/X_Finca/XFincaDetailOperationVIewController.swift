//
//  XFincaDetailOperationVIewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 9/3/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

class XFincaDetailOperationViewController: BaseViewController {
    @IBOutlet weak var paymentDirectionLabel: UILabel!
    @IBOutlet weak var moneySystemLabel: UILabel!
    @IBOutlet weak var senderNameLabel: UILabel!
    @IBOutlet weak var transferSumLabel: UILabel!
    @IBOutlet weak var paymentCodeLabel: UILabel!
    @IBOutlet weak var senderCountryNameLabel: UILabel!
    @IBOutlet weak var transferAccountNoLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var receiverCustomerName: UILabel!
    @IBOutlet weak var transferCurrencyID: UILabel!
    @IBOutlet weak var totalCommission: UILabel!
    
    @IBOutlet weak var commissionView: UIView!
    @IBOutlet weak var paymentCodeView: UIView!
    
    var operationModel: OperationModel!
    var createOperationModel: CreateMoneyTransfer!
    var moneySystemName: String!
    var operationID: Int!
    var senderCountry: String!
    var senderName: String!
    
    var currencies: [Currency]!
    var countries: [CountryReferenceItemModel]!
    var xFincaCountries: [XFincaCountryDictionary]!
    var moneySystems: [MoneySystem]!
    
    var isNeedOperationDetail = false
    var delegate: NoNeedLoadDataDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        barButtonCustomize()
        if isNeedOperationDetail{
            getCurrencies()
        } else{
            paymentDirectionLabel.text = localizedText(key: "Incoming")
            moneySystemLabel.text = moneySystemName
            senderNameLabel.text = senderName
            transferSumLabel.text = "\(createOperationModel.transferSumm!)"
            paymentCodeLabel.text = createOperationModel.paymentCode
            transferAccountNoLabel.text = createOperationModel.transferAccountNo
            commentLabel.text = createOperationModel.paymentComment
        }
    }
    
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        let image = "Close"
        button.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        button.setImage(UIImage(named: image), for: .normal)
        
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 50)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    
    @objc func closeButtonPressed() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    func  getInfoOperationTransactions(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationTransactions(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    if operation.directionTypeID == 0 {
                        self.commissionView.isHidden = true
                        self.paymentCodeView.isHidden = true
                    } else {
                        self.commissionView.isHidden = false
                        self.paymentCodeView.isHidden = false
                    }
                    self.paymentDirectionLabel.text = operation.directionTypeID == 0 ? self.localizedText(key: "Outgoing") : self.localizedText(key: "Incoming")
                    self.moneySystemLabel.text = self.moneySystems.first{ $0.systemId == operation.moneySystemID }?.moneySystemName
                    self.senderNameLabel.text = operation.senderCustomerName
                    self.receiverCustomerName.text = operation.receiverCustomerName
                    self.transferSumLabel.text = String(format:"%.2f", operation.transferSumm ?? 0.0)
                    self.paymentCodeLabel.text = operation.paymentCode
                    self.transferAccountNoLabel.text = operation.transferAccountNo
                    self.commentLabel.text = operation.paymentComment
                    self.totalCommission.text = String(format:"%.2f", operation.totalCommission ?? 0.0)
                    self.transferCurrencyID.text = self.currencies.first{$0.currencyID == operation.transferCurrencyID}?.currencyName
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func getCurrencies(){
        
        let currencyObservable = managerApi.getCurrenciesWithoutCertificate()
        let countryObservable = managerApi.getCountries()
        let getCustomerData = managerApi.getCustomerData()
        let transferCountriesObservable = managerApi.getXFincaCountries()
        let moneySystemObservable = managerApi.getMoneySystem()
        
        showLoading()
        Observable
            .zip(currencyObservable, countryObservable, getCustomerData, transferCountriesObservable, moneySystemObservable)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (currencies, countries, customerData, transferCountries, moneySystems) in
                    guard let `self` = self else { return }
                    self.currencies = currencies
                    self.countries = countries
                    self.xFincaCountries = transferCountries
                    self.moneySystems = moneySystems
                    self.getInfoOperationTransactions(operationID: self.operationID)
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
