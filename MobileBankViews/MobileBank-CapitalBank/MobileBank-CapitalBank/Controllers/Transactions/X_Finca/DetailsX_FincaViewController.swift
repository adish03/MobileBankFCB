//
//  DetailsX_FincaViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 4/10/21.
//  Copyright © 2021 Spalmalo. All rights reserved.
//

import UIKit

class DetailsX_FincaViewController: BaseViewController {
        
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emptyStateLable: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    var navigationTitle = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationTitle
        tableView.isHidden = true
        
    }
    
    @IBAction func createXtransfers(_ sender: Any) {
        performSegue(withIdentifier: "createTransferSegue", sender: self)
    }
}
