//
//  XFincaSubmitViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 14/2/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

class XFincaSubmitViewController: BaseViewController {
    
    @IBOutlet weak var paymentDirection: UILabel!
    @IBOutlet weak var paymentSystem: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var currency: UILabel!
    @IBOutlet weak var paymentCode: UILabel!
    @IBOutlet weak var accountNo: UILabel!
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var selectedDeposit: Deposit!
    var payData: CreateMoneyTransfer!
    var operationModel: OperationModel!
    var moneySystemName: String!
    var operationID = 0
    var currencySymbol: String!
    var senderFullName: String!
    var countryName: String!
    var currentCurrency = ""
    var imageUtility = ""

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = localizedText(key: "confirmation")

        let sum = "\(payData.transferSumm!)"
        paymentDirection.text = localizedText(key: "Incoming")
        paymentSystem.text = moneySystemName
        currency.text = currentCurrency
        amount.text = sum
        paymentCode.text = payData.paymentCode
        accountNo.text = payData.transferAccountNo
        senderName.text = senderFullName
        descriptionLabel.text = payData.paymentComment
        
    }
    
    @IBAction func submitButton(_ sender: Any) {
        let operationModel = OperationModel(operationID: self.operationID)
        postConfirmPay(operationModel: operationModel)
    }
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as! XFincaDetailOperationViewController
        vc.operationModel = self.operationModel
        vc.operationID = self.operationID
        vc.senderName = self.senderFullName
        vc.senderCountry = self.countryName
        vc.moneySystemName = self.moneySystemName
        vc.createOperationModel = self.payData
    }
// Подтвердить операцию
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postMoneyTransferConfirm(operationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    self.performSegue(withIdentifier: "toDetailXFinca", sender: self)
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
