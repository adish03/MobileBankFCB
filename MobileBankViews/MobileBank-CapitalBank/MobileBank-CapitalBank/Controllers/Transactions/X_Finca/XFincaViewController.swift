//
//  XFincaViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 4/10/21.
//  Copyright © 2021 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

class XFincaViewController: BaseViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, DepositProtocol  {
    
    @IBOutlet weak var transferedSumView: UIView!
    @IBOutlet weak var viewFromCode: UIView!
    @IBOutlet weak var viewFromAccount: UIView!
    @IBOutlet weak var paymentDiractionEditText: UITextField!
    @IBOutlet weak var paymentSystemEditText: UITextField!
    @IBOutlet weak var currenciesEditText: UITextField!
    @IBOutlet weak var transferCommentTextField: UITextField!
    @IBOutlet weak var paymentCodeView: UIView!
    
    @IBOutlet weak var commissionField: UITextField!
    @IBOutlet weak var commissionView: UIView!
    
    @IBOutlet weak var summOfTransactionTextField: UITextField!
    @IBOutlet weak var payFrom_accountsLabel: UILabel!
    @IBOutlet weak var choiceCard_accountsLabel: UILabel!
    @IBOutlet weak var currencySymbolLabel: UILabel!
    @IBOutlet weak var transferCodeEditText: UITextField!
    
    @IBOutlet weak var transferSearchResultHolder: UIView!
    
    @IBOutlet weak var nameOfSenderLable: UILabel!
    @IBOutlet weak var countryOfSenderLable: UILabel!
    
    //error
    @IBOutlet weak var depositErrorlable: UILabel!
    @IBOutlet weak var transferedSummErrorLable: UILabel!
    
    //Исходящие платежи
    @IBOutlet weak var transferCountryView: UIView!
    
    @IBOutlet weak var transferCityView: UIView!
    
    @IBOutlet weak var transferFirstNameView: UIView!
    @IBOutlet weak var transferSecondNameView: UIView!
    
    @IBOutlet weak var transferLastNameView: UIView!
    @IBOutlet weak var transferPhoneNumberView: UIView!
    
    @IBOutlet weak var transferCountryField: UITextField!
    
    @IBOutlet weak var transferCityField: UITextField!
    
    @IBOutlet weak var receiverFirstName: UITextField!
    @IBOutlet weak var receiverSecondName: UITextField!
    @IBOutlet weak var receiverLastName: UITextField!
    @IBOutlet weak var phoneNumberLabel: UITextField!
    
    //XFinca Accounts
    var transactionAccounts: [Accounts]!
    var selectedDeposit: Deposit!
    var submitModel: CreateMoneyTransfer!
    var operationID: Int = 0
    var phoneNumberLength: Int = 0
    var selectCityCount: Int? = nil
    
    // payment diraction
    var arrayOfDirections = [String]()
    var Directions = [expenseTypeModel]()
    let pickerViewDirection = UIPickerView()
    var operationDirection: String!
    
    // transfer System
    var arrayOfTransferSystem = [MoneySystem]()
    var arrayOfMoneySystemCopy = [MoneySystem]()
    var pickerViewTransferSystem = UIPickerView()
    var Systems = [expenseTypeModel]()
    var pickedSystemModel: MoneySystem?
    var countries: [CountryReferenceItemModel]?
    var operationMoneySystem: String!
    
    // currencies
    var arrayOfCurrencies = [Currency]()
    var arrayOfCurrenciesCopy = [Currency]()
    var arrayOfCurrenciesCopy2 = [Currency]()
    var pickerViewCurrencies = UIPickerView()
    var Currencies = [expenseTypeModel]()
    var currencyModel: Currency?
    var operationCurrency: String!
    var contragentCountryID: Int?
    var conversionSum: Double?
    var customerID: Int?
    var customerName: String?
    var senderNameCustomer: String?
    var senderCountryName: String!
    
    var sortedArrayOfCityCurrencies: [Currency] = []
    var cityCurrencies: [Int?] = []
    var sortedCityCurrency: [Int] = []
    
    //dictionaries
    var arrayOfCountries = [XFincaCountryDictionary]()
    var arrayOfCountriesCopy = [XFincaCountryDictionary]()
    var pickerViewCountries = UIPickerView()
    var pickedCountry: XFincaCountryDictionary?
    var operationCountry: String!
    var operationCountryCode: Int!
    
    var arrayOfCities = [XFincaCityDictionary]()
    var arrayOfCitiesCope = [XFincaCityDictionary]()
    var pickerViewCities = UIPickerView()
    var pickedCity: XFincaCityDictionary?
    var operationCity: String!
    var operationCityCode: Int!
    var totalCommission: Double!
    
    //set's picked value if clicks for the second time
    var directionRow   = 0
    var moneySystemRow = 0
    var currencyRow    = 0
    var countryRow     = 0
    var cityRow        = 0
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return arrayOfDirections.count
        } else if pickerView.tag == 1 {
            setMoneyTransferView()
            arrayOfTransferSystem = arrayOfMoneySystemCopy
            if directionRow == 0 {
                return arrayOfTransferSystem.filter{$0.isIBReceivingAllowed == true}.count
            }
            else {
                let count = arrayOfTransferSystem.filter{$0.isIBSendingAllowed == true}.count
                if(count != 0) {
                    getXFincaCountries()
                }
                return count
            }
        } else if pickerView.tag == 2 {
            return sortedCityCurrency.count
        } else if pickerView.tag == 3 {
            return arrayOfCountries.count
        } else if pickerView.tag == 4 {
            return arrayOfCities.count
        }
        
        return 1
    }
    
    // Displays the values
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == 0 {
            return arrayOfDirections[row]
        } else if pickerView.tag == 1 {
            arrayOfTransferSystem = arrayOfMoneySystemCopy
            if directionRow == 0 {
                return arrayOfTransferSystem.filter{$0.isIBReceivingAllowed == true}[row].moneySystemName
            }
            else {
                return arrayOfTransferSystem.filter{$0.isIBSendingAllowed == true}[row].moneySystemName
            }
        } else if pickerView.tag == 2 {
            if sortedCityCurrency[row] == 810 {
                for i in arrayOfCurrencies {
                    if i.currencyID == 643 {
                        return i.symbol
                    }
                }
            }
            for itemCurrencies in arrayOfCurrencies {
                if itemCurrencies.currencyID == sortedCityCurrency[row] {
                    return itemCurrencies.symbol
                }
            }
            
        } else if pickerView.tag == 3 {
            return arrayOfCountries[row].shortName
        } else if pickerView.tag == 4 {
            return arrayOfCities[row].name
        }
        return ""
    }
    //   обработка нажатия  pickerView
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            directionRow = row
            for direction in Directions{
                if direction.text == arrayOfDirections[row]{
                    operationDirection = "\(String(describing: direction.value ?? 0))"
                }
            }
            if directionRow == 1 {
                transferCityView.isHidden = false
                transferCountryView.isHidden = false
                paymentCodeView.isHidden = true
                transferPhoneNumberView.isHidden = false
                transferSecondNameView.isHidden = false
                transferFirstNameView.isHidden = false
                transferLastNameView.isHidden = false
                commissionView.isHidden = false
            } else {
                transferCityView.isHidden = true
                transferCountryView.isHidden = true
                paymentCodeView.isHidden = false
                transferPhoneNumberView.isHidden = true
                transferSecondNameView.isHidden = true
                transferFirstNameView.isHidden = true
                transferLastNameView.isHidden = true
                commissionView.isHidden = true
            }
            paymentSystemEditText.text = ""
            return paymentDiractionEditText.text = arrayOfDirections[row]
        }
        if pickerView.tag == 1{
            moneySystemRow = row
            arrayOfTransferSystem = directionRow == 0 ? arrayOfTransferSystem.filter{$0.isIBReceivingAllowed == true}
            : arrayOfTransferSystem.filter{$0.isIBSendingAllowed == true}
            for system in Systems{
                if system.text == arrayOfTransferSystem[row].moneySystemName {
                    operationMoneySystem = "\(String(describing: system.value ?? 0))"
                }
            }
            if !arrayOfTransferSystem.isEmpty {
                pickedSystemModel = arrayOfTransferSystem[row]
                return paymentSystemEditText.text = arrayOfTransferSystem[row].moneySystemName
            } else {
                return paymentSystemEditText.text = ""
            }
        }
        
        //            for currency in Currencies{
        //                if currency.text == arrayOfCurrencies[row].symbol {
        //                    operationCurrency = "\(String(describing: currency.value ?? 0))"
        //                }
        //            }
        //            currencySymbolLabel.text = " \(arrayOfCurrencies[row].symbol!)"
        //            currencyModel = arrayOfCurrencies[row]
        
        if pickerView.tag == 2 {
            currencyRow = row
            
            if sortedCityCurrency[row] == 810 {
                currenciesEditText.text = "RUB"
                currencySymbolLabel.text = "RUB"
                operationCurrency = "RUB"
                
                for i in arrayOfCurrencies {
                    if i.currencyID == 643 {
                        currencyModel = i
                    }
                }
            } else {
                for itemCurrencies in arrayOfCurrencies {
                    if itemCurrencies.currencyID == sortedCityCurrency[row] {
                        currenciesEditText.text = itemCurrencies.symbol
                        currencySymbolLabel.text = itemCurrencies.symbol
                        operationCurrency = itemCurrencies.symbol
                        currencyModel = itemCurrencies
                    }
                }
            }
        }
        
        if pickerView.tag == 3 {
            countryRow = row
            for country in arrayOfCountries {
                if country.countryCode == arrayOfCountries[row].countryCode {
                    operationCountry = country.shortName
                    operationCountryCode = country.countryID
                }
            }
            pickedCountry = arrayOfCountries[row]
            getCity(countryCode: arrayOfCountries[row].countryID!)
            getPhoneNumberMask(countryID: arrayOfCountries[row].countryID!)
            return transferCountryField.text = arrayOfCountries[row].shortName
        }
        if pickerView.tag == 4 {
            cityRow = row
            for city in arrayOfCities {
                if city.id == arrayOfCities[row].id {
                    operationCity = city.name
                    operationCityCode = city.id
                }
            }
            pickedCity = arrayOfCities[row]
            cityCurrencies = pickedCity?.currencies ?? []
            
            sortedCityCurrency = []
            var once = true
            for i in cityCurrencies {
                
                
                for d in arrayOfCurrencies {
                    if i == d.currencyID {
                        sortedCityCurrency.append(i ?? 0)
                    }
                    if i == 810 && once == true {
                        once = false
                        sortedCityCurrency.append(810)
                    }
                }
            }
        
            return transferCityField.text = arrayOfCities[row].name
        }
    }
    
    //MARK: - ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
    }
    //MARK: - ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCurrencies()
        arrayOfDirections = [localizedText(key: "Incoming"), localizedText(key: "Outgoing")]
        getAccounts()
        getMoneySystems()
        
        if self.selectedDeposit != nil{
            self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
        }
        
        hideKeyboardWhenTappedAround()
        
        paymentDiractionEditText.delegate = self
        pickerViewDirection.delegate = self
        
        paymentSystemEditText.delegate = self
        pickerViewTransferSystem.delegate = self
        
        currenciesEditText.delegate = self
        pickerViewCurrencies.delegate = self
        
        transferCountryField.delegate = self
        pickerViewCountries.delegate = self
        
        transferCityField.delegate = self
        pickerViewCities.delegate = self
        
        summOfTransactionTextField.addTarget(self, action: #selector(textFieldEditingEndTransferedSumm(_:)), for: UIControl.Event.editingDidEnd)
    }
    
    func setMoneyTransferView(){
        let direction = directionRow == 0 ? 1 : 0
        if direction == 0 {
            transferCityView.isHidden = false
            transferCountryView.isHidden = false
            commissionView.isHidden = false
            transferFirstNameView.isHidden = false
            transferSecondNameView.isHidden = false
            transferLastNameView.isHidden = false
            transferPhoneNumberView.isHidden = false
            paymentCodeView.isHidden = true
        } else {
            transferCityView.isHidden = true
            transferCountryView.isHidden = true
            commissionView.isHidden = true
            transferFirstNameView.isHidden = true
            transferSecondNameView.isHidden = true
            transferLastNameView.isHidden = true
            transferPhoneNumberView.isHidden = true
            paymentCodeView.isHidden = false
        }
    }
    
    @objc func textFieldEditingEndTransferedSumm(_ sender: Any) {
        summOfTransactionTextField.stateIfEmpty(view: transferedSumView, labelError: transferedSummErrorLable)
    }
    
    //    проверка на фокус поля ввода
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if (textField == paymentDiractionEditText) {
            //            if !arrayOfDirections.isEmpty{
            //                paymentDiractionEditText.text = arrayOfDirections[directionRow]
            //            }
            //            operationDirection = nil
        }
        
        if (textField == paymentSystemEditText) {
            arrayOfTransferSystem = directionRow == 0 ? arrayOfTransferSystem.filter{$0.isIBReceivingAllowed == true}
            : arrayOfTransferSystem.filter{$0.isIBSendingAllowed == true}
            if !arrayOfTransferSystem.isEmpty {
                paymentSystemEditText.text = arrayOfTransferSystem[moneySystemRow].moneySystemName
                pickedSystemModel = arrayOfTransferSystem[moneySystemRow]
            }
            operationMoneySystem = nil
        }
        
        if (textField == currenciesEditText) {
            if !arrayOfCurrencies.isEmpty{
                currenciesEditText.text = arrayOfCurrencies[currencyRow].symbol
            }
            operationCurrency = nil
        }
        if (textField == transferCountryField) {
            if !arrayOfCountries.isEmpty {
                transferCountryField.text = arrayOfCountries[currencyRow].shortName
            }
        }
        
        return true
    }
    
    func pickerSettings(){
        // diraction
        paymentDiractionEditText.inputView = pickerViewDirection
        pickerViewDirection.tag = 0
        //money system
        paymentSystemEditText.inputView = pickerViewTransferSystem
        pickerViewTransferSystem.tag = 1
        //currencies
        currenciesEditText.inputView = pickerViewCurrencies
        pickerViewCurrencies.tag = 2
        //country
        transferCountryField.inputView = pickerViewCountries
        pickerViewCountries.tag = 3
        
        transferCityField.inputView = pickerViewCities
        pickerViewCities.tag = 4
        
        defaultPickerValue()
        for direction in Directions {
            if direction.value == Int(self.operationDirection) ?? 0{
                operationDirection = "\(String(describing: direction.value ?? 0))"
                
                paymentDiractionEditText.text = direction.text
            }
        }
        for currency in Currencies {
            if currency.value == Int(self.operationCurrency) ?? 0 {
                operationCurrency = "\(String(describing: currency.value ?? 0))"
                currenciesEditText.text = currency.text
            }
        }
        for country in arrayOfCountries {
            if country.countryCode == self.operationCountryCode {
                transferCountryField.text = country.shortName
            }
        }
        for city in arrayOfCities {
            if city.id == self.operationCityCode {
                transferCityField.text = city.name
            }
        }
    }
    // sets default value to pickers
    func defaultPickerValue() {
        paymentDiractionEditText.text = arrayOfDirections.first
        currenciesEditText.text = arrayOfCurrencies.first?.symbol
        transferCountryField.text = arrayOfCountries.first?.shortName
    }
    //MARK: - Segue Prepare
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toTransactionAccountSegue" {
            if summOfTransactionTextField.text != ""{
            
            let vc = segue.destination as! AccountsViewController
            if transactionAccounts != nil {
                for account in transactionAccounts{
                    account.items = account.items?.filter{$0.balance != 0.0}
                }
                transactionAccounts = transactionAccounts.filter{$0.items?.count != 0}
                transactionAccounts = transactionAccounts.filter{$0.accountType != 1}
                vc.accounts = transactionAccounts
            }
            vc.delegate = self
            vc.navigationTitle = directionRow == 0 ? localizedText(key: "write_off") : localizedText(key: "transfer_to")
            vc.selectedDeposit = selectedDeposit
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        }
        
        if segue.identifier == "PaymentHistoryViewController" {
            let vc = segue.destination as! PaymentHistoryViewController
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        
        if segue.identifier == "toSubmitXFincaSegue"{
            let vc = segue.destination as! XFincaSubmitViewController
            
            vc.payData = submitModel
            vc.selectedDeposit = selectedDeposit
            vc.operationID = operationID
            vc.moneySystemName = paymentSystemEditText.text
            vc.senderFullName = senderNameCustomer
            vc.countryName = senderCountryName
            
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            
        }
    }
    
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
        
        if self.selectedDeposit != nil{
            self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit )
        }
    }
    
    //    заполнение текущего счета
    func fullSelectedDeposit(selectedDeposit: Deposit){
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        payFrom_accountsLabel.text = fullAccountsText
        choiceCard_accountsLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currencySymbol ?? ""))"
        
        viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: depositErrorlable)
        
        if directionRow != 0 {
            self.getCommission(countryID: (pickedCountry?.countryID)!)
        }
    }
    
    @IBAction func createTransfer(_ sender: Any){
        let direction = directionRow == 0 ? 1 : 0
        let senderCustomerID = directionRow == 0 ? nil : customerID
        let receiverCustomerID = directionRow == 0 ? customerID : nil
        let senderFullName = directionRow == 0 ? senderNameCustomer : customerName
        if directionRow == 1 {
            contragentCountryID = operationCountryCode
        }
        let receiverFullName = directionRow == 0 ? customerName : "\(receiverSecondName.text!) \(receiverFirstName.text!) \(receiverLastName.text!)"
        if selectedDeposit != nil && summOfTransactionTextField.text != nil && transferCommentTextField.text != nil {
            
            if let date = UserDefaultsHelper.bankDate{
                
                submitModel = CreateMoneyTransfer(
                    operationType: InternetBankingOperationType.MoneyTransfer.rawValue,
                    moneySystemID: pickedSystemModel?.systemId,
                    directionTypeID: direction,
                    transferCurrencyID: currencyModel?.currencyID,
                    transferSumm: Double(summOfTransactionTextField.text!),
                    contragentCountryID: contragentCountryID,
                    goldenCrownContragentCountryID: operationCountryCode,
                    goldenCrownContragentCityID: operationCityCode,
                    goldenCrownContactPhone: phoneNumberLabel.text,
                    receiverCustomerID: receiverCustomerID,
                    receiverCustomerName: receiverFullName,
                    senderCustomerID: senderCustomerID,
                    senderCustomerName: senderFullName,
                    transferDate: date,
                    paymentCode: transferCodeEditText.text,
                    paymentComment: transferCommentTextField.text ?? "",
                    bankCommission: 0.0,
                    totalCommission: totalCommission,
                    convertedTransferSum: conversionSum,
                    transferAccountNo: selectedDeposit.accountNo,
                    transferAccountCurrencyID: selectedDeposit.currencyID
                )
                postConfirmPay(operationModel: submitModel)
            } else {
                showAlertController(localizedText(key: "fill_in_all_the_fields"))
            }
            
        }
        else {
            showAlertController(localizedText(key: "fill_in_all_the_fields"))
        }
    }
    
    func convertDateFormatter(date: Date) -> String {
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let string = dateFormatterPrint.string(from: date)
        return string
    }
    
    @IBAction func transferButton(_ sender: Any) {
        if selectedDeposit != nil
            && !(summOfTransactionTextField.text?.isEmpty ?? false){
            
            checkIfTransferExist(paymentCode: transferCodeEditText.text ?? "",
                                 moneySystemID: pickedSystemModel?.systemId ?? 0,
                                 transferSum: Double(summOfTransactionTextField.text!)!,
                                 transferCurrencyID: currencyModel?.currencyID ?? 0)
        } else {
            
            depositErrorlable.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: depositErrorlable)
            
            summOfTransactionTextField.stateIfEmpty(view: transferedSumView, labelError: transferedSummErrorLable)
            showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
        }
    }
}

extension XFincaViewController {
    // get's moneySystems
    func getMoneySystems() {
        showLoading()
        managerApi
            .getMoneySystem()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] systems in
                    guard let `self` = self else { return }
                    self.arrayOfTransferSystem = systems.filter{$0.isIBSendingAllowed == true || $0.isIBReceivingAllowed == true}
                    let moneySystemCopy = self.arrayOfTransferSystem.map({$0.copy() as! MoneySystem})
                    self.arrayOfMoneySystemCopy = moneySystemCopy
                    self.pickerSettings()
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    func getXFincaCountries(){
        let transferCountriesObservable = managerApi.getXFincaCountries()
        showLoading()
        transferCountriesObservable
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (transferCountries) in
                    self?.arrayOfCountries = transferCountries
                    self?.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                }
            )
            .disposed(by: disposeBag)
    }
    
    
    func getCurrencies(){
        
        let currencyObservable = managerApi.getCurrenciesWithoutCertificate()
        let countryObservable = managerApi.getCountries()
        let getCustomerData = managerApi.getCustomerData()
        
        showLoading()
        Observable
            .zip(currencyObservable, countryObservable, getCustomerData)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (currencies, countries, customerData) in
                    guard let `self` = self else { return }
                    self.arrayOfCurrencies = currencies
                    let currenciesCopy = self.arrayOfCurrencies.map({$0.copy() as! Currency})
                    self.arrayOfCurrenciesCopy = currenciesCopy
                    
                    self.arrayOfCurrenciesCopy2 = self.arrayOfCurrencies.map({$0.copy() as! Currency})
                    
                    self.countries = countries
                    self.customerID = customerData.customerID
                    self.customerName = customerData.customerName
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    
    // get's accounts
    func getAccounts(){
        showLoading()
        managerApi.getAccounts(currencyID: Constants.NATIONAL_CURRENCY)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (accounts) in
                    guard let `self` = self else { return }
                    self.transactionAccounts = accounts
                    
                    if self.selectedDeposit != nil{
                        self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit )
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    func getCommission(countryID: Int){
        showLoading()
        managerApi
            .getXFincaCommission(customerID: customerID!, transferSum: Double(summOfTransactionTextField.text!)!, currencyID: (currencyModel?.currencyID)!, countryID: countryID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (commission) in
                    guard let `self` = self else { return }
                    if commission.data?.feeTotal != nil {
                        self.commissionField.text = "\((commission.data?.feeTotal?.amount)! / 100)"
                    } else {
                        self.commissionField.text = "0.0"
                    }
                    //                    self.totalCommission = commission.data?.feeTotal?.amount ?? 0.0
                    self.totalCommission = (commission.data?.feeTotal?.amount ?? 0.0) / 100
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    func getConversionSum(){
        let direction = directionRow == 0 ? 1 : 0
        let transferSum = Double(summOfTransactionTextField.text!)! + self.totalCommission
        showLoading()
        managerApi
            .getConversionSum(accountNo: selectedDeposit!.accountNo!, accountCurrencyID: selectedDeposit!.currencyID!, directionTypeID: direction, moneySystemID: pickedSystemModel!.systemId!, transferSum: transferSum, transferCurrencyID: (currencyModel?.currencyID)!)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (conversion) in
                    guard let `self` = self else { return }
                    self.conversionSum = direction == 0 ? conversion.debetSum : conversion.creditSum
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    // check for the Transfer
    func checkIfTransferExist(
        paymentCode: String,
        moneySystemID: Int,
        transferSum: Double,
        transferCurrencyID: Int) {
            showLoading()
            managerApi
                .findMoneyTransfer(paymentCode: paymentCode,
                                   moneySystemID: moneySystemID,
                                   transferSum: transferSum,
                                   transferCurrencyID: transferCurrencyID)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self] transferFound in
                        guard let `self` = self else { return }
                        
                        self.transferSearchResultHolder.isHidden = false
                        self.nameOfSenderLable.text = transferFound.senderCustomName
                        self.countryOfSenderLable.text = self.countries?.first{ $0.value == transferFound.senderCountryId }?.text
                        self.contragentCountryID = transferFound.senderCountryId
                        self.senderNameCustomer = transferFound.senderCustomName
                        self.senderCountryName = self.countries?.first{ $0.value == transferFound.senderCountryId }?.text
                        self.totalCommission = transferFound.commission
                        self.getConversionSum()
                        self.hideLoading()
                    },
                    onError: {[weak self] error in
                        self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                        self?.hideLoading()
                    })
                .disposed(by: disposeBag)
        }
    
    func getCity(countryCode: Int) {
        showLoading()
        managerApi
            .getXFincaCities(countryID: countryCode)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] cities in
                    guard let `self` = self else { return }
                    self.arrayOfCities = cities
                    let citiesCopy = self.arrayOfCities.map({$0.copy() as! XFincaCityDictionary})
                    self.arrayOfCities = citiesCopy
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        if textField.isEqual(phoneNumberLabel) {
            maxLength = phoneNumberLength
        }
        return newLength <= maxLength
    }
    
    func getPhoneNumberMask(countryID: Int) {
        showLoading()
        managerApi
            .getPhoneMask(countryID: countryID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] phoneMask in
                    guard let `self` = self else { return }
                    self.phoneNumberLabel.placeholder = phoneMask.phoneCode
                    self.phoneNumberLength = phoneMask.phoneCode!.count + phoneMask.lenghtPhoneNumber!
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    func payConfirm(operationID: Int){
        showLoading()
        managerApi
            .postMoneyTransferConfirm(operationModel: OperationModel(operationID: operationID))
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] conversion in
                    guard let `self` = self else { return }
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                    self.view.window?.rootViewController = vc
                    let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
                    let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
                    let navigationController = UINavigationController(rootViewController: destinationController)
                    vc.pushFrontViewController(navigationController, animated: true)
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    func postConfirmPay(operationModel: CreateMoneyTransfer){
        showLoading()
        managerApi
            .postMoneyTransferCreate(createMoneyTransfer: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    self.operationID = result.operationID ?? 0
                    if self.directionRow == 1 {
                        self.payConfirm(operationID: result.operationID!)
                    } else {
                        self.performSegue(withIdentifier: "toSubmitXFincaSegue", sender: self)
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
}


//💭💭💭💭💭💭💭💭💭💭💭api/Currencies💭💭💭💭💭💭💭💭💭💭💭
//🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥 Optional("Китайский юань Жэньминьби") ========  Optional(156)
//🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥 Optional("Евро") ========  Optional(978)
//🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥 Optional("Сом") ========  Optional(417)
//🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥 Optional("Казахский тенге") ========  Optional(398)
//🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥 Optional("Российский рубль") ========  Optional(643)
//🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥 Optional("Новая Турецкая Лира") ========  Optional(949)
//🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥🟥 Optional("Доллар США") ========  Optional(840)


//🏢🏢🏢🏢🏢🏢🏢🏢🏢🏢/api/MoneyTransfer/MoneyTransfers/GetXFincaCitiesZk + countryID🏢🏢🏢🏢🏢🏢🏢🏢🏢🏢
//🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩Optional(810)
//🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩Optional(840)
//🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩🟩Optional(978)


//USERDEFAULTS
//==========================================Optional(156)---------------Optional("Китайский юань Жэньминьби")
//==========================================Optional(978)---------------Optional("Евро")
//==========================================Optional(417)---------------Optional("Сом")
//==========================================Optional(398)---------------Optional("Казахский тенге")
//==========================================Optional(643)---------------Optional("Российский рубль")
//==========================================Optional(949)---------------Optional("Новая Турецкая Лира")
//==========================================Optional(840)---------------Optional("Доллар США")
