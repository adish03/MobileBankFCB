//
//  TemplateTransactionViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/20/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper

class TemplateTransactionViewController: BaseViewController {
    
    var clearingGrossOperationModel: ClearingGrossOperationModel!
    var internalOperationModel: InternalOperationModel!
    
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    var state = "operation_complete"
    var stateImage = "succes"
    
    @IBOutlet weak var viewTransaction: UIView!
    @IBOutlet weak var viewCliringGross: UIView!
    
    @IBOutlet weak var paymentDateStackView: UIStackView!
    @IBOutlet weak var pNumberStackView: UIStackView!
    @IBOutlet weak var paymentTypeIDStackView: UIStackView!
    @IBOutlet weak var senderFullNameStackView: UIStackView!
    @IBOutlet weak var receiverFullNameStackView: UIStackView!
    @IBOutlet weak var receiverBIKStackView: UIStackView!
    @IBOutlet weak var senderAccountNoStackView: UIStackView!
    @IBOutlet weak var receiverAccountNoStackView: UIStackView!
    @IBOutlet weak var paymentCommentStackView: UIStackView!
    @IBOutlet weak var paymentCodeStackView: UIStackView!
    @IBOutlet weak var transferSummStackView: UIStackView!
   
    @IBOutlet weak var operationTypeLabel: UILabel!
    
    @IBOutlet weak var paymentDate: UILabel!
    @IBOutlet weak var pNumber: UILabel!
    @IBOutlet weak var paymentTypeID: UILabel!
    @IBOutlet weak var senderFullName: UILabel!
    @IBOutlet weak var receiverFullName: UILabel!
    @IBOutlet weak var receiverBIK: UILabel!
    @IBOutlet weak var senderAccountNo: UILabel!
    @IBOutlet weak var receiverAccountNo: UILabel!
    @IBOutlet weak var paymentComment: UILabel!
    @IBOutlet weak var paymentCode: UILabel!
    @IBOutlet weak var transferSumm: UILabel!
    
    @IBOutlet weak var paymentType: UILabel!
    @IBOutlet weak var DtAccountNo: UILabel!
    @IBOutlet weak var CtAccountNo: UILabel!
    @IBOutlet weak var ScheduleDate: UILabel!
    @IBOutlet weak var Sum: UILabel!
    @IBOutlet weak var Comment: UILabel!
    
    @IBOutlet weak var paymentDescriptionLabel: UILabel!
    @IBOutlet weak var bikDescriptionLabel: UILabel!
    
    var typeOperation = ""
    var operationTypeID = 0
    var currencyCurrent = ""
    var currencyCurrentID = 0
    var selectedDeposit: Deposit!
    var selectedDepositTo : Deposit?
    var confirmType = 0
    var clearingGrossPaymenModel: ClearingGrossPaymenModel!
    var delegate: NoNeedLoadDataDelegate?
    
    var paymentsCodes: [PaymentCode]!
    var bikCodes: [BikCode]!
    var bikCode: BikCode!
    
    //operation detail
    var isNeedOperationDetail = false
    var isNeedOperationDetailByPayment = false
    var operationID = 0
    var navigationTitle = ""
    var paymentID = 0
    
    @IBOutlet weak var viewTemplate: UIView!
    @IBOutlet weak var viewOperationState: UIView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var viewButton: UIView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.isNeedData(isNeed: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        statusLabel.text = localizedText(key: state )
        statusImage.image = UIImage(named: stateImage )
        barButtonCustomize()
        
        switch operationTypeID {
        
        case  InternetBankingOperationType.InternalOperation.rawValue:
            self.operationTypeLabel.text = localizedText(key:"Internal_operation")
            
        case  InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue:
            self.operationTypeLabel.text = localizedText(key:"Resources_Internal_Customer_Transaction")
            
        case  InternetBankingOperationType.CliringTransfer.rawValue:
            self.operationTypeLabel.text = localizedText(key:"clearing_payment")
            
        case  InternetBankingOperationType.GrossTransfer.rawValue:
            self.operationTypeLabel.text = localizedText(key:"gross_payment")
            
        default:
             self.operationTypeLabel.text = ""
        }
        
        
        if let bikCodes = UserDefaultsHelper.bikCodes {
            self.bikCodes = bikCodes
        }
        if let paymentsCodes = UserDefaultsHelper.paymentsCodes {
            self.paymentsCodes = paymentsCodes
        }
        
        if isNeedOperationDetail{
            viewTemplate.isHidden = true
            viewButton.isHidden = true
            viewOperationState.isHidden = false
            let directions: [UISwipeGestureRecognizer.Direction] = [.up, .down, .right, .left]
            for direction in directions {
                let gesture = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(gesture:)))
                gesture.direction = direction
                view?.addGestureRecognizer(gesture)
            }
            
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue ||
                operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                viewTransaction.isHidden = false
                viewCliringGross.isHidden = true
                if isNeedOperationDetailByPayment{
                    getInfoOperationClearingGrossbyPaymentID(paymentID: paymentID)
                }else{
                    getInfoOperationTransactions(operationID: operationID)
                }
            }else{
                viewTransaction.isHidden = true
                viewCliringGross.isHidden = false
                if isNeedOperationDetailByPayment{
                    getInfoOperationClearingGrossbyPaymentID(paymentID: paymentID)
                }else{
                    getInfoOperation(operationID: operationID)
                }
            }
        }else{
            viewTemplate.isHidden = false
            viewButton.isHidden = false
            viewOperationState.isHidden = true
            if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue ||
                operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                viewTransaction.isHidden = false
                viewCliringGross.isHidden = true
                
                paymentType.text = ValueHelper.shared.operationTypeDefine(operationTypeId: operationTypeID)
                DtAccountNo.text = internalOperationModel.dtAccountNo
                CtAccountNo.text = internalOperationModel.ctAccountNo
                if internalOperationModel.scheduleDate != nil{
                    ScheduleDate.text = internalOperationModel.scheduleDate
                }else{
                    ScheduleDate.text = self.dateCurrentOpendepostit()
                }
                Sum.text = "\(String(format:"%.2f", internalOperationModel.sum ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: internalOperationModel.currencyID ?? 0))"
                Comment.text = internalOperationModel.comment
                if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                    navigationItem.title = localizedText(key:"Internal_operation")
                }else{
                    navigationItem.title = localizedText(key:"Resources_Internal_Customer_Transaction")
                }
                if operationID != 0{
                    getInfoOperationTransactions(operationID: operationID)
                }
            }else{
                viewTransaction.isHidden = true
                viewCliringGross.isHidden = false
                
                paymentTypeID.text = ValueHelper.shared.operationTypeDefine(operationTypeId: operationTypeID)
                senderFullName.text = SessionManager.shared.user?.userdisplayname//clearingGrossOperationModel.senderFullName
                paymentDate.text = ScheduleHelper.shared.dateFromServer(stringDate: clearingGrossOperationModel.paymentDate ?? "")
                pNumber.text = clearingGrossOperationModel.pNumber
                receiverFullName.text = clearingGrossOperationModel.receiverFullName
                receiverBIK.text = clearingGrossOperationModel.receiverBIK
                senderAccountNo.text = clearingGrossOperationModel.senderAccountNo
                receiverAccountNo.text = clearingGrossOperationModel.receiverAccountNo
                paymentComment.text = clearingGrossOperationModel.paymentComment
                paymentCode.text = clearingGrossOperationModel.paymentCode
                transferSumm.text = "\(String(format:"%.2f",  clearingGrossOperationModel.transferSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: currencyCurrentID))"
                
                for code in self.bikCodes{
                    if code.code == self.clearingGrossOperationModel.receiverBIK{
                        self.bikDescriptionLabel.text = code.name
                    }
                }
                for code in self.paymentsCodes{
                    if code.code == self.clearingGrossOperationModel.paymentCode{
                        self.paymentDescriptionLabel.text = code.description
                    }
                }
                if operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                    navigationItem.title = localizedText(key:"clearing_payment")
                }else{
                    navigationItem.title = localizedText(key:"gross_payment")
                }
                
                if operationID != 0{
                    getInfoOperation(operationID: operationID)
                }
            }
        }
    }
    //обработка swipe 
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
            
        case UISwipeGestureRecognizer.Direction.right:
            self.navigationController?.popViewController(animated: true)
        default:
            print("")
        }
    }
    // перход к Шаблонам
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toTransactionsSegue"{
            let vc = segue.destination as! TransactionViewController
            vc.internalTemplateFromStory = internalOperationModel
            vc.isRepeatPay = true
            vc.isRepeatFromStory = true
            vc.navigationTitle = localizedText(key: "create_operation_request")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        if segue.identifier == "toClearingGrossSegue"{
            let vc = segue.destination as! ClearingGrossViewController
            if isNeedOperationDetailByPayment{
                clearingGrossOperationModel = ClearingGrossOperationModel(
                    paymentDate: clearingGrossOperationModel.paymentDate,
                    pNumber: clearingGrossOperationModel.pNumber,
                    paymentTypeID: clearingGrossOperationModel.paymentTypeID,
                    senderFullName: clearingGrossOperationModel.senderFullName,
                    receiverFullName: clearingGrossOperationModel.receiverFullName,
                    receiverBIK: clearingGrossOperationModel.receiverBIK,
                    senderAccountNo: clearingGrossOperationModel.senderAccountNo,
                    receiverAccountNo: clearingGrossOperationModel.receiverAccountNo,
                    paymentComment: clearingGrossOperationModel.paymentComment,
                    paymentCode: clearingGrossOperationModel.paymentCode,
                    transferSumm: clearingGrossOperationModel.transferSumm,
                    operationType: self.operationTypeID,
                    operationID: clearingGrossOperationModel.operationID,
                    isSchedule: clearingGrossOperationModel.isSchedule,
                    isTemplate: clearingGrossOperationModel.isTemplate,
                    templateName: clearingGrossOperationModel.templateName,
                    templateDescription: "Клиринг/Гросс",
                    scheduleID: clearingGrossOperationModel.scheduleID,
                    schedule: clearingGrossOperationModel.schedule)
                
                vc.clearingGrossOperationModel = clearingGrossOperationModel
                vc.isRepeatPay = true
                vc.clearingGrossCurrency = self.currencyCurrentID
                vc.navigationTitle = localizedText(key: "create_operation_request")
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            }else{
                clearingGrossOperationModel = ClearingGrossOperationModel(
                    paymentDate: clearingGrossOperationModel.paymentDate,
                    pNumber: clearingGrossOperationModel.pNumber,
                    paymentTypeID: clearingGrossOperationModel.paymentTypeID,
                    senderFullName: clearingGrossOperationModel.senderFullName,
                    receiverFullName: clearingGrossOperationModel.receiverFullName,
                    receiverBIK: clearingGrossOperationModel.receiverBIK,
                    senderAccountNo: clearingGrossOperationModel.senderAccountNo,
                    receiverAccountNo: clearingGrossOperationModel.receiverAccountNo,
                    paymentComment: clearingGrossOperationModel.paymentComment,
                    paymentCode: clearingGrossOperationModel.paymentCode,
                    transferSumm: clearingGrossOperationModel.transferSumm,
                    operationType: self.operationTypeID,
                    operationID: clearingGrossOperationModel.operationID,
                    isSchedule: clearingGrossOperationModel.isSchedule,
                    isTemplate: clearingGrossOperationModel.isTemplate,
                    templateName: clearingGrossOperationModel.templateName,
                    templateDescription: "Клиринг/Гросс",
                    scheduleID: clearingGrossOperationModel.scheduleID,
                    schedule: clearingGrossOperationModel.schedule)
                
                vc.clearingGrossOperationModel = clearingGrossOperationModel
                vc.isRepeatPay = true
                vc.isRepeatFromStory = true
                vc.clearingGrossCurrency = self.currencyCurrentID
                vc.navigationTitle = localizedText(key: "create_operation_request")
                self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            }
        }
    }
    
    // кнопка сохранения шаблона
    @IBAction func saveAsTemplateButton(_ sender: Any) {
        
        if operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
            self.typeOperation = localizedText(key:"Internal_operation")
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            vc.isCreateTemplate = true
            vc.submitModel = self.internalOperationModel
            vc.selectedDeposit = self.selectedDeposit
            vc.operationTypeID = self.internalOperationModel.operationType ?? 0
            vc.navigationTitle = localizedText(key: "create_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
            self.typeOperation = localizedText(key:"Resources_Internal_Customer_Transaction")
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
            vc.isCreateTemplate = true
            vc.submitModel = self.internalOperationModel
            vc.selectedDeposit = self.selectedDeposit
            vc.selectedDepositTo = self.selectedDepositTo
            vc.operationTypeID = self.internalOperationModel.operationType ?? 0
            vc.navigationTitle = localizedText(key: "create_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
            self.typeOperation = localizedText(key:"clearing_payment")
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
            vc.isCreateTemplate = true
            vc.submitModel = self.clearingGrossOperationModel
            vc.selectedDeposit = self.selectedDeposit
            vc.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
            vc.navigationTitle = localizedText(key: "create_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        if operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue{
            self.typeOperation = localizedText(key:"gross_payment")
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
            vc.isCreateTemplate = true
            vc.submitModel = self.clearingGrossOperationModel
            vc.selectedDeposit = self.selectedDeposit
            vc.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
            vc.navigationTitle = localizedText(key: "create_template")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    // кнопка закрытия
    @IBAction func buttonClose(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    // кнопка FAB
    @IBAction func AlertSheetControl(_ sender: Any) {
        
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        let actionRepeatOperation = UIAlertAction(title: self.localizedText(key: "create_operation_request"), style: .default) { (action) in
            
            if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                self.performSegue(withIdentifier: "toTransactionsSegue", sender: self)
            }
            if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                self.performSegue(withIdentifier: "toTransactionsSegue", sender: self)
            }
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                self.performSegue(withIdentifier: "toClearingGrossSegue", sender: self)
                
            }
            if self.operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue{
                self.performSegue(withIdentifier: "toClearingGrossSegue", sender: self)
                
            }
            
        }
        let actionHSaveTemplate = UIAlertAction(title: self.localizedText(key: "save_as_template"), style: .default) { (action) in
            
            if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                self.typeOperation = self.localizedText(key: "internal_bank_transfer")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
                vc.isCreateTemplate = true
                vc.submitModel = self.internalOperationModel
                vc.selectedDeposit = self.selectedDeposit
                vc.operationTypeID = self.internalOperationModel.operationType ?? 0
                vc.navigationTitle = self.localizedText(key: "create_template")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                self.typeOperation = self.localizedText(key: "Resources_Internal_Customer_Transaction")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
                vc.isCreateTemplate = true
                vc.submitModel = self.internalOperationModel
                vc.selectedDeposit = self.selectedDeposit
                vc.selectedDepositTo = self.selectedDepositTo
                vc.operationTypeID = self.internalOperationModel.operationType ?? 0
                vc.navigationTitle = self.localizedText(key: "create_template")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                self.typeOperation = self.localizedText(key: "Resources_ClearingGross_Common_Clearing")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
                vc.isCreateTemplate = true
                
                vc.clearingGrossCurrency = self.currencyCurrentID
                
                vc.submitModel = self.clearingGrossOperationModel
                vc.selectedDeposit = self.selectedDeposit
                vc.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
                vc.navigationTitle = self.localizedText(key: "create_template")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if self.operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue{
                self.typeOperation = self.localizedText(key: "Resources_ClearingGross_Common_Gross")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
                vc.isCreateTemplate = true
                vc.submitModel = self.clearingGrossOperationModel
                
                vc.clearingGrossCurrency = self.currencyCurrentID
                
                vc.selectedDeposit = self.selectedDeposit
                vc.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
                vc.navigationTitle = self.localizedText(key: "create_template")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        let sendToBank = UIAlertAction(title: self.localizedText(key: "send_to_bank"), style: .default) { (action) in

            if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                self.typeOperation = self.localizedText(key:"Internal_operation")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SubmitTransactionViewController") as! SubmitTransactionViewController
                vc.clearingGrossOperationModel = self.clearingGrossOperationModel
                vc.submitModelInternalOperation = self.internalOperationModel
                vc.operationTypeID = self.internalOperationModel.operationType ?? 0
                vc.operationID = self.operationID
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                self.typeOperation = self.localizedText(key:"Resources_Internal_Customer_Transaction")

                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SubmitTransactionViewController") as! SubmitTransactionViewController
                vc.clearingGrossOperationModel = self.clearingGrossOperationModel
                vc.submitModelInternalOperation = self.internalOperationModel
                vc.operationTypeID = self.internalOperationModel.operationType ?? 0
                vc.operationID = self.operationID
                self.navigationController?.pushViewController(vc, animated: true)
            }
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                self.typeOperation =  self.localizedText(key:"clearing_payment")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SubmitTransactionViewController") as! SubmitTransactionViewController
                vc.clearingGrossOperationModel = self.clearingGrossOperationModel
//                vc.submitModelInternalOperation = self.internalOperationModel
                vc.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
                vc.operationID = self.operationID
                self.navigationController?.pushViewController(vc, animated: true)
               
            }
            if self.operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue{
                self.typeOperation = self.localizedText(key:"gross_payment")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "SubmitTransactionViewController") as! SubmitTransactionViewController
                vc.clearingGrossOperationModel = self.clearingGrossOperationModel
//                vc.submitModelInternalOperation = self.internalOperationModel
                vc.operationTypeID = self.clearingGrossOperationModel.operationType ?? 0
                vc.operationID = self.operationID
                self.navigationController?.pushViewController(vc, animated: true)
            }

        }
        
        let changeData = UIAlertAction(title: self.localizedText(key: "edit"), style: .default) { (action) in
            
            if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue || self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                self.typeOperation = self.localizedText(key:"Internal_operation")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "TransactionViewController") as! TransactionViewController
                vc.internalTemplateFromStory = self.internalOperationModel
                vc.isRepeatPay = true
                vc.isRepeatFromStory = true
                vc.isChangeData = true
                vc.navigationTitle = self.localizedText(key: "create_operation_request")
                self.navigationController?.pushViewController(vc, animated: true)
            }
           
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue || self.operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue{
                self.typeOperation =  self.localizedText(key:"clearing_payment")
                let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "ClearingGrossViewController") as! ClearingGrossViewController
                self.clearingGrossOperationModel = ClearingGrossOperationModel(
                    paymentDate: self.clearingGrossOperationModel.paymentDate,
                    pNumber: self.clearingGrossOperationModel.pNumber,
                    paymentTypeID: self.clearingGrossOperationModel.paymentTypeID,
                    senderFullName: self.clearingGrossOperationModel.senderFullName,
                    receiverFullName: self.clearingGrossOperationModel.receiverFullName,
                    receiverBIK: self.clearingGrossOperationModel.receiverBIK,
                    senderAccountNo: self.clearingGrossOperationModel.senderAccountNo,
                    receiverAccountNo: self.clearingGrossOperationModel.receiverAccountNo,
                    paymentComment: self.clearingGrossOperationModel.paymentComment,
                    paymentCode: self.clearingGrossOperationModel.paymentCode,
                    transferSumm: self.clearingGrossOperationModel.transferSumm,
                    operationType: self.self.operationTypeID,
                    operationID: self.clearingGrossOperationModel.operationID,
                    isSchedule: self.clearingGrossOperationModel.isSchedule,
                    isTemplate: self.clearingGrossOperationModel.isTemplate,
                    templateName: self.clearingGrossOperationModel.templateName,
                    templateDescription: "Клиринг/Гросс",
                    scheduleID: self.clearingGrossOperationModel.scheduleID,
                    schedule: self.clearingGrossOperationModel.schedule)
                
                vc.clearingGrossOperationModel = self.clearingGrossOperationModel
                vc.isRepeatPay = true
                vc.isRepeatFromStory = true
                vc.clearingGrossCurrency = self.currencyCurrentID
                vc.isChangeData = true
                vc.navigationTitle = self.localizedText(key: "create_operation_request")
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        let actionDownload = UIAlertAction(title: self.localizedText(key: "receipt"), style: .default) { (action) in
            if self.operationTypeID == InternetBankingOperationType.InternalOperation.rawValue{
                ApiHelper.shared.getInfoOperationFile(operationID: self.internalOperationModel.operationID ?? 0,
                                                      code: 00000,
                                                      operationTypeID: self.internalOperationModel.operationType ?? 0, vc: self)
            }
            if self.operationTypeID == InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue{
                ApiHelper.shared.getInfoOperationFile(operationID: self.internalOperationModel.operationID ?? 0,
                                                      code: 00000,
                                                      operationTypeID: self.internalOperationModel.operationType ?? 0, vc: self)
            }
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                if self.paymentID != 0{
                    ApiHelper.shared.getInfoOperationFileClearingGross(paymentID: self.paymentID, vc: self)
                }else{
                ApiHelper.shared.getInfoOperationFile(operationID: self.clearingGrossOperationModel.operationID ?? 0,
                                                      code: 00000,
                                                      operationTypeID: self.clearingGrossOperationModel.operationType ?? 0, vc: self)
                }
            }
            if self.operationTypeID == InternetBankingOperationType.GrossTransfer.rawValue{
                ApiHelper.shared.getInfoOperationFile(operationID: self.clearingGrossOperationModel.operationID ?? 0,
                                                      code: 00000,
                                                      operationTypeID: self.clearingGrossOperationModel.operationType ?? 0, vc: self)
            }
            
        }
        
        let actionRemove = UIAlertAction(title: self.localizedText(key: "delete"), style: .default) { (action) in
            let alert = UIAlertController(title:  self.localizedText(key: "delete_operation"), message: "", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                ApiHelper.shared.deleteTemplate(operationID: "\(self.operationID)")
                self.navigationController?.popViewController(animated: true)
            })
           let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)
            alert.addAction(actionOk)
            alert.addAction(actionCancel)
            self.present(alert, animated: true, completion: nil)
        }
        
        let actionCancel  = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel, handler: nil)
        
        // отображение действий в зависимости от confirmType
        switch confirmType {
        case ConfirmType.Save.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }else{
                if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsEditAllowed"){
                alertSheet.addAction(changeData)
            }
            
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsRemoveAllowed"){
                    alertSheet.addAction(actionRemove)                }
            }else{
                if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsRemoveAllowed"){
                    alertSheet.addAction(actionRemove)                }
            }
           
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Confirm.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            alertSheet.addAction(actionDownload)
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.InConfirm.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }else{
                if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Error.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }else{
                if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Verified.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if self.operationTypeID == InternetBankingOperationType.CliringTransfer.rawValue{
                if AllowHelper.shared.clearingGrossAllowOpertaion(nameOperation: "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }else{
                if AllowHelper.shared.operationAllowInternalOpertaion(nameOperation: "Operation.InternalOperationSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }
            alertSheet.addAction(actionCancel)
            
        default:
//            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
//                alertSheet.addAction(actionRepeatOperation)
//            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            alertSheet.addAction(actionDownload)
            alertSheet.addAction(actionCancel)
        }
        
        present(alertSheet, animated: true, completion: nil)
    }
    
    
}
extension TemplateTransactionViewController{
    //    получение данных операции
    func  getInfoOperation(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoCliringGross(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.clearingGrossOperationModel = operation
                    
                    self.senderFullName.text = SessionManager.shared.user?.userdisplayname//clearingGrossOperationModel.senderFullName
                    self.paymentDate.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: self.clearingGrossOperationModel.paymentDate ?? "")
                    self.pNumber.text = self.clearingGrossOperationModel.pNumber
                    self.paymentTypeID.text = ValueHelper.shared.operationTypeDefine(operationTypeId: self.clearingGrossOperationModel.operationType ?? 0)
                    self.receiverFullName.text = self.clearingGrossOperationModel.receiverFullName
                    self.receiverBIK.text = self.clearingGrossOperationModel.receiverBIK
                    self.senderAccountNo.text = self.clearingGrossOperationModel.senderAccountNo
                    self.receiverAccountNo.text = self.clearingGrossOperationModel.receiverAccountNo
                    self.paymentComment.text = self.clearingGrossOperationModel.paymentComment
                    self.paymentCode.text = self.clearingGrossOperationModel.paymentCode
                    self.transferSumm.text = "\(String(format:"%.2f",  self.clearingGrossOperationModel.transferSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID:self.currencyCurrentID)) "
                    (self.stateLabel.text, self.stateLabel.textColor) = ValueHelper.shared.confirmTypeDefine(confirmType: self.confirmType)
//                    self.confirmType = self.clearingGrossOperationModel.confirmType ?? 0
                    for code in self.bikCodes{
                        if code.code == self.clearingGrossOperationModel.receiverBIK{
                            self.bikDescriptionLabel.text = code.name
                        }
                    }
                    for code in self.paymentsCodes{
                        if code.code == self.clearingGrossOperationModel.paymentCode{
                            self.paymentDescriptionLabel.text = code.description
                        }
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    // получение данных операции перевода
    func  getInfoOperationTransactions(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationTransactions(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.internalOperationModel = operation
                  
                    self.paymentType.text = ValueHelper.shared.operationTypeDefine(operationTypeId: self.operationTypeID)
                    self.DtAccountNo.text = self.internalOperationModel.dtAccountNo
                    self.CtAccountNo.text = self.internalOperationModel.ctAccountNo
                    self.ScheduleDate.text = self.dateFormater(date: self.internalOperationModel.createDate ?? "")
                    self.Sum.text = "\(String(format:"%.2f", self.internalOperationModel.sum ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.internalOperationModel.currencyID ?? 0))"
                    self.Comment.text = self.internalOperationModel.comment
                    (self.stateLabel.text, self.stateLabel.textColor) = ValueHelper.shared.confirmTypeDefine(confirmType: self.internalOperationModel.confirmType ?? 0)
                    self.confirmType = self.internalOperationModel.confirmType ?? 0
//                    self.stateLabel.textColor = self.confirmTypeState(string: self.stateLabel.text ?? "")
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    // получение данных операции перевода
    func  getInfoOperationClearingGrossbyPaymentID(paymentID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationClearingGrossbyPaymentID(paymentID: paymentID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.clearingGrossPaymenModel = operation
                    self.clearingGrossOperationModel = ClearingGrossOperationModel(
                        paymentDate: self.clearingGrossPaymenModel.paymentDate,
                        pNumber: self.clearingGrossPaymenModel.pNumber,
                        paymentTypeID: self.clearingGrossPaymenModel.paymentTypeID,
                        senderFullName: self.clearingGrossPaymenModel.senderFullName,
                        receiverFullName: self.clearingGrossPaymenModel.receiverFullName,
                        receiverBIK: self.clearingGrossPaymenModel.receiverBIK,
                        senderAccountNo: self.clearingGrossPaymenModel.senderAccountNo,
                        receiverAccountNo: self.clearingGrossPaymenModel.receiverAccountNo,
                        paymentComment: self.clearingGrossPaymenModel.paymentComment,
                        paymentCode: self.clearingGrossPaymenModel.paymentCode,
                        transferSumm: self.clearingGrossPaymenModel.transferSumm,
                        operationType: self.operationTypeID,
                        operationID: 0,
                        isSchedule: false,
                        isTemplate: false,
                        templateName: nil,
                        templateDescription: nil,
                        scheduleID: nil,
                        schedule: nil)
                    
                    self.senderFullName.text = self.clearingGrossPaymenModel.senderFullName
                    self.paymentDate.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: self.clearingGrossPaymenModel.paymentDate ?? "")
                    self.pNumber.text = self.clearingGrossPaymenModel.pNumber
                    self.paymentTypeID.text = ValueHelper.shared.operationTypeDefine(operationTypeId: self.operationTypeID)
                    self.receiverFullName.text = self.clearingGrossPaymenModel.receiverFullName
                    self.receiverBIK.text = self.clearingGrossPaymenModel.receiverBIK
                    self.senderAccountNo.text = self.clearingGrossPaymenModel.senderAccountNo
                    self.receiverAccountNo.text = self.clearingGrossPaymenModel.receiverAccountNo
                    self.paymentComment.text = self.clearingGrossPaymenModel.paymentComment
                    self.paymentCode.text = self.clearingGrossPaymenModel.paymentCode
                    self.transferSumm.text = "\(String(format:"%.2f",  self.clearingGrossPaymenModel.transferSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID:self.currencyCurrentID)) "
                    (self.stateLabel.text, self.stateLabel.textColor) = ValueHelper.shared.confirmTypeDefine(confirmType: self.confirmType)
                    for code in self.bikCodes{
                        if code.code == self.clearingGrossPaymenModel.receiverBIK{
                            self.bikDescriptionLabel.text = code.name
                        }
                    }
                    for code in self.paymentsCodes{
                        if code.code == self.clearingGrossPaymenModel.paymentCode{
                            self.paymentDescriptionLabel.text = code.description
                        }
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    
    //    кастомизация кнопки
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        var image = ""
        if isNeedOperationDetail{
            image = "ArrowBack"
            button.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        }else{
            image = "Close"
            button.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        }
        button.setImage(UIImage(named: image), for: .normal)
        
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 50)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    //    кнопка закрытия
    @objc func closeButtonPressed() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    //    кнопка назад
    @objc func backButtonPressed() {
        navigationController?.popViewController(animated: true)
        delegate?.isNeedData(isNeed: false)
    }
    
    
}


