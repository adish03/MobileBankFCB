//
//  TemplateSwiftTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/8/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

// создание модели ячейки для таблицы коммиссий
class TemplateSwiftTableViewCell: UITableViewCell {

    @IBOutlet weak var descriptionCommissionLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var commissionCurrencyLabel: UILabel!
 
    override func awakeFromNib() {
        super.awakeFromNib()
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
