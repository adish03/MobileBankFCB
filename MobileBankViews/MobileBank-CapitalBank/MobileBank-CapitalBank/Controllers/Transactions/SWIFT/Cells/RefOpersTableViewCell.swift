//
//  RefOpersTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/2/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

// создание модели ячейки для таблицы данных операций
class RefOpersTableViewCell: UITableViewCell {

    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    // кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.accessoryType = selected ? .checkmark : .none
        
    }
}
