//
//  DetailSwiftTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/16/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

class DetailSwiftTableViewCell: UITableViewCell {

    
    @IBOutlet weak var numberPPLabel: UILabel!
    @IBOutlet weak var datePPLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var dateValLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var accountNoLabel: UILabel!
    @IBOutlet weak var transferSummLabel: UILabel!
   
    @IBOutlet weak var FAButton: UIButton!
    
    @IBOutlet weak var moreButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }

}
