//
//  CodeVOTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/5/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

// создание модели ячейки для таблицы кодов валютных операций
class CodeVOTableViewCell: UITableViewCell {

    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
        self.accessoryType = selected ? .checkmark : .none
        
       
    }

}
