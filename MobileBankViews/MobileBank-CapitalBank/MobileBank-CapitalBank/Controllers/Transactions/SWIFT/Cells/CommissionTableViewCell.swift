//
//  CommissionTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/5/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

// класс для кастомизации ячейки таблицы
class CommissionTableViewCell: UITableViewCell {
    
    @IBOutlet weak var descriptionCommissionLabel: UILabel!
    @IBOutlet weak var commissionLabel: UILabel!
    @IBOutlet weak var commissionCurrencyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
