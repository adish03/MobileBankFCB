//
//  PDFViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/26/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import WebKit
import PDFKit
import PDFReader

class PDFViewControllerNew: BaseViewController {
    @IBOutlet weak var pdfView: PDFView!
    
    @IBOutlet weak var webView: WKWebView!
   
    var operationID = 0
    var operationTypeID = 0

    override func viewDidLoad() {
        super.viewDidLoad()
      
        getInfoOperationFile(operationID: operationID, code: 0, operationTypeID: operationTypeID)
       
      
       
    }
    func  getInfoOperationFile(operationID: Int, code: Int, operationTypeID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationFile(operationID: operationID, code: code, operationTypeID: operationTypeID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {filePDFString in
                    if  let stringPDF = filePDFString.reportDocuments?[0].fileBody{
                    let name = filePDFString.reportDocuments?[0].fileName
                        if let decodeData = Data(base64Encoded: stringPDF, options: .ignoreUnknownCharacters) {
                            //                            self.webView.load(decodeData, mimeType: "application/pdf", characterEncodingName: "utf-8", baseURL: NSURL(fileURLWithPath: "") as URL)
                            //
                            
                            let actionButtonImage = UIImage(named: "cupcakeActionButtonImage")
                            let document = PDFDocument(fileData: decodeData, fileName: name ?? "")!
                            let readerController = PDFViewController.createNew(with: document, title: name, actionButtonImage: actionButtonImage, actionStyle: .activitySheet)
                            self.navigationController?.pushViewController(readerController, animated: true)
                            
                            
                            
                            
                            
                        }
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    public enum ActionStyle {
        /// Brings up a print modal allowing user to print current PDF
        case print
        
        /// Brings up an activity sheet to share or open PDF in another app
        case activitySheet
        
        /// Performs a custom action
        case customAction(() -> ())
    }
    
}
