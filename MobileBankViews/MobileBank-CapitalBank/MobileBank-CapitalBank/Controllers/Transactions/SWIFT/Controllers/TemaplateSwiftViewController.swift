//
//  TemaplateSwiftViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/8/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper


class TemaplateSwiftViewController: BaseViewController {
    
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    var state = "operation_complete"
    var stateImage = "succes"
    
    @IBOutlet weak var operationTypeLabel: UILabel!
    @IBOutlet weak var SenderFullNameLabel: UILabel!
    @IBOutlet weak var SenderAccountNoLabel: UILabel!
    @IBOutlet weak var PNumberLabel: UILabel!
    @IBOutlet weak var TransferSummLabel: UILabel!
    @IBOutlet weak var PaymentCodeLabel: UILabel!
    @IBOutlet weak var PaymentCodeDescriptionLabel: UILabel!
    @IBOutlet weak var TarifLabel: UILabel!
    @IBOutlet weak var PaymentCommentLabel: UILabel!
    @IBOutlet weak var VOcodeLabel: UILabel!
    @IBOutlet weak var VOcodeDescriptionLabel: UILabel!
    @IBOutlet weak var ReceiverFullNameLabel: UILabel!
    @IBOutlet weak var ReceiverAccountNoLabel: UILabel!
    @IBOutlet weak var IntermediaryBankCountryLabel: UILabel!
    @IBOutlet weak var IntermediaryBankNameLabel: UILabel!
//    @IBOutlet weak var AccountNoIntermediaryBankLabel: UILabel!
    @IBOutlet weak var ReceiverBIKIntermediaryBankLabel: UILabel!
    @IBOutlet weak var ReceiverBIKPayThruLabel: UILabel!
    
    @IBOutlet weak var PayThruCountryLabel: UILabel!
    @IBOutlet weak var PayThruCountryNameLabel: UILabel!
    @IBOutlet weak var AccountNoPayThruBankLabel: UILabel!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var summCommissionSender: UILabel!
    @IBOutlet weak var summCommissionSenderCurrency: UILabel!
    @IBOutlet weak var summCommisionIntermediary: UILabel!
    @IBOutlet weak var summCommisionIntermediaryCurrency: UILabel!
    @IBOutlet weak var summCommissionReceiver: UILabel!
    @IBOutlet weak var summCommissionReceiverCurrency: UILabel!
    @IBOutlet weak var summCommissionSenderStackView: UIStackView!
    @IBOutlet weak var summCommisionIntermediaryStackView: UIStackView!
    @IBOutlet weak var summCommissionReceiverStackView: UIStackView!
    @IBOutlet weak var voCodeStackView: UIStackView!
    @IBOutlet weak var dateVal: UILabel!
    @IBOutlet weak var datePP: UILabel!
    
    @IBOutlet weak var receiverAddress: UILabel!
    @IBOutlet weak var receiverCountryCode: UILabel!
    
    var swiftTransferModel: SwiftTransferModel!
    var selectedDeposit: Deposit!
    
    var typeOperation = ""
    var commisions = [SwiftCommissionModel]()
    var currencies = [Currency]()
    var paymentCodeDescription = ""
    var voDescription = ""
    var operationTypeID = 0
    var confirmType = 0
    var voCodes: [ReferenceItemModel]!
    var voCode: ReferenceItemModel!
    var voCodeID: String!
    var refOpers: [ReferenceItemModel]!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewIntermediaryBank: UIView!
    
    @IBOutlet weak var viewTemplate: UIView!
    @IBOutlet weak var viewOperationState: UIView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var viewButton: UIView!
    
    //operation detail
    var isNeedOperationDetail = false
    var isNeedOperationDetailByPayment = false
    var operationID = 0
    var navigationTitle = ""
    var expenseTypes: [expenseTypeModel]!
    var transferID = 0
    var delegate: NoNeedLoadDataDelegate?
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.isNeedData(isNeed: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        statusLabel.text = localizedText(key: state )
        statusImage.image = UIImage(named: stateImage )
        barButtonCustomize()
        
        if let expenseTypes = UserDefaultsHelper.expenseTypes {
            self.expenseTypes = expenseTypes
        }
        if let currencs =  UserDefaultsHelper.currencies{
            currencies = currencs
        }
        if let refOpers = UserDefaultsHelper.refOpers {
            self.refOpers = refOpers
        }
        if let voCodes = UserDefaultsHelper.voCodes {
            self.voCodes = voCodes
        }
        
        if isNeedOperationDetail{
            viewTemplate.isHidden = true
            viewButton.isHidden = true
            viewOperationState.isHidden = false
            if !isNeedOperationDetailByPayment{
            
                getInfoOperationTransactions(operationID: operationID)

            }else{
                self.commissionHelper(commissions: commisions,
                                      currencyID: swiftTransferModel.currencyID ?? 0,
                                      sum: swiftTransferModel?.transferSum ?? 0.0,
                                      summCommissionSender: summCommissionSender,
                                      summCommissionSenderCurrency: summCommissionSenderCurrency,
                                      summCommissionSenderStackView: summCommissionSenderStackView,
                                      summCommisionIntermediary: summCommisionIntermediary,
                                      summCommisionIntermediaryCurrency: summCommisionIntermediaryCurrency,
                                      summCommisionIntermediaryStackView: summCommisionIntermediaryStackView,
                                      summCommissionReceiver: summCommissionReceiver,
                                      summCommissionReceiverCurrency: summCommissionReceiverCurrency,
                                      summCommissionReceiverStackView: summCommissionReceiverStackView )
                view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
                if swiftTransferModel.intermediaryBankCountry == "" || swiftTransferModel.intermediaryBankCountry == nil {
                    viewIntermediaryBank.isHidden = true
                }else{
                    viewIntermediaryBank.isHidden = false
                }

                if swiftTransferModel.operationType == InternetBankingOperationType.SwiftTransfer.rawValue{
                    typeOperation = localizedText(key: "swift_transfer")
                }
                if swiftTransferModel.codeVO == "" || swiftTransferModel.codeVO == nil {
                    voCodeStackView.isHidden = true
                }else{
                    voCodeStackView.isHidden = false
                }
                self.operationTypeLabel.text = localizedText(key: "swift_transfer")
                let transferSumm = "\(String(format:"%.2f",  swiftTransferModel.transferSum ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: swiftTransferModel.currencyID ?? 0))"
                SenderFullNameLabel.text = swiftTransferModel.senderFullName
                SenderAccountNoLabel.text = swiftTransferModel.accountNo
                PNumberLabel.text = swiftTransferModel.numberPP
                TransferSummLabel.text = transferSumm
                PaymentCodeLabel.text = swiftTransferModel.paymentCode
                dateVal.text = dateFormaterBankDay(date: swiftTransferModel.dateVal!)
                datePP.text = dateFormaterBankDay(date: swiftTransferModel.datePP!)
                if swiftTransferModel.paymentCode != ""{
                    for code in self.refOpers{
                        if swiftTransferModel.paymentCode == code.value{
                            PaymentCodeDescriptionLabel.text = code.text
                        }
                    }
                }
                TarifLabel.text =  "\((swiftTransferModel.expenseType ?? 1)) - \(String(describing: self.expenseTypes[(swiftTransferModel.expenseType ?? 1) - 1].text ?? ""))"
                PaymentCommentLabel.text = swiftTransferModel.detailsOfPayment
                VOcodeLabel.text = swiftTransferModel.codeVO
                VOcodeDescriptionLabel.text = voDescription
                ReceiverFullNameLabel.text = swiftTransferModel.fullNameReceiver
                ReceiverAccountNoLabel.text = swiftTransferModel.receiverAccountNo
                ReceiverBIKIntermediaryBankLabel.text = swiftTransferModel.intermediaryBankBic
                IntermediaryBankCountryLabel.text = swiftTransferModel.intermediaryBankCountry
                IntermediaryBankNameLabel.text = swiftTransferModel.intermediaryBank
//                AccountNoIntermediaryBankLabel.text = swiftTransferModel.intermediaryBankLoroAccountNo
                ReceiverBIKPayThruLabel.text = swiftTransferModel.receiverBankBic
                PayThruCountryLabel.text = swiftTransferModel.receiverBankCountryCode
                PayThruCountryNameLabel.text = swiftTransferModel.receiverBank
                AccountNoPayThruBankLabel.text = swiftTransferModel.receiverBankLoroAccountNo
                receiverAddress.text = swiftTransferModel.receiverAddress
                receiverCountryCode.text = swiftTransferModel.receiverCountryCode
            }
            let directions: [UISwipeGestureRecognizer.Direction] = [.up, .down, .right, .left]
            for direction in directions {
                let gesture = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(gesture:)))
                gesture.direction = direction
                view?.addGestureRecognizer(gesture)
            }
            
        }else{
            viewTemplate.isHidden = false
            viewButton.isHidden = false
            viewOperationState.isHidden = true
            self.commissionHelper(commissions: commisions,
                                  currencyID: swiftTransferModel.currencyID ?? 0,
                                  sum: swiftTransferModel?.transferSum ?? 0.0,
                                  summCommissionSender: summCommissionSender,
                                  summCommissionSenderCurrency: summCommissionSenderCurrency,
                                  summCommissionSenderStackView: summCommissionSenderStackView,
                                  summCommisionIntermediary: summCommisionIntermediary,
                                  summCommisionIntermediaryCurrency: summCommisionIntermediaryCurrency,
                                  summCommisionIntermediaryStackView: summCommisionIntermediaryStackView,
                                  summCommissionReceiver: summCommissionReceiver,
                                  summCommissionReceiverCurrency: summCommissionReceiverCurrency,
                                  summCommissionReceiverStackView: summCommissionReceiverStackView )
            view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
            if swiftTransferModel.intermediaryBankCountry == "" || swiftTransferModel.intermediaryBankCountry == nil {
                viewIntermediaryBank.isHidden = true
            }else{
                viewIntermediaryBank.isHidden = false
            }
            
            if swiftTransferModel.operationType == InternetBankingOperationType.SwiftTransfer.rawValue{
                typeOperation = localizedText(key: "swift_transfer")
            }
            self.operationTypeLabel.text = localizedText(key: "swift_transfer")
            if swiftTransferModel.codeVO == "" || swiftTransferModel.codeVO == nil {
                voCodeStackView.isHidden = true
            }else{
                voCodeStackView.isHidden = false
            }
            
            let transferSumm = "\(String(format:"%.2f",  swiftTransferModel.transferSum ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: swiftTransferModel.currencyID ?? 0))"
            SenderFullNameLabel.text = swiftTransferModel.senderFullName
            SenderAccountNoLabel.text = swiftTransferModel.accountNo
            PNumberLabel.text = swiftTransferModel.numberPP
            dateVal.text = swiftTransferModel.dateVal
            datePP.text = swiftTransferModel.datePP
            TransferSummLabel.text = transferSumm
            PaymentCodeLabel.text = swiftTransferModel.paymentCode
            PaymentCodeDescriptionLabel.text = paymentCodeDescription
            TarifLabel.text =  "\((swiftTransferModel.expenseType ?? 1)) - \(String(describing: self.expenseTypes[(swiftTransferModel.expenseType ?? 1) - 1].text ?? ""))"
            PaymentCommentLabel.text = swiftTransferModel.detailsOfPayment
            VOcodeLabel.text = swiftTransferModel.codeVO
            VOcodeDescriptionLabel.text = voDescription
            ReceiverFullNameLabel.text = swiftTransferModel.fullNameReceiver
            ReceiverAccountNoLabel.text = swiftTransferModel.receiverAccountNo
            ReceiverBIKIntermediaryBankLabel.text = swiftTransferModel.intermediaryBankBic
            IntermediaryBankCountryLabel.text = swiftTransferModel.intermediaryBankCountry
            IntermediaryBankNameLabel.text = swiftTransferModel.intermediaryBank
//            AccountNoIntermediaryBankLabel.text = swiftTransferModel.intermediaryBankLoroAccountNo
            ReceiverBIKPayThruLabel.text = swiftTransferModel.receiverBankBic
            PayThruCountryLabel.text = swiftTransferModel.receiverBankCountryCode
            PayThruCountryNameLabel.text = swiftTransferModel.receiverBank
            AccountNoPayThruBankLabel.text = swiftTransferModel.receiverBankLoroAccountNo
            receiverAddress.text = swiftTransferModel.receiverAddress
            receiverCountryCode.text = swiftTransferModel.receiverCountryCode
        }
    }
    
    //обработка swipe
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
            
        case UISwipeGestureRecognizer.Direction.right:
            self.navigationController?.popViewController(animated: true)
        default:
            print("")
        }
    }
    //    переход по segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSwiftsSegue"{
            let vc = segue.destination as! SWIFTViewController
            swiftTransferModel = SwiftTransferModel(
                senderFullName: swiftTransferModel.senderFullName,
                senderCustomerID: swiftTransferModel.senderCustomerID,
                expenseType: swiftTransferModel.expenseType,
                currencyID: swiftTransferModel.currencyID,
                accountNo: swiftTransferModel.accountNo,
                transferSum: swiftTransferModel.transferSum,
                detailsOfPayment: swiftTransferModel.detailsOfPayment,
                codeVO: swiftTransferModel.codeVO,
                intermediaryBank: swiftTransferModel.intermediaryBank,
                intermediaryBankCountry: swiftTransferModel.intermediaryBankCountry,
                intermediaryBankBic: swiftTransferModel.intermediaryBankBic,
                intermediaryBankLoroAccountNo: swiftTransferModel.intermediaryBankLoroAccountNo,
                paymentCode: swiftTransferModel.paymentCode,
                receiverBank: swiftTransferModel.receiverBank,
                receiverBankCountryCode: swiftTransferModel.receiverBankCountryCode,
                receiverBankBic: swiftTransferModel.receiverBankBic,
                fullNameReceiver: swiftTransferModel.fullNameReceiver,
                receiverAccountNo: swiftTransferModel.receiverAccountNo,
                receiverBankLoroAccountNo: swiftTransferModel.receiverBankLoroAccountNo,
                numberPP: swiftTransferModel.numberPP,
                datePP: swiftTransferModel.datePP,
                dateVal: swiftTransferModel.dateVal,
                operationType: swiftTransferModel.operationType,
                type: swiftTransferModel.type,
                senderBank: swiftTransferModel.senderBank,
                senderBankCountry: swiftTransferModel.senderBankCountry,
                senderBankBic: swiftTransferModel.senderBankBic,
                senderBankLoroAccountNo: swiftTransferModel.senderBankLoroAccountNo,
                operationID: operationID,
                isSchedule: false,
                isTemplate: swiftTransferModel.isTemplate,
                templateName: swiftTransferModel.templateName,
                templateDescription: "SWIFT перевод",
                scheduleID: swiftTransferModel.scheduleID,
                schedule: swiftTransferModel.schedule,
                additionalInfo: swiftTransferModel.additionalInfo,
                receiverAddress: swiftTransferModel.receiverAddress,
                receiverCountryCode: swiftTransferModel.receiverCountryCode,
                isCommissionAgreementChecked: swiftTransferModel.isCommissionAgreementChecked,
                swiftDocuments: swiftTransferModel.swiftDocuments
            )
            vc.swiftTransferModel = swiftTransferModel
            vc.isRepeatPay = true
            vc.isRepeatFromStory = true
            vc.operationID = operationID
            vc.navigationTitle = self.localizedText(key: "create_operation_request")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        
    }
    
    // кнопка закрытия
    @IBAction func closeButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func saveTemplateButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SWIFTViewController") as! SWIFTViewController
        vc.isCreateTemplate = true
        vc.swiftTransferModel = swiftTransferModel
        vc.selectedDeposit = selectedDeposit
        vc.navigationTitle = localizedText(key: "create_template")
        vc.expenseTypeID = swiftTransferModel.expenseType ?? 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    // кнопка FAB
    @IBAction func AlertSheetControl(_ sender: Any) {
        
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionRepeatOperation = UIAlertAction(title: self.localizedText(key: "create_operation_request"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toSwiftsSegue", sender: self)
        }
        let actionHSaveTemplate = UIAlertAction(title: self.localizedText(key: "save_as_template"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SWIFTViewController") as! SWIFTViewController
            vc.isCreateTemplate = true
            vc.isCreateTemplateFromStory = true
            vc.swiftTransferModel = self.swiftTransferModel
            vc.selectedDeposit = self.selectedDeposit
            vc.expenseTypeID = self.swiftTransferModel.expenseType ?? 1
            vc.navigationTitle = self.localizedText(key: "create_template")
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        let sendToBank = UIAlertAction(title: self.localizedText(key: "send_to_bank"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SubmitSwiftViewController") as! SubmitSwiftViewController
            vc.swiftTransferModel = self.swiftTransferModel
            vc.operationTypeID = self.swiftTransferModel.operationType ?? 0
            vc.operationID = self.operationID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let changeData = UIAlertAction(title: self.localizedText(key: "edit"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SWIFTViewController") as! SWIFTViewController
            self.swiftTransferModel = SwiftTransferModel(
                senderFullName: self.swiftTransferModel.senderFullName,
                senderCustomerID: self.swiftTransferModel.senderCustomerID,
                expenseType: self.swiftTransferModel.expenseType,
                currencyID: self.swiftTransferModel.currencyID,
                accountNo: self.swiftTransferModel.accountNo,
                transferSum: self.swiftTransferModel.transferSum,
                detailsOfPayment: self.swiftTransferModel.detailsOfPayment,
                codeVO: self.swiftTransferModel.codeVO,
                intermediaryBank: self.swiftTransferModel.intermediaryBank,
                intermediaryBankCountry: self.swiftTransferModel.intermediaryBankCountry,
                intermediaryBankBic: self.swiftTransferModel.intermediaryBankBic,
                intermediaryBankLoroAccountNo: self.swiftTransferModel.intermediaryBankLoroAccountNo, 
                paymentCode: self.swiftTransferModel.paymentCode,
                receiverBank: self.swiftTransferModel.receiverBank,
                receiverBankCountryCode: self.swiftTransferModel.receiverBankCountryCode,
                receiverBankBic: self.swiftTransferModel.receiverBankBic,
                fullNameReceiver: self.swiftTransferModel.fullNameReceiver,
                receiverAccountNo: self.swiftTransferModel.receiverAccountNo,
                receiverBankLoroAccountNo: self.swiftTransferModel.receiverBankLoroAccountNo,
                numberPP: self.swiftTransferModel.numberPP,
                datePP: self.swiftTransferModel.datePP,
                dateVal: self.swiftTransferModel.dateVal,
                operationType: self.swiftTransferModel.operationType,
                type: self.swiftTransferModel.type,
                senderBank: self.swiftTransferModel.senderBank,
                senderBankCountry: self.swiftTransferModel.senderBankCountry,
                senderBankBic: self.swiftTransferModel.senderBankBic,
                senderBankLoroAccountNo: self.swiftTransferModel.senderBankLoroAccountNo,
                operationID: self.operationID,
                isSchedule: false,
                isTemplate: self.swiftTransferModel.isTemplate,
                templateName: self.swiftTransferModel.templateName,
                templateDescription: "SWIFT перевод",
                scheduleID: self.swiftTransferModel.scheduleID,
                schedule: self.swiftTransferModel.schedule,
                additionalInfo: self.swiftTransferModel.additionalInfo,
                receiverAddress: self.swiftTransferModel.receiverAddress,
                receiverCountryCode: self.swiftTransferModel.receiverCountryCode,
                isCommissionAgreementChecked: self.swiftTransferModel.isCommissionAgreementChecked,
                swiftDocuments: self.swiftTransferModel.swiftDocuments
            )
            vc.swiftTransferModel = self.swiftTransferModel
            vc.isRepeatPay = true
            vc.isRepeatFromStory = true
            vc.isChangeData = true
            vc.operationID = self.operationID
            vc.navigationTitle = self.localizedText(key: "create_operation_request")
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let actionDownload = UIAlertAction(title: self.localizedText(key: "receipt"), style: .default) { (action) in
            ApiHelper.shared.getInfoOperationFile(operationID: self.swiftTransferModel.operationID ?? 0,
                                                  code: 00000,
                                                  operationTypeID: self.swiftTransferModel.operationType ?? 0,
                                                  vc:self )
        }
        let actionRemove = UIAlertAction(title: self.localizedText(key: "delete"), style: .default) { (action) in
            let alert = UIAlertController(title:  self.localizedText(key: "delete_operation"), message: "", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                ApiHelper.shared.deleteTemplate(operationID: "\(self.operationID)")
                self.navigationController?.popViewController(animated: true)
            })
            let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)
            alert.addAction(actionOk)
            alert.addAction(actionCancel)
            self.present(alert, animated: true, completion: nil)
        }
        
        let actionDoc = UIAlertAction(title: localizedText(key: "Documents"), style: .default) { (action) in
            ApiHelper.shared.getInfoOperationFileSwift(transferID: self.transferID, vc: self)
            
        }
        
        let actionCancel  = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)
        
        
        // отображение действий в зависимости от confirmType
        switch confirmType {
        case ConfirmType.Save.rawValue:
            if isNeedOperationDetailByPayment{
                alertSheet.addAction(actionDoc)
            }else{
                if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionRepeatOperation)
                }
                
                if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionHSaveTemplate)
                }
                if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
                
                if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsEditAllowed"){
                    alertSheet.addAction(changeData)
                }
                if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsRemoveAllowed"){
                    alertSheet.addAction(actionRemove)
                }
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Confirm.rawValue:
            if isNeedOperationDetailByPayment{
                alertSheet.addAction(actionDoc)
            }else{
                if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionRepeatOperation)
                }
                if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionHSaveTemplate)
                }
                alertSheet.addAction(actionDownload)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.InConfirm.rawValue:
            if isNeedOperationDetailByPayment{
                alertSheet.addAction(actionDoc)
            }else{
                if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionRepeatOperation)
                }
                if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionHSaveTemplate)
                }
                if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Error.rawValue:
            if isNeedOperationDetailByPayment{
                alertSheet.addAction(actionDoc)
            }else{
                if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionRepeatOperation)
                }
                if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionHSaveTemplate)
                }
                if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Verified.rawValue:
            if isNeedOperationDetailByPayment{
                alertSheet.addAction(actionDoc)
            }else{
                if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionRepeatOperation)
                }
                if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionHSaveTemplate)
                }
                if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
                    alertSheet.addAction(sendToBank)
                }
            }
            alertSheet.addAction(actionCancel)
            
        default:
            if isNeedOperationDetailByPayment{
                alertSheet.addAction(actionDoc)
            }else{
//                if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
//                    alertSheet.addAction(actionRepeatOperation)
//                }
                if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                    alertSheet.addAction(actionHSaveTemplate)
                }
            }
            alertSheet.addAction(actionCancel)
        }
        
        
        present(alertSheet, animated: true, completion: nil)
    }
    
}

// Создание таблицы списка коммиссий
extension TemaplateSwiftViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commisions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TemplateSwiftTableViewCell
        cell.commissionLabel.text = "\(String(describing: commisions[indexPath.row].commissionSumm ?? 0.0))"
        cell.descriptionCommissionLabel.text = commisions[indexPath.row].commissionName
        cell.commissionCurrencyLabel.text = commisions[indexPath.row].currency?.symbol
        
        return cell
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableHeight?.constant = self.tableView.contentSize.height
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    //    получение инфо об операции
    func  getInfoOperationTransactions(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationSwift(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.swiftTransferModel = operation
                    if self.swiftTransferModel.paymentCode != ""{
                        for code in self.refOpers{
                            if self.swiftTransferModel.paymentCode == code.value{
                                self.PaymentCodeDescriptionLabel.text = code.text
                            }
                        }
                    }
                    if self.swiftTransferModel.codeVO == "" || self.swiftTransferModel.codeVO == nil{
                        self.voCodeStackView.isHidden = true
                    }else{
                        self.voCodeStackView.isHidden = false
                        for code in self.voCodes{
                            if self.swiftTransferModel.codeVO == code.value{
                                self.VOcodeDescriptionLabel.text = code.text
                            }
                        }
                    }
                    
                    if self.swiftTransferModel.intermediaryBankBic == nil{
                        self.viewIntermediaryBank.isHidden = true
                    }
                    self.operationTypeLabel.text = self.localizedText(key: "swift_transfer")
                    
                    let transferSumm = "\(String(format:"%.2f", self.swiftTransferModel.transferSum ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: self.swiftTransferModel.currencyID ?? 0))"
                    self.SenderFullNameLabel.text = self.swiftTransferModel.senderFullName
                    self.SenderAccountNoLabel.text = self.swiftTransferModel.accountNo
                    self.PNumberLabel.text = self.swiftTransferModel.numberPP
                    self.TransferSummLabel.text = transferSumm
                    self.PaymentCodeLabel.text = self.swiftTransferModel.paymentCode
                    
                    self.TarifLabel.text = "\((self.swiftTransferModel.expenseType ?? 1)) - \(String(describing: self.expenseTypes[(self.swiftTransferModel.expenseType ?? 1) - 1].text ?? ""))"
                    self.PaymentCommentLabel.text = self.swiftTransferModel.detailsOfPayment
                    self.VOcodeLabel.text = self.swiftTransferModel.codeVO
                    
                    self.ReceiverBIKIntermediaryBankLabel.text = self.swiftTransferModel.intermediaryBankBic
                    self.ReceiverBIKPayThruLabel.text = self.swiftTransferModel.receiverBankBic
                    self.ReceiverFullNameLabel.text = self.swiftTransferModel.fullNameReceiver
                    self.ReceiverAccountNoLabel.text = self.swiftTransferModel.receiverAccountNo
                    self.IntermediaryBankCountryLabel.text = self.swiftTransferModel.intermediaryBankCountry
                    self.IntermediaryBankNameLabel.text = self.swiftTransferModel.intermediaryBank
//                    self.AccountNoIntermediaryBankLabel.text = self.swiftTransferModel.intermediaryBankLoroAccountNo
                    self.PayThruCountryLabel.text = self.swiftTransferModel.receiverBankCountryCode
                    self.PayThruCountryNameLabel.text = self.swiftTransferModel.receiverBank
                    self.dateVal.text = self.dateFormaterBankDay(date: self.swiftTransferModel.dateVal!)
                    self.datePP.text = self.dateFormaterBankDay(date: self.swiftTransferModel.datePP!)
                    self.AccountNoPayThruBankLabel.text = self.swiftTransferModel.receiverBankLoroAccountNo
                    (self.stateLabel.text, self.stateLabel.textColor) = ValueHelper.shared.confirmTypeDefine(confirmType: self.swiftTransferModel.confirmType ?? 0)
                    self.confirmType = self.swiftTransferModel.confirmType ?? 0
                    let sum = self.swiftTransferModel.transferSum ?? 0.0
                    self.commissionHelper(commissions: self.commisions,
                                          currencyID: self.swiftTransferModel.currencyID ?? 0,
                                          sum: self.swiftTransferModel?.transferSum ?? 0.0,
                                          summCommissionSender: self.summCommissionSender,
                                          summCommissionSenderCurrency: self.summCommissionSenderCurrency,
                                          summCommissionSenderStackView: self.summCommissionSenderStackView,
                                          summCommisionIntermediary: self.summCommisionIntermediary,
                                          summCommisionIntermediaryCurrency: self.summCommisionIntermediaryCurrency,
                                          summCommisionIntermediaryStackView: self.summCommisionIntermediaryStackView,
                                          summCommissionReceiver: self.summCommissionReceiver,
                                          summCommissionReceiverCurrency: self.summCommissionReceiverCurrency,
                                          summCommissionReceiverStackView: self.summCommissionReceiverStackView )
                    self.getCommission(accountNo: self.swiftTransferModel.accountNo ?? "", currencyID: self.swiftTransferModel.currencyID ?? 0, transferSum: sum , expenseType: self.swiftTransferModel.expenseType ?? 0)
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    кастомизация кнопки
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        var image = ""
        if isNeedOperationDetail{
            image = "ArrowBack"
            button.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        }else{
            image = "Close"
            button.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        }
        button.setImage(UIImage(named: image), for: .normal)
        
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 50)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    //    кнопка закрытия
    @objc func closeButtonPressed() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    //    кнопка назад
    @objc func backButtonPressed() {
        navigationController?.popViewController(animated: true)
        delegate?.isNeedData(isNeed: false)
    }
    
    // Расчет коммиссии
    func getCommission(accountNo: String, currencyID: Int, transferSum: Double, expenseType: Int){
        showLoadingWithStatus(message: self.localizedText(key: "commission_calculation"))
        managerApi
            .getSwiftCommission(accountNo: accountNo, currencyID: currencyID, transferSum: transferSum, expenseType: expenseType)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (commisions) in
                    guard let `self` = self else { return }
                    
                    self.commisions = self.commissionWithCurrencies(currencies: self.currencies, commissions: commisions)
                    let sum = self.swiftTransferModel.transferSum ?? 0.0
                    self.commissionHelper(
                        commissions: self.commisions,
                        currencyID: self.swiftTransferModel.currencyID ?? 0,
                        sum: sum,
                        summCommissionSender: self.summCommissionSender,
                        summCommissionSenderCurrency: self.summCommissionSenderCurrency,
                        summCommissionSenderStackView: self.summCommissionSenderStackView,
                        summCommisionIntermediary: self.summCommisionIntermediary,
                        summCommisionIntermediaryCurrency: self.summCommisionIntermediaryCurrency,
                        summCommisionIntermediaryStackView: self.summCommisionIntermediaryStackView,
                        summCommissionReceiver: self.summCommissionReceiver,
                        summCommissionReceiverCurrency: self.summCommissionReceiverCurrency,
                        summCommissionReceiverStackView: self.summCommissionReceiverStackView )
                    self.tableView.reloadData()
                    
                    self.hideLoading()
                    
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //Добавление вида валюты в коммиссию
    func commissionWithCurrencies(currencies: [Currency], commissions: [SwiftCommissionModel]) -> [SwiftCommissionModel]{
        
        for commission in commissions{
            for currency in currencies{
                if commission.currencyID == currency.currencyID{
                    commission.currency = currency
                }
            }
        }
        return commissions
    }
}
