//
//  SWIFTViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/2/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa
import MobileCoreServices
import UniformTypeIdentifiers
import MobileCoreServices

class SWIFTViewController: BaseViewController, DepositProtocol, SwiftRefOpersProtocol, CodeVOProtocol, EditOperationProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var SenderFullNameLabel: UILabel!
    @IBOutlet weak var PNumberTextFields: UITextField!
    @IBOutlet weak var TransferSummTextField: UITextField!
    @IBOutlet weak var PaymentCodeTextField: UITextField!
    @IBOutlet weak var PaymentDatePPFull: UITextField!
    @IBOutlet weak var PaymentDateVal: UITextField!
    @IBOutlet weak var TarifTextField: UITextField!
    @IBOutlet weak var PaymentCommentTextField: UITextField!
    @IBOutlet weak var VOcodeTextField: UITextField!
    @IBOutlet weak var ReceiverFullNameTextField: UITextField!
    @IBOutlet weak var ReceiverAccountNoTextField: UITextField!
    @IBOutlet weak var ReceiverBIKIntermediaryBankTextField: UITextField!
    @IBOutlet weak var ReceiverBIKPayThruTextField: UITextField!
    @IBOutlet weak var PaymentCodeDescriptionLabel: UILabel!
    
    @IBOutlet weak var AccountNoIntermediaryBankTextField: UITextField!
    @IBOutlet weak var AccountNoPayThruBankTextField: UITextField!
    
    //commission
    @IBOutlet weak var commissionView: UIView!
    @IBOutlet weak var VODescriptionLabel: UILabel!
    
    @IBOutlet weak var summCommissionSender: UILabel!
    @IBOutlet weak var summCommissionSenderCurrency: UILabel!
    @IBOutlet weak var summCommisionIntermediary: UILabel!
    @IBOutlet weak var summCommisionIntermediaryCurrency: UILabel!
    @IBOutlet weak var summCommissionReceiver: UILabel!
    @IBOutlet weak var summCommissionReceiverCurrency: UILabel!
    @IBOutlet weak var totalSumm4: UILabel!
    @IBOutlet weak var totalSumm4Currency: UILabel!
    @IBOutlet weak var totalSumm5: UILabel!
    @IBOutlet weak var totalSumm5Currency: UILabel!
    @IBOutlet weak var totalSumm6: UILabel!
    @IBOutlet weak var totalSumm6Currency: UILabel!
    @IBOutlet weak var summCommissionSenderStackView: UIStackView!
    @IBOutlet weak var summCommisionIntermediaryStackView: UIStackView!
    @IBOutlet weak var summCommissionReceiverStackView: UIStackView!
    @IBOutlet weak var totalSumm4StackView: UIStackView!
    @IBOutlet weak var totalSumm5StackView: UIStackView!
    @IBOutlet weak var totalSumm6StackView: UIStackView!
    @IBOutlet weak var IntermediaryBankCountryLabel: UILabel!
    @IBOutlet weak var IntermediaryBankNameLabel: UILabel!
    @IBOutlet weak var PayThruCountryLabel: UILabel!
    @IBOutlet weak var PayThruCountryNameLabel: UILabel!
    //    @IBOutlet weak var payFrom_accountsLabel: UILabel!
    @IBOutlet weak var choiceCard_accountsLabel: UILabel!
    @IBOutlet weak var ccurrencySymbolLabel: UILabel!
    
    // views error
    @IBOutlet weak var viewTemplateNameTextFields: UIView!
    @IBOutlet weak var viewPNumberTextFields: UIView!
    @IBOutlet weak var viewFromAccount: UIView!
    @IBOutlet weak var viewTransferSummTextField: UIView!
    @IBOutlet weak var viewPaymentCodeTextField: UIView!
    @IBOutlet weak var viewPaymentCommentTextField: UIView!
    @IBOutlet weak var viewVOcodeTextField: UIView!
    
    @IBOutlet weak var viewReceiverFullNameTextField: UIView!
    @IBOutlet weak var viewReceiverAccountNoTextField: UIView!
    @IBOutlet weak var viewReceiverBIKTextField: UIView!
    @IBOutlet weak var viewPayThruAccountNoTextField: UIView!
    @IBOutlet weak var viewIntermediaryBank: UIView!
    @IBOutlet weak var viewPayThru: UIView!
    @IBOutlet weak var viewVO: UIView!
    //label error
    
    @IBOutlet weak var labelErrorTemplateName: UILabel!
    @IBOutlet weak var labelErrorPNumber: UILabel!
    @IBOutlet weak var labelErrorFromAccount: UILabel!
    @IBOutlet weak var labelErrorTransferSumm: UILabel!
    @IBOutlet weak var labelErrorPaymentCode: UILabel!
    @IBOutlet weak var labelErrorPaymentComment: UILabel!
    @IBOutlet weak var labelErrorVOcode: UILabel!
    @IBOutlet weak var labelErrorReceiverFullName: UILabel!
    @IBOutlet weak var labelErrorReceiverAccountNo: UILabel!
    @IBOutlet weak var labelErrorReceiverBIK: UILabel!
    @IBOutlet weak var labelErrorPaythruAccountNo: UILabel!
    
    // Intermediary Bank - Банк Посредник
    @IBOutlet weak var showHideIntermediaryBankField: UIImageView!
    @IBOutlet weak var BIKIntermediaryBankView: UIView!
    @IBOutlet weak var accountPayThroughView: UIView!
    
    // Additional info - Доп информация
    @IBOutlet weak var additionalInfoChevron: UIImageView!
    @IBOutlet weak var additionalInfoView: UIView!
    
    // Guaranteed Payment Hint - Гарантированный платеж
    @IBOutlet weak var guaranteedPaymentHint: UILabel!
    
    // address View
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var recipientCountryTextField: UITextField!
    private var countryCode: String = ""
    
    @IBOutlet weak var checkBoxView: UIImageView!
    
    private var arrayOfCountries: [ReferenceItemModel]!
    
    
    @IBOutlet weak var pickedImage: UIImageView!
    @IBOutlet weak var pickedImagesStackView: UIStackView!
    
    //Template IBOutlets
    var isCreateTemplate = false
    var isSchedule = false
    var StartDate = ""
    var EndDate: String? = ""
    var RecurringTypeID = 2
    var ProcessDay = 1
    var schedule: Schedule!
    //EditingTemplate
    var isEditTemplate = false
    var template: ContractModelTemplate!
    //RepeatPayTemplate
    var isRepeatPay = false
    var isRepeatOperation = false
    var isChangeData = false
    
    // checkBox variables
    private var checkBox = false
    private var enableTransfer = false
    
    private lazy var swiftDocuments: [SwiftDocument] = []
    private lazy var swiftDocumentsData: [Data] = []
    private var allSize: [Double] = []
    
    
    @IBOutlet weak var TemplateDescriptionView: UIView!
    @IBOutlet weak var ScheduleView: UIView!
    @IBOutlet weak var templateNameTextField: UITextField!
    
    @IBOutlet weak var switchPlanner: UISwitch!
    @IBOutlet weak var viewPlanner: UIView!
    @IBOutlet weak var periodTextField: UITextField!
    
    @IBOutlet weak var viewDateBegin: UIView!
    @IBOutlet weak var viewDateFinish: UIView!
    @IBOutlet weak var viewWeekDay: UIView!
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var viewDateOperation: UIView!
    
    @IBOutlet weak var dateBeginTextField: UITextField!
    @IBOutlet weak var dateFinishTextField: UITextField!
    
    @IBOutlet weak var dayNumberTextField: UITextField!
    @IBOutlet weak var dateOperationTextField: UITextField!
    @IBOutlet weak var weekDayTextField: UITextField!
    
    @IBOutlet weak var additionalInfo: UITextField!
    
    @IBOutlet weak var createSwiftButton: RoundedButton!
    @IBOutlet weak var attachDocumentButton: RoundedButton!
    
    var typeOperation = ""
    
    var pickerView = UIPickerView()
    var pickerViewDayOperation = UIPickerView()
    var pickerViewWeekDay = UIPickerView()
    var pickerTarif = UIPickerView()
    var pickerCountry = UIPickerView()
    
    var periodList = ["EachWeek".localized, "EachMonth".localized, "OneTime".localized]
    var daysList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20","21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
    var weekList = ["monday".localized, "tuesday".localized, "wednesday".localized, "thursday".localized, "friday".localized, "saturday".localized, "sunday".localized]
    let datePicker = UIDatePicker()
    
    var operationTypeID = 0
    var swiftAccounts: [Accounts]!
    var selectedDeposit: Deposit!
    var voCodes: [ReferenceItemModel]!
    var voCode: ReferenceItemModel!
    var voCodeID: String!
    var refOpers: [ReferenceItemModel]!
    var selectedRefOpers: ReferenceItemModel!
    var expenseTypes: [expenseTypeModel]!
    var requireVoCodeForCurrencies: SwiftValidateDataModel!
    
    var expenseTypeID = 1
    var senderCustomerID = 0
    
    var swiftTransferModel: SwiftTransferModel!
    var templateSwiftTransferModel: SwiftTransferModel!
    var receiverBank: SwiftBikModel!
    var intermediaryBank: SwiftBikModel!
    var senderBank: SwiftBikModel!
    var navigationTitle = ""
    var operationID = 0
    var type = 0
    var commisions = [SwiftCommissionModel]()
    var currencies = [Currency]()
    var transferSum = 0.0
    
    var isRepeatFromStory = false
    var isCreateTemplateFromStory = false
    
    var currencyIDcurrent = 0
    var accountNoCurrent = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        allowToCreatAndSaveOperation(button: createSwiftButton)
        
        if isCreateTemplate{
            if swiftTransferModel != nil{
                if self.swiftTransferModel.paymentCode != ""{
                    for code in self.refOpers{
                        if self.swiftTransferModel.paymentCode == code.value{
                            self.selectedRefOpers = code
                        }
                    }
                }
                if self.swiftTransferModel.codeVO != ""{
                    for code in self.voCodes{
                        if self.swiftTransferModel.codeVO == code.value{
                            self.VODescriptionLabel.text = code.text
                        }
                    }
                }
                SenderFullNameLabel.text = swiftTransferModel.senderFullName
                PNumberTextFields.text = swiftTransferModel.numberPP
                TransferSummTextField.text = "\(swiftTransferModel.transferSum ?? 0.00)"
                PaymentCodeTextField.text = swiftTransferModel.paymentCode
                TarifTextField.text = "\(swiftTransferModel.expenseType ?? 1) - \(String(describing: expenseTypes?[(swiftTransferModel.expenseType ?? 1) - 1].text ?? ""))"
                PaymentCommentTextField.text = swiftTransferModel.detailsOfPayment
                PaymentDateVal.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: swiftTransferModel.dateVal ?? "")
                PaymentDatePPFull.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: swiftTransferModel.datePP ?? "")
                VOcodeTextField.text = swiftTransferModel.codeVO
                ReceiverFullNameTextField.text = swiftTransferModel.fullNameReceiver
                ReceiverAccountNoTextField.text = swiftTransferModel.receiverAccountNo
                ReceiverBIKIntermediaryBankTextField.text = swiftTransferModel.intermediaryBankBic
                IntermediaryBankCountryLabel.text = swiftTransferModel.intermediaryBankCountry
                IntermediaryBankNameLabel.text = swiftTransferModel.intermediaryBank
                AccountNoIntermediaryBankTextField.text = swiftTransferModel.intermediaryBankLoroAccountNo
                ReceiverBIKPayThruTextField.text = swiftTransferModel.receiverBankBic
                PayThruCountryNameLabel.text = swiftTransferModel.intermediaryBankCountry
                PayThruCountryLabel.text = swiftTransferModel.intermediaryBank
                AccountNoPayThruBankTextField.text = swiftTransferModel.receiverBankLoroAccountNo
                
                
                if IntermediaryBankCountryLabel.text == nil || swiftTransferModel.intermediaryBank == nil {
                    viewIntermediaryBank.isHidden = true
                }else{
                    viewIntermediaryBank.isHidden = false
                    getSwiftBikForIntermediary(bic: ReceiverBIKIntermediaryBankTextField.text ?? "")
                }
                if ReceiverBIKPayThruTextField.text != ""{
                    getSwiftBikForIPayThru(bic: ReceiverBIKPayThruTextField.text ?? "")
                }
                
            }
            createSwiftButton.setTitle(localizedText(key: "save_template"),for: .normal)
        }else if isEditTemplate || isRepeatPay {
            TemplateDescriptionView.isHidden = false
            ScheduleView.isHidden = false
            
            if isEditTemplate{
                //                isCreateTemplate = true
                if template.isSchedule ?? false{
                    RecurringTypeID = template.schedule?.recurringTypeID ?? 1
                    isSchedule = true
                    switchPlanner.isOn = true
                    StartDate = template.schedule?.startDate ?? ""
                    EndDate = template.schedule?.endDate
                    RecurringTypeID = template.schedule?.recurringTypeID ?? 0
                    ProcessDay = template.schedule?.processDay ?? 0
                    viewPlanner.isHidden = false
                    operationID = template.operationID ?? 0
                    
                    let schedule = ScheduleHelper.shared.scheduleFullScheduleHelper(
                        StartDate: template.schedule?.startDate,
                        EndDate: template.schedule?.endDate,
                        RecurringTypeID: template.schedule?.recurringTypeID,
                        ProcessDay: template.schedule?.processDay)
                    
                    switch RecurringTypeID {
                        
                    case ScheduleRepeat.OneTime.rawValue:
                        viewDateBegin.isHidden = true
                        viewDateFinish.isHidden = true
                        viewWeekDay.isHidden = true
                        viewDay.isHidden = true
                        viewDateOperation.isHidden = false
                        periodTextField.text = periodList[2]
                        dateOperationTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                        
                    case ScheduleRepeat.EachWeek.rawValue:
                        viewDateBegin.isHidden = false
                        viewDateFinish.isHidden = false
                        viewWeekDay.isHidden = false
                        viewDay.isHidden = true
                        viewDateOperation.isHidden = true
                        periodTextField.text = periodList[0]
                        dateBeginTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                        dateFinishTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.endDate ?? "")
                        weekDayTextField.text = weekList[(schedule?.processDay ?? 1) - 1]
                        
                    case ScheduleRepeat.EachMonth.rawValue:
                        viewDateBegin.isHidden = false
                        viewDateFinish.isHidden = false
                        viewWeekDay.isHidden = true
                        viewDay.isHidden = false
                        viewDateOperation.isHidden = true
                        periodTextField.text = periodList[1]
                        dateBeginTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                        dateFinishTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.endDate ?? "")
                        dayNumberTextField.text = daysList[(schedule?.processDay ?? 1) - 1]
                        
                    default:
                        viewDateBegin.isHidden = true
                        viewDateFinish.isHidden = true
                        viewWeekDay.isHidden = true
                        viewDay.isHidden = true
                        viewDateOperation.isHidden = true
                        
                    }
                    
                }
                createSwiftButton.setTitle(localizedText(key: "edit_template"),for: .normal)
            }
            
            if isRepeatPay{
                TemplateDescriptionView.isHidden = true
                ScheduleView.isHidden = true
                
                createSwiftButton.setTitle(localizedText(key: "execute_operation"),for: .normal)
                
            }
            if self.selectedDeposit != nil{
                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
            }
        }
        
        if selectedDeposit != nil{
            let symbolString = " " + (selectedDeposit.currencySymbol ?? "")
            let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
            //            payFrom_accountsLabel.text = fullAccountsText
            choiceCard_accountsLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currencySymbol ?? ""))"
            ccurrencySymbolLabel.text = symbolString
            if let voCodes = requireVoCodeForCurrencies?.requireVoCodeForCurrencies{
                if let selectCurrency = selectedDeposit.currencyID{
                    if voCodes.contains(selectCurrency){
                        viewVO.isHidden = false
                    }else{
                        viewVO.isHidden = true
                        VOcodeTextField.text = ""
                        VODescriptionLabel.text  = ""
                    }
                }
            }
            if TransferSummTextField.text != ""{
                let textNonZeroFirst = TransferSummTextField.text
                let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
                let numberAsInt = Double(newString ?? "0")
                TransferSummTextField.text = "\(numberAsInt ?? 0)"
                if (numberAsInt ?? 0) >= Double(selectedDeposit.balance ?? 0.0){
                    showAlertController(localizedText(key: "insufficient_account_balance"))
                    TransferSummTextField.text = ""
                }
                let sum = Double(TransferSummTextField.text ?? "0.0")
                if self.selectedDeposit != nil{
                    getCommission(accountNo: selectedDeposit.accountNo ?? "", currencyID: selectedDeposit.currencyID ?? 0, transferSum: sum ?? 0.0, expenseType: expenseTypeID)
                }
            }
        }else{
            //            payFrom_accountsLabel.text = localizedText(key: "AccountNo")
            choiceCard_accountsLabel.text = localizedText(key: "select_account_or_card")
        }
        if selectedRefOpers != nil{
            PaymentCodeTextField.text = selectedRefOpers.value
            PaymentCodeDescriptionLabel.text = selectedRefOpers.text
            PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
            
        }else{
            PaymentCodeDescriptionLabel.text = ""
        }
        if voCode != nil{
            viewVO.isHidden = false
            VOcodeTextField.text = voCode.value
            VODescriptionLabel.text  = voCode.text
            VOcodeTextField.stateIfEmpty(view: viewVOcodeTextField, labelError: labelErrorVOcode)
        }else{
            VODescriptionLabel.text = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCountries()
        
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        periodList = [localizedText(key: "every_week"),localizedText(key: "every_month"), NSLocalizedString("_retry", comment: "Однократно")]
        weekList = [localizedText(key: "mon"),
                    localizedText(key:"tue"),
                    localizedText(key:"wed"),
                    localizedText(key:"th"),
                    localizedText(key:"fri"),
                    localizedText(key:"sat"),
                    localizedText(key:"the_sun")]
        
        hideKeyboardWhenTappedAround()
        getSwiftAccounts()
        
        if let requireVoCodeForCurrencies = UserDefaultsHelper.requireVoCodeForCurrencies {
            self.requireVoCodeForCurrencies = requireVoCodeForCurrencies
        }
        if let refOpers = UserDefaultsHelper.refOpers {
            self.refOpers = refOpers
        }
        if let voCodes = UserDefaultsHelper.voCodes {
            self.voCodes = voCodes.sorted{ $0.value! < $1.value! }
        }
        if let expenseTypes = UserDefaultsHelper.expenseTypes {
            self.expenseTypes = expenseTypes
        }
        if let currencs =  UserDefaultsHelper.currencies{
            currencies = currencs
        }
        
        if isCreateTemplate{
            TemplateDescriptionView.isHidden = false
            ScheduleView.isHidden = false
        }else{
            TemplateDescriptionView.isHidden = true
            ScheduleView.isHidden = true
            
        }
        
        summCommissionSenderStackView.isHidden = true
        summCommisionIntermediaryStackView.isHidden = true
        summCommissionReceiverStackView.isHidden = true
        totalSumm4StackView.isHidden = true
        totalSumm5StackView.isHidden = true
        totalSumm6StackView.isHidden = true
        
        commissionView.isHidden = true
        SenderFullNameLabel.text = SessionManager.shared.user?.userdisplayname
        
        if !isCreateTemplate{
            if template != nil{
                getSwiftExistTemplate(operationID: template.operationID ?? 0, template: "true")
            }
            if operationID != 0{ // qweqwe figure out what is this for operationID
                getSwiftExistTemplate(operationID: self.operationID, template: "true")
            }
            if swiftTransferModel != nil{
                if swiftTransferModel.operationID != 0 {
                    getSwiftExistTemplate(operationID: swiftTransferModel.operationID ?? 0, template: "true")
                }
            }
            
            // qweqwe do some refactoring at this point && Make sure you are calling this API call at the appropriate pace
            if template == nil && operationID == 0 && swiftTransferModel == nil {
                getNameAndNumberPPFromSwiftTemplate(template: "false", type: 1)
            }
        }
        
        templateNameTextField.delegate = self
        PaymentCodeTextField.delegate = self
        ReceiverAccountNoTextField.delegate = self
        TransferSummTextField.delegate = self
        PNumberTextFields.delegate = self
        PaymentCommentTextField.delegate = self
        ReceiverFullNameTextField.delegate = self
        AccountNoIntermediaryBankTextField.delegate = self
        ReceiverBIKIntermediaryBankTextField.delegate = self
        ReceiverBIKPayThruTextField.delegate = self
        PaymentDateVal.delegate = self
        PaymentDatePPFull.delegate = self
        AccountNoPayThruBankTextField.delegate = self
        VOcodeTextField.delegate = self
        
        viewIntermediaryBank.isHidden = true
        viewPayThru.isHidden = true
        viewVO.isHidden = true
        
        
        
        PNumberTextFields.addTarget(self, action: #selector(textFieldEditingDidEndPNumber(_:)), for: UIControl.Event.editingDidEnd)
        TransferSummTextField.addTarget(self, action: #selector(textFieldEditingDidChange(_:)), for: UIControl.Event.editingDidEnd)
        PaymentCodeTextField.addTarget(self, action: #selector(textFieldEditingEnd(_:)), for: UIControl.Event.editingDidEnd)
        PaymentCommentTextField.addTarget(self, action: #selector(textFieldEditingEndPaymentComment(_:)), for: UIControl.Event.editingDidEnd)
        TarifTextField.addTarget(self, action: #selector(textFieldEditingTarif(_:)), for: UIControl.Event.editingDidEnd)
        
        ReceiverFullNameTextField.addTarget(self, action: #selector(textFieldEditingEndReceiverFullName(_:)), for: UIControl.Event.editingDidEnd)
        ReceiverAccountNoTextField.addTarget(self, action: #selector(textFieldEditingEndReceiverAccountNo(_:)), for: UIControl.Event.editingDidEnd)
        AccountNoIntermediaryBankTextField.addTarget(self, action: #selector(textFieldEditingEndAccountNoIntermediaryBank(_:)), for: UIControl.Event.editingDidEnd)
        ReceiverBIKIntermediaryBankTextField.addTarget(self, action: #selector(textFieldEditingEndReceiverBIKIntermediaryBank(_:)), for: UIControl.Event.editingDidEnd)
        ReceiverBIKPayThruTextField.addTarget(self, action: #selector(textFieldEditingEndReceiverBIKPay(_:)), for: UIControl.Event.editingDidEnd)
        VOcodeTextField.addTarget(self, action: #selector(textFieldEditingEndVOcode(_:)), for: UIControl.Event.editingDidEnd)
        
        templateNameTextField.addTarget(self, action: #selector(textFieldEditingEndtemplateName(_:)), for: UIControl.Event.editingDidEnd)
        
        let arrowDownTap = UITapGestureRecognizer(target: self, action: #selector(self.showHideIntermediaryImageTapped))
        showHideIntermediaryBankField.addGestureRecognizer(arrowDownTap)
        showHideIntermediaryBankField.isUserInteractionEnabled = true
        
        let additionalInfoTap = UITapGestureRecognizer(target: self, action: #selector(self.showHideAdditionalInfoImageTapped))
        additionalInfoChevron.addGestureRecognizer(additionalInfoTap)
        additionalInfoChevron.isUserInteractionEnabled = true
        
        let checkBoxTap = UITapGestureRecognizer(target: self, action: #selector(checkBoxViewTapped))
        checkBoxView.addGestureRecognizer(checkBoxTap)
        checkBoxView.isUserInteractionEnabled = true
        
        //schedule
        viewPlanner.isHidden = true
        viewDateBegin.isHidden = true
        viewDateFinish.isHidden = true
        viewWeekDay.isHidden = true
        viewDay.isHidden = true
        pickerPeriodDate()
        showDatePicker()
        
        viewDateOperation.isHidden = true
        dateBeginTextField.delegate = self
        dateFinishTextField.delegate = self
        dayNumberTextField.delegate = self
        weekDayTextField.delegate = self
    }
    
    @objc func checkBoxViewTapped(sender: UITapGestureRecognizer) {
        if checkBox {
            checkBoxView.image = UIImage(named: "unCheckedBoxRed")
            enableTransfer = false
        } else {
            checkBoxView.image = UIImage(named: "checkedBoxRed")
            enableTransfer = true
        }
        checkBox = !checkBox
    }
    
    // Запрос на получения списка стран
    func getCountries(){
        let transferCountriesObservable = managerApi.getSwiftCountries()
        showLoading()
        transferCountriesObservable
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (transferCountries) in
                    self?.arrayOfCountries = transferCountries
                    self?.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                }
            )
            .disposed(by: disposeBag)
    }
    
    // Оброботка нажатия на additionalInfoStackView Доп Инфо
    @objc func showHideAdditionalInfoImageTapped(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            additionalInfoView.isHidden = !additionalInfoView.isHidden
        }
    }
    
    // Оброботка нажатия на showHideIntermediaryBankField Банк Посредник
    @objc func showHideIntermediaryImageTapped(sender: UITapGestureRecognizer) {
        if sender.state == .ended {
            BIKIntermediaryBankView.isHidden = !BIKIntermediaryBankView.isHidden
            accountPayThroughView.isHidden = !accountPayThroughView.isHidden
        }
    }
    
    // Проверка поля "Номер платежного поручения" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidEndPNumber(_ sender: Any) {
        PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
    }
    // Проверка поля "Название шаблона" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndtemplateName(_ sender: Any) {
        templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
    }
    // Проверка поля "Тариф" на наличие данных, после потери фокуса
    @objc func textFieldEditingTarif(_ sender: Any) {
        if TransferSummTextField.text != ""{
            let sum = Double(TransferSummTextField.text ?? "0.0")
            if self.selectedDeposit != nil{
                getCommission(accountNo: selectedDeposit.accountNo ?? "", currencyID: selectedDeposit.currencyID ?? 0, transferSum: sum ?? 0.0, expenseType: expenseTypeID)
            }
        }
        
    }
    // Проверка поля "Сумма" на наличие данных, после потери фокуса
    @objc func textFieldEditingDidChange(_ sender: Any) {
        TransferSummTextField.stateIfEmpty(view: viewTransferSummTextField, labelError: labelErrorTransferSumm)
        let textNonZeroFirst = TransferSummTextField.text
        let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
        let numberAsInt = Double(newString ?? "0")
        TransferSummTextField.text = "\(numberAsInt ?? 0)"
        if selectedDeposit != nil{
            if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                showAlertController(localizedText(key: "insufficient_account_balance"))
                TransferSummTextField.text = ""
            }
            let sum = Double(TransferSummTextField.text ?? "0.0")
            if self.selectedDeposit != nil{
                getCommission(accountNo: selectedDeposit.accountNo ?? "", currencyID: selectedDeposit.currencyID ?? 0, transferSum: sum ?? 0.0, expenseType: expenseTypeID)
            }
        }else{
            viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
            TransferSummTextField.text = ""
            self.commissionView.isHidden = true
            showAlertController(localizedText(key: "select_account_or_card"))
        }
        if TransferSummTextField.text == ""{
            self.commissionView.isHidden = true
            self.summCommissionSender.text = ""
            self.summCommissionSenderCurrency.text = ""
            self.summCommisionIntermediary.text = ""
            self.summCommisionIntermediaryCurrency.text = ""
            self.summCommissionReceiver.text = ""
            self.summCommissionReceiverCurrency.text = ""
            
        }
    }
    // Проверка поля "Код платежа" на наличие данных, после потери фокуса
    @objc func textFieldEditingEnd(_ sender: Any) {
        if refOpers.contains(where: { $0.value == PaymentCodeTextField.text }) {
            print("success")
            for selectedRefOpers in refOpers{
                if selectedRefOpers.value == PaymentCodeTextField.text{
                    self.selectedRefOpers = selectedRefOpers
                    self.PaymentCodeDescriptionLabel.text = selectedRefOpers.text
                }
            }
        } else {
            showAlertController(self.localizedText(key: "invalid_payment_code_entered"))
        }
        PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
    }
    // Проверка поля "Комментарии" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndPaymentComment(_ sender: Any) {
        if PaymentCommentTextField.text!.count > 1 {
            PaymentCommentTextField.stateIfSizeIncorrect(view: viewPaymentCodeTextField, maxTextSize: 140, labelError: labelErrorPaymentComment)
        } else {
            PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
        }
    }
    // Проверка поля "Имя получателя" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndReceiverFullName(_ sender: Any) {
        ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
    }
    // Проверка поля "Номер аккаунта получателя" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndReceiverAccountNo(_ sender: Any) {
        ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverAccountNo)
    }
    // Проверка поля "" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndAccountNoIntermediaryBank(_ sender: Any) {
        
    }
    // Проверка поля "БИК посредника" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndReceiverBIKIntermediaryBank(_ sender: Any) {
        if ReceiverBIKIntermediaryBankTextField.text == ""{
            viewIntermediaryBank.isHidden = true
            self.IntermediaryBankCountryLabel.text = ""
            self.IntermediaryBankNameLabel.text = ""
        }else{
            if let bic = ReceiverBIKIntermediaryBankTextField.text{
                getSwiftBikForIntermediary(bic: bic)
            }
        }
    }
    // Проверка поля "БИК получателя" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndReceiverBIKPay(_ sender: Any) {
        if ReceiverBIKPayThruTextField.text == ""{
            self.viewPayThru.isHidden = false
            self.ReceiverBIKPayThruTextField.stateIfEmpty(view: self.viewReceiverBIKTextField, labelError: self.labelErrorReceiverBIK)
            self.PayThruCountryLabel.text = ""
            self.PayThruCountryNameLabel.text = ""
        }else{
            if let bic = ReceiverBIKPayThruTextField.text{
                getSwiftBikForIPayThru(bic: bic)
            }
        }
        self.ReceiverBIKPayThruTextField.stateIfEmpty(view: self.viewReceiverBIKTextField, labelError: self.labelErrorReceiverBIK)
    }
    //    // Проверка поля "Номер аккаунта получателя банка"  на наличие данных, после потери фокуса
    //    @objc func textFieldEditingEndReceiverPayThruBank(_ sender: Any) {
    //        AccountNoPayThruBankTextField.stateIfEmpty(view: viewPayThruAccountNoTextField, labelError: labelErrorPaythruAccountNo)
    //    }
    // Проверка поля "код ВО" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndVOcode(_ sender: Any) {
        if voCodes.contains(where: { $0.value == VOcodeTextField.text }) {
            print("success")
            for voCode in voCodes{
                if voCode.value == VOcodeTextField.text{
                    self.voCode = voCode
                    self.VODescriptionLabel.text = voCode.text
                }
            }
        } else {
            showAlertController(self.localizedText(key: "invalid_payment_code_entered"))
        }
        VOcodeTextField.stateIfEmpty(view: viewVOcodeTextField, labelError: labelErrorVOcode)
    }
    
    // Переход по скринам с аккаунтами и передача данных
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Переход на скрин с депозитами
        if segue.identifier == "toSWIFTAccountsSegue"{
            let vc = segue.destination as! AccountsViewController
            for account in swiftAccounts{
                account.items = account.items?.filter{$0.balance != 0.0}
            }
            swiftAccounts = swiftAccounts.filter{$0.items?.count != 0}
            vc.accounts = swiftAccounts
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "write_off")
            vc.selectedDeposit = selectedDeposit
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        // Переход на скрин с RefOpers
        if segue.identifier == "toRefOpersSegue"{
            let vc = segue.destination as! RefOpersViewController
            vc.refOpers = refOpers
            vc.navigationTitle = localizedText(key: "payment_purpose_codes")
            vc.selectedRefOpers = selectedRefOpers
            vc.delegate = self
        }
        // Переход на скрин с кодами ВО
        if segue.identifier == "toVOCodeSegue"{
            let vc = segue.destination as! CodeVOViewController
            vc.voCodes = voCodes
            vc.navigationTitle = localizedText(key: "currency_codes")
            vc.voCode = voCode
            vc.delegate = self
        }
        
        // Переход на скрин подтверждения
        if segue.identifier == "toSubmitSwiftSegue"{
            let vc = segue.destination as! SubmitSwiftViewController
            vc.delegate = self
            
            // qweqwe: you might be adding ReceiverAddress and ReceiverCountry to this model
            self.swiftTransferModel = SwiftTransferModel(
                senderFullName: templateSwiftTransferModel.senderFullName,
                senderCustomerID: templateSwiftTransferModel.senderCustomerID,
                expenseType: self.expenseTypeID,
                currencyID: self.selectedDeposit.currencyID,
                accountNo: self.selectedDeposit.accountNo,
                transferSum: self.transferSum,
                detailsOfPayment: self.PaymentCommentTextField.text,
                codeVO: self.voCodeID ?? nil,
                intermediaryBank: self.intermediaryBank?.country ?? nil,
                intermediaryBankCountry: self.intermediaryBank?.countryCode ?? nil,
                intermediaryBankBic: self.ReceiverBIKIntermediaryBankTextField.text,
                intermediaryBankLoroAccountNo: self.AccountNoIntermediaryBankTextField.text ?? nil,
                paymentCode: self.PaymentCodeTextField.text,
                receiverBank: self.receiverBank.name,
                receiverBankCountryCode: self.receiverBank.countryCode,
                receiverBankBic: self.receiverBank.bic,
                fullNameReceiver: self.ReceiverFullNameTextField.text,
                receiverAccountNo: self.ReceiverAccountNoTextField.text,
                receiverBankLoroAccountNo: self.AccountNoPayThruBankTextField.text,
                numberPP: self.PNumberTextFields.text,
                datePP: convertDateFormatter(date: self.PaymentDatePPFull.text ?? ""),
                dateVal: convertDateFormatter(date: self.PaymentDateVal.text ?? ""),
                operationType: operationTypeID,
                type: self.type,
                senderBank: templateSwiftTransferModel.senderBank,
                senderBankCountry: templateSwiftTransferModel.senderBankCountry,
                senderBankBic: templateSwiftTransferModel.senderBankBic,
                senderBankLoroAccountNo: templateSwiftTransferModel.senderBankLoroAccountNo,
                operationID: templateSwiftTransferModel.operationID,
                isSchedule: false,
                isTemplate: isCreateTemplate,
                templateName: templateNameTextField.text ?? nil,
                templateDescription: "SWIFT перевод",
                scheduleID: 0,
                schedule: nil,
                additionalInfo: self.additionalInfo.text,
                receiverAddress: swiftTransferModel.receiverAddress,
                receiverCountryCode: swiftTransferModel.receiverCountryCode,
                isCommissionAgreementChecked: swiftTransferModel.isCommissionAgreementChecked,
                swiftDocuments: swiftDocuments
            )
            vc.commisions = commisions
            vc.swiftTransferModel = swiftTransferModel
            vc.operationID = operationID
            vc.operationTypeID = operationTypeID
            vc.selectedDeposit = selectedDeposit
            vc.paymentCodeDescription = PaymentCodeDescriptionLabel.text ?? ""
            vc.voDescription = VODescriptionLabel.text ?? ""
            
            
        }
    }
    
    // Получение данных о выбраном депозите
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
        viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
    }
    // Получение данных текущей операции
    func selectedRefOpers(refOpers: ReferenceItemModel) {
        selectedRefOpers = refOpers
        
    }
    // Получение кода валютной операции
    func selectedCodeVO(voCode: ReferenceItemModel) {
        self.voCode = voCode
    }
    // Получение данных для редактирования оперции
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool) {
        self.operationID = operationID
        self.isRepeatPay = isRepeatPay
        self.isRepeatOperation = isRepeatOperation
    }
    
    // кнопка транслитерации
    @IBAction func transliterationButton(_ sender: Any) {
        if ReceiverFullNameTextField.text != ""{
            getTransliteration(detailsOfPayment: "", fullNameReceiver: ReceiverFullNameTextField.text ?? "", lang: "ru")
        }
    }
    
    // кнопка транслитерации Номера счета IBAN
    @IBAction func translateIBANButton(_ sender: Any) {
        if ReceiverAccountNoTextField.text != ""{
            getTransliterationToIBAN(detailsOfPayment: "", fullNameReceiver: ReceiverAccountNoTextField.text ?? "", lang: "ru")
        }
    }
    // кнопка bik кодов банка посредника
    @IBAction func searchBikIntermediary(_ sender: Any) {
        if ReceiverBIKIntermediaryBankTextField.text != ""{
            getSwiftBikForIntermediary(bic: ReceiverBIKIntermediaryBankTextField.text ?? "")
        }
    }
    // кнопка bik кодов банка получателя
    @IBAction func searchBikPay(_ sender: Any) {
        if ReceiverBIKPayThruTextField.text != ""{
            getSwiftBikForIntermediary(bic: ReceiverBIKPayThruTextField.text ?? "")
        }
    }
    
    //qweqwe
    // кнопка для выбора документа из телефона
    @IBAction func attachDocumentButton(_ sender: Any) {
        self.showAlertSelectDocs()
    }
    
    // кнопка создания свифт операции
    @IBAction func createSwiftButton(_ sender: Any) {
        guard enableTransfer else { return }
        
        if (PaymentCommentTextField.text?.count ?? 0) > 140 {
            
            showAlertController("\"Назначение платежа\" должно содержать не более 140 символов")
            
            return
        }
        
        
        if viewVO.isHidden{
            if isCreateTemplate{ // qweqwe Check later where it has been assigned
                
                if  templateNameTextField.text != "" &&
                        selectedDeposit != nil &&
                        PNumberTextFields.text != "" &&
                        PaymentCodeTextField.text != "" &&
                        PaymentCommentTextField.text != "" &&
                        ReceiverFullNameTextField.text != "" &&
                        ReceiverAccountNoTextField.text != "" &&
                        ReceiverBIKPayThruTextField.text != ""
                {
                    getSwiftTemplate(template: "true", type: 1)
                    
                }else{
                    viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
                    PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
                    PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                    PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
                    PaymentCommentTextField.stateIfSizeIncorrect(view: viewPaymentCommentTextField, maxTextSize: 140, labelError: labelErrorPaymentComment)
                    ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
                    ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
                    ReceiverBIKPayThruTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }else if isEditTemplate{
                if  templateNameTextField.text != "" &&
                        selectedDeposit != nil &&
                        PNumberTextFields.text != "" &&
                        PaymentCodeTextField.text != "" &&
                        PaymentCommentTextField.text != "" &&
                        ReceiverFullNameTextField.text != "" &&
                        ReceiverAccountNoTextField.text != "" &&
                        ReceiverBIKPayThruTextField.text != ""
                {
                    getSwiftTemplate(template: "true", type: 1)
                    
                }else{
                    viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
                    PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
                    PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                    PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
                    ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
                    ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
                    ReceiverBIKPayThruTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }else{
                if  selectedDeposit != nil &&
                        PNumberTextFields.text != "" &&
                        TransferSummTextField.text != "" &&
                        TransferSummTextField.text != "0.0" &&
                        PaymentCodeTextField.text != "" &&
                        PaymentCommentTextField.text != "" &&
                        ReceiverFullNameTextField.text != "" &&
                        ReceiverAccountNoTextField.text != "" &&
                        ReceiverBIKPayThruTextField.text != ""
                {
                    getSwiftTemplate(template: "false", type: 1)
                    
                }
                else{
                    viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
                    PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
                    TransferSummTextField.stateIfEmpty(view: viewTransferSummTextField, labelError: labelErrorTransferSumm)
                    PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                    PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
                    PaymentCommentTextField.stateIfSizeIncorrect(view: viewPaymentCommentTextField, maxTextSize: 140, labelError: labelErrorPaymentComment)
                    ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
                    ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
                    ReceiverBIKPayThruTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
                
            }
        }else{
            if isCreateTemplate{
                
                if  templateNameTextField.text != "" &&
                        selectedDeposit != nil &&
                        PNumberTextFields.text != "" &&
                        PaymentCodeTextField.text != "" &&
                        PaymentCommentTextField.text != "" &&
                        ReceiverFullNameTextField.text != "" &&
                        ReceiverAccountNoTextField.text != "" &&
                        ReceiverBIKPayThruTextField.text != "" &&
                        VOcodeTextField.text != ""
                {
                    getSwiftTemplate(template: "false", type: 1)
                    
                }else{
                    viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
                    PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
                    PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                    PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
                    PaymentCommentTextField.stateIfSizeIncorrect(view: viewPaymentCommentTextField, maxTextSize: 140, labelError: labelErrorPaymentComment)
                    ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
                    ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
                    ReceiverBIKPayThruTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    VOcodeTextField.stateIfEmpty(view: viewVOcodeTextField, labelError: labelErrorVOcode)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }else if isEditTemplate{
                if  templateNameTextField.text != "" &&
                        selectedDeposit != nil &&
                        PNumberTextFields.text != "" &&
                        PaymentCodeTextField.text != "" &&
                        PaymentCommentTextField.text != "" &&
                        ReceiverFullNameTextField.text != "" &&
                        ReceiverAccountNoTextField.text != "" &&
                        ReceiverBIKPayThruTextField.text != ""
                {
                    
                    getSwiftTemplate(template: "true", type: 1)
                }else{
                    viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
                    PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
                    PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                    PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
                    PaymentCommentTextField.stateIfSizeIncorrect(view: viewPaymentCommentTextField, maxTextSize: 140, labelError: labelErrorPaymentComment)
                    ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
                    ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
                    ReceiverBIKPayThruTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                    templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                    
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }else{
                if  selectedDeposit != nil &&
                        PNumberTextFields.text != "" &&
                        TransferSummTextField.text != "" &&
                        TransferSummTextField.text != "0.0" &&
                        PaymentCodeTextField.text != "" &&
                        PaymentCommentTextField.text != "" &&
                        (PaymentCommentTextField.text?.count ?? 0) <= 140 &&
                        ReceiverFullNameTextField.text != "" &&
                        ReceiverAccountNoTextField.text != "" &&
                        ReceiverBIKPayThruTextField.text != "" &&
                        VOcodeTextField.text != ""
                {
                    getSwiftTemplate(template: "false", type: 1)
                    
                }else{
                    viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
                    PNumberTextFields.stateIfEmpty(view: viewPNumberTextFields, labelError: labelErrorPNumber)
                    TransferSummTextField.stateIfEmpty(view: viewTransferSummTextField, labelError: labelErrorTransferSumm)
                    PaymentCodeTextField.stateIfEmpty(view: viewPaymentCodeTextField, labelError: labelErrorPaymentCode)
                    PaymentCommentTextField.stateIfEmpty(view: viewPaymentCommentTextField, labelError: labelErrorPaymentComment)
                    PaymentCommentTextField.stateIfSizeIncorrect(view: viewPaymentCommentTextField, maxTextSize: 140, labelError: labelErrorPaymentComment)
                    ReceiverFullNameTextField.stateIfEmpty(view: viewReceiverFullNameTextField, labelError: labelErrorReceiverFullName)
                    ReceiverAccountNoTextField.stateIfEmpty(view: viewReceiverAccountNoTextField, labelError: labelErrorReceiverAccountNo)
                    ReceiverBIKPayThruTextField.stateIfEmpty(view: viewReceiverBIKTextField, labelError: labelErrorReceiverBIK)
                    VOcodeTextField.stateIfEmpty(view: viewVOcodeTextField, labelError: labelErrorVOcode)
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
        }
    }
}


extension SWIFTViewController: UITextFieldDelegate{
    // Лимит на количество вводимых символов в полях
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        
        if textField.isEqual(PaymentCodeTextField) {
            maxLength = 8
        } else if textField.isEqual(ReceiverAccountNoTextField) {
            maxLength = 30// Constants.ACCOUNTS_CHARACTERS_COUNT
        }
        else if textField.isEqual(TransferSummTextField) {
            maxLength = 10
        }
        else if textField.isEqual(PNumberTextFields) {
            maxLength = 10
        }
        else if textField.isEqual(PaymentCommentTextField) {
            maxLength = 200
        }
        else if textField.isEqual(ReceiverFullNameTextField) {
            maxLength = 100
        }
        else if textField.isEqual(AccountNoIntermediaryBankTextField) {
            maxLength = 30
        }
        else if textField.isEqual(ReceiverBIKIntermediaryBankTextField) {
            maxLength = 20
        }
        else if textField.isEqual(ReceiverBIKPayThruTextField) {
            maxLength = 20
        }
        else if textField.isEqual(AccountNoPayThruBankTextField) {
            maxLength = 20
        }
        else if textField.isEqual(VOcodeTextField) {
            maxLength = 20
        }
        else if textField.isEqual(templateNameTextField) {
            maxLength = 100
        }
        return newLength <= maxLength
        
    }
    
    
    //    переход на следкующее поле
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == templateNameTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    //    обработка даты
    func convertDateFormatter(date: String) -> String {
        let dateFormatter = DateFormatter()
        let dateFormatterTo = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"//this your string date format
        dateFormatterTo.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "language".localized)
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }
        let timeStamp = dateFormatterTo.string(from: convertedDate!)
        
        return timeStamp
    }
    
    // кнопка включения планировщика
    @IBAction func switchPlanner(_ sender: UISwitch) {
        if sender.isOn{
            viewPlanner.isHidden = false
            isSchedule = true
            StartDate = ScheduleHelper.shared.dateCurrentForServer()
            EndDate = ScheduleHelper.shared.dateCurrentForServer()
            dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
            dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
            
        }else{
            viewPlanner.isHidden = true
            isSchedule = false
        }
    }
}
// выбор периода планировщика
extension SWIFTViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //  установка дефолтных значений периода
    func pickerPeriodDate() {
        pickerView.delegate = self
        pickerViewDayOperation.delegate = self
        pickerTarif.delegate = self
        pickerViewWeekDay.delegate = self
        pickerCountry.delegate = self
        
        periodTextField.inputView = pickerView
        dayNumberTextField.inputView = pickerViewDayOperation
        weekDayTextField.inputView = pickerViewWeekDay
        TarifTextField.inputView = pickerTarif
        recipientCountryTextField.inputView = pickerCountry
        
        pickerView.selectRow(0, inComponent: 0, animated: true)
        pickerViewDayOperation.selectRow(0, inComponent: 0, animated: true)
        pickerViewWeekDay.selectRow(0, inComponent: 0, animated: true)
        pickerTarif.selectRow(0, inComponent: 0, animated: true)
        pickerCountry.selectRow(0, inComponent: 0, animated: true)
        
        periodTextField.text = periodList[0]
        dayNumberTextField.text = daysList[0]
        weekDayTextField.text = weekList[0]
        TarifTextField.text = "\(expenseTypeID) - \(String(describing: expenseTypes[0].text ?? ""))"
        viewDateBegin.isHidden = false
        viewDateFinish.isHidden = false
        viewWeekDay.isHidden = false
        viewDay.isHidden = true
        viewDateOperation.isHidden = true
        
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        //        let cancelButton = UIBarButtonItem(title: "Отмена", style: .plain, target: self, action: #selector(cancelTarifPicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneTarifPicker));
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        
        toolbar.setItems([spaceButton,doneButton], animated: false)
        
        TarifTextField.inputAccessoryView = toolbar
        
    }
    // обработка кнопки ГОТОВО
    @objc func doneTarifPicker(){
        if TransferSummTextField.text == "" || TransferSummTextField.text == "0"{
            self.view.endEditing(true)
        }else{
            let sum = Double(TransferSummTextField.text ?? "0.0")
            if self.selectedDeposit != nil{
                getCommission(accountNo: selectedDeposit.accountNo ?? "", currencyID: selectedDeposit.currencyID ?? 0, transferSum: sum ?? 0.0, expenseType: expenseTypeID)
            }
            self.view.endEditing(true)
        }
        
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelTarifPicker(){
        
        self.view.endEditing(true)
    }
    
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count = 0
        if pickerView == self.pickerView {
            count = periodList.count
        } else if pickerView == self.pickerViewDayOperation{
            count = daysList.count
        } else if pickerView == self.pickerViewWeekDay{
            count = weekList.count
        } else if pickerView == self.pickerTarif{
            if selectedDeposit != nil && selectedDeposit.currencyID == 840 {
                count = expenseTypes.count
            } else if selectedDeposit == nil || selectedDeposit.currencyID != 840 {
                count = 1
            }
        } else if pickerView == self.pickerCountry {
            if arrayOfCountries != nil && arrayOfCountries.count > 0 {
                count = arrayOfCountries.count
            } else {
                return 0
            }
        }
        
        return count
    }
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var list = ""
        if pickerView == self.pickerView {
            list = periodList[row]
        } else if pickerView == self.pickerViewDayOperation{
            list = daysList[row]
        } else if pickerView == self.pickerViewWeekDay{
            list = weekList[row]
        } else if pickerView == self.pickerTarif{
            if selectedDeposit != nil && selectedDeposit.currencyID == 840 {
                list = "\(String(describing: expenseTypes[row].value ?? 0)) - \(String(describing: expenseTypes[row].text ?? ""))"
            } else if selectedDeposit == nil || selectedDeposit.currencyID != 840 {
                list = "\(String(describing: expenseTypes[0].value ?? 0)) - \(String(describing: expenseTypes[0].text ?? ""))"
            }
        } else if pickerView == self.pickerCountry{
            list = arrayOfCountries[row].text ?? ""
        }
        
        return list
    }
    // скрытие ненужных полей
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.pickerView {
            periodTextField.text = periodList[row]
            let formatterToServer = DateFormatter()
            formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            formatterToServer.locale = Locale(identifier: "language".localized)
            let dateNow = Date()
            switch row {
                
            case SchedulePickerView.EachWeek.rawValue:
                viewDateBegin.isHidden = false
                viewDateFinish.isHidden = false
                viewWeekDay.isHidden = false
                viewDay.isHidden = true
                viewDateOperation.isHidden = true
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = formatterToServer.string(from: dateNow )
                RecurringTypeID = ScheduleRepeat.EachWeek.rawValue
                ProcessDay = 1
                dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
                dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
                
            case SchedulePickerView.EachMonth.rawValue:
                viewDateBegin.isHidden = false
                viewDateFinish.isHidden = false
                viewWeekDay.isHidden = true
                viewDay.isHidden = false
                viewDateOperation.isHidden = true
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = formatterToServer.string(from: dateNow )
                RecurringTypeID = ScheduleRepeat.EachMonth.rawValue
                ProcessDay = 1
                dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
                dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
                
            case SchedulePickerView.OneTime.rawValue:
                viewDateBegin.isHidden = true
                viewDateFinish.isHidden = true
                viewWeekDay.isHidden = true
                viewDay.isHidden = true
                viewDateOperation.isHidden = false
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = nil
                RecurringTypeID = ScheduleRepeat.OneTime.rawValue
                ProcessDay = 0
                dateOperationTextField.text = ScheduleHelper.shared.dateSchedule()
                
            default:
                viewDateBegin.isHidden = true
                viewDateFinish.isHidden = true
                viewWeekDay.isHidden = true
                viewDay.isHidden = true
                viewDateOperation.isHidden = true
                
            }
        } else if pickerView == self.pickerViewDayOperation{
            dayNumberTextField.text = daysList[row]
            ProcessDay =  row + 1
        } else if pickerView == self.pickerViewWeekDay{
            weekDayTextField.text = weekList[row]
            ProcessDay =  row + 1
        } else if pickerView == self.pickerTarif{
            expenseTypeID = expenseTypes[row].value ?? 1
            TarifTextField.text = "\(String(describing: expenseTypes[row].value ?? 0)) - \(String(describing: expenseTypes[row].text ?? ""))"
            
            if expenseTypeID == 4 {
                guaranteedPaymentHint.isHidden = false
            } else {
                guaranteedPaymentHint.isHidden = true
            }
            
        } else if pickerView == self.pickerCountry{
            recipientCountryTextField.text = arrayOfCountries[row].text ?? ""
            countryCode = arrayOfCountries[row].value ?? ""
        }
        
        
    }
    // выборка даты
    func showDatePicker(){
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.minimumDate = Date()
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        PaymentDateVal.inputAccessoryView = toolbar
        PaymentDateVal.inputView = datePicker
        dateBeginTextField.inputAccessoryView = toolbar
        dateBeginTextField.inputView = datePicker
        dateFinishTextField.inputAccessoryView = toolbar
        dateFinishTextField.inputView = datePicker
        dateOperationTextField.inputAccessoryView = toolbar
        dateOperationTextField.inputView = datePicker
        
    }
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"///////////change date format
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"///////////change date format
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        
        if dateBeginTextField.isFirstResponder {
            dateBeginTextField.text = formatter.string(from: datePicker.date)
            StartDate = formatterToServer.string(from: datePicker.date)
        }
        if dateFinishTextField.isFirstResponder {
            dateFinishTextField.text = formatter.string(from: datePicker.date)
            EndDate = formatterToServer.string(from: datePicker.date)
        }
        if dateOperationTextField.isFirstResponder {
            dateOperationTextField.text = formatter.string(from: datePicker.date)
        }
        if PaymentDateVal.isFirstResponder {
            PaymentDateVal.text = formatter.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    // проверка на фокус поля ввода
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dateBeginTextField {
            datePicker.datePickerMode = .date
        }
        if textField == dateFinishTextField {
            datePicker.datePickerMode = .date
        }
        if textField == dateOperationTextField {
            datePicker.datePickerMode = .date
        }
    }
}


extension SWIFTViewController{
    // Получение SwiftAccounts
    func  getSwiftAccounts(){
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        
        let  swiftAccountsObservabel =  managerApi
            .getAccountSwift()
        
        showLoading()
        Observable
            .zip(currenciesObservable, swiftAccountsObservabel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (currencies, swiftAccounts) in
                    guard let `self` = self else { return }
                    self.swiftAccounts = swiftAccounts
                    self.swiftAccounts = self.checkAccountsForZeroBalance(accounts: self.swiftAccounts)
                    self.dateCurrent(textField: self.PaymentDateVal, stringDate: self.currentDateForServer())
                    self.dateCurrent(textField: self.PaymentDatePPFull, stringDate: self.currentDateForServer())
                    if self.isEditTemplate || self.isRepeatPay {
                        if self.isRepeatFromStory{
                            self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.swiftTransferModel.accountNo ?? "", currency: self.swiftTransferModel.currencyID ?? 0, accounts: self.swiftAccounts)
                            if self.selectedDeposit != nil{
                                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit )
                            }
                        }else{
                            self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.template.from ?? "", currency: self.template.currencyID ?? 0, accounts: self.swiftAccounts)
                            if self.selectedDeposit != nil{
                                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit )
                            }
                        }
                    }
                    if self.isCreateTemplateFromStory{
                        if self.swiftTransferModel != nil{
                            self.expenseTypeID = self.swiftTransferModel.expenseType ?? 0
                            self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.swiftTransferModel.accountNo ?? "", currency: self.swiftTransferModel.currencyID ?? 0, accounts: self.swiftAccounts)
                            if self.selectedDeposit != nil{
                                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit )
                            }
                        }
                    }
                    
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    func currentDateForServer() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormatter.locale = Locale(identifier: "language".localized)
        let convertedDate = dateFormatter.string(from: Date())
        
        return convertedDate
    }
    
    //    обработка даты
    func dateCurrent(textField: UITextField, stringDate: String){
        
        let dateFormater = DateFormatter()
        let dateFormater1 = DateFormatter()
        let dateFormater2 = DateFormatter()
        
        dateFormater1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormater1.locale = Locale(identifier: "language".localized)
        
        dateFormater2.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        dateFormater.dateFormat = "dd MMMM yyyy"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        if let newDate: Date = dateFormater1.date(from:stringDate){
            
            textField.text = dateFormater.string(from: (newDate))
        }
        if let newDate: Date = dateFormater2.date(from:stringDate){
            
            textField.text = dateFormater.string(from: (newDate))
        }
    }
}


extension SWIFTViewController {
    // Расчет коммиссии
    func getCommission(accountNo: String, currencyID: Int, transferSum: Double, expenseType: Int){
        showLoadingWithStatus(message: self.localizedText(key: "commission_calculation"))
        managerApi
            .getSwiftCommission(accountNo: accountNo, currencyID: currencyID, transferSum: transferSum, expenseType: expenseType)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (commisions) in
                    guard let `self` = self else { return }
                    
                    self.commisions = self.commissionWithCurrencies(currencies: self.currencies, commissions: commisions)
                    self.commissionView.isHidden = false
                    let sum = Double(self.TransferSummTextField.text ?? "0.0")
                    
                    self.commissionHelper(
                        commissions: self.commisions,
                        currencyID: self.selectedDeposit.currencyID ?? 0,
                        sum: sum ?? 0.0,
                        summCommissionSender: self.summCommissionSender,
                        summCommissionSenderCurrency: self.summCommissionSenderCurrency,
                        summCommissionSenderStackView: self.summCommissionSenderStackView,
                        summCommisionIntermediary: self.summCommisionIntermediary,
                        summCommisionIntermediaryCurrency: self.summCommisionIntermediaryCurrency,
                        summCommisionIntermediaryStackView: self.summCommisionIntermediaryStackView,
                        summCommissionReceiver: self.summCommissionReceiver,
                        summCommissionReceiverCurrency: self.summCommissionReceiverCurrency,
                        summCommissionReceiverStackView: self.summCommissionReceiverStackView )
                    
                    self.tableView.reloadData()
                    
                    self.hideLoading()
                    
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    //Добавление вида валюты в коммиссию
    func commissionWithCurrencies(currencies: [Currency], commissions: [SwiftCommissionModel]) -> [SwiftCommissionModel]{
        
        for commission in commissions{
            for currency in currencies{
                if commission.currencyID == currency.currencyID{
                    commission.currency = currency
                }
            }
        }
        return commissions
    }
    
    func fullSelectedDeposit(selectedDeposit: Deposit){
        let symbolString = " " + (selectedDeposit.currencySymbol ?? "")
        
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        //        payFrom_accountsLabel.text = fullAccountsText
        choiceCard_accountsLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currencySymbol ?? ""))"
        
        ccurrencySymbolLabel.text = symbolString
        viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorFromAccount)
    }
    
    //Проверка БИК-кода Банка посредника
    func getSwiftBikForIntermediary(bic: String){
        showLoadingWithStatus(message: self.localizedText(key: "check_nir_code"))
        managerApi
            .getSwiftBik(bic: bic)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](bik) in
                    guard let `self` = self else { return }
                    self.viewIntermediaryBank.isHidden = false
                    self.IntermediaryBankCountryLabel.text = bik.country
                    self.IntermediaryBankNameLabel.text = bik.name
                    self.intermediaryBank = bik
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    //Проверка БИК-кода Банка получателя
    func getSwiftBikForIPayThru(bic: String){
        showLoadingWithStatus(message: self.localizedText(key: "check_nir_code"))
        managerApi
            .getSwiftBik(bic: bic)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](bik) in
                    guard let `self` = self else { return }
                    self.viewPayThru.isHidden = false
                    self.ReceiverBIKPayThruTextField.stateIfEmpty(view: self.viewReceiverBIKTextField, labelError: self.labelErrorReceiverBIK)
                    self.PayThruCountryLabel.text = bik.country
                    self.PayThruCountryNameLabel.text = bik.name
                    self.receiverBank = bik
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.viewPayThru.isHidden = false
                    self?.PayThruCountryLabel.text = ""
                    self?.PayThruCountryNameLabel.text = ""
                    self?.ReceiverBIKPayThruTextField.text = ""
                    self?.ReceiverBIKPayThruTextField.becomeFirstResponder()
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    //Создание  Swift перевода
    func CreateSwiftTransaction(swiftTransferModel: SwiftTransferModel){
        showLoading()
        managerApi
            .postSwiftCreate(SwiftTransferModel: swiftTransferModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                    guard let `self` = self else { return }
                    if self.isCreateTemplate || self.isEditTemplate{
                        print("Шаблон создан")
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                        self.view.window?.rootViewController = vc
                        
                        let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                        let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                        let navigationController = UINavigationController(rootViewController: destinationController)
                        vc.pushFrontViewController(navigationController, animated: true)
                        
                    } else if(!AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed")) {
                        self.informClient()
                    } else {
                        if operation.operationID != nil {
                            let message = operation.message?.trimmingCharacters(in: .whitespacesAndNewlines)
                            if(message != nil || message != "")
                            {
                                self.informClient(message: operation.message!)
                            }
                            self.operationID = operation.operationID ?? 0
                        }
                        self.allowOperation()
                    }
                    
                    self.hideLoading()
                    
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    // infroming client operation completed
    func informClient() {

        let showAlert = UIAlertController(title: localizedText(key: "SaveSuccess"), message: nil, preferredStyle: .alert)
        showAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
            self.view.window?.rootViewController = vc

            let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
            let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
            let navigationController = UINavigationController(rootViewController: destinationController)
            vc.pushFrontViewController(navigationController, animated: true)

        }))
        self.present(showAlert, animated: true, completion: nil)
        
    }
    
    func informClient(message: String) {
        let showAlert = UIAlertController(title: "Создать перевод", message: nil, preferredStyle: .alert)
        showAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            self.allowOperation()
        }))
        self.present(showAlert, animated: true, completion: nil)
        
    }
    // Транслитерация
    func getTransliteration(detailsOfPayment: String, fullNameReceiver: String, lang: String ){
        showLoading()
        managerApi
            .getTransliteration(detailsOfPayment: detailsOfPayment, fullNameReceiver: fullNameReceiver, lang: lang)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (nameTransliteration) in
                    guard let `self` = self else { return }
                    self.ReceiverFullNameTextField.text = nameTransliteration.fullNameReceiver
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    // Транслитерация для IBAN
    func getTransliterationToIBAN(detailsOfPayment: String, fullNameReceiver: String, lang: String ){
        showLoading()
        managerApi
            .getTransliteration(detailsOfPayment: detailsOfPayment, fullNameReceiver: fullNameReceiver, lang: lang)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (nameTransliteration) in
                    guard let `self` = self else { return }
                    self.ReceiverAccountNoTextField.text = nameTransliteration.fullNameReceiver
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    //Получение шаблона
    func getSwiftTemplate(template: String, type: Int){
        showLoading()
        managerApi
            .getSwiftTemplate(template: template, type: type)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (template) in
                    guard let `self` = self else { return }
                    self.templateSwiftTransferModel = template
                    self.type = template.type ?? 1
                    if self.VOcodeTextField.text == ""{
                        self.voCodeID = template.codeVO
                    }else{
                        self.voCodeID = self.VOcodeTextField.text
                    }
                    
                    if let transferSum = Double(String(self.TransferSummTextField.text ?? "0.0")){
                        self.transferSum = transferSum
                    }
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: self.operationID, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    if self.isRepeatPay{
                        if self.isChangeData{
                            self.swiftTransferModel = SwiftTransferModel(
                                senderFullName: template.senderFullName,
                                senderCustomerID: template.senderCustomerID,
                                expenseType: self.expenseTypeID,
                                currencyID: self.selectedDeposit.currencyID,
                                accountNo: self.selectedDeposit.accountNo,
                                transferSum: self.transferSum,
                                detailsOfPayment: self.PaymentCommentTextField.text,
                                codeVO: self.voCodeID ?? nil,
                                intermediaryBank: self.intermediaryBank?.country ?? nil,
                                intermediaryBankCountry: self.intermediaryBank?.countryCode ?? nil,
                                intermediaryBankBic: self.intermediaryBank?.bic ?? nil,
                                intermediaryBankLoroAccountNo: self.AccountNoIntermediaryBankTextField.text ?? nil,
                                paymentCode: self.PaymentCodeTextField.text,
                                receiverBank: self.receiverBank.name,
                                receiverBankCountryCode: self.receiverBank.countryCode,
                                receiverBankBic: self.receiverBank.bic,
                                fullNameReceiver: self.ReceiverFullNameTextField.text,
                                receiverAccountNo: self.ReceiverAccountNoTextField.text,
                                receiverBankLoroAccountNo: self.AccountNoPayThruBankTextField.text,
                                numberPP: self.PNumberTextFields.text,
                                datePP: self.convertDateFormatter(date: self.PaymentDatePPFull.text ?? ""),
                                dateVal: self.convertDateFormatter(date: self.PaymentDateVal.text ?? ""),
                                operationType: self.operationTypeID,
                                type: self.type,
                                senderBank: template.senderBank,
                                senderBankCountry: template.senderBankCountry,
                                senderBankBic: template.senderBankBic,
                                senderBankLoroAccountNo: template.senderBankLoroAccountNo,
                                operationID: self.swiftTransferModel.operationID,
                                isSchedule: false,
                                isTemplate: self.isCreateTemplate,
                                templateName: self.templateNameTextField.text ?? nil,
                                templateDescription: "SWIFT перевод",
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                additionalInfo: self.additionalInfo.text,
                                receiverAddress: self.addressTextField.text,
                                receiverCountryCode: self.countryCode,
                                isCommissionAgreementChecked: self.checkBox,
                                swiftDocuments: self.swiftDocuments
                            )
                        }else{
                            self.swiftTransferModel = SwiftTransferModel(
                                senderFullName: template.senderFullName,
                                senderCustomerID: template.senderCustomerID,
                                expenseType: self.expenseTypeID,
                                currencyID: self.selectedDeposit.currencyID,
                                accountNo: self.selectedDeposit.accountNo,
                                transferSum: self.transferSum,
                                detailsOfPayment: self.PaymentCommentTextField.text,
                                codeVO: self.voCodeID ?? nil,
                                intermediaryBank: self.intermediaryBank?.country ?? nil,
                                intermediaryBankCountry: self.intermediaryBank?.countryCode ?? nil,
                                intermediaryBankBic: self.intermediaryBank?.bic ?? nil,
                                intermediaryBankLoroAccountNo: self.AccountNoIntermediaryBankTextField.text ?? nil,
                                paymentCode: self.PaymentCodeTextField.text,
                                receiverBank: self.receiverBank.name,
                                receiverBankCountryCode: self.receiverBank.countryCode,
                                receiverBankBic: self.receiverBank.bic,
                                fullNameReceiver: self.ReceiverFullNameTextField.text,
                                receiverAccountNo: self.ReceiverAccountNoTextField.text,
                                receiverBankLoroAccountNo: self.AccountNoPayThruBankTextField.text,
                                numberPP: self.PNumberTextFields.text,
                                datePP: self.convertDateFormatter(date: self.PaymentDatePPFull.text ?? ""),
                                dateVal: self.convertDateFormatter(date: self.PaymentDateVal.text ?? ""),
                                operationType: self.operationTypeID,
                                type: self.type,
                                senderBank: template.senderBank,
                                senderBankCountry: template.senderBankCountry,
                                senderBankBic: template.senderBankBic,
                                senderBankLoroAccountNo: template.senderBankLoroAccountNo,
                                operationID: 0,
                                isSchedule: false,
                                isTemplate: self.isCreateTemplate,
                                templateName: self.templateNameTextField.text ?? nil,
                                templateDescription: "SWIFT перевод",
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                additionalInfo: self.additionalInfo.text,
                                receiverAddress: self.addressTextField.text,
                                receiverCountryCode: self.countryCode,
                                isCommissionAgreementChecked: self.checkBox,
                                swiftDocuments: self.swiftDocuments
                            )
                        }
                    }else if self.isCreateTemplate{
                        self.swiftTransferModel = SwiftTransferModel(
                            senderFullName: template.senderFullName,
                            senderCustomerID: template.senderCustomerID,
                            expenseType: self.expenseTypeID,
                            currencyID: self.selectedDeposit.currencyID,
                            accountNo: self.selectedDeposit.accountNo,
                            transferSum: self.transferSum,
                            detailsOfPayment: self.PaymentCommentTextField.text,
                            codeVO: self.voCodeID ?? nil,
                            intermediaryBank: self.intermediaryBank?.country ?? nil,
                            intermediaryBankCountry: self.intermediaryBank?.countryCode ?? nil,
                            intermediaryBankBic: self.intermediaryBank?.bic ?? nil,
                            intermediaryBankLoroAccountNo: self.AccountNoIntermediaryBankTextField.text ?? nil,
                            paymentCode: self.PaymentCodeTextField.text,
                            receiverBank: self.receiverBank.name,
                            receiverBankCountryCode: self.receiverBank.countryCode,
                            receiverBankBic: self.receiverBank.bic,
                            fullNameReceiver: self.ReceiverFullNameTextField.text,
                            receiverAccountNo: self.ReceiverAccountNoTextField.text,
                            receiverBankLoroAccountNo: self.AccountNoPayThruBankTextField.text,
                            numberPP: self.PNumberTextFields.text,
                            datePP: self.convertDateFormatter(date: self.PaymentDatePPFull.text ?? ""),
                            dateVal: self.convertDateFormatter(date: self.PaymentDateVal.text ?? ""),
                            operationType: self.operationTypeID,
                            type: self.type,
                            senderBank: template.senderBank,
                            senderBankCountry: template.senderBankCountry,
                            senderBankBic: template.senderBankBic,
                            senderBankLoroAccountNo: template.senderBankLoroAccountNo,
                            operationID: 0,
                            isSchedule: false,
                            isTemplate: self.isCreateTemplate,
                            templateName: self.templateNameTextField.text ?? nil,
                            templateDescription: "SWIFT перевод",
                            scheduleID: 0,
                            schedule: self.schedule ?? nil,
                            additionalInfo: self.additionalInfo.text,
                            receiverAddress: self.addressTextField.text,
                            receiverCountryCode: self.countryCode,
                            isCommissionAgreementChecked: self.checkBox,
                            swiftDocuments: self.swiftDocuments
                        )
                    }else if self.isEditTemplate{
                        
                        self.swiftTransferModel = SwiftTransferModel(
                            senderFullName: template.senderFullName,
                            senderCustomerID: template.senderCustomerID,
                            expenseType: self.expenseTypeID,
                            currencyID: self.selectedDeposit.currencyID,
                            accountNo: self.selectedDeposit.accountNo,
                            transferSum: self.transferSum,
                            detailsOfPayment: self.PaymentCommentTextField.text,
                            codeVO: self.voCodeID ?? nil,
                            intermediaryBank: self.intermediaryBank?.country ?? nil,
                            intermediaryBankCountry: self.intermediaryBank?.countryCode ?? nil,
                            intermediaryBankBic: self.intermediaryBank?.bic ?? nil,
                            intermediaryBankLoroAccountNo: self.AccountNoIntermediaryBankTextField.text ?? nil,
                            paymentCode: self.PaymentCodeTextField.text,
                            receiverBank: self.receiverBank.name,
                            receiverBankCountryCode: self.receiverBank.countryCode,
                            receiverBankBic: self.receiverBank.bic,
                            fullNameReceiver: self.ReceiverFullNameTextField.text,
                            receiverAccountNo: self.ReceiverAccountNoTextField.text,
                            receiverBankLoroAccountNo: self.AccountNoPayThruBankTextField.text,
                            numberPP: self.PNumberTextFields.text,
                            datePP: self.convertDateFormatter(date: self.PaymentDatePPFull.text ?? ""),
                            dateVal: self.convertDateFormatter(date: self.PaymentDateVal.text ?? ""),
                            operationType: self.operationTypeID,
                            type: self.type,
                            senderBank: template.senderBank,
                            senderBankCountry: template.senderBankCountry,
                            senderBankBic: template.senderBankBic,
                            senderBankLoroAccountNo: template.senderBankLoroAccountNo,
                            operationID: self.template.operationID,
                            isSchedule: false,
                            isTemplate: true,
                            templateName: self.templateNameTextField.text ?? nil,
                            templateDescription: "SWIFT перевод",
                            scheduleID: 0,
                            schedule: self.schedule ?? nil,
                            additionalInfo: self.additionalInfo.text,
                            receiverAddress: self.addressTextField.text,
                            receiverCountryCode: self.countryCode,
                            isCommissionAgreementChecked: self.checkBox,
                            swiftDocuments: self.swiftDocuments
                        )
                    }else{
                        self.swiftTransferModel = SwiftTransferModel(
                            senderFullName: template.senderFullName,
                            senderCustomerID: template.senderCustomerID,
                            expenseType: self.expenseTypeID,
                            currencyID: self.selectedDeposit.currencyID,
                            accountNo: self.selectedDeposit.accountNo,
                            transferSum: self.transferSum,
                            detailsOfPayment: self.PaymentCommentTextField.text,
                            codeVO: self.voCodeID ?? nil,
                            intermediaryBank: self.intermediaryBank?.country ?? nil,
                            intermediaryBankCountry: self.intermediaryBank?.countryCode ?? nil,
                            intermediaryBankBic: self.intermediaryBank?.bic ?? nil,
                            intermediaryBankLoroAccountNo: self.AccountNoIntermediaryBankTextField.text ?? nil,
                            paymentCode: self.PaymentCodeTextField.text,
                            receiverBank: self.receiverBank.name,
                            receiverBankCountryCode: self.receiverBank.countryCode,
                            receiverBankBic: self.receiverBank.bic,
                            fullNameReceiver: self.ReceiverFullNameTextField.text,
                            receiverAccountNo: self.ReceiverAccountNoTextField.text,
                            receiverBankLoroAccountNo: self.AccountNoPayThruBankTextField.text,
                            numberPP: self.PNumberTextFields.text,
                            datePP: self.convertDateFormatter(date: self.PaymentDatePPFull.text ?? ""),
                            dateVal: self.convertDateFormatter(date: self.PaymentDateVal.text ?? ""),
                            operationType: self.operationTypeID,
                            type: self.type,
                            senderBank: template.senderBank,
                            senderBankCountry: template.senderBankCountry,
                            senderBankBic: template.senderBankBic,
                            senderBankLoroAccountNo: template.senderBankLoroAccountNo,
                            operationID: 0,
                            isSchedule: false,
                            isTemplate: self.isCreateTemplate,
                            templateName: self.templateNameTextField.text ?? nil,
                            templateDescription: "SWIFT перевод",
                            scheduleID: 0,
                            schedule: self.schedule ?? nil,
                            additionalInfo: self.additionalInfo.text,
                            receiverAddress: self.addressTextField.text,
                            receiverCountryCode: self.countryCode,
                            isCommissionAgreementChecked: self.checkBox,
                            swiftDocuments: self.swiftDocuments
                        )
                    }
                    if self.isRepeatOperation{
                        if self.operationID != 0{
                            self.hideLoading()
                            self.allowOperation()
                        }else{
                            self.CreateSwiftTransaction(swiftTransferModel: self.swiftTransferModel)
                        }
                    }else{
                        self.CreateSwiftTransaction(swiftTransferModel: self.swiftTransferModel)
                    }
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    // Получение имени и номер платежного поручения
    func getNameAndNumberPPFromSwiftTemplate(template: String, type: Int){
        showLoading()
        managerApi
            .getSwiftTemplate(template: template, type: type)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (template) in
                    guard let `self` = self else { return }
                    
                    if template.senderFullName != nil && template.senderFullName != "" {
                        self.SenderFullNameLabel.text = template.senderFullName
                    }
                    
                    if template.numberPP != nil && template.numberPP != "" {
                        self.PNumberTextFields.text = template.numberPP
                    }
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    // Получение данных для создания нового перевода на основании существующего перевода
    func getSwiftExistTemplate(operationID: Int, template: String){
        showLoading()
        managerApi
            .getSwiftExistTemplate(operationID: operationID, template: template)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (template) in
                    guard let `self` = self else { return }
                    self.templateSwiftTransferModel = template
                    self.type = template.type ?? 1
                    if self.VOcodeTextField.text == ""{
                        self.voCodeID = template.codeVO
                    }else{
                        self.voCodeID = self.VOcodeTextField.text
                    }
                    
                    if let transferSum = Double(String(self.TransferSummTextField.text ?? "0.0")){
                        self.transferSum = transferSum
                    }
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: self.operationID, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    self.expenseTypeID = template.expenseType ?? 0
                    self.swiftTransferModel = SwiftTransferModel(
                        senderFullName: template.senderFullName,
                        senderCustomerID: template.senderCustomerID,
                        expenseType: template.expenseType ?? 1,
                        currencyID: template.currencyID,
                        accountNo: template.accountNo,
                        transferSum: template.transferSum,
                        detailsOfPayment: template.detailsOfPayment,
                        codeVO: template.codeVO ?? nil,
                        intermediaryBank: template.intermediaryBank ?? nil,
                        intermediaryBankCountry: template.intermediaryBankCountry ?? nil,
                        intermediaryBankBic: template.intermediaryBankBic ?? nil,
                        intermediaryBankLoroAccountNo: template.intermediaryBankLoroAccountNo ?? nil,
                        paymentCode: template.paymentCode,
                        receiverBank: template.receiverBank,
                        receiverBankCountryCode: template.receiverBankCountryCode,
                        receiverBankBic: template.receiverBankBic,
                        fullNameReceiver: template.fullNameReceiver,
                        receiverAccountNo: template.receiverBankLoroAccountNo,
                        receiverBankLoroAccountNo: template.receiverBankLoroAccountNo,
                        numberPP: template.numberPP,
                        datePP: template.datePP,
                        dateVal: template.dateVal,
                        operationType: template.operationType,
                        type: template.type,
                        senderBank: template.senderBank,
                        senderBankCountry: template.senderBankCountry,
                        senderBankBic: template.senderBankBic,
                        senderBankLoroAccountNo: template.senderBankLoroAccountNo,
                        operationID: self.operationID,
                        isSchedule: false,
                        isTemplate: self.isCreateTemplate,
                        templateName: template.templateName ?? nil,
                        templateDescription: "SWIFT перевод",
                        scheduleID: 0,
                        schedule: self.schedule ?? nil,
                        additionalInfo: self.additionalInfo.text,
                        receiverAddress: template.receiverAddress,
                        receiverCountryCode: template.receiverCountryCode,
                        isCommissionAgreementChecked: template.isCommissionAgreementChecked,
                        swiftDocuments: self.swiftDocuments
                    )
                    if template.paymentCode != ""{
                        for code in self.refOpers{
                            if template.paymentCode == code.value{
                                self.PaymentCodeDescriptionLabel.text = code.text
                            }
                        }
                    }
                    if template.codeVO != ""{
                        for code in self.voCodes{
                            if template.codeVO == code.value{
                                self.VODescriptionLabel.text = code.text
                            }
                        }
                    }
                    if template.expenseType != 0{
                        self.TarifTextField.text =  "\((template.expenseType ?? 1)) - \(String(describing: self.expenseTypes[(template.expenseType ?? 1) - 1].text ?? ""))"
                    }
                    self.SenderFullNameLabel.text = template.senderFullName
                    self.PNumberTextFields.text = template.numberPP
                    self.TransferSummTextField.text = "\(template.transferSum ?? 0)"
                    self.PaymentCodeTextField.text = template.paymentCode
                    
                    self.PaymentCommentTextField.text = template.detailsOfPayment
                    self.VOcodeTextField.text = template.codeVO
                    self.ReceiverFullNameTextField.text = template.fullNameReceiver
                    self.ReceiverAccountNoTextField.text = template.receiverAccountNo
                    self.ReceiverBIKIntermediaryBankTextField.text = template.intermediaryBankBic
                    self.IntermediaryBankCountryLabel.text = template.intermediaryBankCountry
                    self.IntermediaryBankNameLabel.text = template.intermediaryBank
                    self.AccountNoIntermediaryBankTextField.text = template.intermediaryBankLoroAccountNo
                    self.ReceiverBIKPayThruTextField.text = template.receiverBankBic
                    self.PayThruCountryNameLabel.text = template.intermediaryBankCountry
                    self.PayThruCountryLabel.text = template.intermediaryBank
                    self.AccountNoPayThruBankTextField.text = template.receiverBankLoroAccountNo
                    self.templateNameTextField.text = template.templateName
                    if template.intermediaryBankBic != nil{
                        self.viewIntermediaryBank.isHidden = false
                        self.getSwiftBikForIntermediary(bic: template.intermediaryBankBic ?? "")
                    }else{
                        self.viewIntermediaryBank.isHidden = true
                    }
                    
                    self.getSwiftBikForIPayThru(bic: template.receiverBankBic ?? "")
                    let sum = Double(template.transferSum ?? 0.0)
                    if self.selectedDeposit != nil{
                        self.getCommission(accountNo: template.accountNo ?? "", currencyID: template.currencyID ?? 0, transferSum: sum , expenseType: template.expenseType ?? 0)
                    }
                    self.operationTypeID = template.operationType ?? 6
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                })
            .disposed(by: disposeBag)
    }
    
    
}
// Создание таблицы списка коммиссий
extension SWIFTViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commisions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CommissionTableViewCell
        cell.commissionLabel.text = "\(String(describing: commisions[indexPath.row].commissionSumm ?? 0.0))"
        cell.descriptionCommissionLabel.text = commisions[indexPath.row].commissionName
        cell.commissionCurrencyLabel.text = commisions[indexPath.row].currency?.symbol
        
        return cell
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableHeight?.constant = self.tableView.contentSize.height
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
}
extension SWIFTViewController{
    // проверка для прав на создание и подтверждние
    func  allowToCreatAndSaveOperation(button: UIButton) {
        
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed") && AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "execute_operation"),for: .normal)
        }else if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed") && !AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "Resources_Common_Save"),for: .normal)
        }
    }
    
    //    проверка прав
    func  allowOperation() {
        
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed") && AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
            self.performSegue(withIdentifier: "toSubmitSwiftSegue", sender: self)
            
        }else if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed") && !AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
            self.view.window?.rootViewController = vc
            
            let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
            let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
            let navigationController = UINavigationController(rootViewController: destinationController)
            vc.pushFrontViewController(navigationController, animated: true)
            
        }
    }
}

// DocumentPicker
extension SWIFTViewController: UIDocumentPickerDelegate, UINavigationControllerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let myURL = urls.first else { return }
        
        guard myURL.startAccessingSecurityScopedResource() else { return }
        guard let data = try? Data(contentsOf: myURL) else { return }
        guard checkSize(data) else { return }
        
        self.attachFileToTransferModel(myURL, data)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        dismiss(animated: true, completion: nil)
    }
}
 
// PhotoPicker
extension SWIFTViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let myURL = info[.imageURL] as? URL else { return }
        guard let data = try? Data(contentsOf: myURL) else { return }
        guard checkSize(data) else { return }
        
        self.attachFileToTransferModel(myURL, data)
        picker.dismiss(animated: true)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
}

// Функции для работы с PhotoPicker && DocumentPicker
extension SWIFTViewController  {
    
    private func attachFileToTransferModel(_ myURL: URL, _ data: Data) {
        let fileName = myURL.lastPathComponent
        let base64 = data.bytes.toBase64()
        let mimeType = mimeTypeForPath(path: myURL.absoluteString)
        
        let swiftDocument = SwiftDocument(fileName: fileName,
                                          fileBody: base64,
                                          contentType: mimeType)
        
        let image = UIImageView()
        image.image = UIImage(data: data)
        
        pickedImagesStackView.isHidden = false
        pickedImagesStackView.addArrangedSubview(image)
        
        checkArrayDataSize(data, swiftDocument)
    }
    
    // Функция для определение MIME TYPE
    func mimeTypeForPath(path: String) -> String {
        let url = NSURL(fileURLWithPath: path)
        let pathExtension = url.pathExtension
        
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension! as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
    private func checkArrayDataSize(_ data: Data, _ doc: SwiftDocument){
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB]
        bcf.countStyle = .file
        
        swiftDocumentsData.append(data)
        print("\(swiftDocuments.count)")
        
        for i in swiftDocumentsData {
            
            let sizeAsString = bcf.string(fromByteCount: Int64(i.count)).dropLast(2)
            let convertedSize = (sizeAsString as NSString).doubleValue
            
            allSize.append(convertedSize)
            let d = allSize.reduce(0, +)
            
            if d > 20 {
                print("\(d)")
                
                if let _ = navigationController?.presentedViewController {
                    print("is already presenting \(String(describing: navigationController?.presentedViewController))")
                    self.dismiss(animated: true)
                }
                self.showAlertRestriction20mB()
                return
            } else {
                print("\(d)")
                self.swiftDocuments.append(doc)
                return
            }
        }
    }
    
    private func checkSize(_ data: Data) -> Bool {
        
        let bcf = ByteCountFormatter()
        bcf.allowedUnits = [.useMB]
        bcf.countStyle = .file
        
        let sizeAsString = bcf.string(fromByteCount: Int64(data.count)).dropLast(2)
        let convertedSize = (sizeAsString as NSString).doubleValue
        
        if convertedSize > 1.6 {
            print("\(convertedSize)")
            
            if let _ = navigationController?.presentedViewController {
                print("is already presenting \(String(describing: navigationController?.presentedViewController))")
                self.dismiss(animated: true)
            }
            
            self.showAlertRestriction()
            return false
        } else {
            return true
        }
    }
    
    private func showAlertRestriction() {
        let alertController = UIAlertController(title: "Размер не должен превышать 1.6 MB",
                                                message: "",
                                                preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showAlertRestriction20mB() {
        let alertController = UIAlertController(title: "Общий размер загружаемых файлов должен превышать 20 MB",
                                                message: "",
                                                preferredStyle: .alert)
        let cancel = UIAlertAction(title: "OK", style: .cancel)
        alertController.addAction(cancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showAlertSelectDocs() {
        let alertController = UIAlertController(title: "Выбрать тип",
                                                message: nil,
                                                preferredStyle: .alert)
        
        let actionPhoto = UIAlertAction(title: "Выбрать фото", style: .default) { (action) in self.showPhotoPicker() }
        let actionPDF = UIAlertAction(title: "Выбрать PDF", style: .default) { (action) in self.showDocumentPicker() }
        let cancel = UIAlertAction(title: "Отменить", style: .cancel)
        
        alertController.addAction(actionPhoto)
        alertController.addAction(actionPDF)
        alertController.addAction(cancel)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showDocumentPicker(){
        if #available(iOS 14.0, *) {
            let documentPickerController = UIDocumentPickerViewController(
                forOpeningContentTypes: [UTType.pdf, UTType.jpeg, UTType.heif, UTType.mp3])
            documentPickerController.delegate = self
            self.present(documentPickerController, animated: true, completion: nil)
        }
    }
    
    private func showPhotoPicker() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
    }
}
