//
//  RefOpersViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/2/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa


protocol SwiftRefOpersProtocol {

    func selectedRefOpers(refOpers: ReferenceItemModel)
}
// Класс для данных операций  RefOpers
class RefOpersViewController: BaseViewController {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    var refOpers = [ReferenceItemModel]()
    var isSearching = false
    var filteredArray = [ReferenceItemModel]()
    var operationID = 0
    var navigationTitle = ""
    var selectedRefOpers: ReferenceItemModel!
    
    var delegate: SwiftRefOpersProtocol?
    @IBOutlet weak var search: UISearchBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = navigationTitle
        emptyStateStack.isHidden = true
        tableView.separatorStyle = .singleLine
    }
    
}
// Создание таблицы RefOpers
extension RefOpersViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching{
            return filteredArray.count
        }
        return refOpers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RefOpersTableViewCell
        
        if isSearching{
            cell.codeLabel.text = filteredArray[indexPath.row].value
            cell.descriptionLabel.text =  filteredArray[indexPath.row].text
            if selectedRefOpers != nil{
                if filteredArray[indexPath.row].value == selectedRefOpers.value{
                    cell.isSelected = true
                }
            }
        }else{
            cell.codeLabel.text = refOpers[indexPath.row].value
            cell.descriptionLabel.text =  refOpers[indexPath.row].text
            if selectedRefOpers != nil{
                if refOpers[indexPath.row].value == selectedRefOpers.value{
                    cell.isSelected = true
                }
            }
        }
        return cell
    }
    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        
        if filteredArray.count != 0{
            let filteredCode = filteredArray[indexPath.row]
            delegate?.selectedRefOpers(refOpers: filteredCode)
        }else{
            let refOper = refOpers[indexPath.row]
            delegate?.selectedRefOpers(refOpers: refOper)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isSelected {
            cell.setSelected(true, animated: false)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
}
extension RefOpersViewController: UISearchBarDelegate{
    //  поиск по RefOpers
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            
            isSearching = false
            emptyStateStack.isHidden = true
            tableView.separatorStyle = .singleLine
            tableView.reloadData()
            
        }else{
            filteredArray = refOpers.filter({ (code: ReferenceItemModel) -> Bool in
                
                if searchText.isNumeric{
                    return code.value?.lowercased().contains(searchText.lowercased()) ?? true
                }else{
                    return code.text?.lowercased().contains(searchText.lowercased()) ?? true
                }
            })
            if filteredArray.isEmpty{
                self.emptyStateStack.isHidden = false
                self.tableView.separatorStyle = .none
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.emptyStateStack.isHidden = true
                self.tableView.separatorStyle = .singleLine
            }
            searchBar.setShowsCancelButton(true, animated: true)
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    //  отмена поиска по RefOpers
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false
        filteredArray.removeAll()
        search.text = ""
        emptyStateStack.isHidden = true
        tableView.separatorStyle = .singleLine
        tableView.reloadData()
        view.endEditing(true)
    }
}



//qweqwe NOTE THIS PLACE
