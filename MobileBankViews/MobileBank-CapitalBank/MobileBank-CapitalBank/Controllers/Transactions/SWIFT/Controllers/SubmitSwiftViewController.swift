//
//  SubmitSwiftViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/8/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper

protocol EditOperationProtocol {
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool)
}

class SubmitSwiftViewController: BaseViewController {
    
    
    @IBOutlet weak var operationNameLabel: UILabel!
    @IBOutlet weak var SenderFullNameLabel: UILabel!
    @IBOutlet weak var SenderAccountNoLabel: UILabel!
    @IBOutlet weak var PNumberLabel: UILabel!
    @IBOutlet weak var TransferSummLabel: UILabel!
    @IBOutlet weak var PaymentCodeLabel: UILabel!
    @IBOutlet weak var PaymentCodeDescriptionLabel: UILabel!
    @IBOutlet weak var TarifLabel: UILabel!
    @IBOutlet weak var PaymentCommentLabel: UILabel!
    @IBOutlet weak var VOcodeLabel: UILabel!
    @IBOutlet weak var VOcodeDescriptionLabel: UILabel!
    @IBOutlet weak var ReceiverFullNameLabel: UILabel!
    @IBOutlet weak var ReceiverAccountNoLabel: UILabel!
    @IBOutlet weak var IntermediaryBankBic: UILabel!
    
    @IBOutlet weak var IntermediaryBankCountryLabel: UILabel!
    @IBOutlet weak var IntermediaryBankNameLabel: UILabel!
    @IBOutlet weak var AccountNoIntermediaryBankLabel: UILabel!
    
    @IBOutlet weak var PayThruBic: UILabel!
    
    @IBOutlet weak var PayThruCountryLabel: UILabel!
    @IBOutlet weak var PayThruCountryNameLabel: UILabel!
    @IBOutlet weak var AccountNoPayThruBankLabel: UILabel!
    @IBOutlet weak var viewIntermediaryBank: UIView!
    @IBOutlet weak var tableHeight: NSLayoutConstraint!
    @IBOutlet weak var summCommissionSender: UILabel!
    @IBOutlet weak var summCommissionSenderCurrency: UILabel!
    @IBOutlet weak var summCommisionIntermediary: UILabel!
    @IBOutlet weak var summCommisionIntermediaryCurrency: UILabel!
    @IBOutlet weak var summCommissionReceiver: UILabel!
    @IBOutlet weak var summCommissionReceiverCurrency: UILabel!
    @IBOutlet weak var summCommissionSenderStackView: UIStackView!
    @IBOutlet weak var summCommisionIntermediaryStackView: UIStackView!
    @IBOutlet weak var summCommissionReceiverStackView: UIStackView!
    @IBOutlet weak var voCodeStackView: UIStackView!
    @IBOutlet weak var datePP: UILabel!
    @IBOutlet weak var dateVal: UILabel!

    
    @IBOutlet weak var receiverAddress: UILabel!
    @IBOutlet weak var receiverCountryCode: UILabel!
    
    var swiftTransferModel: SwiftTransferModel!
    var operationModel: OperationModel!
    var operationID = 0
    var code = "000000"
    var operationTypeID = 0
    var typeOperation = ""
    var commisions = [SwiftCommissionModel]()
    var expenseTypes: [expenseTypeModel]!
    var selectedDeposit: Deposit!
    var paymentCodeDescription = ""
    var voDescription = ""
    var isRepeatPay = true
    var isRepeatOperation = true
    var delegate: EditOperationProtocol?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isRepeatPay{
            delegate?.editOperationData(operationID: operationID, isRepeatPay: isRepeatPay, isRepeatOperation: isRepeatOperation)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        if swiftTransferModel.operationType == nil &&  {
//            
//        }
            
        

        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = localizedText(key: "confirmation")
        if let expenseTypes = UserDefaultsHelper.expenseTypes {
            self.expenseTypes = expenseTypes
        }
        
        if swiftTransferModel.intermediaryBankBic == "" || swiftTransferModel.intermediaryBankBic == nil {
            viewIntermediaryBank.isHidden = true
        }else{
            viewIntermediaryBank.isHidden = false
        }
        
        if swiftTransferModel.operationType == InternetBankingOperationType.SwiftTransfer.rawValue{
            typeOperation = localizedText(key: "swift_transfer")
        }
        
        if swiftTransferModel.codeVO == "" || swiftTransferModel.codeVO == nil {
            voCodeStackView.isHidden = true
        }else{
            voCodeStackView.isHidden = false
        }
        
        self.commissionHelper(commissions: commisions,
                              currencyID: swiftTransferModel.currencyID ?? 0,
                              sum: swiftTransferModel.transferSum ?? 0.0,
                              summCommissionSender: summCommissionSender,
                              summCommissionSenderCurrency: summCommissionSenderCurrency,
                              summCommissionSenderStackView: summCommissionSenderStackView,
                              summCommisionIntermediary: summCommisionIntermediary,
                              summCommisionIntermediaryCurrency: summCommisionIntermediaryCurrency,
                              summCommisionIntermediaryStackView: summCommisionIntermediaryStackView,
                              summCommissionReceiver: summCommissionReceiver,
                              summCommissionReceiverCurrency: summCommissionReceiverCurrency,
                              summCommissionReceiverStackView: summCommissionReceiverStackView )
        
        let transferSumm = "\(String(format:"%.2f",  swiftTransferModel.transferSum ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: swiftTransferModel.currencyID ?? 0))"
        operationNameLabel.text = typeOperation
        SenderFullNameLabel.text = self.swiftTransferModel.senderFullName
        SenderAccountNoLabel.text = swiftTransferModel.accountNo
        PNumberLabel.text = swiftTransferModel.numberPP
        TransferSummLabel.text = transferSumm
        PaymentCodeLabel.text = swiftTransferModel.paymentCode
        PaymentCodeDescriptionLabel.text = paymentCodeDescription
        TarifLabel.text = "\((self.swiftTransferModel.expenseType ?? 1)) - \(String(describing: self.expenseTypes[(self.swiftTransferModel.expenseType ?? 1) - 1].text ?? ""))"
        PaymentCommentLabel.text = swiftTransferModel.detailsOfPayment
        VOcodeLabel.text = swiftTransferModel.codeVO
        VOcodeDescriptionLabel.text = voDescription
        ReceiverFullNameLabel.text = swiftTransferModel.fullNameReceiver
        ReceiverAccountNoLabel.text = swiftTransferModel.receiverAccountNo
        IntermediaryBankBic.text = swiftTransferModel.intermediaryBankBic
        IntermediaryBankCountryLabel.text = swiftTransferModel.intermediaryBankCountry
        IntermediaryBankNameLabel.text = swiftTransferModel.intermediaryBank
        AccountNoIntermediaryBankLabel.text = swiftTransferModel.intermediaryBankLoroAccountNo
        PayThruBic.text = swiftTransferModel.receiverBankBic
        datePP.text = dateFormaterBankDay(date: swiftTransferModel.datePP!)
        dateVal.text = dateFormaterBankDay(date: swiftTransferModel.dateVal!)
        PayThruCountryLabel.text = swiftTransferModel.receiverBankCountryCode
        PayThruCountryNameLabel.text = swiftTransferModel.receiverBank
        AccountNoPayThruBankLabel.text = swiftTransferModel.receiverBankLoroAccountNo
        receiverAddress.text = swiftTransferModel.receiverAddress
        receiverCountryCode.text = swiftTransferModel.receiverCountryCode
        
    }
    // кнопка подтверждения оперции
    @IBAction func submitButton(_ sender: Any) {
    // запрос на подтверждение СМС кода
        isRepeatPay = false
        self.postIsRequireConfirmOperationCode(operationID: self.operationID)
        
    }
    // переход на скрин СМС
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSMSSegue"{
            let vc = segue.destination as! SMSForTransactionViewController
            vc.commisions = commisions
            vc.swiftTransferModel = swiftTransferModel
            vc.operationID = operationID
            vc.operationTypeID = operationTypeID
            vc.selectedDeposit = selectedDeposit
            vc.paymentCodeDescription = PaymentCodeDescriptionLabel.text ?? ""
            vc.voDescription = voDescription
            vc.operationModel = operationModel
            vc.delegate = self
            
        }
    }
    //Подтверждение платежа
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TemaplateSwiftViewController") as! TemaplateSwiftViewController
                    let navigationController = UINavigationController(rootViewController: vc)
                    vc.commisions = self.commisions
                    vc.swiftTransferModel = self.swiftTransferModel
                    vc.selectedDeposit = self.selectedDeposit
                    vc.paymentCodeDescription = self.paymentCodeDescription
                    vc.voDescription = self.voDescription
                    vc.navigationTitle = self.localizedText(key: "swift_transfer_no_hyphen")
                    vc.operationID = self.operationID
                    navigationController.modalPresentationStyle = .fullScreen
                    self.present(navigationController, animated: true, completion: nil)
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //Проверка на СМС код
    func postIsRequireConfirmOperationCode(operationID: Int){
        showLoading()
        managerApi
            .postIsRequireConfirmOperationCode(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] isConfirm in
                    guard let `self` = self else { return }
                    self.operationModel = OperationModel(operationID: operationID,
                                                         code: self.code,
                                                         operationTypeID: self.operationTypeID)
                    
                    if isConfirm == "true"{
                        self.sendSms(operationID: operationID)
                        self.hideLoading()
                    }
                    if isConfirm == "false"{
                       
                        self.postConfirmPay(operationModel: self.operationModel)
                       
                    }
                    
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //отправка смс
    func  sendSms(operationID: Int){ //for resend sms
        showLoading()
        managerApi
            .postSendConfirmOperationCode(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](sms) in
                    guard let `self` = self else { return }
                    
                    let alertController = UIAlertController(title: nil, message: sms, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.performSegue(withIdentifier: "toSMSSegue", sender: self)
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                    
                    self.hideLoading()
            },
                onError: {[weak self](error) in
                    
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    
}
extension SubmitSwiftViewController: OperationIDRepeatDelegate{
    //получение  operationID при редактировании операции
    func resendOperationID(Id: Int) {
        operationID = Id
    }
    
}
// Создание таблицы списка коммиссий
extension SubmitSwiftViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commisions.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SubmitSwiftTableViewCell
        cell.commissionLabel.text = "\(String(describing: commisions[indexPath.row].commissionSumm ?? 0.0))"
        cell.descriptionCommissionLabel.text = commisions[indexPath.row].commissionName
        cell.commissionCurrencyLabel.text = commisions[indexPath.row].currency?.symbol
        
        return cell
    }
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.tableHeight?.constant = self.tableView.contentSize.height
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.viewWillLayoutSubviews()
    }
    
    
}

