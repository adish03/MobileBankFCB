//
//  DetailSwiftViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/15/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift


class DetailSwiftViewController: BaseViewController, FilterSwiftPropertyDelegate {
   

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var createButton: UIButton!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    var navigationTitle = ""
    var dateFrom = ""
    var dateTo = ""
    var isFiltered = false
    var fetchingMore = false
    var totalItems = 0
    var pageSize = 0
    var pageCount = 0
    var page = 1
    var operationID = 0
    
    var isSearching = false
    var filteredArray = [TransfersSwiftModel]()
    var detailsOfSwift = [TransfersSwiftModel]()

    var operationState = "null"
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        barButtonCustomize()
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsViewAllowed"){
            if isFiltered{
                
                detailsOfSwift.removeAll()
                getSwiftTransfers( startDate: dateFrom,
                                   endDate: dateTo,
                                   status: operationState,
                                   text: nil,
                                   PageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                   Page: self.page,
                                   TotalItems: nil)
            }else{
                detailsOfSwift.removeAll()
                getSwiftTransfers( startDate: dateFrom,
                                   endDate: dateTo,
                                   status: nil,
                                   text: nil,
                                   PageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                   Page: self.page,
                                   TotalItems: nil)
                
                tableView.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = navigationTitle
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        emptyStateStack.isHidden = true
        allowToCreatAndSaveOperation(button: createButton)
      
       
        if let bankDate = UserDefaultsHelper.bankDate{
            dateTo = dateFormaterBankDay(date: bankDate)
        }
        
        let calendar = Calendar.current
        if let date = calendar.date(byAdding: .day, value: -7, to: stringToDateFormatterFiltered(date: dateTo)){
            
            dateFrom = dateToStringFormatter(date: date)
        }
        
        hideKeyboardWhenTappedAround()
        tableView.reloadData()
        
    }
  
    // проверка при скроллинге до конца страницы
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height{
            if !fetchingMore{
                fetchingMore = true
                page += 1
                
                if isFiltered{
                    print("total Items: \(totalItems) ---- payments.count: \(detailsOfSwift.count * 10)")
                    if !(totalItems < pageCount * pageSize){
                        getSwiftTransfers( startDate: dateFrom,
                                           endDate: dateTo,
                                           status: operationState,
                                           text: nil,
                                           PageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                           Page: self.page,
                                           TotalItems: nil)
                    }
                }else{
                    if !(totalItems < pageCount * pageSize){
                        self.activityIndicator.startAnimating()
                        self.tableView.tableFooterView?.isHidden = false
                        getSwiftTransfers( startDate: dateFrom,
                                           endDate: dateTo,
                                           status: nil,
                                           text: nil,
                                           PageSize: Constants.MAIN_PAGE_OPERATION_COUNT,
                                           Page: self.page,
                                           TotalItems: nil)
                        
                    }
                }
            }
        }
    }
    @IBAction func creatSwiftButton(_ sender: Any) {
        performSegue(withIdentifier: "toCreateSwift", sender: self)
    }
    
    @IBAction func filterButton(_ sender: Any) {
    }
    
    //    обработка перехода по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toCreateSwift"{
            let vc = segue.destination as! SWIFTViewController
            vc.navigationTitle =  self.localizedText(key:"swift_transfer")
            vc.operationTypeID = InternetBankingOperationType.SwiftTransfer.rawValue
        }
        
        if segue.identifier == "toFilterSegue"{
            let vc = segue.destination as! FilterSwiftViewController
            
            vc.dateFrom = self.dateFrom
            vc.dateTo = self.dateTo
            vc.operationState = self.operationState
            vc.isNeedData = true
            vc.delegate = self
        }
        
    }
    
     //    установка значений при фильтрации
    func property(dateFrom: String?,
                  dateTo: String?,
                  operationState: String?,
                  isFilter: Bool?) {
        
        self.dateFrom = dateFrom ?? "null"
        self.dateTo = dateTo ?? "null"
        self.operationState = operationState ?? ""
        self.isFiltered = isFilter ?? true
    }
    
     //    установка значений при сбросе фильтрации
    func resetFilter(isFilter: Bool?) {
        
        self.isFiltered = isFilter ?? false
        self.operationState = ""
        self.page = 1
        
        detailsOfSwift.removeAll()
    }
    
    
    // получение  DetailSwift
    
    func  getSwiftTransfers(startDate: String?,
                            endDate: String?,
                            status: String?,
                            text: String?,
                            PageSize: Int,
                            Page: Int,
                            TotalItems: Int?) {
        showLoading()
        
        managerApi
            .getSwiftTransfers(startDate: startDate, endDate: endDate, status: status, text: text, PageSize: PageSize, Page: Page, TotalItems: TotalItems)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] transfers in
                    guard let `self` = self else { return }
                    self.detailsOfSwift.append(contentsOf:  transfers.result ?? [])
                    self.fetchingMore = false
                    self.totalItems = transfers.paginationInfo?.totalItems ?? 0
                    self.pageCount = transfers.paginationInfo?.page ?? 0
                    self.pageSize = transfers.paginationInfo?.pageSize ?? 0
                   
                    self.activityIndicator.stopAnimating()
                    
                    if self.detailsOfSwift.isEmpty{
                        self.tableView.isHidden = true
                        self.emptyStateStack.isHidden = false
                        if self.isFiltered{
                            self.emptyStateLabel.text = self.localizedText(key: "nothing_found_change_search_criteria")
                            self.emptyImage.image = UIImage(named: "empty_search_icn")
                        }
                        
                    }else{
                        self.tableView.isHidden = false
                        self.emptyStateStack.isHidden = true
                    }
                    
                    self.tableView.reloadData()
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
}
extension DetailSwiftViewController: UITableViewDataSource, UITableViewDelegate{
    
    
    // количество секций таблицы
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredArray.count
        }else{
            return detailsOfSwift.count
        }
    }
    
    // кастомизация ячейки
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! DetailSwiftTableViewCell
        
        
        if isSearching{
            let detail = filteredArray[indexPath.row]
            cell.numberPPLabel.text = detail.numberPP
            cell.datePPLabel.text = dateFormaterBankDay(date: detail.datePP ?? "")
            cell.stateLabel.text = detail.state
            cell.dateValLabel.text = dateFormaterBankDay(date: detail.dateVal ?? "")
            cell.currencyLabel.text = detail.currency
            cell.accountNoLabel.text = detail.accountNo
            cell.transferSummLabel.text = "\(String(format:"%.2f", detail.transferSum ?? 0)) \(detail.currency ?? "")"
            cell.FAButton.tag = indexPath.row
            cell.FAButton.addTarget(self, action: #selector(DetailClearingGrossViewController.oneTapped(_:)), for: .touchUpInside)
            cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action: #selector(DetailClearingGrossViewController.moreDetailTapped(_:)), for: .touchUpInside)
        }else{
            let detail = detailsOfSwift[indexPath.row]
            cell.numberPPLabel.text = detail.numberPP
            cell.datePPLabel.text = dateFormaterBankDay(date: detail.datePP ?? "")
            cell.stateLabel.text = detail.state
            cell.dateValLabel.text = dateFormaterBankDay(date: detail.dateVal ?? "")
            cell.currencyLabel.text = detail.currency
            cell.accountNoLabel.text = detail.accountNo
            cell.transferSummLabel.text = "\(String(format:"%.2f", detail.transferSum ?? 0)) \(detail.currency ?? "")"
            cell.FAButton.tag = indexPath.row
            cell.FAButton.addTarget(self, action: #selector(DetailClearingGrossViewController.oneTapped(_:)), for: .touchUpInside)
            cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action: #selector(DetailClearingGrossViewController.moreDetailTapped(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    
    // обработка нажатия кнопки на ячейке
    @objc func moreDetailTapped(_ sender: UIButton) {
        
        let buttonTag = sender.tag
        
        let object = detailsOfSwift[buttonTag]
        // qweqwe Figure out for what is this
        let swiftTransferModel = SwiftTransferModel(
            senderFullName: nil,
            senderCustomerID: nil,
            expenseType: nil,
            currencyID: object.currencyID,
            accountNo: object.accountNo,
            transferSum: object.transferSum,
            detailsOfPayment: object.detailsOfPayment,
            codeVO: nil,
            intermediaryBank: nil,
            intermediaryBankCountry: nil,
            intermediaryBankBic: nil,
            intermediaryBankLoroAccountNo: object.intermediaryBankLoroAccountNo,
            paymentCode: object.paymentCode,
            receiverBank: nil,
            receiverBankCountryCode: object.receiverBankCountryCode,
            receiverBankBic: object.receiverBankBic,
            fullNameReceiver: nil,
            receiverAccountNo: object.receiverAccountNo,
            receiverBankLoroAccountNo: object.receiverBankLoroAccountNo,
            numberPP: object.numberPP,
            datePP: object.datePP,
            dateVal: object.dateVal,
            operationType: nil,
            type: nil,
            senderBank: nil,
            senderBankCountry: nil,
            senderBankBic: nil,
            senderBankLoroAccountNo: nil,
            operationID: nil,
            isSchedule: nil,
            isTemplate: nil ,
            templateName: nil,
            templateDescription: nil,
            scheduleID: nil,
            schedule: nil,
            additionalInfo: nil,
            receiverAddress: nil,
            receiverCountryCode: nil,
            isCommissionAgreementChecked: nil,
            swiftDocuments: nil
        )
       
        
        let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TemaplateSwiftViewController") as! TemaplateSwiftViewController
        vc.swiftTransferModel = swiftTransferModel
        vc.isNeedOperationDetail = true
        vc.transferID = object.transferID ?? 0
        vc.isNeedOperationDetailByPayment = true
        vc.navigationTitle = localizedText(key: "details_of_operation")
        vc.operationTypeID = InternetBankingOperationType.SwiftTransfer.rawValue
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    // обработка нажатия кнопки на ячейке
    @objc func oneTapped(_ sender: UIButton) {
        
        let buttonTag = sender.tag
        
        let object = self.detailsOfSwift[buttonTag]
        
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
      
        let actionDoc = UIAlertAction(title: localizedText(key: "Documents"), style: .default) { (action) in
                        ApiHelper.shared.getInfoOperationFileSwift(transferID: object.transferID ?? 0, vc: self)
            
        }
        
        let actionHSaveTemplate  = UIAlertAction(title: self.localizedText(key: "save_as_template"), style: .default) { (action) in
            
        }
        
        let actionRepeatOperation = UIAlertAction(title: localizedText(key: "create_operation_request"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SWIFTViewController") as! SWIFTViewController
            
            // qweqwe Figure out for what is this
            let swiftTransferModel = SwiftTransferModel(
                senderFullName: nil,
                senderCustomerID: nil,
                expenseType: nil,
                currencyID: object.currencyID,
                accountNo: object.accountNo,
                transferSum: object.transferSum,
                detailsOfPayment: object.detailsOfPayment,
                codeVO: nil,
                intermediaryBank: nil,
                intermediaryBankCountry: nil,
                intermediaryBankBic: nil,
                intermediaryBankLoroAccountNo: object.intermediaryBankLoroAccountNo,
                paymentCode: object.paymentCode,
                receiverBank: nil,
                receiverBankCountryCode: object.receiverBankCountryCode,
                receiverBankBic: object.receiverBankBic,
                fullNameReceiver: nil,
                receiverAccountNo: object.receiverAccountNo,
                receiverBankLoroAccountNo: object.receiverBankLoroAccountNo,
                numberPP: object.numberPP,
                datePP: object.datePP,
                dateVal: object.dateVal,
                operationType: nil,
                type: nil,
                senderBank: nil,
                senderBankCountry: nil,
                senderBankBic: nil,
                senderBankLoroAccountNo: nil,
                operationID: nil,
                isSchedule: nil,
                isTemplate: nil ,
                templateName: nil,
                templateDescription: nil,
                scheduleID: nil,
                schedule: nil,
                additionalInfo: nil,
                receiverAddress: nil,
                receiverCountryCode: nil,
                isCommissionAgreementChecked: nil,
                swiftDocuments: nil)
            vc.swiftTransferModel = swiftTransferModel
            vc.isRepeatPay = true
            vc.isRepeatFromStory = true
            vc.navigationTitle = self.localizedText(key: "create_operation_request")
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        let actionCancel  = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel, handler: nil)
     
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsPrintDocumentsAllowed"){
            alertSheet.addAction(actionDoc)
        }
    
        alertSheet.addAction(actionCancel)
        
        present(alertSheet, animated: true, completion: nil)
        
    }
    
}

extension DetailSwiftViewController: UISearchBarDelegate{
    //поиск по истории платежей
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            
            isSearching = false
            if detailsOfSwift.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
                self.emptyImage.image = UIImage(named: "empty_icn_white")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
            tableView.reloadData()
            
        }else{
            
            filteredArray = detailsOfSwift.filter({ (swift: TransfersSwiftModel ) -> Bool in
                return swift.currency?.lowercased().contains(searchText.lowercased()) ?? true
            })
            if filteredArray.isEmpty{
                self.tableView.isHidden = true
                self.emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.tableView.isHidden = false
                self.emptyStateStack.isHidden = true
                fetchingMore = true
            }
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    // обработка кнопки search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        filteredArray.removeAll()
        isSearching = false
        searchBar.text = ""
        if detailsOfSwift.isEmpty{
            tableView.isHidden = true
            emptyStateStack.isHidden = false
            self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
            self.emptyImage.image = UIImage(named: "empty_icn_white")
        }else{
            tableView.isHidden = false
            emptyStateStack.isHidden = true
        }
        fetchingMore = false
        if self.isFiltered{
            if self.detailsOfSwift.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
        }
        tableView.reloadData()
        view.endEditing(true)
    }
    // кастомизация кнопки navigationabar
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        var image = ""
        if isFiltered{
            image = "selected"
            self.detailsOfSwift.removeAll()
        }else{
            image = "options"
            self.detailsOfSwift.removeAll()
        }
        button.setImage(UIImage(named: image), for: .normal)
        button.addTarget(self, action: #selector(filterButtonPressed), for: .touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = barButton
    }
    //    обработка кнопки фильтр
    @objc func filterButtonPressed() {
        performSegue(withIdentifier: "toFilterSegue", sender: self)
    }
    
    
}

extension DetailSwiftViewController{
    // проверка для прав на создание и подтверждние
    func  allowToCreatAndSaveOperation(button: UIButton) {
        
        if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed") && AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
            button.isEnabled = true
        }else if AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsAddAllowed") && !AllowHelper.shared.swiftAllowOpertaion(nameOperation: "Swift.SwiftSecurityOperations.IsApproveAllowed"){
            button.isEnabled = true
        }else{
            button.isEnabled = false
        }
        
        
    }
}

