//
//  CodeVOViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/5/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

protocol CodeVOProtocol {
    
    func selectedCodeVO(voCode: ReferenceItemModel)
}

// Класс для Кодов валютных операций

class CodeVOViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    @IBOutlet weak var search: UISearchBar!
    
    var navigationTitle = ""
    var isSearching = false
    var filteredArray = [ReferenceItemModel]()
    var voCodes: [ReferenceItemModel]!
    var voCode: ReferenceItemModel!
    var delegate: CodeVOProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = navigationTitle
        emptyStateStack.isHidden = true
        tableView.separatorStyle = .singleLine
        tableView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        tableView.reloadData()
    }
    

}
// Создание таблицы RefOpers
extension CodeVOViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredArray.count
        }
        return voCodes.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return  1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CodeVOTableViewCell
        
        if isSearching{
            cell.numberLabel.text = filteredArray[indexPath.row].value
            cell.descriptionLabel.text =  filteredArray[indexPath.row].text
            if voCode != nil{
                if filteredArray[indexPath.row].value == voCode.value{
                    cell.isSelected = true
                }
            }
        }else{
            cell.numberLabel.text = voCodes[indexPath.row].value
            cell.descriptionLabel.text = voCodes[indexPath.row].text
            if voCode != nil{
                if voCodes[indexPath.row].value == voCode.value{
                    cell.isSelected = true
                }
            }
        }
        return cell
    }
    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if filteredArray.count != 0{
            let filteredCode = filteredArray[indexPath.row]
            delegate?.selectedCodeVO(voCode: filteredCode)
        }else{
            let selectedCodeVO = voCodes[indexPath.row]
            delegate?.selectedCodeVO(voCode: selectedCodeVO)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isSelected {
            cell.setSelected(true, animated: false)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
}

extension CodeVOViewController: UISearchBarDelegate{
    //  поиск по RefOpers
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            
            isSearching = false
            emptyStateStack.isHidden = true
            tableView.separatorStyle = .singleLine
            tableView.reloadData()
            
        }else{
            filteredArray = voCodes.filter({ (code: ReferenceItemModel) -> Bool in
                
                if searchText.isNumeric{
                    return code.value?.lowercased().contains(searchText.lowercased()) ?? true
                }else{
                    return code.text?.lowercased().contains(searchText.lowercased()) ?? true
                }
            })
            if filteredArray.isEmpty{
                self.emptyStateStack.isHidden = false
                self.tableView.separatorStyle = .none
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.emptyStateStack.isHidden = true
                self.tableView.separatorStyle = .singleLine
            }
            searchBar.setShowsCancelButton(true, animated: true)
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    //  отмена поиска по RefOpers
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        isSearching = false
        filteredArray.removeAll()
        search.text = ""
        emptyStateStack.isHidden = true
        tableView.separatorStyle = .singleLine
        tableView.reloadData()
        view.endEditing(true)
    }
}
