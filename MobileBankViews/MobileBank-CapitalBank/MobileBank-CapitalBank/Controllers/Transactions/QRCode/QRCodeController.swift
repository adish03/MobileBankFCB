//
//  QRCodeController.swift
//  MobileBank-CapitalBank
//
//  Created by Bakai Ismailov on 4/11/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//
import Foundation
import UIKit
import SnapKit

class QRCodeController: BaseViewController {
    private let redColor = #colorLiteral(red: 0.7019607843, green: 0.03529411765, blue: 0.1921568627, alpha: 1)
    
    private let segmentView = UICustomSegmentView()
    private let accountSelectionView = UICustomSelectAccountView()
    
    private lazy var progressBar: UIProgressView = {
        let view = UIProgressView(progressViewStyle: .bar)
        view.trackTintColor = .lightGray
        view.progressTintColor = redColor
        return view
    }()
    
    private let selectAccountLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        view.textColor = .lightGray
        view.text = "Выберите счет для отображения QR кода, чтобы получить перевод"
        view.numberOfLines = 2
        view.textAlignment = .center
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        
        title = "QR Коды ваших счетов"
        
        let image = UIImage(named: "ArrowBack")
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .done, target: self, action: #selector(self.popViewController))
        
        view.addSubview(segmentView)
        segmentView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(100) //TODO: bottom to navigationItem
            make.left.right.equalToSuperview().inset(8)
        }
        
        view.addSubview(progressBar)
        progressBar.snp.makeConstraints { make in
            make.top.equalTo(segmentView.snp.bottom)
            make.height.equalTo(1.5)
            make.left.right.equalToSuperview()
        }
        
        view.addSubview(selectAccountLabel)
        selectAccountLabel.snp.makeConstraints { make in
            make.top.equalTo(progressBar.snp.bottom).offset(26)
            make.left.right.equalToSuperview().inset(24)
        }
        
        view.addSubview(accountSelectionView)
        accountSelectionView.snp.makeConstraints { make in
            make.top.equalTo(selectAccountLabel.snp.bottom).offset(26)
            make.left.right.equalToSuperview().inset(22)
        }
    }
    
    private func setupUI(){
        segmentView.addItem("Мои счета", color: redColor)
        segmentView.addItem("Сканировать", color: .lightGray)
        
        progressBar.setProgress(0.5, animated: false)
    }
    
    @objc func popViewController() {
        navigationController?.popViewController(animated: true)
    }
}
