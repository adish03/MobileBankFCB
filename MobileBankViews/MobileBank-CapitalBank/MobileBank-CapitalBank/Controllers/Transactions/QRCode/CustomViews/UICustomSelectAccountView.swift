//
//  UICustomSelectAccountView.swift
//  MobileBank-CapitalBank
//
//  Created by Bakai Ismailov on 4/11/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import UIKit
import SnapKit

class UICustomSelectAccountView: BaseView {
    
    private let accountNumberLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        view.text = "asdasdasd"
        return view
    }()
    
    private let accountMoneyAmountLabel: UILabel = {
        let view = UILabel()
        view.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        view.text = "asdasdasd"
        return view
    }()
    
    private let chevronImage: UIImageView = {
        let view = UIImageView()
        view.image = UIImage(named: "Fw")
        return view
    }()
    
    override func addSubViews() {
        addSubview(chevronImage)
        chevronImage.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-16)
            make.width.height.equalTo(32)
            make.centerY.equalToSuperview()
        }
        
        addSubview(accountNumberLabel)
        accountNumberLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(12)
            make.left.equalToSuperview().offset(16)
            make.right.equalTo(chevronImage.snp.left).offset(-10)
        }
        
        addSubview(accountMoneyAmountLabel)
        accountMoneyAmountLabel.snp.makeConstraints { make in
            make.top.equalTo(accountNumberLabel.snp.bottom).offset(6)
            make.left.equalToSuperview().offset(16)
            make.right.equalTo(chevronImage.snp.left).offset(-10)
            make.bottom.equalToSuperview().offset(-12)
        }
    }
     
    override func setupUI() {
        
    }
}
