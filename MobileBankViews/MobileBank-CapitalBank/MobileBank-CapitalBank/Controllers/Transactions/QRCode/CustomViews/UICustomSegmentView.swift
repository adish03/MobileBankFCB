//
//  UICustomSegmentView.swift
//  MobileBank-CapitalBank
//
//  Created by Bakai Ismailov on 4/11/22.
//  Copyright © 2022 FinanceSoft. All rights reserved.
//

import UIKit
import SnapKit

class UICustomSegmentView: BaseView {
    
    private let stackView: UIStackView = {
        let view = UIStackView()
        view.axis = .horizontal
        view.distribution = .fillEqually
        return view
    }()
    
    override func setupUI() {
        layer.cornerRadius = 7
        backgroundColor = .clear //TODO: Bakai change later
    }
    
    override func addSubViews() {
        addSubview(stackView)
        
        stackView.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.edges.equalToSuperview()
        }
    }
    
    func addItem(_  title: String, color: UIColor = .white) {
        
        let containerViewItem = UIView()
        let titleLabel: UILabel = {
            let view = UILabel()
            view.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
            view.textColor = color
            view.text = title
            view.textAlignment = .center
            return view
        }()
        
        stackView.addArrangedSubview(containerViewItem)
        
        containerViewItem.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
