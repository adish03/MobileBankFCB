//
//  SMSForTransactionViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/19/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift

// протокол повтора операции
protocol OperationIDRepeatDelegate {
    func resendOperationID(Id: Int)
}

class SMSForTransactionViewController: BaseViewController {
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var smsTextField: UITextField!
    @IBOutlet weak var timeSmsLabel: UILabel!
    @IBOutlet weak var smsSendButtonOutlet: UIButton!
    
    var clearingGrossOperationModel: ClearingGrossOperationModel?
    var submitModelInternalOperation: InternalOperationModel?
    var swiftTransferModel: SwiftTransferModel?
    var selectedDeposit: Deposit!
    var selectedDepositTo: Deposit?
    var operationID = 0
    var operationTypeID = 0
    var countdownTimer: Timer!
    var totalTime = 60
    var codeLength = 0
    var code = ""
    var currencyCurrent = ""
    var commisions = [SwiftCommissionModel]()
    var paymentCodeDescription = ""
    var voDescription = ""
    var goingForwards = false
    var operationModel: OperationModel!
    var delegate: OperationIDRepeatDelegate?
    var openAccountNo = ""



    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // проверка на наличие operationID
        if !goingForwards{
           delegate?.resendOperationID(Id: operationID)
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // закрытие клавиатуры
        hideKeyboardWhenTappedAround()
        
        navigationItem.title = localizedText(key: "verification_code")
        if let time = UserDefaultsHelper.SmsCodeAuthExpires {
            totalTime = time
            print("pinNumberCount: \(totalTime)")
        }
        if let Length = UserDefaultsHelper.EtokenCodeLength {
            codeLength = Length
            print("pinNumberCount: \(codeLength)")
        }
        
        smsTextField.defaultTextAttributes.updateValue(5.0, forKey: NSAttributedString.Key.kern)
        smsTextField.becomeFirstResponder()
        smsTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged) // make sure it is the desired textField
        
        startTimer()
        
        if let name = SessionManager.shared.user?.phone{
            phoneLabel.text = name
        }
        
    }
    
    // пересылка смс кода
    @IBAction func sendSmsButton(_ sender: Any) {
        resendSms()
        if let time = UserDefaultsHelper.SmsCodeAuthExpires {
            totalTime = time
        }
        startTimer()
    }
   
}
extension SMSForTransactionViewController{
    // проверка на количество символов смс
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.count  == codeLength{
            self.code = textField.text!
            self.operationModel = OperationModel(operationID: operationID,
                                                 code: self.code,
                                                 operationTypeID: self.operationTypeID)
            goingForwards = true
            
            postConfirmPay(operationModel: self.operationModel)
        }
    }
}


extension SMSForTransactionViewController{
    // таймер обратного отсчета
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        timeSmsLabel.isHidden = false
        smsSendButtonOutlet.isHidden = true

    }
    // таймер обратного отсчета
    @objc func updateTime() {
        timeSmsLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
        
    }
    // таймер обратного отсчета
    func endTimer() {
        timeSmsLabel.isHidden = true
        smsSendButtonOutlet.isHidden = false
        countdownTimer.invalidate()

        //send sms to server for confirm
        
    }
    // таймер обратного отсчета формат даты
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
       
        return LocalizeHelper.shared.addWord("\(String(format: "%02d", seconds))", localizedText(key: "send_sms_again_after"))
    }

    //Подтверждение платежа
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    if self.operationTypeID == InternetBankingOperationType.SwiftTransfer.rawValue{
                        let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "TemaplateSwiftViewController") as! TemaplateSwiftViewController
                        let navigationController = UINavigationController(rootViewController: vc)
                        vc.commisions = self.commisions
                        vc.swiftTransferModel = self.swiftTransferModel
                        vc.selectedDeposit = self.selectedDeposit
                        vc.paymentCodeDescription = self.paymentCodeDescription
                        vc.voDescription = self.voDescription
                        vc.navigationTitle = self.localizedText(key: "swift_transfer_no_hyphen")
                        navigationController.modalPresentationStyle = .fullScreen
                        self.present(navigationController, animated: true, completion: nil)
                    }else{
                        let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
                        let navigationController = UINavigationController(rootViewController: vc)
                        vc.clearingGrossOperationModel = self.clearingGrossOperationModel
                        vc.internalOperationModel = self.submitModelInternalOperation
                        vc.operationTypeID = self.operationTypeID
                        navigationController.modalPresentationStyle = .fullScreen
                        self.present(navigationController, animated: true, completion: nil)
                    }
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    var errorMessage = ""
                    errorMessage = error.localizedDescription
                    if (error.apiError?.statusCode) != nil{
                        if let statusCode = (error.apiError?.statusCode){
                            if statusCode >= 500{
                                errorMessage = error.apiError?.error_description ?? error.localizedDescription
                            }
                            if statusCode >= 400 && statusCode <= 499 {
                                errorMessage = error.apiError?.error_description ?? error.localizedDescription
                            }
                        }
                    }
                    self?.showAlertController(errorMessage)
                    self?.smsTextField.text = ""
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
