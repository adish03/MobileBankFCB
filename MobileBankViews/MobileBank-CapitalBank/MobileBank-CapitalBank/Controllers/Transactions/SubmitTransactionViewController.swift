//
//  SubmitTransactionViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/19/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper

protocol EditCliringOperationProtocol {
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool)
}
protocol EditTransactionsOperationProtocol {
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool, operationTypeID: Int)
}


class SubmitTransactionViewController: BaseViewController{

    var clearingGrossOperationModel: ClearingGrossOperationModel?
    var submitModelInternalOperation: InternalOperationModel?
    var operationModel: OperationModel!
    var selectedDeposit: Deposit!
    var selectedDepositTo: Deposit?
    var operationID = 0
    var code = "000000"
    var operationTypeID = 0
    var typeOperation = ""
    var currencyCurrent = ""
    var currencyCurrentID = 0
    var goingForwards = false
    var delegate: EditCliringOperationProtocol?
    var delegateTransactions: EditTransactionsOperationProtocol?
    var openAccountNo = ""
    var documents: [Documents]?
    
    @IBOutlet weak var operationNameLabel: UILabel!
    @IBOutlet weak var paymentDateStackView: UIStackView!
    @IBOutlet weak var pNumberStackView: UIStackView!
    @IBOutlet weak var paymentTypeIDStackView: UIStackView!
    @IBOutlet weak var senderFullNameStackView: UIStackView!
    @IBOutlet weak var receiverFullNameStackView: UIStackView!
    @IBOutlet weak var receiverBIKStackView: UIStackView!
    @IBOutlet weak var senderAccountNoStackView: UIStackView!
    @IBOutlet weak var receiverAccountNoStackView: UIStackView!
    @IBOutlet weak var paymentCommentStackView: UIStackView!
    @IBOutlet weak var paymentCodeStackView: UIStackView!
    @IBOutlet weak var transferSummStackView: UIStackView!
    
    @IBOutlet weak var paymentDescriptionLabel: UILabel!
    @IBOutlet weak var bicDescriptionLabel: UILabel!
    
    @IBOutlet weak var paymentDate: UILabel!
    @IBOutlet weak var pNumber: UILabel!
    @IBOutlet weak var paymentTypeID: UILabel!
    @IBOutlet weak var senderFullName: UILabel!
    @IBOutlet weak var receiverFullName: UILabel!
    @IBOutlet weak var receiverBIK: UILabel!
    @IBOutlet weak var senderAccountNo: UILabel!
    @IBOutlet weak var receiverAccountNo: UILabel!
    @IBOutlet weak var paymentComment: UILabel!
    @IBOutlet weak var paymentCode: UILabel!
    @IBOutlet weak var transferSumm: UILabel!
    
    @IBOutlet weak var paymentType: UILabel!
    @IBOutlet weak var DtAccountNo: UILabel!
    @IBOutlet weak var CtAccountNo: UILabel!
    @IBOutlet weak var ScheduleDate: UILabel!
    @IBOutlet weak var Sum: UILabel!
    @IBOutlet weak var Comment: UILabel!
    
    @IBOutlet weak var viewTransaction: UIView!
    @IBOutlet weak var viewCliringGross: UIView!
    
    @IBOutlet weak var nameSend: UILabel!
    
    var paymentsCodes: [PaymentCode]!
    var bikCodes: [BikCode]!
    
    var isRepeatPay = true
    var isRepeatOperation = true
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isRepeatPay{
            delegate?.editOperationData(operationID: operationID, isRepeatPay: isRepeatPay, isRepeatOperation: isRepeatOperation)
        }
        
    }
    
    // Добавляет экшны на алерт для загрузки документа
    private func addDownloadAction(_ item: Documents) -> UIAlertAction {
        
        let action = UIAlertAction(title: item.fileName, style: .default) { (action) in
            
            guard let decodedData = Data(base64Encoded: item.fileBody ?? "", options: .ignoreUnknownCharacters) else { return }
            var filesToShare = [Any]()
            
            filesToShare.append(ExcelFileHelper.shared.saveFileDirectory(docxFile: decodedData, item.fileName ?? "document.docx"))
            let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
            
            activityViewController.completionWithItemsHandler = { activity, completed, items, error in
                self.showAlertDownloadDocuments()
            }
        }
        return action
    }
    
    // Alert Controller для загрузки документа
    private func showAlertDownloadDocuments() {
        guard let documents = documents else { return }
        let alertController = UIAlertController(title: "Ссылка для отчета",
                                                message: "",
                                                preferredStyle: .alert)
        if documents.count > 0 {
            
            for i in documents { alertController.addAction(addDownloadAction(i)) }
            alertController.message = "Депозит успешно создан. Пройдите по ссылкам для ознакомления с договорами публичной оферты. Для завершения открытия депозита Вам необходимо пополнить депозит."
            let cancel = UIAlertAction(title: "Подтвердить", style: .cancel)
            alertController.addAction(cancel)
            
        } else {
            
            alertController.message = "Депозит успешно создан.\nК сожалению, документ для публичной оферты отсутствует. Для завершения открытия депозита Вам необходимо пополнить депозит."
            let confirm = UIAlertAction(title: "Подтвердить", style: .cancel)
            let cancel = UIAlertAction(title: "Отменить", style: .default)
            alertController.addAction(confirm)
            alertController.addAction(cancel)
            
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.showAlertDownloadDocuments()
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = localizedText(key: "confirmation")
        
        if let bikCodes = UserDefaultsHelper.bikCodes {
            self.bikCodes = bikCodes
        }
        if let paymentsCodes = UserDefaultsHelper.paymentsCodes {
            self.paymentsCodes = paymentsCodes
        }
        
        if clearingGrossOperationModel != nil{
            if clearingGrossOperationModel?.paymentTypeID == 1{
                paymentTypeID.text = localizedText(key: "clearing_payment")
                operationNameLabel.text =  localizedText(key: "clearing_payment")
            }else{
                paymentTypeID.text =  localizedText(key: "gross_payment")
                operationNameLabel.text =  localizedText(key: "gross_payment")
            }
            viewTransaction.isHidden = true
            
            senderFullName.text = SessionManager.shared.user?.userdisplayname//clearingGrossOperationModel.senderFullName
            paymentDate.text = ScheduleHelper.shared.dateFromServer(stringDate: clearingGrossOperationModel?.paymentDate ?? "")
            pNumber.text = clearingGrossOperationModel?.pNumber
            receiverFullName.text = clearingGrossOperationModel?.receiverFullName
            receiverBIK.text = clearingGrossOperationModel?.receiverBIK
            senderAccountNo.text = clearingGrossOperationModel?.senderAccountNo
            receiverAccountNo.text = clearingGrossOperationModel?.receiverAccountNo
            paymentComment.text = clearingGrossOperationModel?.paymentComment
            paymentCode.text = clearingGrossOperationModel?.paymentCode
            transferSumm.text = "\(String(format:"%.2f",  clearingGrossOperationModel?.transferSumm ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: currencyCurrentID))"
            for code in self.bikCodes{
                if code.code == self.clearingGrossOperationModel?.receiverBIK{
                    self.bicDescriptionLabel.text = code.name
                }
            }
            for code in self.paymentsCodes{
                if code.code == self.clearingGrossOperationModel?.paymentCode{
                    self.paymentDescriptionLabel.text = code.description
                }
            }
            
        }else{
            nameSend.text = SessionManager.shared.user?.username

            viewCliringGross.isHidden = true
            operationNameLabel.text = localizedText(key: "Resources_Internal_Customer_Transaction")
            paymentType.text = ValueHelper.shared.operationTypeDefine(operationTypeId: operationTypeID)
            DtAccountNo.text = submitModelInternalOperation?.dtAccountNo
            CtAccountNo.text = submitModelInternalOperation?.ctAccountNo
            if submitModelInternalOperation?.scheduleDate != nil{
                ScheduleDate.text = submitModelInternalOperation?.scheduleDate
            }else{
                ScheduleDate.text = self.dateCurrentOpendepostit()
            }
            Sum.text = "\(String(format:"%.2f", submitModelInternalOperation?.sum ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: submitModelInternalOperation?.currencyID ?? 0))"
            Comment.numberOfLines = 0
            Comment.text = submitModelInternalOperation?.comment

        }
    }
    //    обработка перехода по segue
      override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            if segue.identifier == "toSMSSegue"{
                let vc = segue.destination as! SMSForTransactionViewController
    
                vc.code = code
                vc.operationTypeID = operationTypeID
                vc.currencyCurrent = currencyCurrent
                vc.submitModelInternalOperation = submitModelInternalOperation
                vc.clearingGrossOperationModel = clearingGrossOperationModel
                vc.operationID = operationID
                vc.selectedDeposit = selectedDeposit
                vc.selectedDepositTo = selectedDepositTo
                
            }
    }
    //    обработка кнопки подтверждения
    @IBAction func submitButton(_ sender: Any) {
        isRepeatPay = false
        postIsRequireConfirmOperationCode(operationID: operationID)
        
    }
    // Подтверждение операции
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    let storyboard = UIStoryboard(name: "Transaction", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TemplateTransactionViewController") as! TemplateTransactionViewController
                    let navigationController = UINavigationController(rootViewController: vc)
                    vc.clearingGrossOperationModel = self.clearingGrossOperationModel
                    vc.internalOperationModel = self.submitModelInternalOperation
                    vc.selectedDeposit = self.selectedDeposit
                    vc.selectedDepositTo = self.selectedDepositTo
                    vc.currencyCurrentID = self.currencyCurrentID
                    vc.operationTypeID = self.operationTypeID
                   navigationController.modalPresentationStyle = .fullScreen
                    self.present(navigationController, animated: true, completion: nil)
        
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
// Необходим ли код подтверждения
    func postIsRequireConfirmOperationCode(operationID: Int){
        showLoading()
        managerApi
            .postIsRequireConfirmOperationCode(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] isConfirm in
                    guard let `self` = self else { return }
                    self.operationModel = OperationModel(operationID: operationID,
                                                         code: self.code,
                                                         operationTypeID: self.operationTypeID)
                    
                    if isConfirm == "true"{
                        self.sendSms(operationID: operationID)
                       
                        self.hideLoading()
                        
                    }
                    if isConfirm == "false"{
                        
                        self.postConfirmPay(operationModel: self.operationModel)
                    }
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //отправка смс
    func  sendSms(operationID: Int){ //for resend sms
        showLoading()
        managerApi
            .postSendConfirmOperationCode(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](sms) in
                    guard let `self` = self else { return }
                    
                    let alertController = UIAlertController(title: nil, message: sms, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "OK", style: .default) { (action) in
                       self.performSegue(withIdentifier: "toSMSSegue", sender: self)
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                    
                    self.hideLoading()
            },
                onError: {[weak self](error) in
                    
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
