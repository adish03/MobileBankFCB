//
//  DepositAccountsViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/19/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa


class DepositAccountsViewController: BaseViewController {

    @IBOutlet var mainView: UIView!
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var fromAccountTextField: UITextField!
    @IBOutlet weak var fromBalanceTextField: UITextField!
    @IBOutlet weak var labelFrom: UILabel!
    @IBOutlet weak var toAccountTextField: UITextField!
    @IBOutlet weak var toBalanceTextField: UITextField!
    @IBOutlet weak var toCurrencyLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var currencySumLabel: UILabel!
    
    //Conversion react
    @IBOutlet weak var firstCurrencyLabel: UILabel!
    @IBOutlet weak var secondCurrencyLabel: UILabel!
    @IBOutlet weak var firstCurrencyTextField: UITextField!
    @IBOutlet weak var secondCurrencyTextField: UITextField!
    var firstCurrencyId = 0
    var secondCurrencyId = 0
    var coefficient = 0.0
    var buyCurrency = 0.0
    var sellCurrency = 0.0
    
    
    var currencyRate = [CurrencyRate]()
    var currencyRateUpdate = [CurrencyRate]()
    
    var conversionOperationModel: ConversionOperationModel!
    var sumConversion = 0.0
    
    var depositsFrom: [Deposit]!
    var currencies = [Currency]()
    var currenciesWithRate = [Currency]()
    var depositsWithCurrencyFrom: [Deposit]!
    var depositsTo: [Deposit]!
    var selectedAccount: String?
    var selectedCurrency: String?
    var selectedBalance: String?
    var selectedAccountIndex: Int?
    var currentTxtField: UITextField!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super .viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        mainView.backgroundColor = Constants.BACKGROUND_COLOR
        if  menu != nil{
            menu.target = self.revealViewController()
            menu.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        fromAccountTextField.delegate = self
        toAccountTextField.delegate = self
        picker()
        getDeposit()
        
        
        firstCurrencyTextField.rx.text
            .map {  text -> String in
                let x = Double(text!) ?? 0.0
                let comission =  String(format:"%.2f", x * self.buyCurrency/self.sellCurrency) //"\(x * 0.05)"
                return Optional(comission) ?? "" }
            .bind(to: self.secondCurrencyTextField.rx.text)
            .disposed(by: disposeBag)
        
        secondCurrencyTextField.rx.text
            .map {  text -> String in
                let x = Double(text!) ?? 0.0
                let total = String(format:"%.2f", x * self.buyCurrency/self.sellCurrency) //"\(x * 0.05 + x)"
                return Optional(total) ?? "" }
            .bind(to: self.firstCurrencyTextField.rx.text)
            .disposed(by: disposeBag)
        
        
        let tapCloseKeyboard: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tapCloseKeyboard)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    @IBAction func convertButton(_ sender: Any) {
        
       
        if fromAccountTextField.text != "" && toAccountTextField.text != "" && firstCurrencyTextField.text != "" {
            let sell = SellAccount(accountNo: fromAccountTextField.text ?? " ", currencyID: Int(labelFrom.text ?? "0") ?? 0)
            let buy = BuyAccount(accountNo: toAccountTextField.text ?? " ", currencyID: Int(toCurrencyLabel.text ?? "0") ?? 0)
            if let sum = firstCurrencyTextField.text{
                self.sumConversion = Double(sum)!
                
            }
            conversionOperationModel = ConversionOperationModel(sellAccount: sell, buyAccount: buy, sum: sumConversion)
           
        }
        
        conversion(ConversionOperationModel: conversionOperationModel)
    }
    
}


extension DepositAccountsViewController{
    
    
    
    func  getDeposit(){
        
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        
        let depositObservable:Observable<[Accounts]> = managerApi
            .getAccounts()
        
        let currenciesRateObservable:Observable<[CurrencyRate]> = managerApi
            .getCurrenciesRatesWithoutCertificate()
        
        
        showLoading()
        
        Observable
            .zip(currenciesObservable, depositObservable, currenciesRateObservable)
            .subscribe(
                onNext: {(Currencies, deposits, CurrenciesRate) in
                    self.currencies = Currencies
                    UserDefaultsHelper.currencies = Currencies
                    self.depositsFrom = deposits
                    self.depositsWithCurrencyFrom = self.depositsAndCurrencies(currency:self.currencies, deposits: self.depositsFrom)
                    self.depositsTo = self.depositsWithCurrencyFrom
                    self.currencyRate = CurrenciesRate
                    self.currencyRateUpdate = self.nameCurrencies(currency: self.currencies, currencyRate: self.currencyRate)
                    self.currenciesWithRate = self.CurrenciesWithRate(currency: self.currencies, currencyRate: self.currencyRate)
                    
                    self.hideLoading()
            },
                onError: {(error) in
                    print(error)
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
      
//        managerApi
//            .getDeposits()
//            .updateTokenIfNeeded()
//            .subscribe(
//                onNext: {(deposits) in
//                    print(deposits)
//                    self.depositsFrom = deposits
//                    self.depositsTo = self.depositsFrom
//                    for deposit in deposits{
//                        print(deposit.accountNo as Any)
//                    }
//
//                    self.hideLoading()
//            },
//                onError: {(error) in
//                    print(error)
//                    self.hideLoading()
//            })
//            .disposed(by: disposeBag)
    }
    
    func depositsAndCurrencies(currency: [Currency], deposits:[Deposit]) -> [Deposit]{
        
        for id in deposits{
            for id2 in currency{
                if id.currencyID == id2.currencyID{
                    id.currency = id2
                }
            }
        }
        return deposits
    }
    public func conversion(ConversionOperationModel: ConversionOperationModel){
        showLoading()
        
        managerApi
            .postConversion(ConversionOperationModel: ConversionOperationModel)
            .subscribe(
                onNext: {(sum) in
                    
                    let conversionSum: ConversionSum!
                    conversionSum = sum
                    if let sumConverse = conversionSum.value{
                        self.sumLabel.text = "\(sumConverse)"
                    }
                    print(conversionSum)
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    var errorMessage = ""
                    print(error.pinError?.exceptionMessage as Any)
                    print(error.pinError?.statusCode as Any)
                    print(error.localizedDescription)
                    print(error.pinError?.message as Any)
                
                    if (error.pinError?.statusCode) != nil{
                        if let statusCode = (error.pinError?.statusCode){
                            if statusCode >= 500{
                                errorMessage = error.pinError?.exceptionMessage ?? error.localizedDescription
                            }
                            if statusCode >= 400 && statusCode <= 499 {
                                errorMessage = error.pinError?.message ?? error.localizedDescription
                            }
                        }
                    }
                    errorMessage = error.localizedDescription
                    self?.showAlertController(errorMessage)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func nameCurrencies(currency: [Currency], currencyRate:[CurrencyRate]) -> [CurrencyRate]{
        
        for id in currencyRate{
            for id2 in currency{
                if id.currencyID == id2.currencyID{
                    id.currency = id2
                }
            }
        }
        return currencyRate
    }
    func CurrenciesWithRate(currency: [Currency], currencyRate:[CurrencyRate]) -> [Currency]{
        
        for id in currency{
            for id2 in currencyRate{
                if id.currencyID == id2.currencyID{
                    id.currencyRate = id2
                }
            }
        }
        return currency
    }
}



extension DepositAccountsViewController: UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate{
    
    func picker(){
        let picker = UIPickerView()
        picker.delegate = self
        fromAccountTextField.inputView = picker
        toAccountTextField.inputView = picker
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch currentTxtField.tag {
        case 1:
            return depositsWithCurrencyFrom.count
        case 2:
            return depositsTo.count
        default:
            return 1
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch currentTxtField.tag {
        case 1:
            return depositsWithCurrencyFrom[row].accountNo ?? ""
        case 2:
            return depositsTo[row].accountNo ?? ""
        default:
            return ""
        }
       
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch currentTxtField.tag {
        case 1:
            selectedAccount = depositsWithCurrencyFrom?[row].accountNo
            if let currency = depositsWithCurrencyFrom?[row].currency?.currencyName, let balance = depositsWithCurrencyFrom?[row].balance {
                
                selectedCurrency = "\(currency)"
                selectedBalance = "\(balance)"
            }
            firstCurrencyId = depositsWithCurrencyFrom?[row].currency?.currencyID ?? 0
            print("firstCurrencyId: \(firstCurrencyId)")
            buyCurrency = currenciesWithRate[row].currencyRate?.buyRate ?? 0.0
            depositsTo = depositsWithCurrencyFrom
            fromAccountTextField.text = selectedAccount
            firstCurrencyLabel.text = selectedCurrency
            labelFrom.text = selectedCurrency
            fromBalanceTextField.text = selectedBalance
            selectedAccountIndex = row
            
        case 2:
            selectedAccount = depositsTo?[row].accountNo
            if let currency = depositsTo?[row].currency?.currencyName, let balance = depositsTo?[row].balance {
                
                selectedCurrency = "\(currency)"
                selectedBalance = "\(balance)"
            }
            secondCurrencyId = depositsWithCurrencyFrom?[row].currency?.currencyID ?? 0
            print("secondCurrencyId: \(secondCurrencyId)")
            sellCurrency = currenciesWithRate[row].currencyRate?.sellRate ?? 0.0
            toAccountTextField.text = selectedAccount
            secondCurrencyLabel.text = selectedCurrency
            toCurrencyLabel.text = selectedCurrency
            toBalanceTextField.text = selectedBalance
            selectedAccountIndex = row
            
        default:
                return
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField){
        currentTxtField = textField
        if currentTxtField == fromAccountTextField{
            currentTxtField.tag = 1
        }
        if currentTxtField == toAccountTextField{
            currentTxtField.tag = 2
        }
        print(depositsTo)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        switch currentTxtField.tag {
        case 1:
            if let index = selectedAccountIndex{
                depositsTo.remove(at: index)
                
                print(depositsTo)
            }
        case 2:
                print(depositsTo)
        default:
            return
        }
    }
}
