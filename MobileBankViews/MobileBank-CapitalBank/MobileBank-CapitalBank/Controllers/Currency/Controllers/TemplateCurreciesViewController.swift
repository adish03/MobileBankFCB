//
//  TemplateCurreciesViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/11/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

protocol NoNeedLoadDataDelegate {
    func isNeedData(isNeed : Bool)
}

class TemplateCurreciesViewController: BaseViewController {
    
    @IBOutlet weak var operationTypeLabel: UILabel!
    @IBOutlet weak var fromAccountLabel: UILabel!
    @IBOutlet weak var sumLabelSale: UILabel!
    @IBOutlet weak var toAccountLabel: UILabel!
    @IBOutlet weak var sumLabelBuy: UILabel!
    @IBOutlet weak var converssionLabel: UILabel!
    @IBOutlet weak var transactionNumberLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    var state = "operation_complete"
    var stateImage = "succes"
    
    @IBOutlet weak var viewTemplate: UIView!
    @IBOutlet weak var viewOperationState: UIView!
    @IBOutlet weak var stateLabel: UILabel!
    
    @IBOutlet weak var stackConversionCoef: UIStackView!
    
    var sumConversion = ""
    var sumConversionTo = ""
    var dateTimeOperation = ""
    var operationID = 0
    var operationTypeID = 7
    var confirmType = 0
    
    var submitModel: SubmitForCurrenciesModel!
    var operation: ConversionOperationModel!
    //operation detail
    var isNeedOperationDetail = false
    var navigationTitle = ""
    var delegate: NoNeedLoadDataDelegate?
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.isNeedData(isNeed: false)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        barButtonCustomize()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = localizedText(key: "operation_status")
        statusLabel.text = localizedText(key: state )
        statusImage.image = UIImage(named: stateImage )
        barButtonCustomize()
        
        
        if isNeedOperationDetail{
            operationTypeLabel.text = "Conversion".localized
            viewTemplate.isHidden = true
            viewOperationState.isHidden = false
            getInfoOperationConversion(operationID: operationID)
            let directions: [UISwipeGestureRecognizer.Direction] = [.up, .down, .right, .left]
            for direction in directions {
                let gesture = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(gesture:)))
                gesture.direction = direction
                view?.addGestureRecognizer(gesture)
            }
        }else{
            viewTemplate.isHidden = false
            viewOperationState.isHidden = true
            fromAccountLabel.text = submitModel.accountFrom
            sumLabelSale.text = sumConversion
            toAccountLabel.text = submitModel.accountTo
            sumLabelBuy.text = sumConversionTo
            converssionLabel.text = "\(String(format:"%.4f", submitModel.converssions))"
            transactionNumberLabel.text = "\(operationID)"
            dateLabel.text = dateTimeOperation
        }
        
    }
    //обработка swipe 
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
            
        case UISwipeGestureRecognizer.Direction.right:
            self.navigationController?.popViewController(animated: true)
        default:
            print("")
        }
    }
    @IBAction func closeButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func saveButton(_ sender: Any) {
        
    }
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toCurrencySegue"{
            let vc = segue.destination as! CurrencyViewController
            vc.conversionOperationModel = operation
            vc.isRepeatPay = true
            vc.isRepeatFromStory = true
            vc.navigationTitle = localizedText(key: "create_operation_request")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
    }
    // кнопка FAB
    @IBAction func AlertSheetControl(_ sender: Any) {
        
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionRepeatOperation = UIAlertAction(title: localizedText(key: "create_operation_request"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toCurrencySegue", sender: self)
        }
        
        
        let sendToBank = UIAlertAction(title: self.localizedText(key: "send_to_bank"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "Conversion", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SubmitConversionViewController") as! SubmitConversionViewController
            vc.sumConversion = self.sumLabelSale.text ?? ""
            vc.sumConversionTo = self.sumLabelBuy.text ?? ""
            vc.conversionOperationModel = self.operation
            vc.operationID = self.operationID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let actionDownload = UIAlertAction(title: localizedText(key: "download_check"), style: .default) { (action) in
            ApiHelper.shared.getInfoOperationFile(operationID: self.operationID,
                                                  code: 00000,
                                                  operationTypeID: self.operationTypeID,
                                                  vc:self )
        }
        
        
        let actionRemove = UIAlertAction(title: localizedText(key: "delete"), style: .default) { (action) in
            let alert = UIAlertController(title: self.localizedText(key:"delete_operation"), message: "", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                ApiHelper.shared.deleteTemplate(operationID: "\(self.operationID)")
                self.navigationController?.popViewController(animated: true)
            })
            let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)
            alert.addAction(actionOk)
            alert.addAction(actionCancel)
            self.present(alert, animated: true, completion: nil)
        }
        
        
        let actionCancel  = UIAlertAction(title: localizedText(key: "cancel"), style: .cancel, handler: nil)
        
        // отображение действий в зависимости от confirmType
        switch confirmType{
            
        case ConfirmType.Save.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            
//            alertSheet.addAction(actionDownload)
            if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsRemoveAllowed"){
                alertSheet.addAction(actionRemove)
            }
            if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Confirm.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            alertSheet.addAction(actionDownload)
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.InConfirm.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Error.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            alertSheet.addAction(actionCancel)
            
        default:
           
            alertSheet.addAction(actionDownload)
            alertSheet.addAction(actionCancel)
            
        }
        
        
        present(alertSheet, animated: true, completion: nil)
        
    }
    
//    получение инфо операции 
    func  getInfoOperationConversion(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperationConversion(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.operation = operation
                    self.fromAccountLabel.text = self.operation.sellAccount?.accountNo
                    self.sumLabelSale.text = "\(String(format:"%.2f", self.operation.sum ?? 0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID:  self.operation.sellAccount?.currencyID ?? 0))"
                    self.toAccountLabel.text = self.operation.buyAccount?.accountNo
                    self.sumLabelBuy.text = "\(String(format:"%.2f", self.operation.buySymDouble ?? 0.0)) \(ValueHelper.shared.getCurrentCurrency(currnecyID:  self.operation.buyAccount?.currencyID ?? 0))"
                    self.stackConversionCoef.isHidden = true
                    self.transactionNumberLabel.text = "\(self.operation.operationID ?? 0)"
                    self.dateLabel.text = self.dateFormater(date: self.operation.createDate ?? "")
                    self.stateLabel.textColor = self.confirmTypeState(string: self.stateLabel.text ?? "")
                    (self.stateLabel.text, self.stateLabel.textColor) = ValueHelper.shared.confirmTypeDefine(confirmType: self.operation.confirmType ?? 0)
                    self.confirmType = self.operation.confirmType ?? 0
                    self.operationTypeID = operation.operationType?.rawValue ?? 7
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    // кастомизация кнопки navigationabar
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        var image = ""
        if isNeedOperationDetail{
            image = "ArrowBack"
            button.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        }else{
            image = "Close"
            button.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        }
        button.setImage(UIImage(named: image), for: .normal)
        
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 50)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    //    кнопка закрытия
    @objc func closeButtonPressed() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    @objc func backButtonPressed() {
        navigationController?.popViewController(animated: true)
        delegate?.isNeedData(isNeed: false)
    }
}
