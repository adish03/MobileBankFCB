//
//  SMSForCurrencyViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/11/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//
import UIKit
import MobileBankCore
import RxSwift

class SMSForCurrencyViewController: BaseViewController {
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var smsTextField: UITextField!
    @IBOutlet weak var timeSmsLabel: UILabel!
    @IBOutlet weak var smsSendButtonOutlet: UIButton!
    
    
    var operationID = 0
    var operationTypeID = 0
    var submitModel: SubmitForCurrenciesModel!
    var conversionOperationModel: ConversionOperationModel!
    var operationModel: OperationModel!
    var dateTimeOperation = ""
    var coef = 0.0
    var sumConversion = ""
    var sumConversionTo = ""
    var countdownTimer: Timer!
    var totalTime = 60
    var codeLength = 0
    var code = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        
        navigationItem.title = localizedText(key: "verification_code")
        if let time = UserDefaultsHelper.SmsCodeAuthExpires {
            totalTime = time
            print("pinNumberCount: \(totalTime)")
        }
        if let Length = UserDefaultsHelper.EtokenCodeLength {
            codeLength = Length
            print("pinNumberCount: \(codeLength)")
        }
        smsTextField.defaultTextAttributes.updateValue(5.0, forKey: NSAttributedString.Key.kern)
        smsTextField.becomeFirstResponder()
        smsTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged) // make sure it is the desired textField
        
        startTimer()
   
        if let name = SessionManager.shared.user?.phone{
            phoneLabel.text = name
        }
        
    }
 
    @IBAction func sendSmsButton(_ sender: Any) {
        resendSms()
        if let time = UserDefaultsHelper.SmsCodeAuthExpires {
            totalTime = time
        }
        startTimer()
    }
  
}
extension SMSForCurrencyViewController{
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.count  == codeLength{
            self.code = textField.text!
            self.code = textField.text!
            self.operationModel = OperationModel(operationID: operationID,
                                                 code: self.code,
                                                 operationTypeID: InternetBankingOperationType.Conversion.rawValue)
            postConfirmPay(operationModel: operationModel)
        }
    }
}


extension SMSForCurrencyViewController{
    // таймер обратного отсчета
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        timeSmsLabel.isHidden = false
        smsSendButtonOutlet.isHidden = true
    }
    // таймер обратного отсчета
    @objc func updateTime() {
        timeSmsLabel.text = "\(timeFormatted(totalTime))"
        
        if totalTime != 0 {
            totalTime -= 1
        } else {
            endTimer()
        }
    }
    // таймер обратного отсчета
    func endTimer() {
        timeSmsLabel.isHidden = true
        smsSendButtonOutlet.isHidden = false
        countdownTimer.invalidate()
        //send sms to server for confirm
        
    }
   // таймер обратного отсчета формат даты
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
       
        return LocalizeHelper.shared.addWord("\(String(format: "%02d", seconds))", localizedText(key: "send_sms_again_after"))
    }

//    подтверждение операции
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    let storyboard = UIStoryboard(name: "Conversion", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TemplateCurreciesViewController") as! TemplateCurreciesViewController
                    
                    let navigationController = UINavigationController(rootViewController: vc)
                    vc.submitModel = self.submitModel
                    vc.sumConversion = self.sumConversion
                    vc.sumConversionTo = self.sumConversionTo
                    vc.dateTimeOperation = SubmitConversionViewController.currentTime()
                    vc.operationID = self.operationID
                    navigationController.modalPresentationStyle = .fullScreen
                    self.present(navigationController, animated: true, completion: nil)
                    
                    if result.message != ""{
                        self.showAlertController(result.message ?? "")
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    var errorMessage = ""
                    errorMessage = error.localizedDescription
                    if (error.apiError?.statusCode) != nil{
                        if let statusCode = (error.apiError?.statusCode){
                            if statusCode >= 500{
                                errorMessage = error.apiError?.error_description ?? error.localizedDescription
                            }
                            if statusCode >= 400 && statusCode <= 499 {
                                errorMessage = error.apiError?.error_description ?? error.localizedDescription
                            }
                        }
                    }
                    self?.showAlertController(errorMessage)
                    self?.smsTextField.text = ""
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
}
