//
//  CurrencyViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/28/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import RxCocoa
import Foundation

class CurrencyViewController: BaseViewController, DepositProtocol, DepositProtocolTo, EditConversionOperationProtocol{
   
    
    @IBOutlet weak var symbolFromLabel: UILabel!
    @IBOutlet weak var changeButton: RoundedButton!
    @IBOutlet weak var symbolToLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var currencyView: UIView!
    @IBOutlet weak var payFrom_accountLabel: UILabel!
    @IBOutlet weak var choiceCard_accountLabel: UILabel!
    @IBOutlet weak var payTo_accountLabel: UILabel!
    @IBOutlet weak var choiceCardTo_accountLabel: UILabel!
    @IBOutlet weak var currencyRateView: UIView!
    //Currency
    
    @IBOutlet weak var firstValueCurrency: UILabel!
    @IBOutlet weak var firstNameCurrency: UILabel!
    @IBOutlet weak var secondValueCurrenncy: UILabel!
    @IBOutlet weak var secondNameCurrency: UILabel!
    @IBOutlet weak var fromTextField: UITextField!
    @IBOutlet weak var toTextField: UITextField!
    
    // views error
    @IBOutlet weak var viewFromAccount: UIView!
    @IBOutlet weak var viewToAccount: UIView!
    @IBOutlet weak var viewSumFromTextField: UIView!
    @IBOutlet weak var viewSumToTextField: UIView!
    //label error
    @IBOutlet weak var labelErrorAccount: UILabel!
    @IBOutlet weak var labelErrorAccountFrom: UILabel!
    @IBOutlet weak var labelErrorSum: UILabel!
    @IBOutlet weak var labelErrorSumTo: UILabel!
    @IBOutlet weak var currencyShowLable: UILabel!
    
    var selectedDeposit: Deposit!
    var selectedDepositTo: Deposit!
    var index = 0
    var sectionAccount = 0
    var operationID = 0
    var navigationTitle = ""
    
    var depositsWithCurrencies = [Deposit]()
    var depositsWithCurrenciesTo = [Deposit]()
    var currencies = [Currency]()
    var deposits = [Deposit]()
    
    var currencyRate = [CurrencyRate]()
    var currencyRateUpdate = [CurrencyRate]()
    var currencyNational: CurrencyNational!
    var accounts: [Accounts] = []
    var accountsAfterChoice: [Accounts] = []
    
    var conversionOperationModel: ConversionOperationModel!
    var sumConversion = 0.0
    var sumConversionBuy = 0.0
    
    var coef = 1.0
    var buyCurrency = 0.0
    var sellCurrency = 0.0
    var rate = 0.0
    var sell = 0.0
    var buy = 0.0
    var sellAmount = 0.0
    var buyAmount = 0.0
    var operationTypeID = InternetBankingOperationType.Conversion.rawValue
    //RepeatPayTemplate
    var isRepeatPay = false
    var isRepeatFromStory = false
    var isRepeatOperation = false
    var currencyIDcurrent = 0
    var accountNoCurrent = ""
    var isNewPayFromFianance = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        segmentController.setTitle(localizedText(key: "exchange"), forSegmentAt: 0)
        segmentController.setTitle(localizedText(key: "currency_rates"), forSegmentAt: 1)
        
        if selectedDeposit != nil{
           fullSelectedDeposit(selectedDeposit: selectedDeposit)
        }else{
            payFrom_accountLabel.text = localizedText(key: "write_off")
            choiceCard_accountLabel.text = localizedText(key: "select_account_or_card")
        }
        
        if selectedDepositTo != nil{
           fullSelectedDepositTo(selectedDeposit: selectedDepositTo)
            
        }else{
            payTo_accountLabel.text = localizedText(key: "transfer_to")
            choiceCardTo_accountLabel.text = localizedText(key: "select_account_or_card")
        }
        
        if selectedDeposit != nil && selectedDepositTo != nil{
            getCoef(selectedDeposit: selectedDeposit, selectedDepositTo: selectedDepositTo)
        }
        print("coef: \(coef)")
        allowToCreatAndSaveOperation(button: changeButton)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if navigationTitle == ""{
             navigationItem.title = localizedText(key: "currency_conversion")
        }else{
            navigationItem.title = navigationTitle
        }
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        tableView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        fromTextField.delegate = self
        toTextField.delegate = self
        hideKeyboardWhenTappedAround()
        
        if isNewPayFromFianance{
            if  menu != nil{
                menu.image = UIImage(named: "ArrowBack")
                menu.target = self
                menu.action = #selector(closeButtonPressed)
            }
        }else{
            if  menu != nil{
                menu.target = self.revealViewController()
                menu.action = #selector(SWRevealViewController.revealToggle(_:))
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            }
        }
        
        tableView.isHidden = true
        getAccountsWithCurrencies(vc: nil)
        conversionOnFly(firstTextField: fromTextField, secondTextField: toTextField)
        tableView.reloadData()
        
        fromTextField.addTarget(self, action: #selector(textFieldEditingDidChangeSum(_:)), for: UIControl.Event.editingDidEnd)
        toTextField.addTarget(self, action: #selector(textFieldEditingDidChangeSumTo(_:)), for: UIControl.Event.editingDidEnd)
        
        
    }
    //    кнопка закрытия
    @objc func closeButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func textFieldEditingDidChangeSum(_ sender: Any) {
        
        let textNonZeroFirst = fromTextField.text
        let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
        let numberAsInt = Double(newString ?? "0")
        fromTextField.text = "\(numberAsInt ?? 0)"
        if selectedDeposit != nil{
            if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                showAlertController(localizedText(key: "insufficient_account_balance"))
                fromTextField.text = ""
                toTextField.text = ""
            }
        }
        fromTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
        if fromTextField.text == "" || fromTextField.text == "0.00" {
            fromTextField.text = "0.00"
            toTextField.text = "0.00"
        }
    }
    @objc func textFieldEditingDidChangeSumTo(_ sender: Any) {
        let textNonZeroFirst = toTextField.text
        let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
        let numberAsInt = Double(newString ?? "0")
        toTextField.text = "\(numberAsInt ?? 0)"        
        if toTextField.text == "" || toTextField.text == "0.00" {
            fromTextField.text = "0.00"
            toTextField.text = "0.00"
        }
    }
    
    @IBAction func segmentControl(_ sender: Any) {
        switch segmentController.selectedSegmentIndex {
        case 0:
            navigationItem.title = localizedText(key: "currency_conversion")
            tableView.isHidden = true
            scrollView.isUserInteractionEnabled = true
            
        case 1:
            navigationItem.title = localizedText(key: "currency_rates")
            tableView.isHidden = false
            scrollView.isUserInteractionEnabled = false
            tableView.reloadData()
            
        default:
            print("no selected")
        }
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField{
        case self.fromTextField:
            if selectedDepositTo == nil || selectedDeposit == nil{
                showAlertController(localizedText(key: "select_account_or_card"))
                viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
                viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorAccountFrom)
                viewTextFieldShouldBeginEditing(viewTextField: viewSumToTextField, labelError: labelErrorSumTo)
                viewTextFieldShouldBeginEditing(viewTextField: viewSumFromTextField, labelError: labelErrorAccountFrom)
            }
            
            
        case self.toTextField:
            if selectedDepositTo == nil || selectedDeposit == nil{
                showAlertController(localizedText(key: "select_account_or_card"))
                viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
                viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorAccountFrom)
                viewTextFieldShouldBeginEditing(viewTextField: viewSumToTextField, labelError: labelErrorSumTo)
                viewTextFieldShouldBeginEditing(viewTextField: viewSumFromTextField, labelError: labelErrorAccountFrom)
            }
            
        default:
            break
        }
        return true
    }
        
    func sortAccounts(accounts: [Accounts]) -> [Accounts] {
        var sortedAccounts = [Accounts]()
        accounts.forEach { account in
            if account.accountType == 0 {
                sortedAccounts.append(account)
            }
        }
        return sortedAccounts
    }
    
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let sortedAccounts = sortAccounts(accounts: accounts)
        if segue.identifier == "segueToAccountsFrom"{
            let vc = segue.destination as! AccountsViewController
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "pay_from")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            vc.selectedDeposit = selectedDeposit
            vc.accounts = sortedAccounts
        }
        if segue.identifier == "segueToAccountsTo"{
            let vc = segue.destination as! AccountsViewController
            vc.delegateTo = self
            vc.navigationTitle = localizedText(key: "Resources_Deposits_Conversion_BuyAccount")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            vc.selectedDeposit = selectedDepositTo
            vc.accounts = sortedAccounts
        }
        if segue.identifier == "PaymentHistoryViewController"{
            let vc = segue.destination as! PaymentHistoryViewController
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        if segue.identifier == "toSubmitSegue"{
            let vc = segue.destination as! SubmitConversionViewController
            vc.delegate = self
            let sell = SellAccount(accountNo: selectedDeposit.name ?? " ", currencyID: Int(selectedDeposit.currencyID ?? 0) )
            let buy = BuyAccount(accountNo: selectedDepositTo.name ?? " ", currencyID: Int(selectedDepositTo.currencyID ?? 0) )
            if let sum = fromTextField.text{
                self.sumConversion = Double(sum)!
                
            }
            if let buy = toTextField.text{
                self.sumConversionBuy = doubleRounded(decimal: buy)
            }
            
            conversionOperationModel = ConversionOperationModel(sellAccount: sell, buyAccount: buy, sum: sumConversion, buy:sumConversionBuy, save: true)
            vc.coef = coef
            //var rate = currencyRate.filter{ $0.currencyID == buy.currencyID}
            vc.buyCurrency = rate
            vc.sumConversion = "\(fromTextField.text ?? "") \(String(describing: selectedDeposit.currency?.symbol ?? ""))"
            vc.sumConversionTo = "\(toTextField.text ?? "") \(String(describing: selectedDepositTo.currency?.symbol ?? ""))"
            vc.conversionOperationModel = conversionOperationModel
            vc.operationID = operationID
            vc.operationTypeID = operationTypeID
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            
        }
    }
    
    @IBAction func changeButton(_ sender: Any) {
        
        if selectedDeposit != nil && selectedDepositTo != nil && fromTextField.text != "" && toTextField.text != ""{
            
            if payFrom_accountLabel.text != "" && payTo_accountLabel.text != "" && fromTextField.text != "" {
                let sell = SellAccount(accountNo: selectedDeposit.accountNo ?? " ", currencyID: Int(selectedDeposit.currencyID ?? 0) )
                let buy = BuyAccount(accountNo: selectedDepositTo.accountNo ?? " ", currencyID: Int(selectedDepositTo.currencyID ?? 0) )
                //MARK: - converstion heppens here
                if fromTextField.text != nil{
                    self.sumConversion = sellAmount //Double(sum)!
                }
                if toTextField.text != nil{
                    self.sumConversionBuy = Double(roundValue(convertedValue: Double(buyAmount)))
                }
                
                conversionOperationModel = ConversionOperationModel(sellAccount: sell, buyAccount: buy, sum: sumConversion, buy: sumConversionBuy , save: true)
                
                if isRepeatOperation{
                    if self.operationID != 0{
                        self.hideLoading()
                        self.allowOperation()
                    }else{
                        createConversion(ConversionOperationModel: conversionOperationModel)
                    }
                }else{
                    createConversion(ConversionOperationModel: conversionOperationModel)
                }
            }
        }else{
            viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
            viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorAccountFrom)
            fromTextField.stateIfEmpty(view: viewSumFromTextField, labelError: labelErrorSum)
            toTextField.stateIfEmpty(view: viewSumToTextField, labelError: labelErrorSumTo)
            showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
        }
    }
    // rounding by even
    func roundValue(convertedValue: Double) -> Double {
        let sign = convertedValue < 0 ? -1 : 1
        let value = abs(convertedValue)
        let reminderFraction = value * 100 - floor(value * 100)
        var rounded = (value * 100).rounded() / 100
        if abs(reminderFraction - 0.5) > 1e-9 {
            return Double(sign) * rounded
        }
        rounded = ceil(value * 100) / 100
        let isLastDigitEven = floor(value * 100).truncatingRemainder(dividingBy: 2)
        let result = Double(sign) * (isLastDigitEven == 0 ? rounded - 0.01 : rounded)
        return result
    }
    
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
        if selectedDeposit != nil && selectedDepositTo != nil{
            if self.selectedDepositTo.currencyID == self.selectedDeposit.currencyID{
                self.selectedDepositTo = nil
            }
        }
        //        currencyView.isHidden = true
        fromTextField.text = ""
        toTextField.text = ""
    }
    
    func selectedDepositUserTo(deposit: Deposit) {
        selectedDepositTo = deposit
        if selectedDeposit != nil && selectedDepositTo != nil{
            if self.selectedDepositTo.currencyID == self.selectedDeposit.currencyID{
                self.selectedDeposit = nil
            }
        }
        //        currencyView.isHidden = true
        fromTextField.text = ""
        toTextField.text = ""
    }
    // Получение данных для редактирования оперции
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool) {
        self.operationID = operationID
        self.isRepeatPay = isRepeatPay
        self.isRepeatOperation = isRepeatOperation
    }
    
    
}


extension CurrencyViewController: UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return currencyRateUpdate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CurrencyTableViewCell
        
        let currency = currencyRateUpdate[indexPath.row].currency
        if let diffBuy = currencyRateUpdate[indexPath.row].diffBuyRate{
            if diffBuy >= 0.0{
                cell.buyArrowImage.image = UIImage(named: "arrow_drop_up")
            }else{
                cell.buyArrowImage.image = UIImage(named: "arrow_drop_down")
            }
        }
        if let diffSell = currencyRateUpdate[indexPath.row].diffSellRate{
            if diffSell >= 0.0{
                cell.saleArrowImage.image = UIImage(named: "arrow_drop_up")
            }else{
                cell.saleArrowImage.image = UIImage(named: "arrow_drop_down")
            }
        }
        let crossCurrency =  currencyRateUpdate[indexPath.row].crossCurrency
        cell.buyCurrencyLabel.text = crossCurrency?.symbol//currencyNational.currency?.symbol
        cell.sellCurrencyLabel.text = currency?.symbol
        cell.currencyBuyLabel.text = "\(String(format:"%.4f", currencyRateUpdate[indexPath.row].buyRate ?? 0))"
        cell.currencySellLabel.text = "\(String(format:"%.4f", currencyRateUpdate[indexPath.row].sellRate ?? 0))"
        
        return cell
    }
    
    
}

extension CurrencyViewController{
    
    func  getAccountsWithCurrencies(vc: AccountsViewController?) {
        
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        
        let depositObservable:Observable<[Accounts]> = managerApi
            .getAccountForConversion()
        
        showLoading()
        Observable
            .zip(currenciesObservable, depositObservable)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (Currencies, Accounts) in
                    guard let `self` = self else { return }
                    self.currencies = Currencies
                    if Accounts.count > 0{
                        self.accounts = self.accountsCurrencies(currency: self.currencies, accounts: Accounts)
                        
                        if self.isRepeatPay{
                            
                            self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.conversionOperationModel.sellAccount?.accountNo ?? "", currency: self.conversionOperationModel.sellAccount?.currencyID ?? 0, accounts: self.accounts)
                            self.selectedDepositTo = self.getCurrentTemplateAccount(accountNo: self.conversionOperationModel.buyAccount?.accountNo ?? "", currency: self.conversionOperationModel.buyAccount?.currencyID ?? 0, accounts: self.accounts)
                            if self.selectedDeposit != nil{
                                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                            }
                            self.fullSelectedDepositTo(selectedDeposit: self.selectedDepositTo)
                            self.fromTextField.text = "\(self.conversionOperationModel.sum ?? 0.0)"
                            self.getCurrenciesAndRatesWithFlatMap()
                            
                        }else{
                          
                            if self.currencyIDcurrent != 0 && self.accountNoCurrent != ""{
                                self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.accountNoCurrent, currency: self.currencyIDcurrent, accounts: self.accounts)
                                if self.selectedDeposit != nil{
                                    self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                }
                                self.getCurrenciesAndRatesWithFlatMap()
                            }
                            
                            if self.selectedDeposit == nil && self.selectedDepositTo == nil{
                                self.getCurrenciesAndRatesWithFlatMap()
                            }
                        }
                        vc?.accounts = self.accounts
                        vc?.tableView.reloadData()
                        
                        self.tableView.reloadData()
                        self.hideLoading()
                        
                    }else{
                        self.showAlertController(self.localizedText(key: "no_active_accounts_found"))
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))

                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
        
    }
    func accountsCurrencies(currency: [Currency], accounts:[Accounts]) -> [Accounts]{
        
        for account in accounts{
            for item in account.items!{
                for id2 in currency{
                    if item.currencyID == id2.currencyID{
                        item.currency = id2
                    }
                }
            }
        }
        return accounts
    }
    
    func  getCurrenciesAndRatesWithFlatMap(){
        let currenciesNationalObservable:Observable<CurrencyNational> = managerApi
            .getCurrenciesNationalWithoutCertificate()
        
        showLoading()
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
            .updateTokenIfNeeded()
        
        let currenciesRateObservable:Observable<[CurrencyRate]> = managerApi
            .getCurrenciesRatesWithoutCertificate()
        
        
        currenciesNationalObservable
            .flatMap{ CurrenciesNational -> Observable<[Currency]> in
                self.currencyNational = CurrenciesNational
                return currenciesObservable
            }
            .flatMap{ Currencies -> Observable<[CurrencyRate]> in
                self.currencies = Currencies
                return currenciesRateObservable
            }
            .subscribe(
                onNext: {[weak self] (CurrenciesRate) in
                    guard let `self` = self else { return }
                    self.currencyRate = CurrenciesRate
                    self.currencyRateUpdate = self.nameCurrencies(currency: self.currencies, currencyRate: self.currencyRate)
                    self.currencyNational = self.nameCurrencyNational(currencyNational: self.currencyNational, currencies: self.currencies)
                    if self.isRepeatPay{
                        if self.selectedDeposit != nil && self.selectedDepositTo != nil{
                            self.getCoef(selectedDeposit: self.selectedDeposit, selectedDepositTo: self.selectedDepositTo)
                        }
                        self.conversionOnFly(firstTextField: self.fromTextField, secondTextField: self.toTextField)
                    }
                    if self.currencyRate.isEmpty{
                        self.changeButton.isEnabled = false
                    }
                    self.hideLoading()
                    self.tableView.reloadData()
            },
                onError: {(error) in
                    self.showAlertController(ApiHelper.shared.errorHelper(error: error))

                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    func conversionOnFly(firstTextField: UITextField, secondTextField: UITextField){
        firstTextField.rx.text
            .map {  text -> String in
                let newString = text?.replacingOccurrences(of: ",", with: ".")
                let x = Double(newString!) ?? 0.0
                let comission =  String(format:"%.2f",  self.roundValue(convertedValue:  Double(x * self.coef) ))
                self.sellAmount = x
                self.buyAmount = x * self.coef
                return Optional(comission) ?? "" }
            .bind(to: secondTextField.rx.text)
            .disposed(by: disposeBag)
        
        secondTextField.rx.text
            .map {  text -> String in
                let newString = text?.replacingOccurrences(of: ",", with: ".")
                let x = Double(newString!) ?? 0.0
                let total = String(format:"%.2f",self.roundValue(convertedValue:  Double(x / self.coef)))
                self.buyAmount = x
                self.sellAmount = x / self.coef
                return Optional(total) ?? "" }
            .bind(to: firstTextField.rx.text)
            .disposed(by: disposeBag)
    }
    //    заполнение текущего счета
    func fullSelectedDeposit(selectedDeposit: Deposit){
        let balance = "\(String(format:"%.2f", selectedDeposit.balance ?? 0.0))" + "\(String(describing: selectedDeposit.currency?.symbol ?? ""))"
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        payFrom_accountLabel.text = fullAccountsText
        firstNameCurrency.text = selectedDeposit.currency?.symbol
        choiceCard_accountLabel.text = balance
        symbolFromLabel.text = " " + "\(selectedDeposit.currency?.symbol ?? "")" + " "
        viewFromAccount.stateAccountIfEmpty(deposit: selectedDeposit, view: viewFromAccount, labelError: labelErrorAccountFrom)
        
        accountChosen()
    }
    //    заполнение текущего счета
    func fullSelectedDepositTo(selectedDeposit: Deposit){
        let balance = "\(String(format:"%.2f",selectedDepositTo.balance ?? 0.0 )) \(String(describing: selectedDepositTo.currency?.symbol ?? ""))"
        let fullAccountsText: String = "\(String(describing: selectedDepositTo.name ?? "")) ∙∙\(String(describing: selectedDepositTo.accountNo?.suffix(4) ?? ""))"
        payTo_accountLabel.text = fullAccountsText
        choiceCardTo_accountLabel.text = balance
        secondNameCurrency.text = selectedDepositTo.currency?.symbol
        symbolToLabel.text = " " + "\(selectedDepositTo.currency?.symbol ?? "")" + " "
        viewToAccount.stateAccountIfEmpty(deposit: selectedDepositTo, view: viewToAccount, labelError: labelErrorAccount)
        
        accountChosen()
    }
 
    func accountChosen() {
        if selectedDeposit != nil && selectedDepositTo != nil {

            let baseRate = searchRateBase(currencies: self.currencyRate, sellAccount: selectedDeposit, buyAccount: selectedDepositTo)
            let finalRate = convertCurrency(sellRate: baseRate.sellRate, buyRate: baseRate.buyRate)
            
            if(baseRate.sellRate != nil && baseRate.buyRate == nil){
                currencyRateView.isHidden = false
                currencyShowLable.text =
                    "1 \(selectedDeposit.currencySymbol ?? "") = \(String(format:"%.4f", finalRate)) \(String(describing: selectedDepositTo.currencySymbol ?? ""))"
            }
            else if(baseRate.sellRate != nil && baseRate.buyRate != nil){
                currencyRateView.isHidden = true
            }
            else{
                currencyRateView.isHidden = false
                currencyShowLable.text =
                    "1 \(selectedDepositTo.currencySymbol ?? "") = \(String(format:"%.4f", finalRate)) \(String(describing: selectedDeposit.currencySymbol ?? ""))"
            }
        }
    }
}

extension CurrencyViewController {
    
    func convertCurrency(sellRate: CurrencyRate?, buyRate: CurrencyRate?) -> Double
    {
        if(buyRate == nil && sellRate != nil)
        {
            return sellRate!.buyRate!
        }
        if(sellRate == nil && buyRate != nil)
        {
            return buyRate!.sellRate!
        }
        if(sellRate != nil && buyRate != nil)
        {
            return sellRate!.buyRate! / buyRate!.sellRate!
        }
        return 0.0
    }

    func searchRateBase(currencies: [CurrencyRate], sellAccount: Deposit, buyAccount: Deposit) -> (sellRate: CurrencyRate?, buyRate: CurrencyRate?) {
        
        var sellRate: CurrencyRate? =  nil
        var buyRate: CurrencyRate? = nil
        
        currencies.forEach { (it) in
            if(it.currencyID == sellAccount.currencyID && it.crossCurrencyID == buyAccount.currencyID) {
                sellRate = it // Прямой курс
                buyRate = nil
            }
        }
        
        currencies.forEach { (it) in
            if(it.currencyID == sellAccount.currencyID && it.currencyID == buyAccount.currencyID) {
                buyRate = it // Обратный курс
                sellRate = nil
            }
        }
        
        currencies.forEach{ (it) in //кросс курс
            if(it.currencyID == sellAccount.currencyID && it.crossCurrencyID == 417)
            {
                sellRate = it
            }
            
            if(it.currencyID == buyAccount.currencyID && it.crossCurrencyID ==  417)
            {
                buyRate = it
            }
        }
        return (sellRate, buyRate)
        
    }
}
extension CurrencyViewController{
    
    public func createConversion(ConversionOperationModel: ConversionOperationModel){
        showLoading()
        
        managerApi
            .postConversion(ConversionOperationModel: ConversionOperationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](operation) in
                    guard let `self` = self else { return }
                    self.operationID = operation.key ?? 0
                        self.allowOperation()
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))

                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    func getCoef(selectedDeposit: Deposit, selectedDepositTo: Deposit){
        
        for currency in currencyRateUpdate{
            if (selectedDeposit.currencyID == currency.currencyID) && (selectedDepositTo.currencyID == currency.crossCurrencyID){
                coef = currency.buyRate ?? 0.0
                rate = currency.buyRate ?? 0.0
                return
            }
            if (selectedDepositTo.currencyID == currency.currencyID) && (selectedDeposit.currencyID == currency.crossCurrencyID){
                coef = 1/(currency.sellRate ?? 0.0)
                rate = currency.sellRate ?? 0.0
                return
            }
        }
        for currency in currencyRateUpdate{
            
            if selectedDeposit.currencyID == currency.currencyID  && selectedDepositTo.currencyID !=  Constants.NATIONAL_CURRENCY {
                
                for currency in currencyRateUpdate{
                    if selectedDeposit.currencyID == currency.currencyID &&  Constants.NATIONAL_CURRENCY == currency.crossCurrencyID {
                        buy = currency.buyRate ?? 0.0
                    }
                    if selectedDepositTo.currencyID == currency.currencyID &&  Constants.NATIONAL_CURRENCY == currency.crossCurrencyID {
                        sell = currency.sellRate ?? 0.0
                    }
                }
                coef = buy/sell
                rate = sell
            }
        }
    }
}

extension CurrencyViewController{
    // проверка для прав на создание и подтверждние
    func  allowToCreatAndSaveOperation(button: UIButton) {
     
        if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsAddAllowed") && AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "execute_operation"),for: .normal)
        }else if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsAddAllowed") && !AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "Resources_Common_Save"),for: .normal)
        }
    }
    
    //    проверка прав
    func  allowOperation() {
            
        if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsAddAllowed") && AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsApproveAllowed"){
            self.performSegue(withIdentifier: "toSubmitSegue", sender: self)

        }else if AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsAddAllowed") && !AllowHelper.shared.depositsAllowOpertaion(nameOperation: "Deposits.ConversionsSecurityOperations.IsApproveAllowed"){
        
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
            self.view.window?.rootViewController = vc
            
            let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
            let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
            let navigationController = UINavigationController(rootViewController: destinationController)
            vc.pushFrontViewController(navigationController, animated: true)
        }
    }
}
