//
//  SubmitConversionViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/1/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

protocol EditConversionOperationProtocol {
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool)
}

class SubmitConversionViewController: BaseViewController {
    
    var operationID = 0
    var operationTypeID = 0
    var conversionOperationModel: ConversionOperationModel!
    var coef = 0.0
    var buyCurrency = 0.0
    var sumConversion = ""
    var sumConversionTo = ""
    var operationModel: OperationModel!
    var code = "000000"
    var typeOperation = ""
    var delegate: EditConversionOperationProtocol?
    
    @IBOutlet weak var fromAcccountLabel: UILabel!
    @IBOutlet weak var firstSumLabel: UILabel!
    @IBOutlet weak var toAccountLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var currencyConversionLabel: UILabel!
    
    var submitModel: SubmitForCurrenciesModel!
    var isRepeatPay = true
    var isRepeatOperation = true
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isRepeatPay{
            delegate?.editOperationData(operationID: operationID, isRepeatPay: isRepeatPay, isRepeatOperation: isRepeatOperation)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = localizedText(key: "confirmation")
        
        fromAcccountLabel.text = conversionOperationModel.sellAccount?.accountNo
        toAccountLabel.text = conversionOperationModel.buyAccount?.accountNo
        firstSumLabel.text = sumConversion
        
        currencyConversionLabel.text = "\(String(format:"%.4f", buyCurrency))"
        sumLabel.text = sumConversionTo
        
        let conversession = Double(currencyConversionLabel.text ?? "0.0")
        submitModel = SubmitForCurrenciesModel(accountFrom: fromAcccountLabel.text ?? "",
                                               sumSale: "\(String(describing: conversionOperationModel?.sum))",
                                               accountTo: toAccountLabel.text ?? "",
                                               sumBuy: "\(String(describing: conversionOperationModel?.buySum))",
                                               converssions: conversession ?? 0.0,
                                               transactionNumber: "0000",
                                               dateTime: conversionOperationModel.scheduleDate ?? "")
    }
    
    @IBAction func SubmitConversionButton(_ sender: Any) {
        isRepeatPay = false
        postIsRequireConfirmOperationCode(operationID: operationID)
    }
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toSMSSegue"{
            let vc = segue.destination as! SMSForCurrencyViewController
            
            vc.submitModel = self.submitModel
            vc.sumConversion = self.sumConversion
            vc.sumConversionTo = self.sumConversionTo
            vc.dateTimeOperation = SubmitConversionViewController.currentTime()
            vc.operationID = self.operationID
            vc.operationModel = self.operationModel
        }
    }
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    let storyboard = UIStoryboard(name: "Conversion", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TemplateCurreciesViewController") as! TemplateCurreciesViewController
                    let navigationController = UINavigationController(rootViewController: vc)
                    vc.submitModel = self.submitModel
                    vc.sumConversion = self.sumConversion
                    vc.sumConversionTo = self.sumConversionTo
                    vc.dateTimeOperation = SubmitConversionViewController.currentTime()
                    vc.operationID = self.operationID
                    self.present(navigationController, animated: true, completion: nil)
                    
                    if result.message != ""{
                        self.showAlertController(result.message ?? "")
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                   self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
// Необходим ли код подтверждения
    func postIsRequireConfirmOperationCode(operationID: Int){
        showLoading()
        managerApi
            .postIsRequireConfirmOperationCode(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] isConfirm in
                    guard let `self` = self else { return }
                    self.operationModel = OperationModel(operationID: operationID,
                                                         code: self.code,
                                                         operationTypeID: InternetBankingOperationType.Conversion.rawValue)
                    if isConfirm == "true"{
                         self.sendSms(operationID: operationID)
                        self.hideLoading()
                    }
                    if isConfirm == "false"{
                        self.postConfirmPay(operationModel: self.operationModel)
                    }
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //отправка смс
   
    func  sendSms(operationID: Int){ //for resend sms
        showLoading()
        managerApi
            .postSendConfirmOperationCode(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](sms) in
                    guard let `self` = self else { return }
                    
                    let alertController = UIAlertController(title: nil, message: sms, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.performSegue(withIdentifier: "toSMSSegue", sender: self)
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                    
                    self.hideLoading()
            },
                onError: {(error) in
                    
                    self.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    static func currentTime() -> String {
        let date = Date()
        let calendar = Calendar.current
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        return "\(day).\(month).\(year) \(hour):\(minutes)"
    }
    
    
}

