//
//  CurrencyTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/21/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore

class CurrencyCell: UITableViewCell {

    @IBOutlet weak var currencyNameLabel: UILabel!
    @IBOutlet weak var buyLabel: UILabel!
    @IBOutlet weak var sellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
       
        if selected {
            self.contentView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        } else {
            self.contentView.backgroundColor = .white
        }
    }

}
