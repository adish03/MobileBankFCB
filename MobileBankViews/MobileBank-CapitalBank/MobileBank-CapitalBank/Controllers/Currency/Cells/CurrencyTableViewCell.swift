//
//  CurrencyTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/28/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
// класс для кастомизации ячейки таблицы
class CurrencyTableViewCell: UITableViewCell {

    @IBOutlet weak var buyCurrencyLabel: UILabel!
    @IBOutlet weak var sellCurrencyLabel: UILabel!
    
    @IBOutlet weak var currencyBuyLabel: UILabel!
    
    @IBOutlet weak var currencySellLabel: UILabel!
    
    @IBOutlet weak var saleArrowImage: UIImageView!
    
    @IBOutlet weak var buyArrowImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.contentView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        } else {
            self.contentView.backgroundColor = .white
        }
    }

}
