//
//  CustomActivityViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Atai Ismailov on 21/09/2021.
//  Copyright © 2021 Spalmalo. All rights reserved.
//

import UIKit
import Foundation
import MobileBankCore

class CustomActivityViewController: UIActivityViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UINavigationBar.appearance().backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        UINavigationBar.appearance().tintColor = UIColor(hexFromString: Constants.MAIN_COLOR)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UINavigationBar.appearance().backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        UINavigationBar.appearance().tintColor = UIColor.white
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}
