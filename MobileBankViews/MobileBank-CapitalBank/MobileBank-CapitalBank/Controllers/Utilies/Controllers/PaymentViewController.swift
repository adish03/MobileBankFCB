//
//  PaymentViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/25/19.
//  Copyright © 2019 Spalmalo. All rights reserved.

import UIKit
import MobileBankCore
import RxSwift
import RxCocoa
import ObjectMapper


class PaymentViewController: BaseViewController, DepositProtocol, CategoriesProtocol, UtilityProtocol, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, EditPaymentsProtocol {

    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var nameCategoryLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var sumPaymentTextField: UITextField!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var comissionLabel: UILabel!

    @IBOutlet weak var requisiteLabel: UILabel!
    
    @IBOutlet weak var totalSumLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var sumFromLabel: UILabel!
    @IBOutlet weak var sumToLabel: UILabel!
    @IBOutlet weak var payFrom_accountLabel: UILabel!
    @IBOutlet weak var choiceCard_accountLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var sumCurrencyLabel: UILabel!
    @IBOutlet weak var commissionCurrencyLabel: UILabel!
    @IBOutlet weak var totalCurrencyLabel: UILabel!
    
    @IBOutlet weak var currencySymbol: UILabel!
    
    //Соц. фонд
    @IBOutlet weak var socFondView: UIView!
    @IBOutlet weak var governmentField: UITextField!
    @IBOutlet weak var departmentField: UITextField!
    @IBOutlet weak var paymentTypeField: UITextField!
    @IBOutlet weak var serviceSFField: UITextField!
    @IBOutlet weak var payerTypeField: UITextField!
    @IBOutlet weak var okpoField: UITextField!
    @IBOutlet weak var fullNameField: UITextField!

    //ГНС
    @IBOutlet weak var gnsView: UIView!
    @IBOutlet weak var regionField: UITextField!
    @IBOutlet weak var districtField: UITextField!
    @IBOutlet weak var gnsDepartmentField: UITextField!
    @IBOutlet weak var serviceField: UITextField!
    @IBOutlet weak var paymentPurposeField: UITextField!
    @IBOutlet weak var fullNameView: UIView!
    @IBOutlet weak var okpoView: UIView!
    @IBOutlet weak var paymentPurposeView: UIView!
    
    //view
    @IBOutlet weak var viewNoTextField: UIView!
    @IBOutlet weak var labelErrorNo: UILabel!
    @IBOutlet weak var viewSumTextField: UIView!
    @IBOutlet weak var viewCommentTextField: UIView!
    @IBOutlet weak var labelErrorSum: UILabel!
    @IBOutlet weak var labelErrorAccount: UIView!
    @IBOutlet weak var labelErrorComment: UILabel!
    @IBOutlet weak var LogoView: UIView!
    
  
    @IBOutlet var CurrencyLabel: [UILabel]!
    
    var selectedDeposit: Deposit!
    var CheckPaymentModel: UtilityPaymentOperationModel!
    var PayPaymentModel: UtilityPaymentOperationModel!
    var listUtilities: [UtilityModel]!
    
    var listRegions = [TaxRegionModel]()
    var listDepartments = [TaxDepartmentModel]()
    var listOkmotCodes = [TaxOkmotCodeModel]()
    var listDistricts = [TaxDistrictModel]()
    
    var currencyID = 0
    var providerID = 0
    var parentID: Int? = nil
    var contragentID: Int? = nil
    var sum = 0.0
    
    var categoriesById = [UtilityModel]()
    var agentId = 0
    var categoryID = 0
    var agent: UtilityModel!
    var submitModel: SubmitForPayModel!
    var payData: UtilityPaymentOperationModel!
    var accountssWithCurrencies = [Accounts]()
    var currencies = [Currency]()
    var currentUtility: UtilityModel!
    var accounts: [Accounts] = []
    var timer: Timer? = nil
    var navigationTitle = ""
    var operationID = 0
    var regularExpression = ""
    let currencyCurrentSymbol = ""
    var operationTypeID = 0
    var imageUtility = ""
    var isError: Bool = false
    
    var scenarious: [ScenarioFieldModel] = []
    var textFields: [UITextField] = []
    var arrayOfCommissions = [Commission]()
    
    let specialUtilities: [Int] = [1203, 1163]
    
    //GNS
    //dictionaries
    var arrayOfRegion = [Region]()
    var pickerViewregions = UIPickerView()
    var pickedRegion: Region?
    var operationRegion: String!
    var operationRegionCode: Int!
    
    var arrayOfDistricts = [District]()
    var pickerViewdDistricts = UIPickerView()
    var pickedDistrict: District?
    var operationDistrict: String!
    var operationDistrictCode: Int!
    
    var arrayOfGnsDepartments = [Department]()
    var pickerViewGnsDepartments = UIPickerView()
    var pickedGnsDepartment: Department?
    var operationGnsDepartment: String!
    var operationGnsDepartmentCode: Int!
    
    var arrayOfSFDepartments = [Department]()
    var pickerViewSFDepartments = UIPickerView()
    var pickedSFDepartment: Department?
    var operationSFDepartment: String!
    var operationSFDepartmentCode: Int!
    
    var arrayOfGovernments = [Government]()
    var pickerViewGovernments = UIPickerView()
    var pickedGovernment: Government?
    var operationGovernment: String!
    var operationGovernmentCode: Int!
    var governmentID: Int!
    
    var arrayOfServices = [Service]()
    var pickerViewServices = UIPickerView()
    var pickedService: Service?
    var operationService: String!
    var operationServiceCode: Int!
    
    var arrayOfSFServices = [Service]()
    var pickerViewSFServices = UIPickerView()
    var pickedSFService: Service?
    var operationSFService: String!
    var operationSFServiceCode: Int!
    
    var arrayOfPaymentTypes = [PaymentType]()
    var pickerViewPaymentType = UIPickerView()
    var pickedPaymentType: PaymentType?
    var operationPaymentType: String!
    var operationPaymentTypeCode: Int!
    
    var arrayOfPayerTypes = [PayerType]()
    var pickerViewPayerType = UIPickerView()
    var pickedPayerType: PayerType?
    var operationPayerType: String!
    var operationPayerTypeCode: Int!
    
    //set's picked value if clicks for the second time
    var regionRow = 0
    var districtRow = 0
    var gnsDepartmentRow = 0
    var serviceRow = 0
    
    //Template IBOutlets
    var isCreateTemplate = false
    var isSchedule = false
    var StartDate = ""
    var EndDate: String? = ""
    var RecurringTypeID = 2
    var ProcessDay = 1
    var schedule: Schedule!
    //EditingTemplate
    var isEditTemplate = false
    var template: ContractModelTemplate!
    //RepeatPayTemplate
    var isRepeatPay = false
    var isRepeatFromStory = false
    var operation: ModelOperation!
    var operationModel: UtilityPaymentOperationModel!
    var isCreateTemplateFromStory = false
    var isRepeatOperation = false
    var isChangeData = false

    var regionID: Int? = nil
    var departmentID: Int? = nil
    var districtID: Int? = nil
    var okpoCodeID: Int? = nil
    var districtCode: String? = nil
    var serviceID: Int? = nil
    var serviceSFID: Int? = nil
    var payerTypeID: Int? = nil
    var governmentCode: String? = nil
    var paymentTypeID: Int? = nil
    var paymentCode: String? = nil
    var departmentSFID: Int? = nil
    
    @IBOutlet weak var labelErrorTemplateName: UILabel!
    @IBOutlet weak var viewTemplateNameTextFields: UIView!
    @IBOutlet weak var TemplateDescriptionView: UIView!
    @IBOutlet weak var ScheduleView: UIView!
    @IBOutlet weak var templateNameTextField: UITextField!
    
    @IBOutlet weak var switchPlanner: UISwitch!
    @IBOutlet weak var viewPlanner: UIView!
    @IBOutlet weak var periodTextField: UITextField!
    
    @IBOutlet weak var viewDateBegin: UIView!
    @IBOutlet weak var viewDateFinish: UIView!
    @IBOutlet weak var viewWeekDay: UIView!
    @IBOutlet weak var viewDay: UIView!
    @IBOutlet weak var viewDateOperation: UIView!
    
    @IBOutlet weak var dynamicView: UIView!
    @IBOutlet weak var dynamicViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var requisitView: UIView!
    @IBOutlet weak var dateBeginTextField: UITextField!
    @IBOutlet weak var dateFinishTextField: UITextField!
    
    @IBOutlet weak var dayNumberTextField: UITextField!
    @IBOutlet weak var dateOperationTextField: UITextField!
    @IBOutlet weak var weekDayTextField: UITextField!
    
    //schedule error
    @IBOutlet weak var viewDateBeginTextField: UIView!
    @IBOutlet weak var viewDateEndTextField: UIView!
    @IBOutlet weak var viewDayWeekTextField: UIView!
    @IBOutlet weak var viewDayMonthTextField: UIView!
    @IBOutlet weak var viewOperationDateTextField: UIView!
    
    @IBOutlet weak var createSwiftButton: RoundedButton!
    var typeOperation = ""
    
    var pickerView = UIPickerView()
    var pickerViewDayOperation = UIPickerView()
    var pickerViewWeekDay = UIPickerView()
    var pickerTarif = UIPickerView()
    
    var periodList = ["EachWeek".localized, "EachMonth".localized, "OneTime".localized]
    var daysList = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20","21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"]
    var weekList = ["monday".localized, "tuesday".localized, "wednesday".localized, "thursday".localized, "friday".localized, "saturday".localized, "sunday".localized]
    let datePicker = UIDatePicker()
    
    // select Template category
    var category: CategoriesType!
    var utility: UtilityModel!
    
    var currencyIDcurrent = 0
    var accountNoCurrent = ""
    var isNewPayFromFianance = false
    
    var beneficiaryFullName = ""
    
    @IBOutlet weak var viewUtilityCategory: UIView!
    @IBOutlet weak var categoryTemplateTextField: UITextField!
    @IBOutlet weak var utilityTemplateTextField: UITextField!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        allowToCreatAndSaveOperation(button: createSwiftButton)
        
        if operationModel != nil {
            //1203 = GNS //1163 - Soc fond
            if operationModel.providerID == 1163 || operationModel.providerID == 1203
            {
                if operationModel.providerID == 1163 {  
                    gnsView.isHidden = true
                    socFondView.isHidden = false
                    fullNameView.isHidden = true
                    okpoView.isHidden = true
                    paymentPurposeView.isHidden = true
                } else {
                    gnsView.isHidden = false
                    socFondView.isHidden = true
                    fullNameView.isHidden = false
                    okpoView.isHidden = false
                    paymentPurposeView.isHidden = false
                }
                getPaymentModel(providerID: operationModel.providerID!)
            }
        }
        
        if isCreateTemplate{
            viewUtilityCategory.isHidden = false
            TemplateDescriptionView.isHidden = false
            ScheduleView.isHidden = false
            LogoView.isHidden = true
            
            if isCreateTemplateFromStory{
                
                categoryTemplateTextField.text = operation.category
                utilityTemplateTextField.text = operation.service
                phoneTextField.text = operation.personalAccountNo
                commentTextField.text = operation.comment
                sumPaymentTextField.text = "\(operation.sum ?? 0.0)"
                templateNameTextField.text = operation.templateName
                contragentID = operation.contragentID
                providerID = operation.providerID ?? 0
                operationID = operation.operationID ?? 0
                
            }else if operation != nil{
                categoryTemplateTextField.text = operation.category
                utilityTemplateTextField.text = operation.service
                phoneTextField.text = operation.personalAccountNo
                commentTextField.text = operation.comment
                sumPaymentTextField.text = "\(operation.sum ?? 0.0)"
                templateNameTextField.text = operation.templateName
                contragentID = operation.contragentID
                providerID = operation.providerID ?? 0
                operationID = operation.operationID ?? 0
                
            }
            if selectedDeposit != nil{
                let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                payFrom_accountLabel.text = fullAccountsText
                choiceCard_accountLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
                
                for label in CurrencyLabel{
                    label.text = selectedDeposit.currencySymbol
                }
            }
            createSwiftButton.setTitle(localizedText(key: "save_template"),for: .normal)
            
        }else if isEditTemplate{
            viewUtilityCategory.isHidden = true
            TemplateDescriptionView.isHidden = false
            ScheduleView.isHidden = false
            RecurringTypeID = template.schedule?.recurringTypeID ?? 1
            if template != nil{
                if let logo = self.template.imageName{
                    getImageFromUrl(imageString: logo, imageview: logoImage)
                }
                
                //MARK: - Start Edit Template
                UserDefaultsHelper.listUtilities?.forEach({ model in
                    sumFromLabel.text = "\(String(describing: model.min ?? 0))"
                    sumToLabel.text = "\(String(describing: model.max ?? 0))"
                })
                if template.isSchedule ?? false{
                    isSchedule = true
                    StartDate = template.schedule?.startDate ?? ""
                    EndDate = template.schedule?.endDate
                    RecurringTypeID = template.schedule?.recurringTypeID ?? 0
                    ProcessDay = template.schedule?.processDay ?? 0
                    switchPlanner.isOn = true
                    viewPlanner.isHidden = false
                    operationID = template.operationID ?? 0
                    
                    let schedule = ScheduleHelper.shared.scheduleFullScheduleHelper(
                        StartDate: template.schedule?.startDate,
                        EndDate: template.schedule?.endDate,
                        RecurringTypeID: template.schedule?.recurringTypeID,
                        ProcessDay: template.schedule?.processDay)
                    
                    switch RecurringTypeID {
                        
                    case ScheduleRepeat.OneTime.rawValue:
                        viewDateBegin.isHidden = true
                        viewDateFinish.isHidden = true
                        viewWeekDay.isHidden = true
                        viewDay.isHidden = true
                        viewDateOperation.isHidden = false
                        periodTextField.text = periodList[2]
                        dateOperationTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                        
                    case ScheduleRepeat.EachWeek.rawValue:
                        viewDateBegin.isHidden = false
                        viewDateFinish.isHidden = false
                        viewWeekDay.isHidden = false
                        viewDay.isHidden = true
                        viewDateOperation.isHidden = true
                        periodTextField.text = periodList[0]
                        dateBeginTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                        dateFinishTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.endDate ?? "")
                        weekDayTextField.text = weekList[(schedule?.processDay ?? 1) - 1]
                        
                    case ScheduleRepeat.EachMonth.rawValue:
                        viewDateBegin.isHidden = false
                        viewDateFinish.isHidden = false
                        viewWeekDay.isHidden = true
                        viewDay.isHidden = false
                        viewDateOperation.isHidden = true
                        periodTextField.text = periodList[1]
                        dateBeginTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.startDate ?? "")
                        dateFinishTextField.text = ScheduleHelper.shared.dateForFullSchedule(stringDate: schedule?.endDate ?? "")
                        dayNumberTextField.text = daysList[(schedule?.processDay ?? 1) - 1]
                        
                    default:
                        viewDateBegin.isHidden = true
                        viewDateFinish.isHidden = true
                        viewWeekDay.isHidden = true
                        viewDay.isHidden = true
                        viewDateOperation.isHidden = true
                        
                    }
                }
                if selectedDeposit != nil{
                    let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                    payFrom_accountLabel.text = fullAccountsText
                    choiceCard_accountLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
                    
                    for label in CurrencyLabel{
                        label.text = selectedDeposit.currencySymbol
                    }
                }
            }
            providerID = template.operationParameters?[0].value ?? 0
            contragentID = template.operationParameters?[1].value
            templateNameTextField.text = template.name
            sumPaymentTextField.text = "\(String(describing: template.amount ?? 0.0))"
            nameCategoryLabel.text = serviceNameBy(id: template.operationParameters?[1].value ?? 0, listUtilities: listUtilities)
            categoryTemplateTextField.text = template.operationParameters?[0].key
            utilityTemplateTextField.text = template.operationParameters?[1].key
            phoneTextField.text = template.to
            commentTextField.text = template.comment
            operationID = template.operationID ?? 0
            getOperation(operationID: template.operationID!)
            
            createSwiftButton.setTitle(localizedText(key: "edit_template"),for: .normal)
            
        }else if isRepeatPay{
            viewUtilityCategory.isHidden = true
            TemplateDescriptionView.isHidden = true
            ScheduleView.isHidden = true
            
            if template != nil{
                if let logo = self.template.imageName{
                    getImageFromUrl(imageString: logo, imageview: logoImage)
                    imageUtility = logo
                }
                getImageFromUrl(imageString: imageUtility, imageview: logoImage)
                providerID = template.operationParameters?[0].value ?? 0
                contragentID = template.operationParameters?[1].value
                
                templateNameTextField.text = template.name
                
                //1203 = GNS //1163 - Soc fond
                if operationModel.providerID == 1163 || operationModel.providerID == 1203
                {
                    if operationModel.providerID == 1163 {
                        gnsView.isHidden = true
                        socFondView.isHidden = false
                        fullNameView.isHidden = true
                        okpoView.isHidden = true
                        paymentPurposeView.isHidden = true
                    } else {
                        gnsView.isHidden = false
                        socFondView.isHidden = true
                        fullNameView.isHidden = false
                        okpoView.isHidden = false
                        paymentPurposeView.isHidden = false
                    }
                    getPaymentModel(providerID: operationModel.providerID!)
                }
                
                sumPaymentTextField.text = "\(String(describing: template.amount ?? 0.0))"
                payFrom_accountLabel.text = "\(String(describing: template.from ?? ""))"
                nameCategoryLabel.text = serviceNameBy(id: template.operationParameters?[1].value ?? 0, listUtilities: listUtilities)
                categoryTemplateTextField.text = template.operationParameters?[0].key
                utilityTemplateTextField.text = template.operationParameters?[1].key
                phoneTextField.text = template.to
                commentTextField.text = template.comment
                if selectedDeposit != nil{
                    let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                    payFrom_accountLabel.text = fullAccountsText
                    choiceCard_accountLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
                    for label in CurrencyLabel{
                        label.text = selectedDeposit.currencySymbol
                    }
                    let textNonZeroFirst = sumPaymentTextField.text
                    let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
                    let numberAsInt = Double(newString ?? "0")
                    sumPaymentTextField.text = "\(numberAsInt ?? 0)"
                    
                    if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                        showAlertController(localizedText(key: "insufficient_account_balance"))
                        sumPaymentTextField.text = ""
                    }

                }else{
                    payFrom_accountLabel.text = localizedText(key: "pay_from")
                    choiceCard_accountLabel.text = localizedText(key: "select_account_or_card")
                }
                var fields: [ScenarioFieldValueModel] = []
                var count: Int = 0
                for item in self.scenarious {
                    fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: textFields[count].text))
                    count += 1
                }
                let pay_CheckModel = UtilityPaymentOperationModel(
                    providerID: providerID,
                    personalAccountNo: phoneTextField.text ?? "0000",
                    contragentID: contragentID,
                    sum: template.amount ?? 0.0,
                    fields: fields)
                
                self.postCheckUtility(Pay_CheckModel: pay_CheckModel)

                
            }else if isRepeatFromStory{
                if operation != nil{

                    getImageFromUrl(imageString: imageUtility, imageview: logoImage)
                    providerID = operation.providerID ?? 0
                    contragentID = operation.contragentID
                    
                    templateNameTextField.text = operation.templateName
                    sumPaymentTextField.text = "\(String(describing: operation.sum ?? 0.0))"
                    payFrom_accountLabel.text = "\(String(describing: operation.accountNo ?? ""))"
                    nameCategoryLabel.text = operation.service
                    categoryTemplateTextField.text = operation.service
                    utilityTemplateTextField.text = operation.category
                    phoneTextField.text = operation.personalAccountNo
                    commentTextField.text = operation.comment
                }
                if selectedDeposit != nil{
                    let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                    payFrom_accountLabel.text = fullAccountsText
                    choiceCard_accountLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
                    for label in CurrencyLabel{
                        label.text = selectedDeposit.currencySymbol
                    }
                    let textNonZeroFirst = sumPaymentTextField.text
                    let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
                    let numberAsInt = Double(newString ?? "0")
                    sumPaymentTextField.text = "\(numberAsInt ?? 0)"
                    if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                        showAlertController(localizedText(key: "insufficient_account_balance"))
                        sumPaymentTextField.text = ""
                    }

                }else{
                    payFrom_accountLabel.text = localizedText(key: "pay_from")
                    choiceCard_accountLabel.text = localizedText(key: "select_account_or_card")
                }
                var fields: [ScenarioFieldValueModel] = []
                var count: Int = 0
                for item in self.scenarious {
                    fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: textFields[count].text))
                    count += 1
                }
                let pay_CheckModel = UtilityPaymentOperationModel(
                    providerID: providerID,
                    personalAccountNo: phoneTextField.text ?? "0000",
                    contragentID: contragentID,
                    sum: operation.sum ?? 0.0,
                    fields: fields)
                
                self.postCheckUtility(Pay_CheckModel: pay_CheckModel)

            }else if isChangeData{
                if operation != nil{
                    
                    getImageFromUrl(imageString: imageUtility, imageview: logoImage)
                    providerID = operation.providerID ?? 0
                    contragentID = operation.contragentID
                    
                    templateNameTextField.text = operation.templateName
                    sumPaymentTextField.text = "\(String(describing: operation.sum ?? 0.0))"
                    payFrom_accountLabel.text = "\(String(describing: operation.accountNo ?? ""))"
                    nameCategoryLabel.text = operation.service
                    categoryTemplateTextField.text = operation.service
                    utilityTemplateTextField.text = operation.category
                    phoneTextField.text = operation.personalAccountNo
                    commentTextField.text = operation.comment
                    
                }
            }else if isRepeatOperation{
                if payData != nil{
                    getImageFromUrl(imageString: imageUtility, imageview: logoImage)
                    providerID = payData.providerID ?? 0
                    contragentID = payData.contragentID
                    
                    templateNameTextField.text = payData.templateName
                    sumPaymentTextField.text = "\(String(describing: payData.sum ?? 0.0))"
                    payFrom_accountLabel.text = "\(String(describing: payData.accountNo ?? ""))"
                    phoneTextField.text = payData.personalAccountNo
                    commentTextField.text = payData.comment
                }
                if selectedDeposit != nil{
                    let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                    payFrom_accountLabel.text = fullAccountsText
                    choiceCard_accountLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
                    for label in CurrencyLabel{
                        label.text = selectedDeposit.currencySymbol
                    }
                    
                    let textNonZeroFirst = sumPaymentTextField.text
                    let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
                    let numberAsInt = Double(newString ?? "0")
                    sumPaymentTextField.text = "\(numberAsInt ?? 0)"
                    
                    if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                        showAlertController(localizedText(key: "insufficient_account_balance"))
                        sumPaymentTextField.text = ""
                    }
                    
                }else{
                    payFrom_accountLabel.text = localizedText(key: "pay_from")
                    choiceCard_accountLabel.text = localizedText(key: "select_account_or_card")
                }
        
            }
           
        }else{
            viewUtilityCategory.isHidden = true
            TemplateDescriptionView.isHidden = true
            ScheduleView.isHidden = true
            
            if selectedDeposit != nil{
                let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
                payFrom_accountLabel.text = fullAccountsText
                choiceCard_accountLabel.text = "\(String(format:"%.2f",selectedDeposit.balance ?? 0.0)) \(String(describing: selectedDeposit.currencySymbol ?? "")) "
                for label in CurrencyLabel{
                    label.text = selectedDeposit.currencySymbol
                }
            }else{
                payFrom_accountLabel.text = localizedText(key:"pay_from")
                choiceCard_accountLabel.text = localizedText(key: "select_account_or_card")
            }
            providerID = (self.currentUtility.parentID == nil ? self.currentUtility.ID : self.currentUtility.parentID)!
            contragentID = self.currentUtility.parentID == nil ? self.currentUtility.parentID : self.currentUtility.ID
            parentID = self.currentUtility.parentID
            //MARK: - sumToLable
            nameCategoryLabel.text = currentUtility.name
            sumFromLabel.text = "\(String(describing: currentUtility.min ?? 0))"
            sumToLabel.text = "\(String(describing: currentUtility.max ?? 0))"
            if let logo = currentUtility.logo{
                getImageFromUrl(imageString: logo, imageview: logoImage)
                phoneTextField.placeholder = currentUtility.placeholder
                regularExpression = currentUtility.inputValidate ?? ""
                imageUtility = logo
            }
        }
    }
    
    private func setDynamicField() {
        
        textFields = (0 ..< scenarious.count).map { _ in UITextField() }
        for i in 0..<scenarious.count {
            textFields[i].borderStyle = .roundedRect
            
            textFields[i].translatesAutoresizingMaskIntoConstraints = false
            textFields[i].placeholder = scenarious[i].nameToDisplay
            textFields[i].tag = scenarious[i].fieldID!
            if(operation != nil) {
                textFields[i].text = operation.fields![i].value
            } else if operationModel != nil {
                textFields[i].text = operationModel.fields![i].value
            }
            dynamicView.addSubview(textFields[i])
            
            if i == 0 {
                NSLayoutConstraint.activate([
                    textFields[i].topAnchor.constraint(equalTo: dynamicView.topAnchor),
                    textFields[i].leadingAnchor.constraint(equalTo: dynamicView.leadingAnchor, constant: 16),
                    textFields[i].trailingAnchor.constraint(equalTo: dynamicView.trailingAnchor, constant: 16),
                    textFields[i].heightAnchor.constraint(equalToConstant: 48)
                ])
            } else if i == scenarious.count - 1 {
                NSLayoutConstraint.activate([
                    textFields[i].topAnchor.constraint(equalTo: textFields[i - 1].bottomAnchor),
                    textFields[i].leadingAnchor.constraint(equalTo: dynamicView.leadingAnchor, constant: 16),
                    textFields[i].trailingAnchor.constraint(equalTo: dynamicView.trailingAnchor, constant: 16),
                    textFields[i].bottomAnchor.constraint(equalTo: dynamicView.bottomAnchor, constant: 12),
                    textFields[i].heightAnchor.constraint(equalToConstant: 48)
                ])
            } else {
                NSLayoutConstraint.activate([
                    textFields[i].topAnchor.constraint(equalTo: textFields[i - 1].bottomAnchor),
                    textFields[i].leadingAnchor.constraint(equalTo: dynamicView.leadingAnchor, constant: 16),
                    textFields[i].trailingAnchor.constraint(equalTo: dynamicView.trailingAnchor, constant: 16),
                    textFields[i].heightAnchor.constraint(equalToConstant: 48)
                ])
            }
            dynamicViewConstraint.constant += 48
            textFields[i].layoutIfNeeded()
            
        }
        dynamicView.translatesAutoresizingMaskIntoConstraints = false
        dynamicView.layoutIfNeeded()
    }
    
    private func sizeHeaderToFit(headerView: UIView?) -> CGFloat {
           guard let headerView = headerView else {
               return 0.0
           }

           headerView.setNeedsLayout()
           headerView.layoutIfNeeded()

           let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height

           return height
       }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        hideKeyboardWhenTappedAround()
        periodList = [localizedText(key: "every_week"),localizedText(key: "every_month"), NSLocalizedString("_retry", comment: "Однократно")]
        weekList = [localizedText(key: "mon"),
                    localizedText(key:"tue"),
                    localizedText(key:"wed"),
                    localizedText(key:"th"),
                    localizedText(key:"fri"),
                    localizedText(key:"sat"),
                    localizedText(key:"the_sun")]
        
        if let listUtilities = UserDefaultsHelper.listUtilities{
            self.listUtilities = listUtilities
        }
        
        if currentUtility != nil {
            //1203 = GNS //1163 - Soc fond
            if (currentUtility.ID == 1163 || currentUtility.ID == 1203) && currentUtility.parentID == nil
            {
                if currentUtility.ID == 1163 {
                    gnsView.isHidden = true
                    socFondView.isHidden = false
                    fullNameView.isHidden = false
                    okpoView.isHidden = false
                    paymentPurposeView.isHidden = true
                    requisiteLabel.text = "ИНН"
                } else {
                    gnsView.isHidden = false
                    socFondView.isHidden = true
                    fullNameView.isHidden = false
                    okpoView.isHidden = false
                    paymentPurposeView.isHidden = false
                    requisiteLabel.text = "ИНН"
                }
                getPaymentModel(providerID: currentUtility.ID!)
            }
        }
        
        getAccounts()
        if currentUtility != nil {
            if ((currentUtility != nil && !specialUtilities.contains(currentUtility.ID!))
            || (operationModel != nil && !specialUtilities.contains(operationModel.providerID!))) {
                var scenarioContragentID: Int? = nil
                if(currentUtility != nil){
                    scenarioContragentID = currentUtility.ID
                } else if(operationModel != nil){
                    scenarioContragentID = operationModel.contragentID
                } else if(operation != nil){
                    scenarioContragentID = operation.contragentID
                }
                if scenarioContragentID != nil {
                    getScenario(contragentID: scenarioContragentID!)
                }
            }
        }
        
        templateNameTextField.delegate = self
        phoneTextField.delegate = self
        sumPaymentTextField.delegate = self
        commentTextField.delegate = self
        customImage(view: viewImage, image: logoImage)
        
        
        phoneTextField.addTarget(self, action: #selector(textFieldEditingEnd(_:)), for: UIControl.Event.editingDidEnd)
        sumPaymentTextField.addTarget(self, action: #selector(sumTextFieldEditingEnd(_:)), for: UIControl.Event.editingDidEnd)
        templateNameTextField.addTarget(self, action: #selector(textFieldEditingEndtemplateName(_:)), for: UIControl.Event.editingDidEnd)
        
        //schedule
        viewPlanner.isHidden = true
        viewDateBegin.isHidden = true
        viewDateFinish.isHidden = true
        viewWeekDay.isHidden = true
        viewDay.isHidden = true
        
        viewDateOperation.isHidden = true
        dateBeginTextField.delegate = self
        dateFinishTextField.delegate = self
        dayNumberTextField.delegate = self
        weekDayTextField.delegate = self
        
        pickerGns()
        
        pickerPeriodDate()
        showDatePicker()
    }
    // Проверка поля "Название шаблона" на наличие данных, после потери фокуса
    @objc func textFieldEditingEndtemplateName(_ sender: Any) {
        templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
    }
    // Проверка поля  после потери фокуса
    @objc func textFieldEditingEnd(_ sender: Any) {
        phoneTextField.stateIfEmpty(view: viewNoTextField, labelError: labelErrorNo)
        let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
        let checkModel: UtilityPaymentOperationModel = UtilityPaymentOperationModel(
            providerID: providerID,
            personalAccountNo: phoneTextField.text ?? "",
            contragentID: contragentID,
            sum: sumDouble ?? 0.0,
            fields: nil)
        
        if (providerID == 1203 || providerID == 1163) && parentID == nil {
            if (providerID == 1163) {
                checkModel.serviceID = self.serviceSFID
                checkModel.governmentCode = self.governmentCode
                checkModel.payerTypeID = self.payerTypeID
                checkModel.paymentTypeID = self.paymentTypeID
                checkModel.departmentID = self.departmentSFID
                checkModel.paymentCode = self.paymentCode
            }
            postCheckUtility(Pay_CheckModel: checkModel)
        }
    }
    
    // Проверка поля "сумма" на наличие данных, после потери фокуса
    @objc func sumTextFieldEditingEnd(_ sender: Any) {
        let textNonZeroFirst = sumPaymentTextField.text
        let newString = textNonZeroFirst?.replacingOccurrences(of: ",", with: ".")
        let numberAsInt = Double(newString ?? "0")
        sumPaymentTextField.text = "\(numberAsInt ?? 0)"
        if selectedDeposit != nil{
            if (numberAsInt ?? 0) > Double(selectedDeposit.balance ?? 0.0){
                showAlertController(localizedText(key: "insufficient_account_balance"))
                sumPaymentTextField.text = ""
            }
        }
        sumPaymentTextField.stateIfEmpty(view: viewSumTextField, labelError: labelErrorSum)
        if sumPaymentTextField.text == "" || sumPaymentTextField.text == "0" {
            totalSumLabel.text = "0.0"
            comissionLabel.text = "0.0"
        }
        
        if selectedDeposit != nil{
            if self.isSchedule{
                self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
            }else{
                self.schedule = nil
            }
            var fields: [ScenarioFieldValueModel] = []
            var count: Int = 0
            for item in self.scenarious {
                fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: textFields[count].text))
                count += 1
            }
            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
            let commissionModel: UtilityPaymentOperationModel = UtilityPaymentOperationModel(
                accountNo: selectedDeposit.accountNo ?? "0000",
                currencyID:  Constants.NATIONAL_CURRENCY,
                providerID: providerID,
                contragentID: contragentID,
                personalAccountNo: phoneTextField.text ?? "123",
                сurrentAccountNo: nil,
                sum: sumDouble ?? 0.0,
                paymentSum: nil,
                
                commission: Double(comissionLabel.text ?? "0.0"),
                taxDepartmentID: departmentID,
                departmentID: departmentID,
                OKPOCode: okpoField.text,
                taxServiceID: serviceID,
                
                comment: self.commentTextField.text ?? nil,
                operationID: nil,
                isSchedule: self.isSchedule,
                isTemplate: self.isCreateTemplate,
                templateName: self.templateNameTextField.text ?? nil,
                templateDescription: "Оплата коммунальной услуги",
                scheduleID: nil,
                schedule: self.schedule ?? nil,
                fullName: self.beneficiaryFullName,
                fields: fields)
            commissionModel.serviceID = self.serviceSFID
            commissionModel.governmentCode = self.governmentCode
            commissionModel.payerTypeID = self.payerTypeID
            commissionModel.paymentTypeID = self.paymentTypeID
            postCheckUtility(Pay_CheckModel: commissionModel)
            getCommission(commissionModel: commissionModel)
        }else{
            showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
        }
    }
    
     func getComment() -> String {
        return "\(departmentField.text!) (БИК: \(gnsDepartmentField.text!))" +
        " код платежа: \(serviceField.text!)), \(fullNameField.text!), ИНН: \(phoneTextField.text!)"
    }
    
    //    провека на наличие regularExpression
    func isValidProp(phone: String) -> Bool {
        
        let phoneRegex = regularExpression
        let valid = NSPredicate(format: "SELF MATCHES %@", phoneRegex).evaluate(with: phone)
        return valid
    }
    // Проверка поля "сумма" на наличие данных,  при фокусе
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField{
        case self.phoneTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewNoTextField, labelError: labelErrorNo)
        case self.sumPaymentTextField:
            viewTextFieldShouldBeginEditing(viewTextField: viewSumTextField, labelError: labelErrorSum)
        default:
            break
        }
        return true
    }
    
    //    переход на следкующее поле
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == templateNameTextField {
            textField.resignFirstResponder()
        }
        return true
    }
    
    //TaxDepartmentID.subscribe()
    
    @IBAction func utilityButton(_ sender: Any) {
        if categoryTemplateTextField.text == ""{
            showAlertController(self.localizedText(key: "select_service_category"))
        }else{
            performSegue(withIdentifier: "toUtilitiesSegue", sender: self)
        }
    }
    
    private func setComment(userTin: String?) {
        if(currentUtility.taxProviderTypeID == TaxProviderType.TransportTax.rawValue){
            commentTextField.text =  "Гос. номер авто: \(userTin!) - \(labelErrorNo.text!)"
        }else{
            commentTextField.text = labelErrorNo.text!
        }
    }
    
    public func getUserName() -> String?{
        return labelErrorNo.text
    }
    
    @IBAction func PayButton(_ sender: Any) {
        if isCreateTemplate{
            if sumPaymentTextField.text == "" || phoneTextField.text == "" || templateNameTextField.text == ""{
                sumPaymentTextField.stateIfEmpty(view: viewSumTextField, labelError: labelErrorSum)
                phoneTextField.stateIfEmpty(view: viewNoTextField, labelError: labelErrorNo)
                templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
            }else{
                if selectedDeposit != nil{
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: operationID, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    var fields: [ScenarioFieldValueModel] = []
                    var count: Int = 0
                    for item in self.scenarious {
                        fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: textFields[count].text))
                        count += 1
                    }
                    if regularExpression == ""{
                        let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                        payData = UtilityPaymentOperationModel(
                            accountNo: selectedDeposit.accountNo ?? "0000",
                            currencyID:  Constants.NATIONAL_CURRENCY,
                            providerID: providerID,
                            contragentID: contragentID,
                            personalAccountNo: phoneTextField.text ?? "123",
                            сurrentAccountNo: nil,
                            sum: sumDouble ?? 0.0,
                            paymentSum: nil,
                            
                            commission: Double(comissionLabel.text ?? "0.0"),
                            taxDepartmentID: departmentID,
                            departmentID: departmentID,
                            OKPOCode: okpoField.text,
                            taxServiceID: serviceID,
                            
                            comment: commentTextField.text ?? nil,
                            operationID: 0,
                            isSchedule: false,
                            isTemplate: self.isCreateTemplate,
                            templateName: self.templateNameTextField.text ?? nil,
                            templateDescription: "Оплата коммунальной услуги",
                            scheduleID: 0,
                            schedule: self.schedule ?? nil,
                            fullName: self.beneficiaryFullName,
                            fields: fields)
                        payData.serviceID = self.serviceSFID
                        payData.governmentCode = self.governmentCode
                        payData.payerTypeID = self.payerTypeID
                        payData.paymentTypeID = self.paymentTypeID
                        postPay(checkModel: payData)
                    }else{
                        if isValidProp(phone: phoneTextField.text ?? ""){
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                            payData = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123",
                                сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(self.comissionLabel.text ?? "0.0"),
                                taxDepartmentID: self.departmentID,
                                departmentID: self.departmentID,
                                OKPOCode: self.okpoField.text,
                                taxServiceID: self.serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: 0,
                                isSchedule: false,
                                isTemplate: self.isCreateTemplate,
                                templateName: self.templateNameTextField.text ?? nil,
                                templateDescription: "Оплата коммунальной услуги",
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            payData.serviceID = self.serviceSFID
                            payData.governmentCode = self.governmentCode
                            payData.payerTypeID = self.payerTypeID
                            payData.paymentTypeID = self.paymentTypeID
                            postPay(checkModel: payData)
                        }else{
                            print("phone is not valid")
                            let alert = UIAlertController(title: self.localizedText(key: "incorrectly_specified_props_check_again"), message: "", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
                                self.phoneTextField.becomeFirstResponder()
                            }
                            alert.addAction(action)
                            present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }else{
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
        }else if isEditTemplate{
            if sumPaymentTextField.text == "" || phoneTextField.text == "" || templateNameTextField.text == ""{
                sumPaymentTextField.stateIfEmpty(view: viewSumTextField, labelError: labelErrorSum)
                phoneTextField.stateIfEmpty(view: viewNoTextField, labelError: labelErrorNo)
                templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
            }else{
                if selectedDeposit != nil{
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: operationID, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    var fields: [ScenarioFieldValueModel] = []
                    var count: Int = 0
                    for item in self.scenarious {
                        fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: textFields[count].text))
                        count += 1
                    }
                    if regularExpression == ""{
                        let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                        payData = UtilityPaymentOperationModel(
                            accountNo: selectedDeposit.accountNo ?? "0000",
                            currencyID: Constants.NATIONAL_CURRENCY,
                            providerID: providerID,
                            contragentID: contragentID,
                            personalAccountNo: phoneTextField.text ?? "123",
                            сurrentAccountNo: nil,
                            sum: sumDouble ?? 0.0,
                            paymentSum: nil,
                            
                            commission: Double(comissionLabel.text ?? "0.0"),
                            taxDepartmentID: departmentID,
                            departmentID: departmentID,
                            OKPOCode: okpoField.text,
                            taxServiceID: serviceID,
                            
                            comment: commentTextField.text ?? nil,
                            operationID: template.operationID,
                            isSchedule: false,
                            isTemplate: true,
                            templateName: self.templateNameTextField.text ?? nil,
                            templateDescription: "Оплата коммунальной услуги",
                            scheduleID: 0,
                            schedule: self.schedule ?? nil,
                            fullName: self.beneficiaryFullName,
                            fields: fields)
                        payData.serviceID = self.serviceSFID
                        payData.governmentCode = self.governmentCode
                        payData.payerTypeID = self.payerTypeID
                        payData.paymentTypeID = self.paymentTypeID
                        postPay(checkModel: payData)
                    }else{
                        if isValidProp(phone: phoneTextField.text ?? ""){
                        }else{
                            print("phone is not valid")
                            let alert = UIAlertController(title: self.localizedText(key: "incorrectly_specified_props_check_again"), message: "", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
                                self.phoneTextField.becomeFirstResponder()
                            }
                            alert.addAction(action)
                            present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }else{
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
            
        }else if isRepeatPay{
            if sumPaymentTextField.text == "" || phoneTextField.text == ""{
                sumPaymentTextField.stateIfEmpty(view: viewSumTextField, labelError: labelErrorSum)
                phoneTextField.stateIfEmpty(view: viewNoTextField, labelError: labelErrorNo)
                templateNameTextField.stateIfEmpty(view: viewTemplateNameTextFields, labelError: labelErrorTemplateName)
                showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
            }else{
                if selectedDeposit != nil{
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    var fields: [ScenarioFieldValueModel] = []
                    var count: Int = 0
                    for item in self.scenarious {
                        fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: textFields[count].text))
                        count += 1
                    }
                    if regularExpression == ""{
                        if isRepeatFromStory{
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                            payData = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123", сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(comissionLabel.text ?? "0.0"),
                                taxDepartmentID: departmentID,
                                departmentID: departmentID,
                                OKPOCode: okpoField.text,
                                taxServiceID: serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: 0,
                                isSchedule: self.isSchedule,
                                isTemplate: false,
                                templateName: nil,
                                templateDescription: nil,
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            payData.serviceID = self.serviceSFID
                            payData.governmentCode = self.governmentCode
                            payData.payerTypeID = self.payerTypeID
                            payData.paymentTypeID = self.paymentTypeID
                        }else if isChangeData{
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                            payData = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123", сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(comissionLabel.text ?? "0.0"),
                                taxDepartmentID: departmentID,
                                departmentID: departmentID,
                                OKPOCode: okpoField.text,
                                taxServiceID: serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: self.operation.operationID,
                                isSchedule: self.isSchedule,
                                isTemplate: false,
                                templateName: nil,
                                templateDescription: nil,
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            payData.serviceID = self.serviceSFID
                            payData.governmentCode = self.governmentCode
                            payData.payerTypeID = self.payerTypeID
                            payData.paymentTypeID = self.paymentTypeID
                        }else if isRepeatOperation{
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                            payData = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123", сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(comissionLabel.text ?? "0.0"),
                                taxDepartmentID: departmentID,
                                departmentID: departmentID,
                                OKPOCode: okpoField.text,
                                taxServiceID: serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: 0,
                                isSchedule: self.isSchedule,
                                isTemplate: false,
                                templateName: nil,
                                templateDescription: nil,
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            
                            payData.serviceID = self.serviceSFID
                            payData.governmentCode = self.governmentCode
                            payData.payerTypeID = self.payerTypeID
                            payData.paymentTypeID = self.paymentTypeID
                        }else{
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                            payData = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123", сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(comissionLabel.text ?? "0.0"),
                                taxDepartmentID: departmentID,
                                departmentID: departmentID,
                                OKPOCode: okpoField.text,
                                taxServiceID: serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: 0,
                                isSchedule: self.isSchedule,
                                isTemplate: false,
                                templateName: nil,
                                templateDescription: nil,
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            
                            payData.serviceID = self.serviceSFID
                            payData.governmentCode = self.governmentCode
                            payData.payerTypeID = self.payerTypeID
                            payData.paymentTypeID = self.paymentTypeID
                        }
                        if self.isRepeatOperation{
                            if operationID != 0{
                                self.hideLoading()
                                self.allowOperation()
                            }else{
                               postPay(checkModel: payData)
                            }
                        }else{
                            postPay(checkModel: payData)
                        }
                    }else{
                        if isValidProp(phone: phoneTextField.text ?? ""){
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                            payData = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123", сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(comissionLabel.text ?? "0.0"),
                                taxDepartmentID: departmentID,
                                departmentID: departmentID,
                                OKPOCode: okpoField.text,
                                taxServiceID: serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: 0,
                                isSchedule: self.isSchedule,
                                isTemplate: false,
                                templateName: nil,
                                templateDescription: nil,
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            
                            payData.serviceID = self.serviceSFID
                            payData.governmentCode = self.governmentCode
                            payData.payerTypeID = self.payerTypeID
                            payData.paymentTypeID = self.paymentTypeID
                            
                            
                             postPay(checkModel: payData)
                        }else{
                            print("phone is not valid")
                            let alert = UIAlertController(title: self.localizedText(key: "incorrectly_specified_props_check_again"), message: "", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
                                self.phoneTextField.becomeFirstResponder()
                            }
                            alert.addAction(action)
                            present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }else{
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
        }else{
            if sumPaymentTextField.text == "" || phoneTextField.text == ""{
                sumPaymentTextField.stateIfEmpty(view: viewSumTextField, labelError: labelErrorSum)
                phoneTextField.stateIfEmpty(view: viewNoTextField, labelError: labelErrorNo)
            }else{
                if selectedDeposit != nil{
                    if self.isSchedule{
                        self.schedule = Schedule(operationID: 0, startDate: self.StartDate, endDate: self.EndDate, recurringTypeID: self.RecurringTypeID, processDay: self.ProcessDay)
                    }else{
                        self.schedule = nil
                    }
                    var fields: [ScenarioFieldValueModel] = []
                    var count: Int = 0
                    for item in self.scenarious {
                        fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: textFields[count].text))
                        count += 1
                    }
                    if regularExpression == ""{
                        if comissionLabel.text == "0.0"{
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                            let commissionModel: UtilityPaymentOperationModel = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123",
                                сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(comissionLabel.text ?? "0.0"),
                                taxDepartmentID: departmentID,
                                departmentID: departmentID,
                                OKPOCode: okpoField.text,
                                taxServiceID: serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: nil,
                                isSchedule: self.isSchedule,
                                isTemplate: self.isCreateTemplate,
                                templateName: self.templateNameTextField.text ?? nil,
                                templateDescription: "Оплата коммунальной услуги",
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            
                            commissionModel.serviceID = self.serviceSFID
                            commissionModel.governmentCode = self.governmentCode
                            commissionModel.payerTypeID = self.payerTypeID
                            commissionModel.paymentTypeID = self.paymentTypeID
                            
                            getCommission(commissionModel: commissionModel)
                        }else{
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                            payData = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123",
                                сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(comissionLabel.text ?? "0.0"),
                                taxDepartmentID: departmentID,
                                departmentID: departmentID,
                                OKPOCode: okpoField.text,
                                taxServiceID: serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: nil,
                                isSchedule: self.isSchedule,
                                isTemplate: self.isCreateTemplate,
                                templateName: self.templateNameTextField.text ?? nil,
                                templateDescription: "Оплата коммунальной услуги",
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            payData.serviceID = self.serviceSFID
                            payData.governmentCode = self.governmentCode
                            payData.payerTypeID = self.payerTypeID
                            payData.paymentTypeID = self.paymentTypeID
                            
                            postPay(checkModel: payData)
                        }
                    }else{
                        if isValidProp(phone: phoneTextField.text ?? ""){
                            let sumDouble = Double(sumPaymentTextField.text  ?? "0.0")
                                payData = UtilityPaymentOperationModel(
                                accountNo: selectedDeposit.accountNo ?? "0000",
                                currencyID:  Constants.NATIONAL_CURRENCY,
                                providerID: providerID,
                                contragentID: contragentID,
                                personalAccountNo: phoneTextField.text ?? "123",
                                сurrentAccountNo: nil,
                                sum: sumDouble ?? 0.0,
                                paymentSum: nil,
                                
                                commission: Double(comissionLabel.text ?? "0.0"),
                                taxDepartmentID: departmentID,
                                departmentID: departmentID,
                                OKPOCode: okpoField.text,
                                taxServiceID: serviceID,
                                
                                comment: commentTextField.text ?? nil,
                                operationID: nil,
                                isSchedule: self.isSchedule,
                                isTemplate: self.isCreateTemplate,
                                templateName: self.templateNameTextField.text ?? nil,
                                templateDescription: "Оплата коммунальной услуги",
                                scheduleID: 0,
                                schedule: self.schedule ?? nil,
                                fullName: self.beneficiaryFullName,
                                fields: fields)
                            
                            payData.serviceID = self.serviceSFID
                            payData.governmentCode = self.governmentCode
                            payData.payerTypeID = self.payerTypeID
                            payData.paymentTypeID = self.paymentTypeID
                            
                            postPay(checkModel: payData)
                        }else{
                            print("phone is not valid")
                            let alert = UIAlertController(title: self.localizedText(key: "incorrectly_specified_props_check_again"), message: "", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
                                self.phoneTextField.becomeFirstResponder()
                            }
                            alert.addAction(action)
                            present(alert, animated: true, completion: nil)
                        }
                    }
                    
                }else{
                    showAlertController(self.localizedText(key: "fill_in_all_the_fields"))
                }
            }
        }
        
    }
    // кнопка включения планировщика
    @IBAction func switchPlanner(_ sender: UISwitch) {
        if sender.isOn{
            viewPlanner.isHidden = false
            isSchedule = true
            StartDate = ScheduleHelper.shared.dateCurrentForServer()
            EndDate = ScheduleHelper.shared.dateCurrentForServer()
            dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
            dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
        }else{
            viewPlanner.isHidden = true
            isSchedule = false
        }
    }
    
    
    @IBAction func toAccountsButton(_ sender: Any) {
        
    }
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toSubmitSegue"{
            let vc = segue.destination as! SubmitPaymentViewController
            vc.delegate = self
            submitModel = SubmitForPayModel(
                name: nameCategoryLabel.text ?? "No Name",
                props: phoneTextField.text ?? "No props",
                account: selectedDeposit?.accountNo ?? "No account",
                contract: "00000",
                sum: sum,
                commission: comissionLabel.text ?? "",
                total: totalSumLabel.text ?? "0",
                description: commentTextField.text ?? "",
                comment: commentTextField.text ?? nil,
                image: imageUtility,
                currencyID: selectedDeposit.currencyID ?? 0)
            vc.operationID = operationID
            vc.submitModel = submitModel
            vc.currentCurrency = selectedDeposit.currencySymbol ?? ""
            vc.selectedDeposit = selectedDeposit
            vc.currentUtility = currentUtility
            vc.payData = payData
            vc.imageUtility = imageUtility
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
            
        }
        if segue.identifier == "toAccountsSegue"{
            let vc = segue.destination as! AccountsViewController
            accounts = accounts.filter{$0.items?.count != 0}
            vc.accounts = accounts
            vc.selectedDeposit = selectedDeposit
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "pay_from")
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
        
        if segue.identifier == "toCategoriesSegue"{
            let vc = segue.destination as! CategoryTemplateViewController
            vc.isCreateTemplate = isCreateTemplate
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "select_a_category")
            
        }
        if segue.identifier == "toUtilitiesSegue"{
            let vc = segue.destination as! UtilityTemplateViewController
            vc.isCreateTemplate = isCreateTemplate
            vc.delegate = self
            vc.navigationTitle = localizedText(key: "choose_a_supplier")
            vc.categotyID = categoryID
        }
        
    }
    //    заполнение полей счета
    func selectedDepositUser(deposit: Deposit) {
        selectedDeposit = deposit
        sumCurrencyLabel.text = selectedDeposit.currencySymbol
        commissionCurrencyLabel.text = selectedDeposit.currencySymbol
        totalCurrencyLabel.text = selectedDeposit.currencySymbol
        currencySymbol.text = selectedDeposit.currencySymbol
        
        var fields: [ScenarioFieldValueModel] = []
        var count: Int = 0
        for item in self.scenarious {
            fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: textFields[count].text))
            count += 1
        }
        
        if(phoneTextField.text != "") {
            if selectedDeposit != nil {
                let sumDouble = Double(sumPaymentTextField.text ?? "0.0")
                let pay_CheckModel = UtilityPaymentOperationModel(
                    providerID: providerID,
                    personalAccountNo: phoneTextField.text ?? "0000",
                    contragentID: contragentID,
                    sum: sumDouble ?? 0.0,
                    fields: fields)
                
                postCheckUtility(Pay_CheckModel: pay_CheckModel)
                if(providerID == 1203 || providerID == 1163) {
                    paymentPurposeField.text = getComment()
                    commentTextField.text = getComment()
                }
            }
        }
    }
    //заполнение полей категорий
    func selectedCategory(category: CategoriesType) {
        self.category = category
        categoryTemplateTextField.text = self.category.categoryName
        utilityTemplateTextField.text = ""
        nameCategoryLabel.text = ""
        categoryID = self.category.categoryID ?? 0
    }
    //заполнение полей услуг
    func selectedUtility(utility: UtilityModel) {
        self.currentUtility = utility
        
        if currentUtility != nil {
            //1203 = GNS //1163 - Soc fond
            if currentUtility.ID == 1163 || currentUtility.ID == 1203
            {
                if currentUtility.ID == 1163 {
                    gnsView.isHidden = true
                    socFondView.isHidden = false
                    fullNameView.isHidden = false
                    okpoView.isHidden = false
                    paymentPurposeView.isHidden = true
                } else {
                    gnsView.isHidden = false
                    socFondView.isHidden = true
                    fullNameView.isHidden = false
                    okpoView.isHidden = false
                    paymentPurposeView.isHidden = false
                }
                getPaymentModel(providerID: currentUtility.ID!)
            }
        }
        
        utilityTemplateTextField.text = self.currentUtility.name
        nameCategoryLabel.text = self.currentUtility.name
        providerID = (self.currentUtility.parentID == nil ? self.currentUtility.ID : self.currentUtility.parentID)!
        contragentID = self.currentUtility.parentID == nil ? self.currentUtility.parentID : self.currentUtility.ID
        if let logo = self.currentUtility.logo{
            getImageFromUrl(imageString: logo, imageview: logoImage)
            phoneTextField.placeholder = currentUtility?.placeholder ?? ""
            regularExpression = currentUtility?.inputValidate ?? ""
            imageUtility = logo
        }
    }
    // Получение данных для редактирования оперции
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool, payData: UtilityPaymentOperationModel) {
        self.operationID = operationID
        self.isRepeatPay = isRepeatPay
        self.isRepeatOperation = isRepeatOperation
        self.payData = payData
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
        let currentCharacterCount = textField.text?.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.count - range.length
        var maxLength = 0
        if textField.isEqual(phoneTextField) {
            maxLength = 20
        }else if textField.isEqual(sumPaymentTextField) {
            maxLength = 9
        }else if textField.isEqual(commentTextField) {
            maxLength = 100
        }else if textField.isEqual(templateNameTextField) {
            maxLength = 100
        }
        return newLength <= maxLength
    }
}

// выбор периода планировщика
extension PaymentViewController {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    //  установка дефолтных значений периода
    func pickerPeriodDate() {
        pickerView.delegate = self
        pickerViewDayOperation.delegate = self
        pickerTarif.delegate = self
        pickerViewWeekDay.delegate = self
        periodTextField.inputView = pickerView
        dayNumberTextField.inputView = pickerViewDayOperation
        weekDayTextField.inputView = pickerViewWeekDay
        pickerView.selectRow(0, inComponent: 0, animated: true)
        pickerViewDayOperation.selectRow(0, inComponent: 0, animated: true)
        pickerViewWeekDay.selectRow(0, inComponent: 0, animated: true)
        pickerTarif.selectRow(0, inComponent: 0, animated: true)
        periodTextField.text = periodList[0]
        dayNumberTextField.text = daysList[0]
        weekDayTextField.text = weekList[0]
        viewDateBegin.isHidden = false
        viewDateFinish.isHidden = false
        viewWeekDay.isHidden = false
        viewDay.isHidden = true
        viewDateOperation.isHidden = true
        
    }
    func pickerGns(){
        regionField.delegate = self
        districtField.delegate = self
        serviceField.delegate = self
        gnsDepartmentField.delegate = self
        governmentField.delegate = self
        departmentField.delegate = self
        paymentTypeField.delegate = self
        serviceSFField.delegate = self
        payerTypeField.delegate = self
        
        pickerViewregions.delegate = self
        regionField.inputView = pickerViewregions
        
        pickerViewdDistricts.delegate = self
        districtField.inputView = pickerViewdDistricts
        
        pickerViewServices.delegate = self
        serviceField.inputView = pickerViewServices
        
        pickerViewGnsDepartments.delegate = self
        gnsDepartmentField.inputView = pickerViewGnsDepartments
        
        pickerViewGovernments.delegate = self
        governmentField.inputView = pickerViewGovernments
        
        pickerViewSFDepartments.delegate = self
        departmentField.inputView = pickerViewSFDepartments
        
        pickerViewPaymentType.delegate = self
        paymentTypeField.inputView = pickerViewPaymentType
        
        pickerViewSFServices.delegate = self
        serviceSFField.inputView = pickerViewSFServices
        
        pickerViewPayerType.delegate = self
        payerTypeField.inputView = pickerViewPayerType
    }
    // количество значений в "барабане"
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        var count = 0
        if pickerView == self.pickerView {
            count = periodList.count
        } else if pickerView == self.pickerViewDayOperation{
            count = daysList.count
        } else if pickerView == self.pickerViewWeekDay{
            count = weekList.count
        //GNS SF
        } else if pickerView == self.pickerViewregions{
            count = arrayOfRegion.count
        } else if pickerView == self.pickerViewServices{
            count = arrayOfServices.count
        } else if pickerView == self.pickerViewdDistricts{
            let copy = arrayOfDistricts.filter{$0.RegionID == regionID}
            count = copy.count
        } else if pickerView == self.pickerViewGovernments{
            count = arrayOfGovernments.count
        } else if pickerView == self.pickerViewSFDepartments{
            let copy = arrayOfSFDepartments.filter{$0.GovernmentID == governmentID}
            count = copy.count
        } else if pickerView == self.pickerViewGnsDepartments{
            let array = arrayOfDistricts.first{ $0.DistrictID == districtID }
            count = array?.TaxDepartments?.count ?? 0
        } else if pickerView == self.pickerViewPaymentType{
            count = arrayOfPaymentTypes.count
        } else if pickerView == self.pickerViewSFServices {
            count = arrayOfSFServices.count
        } else if pickerView == self.pickerViewPayerType {
            count = arrayOfPayerTypes.count
        }
        
        return count
    }
    //  название полей "барабана"
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        var list = ""
        if pickerView == self.pickerView {
            list = periodList[row]
        } else if pickerView == self.pickerViewDayOperation{
            list = daysList[row]
        } else if pickerView == self.pickerViewWeekDay{
            list = weekList[row]
        } else if pickerView == self.pickerViewregions{
            list = arrayOfRegion[row].Value!
        } else if pickerView == self.pickerViewServices{
            list = arrayOfServices[row].ServiceName!
        } else if pickerView == self.pickerViewGovernments{
            list = arrayOfGovernments[row].GovernmentCode!
        } else if pickerView == self.pickerViewdDistricts{
            let copy = arrayOfDistricts.filter{$0.RegionID == regionID}
            list = copy[row].DistrictName!
        } else if pickerView == self.pickerViewGnsDepartments{
            let copy = arrayOfDistricts.first{ $0.DistrictID == districtID }
            list = (copy?.TaxDepartments![row].DepartmentName)!
        } else if pickerView == self.pickerViewSFDepartments{
            let copy = arrayOfSFDepartments.filter{$0.GovernmentID == governmentID}
            list = copy[row].DepartmentName!
        } else if pickerView == self.pickerViewPaymentType{
            list = arrayOfPaymentTypes[row].Value!
        } else if pickerView == self.pickerViewSFServices {
            list = arrayOfSFServices[row].ServiceName!
        } else if pickerView == self.pickerViewPayerType {
            list = arrayOfPayerTypes[row].Value!
        }
        return list
    }
    // скрытие ненужных полей
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == self.pickerView {
            periodTextField.text = periodList[row]
            let formatterToServer = DateFormatter()
            formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            formatterToServer.locale = Locale(identifier: "language".localized)
            let dateNow = Date()
            switch row {
                
            case SchedulePickerView.EachWeek.rawValue:
                viewDateBegin.isHidden = false
                viewDateFinish.isHidden = false
                viewWeekDay.isHidden = false
                viewDay.isHidden = true
                viewDateOperation.isHidden = true
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = formatterToServer.string(from: dateNow )
                RecurringTypeID = ScheduleRepeat.EachWeek.rawValue
                ProcessDay = 1
                dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
                dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
                
            case SchedulePickerView.EachMonth.rawValue:
                viewDateBegin.isHidden = false
                viewDateFinish.isHidden = false
                viewWeekDay.isHidden = true
                viewDay.isHidden = false
                viewDateOperation.isHidden = true
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = formatterToServer.string(from: dateNow )
                RecurringTypeID = ScheduleRepeat.EachMonth.rawValue
                ProcessDay = 1
                dateBeginTextField.text = ScheduleHelper.shared.dateSchedule()
                dateFinishTextField.text = ScheduleHelper.shared.dateSchedule()
                
            case SchedulePickerView.OneTime.rawValue:
                viewDateBegin.isHidden = true
                viewDateFinish.isHidden = true
                viewWeekDay.isHidden = true
                viewDay.isHidden = true
                viewDateOperation.isHidden = false
                StartDate = formatterToServer.string(from: dateNow )
                EndDate = nil
                RecurringTypeID = ScheduleRepeat.OneTime.rawValue
                ProcessDay = 0
                dateOperationTextField.text = ScheduleHelper.shared.dateSchedule()
                
            default:
                viewDateBegin.isHidden = true
                viewDateFinish.isHidden = true
                viewWeekDay.isHidden = true
                viewDay.isHidden = true
                viewDateOperation.isHidden = true
                
            }
        } else if pickerView == self.pickerViewDayOperation{
            dayNumberTextField.text = daysList[row]
            ProcessDay =  row + 1
        } else if pickerView == self.pickerViewWeekDay{
            weekDayTextField.text = weekList[row]
            ProcessDay =  row + 1
        } else if pickerView == self.pickerViewGnsDepartments{
            let copy = arrayOfDistricts.first{ $0.DistrictID == districtID }
            gnsDepartmentField.text = copy?.TaxDepartments![row].DepartmentName
            departmentID = copy?.TaxDepartments![row].DepartmentID
        } else if pickerView == self.pickerViewregions {
            regionField.text = arrayOfRegion[row].Value
            regionID = arrayOfRegion[row].Key
        } else if pickerView == self.pickerViewServices {
            serviceField.text = self.arrayOfServices[row].ServiceName
            serviceID = self.arrayOfServices[row].ServiceID
        } else if pickerView == self.pickerViewGovernments {
            governmentField.text = self.arrayOfGovernments[row].GovernmentCode
            governmentID = arrayOfGovernments[row].GovernmentID
            governmentCode = arrayOfGovernments[row].GovernmentCode
        } else if pickerView == self.pickerViewSFDepartments {
            let copy = arrayOfSFDepartments.filter{$0.GovernmentID == governmentID}
            departmentField.text = copy[row].DepartmentName
            departmentSFID = copy[row].DepartmentID
        } else if pickerView == self.pickerViewdDistricts {
            let copy = arrayOfDistricts.filter{$0.RegionID == regionID}
            districtField.text = copy[row].DistrictName
            districtID = copy[row].DistrictID
        } else if pickerView == self.pickerViewPaymentType {
            paymentTypeField.text = self.arrayOfPaymentTypes[row].Value
            paymentTypeID = self.arrayOfPaymentTypes[row].Key
        } else if pickerView == self.pickerViewSFServices {
            serviceSFField.text = self.arrayOfSFServices[row].ServiceName
            paymentCode = self.arrayOfSFServices[row].PaymentCode
            serviceSFID = self.arrayOfSFServices[row].ServiceID
        } else if pickerView == self.pickerViewPayerType {
            payerTypeField.text = self.arrayOfPayerTypes[row].Value
            payerTypeID = self.arrayOfPayerTypes[row].Key
        }
    }
    // выборка даты
    func showDatePicker(){
        
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        datePicker.minimumDate = Date()
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: localizedText(key: "cancel"), style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: self.localizedText(key: "is_done"), style: .plain, target: self, action: #selector(doneDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        dateBeginTextField.inputAccessoryView = toolbar
        dateBeginTextField.inputView = datePicker
        dateFinishTextField.inputAccessoryView = toolbar
        dateFinishTextField.inputView = datePicker
        dateOperationTextField.inputAccessoryView = toolbar
        dateOperationTextField.inputView = datePicker
        
    }
    // обработка кнопки ГОТОВО
    @objc func doneDatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"
        formatter.locale = Locale(identifier: "language".localized)
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        
        if dateBeginTextField.isFirstResponder {
            dateBeginTextField.text = formatter.string(from: datePicker.date)
            StartDate = formatterToServer.string(from: datePicker.date)
        }
        if dateFinishTextField.isFirstResponder {
            dateFinishTextField.text = formatter.string(from: datePicker.date)
            EndDate = formatterToServer.string(from: datePicker.date)
        }
        if dateOperationTextField.isFirstResponder {
            dateOperationTextField.text = formatter.string(from: datePicker.date)
        }
        
        self.view.endEditing(true)
    }
    // обработка кнопки ОТМЕНА
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    // проверка на фокус поля ввода
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == dateBeginTextField {
            datePicker.datePickerMode = .date
        }
        if textField == dateFinishTextField {
            datePicker.datePickerMode = .date
        }
        if textField == dateOperationTextField {
            datePicker.datePickerMode = .date
        }
    }
}
//MARK: AlertSheets

extension PaymentViewController{
    //    получение счетов
    func  getAccounts(){
        showLoading()
        
        let currenciesObservable:Observable<[Currency]> = managerApi
            .getCurrenciesWithoutCertificate()
        
        let depositObservable:Observable<[Accounts]> = managerApi
            .getClearingGrossAcconts()

        showLoading()
        
        Observable
            .zip(currenciesObservable, depositObservable)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (Currencies, Accounts) in
                    guard let `self` = self else { return }
                    if Accounts.count > 0{
                        
                        self.accounts = self.accountsCurrencies(currency: self.currencies, accounts: Accounts)
                        self.accounts = self.checkAccountsForZeroBalance(accounts: self.accounts)

                        if self.isCreateTemplate || self.isRepeatPay{
                            if self.isCreateTemplateFromStory{
                                self.selectedDeposit = self.getCurrentTemplateAccount(
                                    accountNo: self.operation.accountNo ?? "" ,
                                    currency: self.operation.currencyID ?? 0 ,
                                    accounts: self.accounts)
                                
                                if self.selectedDeposit != nil{
                                    self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                }
                                
                            }else if self.isRepeatFromStory{
                                self.selectedDeposit = self.getCurrentTemplateAccount(
                                    accountNo:self.operation.accountNo ?? "" ,
                                    currency: self.operation.currencyID ?? 0 ,
                                    accounts: self.accounts)
                               
                                if self.selectedDeposit != nil{
                                    self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                }
                            }else if self.isRepeatPay{
                                if self.isRepeatOperation{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(
                                        accountNo: self.payData.accountNo ?? "" ,
                                        currency: self.payData.currencyID ?? 0 ,
                                        accounts: self.accounts)
                                }else if self.isChangeData{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(
                                        accountNo: self.operation.accountNo ?? "" ,
                                        currency: self.operation.currencyID ?? 0 ,
                                        accounts: self.accounts)
                                }else{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(
                                        accountNo: self.template.from ?? "" ,
                                        currency: self.template.currencyID ?? 0 ,
                                        accounts: self.accounts)
                                    
                                }
                                if self.selectedDeposit != nil{
                                    self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                }
                            }else{
                                if self.template != nil{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(
                                        accountNo: self.template.from ?? "" ,
                                        currency: self.template.currencyID ?? 0 ,
                                        accounts: self.accounts)
                                    
                                    if self.selectedDeposit != nil{
                                        self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                    }
                                }
                                if self.operation != nil{
                                    self.selectedDeposit = self.getCurrentTemplateAccount(
                                        accountNo: self.operation.accountNo ?? "" ,
                                        currency: self.operation.currencyID ?? 0 ,
                                        accounts: self.accounts)
                                    
                                    if self.selectedDeposit != nil{
                                        self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                    }
                                }
                            }
                        }
                        if self.isEditTemplate{
                            if self.template != nil{
                                self.selectedDeposit = self.getCurrentTemplateAccount(
                                    accountNo: self.template.from ?? "" ,
                                    currency: self.template.currencyID ?? 0 ,
                                    accounts: self.accounts)
                                if self.selectedDeposit != nil{
                                     self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                                }
                            }
                            
                        }
                        if self.currencyIDcurrent != 0 && self.accountNoCurrent != ""{
                            self.selectedDeposit = self.getCurrentTemplateAccount(accountNo: self.accountNoCurrent, currency: self.currencyIDcurrent, accounts: self.accounts)
                            if self.selectedDeposit != nil{
                                self.fullSelectedDeposit(selectedDeposit: self.selectedDeposit)
                            }
                        }
                        if self.selectedDeposit != nil{
                            var fields: [ScenarioFieldValueModel] = []
                            var count: Int = 0
                            for item in self.scenarious {
                                fields.append(ScenarioFieldValueModel(fieldID: item.fieldID, value: self.textFields[count].text))
                                count += 1
                            }
                            let sumDouble = Double(self.sumPaymentTextField.text  ?? "0.0")
                            if let sum = sumDouble{
                                if sum > 0.0{
                                    let commissionModel: UtilityPaymentOperationModel = UtilityPaymentOperationModel(
                                        accountNo: self.selectedDeposit.accountNo ?? "0000",
                                        currencyID:  Constants.NATIONAL_CURRENCY,
                                        providerID: self.providerID,
                                        contragentID: self.contragentID,
                                        personalAccountNo: self.phoneTextField.text ?? "123",
                                        сurrentAccountNo: nil,
                                        sum: sumDouble ?? 0.0,
                                        paymentSum: nil,
                                        
                                        commission: Double(self.comissionLabel.text ?? "0.0"),
                                        taxDepartmentID: self.departmentID,
                                        departmentID: self.departmentID,
                                        OKPOCode: self.okpoField.text,
                                        taxServiceID: self.serviceID,
                                        
                                        comment: self.getComment(),
                                        operationID: nil,
                                        isSchedule: self.isSchedule,
                                        isTemplate: self.isCreateTemplate,
                                        templateName: self.templateNameTextField.text ?? nil,
                                        templateDescription: "Оплата коммунальной услуги",
                                        scheduleID: 0,
                                        schedule: self.schedule ?? nil,
                                        fullName: self.beneficiaryFullName,
                                        fields: fields)
                                    
                                    commissionModel.serviceID = self.serviceSFID
                                    commissionModel.governmentCode = self.governmentCode
                                    commissionModel.payerTypeID = self.payerTypeID
                                    commissionModel.paymentTypeID = self.paymentTypeID
                                    self.getCommission(commissionModel: commissionModel)
                                }
                            }
                        }
                        if self.selectedDeposit != nil{
                            for label in self.CurrencyLabel{
                                label.text = self.selectedDeposit.currencySymbol
                            }
                        }
                        self.hideLoading()
                        
                    }else{
                        self.showAlertController(self.localizedText(key: "no_active_accounts_found"))
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    получение валют
    func accountsCurrencies(currency: [Currency], accounts:[Accounts]) -> [Accounts]{
        
        for account in accounts{
            for item in account.items!{
                for id2 in currency{
                    if item.currencyID == id2.currencyID{
                        item.currency = id2
                    }
                }
            }
        }
        return accounts
    }
}

extension PaymentViewController{
    //    создние операции
    
    func postPay(checkModel: UtilityPaymentOperationModel){
        if isError {
            showAlertController("Проверьте корректность реквизита")
        } else {
            let json = Mapper().toJSONString(checkModel)!
            let model = UtilityPaymentModel(providerID: checkModel.providerID!, paymentModel: json)
            
            showLoading()
            managerApi
                .postPayUtility(Pay_CheckModel: model)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self] operationID in
                        guard let `self` = self else { return }
                        
                        if self.isCreateTemplate || self.isEditTemplate{
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
                            self.view.window?.rootViewController = vc
                            
                            let storyboardTemplates = UIStoryboard(name: "Templates", bundle: nil)
                            let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "TemplatesViewController") as! TemplatesViewController
                            let navigationController = UINavigationController(rootViewController: destinationController)
                            vc.pushFrontViewController(navigationController, animated: true)
                            
                        }else{
                            if operationID != ""{
                                self.operationID = Int(operationID) ?? 0
                            }
                             self.allowOperation()
                        }
                        self.hideLoading()
                },
                    onError: {[weak self] error in
                        self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                        
                        self?.hideLoading()
                })
                .disposed(by: disposeBag)
        }
    }
    
    //    получение коммиссии
    func getCommission(commissionModel: UtilityPaymentOperationModel){
        showLoadingWithStatus(message: self.localizedText(key: "commission_calculation"))
        if commissionModel.providerID == 1203 {
            let commission = arrayOfDistricts.first{ $0.DistrictID == districtID }?.TaxDepartments?.first{$0.TaxDistrictID == districtID}?.Comissions
            if !(commission?.isEmpty ?? true) {
                for item in commission! {
                    if item.EndSumm == nil || (item.StartSumm! <= commissionModel.sum! && item.EndSumm! >= commissionModel.sum!) {
                        let sumNew = commissionModel.sum! + (item.Comission ?? 0.0)
                        if sumNew == 0 {
                            showAlertController("Сумма не может быть меньше или равна комиссии")
                        } else {
                            self.sumLabel.text = "\(String(format:"%.2f", commissionModel.sum!))"
                            self.comissionLabel.text = "\(String(format:"%.2f",item.Comission ?? 0.0))"
                            self.totalSumLabel.text = "\(String(format:"%.2f", sumNew))"
                            self.sum = commissionModel.sum!
                        }
                    }
                }
            }
        } else {
            managerApi
                .postGetCommission(UtilityPaymentOperationModel: commissionModel)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self] (commission) in
                        guard let `self` = self else { return }
                        
                        let sumDouble = Double(self.sumPaymentTextField.text  ?? "0.0")
                        self.comissionLabel.text = "\(String(format:"%.2f", abs(commission.result ?? 0)))"
                        let totalSum = sumDouble ?? 0.0
                        
                        if let commis = commission.result {
                            if commis > 0.0 {
                                let sumNew = totalSum + (abs(commission.result ?? 0.0))
                                self.sumLabel.text = "\(String(format:"%.2f",totalSum))"
                                self.totalSumLabel.text = "\(String(format:"%.2f",sumNew))"
                                self.sum = totalSum
                            }else{
                                let sumNew = (sumDouble ?? 0.0) + (abs(commission.result ?? 0.0))
                                self.sumLabel.text = "\(String(format:"%.2f",totalSum))"
                                self.totalSumLabel.text = "\(String(format:"%.2f",sumNew))"
                                self.sum = (sumDouble ?? 0.0) + (abs(commission.result ?? 0.0))
                            }
                        }
                        
                        self.hideLoading()
                        if commission.message != ""{
                            self.showAlertController(commission.message ?? "")
                        }
                },
                    onError: {[weak self] error in
                        self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                        self?.hideLoading()
                })
                .disposed(by: disposeBag)
        }
    }
    //    заполнение текущего счета
    func fullSelectedDeposit(selectedDeposit: Deposit){
      
        let fullAccountsText: String = "\(String(describing: selectedDeposit.name ?? "")) ∙∙\(String(describing: selectedDeposit.accountNo?.suffix(4) ?? ""))"
        self.payFrom_accountLabel.text = fullAccountsText
        self.choiceCard_accountLabel.text = "\(String(format:"%.2f", selectedDeposit.balance ?? 0.0 )) \(String(describing: selectedDeposit.currencySymbol ?? ""))"
        self.currencySymbol.text = selectedDeposit.currencySymbol
        self.sumCurrencyLabel.text = selectedDeposit.currencySymbol
        self.commissionCurrencyLabel.text = selectedDeposit.currencySymbol
        self.totalCurrencyLabel.text = selectedDeposit.currencySymbol
    }
}

extension PaymentViewController{
    // проверка для прав на создание и подтверждние
    func  allowToCreatAndSaveOperation(button: UIButton) {
        
        if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsAddAllowed") && AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "execute_operation"),for: .normal)
        }else if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsAddAllowed") && !AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsApproveAllowed"){
            button.setTitle(localizedText(key: "Resources_Common_Save"),for: .normal)
        }
    }
    
    //    проверка прав
    func  allowOperation() {
        
        if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsAddAllowed") && AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsApproveAllowed"){
            self.performSegue(withIdentifier: "toSubmitSegue", sender: self)
            
        }else if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsAddAllowed") && !AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsApproveAllowed"){
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
            self.view.window?.rootViewController = vc
            
            let storyboardTemplates = UIStoryboard(name: "Main", bundle: nil)
            let destinationController = storyboardTemplates.instantiateViewController(withIdentifier: "PaymentHistoryViewController") as! PaymentHistoryViewController
            let navigationController = UINavigationController(rootViewController: destinationController)
            vc.pushFrontViewController(navigationController, animated: true)
            
        }
    }
    
    private func getResult(data: String?) -> String? {
        if data != nil {
            let result = data!.data(using: .utf8)!
            do {
                if let json = try JSONSerialization.jsonObject(with: result, options: []) as? [String: Any] {
                    // try to read out a string array
                    if let message = json["message"] as? String? {
                        return message
                    } else {
                        return ""
                    }
                }
            } catch let error as NSError {
                return "Failed to load: \(error.localizedDescription)"
            }
        }
        return nil
    }
    
    private func getAmount(data: String) -> String? {
        let result = data.data(using: .utf8)!
        do {
            if let json = try JSONSerialization.jsonObject(with: result, options: []) as? [String: Any] {
                // try to read out a string array
                if let message = json["summa"] as? String? {
                    return message
                }
                else{
                    return "0.0"
                }
            }
        } catch _ as NSError {
            return "0.0"
        }
        return nil
    }
    
    // Проверка реквизита получателя
    func postCheckUtility(Pay_CheckModel: UtilityPaymentOperationModel) {
        let json = Mapper().toJSONString(Pay_CheckModel)
        let model = UtilityPaymentModel(providerID: Pay_CheckModel.providerID!, paymentModel: json!)
        showLoading()
        managerApi
        .postCheckUtility(Pay_CheckModel: model)
        .updateTokenIfNeeded()
        .subscribe(
            onNext: {[weak self](response) in
                guard let `self` = self else { return }
                let result = self.getResult(data: response.result)
                let jsonData = response.result?.data(using: .utf8)
                
                if let jsonData = jsonData {
                    let data = try? JSONDecoder().decode(CheckModel.self, from: jsonData)
                    if(self.currentUtility != nil && (self.currentUtility.ID == 1203 || self.currentUtility.ID == 1163)){
                        self.fullNameField.text = response.result
                    } else {
                        self.labelErrorNo.textColor = .gray
                        self.labelErrorNo.text = "\(data?.fullName ?? "") \(data?.paymentSum ?? "")"
                        self.beneficiaryFullName = result ?? ""
//                        self.sumPaymentTextField.text = data?.paymentSum
                    }
                    self.hideLoading()
                    self.isError = false
                    print(response)
                }
            },
            onError: {[weak self] error in
                self?.self.labelErrorNo.text = (ApiHelper.shared.errorHelper(error: error))
                self?.isError = true
                self?.hideLoading()
            }
        )
        .disposed(by: disposeBag)
    }
    
    func getScenario(contragentID: Int) {
        showLoading()
        managerApi
            .getScenario(contragentID: contragentID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (scenario) in
                    guard let `self` = self else { return }
                    self.scenarious = scenario
                    self.setDynamicField()
                }, onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                }
            )
            .disposed(by: disposeBag)
    }
    
    func getOperation(operationID: Int) {
        showLoading()
        managerApi
            .getOperation(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (operation) in
                    guard let `self` = self else { return }
                    self.operationModel = operation
                    //1203 = GNS //1163 - Soc fond
                    if operation.providerID == 1163 || operation.providerID == 1203
                    {
                        if operation.providerID == 1163 {
                            self.gnsView.isHidden = true
                            self.socFondView.isHidden = false
                            self.fullNameView.isHidden = true
                            self.okpoView.isHidden = true
                            self.paymentPurposeView.isHidden = true
                        } else {
                            self.gnsView.isHidden = false
                            self.socFondView.isHidden = true
                            self.fullNameView.isHidden = false
                            self.okpoView.isHidden = false
                            self.paymentPurposeView.isHidden = false
                        }
                        self.getPaymentModel(providerID: operation.providerID!)
                    } else {
                        self.getScenario(contragentID: operation.providerID!)
                    }
                }, onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                }
            )
            .disposed(by: disposeBag)
    }
    
    func getPaymentModel(providerID: Int) {
        showLoading()
        managerApi
            .getPaymentModel(providerID: providerID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (data) in
                    guard let `self` = self else { return }
                    if data.result != nil {
                        let jsonData = data.result!.data(using: .utf8)!
                        let model = try? JSONDecoder().decode(PaymentModel.self, from: jsonData)

                        self.arrayOfRegion = model?.Regions ?? [Region]()
                        self.arrayOfDistricts = model?.Districts ?? [District]()
                        self.arrayOfServices = model?.Services ?? [Service]()
                        self.arrayOfGnsDepartments = model?.Departments ?? [Department]()
                        self.arrayOfSFDepartments = model?.Departments ?? [Department]()
                        self.arrayOfPayerTypes = model?.PayerTypes ?? [PayerType]()
                        self.arrayOfPaymentTypes = model?.PaymentTypes ?? [PaymentType]()
                        self.arrayOfCommissions = model?.Comissions ?? [Commission]()
                        self.arrayOfGovernments = model?.Governments ?? [Government]()
                        self.arrayOfSFServices = model?.Services ?? [Service]()
                        
                        if self.isRepeatPay || self.isEditTemplate {
                            if self.operationModel.providerID == 1163 {
                                let department = self.arrayOfSFDepartments.first{$0.DepartmentID == self.operationModel.departmentID}
                                let service = self.arrayOfSFServices.first{$0.ServiceID == self.operationModel.serviceID}
                                let government = self.arrayOfGovernments.first{$0.GovernmentCode == self.operationModel.governmentCode}
                                self.departmentSFID = self.operationModel.departmentID
                                self.serviceSFID = self.operationModel.serviceID
                                self.departmentField.text = department?.DepartmentName
                                self.serviceSFField.text = service?.ServiceName
                                self.governmentField.text = government?.GovernmentCode
                            } else {
                                self.departmentID = self.operationModel.taxDepartmentID
                                self.serviceID = self.operationModel.taxServiceID
                                let taxDepartment = self.arrayOfGnsDepartments.first{$0.DepartmentID == self.departmentID}
                                self.districtID = taxDepartment?.TaxDistrictID
                                let district = self.arrayOfDistricts.first{$0.DistrictID == self.districtID}
                                self.gnsDepartmentField.text = district?.TaxDepartments?.first{$0.DepartmentID == self.departmentID}?.DepartmentName
                                let region = self.arrayOfRegion.first{$0.Key == district?.RegionID}
                                self.regionID = region?.Key
                                self.regionField.text = region?.Value
                                self.districtField.text = district?.DistrictName
                                let service = self.arrayOfServices.first{$0.ServiceID == self.serviceID}
                                self.serviceField.text = service?.ServiceName
                            }
                        }
                        
                    } else {
                        self.showAlertController(data.message!)
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
                }
            )
            .disposed(by: disposeBag)
    }
}
