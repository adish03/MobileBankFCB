//
//  UtiliesViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/22/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift


class UtilitiesViewController: BaseViewController {
    
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    
    var categories = [Categories]()
    var utilities = [Utility]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  menu != nil{
            menu.target = self.revealViewController()
            menu.action = #selector(SWRevealViewController.revealToggle(_:))
        }
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        getUtilitiesAndCategoriesWithFlatMap()
        tableView.reloadData()
    }
    
}

extension UtilitiesViewController{
    
    func  getUtilitiesAndCategoriesWithFlatMap(){
        showLoading()
        let utilitiesObservable:Observable<[Utility]> = managerApi
            .getUtilities()
            .updateTokenIfNeeded()
        
        let categoriesObservable: Observable<[Categories]> = managerApi
            .getCategories()
        
        
        
        utilitiesObservable
            .flatMap{ utilities -> Observable<[Categories]> in

                self.utilities = utilities
                print(utilities)

                return categoriesObservable
            }
            .subscribe(
                onNext: {(categories) in

                    self.categories = categories
                    print(categories)

                    self.tableView.reloadData()
                    self.hideLoading()
                   
            },
                onError: {(error) in
                    print(error)
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

extension UtilitiesViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return utilities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UtilityTableViewCell
        
        cell.utilityNameLabel.text = utilities[indexPath.row].name
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        let sb = UIStoryboard(name: "Utilities", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "CategoriesViewController") as! CategoriesViewController
//        vc.utilities = utilities
        vc.categories = categories
        vc.index = indexPath.row
//        vc.providerID = utilities[indexPath.row].iD ?? 0
        navigationController?.pushViewController(vc, animated: true)
        
    }
}
