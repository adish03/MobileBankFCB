//
//  TemplateViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/28/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import RxCocoa

class TemplateViewController: BaseViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberUtility: UILabel!
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var contractLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var comissionLabel: UILabel!
    @IBOutlet weak var totalPayLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var paymentCommentLabel: UILabel!
    
    @IBOutlet weak var paymentCommentView: UIView!
    
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    var state = "operation_complete"
    var stateImage = "succes"
    
    var submitModel: SubmitForPayModel!
    var submitModelToBank: SubmitForPayModel!
    var currentCurrency = ""
    var operationID = 0
    var selectedDeposit: Deposit!
    var currentUtility: UtilityModel!
    var navigationTitle = ""
    var operationType = 0
    var confirmType = 0
    var imageUtility = ""
    var delegate: NoNeedLoadDataDelegate?

    
    //operation detail
    var isNeedOperationDetail = false
    var operation: ModelOperation!
    @IBOutlet weak var viewTemplate: UIView!
    @IBOutlet weak var viewOperationState: UIView!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var viewButton: UIView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        delegate?.isNeedData(isNeed: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        statusLabel.text = localizedText(key: state )
        statusImage.image = UIImage(named: stateImage )
        barButtonCustomize()
        
        if currentUtility != nil {
            if currentUtility.ID == 1203 || currentUtility.ID == 1163 {
                paymentCommentView.isHidden = false
            }
        }
        
        barButtonCustomize()
        if isNeedOperationDetail{
            viewTemplate.isHidden = true
            viewButton.isHidden = true
            viewOperationState.isHidden = false
            getInfoOperation(operationID: operationID)
            let directions: [UISwipeGestureRecognizer.Direction] = [.up, .down, .right, .left]
            for direction in directions {
                let gesture = UISwipeGestureRecognizer(target: self, action: #selector(self.handleSwipe(gesture:)))
                gesture.direction = direction
                view?.addGestureRecognizer(gesture)
            }
            
        }else{
            viewTemplate.isHidden = false
            viewButton.isHidden = false
            viewOperationState.isHidden = true
            getInfoOperation(operationID: operationID)
            
        }
    }
    //обработка swipe 
    @objc func handleSwipe(gesture: UISwipeGestureRecognizer) {
        switch gesture.direction {
            
        case UISwipeGestureRecognizer.Direction.right:
            self.navigationController?.popViewController(animated: true)
        default:
            print("")
        }
    }
    //    кнопка закрыть
    @IBAction func closeButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    //    кнапка закрыть
    @IBAction func saveButton(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
        
        vc.isCreateTemplate = true
        vc.submitModel = self.submitModel
        vc.selectedDeposit = self.selectedDeposit
        vc.navigationTitle = localizedText(key: "create_template")
        vc.currentUtility = self.currentUtility
        vc.operation = operation
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toUtilitiesSegue"{
            let vc = segue.destination as! PaymentViewController
            if submitModel != nil{
                vc.imageUtility = submitModel.image ?? "placeholder"
            }
            vc.operation = self.operation
            vc.isRepeatPay = true
            vc.isRepeatFromStory = true
            vc.navigationTitle = localizedText(key: "create_operation_request")
            vc.imageUtility = imageUtility
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        }
    }
    // кнопка FAB
    @IBAction func AlertSheetControl(_ sender: Any) {
        
        let alertSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let actionRepeatOperation = UIAlertAction(title: self.localizedText(key: "create_operation_request"), style: .default) { (action) in
            self.performSegue(withIdentifier: "toUtilitiesSegue", sender: self)
        }
        let actionHSaveTemplate = UIAlertAction(title: self.localizedText(key: "save_as_template"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            vc.isCreateTemplate = true
            vc.isCreateTemplateFromStory = true
            vc.operation = self.operation
            vc.selectedDeposit = self.selectedDeposit
            vc.navigationTitle = self.localizedText(key: "create_template")
            vc.currentUtility = self.currentUtility
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let changeData = UIAlertAction(title: self.localizedText(key: "edit"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            if self.submitModel != nil{
                vc.imageUtility = self.submitModel.image ?? "placeholder"
            }
            vc.operation = self.operation
            vc.isRepeatPay = true
            vc.isChangeData = true
            vc.navigationTitle = self.localizedText(key: "create_operation_request")
            vc.imageUtility = self.imageUtility
            self.navigationController?.pushViewController(vc, animated: true)
        }
        let sendToBank = UIAlertAction(title: self.localizedText(key: "send_to_bank"), style: .default) { (action) in
            
            let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "SubmitPaymentViewController") as! SubmitPaymentViewController
            
            vc.submitModel = self.submitModelToBank
            vc.operationID = self.operationID
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        let actionDownload = UIAlertAction(title: self.localizedText(key: "receipt"), style: .default) { (action) in
            ApiHelper.shared.getInfoOperationFile(operationID: self.operation.operationID ?? 0,
                                                  code: 00000,
                                                  operationTypeID: self.operation.operationType ?? 0, vc: self)
        }
        let actionRemove = UIAlertAction(title: self.localizedText(key: "delete"), style: .default) { (action) in
            let alert = UIAlertController(title:  self.localizedText(key: "delete_operation"), message: "", preferredStyle: .alert)
            let actionOk = UIAlertAction(title: "OK", style: .default, handler: { (action) in
                ApiHelper.shared.deleteTemplate(operationID: "\(self.operationID)")
                self.navigationController?.popViewController(animated: true)
            })
            let actionCancel = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)
            alert.addAction(actionOk)
            alert.addAction(actionCancel)
            self.present(alert, animated: true, completion: nil)
        }
        
        let actionCancel  = UIAlertAction(title: self.localizedText(key: "cancel"), style: .cancel, handler: nil)
        
        // отображение действий в зависимости от confirmType
        switch confirmType {
        case ConfirmType.Save.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsEditAllowed"){
                alertSheet.addAction(changeData)
            }
            if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsRemoveAllowed"){
                alertSheet.addAction(actionRemove)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Confirm.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsPrintDocumentsAllowed"){
               alertSheet.addAction(actionDownload)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.InConfirm.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Error.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            alertSheet.addAction(actionCancel)
            
        case ConfirmType.Verified.rawValue:
            if AllowHelper.shared.operationAllowOpertaion(nameOperation: "Operation.OperationSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionRepeatOperation)
            }
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            if AllowHelper.shared.commonAllowOpertaion(nameOperation: "Common.UtilitiesPaymentsSecurityOperations.IsApproveAllowed"){
                alertSheet.addAction(sendToBank)
            }
            alertSheet.addAction(actionCancel)
            
        default:
           
            if AllowHelper.shared.templateAllowOpertaion(nameOperation: "TemplateOperation.TemplateSecurityOperations.IsAddAllowed"){
                alertSheet.addAction(actionHSaveTemplate)
            }
            alertSheet.addAction(actionCancel)
        }
        
        present(alertSheet, animated: true, completion: nil)
    }
}

extension TemplateViewController {
    //    получение инфо об операции
    func  getInfoOperation(operationID: Int) {
        showLoading()
        
        managerApi
            .getInfoOperation(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] operation in
                    guard let `self` = self else { return }
                    self.operation = operation
                    
                    let comission = "\(String(format:"%.2f", abs(operation.commission ?? 0.0))) \(ValueHelper.shared.getCurrentCurrency(currnecyID: operation.currencyID ?? 0))"
                    let total = "\(String(format:"%.2f", operation.sum! + abs(operation.commission!) )) \(ValueHelper.shared.getCurrentCurrency(currnecyID: operation.currencyID ?? 0))"
                    let amount: Double = (operation.total ?? 0.0) - (abs(operation.commission ?? 0.0))
                    let sum = "\(String(format:"%.2f", amount)) \(ValueHelper.shared.getCurrentCurrency(currnecyID: operation.currencyID ?? 0))"
                    
                    self.nameLabel.text = operation.service
                    self.numberUtility.text = operation.personalAccountNo 
                    self.accountLabel.text = operation.accountNo
                    self.contractLabel.text = "\(operationID)"
                    self.sumLabel.text = sum
                    self.comissionLabel.text = comission
                    self.totalPayLabel.text = total
                    self.descriptionLabel.text = operation.comment
                    (self.stateLabel.text, self.stateLabel.textColor) = ValueHelper.shared.confirmTypeDefine(confirmType: self.operation.confirmType ?? 0)
                    self.confirmType = self.operation.confirmType ?? 0
                    self.hideLoading()
                    
                    self.submitModelToBank = SubmitForPayModel(name: operation.service ?? "",
                                                               props: operation.personalAccountNo ?? "",
                                                               account: operation.accountNo ?? "",
                                                               contract: "\(operation.operationID ?? 0)",
                        sum: Double(operation.sum ?? 0.0),
                        commission: "\(operation.commission ?? 0.0)",
                        total: "\(operation.total ?? 0.0)",
                        description: operation.comment ?? "",
                        comment: operation.comment ?? "",
                        image: "\(operation.contragentID ?? 0)",
                        currencyID: operation.currencyID ?? 0)
                    
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    
    //    кастомизация кнопки
    func barButtonCustomize(){
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        let button = UIButton(type: .custom)
        var image = ""
        if isNeedOperationDetail{
            image = "ArrowBack"
            button.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
        }else{
            image = "Close"
            button.addTarget(self, action: #selector(closeButtonPressed), for: .touchUpInside)
        }
        button.setImage(UIImage(named: image), for: .normal)
        
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 50)
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    //    кнопка закрытия
    @objc func closeButtonPressed() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NavMainController") as! SWRevealViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
    //    кнопка назад
    
    @objc func backButtonPressed() {
        navigationController?.popViewController(animated: true)
        delegate?.isNeedData(isNeed: false)

    }
}
