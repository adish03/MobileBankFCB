//
//  TaxPaymentViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Рустам on 11/12/20.
//  Copyright © 2020 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import RxCocoa

class TaxPaymentViewController: BaseViewController, DepositProtocol, UITextFieldDelegate, EditPaymentsProtocol {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet weak var logoImage: UIImageView!
    @IBOutlet weak var nameCategoryLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var sumPaymentTextField: UITextField!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var comissionLabel: UILabel!
    @IBOutlet weak var totalSumLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var sumFromLabel: UILabel!
    @IBOutlet weak var sumToLabel: UILabel!
    @IBOutlet weak var payFrom_accountLabel: UILabel!
    @IBOutlet weak var choiceCard_accountLabel: UILabel!
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var sumCurrencyLabel: UILabel!
    @IBOutlet weak var commissionCurrencyLabel: UILabel!
    @IBOutlet weak var totalCurrencyLabel: UILabel!
    
    @IBOutlet weak var currencySymbol: UILabel!
    //view
    
    @IBOutlet weak var viewNoTextField: UIView!
    @IBOutlet weak var labelErrorNo: UILabel!
    @IBOutlet weak var viewSumTextField: UIView!
    @IBOutlet weak var viewCommentTextField: UIView!
    @IBOutlet weak var labelErrorSum: UILabel!
    @IBOutlet weak var labelErrorAccount: UIView!
    @IBOutlet weak var labelErrorComment: UILabel!
    @IBOutlet weak var LogoView: UIView!
    
  
    @IBOutlet var CurrencyLabel: [UILabel]!
    
    func selectedDepositUser(deposit: Deposit) {
        
    }
    
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool, payData: UtilityPaymentOperationModel) {
        
    }
}
