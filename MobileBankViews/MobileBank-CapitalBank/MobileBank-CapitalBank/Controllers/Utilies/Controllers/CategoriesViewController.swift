//
//  CategoriesViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/22/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import MobileBankCore
import RxSwift
import AlamofireImage
import Alamofire


class CategoriesViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    var utilities: Utilities!
    var categories: Categories!
    var listUtilities: [UtilityModel]!
    var categoryArray = [CategoriesType]() // need for search
    var index = 1
    var modifyDateCategories = ""
    var modifyDateUtilities = ""
    var filteredArray = [UtilityModel]()
    var isSearching = false
    var currencyIDcurrent = 0
    var accountNoCurrent = ""
    var isNewPayFromFianance = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = localizedText(key: "utilities_payment")
       
        emptyStateStack.isHidden = true
        
        hideKeyboardWhenTappedAround()
        if isNewPayFromFianance{
            if  menu != nil{
                menu.image = UIImage(named: "ArrowBack")
                menu.target = self
                menu.action = #selector(closeButtonPressed)
            }
        }else{
            if  menu != nil{
                menu.target = self.revealViewController()
                menu.action = #selector(SWRevealViewController.revealToggle(_:))
            }
        }
       
        
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
      
        // TODO: BAKAI sort it to exclude 28 parentCategory ID's
        // TODO: BAKAI save to separate array parentCategory 28 ID's
        if let categories = UserDefaultsHelper.categories{
            self.categories = categories
            self.categoryArray = categories.items!
        }
        if let utilities = UserDefaultsHelper.utilities{
            self.utilities = utilities
        }
        if let listUtilities = UserDefaultsHelper.listUtilities{
            self.listUtilities = listUtilities
        }
    
    }
    //    кнопка закрытия
    @objc func closeButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
}


 

extension CategoriesViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        if isSearching{
            return filteredArray.count
        }
        return  categoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CategoryTableViewCell
        
        if isSearching{
            cell.categoryLabel.text = filteredArray[indexPath.row].name
            self.getImage(image: filteredArray[indexPath.row].logo ?? "", imageView: cell.categoryImage, cell: cell, cell1: nil)
        }else{
            cell.categoryLabel.text = categoryArray[indexPath.row].categoryName
            DispatchQueue.main.async {
                self.getImage(image: self.categoryArray[indexPath.row].logoFileName ?? "", imageView: cell.categoryImage, cell: cell, cell1: nil)
            }
        }
        selectedCellCustomColor(cell: cell)
        return cell
    }
    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if filteredArray.count != 0{
            let sb = UIStoryboard(name: "Utilities", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
            vc.currentUtility = filteredArray[indexPath.row]
            vc.navigationTitle = filteredArray[indexPath.row].name ?? ""
            navigationController?.pushViewController(vc, animated: true)
            if isNewPayFromFianance{
                vc.currencyIDcurrent = currencyIDcurrent
                vc.accountNoCurrent = accountNoCurrent
                vc.isNewPayFromFianance = true
            }
        }else{
            let sb = UIStoryboard(name: "Utilities", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "SubCategoriesViewController") as! SubCategoriesViewController
            vc.categotyID = categoryArray[indexPath.row].categoryID ?? 0
            vc.navigationTitle = categoryArray[indexPath.row].categoryName ?? ""
            if isNewPayFromFianance{
                vc.currencyIDcurrent = currencyIDcurrent
                vc.accountNoCurrent = accountNoCurrent
                vc.isNewPayFromFianance = true
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}


extension CategoriesViewController: UISearchBarDelegate{
    //поиск
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            
            isSearching = false
            if listUtilities.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
                self.emptyImage.image = UIImage(named: "empty_icn")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
            tableView.reloadData()

        }else{
            
            filteredArray = listUtilities.filter({ (utility: UtilityModel) -> Bool in
                return utility.name?.lowercased().contains(searchText.lowercased()) ?? true
            })
            if filteredArray.isEmpty{
                self.tableView.isHidden = true
                self.emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.tableView.isHidden = false
                self.emptyStateStack.isHidden = true
            }
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
       
    }
    // обработка кнопки search
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        filteredArray.removeAll()
        isSearching = false
        search.text = ""
        if listUtilities.isEmpty{
            tableView.isHidden = true
            emptyStateStack.isHidden = false
            self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
            self.emptyImage.image = UIImage(named: "empty_icn")
        }else{
            tableView.isHidden = false
            emptyStateStack.isHidden = true
        }
        tableView.reloadData()
        view.endEditing(true)
    }
}
