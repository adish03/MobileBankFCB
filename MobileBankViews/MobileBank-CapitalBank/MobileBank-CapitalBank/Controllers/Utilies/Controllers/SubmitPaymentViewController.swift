//
//  SubmitPaymentViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/26/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import RxSwift
import RxCocoa

protocol EditPaymentsProtocol {
    func editOperationData(operationID: Int, isRepeatPay: Bool, isRepeatOperation: Bool, payData: UtilityPaymentOperationModel)
}


class SubmitPaymentViewController: BaseViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var numberUtility: UILabel!
    @IBOutlet weak var accountLabel: UILabel!
    @IBOutlet weak var contractLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    @IBOutlet weak var comissionLabel: UILabel!
    @IBOutlet weak var totalPayLabel: UILabel!
    @IBOutlet weak var paymentCommentLabel: UILabel!
    @IBOutlet weak var paymentCommentView: UIView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var FIO: UIStackView!
    
    @IBOutlet weak var fullNameClient: UILabel!
    
    var selectedDeposit: Deposit!
    var submitModel: SubmitForPayModel!
    var payData: UtilityPaymentOperationModel!
    var operationModel: OperationModel!
    var currentUtility: UtilityModel!
    var operationID = 0
    var code = "000000"
    var currentCurrency = ""
    var isRepeatPay = true
    var isRepeatOperation = true
    var delegate: EditPaymentsProtocol?
    var imageUtility = ""

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isRepeatPay{
            delegate?.editOperationData(operationID: operationID, isRepeatPay: isRepeatPay, isRepeatOperation: isRepeatOperation, payData: payData)
        }
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        FIO.isHidden = true
        
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = localizedText(key: "confirmation")

        let sum = "\(Double(submitModel.sum) - abs((Double(submitModel.commission) ?? 0.0))) \(currentCurrency)"
        let comission = "\(submitModel.commission) \(currentCurrency)"
        let total = "\(submitModel.total) \(currentCurrency)"
        
        if submitModel.currencyID == 1203 || submitModel.currencyID == 1163 {
            paymentCommentView.isHidden = false
        }
        
        nameLabel.text = submitModel.name
        numberUtility.text = submitModel.props
        accountLabel.text = submitModel.account
        contractLabel.text = "\(operationID)"
        sumLabel.text = sum
        comissionLabel.text = comission
        totalPayLabel.text = total
        descriptionLabel.text = submitModel.description
        paymentCommentLabel.text = submitModel.comment
        
        if(payData != nil){
            fullNameClient.text = payData.fullName
        }
    }
    
    @IBAction func submitButton(_ sender: Any) {
        isRepeatPay = false
        postIsRequireConfirmOperationCode(operationID: operationID)
       
    }
    //    переход по segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "toSMSSegue"{
            let vc = segue.destination as! SMSForUtilitiesViewController
            
            vc.submitModel = submitModel
            vc.operationID = operationID
            vc.currentCurrency = ValueHelper.shared.getCurrentCurrency(currnecyID: submitModel.currencyID)
            vc.operationModel = operationModel
            vc.selectedDeposit = selectedDeposit
            vc.currentUtility = currentUtility
            vc.imageUtility = imageUtility
        }
    }
// Подтвердить операцию
    func postConfirmPay(operationModel: OperationModel){
        showLoading()
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    let storyboard = UIStoryboard(name: "Utilities", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "TemplateViewController") as! TemplateViewController
                    let navigationController = UINavigationController(rootViewController: vc)
                    vc.submitModel = self.submitModel
                    vc.imageUtility = self.imageUtility
                    vc.currentCurrency = self.currentCurrency
                    vc.operationID = self.operationID
                    vc.selectedDeposit = self.selectedDeposit
                    vc.currentUtility = self.currentUtility
                    vc.navigationTitle = self.localizedText(key: "operation_status")
                    navigationController.modalPresentationStyle = .fullScreen
                    self.present(navigationController, animated: true, completion: nil)
                    
                    if result.message != ""{
                        self.showAlertController(result.message ?? "")
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
//    Необходим ли код подтверждения
    func postIsRequireConfirmOperationCode(operationID: Int){
        showLoading()
        managerApi
            .postIsRequireConfirmOperationCode(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] isConfirm in
                    guard let `self` = self else { return }
                    self.operationModel = OperationModel(operationID: operationID,
                                                         code: self.code,
                                                         operationTypeID: InternetBankingOperationType.UtilityPayment.rawValue)
                    if isConfirm == "true"{
                        self.sendSms(operationID: operationID)
                        self.hideLoading()
                    }
                    if isConfirm == "false"{
                        self.postConfirmPay(operationModel: self.operationModel)
                    }
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //отправка смс
    func  sendSms(operationID: Int){ //for resend sms
        showLoading()
        managerApi
            .postSendConfirmOperationCode(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (sms) in
                    guard let `self` = self else { return }
                    
                    let alertController = UIAlertController(title: nil, message: sms, preferredStyle: .alert)
                    
                    let action = UIAlertAction(title: "OK", style: .default) { (action) in
                        self.performSegue(withIdentifier: "toSMSSegue", sender: self)
                    }
                    alertController.addAction(action)
                    self.present(alertController, animated: true, completion: nil)
                    
                    self.hideLoading()
            },
                onError: {(error) in
                    
                    self.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}

