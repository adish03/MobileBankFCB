//
//  UtilityTemplateViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/19/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//


import MobileBankCore
import RxSwift
import AlamofireImage

protocol UtilityProtocol {
    func selectedUtility(utility: UtilityModel)
}


class UtilityTemplateViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    @IBOutlet weak var emptyStack: UIStackView!
    
    var categotyID = 0
    var utilities: Utilities!
    var utilitiesByCategoryID = [UtilityModel]()
    var navigationTitle = ""
    var listUtilities: [UtilityModel]!
    var filteredArray = [UtilityModel]()
    var isSearching = false
    
    var delegate: UtilityProtocol?
    var isCreateTemplate = false
    var category: UtilityModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        emptyStateStack.isHidden = true
        
        if let utilities = UserDefaultsHelper.utilities{
            self.utilities = utilities
        }
        if let listUtilities = UserDefaultsHelper.listUtilities{
            self.listUtilities = listUtilities
        }
        
        if let items = utilities?.items{
            for item in items{
                if item.categoryID == categotyID{
                    utilitiesByCategoryID = item.items ?? []
                }
            }
        }
        if utilitiesByCategoryID.count == 0{
            emptyStateStack.isHidden = false
            tableView.separatorStyle = .none
        }
        tableView.reloadData()
    }
    
}



extension UtilityTemplateViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearching{
            return filteredArray.count
        }
        return utilitiesByCategoryID.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! UtilityTemplateTableViewCell
        
        if isSearching{
            cell.subCategoryLabel.text = filteredArray[indexPath.row].name
            self.getImageTemplate(image: filteredArray[indexPath.row].logo ?? "", imageView: cell.imageSubCategory, cell: nil, cell1: cell)
        }else{
            cell.subCategoryLabel.text = utilitiesByCategoryID[indexPath.row].name
            getImageTemplate(image: utilitiesByCategoryID[indexPath.row].logo ?? "", imageView: cell.imageSubCategory, cell: nil, cell1: cell)
        }
        return cell
    }
    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if filteredArray.count != 0{
            if isCreateTemplate{
                delegate?.selectedUtility(utility: utilitiesByCategoryID[indexPath.row])
            }else{
                let sb = UIStoryboard(name: "Utilities", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                vc.currentUtility = filteredArray[indexPath.row]
                vc.navigationTitle = filteredArray[indexPath.row].name ?? ""
                navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            if isCreateTemplate{
                 delegate?.selectedUtility(utility: utilitiesByCategoryID[indexPath.row])
            }else{
                let sb = UIStoryboard(name: "Utilities", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                vc.currentUtility = utilitiesByCategoryID[indexPath.row]
                vc.navigationTitle = utilitiesByCategoryID[indexPath.row].name ?? ""
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}

extension UtilityTemplateViewController: UISearchBarDelegate{
    //поиск
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            
            isSearching = false
            if utilitiesByCategoryID.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
                self.emptyImage.image = UIImage(named: "empty_icn")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
            tableView.reloadData()
            
        }else{
            
            filteredArray = utilitiesByCategoryID.filter({ (utility: UtilityModel) -> Bool in
                return utility.name?.lowercased().contains(searchText.lowercased()) ?? true
            })
            if filteredArray.isEmpty{
                self.tableView.isHidden = true
                self.emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.tableView.isHidden = false
                self.emptyStateStack.isHidden = true
            }
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        filteredArray.removeAll()
        isSearching = false
        search.text = ""
        if listUtilities.isEmpty{
            tableView.isHidden = true
            emptyStateStack.isHidden = false
            self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
            self.emptyImage.image = UIImage(named: "empty_icn")
        }else{
            tableView.isHidden = false
            emptyStateStack.isHidden = true
        }
        tableView.reloadData()
        view.endEditing(true)
    }
}
