//
//  CategoryTemplateViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/19/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import MobileBankCore
import RxSwift
import AlamofireImage
import Alamofire

protocol CategoriesProtocol {
    func selectedCategory(category: CategoriesType)
}

class CategoryTemplateViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var emptyStateLabel: UILabel!
    @IBOutlet weak var emptyImage: UIImageView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    var utilities: Utilities!
    var categories: Categories!
    var listUtilities: [UtilityModel]!
    var categoryArray = [CategoriesType]() // need for search
    var index = 1
    var modifyDateCategories = ""
    var modifyDateUtilities = ""
    var filteredArray = [UtilityModel]()
    
    var delegate: CategoriesProtocol?
    var isCreateTemplate = false
    var category: CategoriesType!
    
    var isSearching = false
    var navigationTitle = ""

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        navigationItem.title = navigationTitle
        emptyStateStack.isHidden = true
        
        hideKeyboardWhenTappedAround()
       
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: Constants.MAIN_COLOR)]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
//        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        if let categories = UserDefaultsHelper.categories{
            self.categories = categories
            self.categoryArray = categories.items!
        }
        if let utilities = UserDefaultsHelper.utilities{
            self.utilities = utilities
        }
        if let listUtilities = UserDefaultsHelper.listUtilities{
            self.listUtilities = listUtilities
        }
        
        if let date = UserDefaultsHelper.modifyUtlitiesDate{
            self.modifyDateUtilities = date
        }
        if let date = UserDefaultsHelper.modifyCategoriesDate{
            self.modifyDateCategories = date
        }
        
        if categories != nil{
            isModifyCategories(modifyDate: modifyDateCategories)
            isModifyUtilities(modifyDate: modifyDateUtilities)
        }else{
            getCategoriesAndUtilities()
        }
    }
}

extension CategoryTemplateViewController{
    //    получение категорий и утилит
    func  getCategoriesAndUtilities(){
        let categoriesObservable = managerApi
            .getCategories()
        
        let utilitiesObservable = managerApi
            .getUtilities()
        
        let getListUtilitiesObservable = managerApi
            .getListUtilities()
        
        
        showLoading()
        
        Observable
            .zip(categoriesObservable, utilitiesObservable, getListUtilitiesObservable)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (categories, utilities, listUtilities) in
                    guard let `self` = self else { return }
                    self.listUtilities = listUtilities
                    self.categories = categories
                    self.categoryArray = categories.items!
                    self.utilities = utilities
                    self.modifyDateCategories = categories.date ?? ""
                    self.modifyDateUtilities = utilities.date ?? ""
                    
                    UserDefaultsHelper.listUtilities = listUtilities
                    UserDefaultsHelper.categories = categories
                    UserDefaultsHelper.utilities = utilities
                    UserDefaultsHelper.modifyUtlitiesDate = self.modifyDateUtilities
                    UserDefaultsHelper.modifyCategoriesDate = self.modifyDateCategories
                    
                    self.tableView.reloadData()
                    self.hideLoading()
                    
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    проверка на изменение в категории

    func  isModifyCategories(modifyDate: String){
        showLoading()
        managerApi
            .getIsModifyCategories(date: modifyDate)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (isModify) in
                    guard let `self` = self else { return }
                    if isModify == "true"{
                        self.getCategoriesAndUtilities()
                    }
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    func  isModifyUtilities(modifyDate: String){
        showLoading()
        managerApi
            .getIsModifyUtilities(date: modifyDate)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] (isModify) in
                    guard let `self` = self else { return }
                    if isModify == "true"{
                        self.getCategoriesAndUtilities()
                    }
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}



extension CategoryTemplateViewController: UITableViewDataSource, UITableViewDelegate{
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if isSearching{
            return filteredArray.count
        }
        return  categoryArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! CategoryTemplateTableViewCell
        
        if isSearching{
            cell.categoryLabel.text = filteredArray[indexPath.row].name
            self.getImageTemplate(image: filteredArray[indexPath.row].logo ?? "", imageView: cell.categoryImage, cell: cell, cell1: nil)
        }else{
            cell.categoryLabel.text = categoryArray[indexPath.row].categoryName
            DispatchQueue.main.async {
                self.getImageTemplate(image: self.categoryArray[indexPath.row].logoFileName ?? "", imageView: cell.categoryImage, cell: cell, cell1: nil)
            }
        }
        
        return cell
    }
    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if filteredArray.count != 0{
            if isCreateTemplate{
                delegate?.selectedCategory(category: categoryArray[indexPath.row])
            }else{
                let sb = UIStoryboard(name: "Utilities", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                vc.currentUtility = filteredArray[indexPath.row]
                vc.navigationTitle = filteredArray[indexPath.row].name ?? ""
                navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            if isCreateTemplate{
                delegate?.selectedCategory(category: categoryArray[indexPath.row])
            }else{
                let sb = UIStoryboard(name: "Utilities", bundle: nil)
                let vc = sb.instantiateViewController(withIdentifier: "SubCategoriesViewController") as! SubCategoriesViewController
                vc.categotyID = categoryArray[indexPath.row].categoryID ?? 0
                vc.navigationTitle = categoryArray[indexPath.row].categoryName ?? ""
                navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}


extension CategoryTemplateViewController: UISearchBarDelegate{
    //поиск
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == ""{
            
            isSearching = false
            if listUtilities.isEmpty{
                tableView.isHidden = true
                emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
                self.emptyImage.image = UIImage(named: "empty_icn")
            }else{
                tableView.isHidden = false
                emptyStateStack.isHidden = true
            }
            tableView.reloadData()
            
        }else{
            
            filteredArray = listUtilities.filter({ (utility: UtilityModel) -> Bool in
                return utility.name?.lowercased().contains(searchText.lowercased()) ?? true
            })
            if filteredArray.isEmpty{
                self.tableView.isHidden = true
                self.emptyStateStack.isHidden = false
                self.emptyStateLabel.text = localizedText(key: "nothing_found_change_search_criteria")
                self.emptyImage.image = UIImage(named: "empty_search_icn")
            }else{
                self.tableView.isHidden = false
                self.emptyStateStack.isHidden = true
            }
            isSearching = true
            tableView.reloadData()
        }
        searchBar.setShowsCancelButton(true, animated: true)
        
    }
    // добавление кнопки ОТМЕНА в searchBar
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
        filteredArray.removeAll()
        isSearching = false
        search.text = ""
        if listUtilities.isEmpty{
            tableView.isHidden = true
            emptyStateStack.isHidden = false
            self.emptyStateLabel.text = localizedText(key: "the_list_of_operations_is_empty")
            self.emptyImage.image = UIImage(named: "empty_icn")
        }else{
            tableView.isHidden = false
            emptyStateStack.isHidden = true
        }
        tableView.reloadData()
        view.endEditing(true)
    }
}
