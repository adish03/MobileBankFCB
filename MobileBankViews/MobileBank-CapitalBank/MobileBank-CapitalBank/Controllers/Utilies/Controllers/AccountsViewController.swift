//
//  AccountsViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/28/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
import ObjectMapper
import RxSwift
import RxCocoa

protocol DepositProtocol {
    
    func selectedDepositUser(deposit: Deposit)
}

protocol DepositProtocolTo {
    
    func selectedDepositUserTo(deposit: Deposit)
}

protocol NewDepositProtocol {
    
    func newDepositSettings(currency: Int)
}

class AccountsViewController: BaseViewController {
   
    var delegate: DepositProtocol?
    var delegateTo: DepositProtocolTo?
    var navigationTitle = ""
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyStateStack: UIStackView!
    
    var currencies = [Currency]()
    var deposits = [Deposit]()
    var depositsWithCurrencies = [Deposit]()
    var currentIndexPath: Int!
    
    var accounts = [Accounts]()
    var selectedDeposit: Deposit!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Для отсеивание Депозитных счет
        self.accounts = accounts.filter{ $0.accountType != 1}
        
        tableView.estimatedRowHeight = 140
        tableView.rowHeight = UITableView.automaticDimension
        
        navigationItem.title = navigationTitle
        tableView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        if accounts.count == 0{
            emptyStateStack.isHidden = false
            tableView.separatorStyle = .none
        }
        tableView.reloadData()
        
    }
    
   
}

extension AccountsViewController: UITableViewDataSource, UITableViewDelegate{
   
    func numberOfSections(in tableView: UITableView) -> Int {
       return accounts.count
    }
    // количество строк таблицы
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return accounts[section].items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (accounts[section].items?.count ?? 0) > 0 {
            return accounts[section].text
        } else {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 16))
        footerView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        
        return footerView
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height))
        headerView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        let label = UILabel(frame: CGRect(x: 16, y: 13, width: view.frame.size.width, height: 25))
        
        if (accounts[section].items?.count ?? 0) > 0 {
            label.text = getAccountTitle(accountType: self.accounts[section].accountType!).uppercased()
        } else {
            label.text = ""
        }
            
        label.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        headerView.addSubview(label)
        
        return headerView
    }
    
    func getAccountTitle(accountType: Int) -> String {
        switch accountType {
        case 0:
            return localizedText(key: "current_accounts")
        case 1:
            return localizedText(key: "deposit_accounts")
        case 2:
            return localizedText(key: "cards")
        case 3:
            return localizedText(key: "credits")
            
        default:
            return "Unknown"
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        
        return 16
    }
    
    //  изменение высоты Header
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
       
            return 46
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AccountTableViewCell

        if let item = accounts[indexPath.section].items?[indexPath.row]{
            let fullAccountsText: String = "\(String(describing: item.name ?? ""))  ∙∙\(String(describing: item.accountNo?.suffix(4) ?? ""))"
            let stringBalance = String(format:"%.2f",item.balance ?? 0.0)
            let balance = stringBalance + " \(String(describing: item.currencySymbol  ?? ""))"
            cell.accountNameLabel.text = fullAccountsText
            cell.sumLabel.text = balance
            self.getImageAccount(image: item.imageName ?? "", imageView: cell.accountImage, cell: cell)
            tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
            if selectedDeposit != nil{
                if item.accountNo == selectedDeposit.accountNo && item.balance == selectedDeposit.balance && item.currencyID == selectedDeposit.currencyID {
                    cell.isSelected = true
                }
            }
        }
        return cell
    }

    // обработка действия при выборе ячейки

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AccountTableViewCell
        
        if let selectedDeposit = accounts[indexPath.section].items?[indexPath.row]{
            //cell.isSelected = true
            delegate?.selectedDepositUser(deposit: selectedDeposit)
            delegateTo?.selectedDepositUserTo(deposit: selectedDeposit)
            
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.isSelected {
            cell.setSelected(true, animated: false)
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }
}
