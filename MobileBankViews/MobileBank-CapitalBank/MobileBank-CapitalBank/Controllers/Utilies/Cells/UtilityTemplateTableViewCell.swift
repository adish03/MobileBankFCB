//
//  UtilityTemplateTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/19/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
// класс для кастомизации ячейки таблицы
class UtilityTemplateTableViewCell: UITableViewCell {

    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var subCategoryLabel: UILabel!
    @IBOutlet weak var imageSubCategory: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //  кастомизация иконки ячейки
        customImageCell(view: view, image: imageSubCategory)
    }
    // кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.contentView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        } else {
            self.contentView.backgroundColor = .white
        }
         self.accessoryType = selected ? .checkmark : .none
    }

}
