//
//  AccountTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/28/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import MobileBankCore
// класс для кастомизации ячейки таблицы
class AccountTableViewCell: UITableViewCell {
    @IBOutlet weak var accountImage: UIImageView!
    @IBOutlet weak var accountNameLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //  кастомизация иконки ячейки
        customImageCell(view: view, image: accountImage)
    }
// кастомизация ячейки при выборе 
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.accessoryType = selected ? .checkmark : .none
        if selected {
            contentView.backgroundColor = UIColor(hexFromString: Constants.COLOR_BACKGROUND)
        } else {
            self.contentView.backgroundColor = .white
        }
    }
}
