//
//  UtilityTableViewCell.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/22/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit

class UtilityTableViewCell: UITableViewCell {

    @IBOutlet weak var utilityImage: UIImageView!
    
    @IBOutlet weak var utilityNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
