//
//   ScheduleHelper.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/24/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import RxSwift

// Общий класс для получения и сохранения данных используемых в приложении
class  ScheduleHelper: BaseViewController{
    public static let shared = ScheduleHelper()
    
    // helper  для получения расписания
    func scheduleDescriptionHelper(StartDate: String?, EndDate: String?, RecurringTypeID: Int?, ProcessDay: Int?) -> String{
        var description = ""
        var dateStart = ""
        var dateEnd = ""
        var processDay = ""
        
        if let start = StartDate{
            dateStart = dateForSchedule(stringDate: start)
        }
        if let end = EndDate{
            dateEnd = dateForSchedule(stringDate: end)
        }
        if let process = ProcessDay{
            processDay = weekDayHelper(day: process)
        }
        
        switch RecurringTypeID {
        case ScheduleRepeat.OneTime.rawValue:
            description = "\(localizedText(key: "only_1_time")) \(dateStart)"
            
        case ScheduleRepeat.EachWeek.rawValue:
            description = "\(localizedText(key: "every_week")) с \(dateStart) по \(dateEnd) по \(processDay)"
            
        case ScheduleRepeat.EachMonth.rawValue:
            description = "\(localizedText(key: "every_month")) c \(dateStart) по \(dateEnd) \(String(describing: ProcessDay ?? 0))"
            
        default:
            description = ""
        }
        return description
    }
    
    //    обработка даты
    func dateForSchedule(stringDate: String) -> String{
        var dateString = ""
        let dateFormater = DateFormatter()
        let dateFormaterFirst = DateFormatter()
        let dateFormaterSecond = DateFormatter()
        dateFormaterFirst.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormater.locale = Locale(identifier: "language".localized)
        dateFormaterSecond.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormater.locale = Locale(identifier: "language".localized)
        dateFormater.dateFormat = "dd/MM/yyyy"
        dateFormater.locale = Locale(identifier: "language".localized)
        if let newDate: Date = dateFormaterFirst.date(from:stringDate){
            dateString = dateFormater.string(from: (newDate))
            return dateString
        }
        if let newDate: Date = dateFormaterSecond.date(from:stringDate){
            dateString = dateFormater.string(from: (newDate))
            return dateString
        }

        return dateString
    }
    //    обработка даты
    func dateForFullSchedule(stringDate: String) -> String{
        var dateString = ""
        let dateFormater = DateFormatter()
        let dateFormaterFirst = DateFormatter()
        let dateFormaterSecond = DateFormatter()
        dateFormaterFirst.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        dateFormaterSecond.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        dateFormater.dateFormat = "dd MMMM yyyy"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        if let newDate: Date = dateFormaterFirst.date(from:stringDate){
            dateString = dateFormater.string(from: (newDate))
            return dateString
        }
        if let newDate: Date = dateFormaterSecond.date(from:stringDate){
            dateString = dateFormater.string(from: (newDate))
            return dateString
        }
        
        return dateString
    }
    //    обработка даты
    func dateSchedule() -> String{
        var dateString = ""
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd MMMM yyyy"
        dateFormater.locale = Locale(identifier: "language".localized)
        let newDate = Date()
        dateString = dateFormater.string(from: (newDate))
        return dateString
    }
    //    обработка даты
    func dateCurrentForServer() -> String{
        var dateString = ""
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormater.locale = Locale(identifier: "language".localized)
        let newDate = Date()
        dateString = dateFormater.string(from: (newDate))
        return dateString
    }
    
    //    обработка даты
    func dateForServer(stringDate: String) -> String{
        var dateString = ""
        let dateFormater = DateFormatter()
        let dateFormater1 = DateFormatter()
        dateFormater1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormater.locale = Locale(identifier: "language".localized)
        dateFormater.dateFormat = "dd MMMM yyyy"
        dateFormater.locale = Locale(identifier: "language".localized)
        if stringDate != ""{
            let newDate: Date = dateFormater.date(from:stringDate)!
            dateString = dateFormater1.string(from: (newDate))
        }else{
            dateString = ""
        }
        return dateString
    }
    //    обработка даты
    func dateFromServer(stringDate: String) -> String{
        var dateString = ""
        let dateFormater = DateFormatter()
        let dateFormaterFirst = DateFormatter()
        let dateFormaterSecond = DateFormatter()
        dateFormaterFirst.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        dateFormaterSecond.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        dateFormater.dateFormat = "dd MMMM yyyy"
        dateFormater.locale = Locale(identifier: "language".localized)
        
        if let newDate: Date = dateFormaterFirst.date(from:stringDate){
            dateString = dateFormater.string(from: (newDate))
            return dateString
        }
        if let newDate: Date = dateFormaterSecond.date(from:stringDate){
            dateString = dateFormater.string(from: (newDate))
            return dateString
        }
        
        return dateString
    }
    
    
    // обработка дней недели
    func weekDayHelper(day: Int?) -> String{
        var weekDayString = ""
        switch day {
        case ScheduleWeekDay.monday.rawValue:
            weekDayString = localizedText(key: "mon")
            
        case ScheduleWeekDay.tuesday.rawValue:
            weekDayString = localizedText(key: "tue")
            
        case ScheduleWeekDay.wednesday.rawValue:
            weekDayString = localizedText(key: "wed")
            
        case ScheduleWeekDay.thursday.rawValue:
            weekDayString = localizedText(key: "th")
            
        case ScheduleWeekDay.friday.rawValue:
            weekDayString = localizedText(key: "fri")
            
        case ScheduleWeekDay.saturday.rawValue:
            weekDayString = localizedText(key: "sat")
            
        case ScheduleWeekDay.sunday.rawValue:
            weekDayString = localizedText(key: "the_sun")
            
        default:
            weekDayString = ""
        }
        return weekDayString
    }
    
    // helper  для получения расписания
    func scheduleFullScheduleHelper(StartDate: String?, EndDate: String?, RecurringTypeID: Int?, ProcessDay: Int?) -> Schedule?{
        
        var description: Schedule?
        
        switch RecurringTypeID {
        case ScheduleRepeat.EachMonth.rawValue:
            description = Schedule(operationID: 0, startDate: StartDate, endDate: EndDate, recurringTypeID: RecurringTypeID, processDay: ProcessDay)
            
        case ScheduleRepeat.OneTime.rawValue:
            description = Schedule(operationID: 0, startDate: StartDate, endDate: EndDate, recurringTypeID: RecurringTypeID, processDay: ProcessDay)
            
        case ScheduleRepeat.EachWeek.rawValue:
            description = Schedule(operationID: 0, startDate: StartDate, endDate: EndDate, recurringTypeID: RecurringTypeID, processDay: ProcessDay)
            
        default:
            description = Schedule(operationID: 0, startDate: StartDate, endDate: EndDate, recurringTypeID: RecurringTypeID, processDay: ProcessDay)
        }
        return description
    }
    
    
}
