//
//  BaseViewControllerNew.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/15/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import MobileBankCore
import SVProgressHUD
import AlamofireImage


open class BaseViewController: UIViewController, BaseViewControllerProtocol{
    public var alertController: UIAlertController?
    
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        Logger.logViewDidLoad(self)
        
    }
    
    var PhoneIdVendor = UIDevice.current.identifierForVendor!.uuidString
    let disposeBag = DisposeBag()
    var managerApi:Manager{
        get {return Manager.shared}
    }
    func refresh(){}
    deinit {
        Logger.logDeinit(self)
    }
    
    public func showLoading(){
        SVProgressHUD.show()
    }
    public func showLoadingWithStatus(message: String){
        SVProgressHUD.show(withStatus: message)
    }
    public func hideLoading(){
        SVProgressHUD.dismiss()
    }
    
    open func showController(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        //   self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
        
        
    }
    open func showControllerWithoutAnimation(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        //    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: false, completion: nil)
        
    }
    open func pushController(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        //   self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    open func popControllerWithoutAnimation(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        _ = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        //   self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        self.navigationController?.popViewController(animated: false)
    }
    open func pushControllerWithoutAnimation(StoryBoard: String, ViewControllerID: String){
        let storyboard = UIStoryboard(name: StoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: ViewControllerID)
        //  self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back".localized, style: .plain, target: nil, action: nil)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    open func alertNotRegistrationUser(error: String){
        let alert = UIAlertController(title: error, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    open func showAlertController(_ message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    func showAlertConfirmAuthAdditional(message: String) {
        let alert = UIAlertController(title: localizedText(key: "code_is_confirmed"), message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: self.localizedText(key: "continue_title"), style: .cancel) { (action) in
            
            
            self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    static func showMessage(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    
    
    //    отправка смс
    func sendSMS(){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SmsViewController")
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    //    отправка смс etoken
    func sendSMSEtoken(){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OtkSmsViewController")
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    //переотправка смс
    func  resendSms(){ //for resend sms
        showLoading()
        managerApi
            .postSMS()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](sms) in
                    guard let `self` = self else { return }
                    
                    self.hideLoading()
                },
                onError: {(error) in
                    print(error)
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    проверка на наличие пин кода
    func  isExistPin(){
        
        showLoading()
        managerApi
            .getIsPinExist(ApplicationID: UIDevice.current.identifierForVendor!.uuidString)
            .subscribe(
                onNext: {[weak self](pinIsTrue) in
                    guard let `self` = self else { return }
                    
                    let pinIsTrue = pinIsTrue
                    if pinIsTrue == "true"{
                        UserDefaultsHelper.pinIsEnabled = true
                        
                    }else{
                        UserDefaultsHelper.pinIsEnabled = false
                    }
                    ApiHelper.shared.getAllowedOperations()
                    self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                    
                    self.hideLoading()
                },
                onError: {[weak self](error) in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    print(error)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    отправка токена
    func sendEtoken(){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "OtkViewController")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    //    провкерка при фокусе на инпут
    func viewTextFieldShouldBeginEditing(viewTextField: UIView, labelError: UILabel){
        viewTextField.layer.borderColor = UIColor(hexFromString: Constants.MAIN_COLOR).cgColor
        viewTextField.layer.borderWidth = 2.0
        labelError.text = ""
    }
    //    получение картинки
    func  getImage(image: String, imageView: UIImageView?, cell: CategoryTableViewCell?, cell1: SubCategoryTableViewCell?){
        if let image = imageCache[image]{
            imageView?.image = image
            cell?.categoryImage.image = image
            cell1?.imageSubCategory.image = image
        } else {
            managerApi
                .getImage(imageName: image)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self](data) in
                        DispatchQueue.main.async {
                            
                            self?.imageCache[image] = UIImage(data: data)
                            imageView?.image = self?.imageCache[image]
                            cell?.categoryImage.image = self?.imageCache[image]
                            cell1?.imageSubCategory.image = self?.imageCache[image]
                            
                        }
                    },
                    onError: {(error) in
                        print(error)
                })
                .disposed(by: disposeBag)
        }
    }
    //    получение картинки
    func  getImageTemplate(image: String, imageView: UIImageView?, cell: CategoryTemplateTableViewCell?, cell1: UtilityTemplateTableViewCell?){
        
        managerApi
            .getImage(imageName: image)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {(data) in
                    DispatchQueue.main.async {
                        imageView?.image = Image(data: data)
                        cell?.categoryImage.image = Image(data: data)
                        cell1?.imageSubCategory.image = Image(data: data)
                    }
            },
                onError: {(error) in
                    print(error)
            })
            .disposed(by: disposeBag)
    }
    //    получение картинки
    func  getImageMyAccount(image: String, imageView: UIImageView?, cell: MyAccountTableViewCell){
        if let image = imageCache[image]{
            imageView?.image = image
            cell.accountImage.image = image
        } else {
            managerApi
                .getImage(imageName: image)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self](data) in
                        DispatchQueue.main.async {
                            self?.imageCache[image] = UIImage(data: data)
                            imageView?.image = self?.imageCache[image]
                            cell.accountImage.image = self?.imageCache[image]
                        }
                    },
                    onError: {(error) in
                        print(error)
                })
                .disposed(by: disposeBag)
        }
    }
    //    получение картинки
    var imageCache = [String:UIImage]()
    func  getImageProduct(image: String, imageView: UIImageView?, cell: ProductsDepositTableViewCell){
        if let image = imageCache[image]{
            imageView?.image = image
            cell.productImage.image = image
        } else {
            managerApi
                .getImage(imageName: image)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self] (data) in
                        DispatchQueue.main.async {
                            self?.imageCache[image] = UIImage(data: data)
                            imageView?.image = self?.imageCache[image]
                            cell.productImage.image = self?.imageCache[image]
                        }
                    },
                    onError: {(error) in
                        DispatchQueue.main.async {
//                            cell.productImage.image = UIImage(named: "Image_placeholder")
                        }
                        print(error)
                })
                .disposed(by: disposeBag)
        }
    }
    //    получение картинки
    func  getImageCardAccounts(image: String, imageView: UIImageView?, cell: CardTableViewCell){
        if let image = imageCache[image]{
            imageView?.image = image
            cell.accountImage.image = image
        } else {
            managerApi
                .getImage(imageName: image)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self](data) in
                        DispatchQueue.main.async {
                            self?.imageCache[image] = UIImage(data: data)
                            imageView?.image = self?.imageCache[image]
                            cell.accountImage.image = self?.imageCache[image]
                        }
                    },
                    onError: {(error) in
                        DispatchQueue.main.async {
                            cell.accountImage.image = UIImage(named: "logoNew")
                        }
                        print(error)
                })
                .disposed(by: disposeBag)
        }
    }
    //  получение картинки
    func  getImageMyTemplates(image: String, imageView: UIImageView?, cell: TemplateTableViewCell){
        
        managerApi
            .getImage(imageName: image)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {(data) in
                    DispatchQueue.main.async {
                        cell.templateImage.image = Image(data: data)
                    }
            },
                onError: {(error) in
                    print(error)
            })
            .disposed(by: disposeBag)
    }
    //    получение картинки
    func  getImageMainAccount(image: String, imageView: UIImageView?, cell: AccountsMainTableViewCell){
        
        managerApi
            .getImage(imageName: image)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {(data) in
                    DispatchQueue.main.async {
                        imageView?.image = Image(data: data)
                        cell.accountImage.image = Image(data: data)
                    }
            },
                onError: {(error) in
                    print(error)
            })
            .disposed(by: disposeBag)
    }
    //    получение картинки
    func  getImageMainAccount(image: String, imageView: UIImageView?, cell: BannerCollectionViewCell){
        
        cell.activityIndicator.isHidden = false
        cell.activityIndicator.startAnimating()
        managerApi
            .getImage(imageName: image)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {(data) in
                    DispatchQueue.main.async {
                        imageView?.image = Image(data: data)
                        cell.bannerImage.image = Image(data: data)
                        cell.activityIndicator.stopAnimating()
                    }
            },
                onError: {(error) in
                    print(error)
            })
            .disposed(by: disposeBag)
    }
    //    получение картинки
    func  getImageHistoryPayment(image: String, imageView: UIImageView?, cell: PaymentHistoryTableViewCell){
        if let image = imageCache[image]{
            imageView?.image = image
            cell.accountImage.image = image
        } else {
            managerApi
                .getImage(imageName: image)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self](data) in
                        DispatchQueue.main.async {
                            self?.imageCache[image] = UIImage(data: data)
                            imageView?.image = self?.imageCache[image]
                            cell.accountImage.image = self?.imageCache[image]
                        }
                    },
                    onError: {(error) in
                        DispatchQueue.main.async {
                            cell.accountImage.image = UIImage(named: "logoNew")
                        }
                        print(error)
                })
                .disposed(by: disposeBag)
        }
    }
    //    получение картинки
    func  getImageAccount(image: String, imageView: UIImageView?, cell: AccountTableViewCell){
        if let image = imageCache[image]{
            imageView?.image = image
            cell.accountImage.image = image
        } else {
            managerApi
                .getImage(imageName: image)
                .updateTokenIfNeeded()
                .subscribe(
                    onNext: {[weak self](data) in
                        DispatchQueue.main.async {
                            self?.imageCache[image] = UIImage(data: data)
                            imageView?.image = self?.imageCache[image]
                            cell.accountImage.image = self?.imageCache[image]
                        }
                    },
                    onError: {(error) in
                        DispatchQueue.main.async {
                            cell.accountImage.image = UIImage(named: "logoNew")
                        }
                        print(error)
                })
                .disposed(by: disposeBag)
        }
    }
    //    получение картинки
    func  getImageFromUrl(imageString: String, imageview: UIImageView){
        managerApi
            .getImage(imageName: imageString)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {(data) in
                    
                    DispatchQueue.main.async {
                        imageview.image = UIImage(data: data)
                    }
            },
                onError: {(error) in
                    print(error)
            })
            .disposed(by: disposeBag)
    }
    //    получение SettingsMobile
    func getSettingsMobile(viewController: UIViewController){
        
        showLoading()
        let mobileSettings =  managerApi
            .getMobileSettings()
        
        mobileSettings
            .subscribe(
                onNext: {[weak self](mobileSettings) in
                    guard let `self` = self else { return }
                    UserDefaultsHelper.PinLength = mobileSettings.pinLength
                    UserDefaultsHelper.AuthType = mobileSettings.authType
                    UserDefaultsHelper.SmsCodeLength = mobileSettings.smsCodeLength
                    UserDefaultsHelper.EtokenCodeLength = mobileSettings.etokenCodeLength
                    UserDefaultsHelper.SmsCodeAuthExpires = mobileSettings.smsCodeAuthExpires
                    UserDefaultsHelper.IsInvalidSmsCodeAuthOnIncorrect = mobileSettings.isInvalidSmsCodeAuthOnIncorrect
                    UserDefaultsHelper.mainPageOperationCount = mobileSettings.mainPageOperationCount
                    UserDefaultsHelper.AccountLength = mobileSettings.accountLength
                    UserDefaultsHelper.PasswordLength = mobileSettings.passwordLength
                    UserDefaultsHelper.PasswordStrength = mobileSettings.passwordStrength
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
        
    }
    
    //    текст для перевода из словаря переводов
    func localizedText(key: String) -> String {
        var string = ""
        let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let customPlistURL = docsBaseURL.appendingPathComponent("LocalizeStrings.plist")
        let dictRoot = NSDictionary(contentsOf: customPlistURL) as? [String : AnyObject]
        if let dict = dictRoot {
            string = dict[key] as? String ?? ""
            
        }
        return string
    }
    
    
    
    //    обновление токена
    public func updateToken(user: User){
        
        showLoading()
        managerApi.getUpdateTokenWithoutCertificate(grant_type: "refresh_token", refresh_token: user.refresh_token ?? "")
            .subscribe(
                onNext: {[weak self](user) in
                    guard let `self` = self else { return }
                    UserDefaults.standard.set(false, forKey: "isNeedDismissPinCintroller")
                    SessionManager.shared.updateUser(user: user)
                    ApiHelper.shared.getAllowedOperations()
                    self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                    self.hideLoading()
                },
                onError: {(error) in
                    //                    self.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    print(error)
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }

    //   getUserAvater in the navigation drawer
    func getUserAvatar(loginID: String, userAvatar: UIImageView!) {
        managerApi.getUserAvatar(loginId: loginID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {(avatarString) in
                    DispatchQueue.main.async {
                        //cerciling the image and assinment
                        let dataDecoded: NSData = NSData(base64Encoded: avatarString, options: NSData.Base64DecodingOptions.ignoreUnknownCharacters)!
                        let image = UIImage(data: dataDecoded as Data,scale: 1)
                        userAvatar.layer.masksToBounds = false
                        userAvatar.layer.cornerRadius = userAvatar.frame.size.width / 2
                        userAvatar.clipsToBounds = true
                        userAvatar.image = image
                    }
                },
                onError: {[weak self](error) in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    print(error)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //    обновление токена
    public func updateTokenForSideMenu(user: User){
        
        showLoading()
        managerApi.getUpdateTokenWithoutCertificate(grant_type: "refresh_token", refresh_token: user.refresh_token ?? "")
            .subscribe(
                onNext: {[weak self](user) in
                    guard let `self` = self else { return }
                    UserDefaults.standard.set(false, forKey: "isNeedDismissPinCintroller")
                    
                    SessionManager.shared.updateUser(user: user)
                    var currentIndex = 0
                    for userCurrent in SessionManager.shared.users ?? []
                    {
                        if userCurrent.loginID == user.loginID {
                            break
                        }
                        currentIndex += 1
                    }
                    UserDefaultsHelper.users?[currentIndex] = user
                    SessionManager.shared.current(loginID: user.loginID ?? "")
                    
                    ApiHelper.shared.getAllowedOperations()
                    self.showController(StoryBoard: "Login", ViewControllerID: "PinViewController")
                    self.hideLoading()
                },
                onError: {[weak self](error) in
                    
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    print(error)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    //    алерт при неправильном пин
    func alertWrongPin(errorMessage: String, viewController: UIViewController ){
        let alert = UIAlertController(title: errorMessage, message: "", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel) { (action) in
            self.showController(StoryBoard: "Login", ViewControllerID: "NavLogin")
            
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    //    скрытие клавиатуры
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer =     UITapGestureRecognizer(target: self, action:    #selector(BaseViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //  преобразование даты для сортировки списка истории платежей
    func dateFormater(date: String) -> String{
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM yyyy"
        
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
        } else {
            formatDate = date
        }
        return formatDate
    }
    //  преобразование даты для деталей счета
    func dateFormaterBankDay(date: String) -> String{
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd.MM.yyyy"
        
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
        } else {
            formatDate = date
        }
        return formatDate
    }
    
    func getDateTimeStap(date: String) -> TimeInterval? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
   
        return dateFormatterGet.date(from: date)?.timeIntervalSince1970
    }
    
    func getCurrentTimeStap() -> TimeInterval? {
        return Date().timeIntervalSince1970
    }
    //  преобразование даты для деталей счета
    func dateFormaterOperationDay(date: String) -> String{
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd.MM.yyyy - HH:mm"
        
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
        } else {
            formatDate = date
        }
        return formatDate
    }
    //  преобразование даты для сортировки списка истории платежей
    func dateFormaterForFilter(date: String) -> String{
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd.MM.yyyy"
        
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
        } else {
            formatDate = date
        }
        return formatDate
    }
    //  преобразование даты
    func dateFormaterForLoan(date: String) -> String{
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM yyyy"
        
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
        } else {
            formatDate = date
        }
        return formatDate
    }
    
    //  преобразование даты
    func stringToDateFormatter(date: String) -> Date{
        var newDate: Date!
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMMM yyyy, EEE"
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
            newDate = dateFormatterPrint.date(from: formatDate)
        } else {
            print("There was an error decoding the string")
        }
        return newDate
    }
    //  преобразование даты для фильтрации
    func stringToDateFormatterFiltered(date: String) -> Date{
        var newDate: Date!
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd.MM.yyyy"
        if let date = dateFormatterPrint.date(from: date) {
            formatDate = dateFormatterGet.string(from: date)
            newDate = dateFormatterGet.date(from: formatDate)
        } else {
            print("There was an error decoding the string")
        }
        return newDate
    }
    
    //  преобразование даты
    func dateFormaterForDeposit(date: String) -> String{
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd.MM.yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
        } else {
            formatDate = date
        }
        return formatDate
    }
    
    //  преобразование даты для фильтрации
    func stringToDateFormatterFilteredDate(date: String) -> Date{
        var newDate: Date!
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd.MM.yyyy"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        if let date = dateFormatterPrint.date(from: date) {
            formatDate = dateFormatterGet.string(from: date)
            newDate = dateFormatterGet.date(from: formatDate)
        } else {
            print("There was an error decoding the string")
        }
        return newDate
    }
    //  преобразование даты для фильтрации
    func stringToDateFilterFromDate(date: String) -> Date{
        var newDate: Date!
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS"
        
        if let date = dateFormatterGet.date(from: date) {
            newDate = date
        } else {
            print("There was an error decoding the string")
        }
        return newDate
    }
    //  преобразование даты для истории платежей
    func stringToDateFormatterForHistory(date: String) -> Date{
        var newDate: Date!
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MMMM yyyy"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        if let dateNew = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: dateNew)
            newDate = dateFormatterPrint.date(from: formatDate)
        } else {
            newDate = dateFormatterPrint.date(from: date)
            print("There was an error decoding the string")
        }
        return newDate
    }
    //  преобразование даты для истории платежей фильтрации
    func stringToDateFormatterForHistoryFiltered(date: String) -> Date{
        var newDate: Date!
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MMMM yyyy"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
            newDate = dateFormatterPrint.date(from: formatDate)
        } else {
            print("There was an error decoding the string")
        }
        return newDate
    }
    //  преобразование даты для истории платежей
    func dateToStringFormatterForHistory(date: Date) -> String{
        var newDate: Date!
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd MMMM yyyy"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        let string = dateFormatterPrint.string(from: date)
        newDate = dateFormatterPrint.date(from: string)
        formatDate = dateFormatterGet.string(from: newDate)
        return formatDate
    }
    //  преобразование даты в строку
    func dateToStringFormatter(date: Date) -> String{
        var newDate: Date!
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd.MM.yyyy"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        let string = dateFormatterPrint.string(from: date)
        newDate = dateFormatterPrint.date(from: string)
        formatDate = dateFormatterGet.string(from: newDate)
        return formatDate
    }
    //    обработка текущей даты
    func dateCurrent() -> String{
        var dateString = ""
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd"
        dateFormater.locale = Locale(identifier: "language".localized)
        let newDate = Date()
        dateString = dateFormater.string(from: (newDate))
        return dateString
    }
    //    обработка текущей даты
    func dateCurrentOpendepostit() -> String{
        var dateString = ""
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd.MM.yyyy - HH:mm"
        dateFormater.locale = Locale(identifier: "language".localized)
        let newDate = Date()
        dateString = dateFormater.string(from: (newDate))
        return dateString
    }
    //    обработка даты для фильтрации
    func dateFormaterForFilterReverse(date: String) -> String{
        var formatDate = ""
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd.MM.yyyy"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        
        if let date = dateFormatterGet.date(from: date) {
            formatDate = dateFormatterPrint.string(from: date)
        } else {
            formatDate = date
        }
        return formatDate
    }
    //    обработка даты для сервера
    func dateFormaterToServer(date: String) -> String{
        var formatDate = ""
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMMM yyyy"///////////change date format
        formatter.locale = Locale(identifier: "language".localized)
        
        let dateFormatterDot = DateFormatter()
        dateFormatterDot.dateFormat = "dd.MM.yyyy"
        dateFormatterDot.locale = Locale(identifier: "language".localized)
        
        let dateFormaterShort = DateFormatter()
        dateFormaterShort.dateFormat = "yyyy-MM-dd"
        dateFormaterShort.locale = Locale(identifier: "language".localized)
        
        let formatterToServer = DateFormatter()
        formatterToServer.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"///////////change date format
        formatterToServer.locale = Locale(identifier: "language".localized)
        
        
        if let date = formatter.date(from: date) {
            formatDate = formatterToServer.string(from: date)
            
        }else if let date = dateFormatterDot.date(from: date){
            formatDate = formatterToServer.string(from: date)
            
        }else if let date = dateFormaterShort.date(from: date){
            formatDate = formatterToServer.string(from: date)
            
        }
        else {
            formatDate = date
        }
        return formatDate
    }
    
    
    // получение текущего счет
    func getCurrentTemplateAccount(accountNo: String, currency: Int, accounts: [Accounts]) -> Deposit?{
        var deposit: Deposit?
        for account in accounts{
            for item in account.items!{
                if item.accountNo == accountNo && item.currencyID == currency {
                    deposit = item
                }
            }
        }
        return deposit
    }
    //    подтверждение статуса
    func confirmTypeState(string: String) -> UIColor{
        switch string {
        case localizedText(key: "Save"):
            return #colorLiteral(red: 0.2549019608, green: 0.6588235294, blue: 0.3725490196, alpha: 1)
        default:
            return UIColor.black
        }
    }
    // проверка на счетов на нулевой баланс
    func checkAccountsForZeroBalance(accounts: [Accounts]) -> [Accounts] {
        for account in accounts{
            account.items?.removeAll(where: { $0.balance == 0 })
        }
        return accounts
    }
    
    
}
protocol BaseViewControllerProtocol: class {
    
    var disposeBag:DisposeBag {get}
    var alertController: UIAlertController? {get set}
    func showAlert(withError e:Error)
    func showAlert(withError e:Error, shouldRefresh:Bool, canGoBack:Bool)
    //for overriding
    func refresh()
    func showLoading()
    func hideLoading()
}


extension BaseViewControllerProtocol where Self: UIViewController{
    
    func showAlert(withError e:Error){
        showAlert(withError: e, shouldRefresh: false, canGoBack: false)
    }
    func showAlert(withError e:Error, shouldRefresh:Bool, canGoBack:Bool){
        hideLoading()
        self.alertController?.dismiss(animated: false, completion: nil)
        let alertController = UIAlertController(title: "ERROR".localized,
                                                message: e.localizedDescription + "\n" + (e.pinError?.exceptionMessage ?? ""),
                                                preferredStyle: .alert)
        if shouldRefresh{
            alertController
                .addAction(UIAlertAction(title: "TRY_AGAIN".localized,
                                         style: UIAlertAction.Style.default,
                                         handler: {_ in
                                            self.refresh()
                }))
            if canGoBack{
                alertController
                    .addAction(UIAlertAction(title: "CLOSE".localized,
                                             style: UIAlertAction.Style.default,
                                             handler: {_ in
                                                self.dismissVC()
                    }))
            }
        } else {
            alertController
                .addAction(UIAlertAction(title: "CLOSE".localized,
                                         style: UIAlertAction.Style.default,
                                         handler: nil))
        }
        present(alertController, animated: true, completion: nil)
        self.alertController = alertController
    }
    
    fileprivate func dismissVC(){
        if let navVc = navigationController{
            navVc.popViewController(animated: true)
        } else{
            dismiss(animated: true, completion: nil)
        }
    }
    
    func nameCurrencies(currency: [Currency], currencyRate:[CurrencyRate]) -> [CurrencyRate]{
        
        for id in currencyRate{
            for id2 in currency{
                if id.currencyID == id2.currencyID{
                    id.currency = id2
                }
                if id.crossCurrencyID == id2.currencyID{
                    id.crossCurrency = id2
                }
                
            }
        }
        return currencyRate
    }
    
    func nameCurrencyNational(currencyNational: CurrencyNational, currencies:[Currency]) -> CurrencyNational{
        
        for id in currencies{
            if id.currencyID == currencyNational.currencyID{
                currencyNational.currency = id
            }
        }
        return currencyNational
    }
    func doubleRounded(decimal: String) -> Double{
        var roundedDecimal: Double = 0.0
        let stringToDouble: Double = Double(decimal) ?? 0.0
        let roundedStringDecimal = String(format:"%.4f", stringToDouble)
        roundedDecimal = Double(roundedStringDecimal) ?? 0.0
        print(roundedDecimal)
        return roundedDecimal
    }
    
    
    // отображение статуса оплаты
    func getStatusForHistoryPayment(confirmTypeID: Int, cell: PaymentHistoryTableViewCell ) {
        switch confirmTypeID {
        case ConfirmType.Save.rawValue:
            cell.statusLabel.textColor = #colorLiteral(red: 0.2549019608, green: 0.6588235294, blue: 0.3725490196, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case ConfirmType.Confirm.rawValue:
            cell.statusLabel.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case ConfirmType.InConfirm.rawValue:
            cell.statusLabel.textColor = #colorLiteral(red: 0.5176470588, green: 0.5529411765, blue: 0.5803921569, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case ConfirmType.Error.rawValue:
            cell.statusLabel.textColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 1, green: 0.6372276218, blue: 0.6235846472, alpha: 0.2075539211)
        case ConfirmType.Verified.rawValue:
            cell.statusLabel.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        default:
            cell.statusLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
    }
    // отображение статуса оплаты
    func getStatusForHistoryPaymentMainPage(confirmTypeID: Int, cell: AccountsMainTableViewCell ) {
        switch confirmTypeID {
        case ConfirmType.Save.rawValue:
            cell.statusLabel.textColor = #colorLiteral(red: 0.2549019608, green: 0.6588235294, blue: 0.3725490196, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case ConfirmType.Confirm.rawValue:
            cell.statusLabel.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case ConfirmType.InConfirm.rawValue:
            cell.statusLabel.textColor = #colorLiteral(red: 0.5176470588, green: 0.5529411765, blue: 0.5803921569, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        case ConfirmType.Error.rawValue:
            cell.statusLabel.textColor = #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 1, green: 0.6372276218, blue: 0.6235846472, alpha: 0.2075539211)
        case ConfirmType.Verified.rawValue:
            cell.statusLabel.textColor = UIColor(hexFromString: Constants.MAIN_COLOR)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        default:
            cell.statusLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            
        }
    }
    
    // отображение статуса ошибки валидации пароля
    func getErrorValidationDescription(PasswordType: Int) -> String {
        var errorDescription = ""
        
        switch PasswordType {
            
        case PasswordStrengthType.Lowest.rawValue:
            errorDescription = "Пароль может не достигать минимальной длины"
            
        case PasswordStrengthType.VeryWeak.rawValue:
            errorDescription =  "Пароль должен достигать минимальной длины"
            
        case PasswordStrengthType.Weak.rawValue:
            errorDescription = "Пароль должен достигать минимальной длины, содержать буквы верх. и нижн. регистра"
            
        case PasswordStrengthType.Medium.rawValue:
            errorDescription = "Пароль должен достигать минимальной длины, содержать цифры,  буквы верх. и нижн. регистра"
            
        case PasswordStrengthType.High.rawValue:
            errorDescription = "Пароль должен достигать минимальной длины, содержать спец. символы, цифры,  буквы верх. и нижн. регистра"
            
        default:
            return ""
            
        }
        return errorDescription
    }
    
    // метод для подсчета и сортировки всех коммиссий
    func commissionHelper(commissions: [SwiftCommissionModel],
                          currencyID: Int,
                          sum: Double,
                          summCommissionSender: UILabel,
                          summCommissionSenderCurrency: UILabel,
                          summCommissionSenderStackView: UIStackView,
                          summCommisionIntermediary: UILabel,
                          summCommisionIntermediaryCurrency: UILabel,
                          summCommisionIntermediaryStackView: UIStackView,
                          summCommissionReceiver: UILabel,
                          summCommissionReceiverCurrency: UILabel,
                          summCommissionReceiverStackView: UIStackView ){
        
        var arrayCommissionSender = [SwiftCommissionModel]()
        var arrayCommissionItermediary = [SwiftCommissionModel]()
        var arrayCommissionReciever = [SwiftCommissionModel]()
        
        var SumSender = 0.0
        var SumIntermediary = 0.0
        var SumReciever = 0.0
        
        let sum = Double(sum)
        
        arrayCommissionSender = commissions.filter{$0.currencyID == currencyID}
        arrayCommissionItermediary = commissions.filter{$0.currencyID == commissions[0].currencyID}
        arrayCommissionReciever = commissions.filter{$0.currencyID != commissions[0].currencyID && $0.currencyID == commissions[1].currencyID}
        
        if arrayCommissionSender.count > 0{
            for item in arrayCommissionSender{
                SumSender = (item.commissionSumm ?? 0.0)
                summCommissionSender.text = "\(SumSender)"
                summCommissionSenderCurrency.text = item.currency?.symbol
            }
            if SumSender == 0.0{
                summCommissionSenderStackView.isHidden = true
                summCommissionSender.text = ""
            }else{
                summCommissionReceiverStackView.isHidden = false
            }
        }
        if arrayCommissionItermediary.count > 0{
            for item in arrayCommissionItermediary{
                SumIntermediary += (item.commissionSumm ?? 0.0)
                summCommisionIntermediaryCurrency.text = item.currency?.symbol
            }
            summCommisionIntermediary.text = "\((sum ) + SumIntermediary)"
            summCommisionIntermediaryStackView.isHidden = false
        }
        if arrayCommissionReciever.isEmpty{
            summCommissionReceiverStackView.isHidden = true
        }else{
            for item in arrayCommissionReciever{
                SumReciever += (item.commissionSumm ?? 0.0)
                summCommissionReceiverCurrency.text = item.currency?.symbol
                summCommissionReceiver.text = "\(SumReciever)"
            }
        }
    }
    //  кастомизация иконки ячейки
    func customImage(view: UIView, image: UIImageView){
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.86022228, green: 0.8639231324, blue: 0.8765630126, alpha: 1)
        view.layer.masksToBounds = false
        image.layer.cornerRadius = view.frame.width / 2
        image.clipsToBounds = true
        
    }
    
    
    // получение названия сервиса по id
    func serviceNameBy(id: Int, listUtilities: [UtilityModel] ) -> String{
        var serviceName = ""
        for utility in listUtilities{
            if utility.ID == id {
                serviceName = utility.name ?? ""
            }
        }
        return serviceName
    }
    //  кастомизация иконки ячейки Card
    func customImageCard(view: UIView, image: UIImageView){
        view.layer.borderWidth = 0.5
        view.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        view.layer.shadowRadius = 1
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds = false
        view.layer.shadowPath = UIBezierPath(roundedRect:view.bounds, cornerRadius:view.layer.cornerRadius).cgPath
        image.layer.cornerRadius = 5
        image.clipsToBounds = true
        
    }
    
    func customImageAccount(view: UIView, image: UIImageView){
        view.layer.borderWidth = 0.5
        view.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        view.layer.shadowRadius = 1
        view.layer.shadowOpacity = 0.5
        view.layer.masksToBounds = false
        image.layer.cornerRadius = 5
        image.clipsToBounds = true

        view.layer.cornerRadius = 27
        image.layer.cornerRadius = 27
        
        view.layer.shadowPath = UIBezierPath(roundedRect:view.bounds, cornerRadius: 27).cgPath
        image.clipsToBounds = true
    }
    //     цвет выделенной ячейки
    func selectedCellCustomColor(cell: UITableViewCell) {
        let backgroundView = UIView()
        backgroundView.backgroundColor = #colorLiteral(red: 0.9411764706, green: 0.9450980392, blue: 0.9529411765, alpha: 1)
        cell.selectedBackgroundView = backgroundView
    }
    
    func CustomStatusBar(color: UIColor){
        if #available(iOS 13.0, *) {
            let statusBar = UIView(frame: (UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame)!)
            statusBar.backgroundColor = color
            UIApplication.shared.keyWindow?.addSubview(statusBar)
            
        } else {
            UIApplication.shared.statusBarView.backgroundColor = #colorLiteral(red: 0.07058823529, green: 0.3019607843, blue: 0.5176470588, alpha: 1)
        }
    }
    
}


open class RoundedButton: UIButton {
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 4.0
        layer.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR).cgColor
        clipsToBounds = true
    }
}
open class RoundedButtonWhite: UIButton {
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 4.0
        layer.borderWidth = 1.0
        layer.borderColor = UIColor(hexFromString: Constants.MAIN_COLOR).cgColor
        layer.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        clipsToBounds = true
    }
}
extension UITableViewCell{
    //  кастомизация иконки ячейки
    func customImageCell(view: UIView, image: UIImageView){
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.86022228, green: 0.8639231324, blue: 0.8765630126, alpha: 1)
        view.layer.masksToBounds = false
        image.clipsToBounds = true
    }
    //  кастомизация иконки ячейки
    func customImageCardCell(view: UIView, image: UIImageView){
        view.layer.borderWidth = 1
        view.layer.borderColor = #colorLiteral(red: 0.86022228, green: 0.8639231324, blue: 0.8765630126, alpha: 1)
        view.layer.masksToBounds = false
        image.layer.cornerRadius = 5
        image.clipsToBounds = true
        
    }
}
//  округление чисел
extension Double {
    func roundedDecimal(to scale: Int = 0, mode: NSDecimalNumber.RoundingMode = .plain) -> Decimal {
        var decimalValue = Decimal(self)
        var result = Decimal()
        NSDecimalRound(&result, &decimalValue, scale, mode)
        return result
    }
}

extension UITextField{
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}
