//
//  AllowHelper.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 6/13/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import RxSwift



class AllowHelper: BaseViewController {
    
    //MARK: singleton
    public static var shared = AllowHelper()
    
    //список прав для views
    var allowViewDictionary = [
        "TemplateOperation.TemplateSecurityOperations.IsViewAllowed": false,
        "Deposits.ConversionsSecurityOperations.IsViewAllowed": false,
        "Common.UtilitiesPaymentsSecurityOperations.IsViewAllowed": false,
        "Swift.SwiftSecurityOperations.IsViewAllowed": false,
        "Loans.LoansSecurityOperations.IsViewAllowed": false,
        "Operation.InternalOperationSecurityOperations.IsViewAllowed": false,
        "ClearingGross.ClearingGrossSecurityOperations.IsViewAllowed": false,
        "Cards.CardsSecurityOperations.IsViewAllowed": false
    ]

    //список прав для template
    var templateDictionary = [
        "TemplateOperation.TemplateSecurityOperations.IsViewAllowed": false,
        "TemplateOperation.TemplateSecurityOperations.IsAddAllowed": false,
        "TemplateOperation.TemplateSecurityOperations.IsRemoveAllowed": false,
        "TemplateOperation.TemplateSecurityOperations.IsEditAllowed": false
    ]
    
    //список прав для Swift
    var swiftDictionary = [
        "Swift.SwiftSecurityOperations.IsViewAllowed": false,
        "Swift.SwiftSecurityOperations.IsAddAllowed": false,
        "Swift.SwiftSecurityOperations.IsRemoveAllowed": false,
        "Swift.SwiftSecurityOperations.IsEditAllowed": false,
        "Swift.SwiftSecurityOperations.IsApproveAllowed": false,
        "Swift.SwiftSecurityOperations.IsPrintDocumentsAllowed": false,
        "Swift.SwiftSecurityOperations.IsCreateTemplateAllowed": false
    ]
    
    //список прав для Loan
    var loanDictionary = [
        "Loans.LoansSecurityOperations.IsViewAllowed": false,
        "Loans.LoansSecurityOperations.IsAddAllowed": false,
        "Loans.LoansSecurityOperations.IsRemoveAllowed": false,
        "Loans.LoansSecurityOperations.IsEditAllowed": false,
        "Loans.LoansSecurityOperations.IsViewScheduleAllowed": false,
        "Loans.LoansSecurityOperations.IsOnSchedulePaymentAllowed": false,
        "Loans.LoansSecurityOperations.IsPartialEarlyPaymentAllowed": false,
        "Loans.LoansSecurityOperations.IsPartialEarlyPaymentMainDeptOnlyAllowed": false,
        "Loans.LoansSecurityOperations.IsFullEarlyPaymentAllowed": false,
        "Loans.LoansSecurityOperations.IsApproveAllowed": false,
        "Loans.LoansSecurityOperations.IsPrintDocumentsAllowed": false,
        "Loans.LoansSecurityOperations.IsCreateTemplateAllowed": false,
        "Loans.LoansSecurityOperations.IsCreateRequestAllowed": false,
        "Loans.LoansSecurityOperations.IsRejectRequestAllowed": false
    ]
    
    //список прав для InternalOperation
    var internalOperationDictionary = [
        "Operation.InternalOperationSecurityOperations.IsViewAllowed": false,
        "Operation.InternalOperationSecurityOperations.IsAddAllowed": false,
        "Operation.InternalOperationSecurityOperations.IsRemoveAllowed": false,
        "Operation.InternalOperationSecurityOperations.IsEditAllowed": false,
        "Operation.InternalOperationSecurityOperations.IsApproveAllowed": false,
        "Operation.InternalOperationSecurityOperations.IsPrintDocumentsAllowed": false,
        "Operation.InternalOperationSecurityOperations.IsCreateTemplateAllowed": false,
        "Operation.OperationSecurityOperations.IsViewAllowed": false,
        "Operation.OperationSecurityOperations.IsAddAllowed": false,
        "Operation.OperationSecurityOperations.IsRemoveAllowed": false,
        "Operation.OperationSecurityOperations.IsEditAllowed": false,
        "Operation.OperationSecurityOperations.IsImportOperationAllowed": false
    ]
    
    //список прав для Operation
    var operationDictionary = [
       
        "Operation.OperationSecurityOperations.IsViewAllowed": false,
        "Operation.OperationSecurityOperations.IsAddAllowed": false,
        "Operation.OperationSecurityOperations.IsRemoveAllowed": false,
        "Operation.OperationSecurityOperations.IsEditAllowed": false,
        "Operation.OperationSecurityOperations.IsImportOperationAllowed": false
    ]
    
    //список прав для ClearingGross
    var clearingGrossDictionary = [
        "ClearingGross.ClearingGrossSecurityOperations.IsViewAllowed": false,
        "ClearingGross.ClearingGrossSecurityOperations.IsAddAllowed": false,
        "ClearingGross.ClearingGrossSecurityOperations.IsRemoveAllowed": false,
        "ClearingGross.ClearingGrossSecurityOperations.IsEditAllowed": false,
        "ClearingGross.ClearingGrossSecurityOperations.IsApproveAllowed": false,
        "ClearingGross.ClearingGrossSecurityOperations.IsPrintDocumentsAllowed": false,
        "ClearingGross.ClearingGrossSecurityOperations.IsCreateTemplateAllowed": false
    ]
    
    //список прав для Deposits
    var depositsDictionary = [
        "Deposits.ConversionsSecurityOperations.IsViewAllowed": false,
        "Deposits.ConversionsSecurityOperations.IsAddAllowed": false,
        "Deposits.ConversionsSecurityOperations.IsRemoveAllowed": false,
        "Deposits.ConversionsSecurityOperations.IsApproveAllowed": false,
        "Deposits.ConversionsSecurityOperations.IsPrintDocumentsAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsViewAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsAddAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsRemoveAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsEditAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsOpenDepositAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsCloseDepositAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsApproveAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsPrintDocumentsAllowed": false,
        "Deposits.DepositAccountSecurityOperations.IsCreateTemplateAllowed": false
    ]
    //список прав для Common
    var commonDictionary = [
        "Common.UtilitiesPaymentsSecurityOperations.IsViewAllowed": false,
        "Common.UtilitiesPaymentsSecurityOperations.IsAddAllowed": false,
        "Common.UtilitiesPaymentsSecurityOperations.IsRemoveAllowed": false,
        "Common.UtilitiesPaymentsSecurityOperations.IsEditAllowed": false,
        "Common.UtilitiesPaymentsSecurityOperations.IsApproveAllowed": false,
        "Common.UtilitiesPaymentsSecurityOperations.IsPrintDocumentsAllowed": false,
        "Common.UtilitiesPaymentsSecurityOperations.IsCreateTemplateAllowed": false
    ]
    
    //список прав для Cards
    var cardsDictionary = [
        "Cards.CardsSecurityOperations.IsViewAllowed": false,
        "Cards.CardsSecurityOperations.IsRemoveAllowed": false,
        "Cards.CardsSecurityOperations.IsEditAllowed": false,
        "Cards.CardsSecurityOperations.IsCheckActualBalanceAllowed": false,
        "Cards.CardsSecurityOperations.IsGetExpiryDateAllowed": false,
        "Cards.CardsSecurityOperations.IsGetCardNoAllowed": false,
        "Cards.CardsSecurityOperations.IsAddToStopListAllowed": false,
        "Cards.CardsSecurityOperations.IsRebasePinAllowed": false,
        "Cards.CardsSecurityOperations.IsReplenishmentAllowed": false,
        "Cards.CardsSecurityOperations.IsWithdrawalAllowed": false,
        "Cards.CardsSecurityOperations.IsApproveAllowed": false,
        "Cards.CardsSecurityOperations.IsPrintDocumentsAllowed": false,
        "Cards.CardsSecurityOperations.IsCreateTemplateAllowed": false
    ]
    
    
    
//    определение списка прав для template
    func templateAllow(allowedOperations: [String]) {
        
        var updatedDictViews: Dictionary<String, Bool> = allowViewDictionary
        var updatedDictTemplate: Dictionary<String, Bool> = templateDictionary
        var updatedDictSwift: Dictionary<String, Bool> = swiftDictionary
        var updatedDictLoan: Dictionary<String, Bool> = loanDictionary
        var updatedDictOperation: Dictionary<String, Bool> = operationDictionary
        var updatedDictInternalOperation: Dictionary<String, Bool> = internalOperationDictionary
        var updatedDictClearingGross: Dictionary<String, Bool> = clearingGrossDictionary
        var updatedDictDeposits: Dictionary<String, Bool> = depositsDictionary
        var updatedDictCommon: Dictionary<String, Bool> = commonDictionary
        var updatedDictCards: Dictionary<String, Bool> = cardsDictionary
        
        for operation in allowedOperations{
            if updatedDictViews.keys.contains(operation){
                updatedDictViews.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictTemplate.keys.contains(operation){
                updatedDictTemplate.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictSwift.keys.contains(operation){
                updatedDictSwift.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictLoan.keys.contains(operation){
                updatedDictLoan.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictOperation.keys.contains(operation){
                updatedDictOperation.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictInternalOperation.keys.contains(operation){
                updatedDictInternalOperation.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictClearingGross.keys.contains(operation){
                updatedDictClearingGross.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictDeposits.keys.contains(operation){
                updatedDictDeposits.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictCommon.keys.contains(operation){
                updatedDictCommon.updateValue(true, forKey: operation)
            }
        }
        for operation in allowedOperations{
            if updatedDictCards.keys.contains(operation){
                updatedDictCards.updateValue(true, forKey: operation)
            }
        }
        UserDefaultsHelper.allowedViewOperations = updatedDictViews
        UserDefaultsHelper.allowedTemplateOperations = updatedDictTemplate
        UserDefaultsHelper.allowedSwiftOperations = updatedDictSwift
        UserDefaultsHelper.allowedLoansOperations = updatedDictLoan
        UserDefaultsHelper.allowedOperationOperations = updatedDictOperation
        UserDefaultsHelper.allowedInternalOperationOperations = updatedDictInternalOperation
        UserDefaultsHelper.allowedClearingGrossOperations = updatedDictClearingGross
        UserDefaultsHelper.allowedDepositsOperations = updatedDictDeposits
        UserDefaultsHelper.allowedUtilitiesOperations = updatedDictCommon
        UserDefaultsHelper.allowedCardsOperations = updatedDictCards
  
    }
    
//  определение права для template operation
    func templateAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let templateAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedTemplateOperations{
            for allow in templateAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    //  определение права для view operation
    func viewAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedViewOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    //  определение права для swift operation
    func swiftAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedSwiftOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    
    //  определение права для loan operation
    func loanAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedLoansOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    //  определение права для Operation operation
    func operationAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedOperationOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    //  определение права для InternalOperation operation
    func operationAllowInternalOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedInternalOperationOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    //  определение права для ClearingGross operation
    func clearingGrossAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedClearingGrossOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    //  определение права для Deposits operation
    func depositsAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedDepositsOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    //  определение права для Common operation
    func commonAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedUtilitiesOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
    
    //  определение права для Cards operation
    func cardsAllowOpertaion(nameOperation: String) -> Bool {
        var isAllowed = false
        if let viewAllowDict: Dictionary<String, Bool> = UserDefaultsHelper.allowedCardsOperations{
            for allow in viewAllowDict{
                if nameOperation == allow.key{
                    isAllowed = allow.value
                }
            }
        }
        return isAllowed
    }
}
