//
//  LocalizeHelper.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/27/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import RxSwift
import PDFReader



class LocalizeHelper: BaseViewController {
   
    //MARK: singleton
    public static var shared = LocalizeHelper()
    
    
    var localizeStrings: Localize!
    //    получение getLocalization
    func getLocalization(culture: String){
        
        showLoading()
        CleanDictInDocDirectory()
        
        let getLocalization:Observable<Localize> = managerApi
            .getLocalization(culture: culture)
        
        getLocalization
            .subscribe(
                onNext: {[weak self] (localization) in
                    guard let `self` = self else { return }
                    UserDefaultsHelper.localize = localization
                    
                    self.addLocalizeString()
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    let alert = UIAlertController(title: nil, message: ApiHelper.shared.errorHelper(error: error), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
   

//    преобразование переводов в словарь
    func addLocalizeString() {
        
        if let locStrings = UserDefaultsHelper.localize{
            localizeStrings = locStrings
            let Language = localizeStrings.toJSON()
            let dictLocalize = Language as! Dictionary<String, String>
            SaveDictInDocDirectory(customDict:dictLocalize )
            
        }
    }
//   сохранение переводов
    func SaveDictInDocDirectory(customDict:Dictionary<String, String> ){
        
        let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let customPlistURL = docsBaseURL.appendingPathComponent("LocalizeStrings.plist")
        NSDictionary(dictionary: customDict).write(to: customPlistURL, atomically: true)
        
    }
//    удаление словаря
    func CleanDictInDocDirectory(){
        let customDict:Dictionary<String, String> = [:]
        let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let customPlistURL = docsBaseURL.appendingPathComponent("LocalizeStrings.plist")
        NSDictionary(dictionary: customDict).write(to: customPlistURL, atomically: true)
    }
    //   редактирование со словаря
    func addWord(_ word: String,_ inString: String ) -> String{
        var editedString = ""
        
        editedString = inString.replacingOccurrences(of: "%1$s", with: word)
        
        return editedString
    }
}
