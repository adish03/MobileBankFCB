//
//  ExelFileHelper.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 5/31/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import RxSwift
import PDFReader



class ExcelFileHelper: BaseViewController {
    
    //MARK: singleton
    public static var shared = ExcelFileHelper()
    
    
    //   сохранение переводов
    func saveFileInDocDirectory(excelFile: Data, _ name: String = "" ) -> URL {
        
        let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let customPlistURL = docsBaseURL.appendingPathComponent("\(name)_excelFile.xls")
        try? excelFile.write(to: customPlistURL, options: .atomic)
        return customPlistURL
    }
    
    func saveFileDirectory(docxFile: Data , _ name: String = "document.docx") -> URL {
        let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let customPlistURL = docsBaseURL.appendingPathComponent(name)
        try? docxFile.write(to: customPlistURL, options: .atomic)
        return customPlistURL
    }
    
    //    удаление excelFile
    func cleanExcelInDocDirectory(){
        let excelFile = ""
        let docsBaseURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let customPlistURL = docsBaseURL.appendingPathComponent("excelFile.xls")
        try? excelFile.write(to: customPlistURL, atomically: true, encoding: .utf8)
        
    }
    
}
