//
//  DatePickerHelper.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/15/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import RxSwift

// Общий класс для DatePicker
class DatePickerHelper: BaseViewController, UITextFieldDelegate {
    public static let shared = DatePickerHelper()
    
    var newDate = ""
    var datePicker: UIDatePicker?
    var dateFrom = "2000-01-01T00:00:00"
    var dateTo = "2000-01-01T00:00:00"

    func datePickerSet(textField: UITextField) {
        textField.delegate = self
        datePicker?.datePickerMode = .date
        textField.inputView = datePicker
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        var components = DateComponents()
        components.year = 0
        components.day = 0
        components.month = 0
        
        let dateFromDate = dateFormater.date(from: dateFrom)
        let dateToDate = dateFormater.date(from: dateTo)
        
        let minDate = Calendar.current.date(byAdding: components, to: dateFromDate!)
        let maxDate = Calendar.current.date(byAdding: components, to: dateToDate!)
        
        datePicker?.minimumDate = minDate
        datePicker?.maximumDate = maxDate
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done".localized, style: .plain, target: self, action: #selector(self.donePressed(sender:)));
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        
        let cancelButton = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        textField.inputAccessoryView = toolbar
        textField.inputView = datePicker
        
    }
    
    
    @objc func donePressed(sender: UITextField ){
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "dd MMMM yyyy"
        
        dateFormater.locale = Locale(identifier: "language".localized)
//        textField.text = dateFormater.string(from: ((datePicker!.date)))
        self.view.endEditing(true)
        
    }
    @objc func cancelDatePicker(){
        
        self.view.endEditing(true)
        
    }
    func convertDateFormatter(date: String) -> String {
        let dateFormatter = DateFormatter()
        let dateFormatter1 = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy"//this your string date format
        dateFormatter1.dateFormat = "yyyy-MM-dd"
        dateFormatter.locale = Locale(identifier: "language".localized)
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }
        let timeStamp = dateFormatter1.string(from: convertedDate!)
        
        return timeStamp
    }
    
    func convertDate(date: String) -> String {
        let dateFormatter = DateFormatter()
        let dateFormatter1 = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//this your string date format
        let convertedDate = dateFormatter.date(from: date)
        
        guard dateFormatter.date(from: date) != nil else {
            assert(false, "no date from string")
            return ""
        }
        dateFormatter1.dateFormat = "dd MMMM yyyy"//this your string date format
        dateFormatter1.locale = Locale(identifier: "language".localized)
        let timeStamp = dateFormatter1.string(from: convertedDate!)
        
        return timeStamp
    }

}
