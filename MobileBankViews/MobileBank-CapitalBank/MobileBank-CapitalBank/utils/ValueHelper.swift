//
//  ValueHelper.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/25/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import RxSwift

// Общий класс для получения и сохранения данных используемых в приложении
class  ValueHelper: BaseViewController{
    public static let shared = ValueHelper()
    // определение вида перевода
    func operationTypeDefine(operationTypeId: Int) -> String{
        var operationType = ""
        switch operationTypeId {
        case InternetBankingOperationType.InternalOperation.rawValue:
            operationType = localizedText(key: "Internal_operation")
            
        case InternetBankingOperationType.InternalOperationCustomerAccounts.rawValue:
            operationType =  localizedText(key: "Resources_Internal_Customer_Transaction")
            
        case InternetBankingOperationType.CliringTransfer.rawValue:
            operationType =  localizedText(key: "clearing_payment")
            
        case InternetBankingOperationType.GrossTransfer.rawValue:
            operationType = localizedText(key: "gross_payment")
            
        case InternetBankingOperationType.SwiftTransfer.rawValue:
            operationType = localizedText(key: "swift_payment")
            
        default:
            print("")
        }
        return operationType
    }
    //    определение типа операции
    func confirmTypeDefine(confirmType: Int) -> (String?, UIColor?){
        var operationType: (String?, UIColor?) = ("", #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        switch confirmType {
        case ConfirmType.Save.rawValue:
            operationType = (localizedText(key: "SaveSuccess"), #colorLiteral(red: 0.2549019608, green: 0.6588235294, blue: 0.3725490196, alpha: 1))
            
        case ConfirmType.Confirm.rawValue:
            operationType = (localizedText(key: "sent_to_bank"), UIColor(hexFromString: Constants.MAIN_COLOR))
            
        case ConfirmType.InConfirm.rawValue:
            operationType = (localizedText(key: "in_processing"), #colorLiteral(red: 0.5176470588, green: 0.5529411765, blue: 0.5803921569, alpha: 1))
            
        case ConfirmType.Error.rawValue:
            operationType = (localizedText(key: "Error"), #colorLiteral(red: 1, green: 0.231372549, blue: 0.1882352941, alpha: 1))
            
        case ConfirmType.Verified.rawValue:
            operationType = (localizedText(key: "approved"), UIColor(hexFromString: Constants.MAIN_COLOR))
            
        default:
           operationType = ("", #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1))
        }
    
        return operationType
    }
    //    получение символа валюты
    func getCurrentCurrency(currnecyID: Int) -> String{
        var currensySymbol = ""
        var allCurrencies: [Currency] = []
        if let currencies = UserDefaultsHelper.currencies{
            allCurrencies = currencies
        }
        
        for currency in allCurrencies{
            if currency.currencyID == currnecyID{
                currensySymbol = currency.symbol ?? ""
            }
        }
        return currensySymbol
    }
}
