//
//  ApiBaseViewController.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/28/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import RxSwift
import AlamofireImage
import Alamofire
import RxCocoa
import MobileBankCore
import SVProgressHUD

class ApiBaseViewController {

    let disposeBag = DisposeBag()
    let managerApi = Manager()
    
    public func showLoading(){
        SVProgressHUD.show()
    }
    public func hideLoading(){
        SVProgressHUD.dismiss()
    }
    
    func  getImage(image: String, cell: CategoryTableViewCell?, cell1: SubCategoryTableViewCell?){
        
        showLoading()
        managerApi
            .getImage(imageName: image)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {(data) in
                    DispatchQueue.main.async {
                        cell?.categoryImage.image = Image(data: data)
                        cell1?.imageSubCategory.image = Image(data: data)
                    }
                    self.hideLoading()
            },
                onError: {(error) in
                    print(error)
                    self.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    

}
