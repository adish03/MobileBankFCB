//
//  ConfirmOperation.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 3/20/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import RxSwift

class ConfirmOperation: BaseViewController{
    public static let shared = ConfirmOperation()
    
    
    //    подтверждение операции
    func postConfirmPay(operationModel: OperationModel, storyBoard: String, viewController: String){
        
        managerApi
            .postConfirmUtility(OperationModel: operationModel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] result in
                    guard let `self` = self else { return }
                    
                    if result.message != ""{
                        self.showAlertController(result.message ?? "")
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    self?.showAlertController(ApiHelper.shared.errorHelper(error: error))
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
}
