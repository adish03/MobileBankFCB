//
//  ApiHelper.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 4/2/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import Foundation
import MobileBankCore
import RxSwift
import PDFReader


// Общий класс для получения и сохранения данных используемых в приложении
class ApiHelper: BaseViewController{
    public static let shared = ApiHelper()
    
    //получение данных
    func  getSwiftData(){
        // данные операций
        let  RefOpersObservabel =  managerApi
            .getRefOpers()
        // данные валютных операций
        let  VOCodesObservabel =  managerApi
            .getVOCodes()
        // данные операций
        let  ExpenseTypesObservabel =  managerApi
            .getExpenseTypes()
        // данные кодов платежей
        let  paymentsCodesObservabel =  managerApi
            .getPaymentsCodes()
        // данные бик кодов
        let  bikCodesObservabel =  managerApi
            .getBikCodes()
        // данные для проверки создания нового перевода
        let  swiftValidateSettingsObservabel =  managerApi
            .getValidateSettings()
        
        
        showLoading()
        Observable
            .zip(RefOpersObservabel,
                 VOCodesObservabel,
                 ExpenseTypesObservabel,
                 paymentsCodesObservabel,
                 bikCodesObservabel,
                 swiftValidateSettingsObservabel)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self](refOpers, voCodes, expenseTypes, paymentsCodes, bikCodes, swiftValidateSettings) in
                    guard let `self` = self else { return }
                    // сохранение данных
                    UserDefaultsHelper.refOpers = refOpers
                    UserDefaultsHelper.voCodes = voCodes
                    UserDefaultsHelper.expenseTypes = expenseTypes
                    UserDefaultsHelper.paymentsCodes = paymentsCodes
                    UserDefaultsHelper.bikCodes = bikCodes
                    UserDefaultsHelper.requireVoCodeForCurrencies = swiftValidateSettings
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    let alert = UIAlertController(title: nil, message: ApiHelper.shared.errorHelper(error: error), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    // обработка ошибок
    func errorHelper(error: Error) -> String {
        
        var errorMessage = ""
        errorMessage = error.localizedDescription
        if (error.pinError?.statusCode) != nil{
            if let statusCode = (error.pinError?.statusCode){
                if statusCode == 401{
                    errorMessage = error.pinError?.exceptionMessage ?? error.localizedDescription
                    
                    if let loginId = SessionManager.shared.user?.loginID{

                        SessionManager.shared.user = nil
                        UserDefaultsHelper.PIN_new = ""
                        UserDefaultsHelper.pinIsEnabled = false
                        UserDefaultsHelper.pinNewIsEnabled = false
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOut"), object: nil)
                        SessionManager.shared.logOutUser(loginID: loginId)
                    }
                    
                    let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc
                }
                if statusCode >= 400 && statusCode <= 499 {
                    errorMessage = error.pinError?.message ?? error.localizedDescription
                }
                if statusCode == 400 {
                    errorMessage = error.pinError?.message ?? error.localizedDescription
                }
                if statusCode == 500 {
                    errorMessage = error.pinError?.exceptionMessage ?? error.localizedDescription
                }
            }
        }
        return errorMessage
    }
    // обработка ошибок API
    func errorHelperApi(error: Error) -> String {
        
        var errorMessage = ""
        errorMessage = error.localizedDescription
        if (error.apiError?.statusCode) != nil{
            if let statusCode = (error.apiError?.statusCode){
                if statusCode >= 500{
                    if let loginId = SessionManager.shared.user?.loginID{
                        
                        SessionManager.shared.user = nil
                        UserDefaultsHelper.PIN_new = ""
                        UserDefaultsHelper.pinIsEnabled = false
                        UserDefaultsHelper.pinNewIsEnabled = false
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOut"), object: nil)
                        SessionManager.shared.logOutUser(loginID: loginId)
                    }
                    errorMessage = error.pinError?.message ?? error.localizedDescription
                    let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc

//                    errorMessage = error.apiError?.error ?? error.localizedDescription
                }
                if statusCode > 400 && statusCode <= 499 {
                    errorMessage = error.pinError?.message ?? error.localizedDescription
                    let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc
                }
                if statusCode == 400 {
                    if error.apiError?.error_description == nil{
                        let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = vc
                    }else{
                        errorMessage = error.apiError?.error_description ?? error.localizedDescription
                    }
                }
            }
        }
        return errorMessage
    }
    // обработка ошибок API
    func errorHelperSMS(error: Error) -> String {
        
        var errorMessage = ""
        errorMessage = error.localizedDescription
        if (error.apiError?.statusCode) != nil{
            if let statusCode = (error.apiError?.statusCode){
                if statusCode >= 500{
                    if let loginId = SessionManager.shared.user?.loginID{
                        
                        SessionManager.shared.user = nil
                        UserDefaultsHelper.PIN_new = ""
                        UserDefaultsHelper.pinIsEnabled = false
                        UserDefaultsHelper.pinNewIsEnabled = false
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOut"), object: nil)
                        SessionManager.shared.logOutUser(loginID: loginId)
                    }
                    errorMessage = error.apiError?.error_description ?? error.localizedDescription
                    let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc
                    
                }
                if statusCode > 400 && statusCode <= 499 {
                    errorMessage = error.apiError?.error_description ?? error.localizedDescription
                }
                if statusCode == 400 {
                    if error.apiError?.error_description == nil{
                        errorMessage = error.apiError?.error ?? error.localizedDescription
                    }else{
                        errorMessage = error.apiError?.error_description ?? error.localizedDescription
                    }
                }
            }
        }
        return errorMessage
    }
    // обработка ошибок API
    func errorHelperChangePassword(error: Error) -> String {
        
        var errorMessage = ""
        errorMessage = error.localizedDescription
        if (error.apiError?.statusCode) != nil{
            if let statusCode = (error.apiError?.statusCode){
                if statusCode >= 500{
                    if let loginId = SessionManager.shared.user?.loginID{
                        
                        SessionManager.shared.user = nil
                        UserDefaultsHelper.PIN_new = ""
                        UserDefaultsHelper.pinIsEnabled = false
                        UserDefaultsHelper.pinNewIsEnabled = false
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOut"), object: nil)
                        SessionManager.shared.logOutUser(loginID: loginId)
                    }
                    errorMessage = error.apiError?.error_description ?? error.localizedDescription
                    let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                    let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = vc
                    
                }
                if statusCode > 400 && statusCode <= 499 {
                    errorMessage = error.apiError?.error_description ?? error.localizedDescription
                }
                if statusCode == 400 {
                    errorMessage = "Не удалось изменить пароль"
                }
            }
        }
        return errorMessage
    }
    //  получение инфо операции
    func  getInfoOperationFile(operationID: Int, code: Int, operationTypeID: Int, vc: UIViewController) {
        showLoading()
        managerApi
            .getInfoOperationFile(operationID: operationID, code: code, operationTypeID: operationTypeID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] filePDFString in
                    guard let `self` = self else { return }
                    
                    if  let stringPDF = filePDFString.reportDocuments?[0].fileBody{
                        let name = filePDFString.reportDocuments?[0].fileName
                        let path = self.shareFile(fileBody: stringPDF, fileName: name!)
                        let fileManager = FileManager.default
                        if fileManager.fileExists(atPath: path){
                            let document = NSData(contentsOfFile: path)
                            let url = NSURL.fileURL(withPath: path)
                            let activityViewController: CustomActivityViewController = CustomActivityViewController(activityItems: [url], applicationActivities: nil)
                            activityViewController.popoverPresentationController?.sourceView=self.view
                            vc.present(activityViewController, animated: true)
                        }
                        else {
                            print("document was not found")
                        }
                        self.hideLoading()
                    }
                    self.hideLoading()
                },
                onError: {[weak self] error in
                    var errorMessage = ""
                    errorMessage = error.localizedDescription
                    if (error.apiErrors?.statusCode) != nil{
                        if let statusCode = (error.apiErrors?.statusCode){
                            if statusCode >= 500{
                                errorMessage = error.apiErrors?.exceptionMessage ?? error.localizedDescription
                            }
                            if statusCode >= 400 && statusCode <= 499 {
                                errorMessage = error.apiErrors?.message ?? error.localizedDescription
                            }
                            if statusCode == 400 {
                                errorMessage = error.apiErrors?.message ?? error.localizedDescription
                            }
                        }
                    }
                    let alert = UIAlertController(title: nil, message: errorMessage, preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    // gets latters after last dot
    func getLastLatter(fileName: String) -> String {
        let fileArray = fileName.components(separatedBy: ".")
        let finalFileName = fileArray.last
        return finalFileName!
    }
    
    func shareFile(fileBody: String, fileName: String) -> String{
        let fileManager = FileManager.default
        
        var finalFileName = getLastLatter(fileName: fileName)
        
        if(finalFileName == "docx" || finalFileName == "doc"){
            finalFileName = fileName.replacingOccurrences(of: "._", with: "", options: .literal, range: nil)
        }else{
            finalFileName = fileName
        }
        
        let path = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(finalFileName)
        
        let doc = NSData(base64Encoded: fileBody, options: NSData.Base64DecodingOptions(rawValue: 0))
        
        fileManager.createFile(atPath: path as String, contents: doc as Data?, attributes: nil)
        return path
   }

    
     //  получение инфо операции swift
    func  getInfoOperationFileSwift(transferID: Int, vc: UIViewController) {
        showLoading()
        managerApi
            .getDocumentsSwift(TransferID: transferID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] filePDFString in
                    guard let `self` = self else { return }
                    if  let stringPDF = filePDFString.reportDocuments?[0].fileBody{
                        let name = filePDFString.reportDocuments?[0].fileName
                        let path = self.shareFile(fileBody: stringPDF, fileName: name!)
                        let fileManager = FileManager.default
                        if fileManager.fileExists(atPath: path){
                            let document = NSData(contentsOfFile: path)
                            let activityViewController: CustomActivityViewController = CustomActivityViewController(activityItems: [document!], applicationActivities: nil)
                            activityViewController.popoverPresentationController?.sourceView=self.view
                            vc.present(activityViewController, animated: true)
                        }
                        else {
                            print("document was not found")
                        }
                        self.hideLoading()
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    let alert = UIAlertController(title: nil, message: ApiHelper.shared.errorHelper(error: error), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    //  получение инфо операции Clearing/Gross
    func  getInfoOperationFileClearingGross(paymentID: Int, vc: UIViewController) {
        showLoading()
        managerApi
            .getDocumentsClearingGross(paymentID: paymentID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: {[weak self] filePDFString in
                    guard let `self` = self else { return }
                    if  let stringPDF = filePDFString.reportDocuments?[0].fileBody{
                        let name = filePDFString.reportDocuments?[0].fileName
                        let path = self.shareFile(fileBody: stringPDF, fileName: name!)
                        let fileManager = FileManager.default
                        if fileManager.fileExists(atPath: path){
                            let document = NSData(contentsOfFile: path)
                            let activityViewController: CustomActivityViewController = CustomActivityViewController(activityItems: [document!], applicationActivities: nil)
                            activityViewController.popoverPresentationController?.sourceView=self.view
                            vc.present(activityViewController, animated: true)
                        }
                        else {
                            print("document was not found")
                        }
                        self.hideLoading()
                    }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    let alert = UIAlertController(title: nil, message: ApiHelper.shared.errorHelper(error: error), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    // удаление шаблона
    func deleteTemplate(operationID: String){
        showLoading()
        
        managerApi
            .deleteTemplate(operationID: operationID)
            .updateTokenIfNeeded()
            .subscribe(
                onNext: { [weak self] response in
                    guard let `self` = self else { return }
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    let alert = UIAlertController(title: nil, message: ApiHelper.shared.errorHelper(error: error), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
    
    public enum ActionStyle {
        case print
        case activitySheet
        case customAction(() -> ())
    }
    // получение прав
    func getAllowedOperations(){
        showLoading()
        
        managerApi
            .getAllowedOperations()
            .updateTokenIfNeeded()
            .subscribe(
                onNext: { [weak self] allowedOperations in
                    guard let `self` = self else { return }
                    var operations = [String]()
                    for allowedOperation in allowedOperations{
                        let operation = allowedOperation.replacingOccurrences(of: "InternetBanking.Security.Common.Implementations.", with: "")
                        operations.append(operation)
                    }
                    

                    AllowHelper.shared.templateAllow(allowedOperations: operations)
                    
                    self.hideLoading()
            },
                onError: {[weak self] error in
                    
                    let alert = UIAlertController(title: nil, message: ApiHelper.shared.errorHelper(error: error), preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
                    //
                    self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }
}
