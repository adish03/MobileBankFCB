//
//  AppDelegate.swift
//  MobileBank-CapitalBank
//
//  Created by Oleg Ten on 2/4/19.
//  Copyright © 2019 Spalmalo. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import MobileBankCore
import SVProgressHUD
import Firebase


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        if #available(iOS 13.0, *) {
            window?.overrideUserInterfaceStyle = .light
        }
        FirebaseApp.configure()
        
        //init Api manager
        Manager.setup(url: Constants.API_MAIN, disableEvaluationUrl: Constants.API_MAIN_URL)
        
        //To change Navigation Bar Background Color
        UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        UINavigationBar.appearance().shadowImage = UIImage()
        UINavigationBar.appearance().backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        UINavigationBar.appearance().barTintColor = UIColor(hexFromString: Constants.MAIN_COLOR)
        // удаление всех кнопок Back в NavBar
        UIBarButtonItem.appearance().setTitleTextAttributes([.foregroundColor: UIColor.clear], for: .normal)

        //To change Back button title & icon color
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        UIApplication.statusBarBackgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)
//        UIApplication.shared.statusBarView.backgroundColor = UIColor(hexFromString: Constants.MAIN_COLOR)


        IQKeyboardManager.shared.enable = true
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        SVProgressHUD.setForegroundColor(UIColor(hexFromString: Constants.MAIN_COLOR))           //Ring Color
        SVProgressHUD.setBackgroundColor(UIColor(hexFromString: Constants.COLOR_BACKGROUND))        //HUD Color
       
        
        if SessionManager.shared.isLoggedIn{
             if !UserDefaultsHelper.pinIsEnabled{
                UserDefaultsHelper.pinIsEnabled = false
                let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                vc.modalPresentationStyle = .fullScreen
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController?.dismiss(animated: false, completion: nil)
                appDelegate.window?.rootViewController = vc
            }else{
                UserDefaultsHelper.pinIsEnabled = true
                let storyboard =  UIStoryboard(name: "Launch", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "LaunchViewController")
                vc.modalPresentationStyle = .fullScreen
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = vc
            }
        }else{
            UserDefaultsHelper.pinIsEnabled = false
            let storyboard =  UIStoryboard(name: "Login", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
            vc.modalPresentationStyle = .fullScreen
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController?.dismiss(animated: false, completion: nil)
            appDelegate.window?.rootViewController = vc

        }

        return true
    }
    

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        if  UserDefaults.standard.bool(forKey: "isNeedChangeUser") == true{
                UserDefaults.standard.set(false, forKey: "isNeedDismissPinCintroller")
        }
        dismissAnyAlertControllerIfPresent()
       
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

        if SessionManager.shared.isLoggedIn{
            if !UserDefaultsHelper.pinIsEnabled{
                UserDefaultsHelper.pinIsEnabled = false
                let storyboard =  UIStoryboard(name: "Login", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
                vc.modalPresentationStyle = .fullScreen
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController?.dismiss(animated: false, completion: nil)
                appDelegate.window?.rootViewController = vc
            }else{
                UserDefaultsHelper.pinIsEnabled = true
            
                if let controller = UIStoryboard(name: "Launch", bundle: nil).instantiateViewController(withIdentifier: "LaunchViewController") as? LaunchViewController {
                    if let window = self.window, let rootViewController = window.rootViewController {
                        var currentController = rootViewController
                        while let presentedController = currentController.presentedViewController {
                            currentController = presentedController
                        }
                        controller.modalPresentationStyle = .fullScreen
                        currentController.present(controller, animated: true, completion: nil)
                    }
                }
            }
        }else{
            UserDefaultsHelper.pinIsEnabled = false
            let storyboard =  UIStoryboard(name: "Login", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "NavLogin")
            vc.modalPresentationStyle = .fullScreen
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController?.dismiss(animated: false, completion: nil)
            appDelegate.window?.rootViewController = vc
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.set(false, forKey: "isNeedDismissPinCintroller")
        UserDefaults.standard.set(false, forKey: "isNeedDismissLaunchController")
    }
    
//     закрытие всех AlertControllers
    func dismissAnyAlertControllerIfPresent() {
        guard let window :UIWindow = UIApplication.shared.keyWindow , var topVC = window.rootViewController?.presentedViewController else {return}
        while topVC.presentedViewController != nil  {
            topVC = topVC.presentedViewController!
        }
        if topVC.isKind(of: UIAlertController.self) {
            topVC.dismiss(animated: false, completion: nil)
        }
    }
}

extension UINavigationController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}


extension UIApplication {
    
    var statusBarView: UIView{
            return value(forKey: "statusBar") as! UIView
    }
    
    class var statusBarBackgroundColor: UIColor? {
        get {
            return statusBarView?.backgroundColor
        } set {
            statusBarView?.backgroundColor = newValue
        }
    }
    
    class var statusBarView: UIView? {
        if #available(iOS 13.0, *) {
            let tag = 987654321
            
            if let statusBar = UIApplication.shared.keyWindow?.viewWithTag(tag) {
                statusBar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                
                return statusBar
            }else {
                let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
                statusBarView.tag = tag
                statusBarView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                UIApplication.shared.keyWindow?.addSubview(statusBarView)
                return statusBarView
            }
        } else {
            if responds(to: Selector(("statusBar"))) {
                return value(forKey: "statusBar") as? UIView
            }
        }
        return nil
    }
    
}

extension UIApplication {
    
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}

extension AppDelegate {
    
    func hidesPinControllerOrTouchID() {
        if let window = UIApplication.shared.keyWindow {
            window.rootViewController?.dismiss(animated: true, completion: nil)
        }
    }
}
